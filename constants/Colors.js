export default {
    primary: '#C2185B',
    accent: '#FFC107',
    error: '#FF0000'
};