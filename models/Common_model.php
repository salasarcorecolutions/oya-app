<?php
/**
 * This model will be auto loaded from ci
 * No need to load additionally 
 * model name 'common_model' 
 *
 */
class Common_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
        //Loding amazone library is taking enormas time
        //$this->load->library('Amazon_library/S3upload');
    }

    public function state_master()
    {
        $sql = "select * from state_master";
        return $this->db->query($sql)->result_array();
    }

	public function country_master()
    {
        $sql = "select * from country_master where  status=1 order by name";
        return $this->db->query($sql)->result_array();
    }

	public function get_state_by_country_id($country_id)
    {
        $sql = "SELECT * FROM state_master WHERE country_id = '".$country_id."' and status=1 order by state";
        return $this->db->query($sql)->result_array();
    }
    public function get_cities_by_state_id($state_id)
    {
        $sql = "SELECT * FROM city_master WHERE state_id = '".$state_id."' and status=1";
        return $this->db->query($sql)->result_array();
    }
	
	/**
     * to get the list of time zone
     */
    public function get_time_zones($country_id='')
    {
        $this->db->select('name');
        $this->db->where('id',$country_id);
        $row = $this->db->get('country_master')->row_array();
        $country_name = $row['name']; 
        if ( ! empty($country_name))
        {
            $this->db->where('country_name',trim($country_name));
        }
        $this->db->select('country_name, timezone, time_zone_description, gmt_offset,time_zone_master_id');
        return $this->db->get('time_zone_master')->result_array();
    }
	
	// un-used common functions
	public function fetch_state_by_country_name($country_name)
    {
        $sql = "SELECT s.* FROM state_master s
				LEFT JOIN country_master c ON c.id = s.country_id
				WHERE c.name = '".$country_name."'";
        return $this->db->query($sql)->result_array();
    }

	public function fetch_city_by_state_name($state_name)
    {
        $sql = "SELECT c.* FROM city_master c
				LEFT JOIN state_master s ON s.id = c.state_id
				WHERE s.state = '".$state_name."'";
        return $this->db->query($sql)->result_array();
    }
    public function fetch_city_by_country_code($country_code)
    {
        $sql = "SELECT DISTINCT c.name FROM country_master cm 
                LEFT JOIN state_master s ON cm.id= s.country_id 
                LEFT JOIN city_master c ON s.id = c.state_id 
                WHERE sortname= 'IN' ORDER BY NAME asc";
        return $this->db->query($sql)->result_array();
    }


    public function product_categories()
    {
		$array_index=0;
		$data=array();
		$sql="SELECT parent_id,category_id,category_name,category_image FROM buy_sell_category WHERE parent_id = 0 ORDER BY category_order";
		$query = $this->db->query($sql);
		foreach ($query->result_array() as $row){
			$data[$array_index]['category_name']=$row['category_name'];
			$data[$array_index]['category_image']=$row['category_image'];
			$sql="SELECT category_name FROM buy_sell_category WHERE parent_id=".$row['category_id']." ";
			$data[$array_index]['sub_menu'] = $this->db->query($sql)->result_array();
			$array_index=$array_index+1;
		} 	
		return $data;	
	}

    public function auction_categories()
    {
		$array_index=0;
		$data=array(); 
			$sql="SELECT id,cat_name,category_image FROM material_master_category ";
			$query = $this->db->query($sql);
			foreach ($query->result_array() as $row){
				$data[$array_index]['category_name']=$row['cat_name'];
				$data[$array_index]['category_image']=$row['category_image'];
				$data[$array_index]['category_id']=$row['id'];

				$sql="SELECT mat_name FROM material_master WHERE cat_id=".$row['id']." ";
				$data[$array_index]['sub_menu'] = $this->db->query($sql)->result_array();
				$array_index=$array_index+1;
			}	
		return $data;	
    }
    
    
    /**
     * To get bidder bid currency details
     * @param  $auction_id
     * @param  $bidder_id
     */
    public function auction_bidder_currency($auction_id, $bidder_id)
    {
        $data=array();
        $date_sql   = "select ynk_auctionid,primary_currency,auction_currency,start_date,end_date,ynk_auctionid,bid_logic from ynk_auction where md5(ynk_auctionid) ='".$auction_id."'";
        $res        = $this->db->query($date_sql);
        $row3       = $res->row();
        $aucid         =$row3->ynk_auctionid;
        $primary_currency_name   = $row3->auction_currency;
        $primary_currency_id   = $row3->primary_currency;
        $currency_conversion_rate   = 1;
        $currency_sql         = "SELECT YC.currency_conversion_rate,CM.currency_name,AT.prefered_currency
                                  FROM  ynk_auction_terms_accept AT,ynk_auction_currency YC,currency_mst CM
                                  WHERE CM.id=AT.prefered_currency and YC.other_currency_id=CM.id
                                  AND AT.ynk_auction_id =".$aucid." AND AT.client_id=".$bidder_id;
        $res1             = $this->db->query($currency_sql);
        if($res1->num_rows()>0)
        {
            $row              = $res1->row();
            $currency_conversion_rate   = $row->currency_conversion_rate;
            $prefered_currency_id   = $row->prefered_currency;
            $prefered_currency_name   = $row->currency_name;
            
        }
        else
        {
            $currency_conversion_rate   = 1;
            $prefered_currency_id   = $primary_currency_id;
            $prefered_currency_name   = $primary_currency_name;
            
        }
        
        $data=array(
            'primary_currency_id'=>$primary_currency_id,
            'primary_currency_name'=>$primary_currency_name,
            'prefered_currency_id'=>$prefered_currency_id,
            'prefered_currency_name'=>$prefered_currency_name,
            'currency_conversion_rate'=>$currency_conversion_rate
        );
        return $data;
    }
   
    /**
     * Email Declaration
     *
     * function send_common_email-to send mail functionality.
     *
     * @param	string	the URI segments of the form destination
     * @param	string	Subject of the mail
     * @param	string 	Body of the email
     * @return	array   array/string of bcc   
     */
    public function send_common_email($to, $subject, $message)
    {
        $config = Array(
            'protocol' => 'smtp',
            'smtp_host' => smtpHost,
            'smtp_port' => smtpPort,
            'smtp_user' => smtpUsername,
            'smtp_pass' => smtpPassword,
            'mailtype' => 'html'
        );
        $this->load->library('email', $config);
        $this->email->set_newline("\r\n");
        $this->email->from('admin@salasarauction.com', 'Salasar Auction');
		$this->email->reply_to('scs@salasarauction.com', 'Salasar Auction');
		$this->email->to($to);  
        $this->email->subject($subject);
        $this->email->message($message);

        if ( ! $this->email->send())
		{
			$this->email->clear();
            return false;
        }
		else
		{
			$this->email->clear();
            return true;
		}
    }

	
    public function send_common_sms($mob, $hash = array(), $template_id, $send_via = 'smsacharya', $conperson = "", $registered = "", $bidder_id = "", $sent_by = 'PRODUCT')
    {
		
        $this->load->library('sms');
		$this->load->helper('sms_helper');
		
		$sql = "SELECT * FROM global_sms_templates WHERE template_id = $template_id";
        $sms_template_data = $this->db->query($sql)->result_array();
		if ( ! empty($sms_template_data))
        {
			$template = $sms_template_data[0]['template_format'];
			$msg = getSmsMessage($template,$hash);
			$msg_count = ceil(strlen($msg)/160);
			$transaction_id = $this->sms->sendMsg($mob, $msg, "SCSAUC", "OYA.AUCTION",$template_id,'',$send_via);
			
			if ( ! empty($template_id))
            {
				$sms_type = "T";
			}
            else
            {
				$sms_type = "P";
			}
			// Log SMS
			$query1   = "insert into sms_transaction_log(sms_type,bidder_id,name,message, mobile_no, flag,transaction_id,delivery_status,log_text,message_count,sent_by,type_of_client) values('".$sms_type."','".$bidder_id."','".$conperson."','".$msg."','".$mob."','Y','".$transaction_id."','Sent','','".$msg_count."','".$sent_by."','".$registered."')" ;
			$result1 = $this->db->query($query1);
			if ($result1)
            {
				if($sms_type == "Transactional")
                {
					$update_used_msg = "UPDATE sms_counter SET transactional_msg_used = transactional_msg_used+".$msg_count;
				}
                else
                {
					$update_used_msg = "UPDATE sms_counter SET promotional_msg_used = promotional_msg_used+".$msg_count;
				}
				$this->db->query($update_used_msg);
			}
		}
    }
    
	
    /**
     * This function login will check whether buyer logged in or not.
     */
    public function isBidderLoggedIn()
    {
		return ($this->session->userdata('bidder_id')=="") ? false : true;
	}

   
	public function auction_details($auction_id)
	{
	    $sql  = "SELECT ynk_auctionid,ynk_saleno,auctiontype, start_date, end_date,stime, etime,a.on_page_bid,primary_currency,
				particulars, acutionmoney,v.vendor_id,vendor_name, restricted,v.address1,v.address2,v.country, v.state, v.city, v.other_state, v.other_city, v.fax, pin
				increase_lot_time_in, published, page_refresh_time,
				lot_opening_notification, bid_logic, maintain_start_bid_condition,
				show_l1_bid, no_of_position,allowed_max_extension, date_updated,show_min_decr,decimal_point,accept_same_bid,top_bidder,is_lot_group,one_by_one_lot,no_of_lot_for_one_by_one,all_lot_sync_flag,ignore_bidder_currency,stop_bidding_same_ip
				FROM ynk_auction a, vendor v where  v.vendor_id = a.vendor_id
				and MD5(ynk_auctionid)='".$auction_id."'";
	    return $this->db->query($sql)->row_array();
	}
	public function vendor_details($vendor_id)
	{
	    $sql  = "SELECT vendor_name,address1,address2,country, state, city, other_state, other_city, fax, pin from vendor where vendor_id=".$vendor_id;
	    $row= $this->db->query($sql)->row_array();
	    
	    $data=array();
	    $data['vendor_name']=$row['vendor_name'];
	    $data['address1']=$row['address1'];
	    $data['address2']=$row['address2'];
	    $data['country']=$row['country'];
	    $data['state']=$row['state'];
	    $data['city']=$row['city'];
	    $data['pin']=$row['pin'];
	    return $data;
	    
    }

    /**
	 * This function login will check whether auctioneer loggedin or not.
	 */
	public function isAuctioneerLogin()
	{
		return $this->session->userdata('vendor_id') == "" ? false : true;
    }

    public function check_permission($module_name = NULL)
	{
		$user_page_permission = $this->session->userdata('page_permission');
		if (is_array($module_name) && ! empty($module_name))
		{
			foreach ($module_name as $single_module)
			{
				if (in_array($single_module,$user_page_permission))
				{
					return TRUE;
				}
			}
		}
		else
		{
			if ( ! empty($module_name) && is_string($module_name) && ! empty($user_page_permission))
			{
				if (in_array($module_name,$user_page_permission))
				{
					return TRUE;
				}
			}
		}
    }
    
    
    /**
     * Add prod in favourite_product
     * 
     */
    public function add_products_in_favourite($post)
    {
        
        $this->db->where('bidder_id',$post['bidder_id']);
        $this->db->where('type',$post['type']);
        $product_id = $this->db->get('favourite_product')->row_array();
        if ( empty($product_id))
        {
            $product_id = $post['product_id'];
            $fav_arr = array(
                'bidder_id'=>$post['bidder_id'],
                'product_id'=>$post['product_id'],
                'type'=>$post['type']
            );
            if ( $this->db->insert('favourite_product',$fav_arr))
            {
                return array('status'=>'success','msg'=>'added successfully');
            }
            else
            {
                return array('status'=>'fail','msg'=>'error');
            }
            
        }
        else
        {
            
            $sql = "SELECT product_id FROM favourite_product WHERE bidder_id ='".$post['bidder_id']."'  AND 
            type ='".$post['type']."' AND find_in_set('".$post['product_id']."', product_id) <> 0";
            $product_list = $this->db->query($sql)->row_array();
            if ( empty($product_list))
            {
                $product_id = $product_id['product_id'].','.$post['product_id'];
                $fav_arr = array(
                    'product_id'=>$product_id,
                );
                $this->db->where('bidder_id',$post['bidder_id']);
                $this->db->where('type',$post['type']);
                if ( $this->db->update('favourite_product',$fav_arr))
                {
                    return array('status'=>'success','msg'=>'added successfully');
                }
                else
                {
                    return array('status'=>'fail','msg'=>'error');
                }
            }
            else
            {
                return array('status'=>'success','msg'=>'success');
            }

            
           
        }
 

    }
    /**
     * To get fav product
     */
    public function fav_product($product_id,$type)
    {
        
        $query = "SELECT * FROM favourite_product 
        WHERE find_in_set('".$product_id."', product_id) <> 0 AND bidder_id ='".$this->session->userdata('bidder_id')."'";
        $result = $this->db->query($query)->row_array();
        if ( ! empty($result))
        {
            return TRUE;
        }
        else
        {
            return FALSE;
        }
    }

    /**
     * Contact Vendor
     */
    public function send_message($vendor_id,$to_email,$bidder_id,$companme,$post)
    {
        $sub = 'Message From :'.$companme;
        $message = '<p>The following bidder has contacted you</p><br/>';
        $message.= '<table>
                        <tr>
                            <td>Name : </td>
                            <td>'.$post['name'].'</td>
                        </tr>
                        <tr>
                            <td>Email : </td>
                            <td>'.$post['email'].'</td>
                        </tr>
                        <tr>
                            <td>Phone : </td>
                            <td>'.$post['phone'].'</td>
                        </tr>
                        <tr>
                            <td>Message : </td>
                            <td>'.$post['message'].'</td>
                        </tr>
                    </table>';
        
        $send_mail = $this->common_model->send_common_email($vendor_id,$to_email, $sub, $message,$cc="", $bcc="sales@oya.auction" ,$bidder_id);
        if ($send_mail)
        {
            return array('status'=>'success','message'=>'Mail Sent Successfully');
        }
        else
        {
            return array('status'=>'fail','message'=>'Try Again After Some Time');
        }
    
    }
    /**
     * get fav product
     */
    public function get_fav_product($bidder_id,$type)
    {
        if ($type == 'P')
        {
            $query = "SELECT * FROM favourite_product WHERE bidder_id = '".$bidder_id."' AND type ='".$type."'";
        }
        else
        {
            $query = "SELECT * FROM favourite_product WHERE bidder_id = '".$bidder_id."' AND type !='P'";
        }
        
        return $this->db->query($query)->result_array();
    }
    /**
     * Get Profile complete percentage in buyer left navigation
     */
    public function profile_complete_percent($bidder_id='')
    {
       if(empty($bidder_id))
       {
            $bidder_id = $this->session->userdata('bidder_id');
       }
       $query = "SELECT compname,conperson,adddress,city,pin,state,country,mob,email,currency,type_of_comp,nature_of_activity,userid,password,
       user_tz,panno,interested_in_region,primary_email_verify FROM bidders WHERE id ='".$bidder_id."'";
       $result = $this->db->query($query)->row_array();
       
       if ( ! empty($result))
       {
          $total_count = 0;
          $complete_field = 0;
          foreach($result as $k=>$row)
          {
            if ($row)
            {
                $complete_field++;
            }
            $total_count++;
          }
       }
       
       $percentage = round(($complete_field/$total_count)*100);
       return $percentage;
    }

    public function check_auctioneer_permission($permission_name)
    {
        $this->db->select('permission_id');
        $this->db->where('permission_code', $permission_name);
        $permission_id = $this->db->get('module_sub_head_permission_sets')->row_array()['permission_id'];
        if ( ! empty($permission_id))
        {
            $this->db->select('vc.id, vc.vendor_id');
            $this->db->join('vendor_contacts vc', 'vc.vendor_id = avr.vendor_id', 'inner');
            $this->db->where('auctioneer_id', $this->session->userdata('vendor_id'));
            $this->db->where('permission_id', $permission_id);
            return $this->db->get('auctioneer_vendor_permission avr')->result_array();
        }
        else
        {
            return array();
        }
    }
}
