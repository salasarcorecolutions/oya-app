<?php

class User_model extends CI_Model
{
   
   public function __construct()
    {        
	  parent::__construct();
	    
    }
   
	 
    public function buyer_login()
    {
        $response=array();
        $rioesponse['status']=false;
         $username = makeSafe($this->input->post('username'));
         $password = makeSafe($this->input->post('password'));
        $this->db->where("userid",$username);
        $row = $this->db->get("bidders")->row_array();
            
        if (password_verify($password, $row['password']))
        {
            $sessionData = array(
                'compname' => $row['compname'],
                'code' => $row['initcode']."-".$row['id'],
                'bidder_id'=>$row['id'],
                'mobile'=>$row['mob'],
                'email'=>$row['email'],
                'bidder_code'=>$row['initcode'],
                'conperson'=>$row['conperson'],
                'activated'=>$row['activated'],
                'userid'=>$row['userid'],
                'user_tz' => $row['user_tz'],
                'blacklisted'=>$row['blacklisted'],
                'cause'=>$row['cause'],
                'cd'=>$row['initcode'],
                'currency'=>$row['currency'],
                'verify_email_popup_state'=> 'not shown',
                'user_tz' => $row['user_tz'],
                'primary_email_verify'=>$row['primary_email_verify'],
                'primary_mobile_verify'=>$row['primary_mobile_verify']
            );
            $this->session->set_userdata($sessionData);
            
            /* Remove Summary bidder */
            $this->db->where("client_id",$row['id']);
            $this->db->delete("summary_bidders_mark");
            
            /* Update Last Login */
            $last_login= array("last_login"=>date("Y-m-d H:i:s"));
            $this->db->where("id",$row['id']);
            $this->db->update("bidders",$last_login);
            
            /* Update Login Log */
            $rowLog   = "SELECT login  as lg,logintime,ip FROM userloginlog where userid=".$this->session->userdata('bidder_id')." order by id desc limit 1,1";
            
            $data=$this->db->query($rowLog);
            if ( $data->num_rows() > 0 )
            {
                $row1= $data->row();
                $sessionData1 = array(
                    'ip' => $row1->ip,
                    'last_login' =>@servertz_to_usertz($row1->lg . ' '.$row1->logintime , $row['user_tz'], 'd m y H:i:s') ,
                    'logintime' => $row1->logintime);
                $this->session->set_userdata($sessionData1);
            }
            else 
            {
                $sessionData1 = array(
                    'ip' => '',
                    'last_login' =>'First time login' ,
                    'logintime' => '');
                $this->session->set_userdata($sessionData1);
                
            }
            
            $response['status']=true;
			$response['bidder_id'] = $row['id'];
            if(!$row['primary_email_verify']) 
                $response['email_verified']="N";
            else 
				$response['email_verified']="Y";
            
        }
        else
        {
       
            $response['status']=false;
            $response['msg']="Invalid Credentials";
           
        }
        
        return $response;
    }  
    
	public function check_temp_buyer(){
        $primary_email  = makeSafe($this->input->post('email'));
        $compname  = makeSafe($this->input->post('company_name'));
        
        //check company or email 
        $query = "SELECT count(*) as cnt FROM temp_bidders WHERE REPLACE(compname, ' ', '') = '".$compname."' OR email ='".makeSafe($primary_email)."'";
        $res = $this->db->query($query)->row_array($query);

        if ($res['cnt'] > 1)
        {
            $data['msg'] = "Duplicate Registration";
            $data['success'] = false;
            return $data;
            exit;
        }

        //check user exists or not
        $data=$this->check_buyer_user(makeSafe($this->input->post('username')));
        if($data['status']=="success")
        {
            $data['msg'] = "Duplicate User Id";
            $data['success'] = false;
            return $data;
            exit;
        } else { 
            $data['msg'] = "No duplicate data";
            $data['success'] = true;
            return $data;
            exit;
        }
    }
	
	public function check_buyer_user($user_id)
    {
        if ( ! empty($user_id))
        {
           
            $chk_user = "SELECT COUNT(*) AS cnt FROM bidders WHERE userid= '".makeSafe($user_id)."'";
            $con_user_check = $this->db->query($chk_user)->row_array();
            if ($con_user_check['cnt'] ==0)
            {
                $chk_user = "SELECT COUNT(*) AS cnt FROM temp_bidders WHERE userid= '".makeSafe($user_id)."'";
                $con_user_check = $this->db->query($chk_user)->row_array();
            }
            if ($con_user_check['cnt']>=1)
            {
                //status: used in reg temp check
                return array('status'=>'success','message'=>'User ID Alread Exist');
            }
            else
            {
                return array('status'=>'fail');
            }
        }
        else
        {
            return array('status'=>'fail');
        } 
    }
	
	
	//save the temp buyer
	public function save_temp_buyer($email, $subject, $message ,$otp)
    { 

        //--- saving the temp data after Accepting terms and conditions  ---// 
        
		$compname= makeSafe($this->input->post('company_name'));
		$pass=makeSafe($this->input->post('pass')); 
		
		$insert_bidder_data = array(
            'compname'=>ucwords($compname),
            'conperson'=> makeSafe($this->input->post('first_name')),
            'adddress'=> makeSafe($this->input->post('company_address')),
            'city'=> makeSafe($this->input->post('city_id')),
            'pin'=> makeSafe($this->input->post('zip')),
            'state'=> makeSafe($this->input->post('state_id')), 
            'country'=> makeSafe($this->input->post('country_id')),
            'state_other'=> makeSafe($this->input->post('b_state_other')),
            'city_other'=> makeSafe($this->input->post('b_city_other')),
            'mob'=> makeSafe($this->input->post('phone')),
            'email'=> makeSafe($this->input->post('email')),
            'userid'=> makeSafe($this->input->post('username')),
            'password'=>password_hash($pass, PASSWORD_DEFAULT),
            'regdate'=>date("Y-m-d H:i:s"),	
            'user_tz'=>trim($this->input->post('zone_id')!="" ? $this->input->post('zone_id') : 'UTC'),	
        );

        if ($this->db->insert('temp_bidders',$insert_bidder_data))
        {
			$send_email= $this->common_model->send_common_email($email, $subject, $message); 
			if($send_email){
				//echo "mail send";
				$data['msg'] = "mail send";
				$data['success'] = true; 
				$data['temp_id'] = $this->db->insert_id(); 
				$data['otp']= $otp;
				return $data;				
			}
			else
			{
				$data['msg'] = "mail not send";
				$data['success'] = false;  
				return $data;	
			} 
            
        }
        else
        {
            $data['msg'] = "Please Try Again After Some Time";
            $data['success'] = false;
            return $data; 
        }  
    }
	
	 
	public function save_buyer(){
		$temp_id=makeSafe($this->input->post('temp_id')); 
		$update_otp = array('primary_email_verify'=>'1');
		$this->db->where('id',$temp_id);
		$this->db->update('temp_bidders',$update_otp);
		$sql = "SELECT compname,conperson,tb.adddress,city,tb.state,country,mob,email,userid,tb.password,initcode,user_tz,pin,regdate FROM temp_bidders tb WHERE id =".$temp_id;
		$res = $this->db->query($sql)->row_array();
		if ( ! empty($res))
		{
			$query = "SELECT count(*) as cnt FROM bidders WHERE compname = '".$res['compname']."' and email ='".$res['email']."'";
			$cnt = $this->db->query($query)->row_array();
			if ( $cnt['cnt'] == 0)
			{
				unset($res['id']);
				$res['primary_email_verify'] = 1;
				$res['activated'] = 1;
				if ($this->db->insert('bidders',$res))
				{
                    $this->db->where('id',$temp_id);
                    $this->db->delete('temp_bidders');
                    if($this->db->affected_rows() == true)
                    {
                        $data['msg'] = "Registred successfully";
						$data['success'] = true; 
                    }
                    else {
                        $data['msg'] = "Error while deleting temp_bidder data";
						$data['success'] = false;
						
                    }
				}
				else
				{
					$data['msg'] = "Error while Register";
					$data['success'] = false; 
				}
				return $data; 
			}
			else
			{
				$data['msg'] = "Please Try Again After Some Time";
				$data['success'] = false;
				return $data; 
			}
			
		} 
	}
   
}
