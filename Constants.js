const api_url = function() {
    let api_url = '';
    if (process.env["APP_ENV"] == 'production') {
       api_url = "https://production.dancingnomads.com";
    } else if (process.env["APP_ENV"] == 'staging') {
       api_url = "https://staging.dancingnomads.com"
    } else {
       api_url = 'http://192.168.1.205:19000' // Need to use ifconfig to find the private IP of your computer. localhost doesn't work because the app is running an emulator.
    }
    return api_url
   }
   export const url = api_url();
