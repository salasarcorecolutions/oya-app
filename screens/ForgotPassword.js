import RadioButton from '../components/RadioButton';

import React from 'react';
import { StyleSheet,
  Text,
  View,
  TextInput,
  Button,
  TouchableOpacity,
  Image,
  } from 'react-native';

  
  const ForgotPassword = () =>{
  
    return (
      <View style={styles.container}>
        <Image style={styles.logo} source={require("../assets/logo.png")} />
          <View style={styles.inputContainer}>
            <Image
              style={styles.inputIcon}
              source={{
                uri: "https://png.icons8.com/message/ultraviolet/50/3498db",
              }}
            />
            <TextInput
              style={styles.inputs}
              placeholder="User Name"
              underlineColorAndroid="transparent"
              onChangeText={(UserEmail) => setUserEmail(UserEmail)}
              placeholder="Enter Email" //dummy@abc.com
              placeholderTextColor="#8b9cb5"
              autoCapitalize="none"
              keyboardType="email-address"
              returnKeyType="next"
              underlineColorAndroid="#f000"
              blurOnSubmit={false}
            />
          </View>
          <RadioButton />
          <TouchableOpacity
              style={styles.loginBtn}
              activeOpacity={0.5}
              >
              <Text style={styles.loginText}>
                Email Password Reset Link
              </Text>
            </TouchableOpacity>
        </View>

    );
  }
  const styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: '#003f5c',
      alignItems: 'center',
      justifyContent: 'center',
    },
    inputContainer: {
      width:"80%",
      backgroundColor:"#465881",
      borderRadius:25,
      height:50,
      marginBottom:20,
      justifyContent:"center",
      padding:20
    },
    loginBtn:{
      width:"80%",
      backgroundColor:"#fb5b5a",
      borderRadius:25,
      height:50,
      alignItems:"center",
      justifyContent:"center",
      marginTop:40,
      marginBottom:10
    },
    
    inputs: {
      height:50,
      color:"white"
    },
    inputIcon: {
      width: 30,
      height: 30,
      marginLeft: 15,
      justifyContent: "center",
    },
   
    
    loginText: {
      color: "white",
    },
    logo: {
      height: 100,
      width: 100,
      borderRadius: 50,
      marginBottom: 30,
    },
 
  });
  
  export default ForgotPassword;