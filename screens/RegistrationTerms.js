import * as React from 'react'
import { View, StyleSheet, Alert } from 'react-native'
import PDFReader from 'rn-pdf-reader-js';
import Colors from '../constants/Colors';
import Urls from '../constants/Urls';

import TouchableButton from '../components/TouchableButton';

export default class RegistrationTerms extends React.Component {

    acceptTerms(){
        var dataToSend = this.props.navigation.state.params.data;
        let formBody = [];
        for (let key in dataToSend) {
            let encodedKey = encodeURIComponent(key);
            let encodedValue = encodeURIComponent(dataToSend[key]);
            formBody.push(encodedKey + '=' + encodedValue);
        }
        formBody = formBody.join('&');
        fetch(Urls.api_url + 'user/temp_vendor_registration/', {
            method: 'POST',
            body: formBody,
            headers: {
                //Header Defination
                'Content-Type':
                'application/x-www-form-urlencoded;charset=UTF-8',
            },
        })
        .then((response) => response.json())
        .then((responseJson) => {
            console.log(responseJson)
            if (responseJson.status == "success"){
                this.props.navigation.navigate('VerifyEmail',{
                    names: 'Verify Email',
                    data: dataToSend
                });
            } else {
                Alert.alert('Falied', 'Problem in registration. Please try again.', [{text: 'Okay'}]);
            }
        })
        .catch((error) => {
            //Hide Loader
            console.error(error);
        });
    }

    render() {
        return (
            <View style={{ flex: 1 }}>
                <View style={ styles.pdfScreen }>
                    <PDFReader
                        source={{
                            uri: 'http://oya.auction/assets/terms/register_terms.pdf',
                        }}
                    />
                </View>
                <View style={ styles.checkScreen }>
                    <TouchableButton style={ styles.accept } onPress={this.acceptTerms.bind(this)}>Accept</TouchableButton>
                    <TouchableButton style={ styles.reject } onPress={
                        () => {this.props.navigation.navigate('Login')}
                    }>Reject</TouchableButton>
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    pdfScreen: {
        flex: 9
    },
    checkScreen: {
        flex: 1,
        flexDirection: "row",
        alignItems: "center",
        justifyContent: "space-evenly"
    },
    doc: {
        height: "100%"
    },
    accept: {
        backgroundColor: Colors.primary,
        flex: 1,
        height: '100%',
        alignItems: 'center',
        justifyContent: "center"
    },
    reject: {
        backgroundColor: Colors.accent,
        flex: 1,
        height: '100%',
        alignItems: 'center',
        justifyContent: "center"
    }
});