import React from 'react'
import { Text, View } from 'react-native';
import Urls from '../constants/Urls';

import TouchableButton from '../components/TouchableButton';

export default class VerifyEmail extends React.Component {
    state = {
        timer: 10,
        email: 'vaibhavs.aonesalasar@gmail.com',
        showResend: true,
        startTimer: false,
        otp: 0,
        expiry: 0,
        otpCount: 0
    };

    componentDidUpdate(){
        if (this.state.timer === 0){
            clearInterval(this.interval);
        } else if (this.state.timer === 1){
            this.setState({ showResend: true, startTimer: false, timer: 0 });
        }
    }

    componentWillUnmount(){
        clearInterval(this.interval);
    }

    resendOtp(){
        console.log(this.props)
        this.setState({
            showResend: false,
            startTimer: true,
            timer: 15
        });
        this.setState((prevState)=> ({ otpCount: prevState.timer + 1 }))
        this.interval = setInterval(() =>
            this.setState((prevState)=> ({ timer: prevState.timer - 1 }))
        ,1000);
        let otpData = {
            email: this.state.email,
            otp: this.state.otp
        };
        let formBody = [];
        for (let key in otpData) {
            let encodedKey = encodeURIComponent(key);
            let encodedValue = encodeURIComponent(dataToSend[key]);
            formBody.push(encodedKey + '=' + encodedValue);
        }
        formBody = formBody.join('&');
        fetch(Urls.api_url + 'user/verify_email_otp/', {
            method: 'POST',
            body: formBody,
            headers: {
                //Header Defination
                'Content-Type':
                'application/x-www-form-urlencoded;charset=UTF-8',
            },
        })
        .then((response) => response.json())
        .then((responseJson) => {
            for (const key in responseJson){
                this.setState({
                    otp: responseJson[key].otp,
                    expiry: responseJson[key].expiry,
                    timer: 10
                });
            }
        })
        .catch((error) => {
            //Hide Loader
            console.error(error);
        });

    }

    timer(){
        if (this.state.startTimer){
            return(
                <Text>Resend OTP In : {this.state.timer}</Text>
            );
        }
        return null;
    }

    render() {
        return (
            <View style={{ marginTop: '50%'}}>
                <Text>Email Address : {this.state.email}</Text>
                {this.timer()}
                {this.state.showResend === true && this.state.otpCount <=5 ? <TouchableButton onPress={this.resendOtp.bind(this)}>Send OTP</TouchableButton> : null }
            </View>
        );
    }
}
