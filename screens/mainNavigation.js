import { createStackNavigator } from 'react-navigation'
import StartScreen from '../screens/LoginView';
import RegistrationTerms from '../screens/RegistrationTerms';

const AppNavigator = createStackNavigator({
  Start: {
    screen: StartScreen
  }
}, {
  initialRouteName: 'Start',
  navigationOptions: {
    header: null
  }
})

export default AppNavigator