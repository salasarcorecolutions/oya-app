
import React, { useEffect, useState } from 'react';
import {
    StyleSheet,
    View,
    TouchableOpacity,
    Alert,
    Image,
    ScrollView,
    Text
} from 'react-native';
import { Picker } from '@react-native-picker/picker';
import { AppLoading } from 'expo';
import { Checkbox, RadioButton } from 'react-native-paper';
import {LinearGradient} from 'expo-linear-gradient';


import customStyles from '../assets/css/customStyles';
import Colors from '../constants/Colors';
import Urls from '../constants/Urls';
import Input from '../components/Input';
import RegistrationTerms from './RegistrationTerms';

const RegisterView = ({navigation}) => {
    const [timeZone, setTimeZone] = useState("");
    const [company, setCompany] = useState("");
    const [companyIsValid, setCompanyIsValid] = useState(true);
    const [companyadd, setCompanyadd] = useState("");
    const [companyaddIsValid, setcompanyaddIsValid] = useState(true);
    const [companypin, setCompanypin] = useState("");
    const [companypinIsValid, setcompanypinIsValid] = useState(true);
    const [branch, setBranch] = useState("");
    const [branchIsValid, setbranchIsValid] = useState(true);
    const [branchadd, setBranchadd] = useState("");
    const [branchaddIsValid, setbranchaddIsValid] = useState(true);
    const [branchpin, setBranchpin] = useState("");
    const [branchpinIsValid, setbranchpinIsValid] = useState(true);
    const [name, setName] = useState("");
    const [nameIsValid, setnameIsValid] = useState(true);
    const [username, setUsername] = useState("");
    const [usernameIsValid, setusernameIsValid] = useState(true);
    const [password, setPassword] = useState("");
    const [passwordIsValid, setpasswordIsValid] = useState(true);
    const [confpassword, setConfpassword] = useState("");
    const [confpasswordIsValid, setconfpasswordIsValid] = useState(true);
    const [email, setEmail] = useState("");
    const [emailIsValid, setemailIsValid] = useState(true);
    const [mobile, setMobile] = useState("");
    const [mobileIsValid, setmobileIsValid] = useState(true);
    const [state, setState] = useState("");
    const [stateIsValid, setstateIsValid] = useState(true);
    const [bstate, setbState] = useState("");
    const [bstateIsValid, setbstateIsValid] = useState(true);
    const [stateCity, setStateCity] = useState("");
    const [stateCityIsValid, setstateCityIsValid] = useState(true);
    const [bstateCity, setBStateCity] = useState("");
    const [bstateCityIsValid, setbstateCityIsValid] = useState(true);
    const [countryState, setCountryState] = useState("");
    const [countryStateIsValid, setcountryStateIsValid] = useState(true);
    const [bcountryState, setbCountryState] = useState("");
    const [bcountryStateIsValid, setbcountryStateIsValid] = useState(true);
    const [countryList, setCountryList] = useState([]);
    const [cstate, setCstate] = useState([]);
    const [bcstate, setbCstate] = useState([]);
    const [cityList, setCityList] = useState([]);
    const [bcityList, setbCityList] = useState([]);
    const [show, setShow] = useState(false);
    const [scrapcheck, setScrapcheck] = useState(false);
    const [logisticheck, setLogisticheck] = useState(false);
    const [tendercheck, setTendercheck] = useState(false);
    const [procurementcheck, setProcurementcheck] = useState(false);
    const [forwardcheck, setForwardcheck] = useState(false);
    const [reversecheck, setReversecheck] = useState(false);
    const [dutchcheck, setDutchcheck] = useState(false);
    const [vendor, setVendor] = useState('');
    const [vendorIsValid, setvendorIsValid] = useState(true);

    const companyChangeHandler = text => {
        if (text.trim().length === 0){
            setCompanyIsValid(false);
        } else {
            setCompanyIsValid(true);
        }
        setCompany(text);
    };

    const companyAddChangeHandler = text => {
        if (text.trim().length === 0){
            setcompanyaddIsValid(false);
        } else {
            setcompanyaddIsValid(true);
        }
        setCompanyadd(text);
    };

    const comCountryChangeHandler = (itemValue, itemIndex) => {
        if (itemValue === ""){
            setstateIsValid(false);
        } else {
            setstateIsValid(true);
        }
        setState(itemValue);
    };

    const comStateChangeHandler = (itemValue, itemIndex) => {
        if (itemValue === ""){
            setcountryStateIsValid(false);
        } else {
            setcountryStateIsValid(true);
        }
        setCountryState(itemValue);
    };

    const comCityChangeHandler = (itemValue, itemIndex) => {
        if (itemValue === ""){
            setstateCityIsValid(false);
        } else {
            setstateCityIsValid(true);
        }
        setStateCity(itemValue);
    };

    const comPinChangeHandler = text => {
        if (text.trim().length === 0){
            setcompanypinIsValid(false);
        } else {
            setcompanypinIsValid(true);
        }
        setCompanypin(text);
    };

    const branchChangeHandler = text => {
        if (text.trim().length === 0){
            setbranchIsValid(false);
        } else {
            setbranchIsValid(true);
        }
        setBranch(text);
    };

    const bAddrChangeHandler = text => {
        if (text.trim().length === 0){
            setbranchaddIsValid(false);
        } else {
            setbranchaddIsValid(true);
        }
        setBranchadd(text);
    };

    const bCountryChangeHandler = (itemValue, itemIndex) => {
        if (itemValue === ""){
            setbstateIsValid(false);
        } else {
            setbstateIsValid(true);
        }
        setbState(itemValue);
    };

    const bStateChangeHandler = (itemValue, itemIndex) => {
        if (itemValue === ""){
            setbcountryStateIsValid(false);
        } else {
            setbcountryStateIsValid(true);
        }
        setbCountryState(itemValue);
    };

    const bCityChangeHandler = (itemValue, itemIndex) => {
        if (itemValue === ""){
            setbstateCityIsValid(false);
        } else {
            setbstateCityIsValid(true);
        }
        setBStateCity(itemValue);
    };

    const bPinChangeHandler = text => {
        if (text.trim().length === 0){
            setbranchpinIsValid(false);
        } else {
            setbranchpinIsValid(true);
        }
        setBranchpin(text);
    };

    const nameChangeHandler = text => {
        console.log(text)
        if (text.trim().length === 0){
            setnameIsValid(false);
        } else {
            setnameIsValid(true);
        }
        setName(text);
    };

    const usernameChangeHandler = text => {
        console.log(text)
        if (text.trim().length === 0){
            setusernameIsValid(false);
        } else {
            setusernameIsValid(true);
        }
        setUsername(text);
    };

    const passwordChangeHandler = text => {
        if (text.trim().length === 0){
            setpasswordIsValid(false);
        } else {
            setpasswordIsValid(true);
        }
        setPassword(text);
    };

    const confPasswordChangeHandler = text => {
        if (text.trim().length === 0){
            setconfpasswordIsValid(false);
        } else {
            setconfpasswordIsValid(true);
        }
        setConfpassword(text);
    };

    const emailChangeHandler = text => {
        if (text.trim().length === 0){
            setemailIsValid(false);
        } else {
            setemailIsValid(true);
        }
        setEmail(text);
    };

    const mobileChangeHandler = text => {
        if (text.trim().length === 0){
            setmobileIsValid(false);
        } else {
            setmobileIsValid(true);
        }
        setMobile(text);
    };

    const vendorChangeHandler = text => {
        if (text.trim().length === 0){
            setvendorIsValid(false);
        } else {
            setvendorIsValid(true);
        }
        setVendor(text);
    };

    const handleSubmitPress = () => {
        if (company.trim().length === 0 ||
            companyadd.trim().length === 0 ||
            companypin.trim().length === 0 ||
            branch.trim().length === 0 ||
            branchadd.trim().length === 0 ||
            branchpin.trim().length === 0 ||
            name.trim().length === 0 ||
            username.trim().length === 0 ||
            password.trim().length === 0 ||
            confpassword.trim().length === 0 ||
            email.trim().length === 0 ||
            mobile.trim().length === 0 ||
            state.trim().length === 0 ||
            bstate.trim().length === 0 ||
            stateCity.trim().length === 0 ||
            bstateCity.trim().length === 0 ||
            countryState.trim().length === 0 ||
            bcountryState.trim().length === 0 ||
            vendor.trim().length === 0 )
        {
            if (company.trim().length === 0){
                setCompanyIsValid(false);
            }
            if (companyadd.trim().length === 0) {
                setcompanyaddIsValid(false);
            }
            if (companypin.trim().length === 0) {
                setcompanypinIsValid(false);
            }
            if (branch.trim().length === 0) {
                setbranchIsValid(false);
            }
            if (branchadd.trim().length === 0) {
                setbranchaddIsValid(false);
            }
            if (branchpin.trim().length === 0) {
                setbranchpinIsValid(false);
            }
            if (name.trim().length === 0) {
                setnameIsValid(false);
            }
            if (username.trim().length === 0) {
                setusernameIsValid(false);
            }
            if (password.trim().length === 0) {
                setpasswordIsValid(false);
            }
            if (confpassword.trim().length === 0) {
                setconfpasswordIsValid(false);
            }
            if (email.trim().length === 0) {
                setemailIsValid(false);
            }
            if (mobile.trim().length === 0) {
                setmobileIsValid(false);
            }
            if (state.trim().length === 0) {
                setstateIsValid(false);
            }
            if (bstate.trim().length === 0) {
                setbstateIsValid(false);
            }
            if (stateCity.trim().length === 0 ) {
                setstateCityIsValid(false);
            }
            if (bstateCity.trim().length === 0 ) {
                setbstateCityIsValid(false);
            }
            if (countryState.trim().length === 0) {
                setcountryStateIsValid(false);
            }
            if (bcountryState.trim().length === 0) {
                setbcountryStateIsValid(false);
            }
            if (vendor.trim().length === 0) {
                setvendorIsValid(false);
            }
            Alert.alert('Invalid Input', 'Please Check the errors in the form.', [{text: 'Okay'}]);
            return;
        }

        let dataToSend = {
            company_name: company,
            company_address: companyadd,
            company_country: state,
            company_state: countryState,
            company_city: stateCity,
            company_pin: companypin,
            branch_name: branch,
            branch_address: branchadd,
            branch_country: bstate,
            branch_state: bcountryState,
            branch_city: bstateCity,
            branch_pin: branchpin,
            name: name,
            username: username,
            password: password,
            conf_password: confpassword,
            email: email,
            mobile: mobile,
            scrap: scrapcheck,
            logistic: logisticheck,
            tender: tendercheck,
            procurement: procurementcheck,
            forward: forwardcheck,
            reverse: reversecheck,
            dutch: dutchcheck,
            reg_type: vendor,
            time_zone: timeZone
        };
        navigation.navigate('RegistrationTerms',{
            names: 'Registration Terms',
            data: dataToSend
        })
    };

    useEffect(() => {
        setShow(false);
        fetch(Urls.api_url + 'common/get_city/' + countryState)
            .then((response) => response.json())
            .then((responseJson) => {
                var citys = [];
                console.log(responseJson)
                for (const key in responseJson) {
                    citys.push({ id: responseJson[key].id, name: responseJson[key].name });
                }
                setCityList(citys);
                console.log(countryState)
                setShow(true);
            })
            .catch(error => {
                console.error(error);
            });
    }, [countryState])

    useEffect(() => {
        setShow(false);
        fetch(Urls.api_url + 'common/get_city/' + bcountryState)
            .then((response) => response.json())
            .then((responseJson) => {
                var citys = [];
                for (const key in responseJson) {
                    citys.push({ id: responseJson[key].id, name: responseJson[key].name });
                }
                setbCityList(citys);
                setShow(true);
            })
            .catch(error => {
                console.error(error);
            });
    }, [bcountryState])

    useEffect(() => {
        setShow(false);
        fetch(Urls.api_url + 'common/get_state/' + state)
            .then((response) => response.json())
            .then((responseJson) => {
                var counstate = [];
                for (const key in responseJson) {
                    counstate.push({ id: responseJson[key].id, name: responseJson[key].state });
                }
                setCstate(counstate);
                if (state){
                fetch(Urls.api_url + 'common/time_zone/' + state)
                    .then((response) => response.json())
                    .then((responseJson) => {
                        setTimeZone(responseJson[0].timezone);
                        setShow(true);
                    })
                    .catch(error => {
                        console.error(error);
                    });
                } else {
                    setShow(true);
                }
            })
            .catch(error => {
                console.error(error);
            });
    }, [state])

    useEffect(() => {
        setShow(false);
        fetch(Urls.api_url + 'common/get_state/' + bstate)
            .then((response) => response.json())
            .then((responseJson) => {
                var counstate = [];
                for (const key in responseJson) {
                    counstate.push({ id: responseJson[key].id, name: responseJson[key].state });
                }
                setbCstate(counstate);
                setShow(true);
            })
            .catch(error => {
                console.error(error);
            });
    }, [bstate])

    useEffect(() => {
        setShow(false);
        fetch(Urls.api_url + 'common/get_country')
            .then((response) => response.json())
            .then((responseJson) => {
                var country = [];
                for (const key in responseJson) {
                    country.push({ id: responseJson[key].id, name: responseJson[key].name });
                }
                setCountryList(country);
                setShow(true);
            })
            .catch(error => {
                console.error(error);
            });
    }, [])
    if (!show) {
        return <AppLoading />;
    }

    return (
        <View style={customStyles.container}>
                <View style={customStyles.header}>
                    <LinearGradient colors={['#F24D88', '#FC4D5B']}>
                        <View style={customStyles.headerContent}>
                            <Image style={customStyles.logo} source={require("../assets/logo.png")} />
                            <Text style={customStyles.headerTitle}> OYA AUCTION</Text>
                        </View>
                    </LinearGradient>
                </View>

                <View style={customStyles.body}>
                    <ScrollView>
                        <View style={customStyles.center}>
                            <Text style={[customStyles.titleText, customStyles.mb50 ,customStyles.mt25]}> Auctioneer Registration</Text>
                        </View>

                            <Input
                                id="company_name"
                                label="Company Name"
                                keyboardType="default"
                                required
                                autoCapitalize="none"
                                onChangeText={companyChangeHandler}
                                value={company}
                            />
                            {!companyIsValid && <Text style={styles.error}>Please Enter Valid Company Name</Text>}
                        <Input
                            id="company Address"
                            label="Address"
                            keyboardType="default"
                            required
                            autoCapitalize="none"
                            value={companyadd}
                            onChangeText={companyAddChangeHandler}
                        />
                            {!companyaddIsValid && <Text style={styles.error}>Please Enter Valid Company Address</Text>}
                    
                            <View style={customStyles.dropBox}>
                            <Text style={customStyles.label}>Country</Text>
                                <Picker
                                    selectedValue={state}
                                    onValueChange={comCountryChangeHandler}
                                    style={customStyles.dropdown}
                                    mode="dropdown">
                                    <Picker.Item key="" value="" label="Select Country" />
                                    {
                                        countryList.map(country =>
                                            <Picker.Item key={country.id} value={country.id} label={country.name} />)
                                    }
                                </Picker>
                            </View>

                            {!stateIsValid && <Text style={styles.error}>Please Select Country</Text>}
                            <View style={customStyles.dropBox}>
                              <Text style={customStyles.label}>State</Text>

                                <Picker
                                    selectedValue={countryState}
                                    onValueChange={comStateChangeHandler}
                                    style={customStyles.dropdown}
                                    mode="dropdown">
                                    <Picker.Item key="" value="" label="Select State" />
                                    {
                                        cstate.map(s =>
                                            <Picker.Item key={s.id} value={s.id} label={s.name} />)
                                    }
                                </Picker>
                            </View>
                            {!countryStateIsValid && <Text style={styles.error}>Please Select State</Text>}
                            <View style={customStyles.dropBox}>
                              <Text style={customStyles.label}>City</Text>
                                <Picker
                                    selectedValue={stateCity}
                                    onValueChange={comCityChangeHandler}
                                    style={customStyles.dropdown}
                                    mode="dropdown">
                                    <Picker.Item key="" value="" label="Select City" />
                                    {
                                        cityList.map(c =>
                                            <Picker.Item key={c.id} value={c.id} label={c.name} />)
                                    }
                                </Picker>
                            </View>
                            {!stateCityIsValid && <Text style={styles.error}>Please Select City</Text>}

                        
                        <Input
                            id="pin_code"
                            label="Pin Code"
                            keyboardType="number-pad"
                            required
                            minLength={5}
                            autoCapitalize="none"
                            value={companypin}
                            onChangeText={comPinChangeHandler}
                        />
                            {!companypinIsValid && <Text style={styles.error}>Please Enter Valid Pin Code</Text>}
                        <Text style={customStyles.label}>Time Zone</Text><Text>{timeZone}</Text>
                        <Input
                            id="branch_name"
                            label="Branch Name"
                            keyboardType="default"
                            required
                            autoCapitalize="none"
                            value={branch}
                            onChangeText={branchChangeHandler}
                        />
                            {!branchIsValid && <Text style={styles.error}>Please Enter Valid Branch Name</Text>}
                        <Input
                            id="branch_address"
                            label="Branch Address"
                            keyboardType="default"
                            required
                            autoCapitalize="none"
                            value={branchadd}
                            onChangeText={bAddrChangeHandler}
                        />
                            {!branchaddIsValid && <Text style={styles.error}>Please Enter Valid Branch Address</Text>}
                            <View style={customStyles.dropBox}>
                              <Text style={customStyles.label}>Branch Country</Text>
                                    <Picker
                                        selectedValue={bstate}
                                        onValueChange={bCountryChangeHandler}
                                        style={customStyles.dropdown}
                                        mode="dropdown">
                                        <Picker.Item key="" value="" label="Select Country" />
                                        {
                                            countryList.map(country =>
                                                <Picker.Item key={country.id} value={country.id} label={country.name} />)
                                        }
                                    </Picker>
                            </View>
                        {!bstateIsValid && <Text style={styles.error}>Please Select Country</Text>}
                        <View style={customStyles.dropBox}>
                              <Text style={customStyles.label}>Branch State</Text>
                                <Picker
                                    selectedValue={bcountryState}
                                    onValueChange={bStateChangeHandler}
                                    style={customStyles.dropdown}
                                    mode="dropdown">
                                    <Picker.Item key="" value="" label="Select State" />
                                    {
                                        bcstate.map(s =>
                                            <Picker.Item key={s.id} value={s.id} label={s.name} />)
                                    }
                                </Picker>
                        </View>        
                        {!bcountryStateIsValid && <Text style={styles.error}>Please Select State</Text>}
                        <View style={customStyles.dropBox}>
                              <Text style={customStyles.label}>Branch City</Text>
                                <Picker
                                    selectedValue={bstateCity}
                                    onValueChange={bCityChangeHandler}
                                    style={customStyles.dropdown}
                                    mode="dropdown">
                                    <Picker.Item key="" value="" label="Select City" />
                                    {
                                        bcityList.map(c =>
                                            <Picker.Item key={c.id} value={c.id} label={c.name} />)
                                    }
                                </Picker>
                        </View>        
                        {!bstateCityIsValid && <Text style={styles.error}>Please Select City</Text>}
                        <View>
                        </View>
                        <Input
                            id="b_pin_code"
                            label="Pin Code"
                            keyboardType="number-pad"
                            required
                            autoCapitalize="none"
                            value={branchpin}
                            onChangeText={bPinChangeHandler}
                        />
                            {!branchpinIsValid && <Text style={styles.error}>Please Enter Valid Pin Code</Text>}
                        <Input
                            id="name"
                            label="Name"
                            keyboardType="default"
                            required
                            autoCapitalize="none"
                            value={name}
                            onChangeText={nameChangeHandler}
                        />
                            {!nameIsValid && <Text style={styles.error}>Please Enter Valid Name</Text>}
                        <Input
                            id="username"
                            label="User Name"
                            keyboardType="default"
                            required
                            autoCapitalize="none"
                            value={username}
                            onChangeText={usernameChangeHandler}
                        />
                            {!usernameIsValid && <Text style={styles.error}>Please Enter Valid Username</Text>}
                        <Input
                            id="password"
                            label="Password"
                            keyboardType="default"
                            secureTextEntry
                            required
                            autoCapitalize="none"
                            placeholder="⚪ ⚪ ⚪ ⚪ ⚪ ⚪"
                            value={password}
                            onChangeText={passwordChangeHandler}
                        />
                            {!passwordIsValid && <Text style={styles.error}>Please Enter Valid Password</Text>}
                        <Input
                            id="confirm_password"
                            label="Confirm Password"
                            keyboardType="default"
                            secureTextEntry
                            required
                            autoCapitalize="none"
                            placeholder="⚪ ⚪ ⚪ ⚪ ⚪ ⚪"
                            value={confpassword}
                            onChangeText={confPasswordChangeHandler}
                        />
                            {!confpasswordIsValid && <Text style={styles.error}>Password and confirm password must be same.</Text>}
                        <Input
                            id="email"
                            label="Email"
                            keyboardType="default"
                            required
                            email
                            autoCapitalize="none"
                            value={email}
                            onChangeText={emailChangeHandler}
                        />
                            {!emailIsValid && <Text style={styles.error}>Please Enter Valid Email Id</Text>}
                        <Input
                            id="mobile"
                            label="Mobile"
                            keyboardType="number-pad"
                            required
                            autoCapitalize="none"
                            value={mobile}
                            onChangeText={mobileChangeHandler}
                        />
                            {!mobileIsValid && <Text style={styles.error}>Please Enter Valid Contact No.</Text>}
                            
                            
                        <Text style={[customStyles.subTitle, customStyles.mb10 ]}> Intrested in</Text>

                        <View style={[customStyles.row, customStyles.mb10 ]}> 

                            <View style={customStyles.col2}>
                                <Checkbox
                                    title='Forward Auction'
                                    status={forwardcheck ? 'checked' : 'unchecked'}
                                    onPress={() => { setForwardcheck(!forwardcheck); }}
                                /><Text style={customStyles.label}>Forward Auction</Text>
                            </View>

                            <View style={customStyles.col2}>
                                <Checkbox
                                    title='Reverse Auction'
                                    status={reversecheck ? 'checked' : 'unchecked'}
                                    onPress={() => { setReversecheck(!reversecheck); }}
                                /><Text style={customStyles.label}>Reverse Auction</Text>
                            </View>
                            

                            <View style={customStyles.col2}>
                                <Checkbox
                                    title='E - Tender'
                                    status={tendercheck ? 'checked' : 'unchecked'}
                                    onPress={() => { setTendercheck(!tendercheck); }}
                                /><Text style={customStyles.label}>E - Tender</Text>
                            </View>

                            <View style={customStyles.col2}>
                                <Checkbox
                                    title='Dutch Auction'
                                    status={dutchcheck ? 'checked' : 'unchecked'}
                                    onPress={() => { setDutchcheck(!dutchcheck); }}
                                /><Text style={customStyles.label}>Dutch Auction</Text>
                            </View>

                            <View style={customStyles.col2}>
                                <Checkbox
                                    title='Procurement'
                                    status={procurementcheck ? 'checked' : 'unchecked'}
                                    onPress={() => { setProcurementcheck(!procurementcheck); }}
                                /><Text style={customStyles.label}>Procurement</Text>
                            </View>

                            <View style={customStyles.col2}>
                                <Checkbox
                                    title='Logistics'
                                    status={logisticheck ? 'checked' : 'unchecked'}
                                    onPress={() => { setLogisticheck(!logisticheck); }}
                                /><Text style={customStyles.label}>Logistics</Text>
                            </View>
                        
                            <View style={customStyles.col2}>
                                <Checkbox
                                    title='Scrap Management'
                                    status={scrapcheck ? 'checked' : 'unchecked'}
                                    onPress={() => { setScrapcheck(!scrapcheck); }}
                                /><Text style={customStyles.label}>Scrap Management</Text>
                            </View>
                      
                        </View>
                        <Text style={[customStyles.subTitle, customStyles.mb10 ]}> Register for</Text>

                        <View style={customStyles.row}>    
                        
                            <View style={customStyles.col2}>
                                <RadioButton
                                    value="V"
                                    label="Vendor"
                                    status={vendor === 'V' ? 'checked' : 'unchecked'}
                                    onPress={() => vendorChangeHandler('V')}
                                /><Text style={customStyles.label}>Vendor</Text>
                            </View>

                            <View style={customStyles.col2}>
                                <RadioButton
                                    value="A"
                                    label="Auctioneer"
                                    status={vendor === 'A' ? 'checked' : 'unchecked'}
                                    onPress={() => vendorChangeHandler('A')}
                                /><Text style={customStyles.label}>Auctioneer</Text>
                            </View>
                            
                        </View>
                            {!vendorIsValid && <Text style={styles.error}>Please Select Vendor/Auctioneer</Text>}
                        <View style={customStyles.center}>
                            <TouchableOpacity
                                style={[customStyles.largeBtn, customStyles.mt25 , customStyles.mb10]}
                                activeOpacity={0.5}
                                onPress={handleSubmitPress}>
                                <LinearGradient style={customStyles.roundedBtn} colors={['#F24D88', '#FC4D5B']}>
                                    <Text style={customStyles.lightText}>
                                    Register
                                </Text>
                                </LinearGradient>
                            </TouchableOpacity>
                        </View>
                    </ScrollView>
                </View>

            </View>


    );
}
const styles = StyleSheet.create({
    
  
    error: {
        color: Colors.error
    }
});

export default RegisterView;