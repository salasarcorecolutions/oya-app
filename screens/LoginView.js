import React ,{useState} from "react";
import {LinearGradient} from 'expo-linear-gradient';
import { Ionicons } from '@expo/vector-icons';
import { FontAwesome5 } from '@expo/vector-icons'; 
import customStyles from '../assets/css/customStyles'



import {
  StyleSheet,
  Text,
  View,
  TextInput,
  Button,
  TouchableHighlight,
  TouchableOpacity,
  Image,
  KeyboardAvoidingView,
  Keyboard,
} from "react-native";


const  LoginView = ({navigation}) => {
  const [userEmail, setUserEmail] = useState('');
  const [userPassword, setUserPassword] = useState('');
  const [loading, setLoading] = useState(false);
  const [errortext, setErrortext] = useState('');

  const handleSubmitPress = () => {
    setErrortext('');
    if (!userEmail) {
      alert('Please fill Username');
      return;
    }
    if (!userPassword) {
      alert('Please fill Password');
      return;
    }
    setLoading(true);
    let dataToSend = {
          user_email: userEmail,
          user_password: userPassword
        };
    let formBody = [];
    for (let key in dataToSend) {
      let encodedKey = encodeURIComponent(key);
      let encodedValue = encodeURIComponent(dataToSend[key]);
      formBody.push(encodedKey + '=' + encodedValue);
    }
    formBody = formBody.join('&');
    fetch('https://login.php', {
      method: 'POST',
      body: formBody,
      headers: {
        //Header Defination
        'Content-Type':
          'application/x-www-form-urlencoded;charset=UTF-8',
      },
    })
      .then((response) => response.json())
      .then((responseJson) => {
        //Hide Loader
        setLoading(false);
        console.log(responseJson);
        // If server response message same as Data Matched
        if (responseJson.status == 1) {
          AsyncStorage.setItem(
            'user_id',
             responseJson.data[0].user_id
          );
          console.log(responseJson.data[0].user_id);
         // navigation.replace('DrawerNavigationRoutes');
        } else {
          setErrortext('Please check your email id or password');
          console.log('Please check your email id or password');
        }
      })
      .catch((error) => {
        //Hide Loader
        setLoading(false);
        console.error(error);
      });
  
  };
  
    return (
      <View style={customStyles.container}>
        <View style={customStyles.header}>
          <LinearGradient colors={['#F24D88', '#FC4D5B']}>
            <View style={customStyles.headerContent}>
                <Image style={customStyles.logo} source={require("../assets/logo.png")} />
                <Text style={customStyles.headerTitle}> OYA AUCTION</Text>
            </View>
          </LinearGradient>
        </View>

        <View style={customStyles.body}>
          <View style={{alignItems:"center"}}>
            <Text style={[customStyles.titleText, customStyles.mb50 ,customStyles.mt25]}> Auctioneer Login</Text>
          </View>
          <View style={[customStyles.inputBorder, customStyles.mb20 ]}>
            <Text style={customStyles.label}>User Id</Text>
            <View style={{flexDirection:'row',justifyContent:'space-between'}}>

            <TextInput
              style={customStyles.inputs}
              underlineColorAndroid="transparent"
              onChangeText={(UserEmail) => setUserEmail(UserEmail)}
              placeholder="John Doe" //dummy@abc.com
              placeholderTextColor="#E4DEDD"
              autoCapitalize="none"
              keyboardType="email-address"
              returnKeyType="next"
              onSubmitEditing={() =>
                passwordInputRef.current &&
                passwordInputRef.current.focus()
              }
              underlineColorAndroid="#f000"
              blurOnSubmit={false}
            >
            </TextInput>
                <FontAwesome5 name="user-alt" size={23} color="#A4AEB7" />
              </View>
          </View>

          <View style={[customStyles.inputBorder, customStyles.mb20 ]}>
            <Text style={customStyles.label}>Password</Text>
            <View style={{flexDirection:'row',justifyContent:'space-between'}}>

              <TextInput
                style={customStyles.inputs}
                secureTextEntry={true}
                underlineColorAndroid="transparent"
                onChangeText={
                  (UserPassword) => setUserPassword(UserPassword)
                }
                placeholder="⚪ ⚪ ⚪ ⚪ ⚪ ⚪" //12345
                placeholderTextColor="#E4DEDD"
                keyboardType="default"
                onSubmitEditing={Keyboard.dismiss}
                blurOnSubmit={false}
                secureTextEntry={true}
                underlineColorAndroid="#f000"
                returnKeyType="next"
              />
                  <Ionicons name="md-lock" size={34} color="#A4AEB7" />

              </View>
          </View>
              
          <View style={{ justifyContent: "space-between" }}>
              <TouchableOpacity
                  style={customStyles.floatRight}
                  onPress={()=>{
                    navigation.navigate('ForgotPassword',{ names: 'Forgot Password' })
                  }}
                >
                <Text style={[customStyles.primaryText, customStyles.mb10]}>Forgot your password ?</Text>
              </TouchableOpacity>

             
          </View>
          {errortext != '' ? (
              <Text style={styles.errorTextStyle}>
                {errortext}
              </Text>
            ) : null}
            <View style={customStyles.center}>

            <TouchableOpacity
               style={[customStyles.largeBtn, customStyles.mt30 , customStyles.mb10]}
              activeOpacity={0.5}
              onPress={handleSubmitPress}>
                <LinearGradient style={customStyles.roundedBtn} colors={['#F24D88', '#FC4D5B']}>
                <Text style={customStyles.lightText}>
                  LOGIN
                </Text>
                </LinearGradient>
            </TouchableOpacity>

            <TouchableOpacity
                  style={[customStyles.rowCenter, customStyles.mt25]}
                  onPress={()=>{
                    navigation.navigate('RegisterView',{ names: 'Register View' })
                  }}
                >
                <Text style={customStyles.label}>Don't have an account ? </Text>
                <Text style={customStyles.primaryText}> Signup Now</Text>
              </TouchableOpacity>

            </View>
          <View>
            <TouchableOpacity>
            <Text style={customStyles.lightText}>Signup</Text>
            </TouchableOpacity>
          </View>
        </View>
      </View>         
     
      
    );
  
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#FC4D5B',
    alignItems: 'center',
    justifyContent: 'center',
    paddingTop:20,
  },
  inputContainer: {
    borderBottomColor:"#DBD3D2",
    borderBottomWidth:2,
    height:60,
    marginBottom:30,
    justifyContent:"center",
  },
  inputs: {
    height:50,
    color:"#AD9C99",
    fontFamily:"open-sans"
  },
  inputIcon: {
    width: 30,
    height: 30,
    marginLeft: 15,
    justifyContent: "center",
  },
  registerButton: {
    height: 5,
    justifyContent: "center",
    alignItems:'flex-start',
  },
  buttonContainer: {
    height: 5,
    justifyContent: "center",
    alignItems:'flex-end',
  },
  loginContainer:{
    alignItems:'center',
  },
  loginBtn:{
    width:"70%",
    backgroundColor:"#fb5b5a",
    borderRadius:25,
    height:50,
    alignItems:"center",
    justifyContent:"center",
    marginTop:60,
    marginBottom:10
  },
  loginText: {
    color: "white",
    fontFamily:'open-sans-bold',
    
  },
  header: {
    flex:1,
    width: '100%',
  },

  errorTextStyle: {
    color: 'red',
    textAlign: 'center',
    fontSize: 14,
  }

});

export default LoginView;