import {StyleSheet} from 'react-native';

const customStyles = StyleSheet.create({

  container: {
    flex: 1,
    backgroundColor: '#FC4D5B',
    alignItems: 'center',
    justifyContent: 'center',
    paddingTop:20,
  },
  header: {
    flex:1,
    width: '100%',
  },
  headerContent: {
    flexDirection: 'row',
    alignItems: 'center'
  },
  headerTitle:{
    color: '#fff',
    fontFamily:'open-sans-bold',
    fontSize:25,
  },
  body:{
    flex:4,
    backgroundColor:'#fff',
    width: '100%',
    borderTopLeftRadius : 30,
    borderTopRightRadius : 30,
    padding:25,
  },
  logo: {
    height: 90,
    width: 90,
    borderRadius: 50,
    margin: 15,
  },
  titleText:{
    color: '#34090D',
    fontFamily:'open-sans-bold',
    fontSize:24,
  },
  subTitle:{
    color: '#34090D',
    fontFamily:'open-sans-bold',
    fontSize:20,
  },
  inputBorder: {
    borderBottomColor:"#DBD3D2",
    borderBottomWidth:2,
    justifyContent:"center",
  },
  inputs: {
    height:40,
    color:"#AD9C99",
    fontFamily:"open-sans"
  },
  label:{
    color: '#5A6772',
    fontFamily:'open-sans-bold',
  },
  
  lightText: {
    color: "white",
    fontFamily:'open-sans-bold',
  },
  primaryText:{
    color:'#FC4D5B',

  },
  center:{
    alignItems:'center',
  },
  rowCenter:{
    flexDirection: 'row',
    justifyContent: 'center'
  },

// component style
dropdown:{
  height:40,
  color:"#AD9C99",
  fontFamily:"open-sans"
},
dropBox: {
  borderBottomColor:"#DBD3D2",
  borderBottomWidth:2,
  marginBottom:20,
  justifyContent:"center",
},
largeBtn:{
  width:"70%",
  height:50,
},
roundedBtn:{
  flex:1,
  justifyContent: "center",
  alignItems:"center",
  borderRadius:25,
},
floatRight: {
  alignItems:'flex-end',
},
// margin style
mt10 :{
  marginTop:10,
},
mt25 :{
  marginTop:25,
},
mt30 :{
  marginTop:30,
},
mt60 :{
  marginTop:60,
},
mb10 :{
  marginBottom:10
},
mb20 :{
  marginBottom:20
},
mb50 :{
  marginBottom:50
},

// colum grid style
row: {
  flex:1,
  flexDirection:'row',
  flexWrap: 'wrap'
  },
  col2: {
      width: '50%',
      flexDirection:'row',
      alignItems: 'center',
      marginBottom: 5,
      
  },
});

export default customStyles;