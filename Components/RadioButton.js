import React, { Component } from 'react';
import { Text, View, StyleSheet,TouchableOpacity } from 'react-native';
import RadioGroup from 'react-native-radio-buttons-group';

export default class RadioButton extends Component {
    state = {
      data: [
       
        {
          label: 'Auctioneer',
          value: "A",
          color: 'white',
        },
        {
          label: 'Buyer',
          color: 'white',
          value : 'B'
        },
      
      ],
    };
  
    // update state
    onPress = data => this.setState({ data });
  
    render() {
      let selectedButton = this.state.data.find(e => e.selected == true);
      selectedButton = selectedButton ? selectedButton.value : this.state.data[0].label;
      return (
        <View >
           <TouchableOpacity
              style={styles.buttonContainer}
            >
            <Text style={styles.forgot}>I Am</Text>
          </TouchableOpacity>
          <RadioGroup radioButtons={this.state.data} onPress={this.onPress}  flexDirection="row"/>
        </View>
      );
    }
  }
  
  const styles = StyleSheet.create({

    Button:{
        color: "red",
        
    },
    Text:{
        color: "red",

    },
  
    buttonContainer: {
        height: 10,
        flexDirection: "row",
        justifyContent: "center",
        alignItems: "center",
        width: 250,
        borderRadius: 5,
        marginBottom:15
      },
      forgot:{
        color:"white",
        fontSize:15
      },
  });
  