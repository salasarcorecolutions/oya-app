import React, { useState } from 'react';
import {
  View,
  Button,
  KeyboardAvoidingView,
  Image,
  ScrollView,
  StyleSheet
} from 'react-native';
import {Picker} from '@react-native-picker/picker';
import Colors from '../constants/Colors';
import Input from './Input';
import Card from './Card';
const Register = (props) => {
  const [state, setState] = useState({country: ''});

    <KeyboardAvoidingView
    behavior="padding"
    keyboardVerticalOffset={50}
    style={styles.screen}
  >
  <View style={styles.container}>
    <Image style={styles.logo} source={require("../assets/logo.png")}/>
      <Card style={styles.authContainer}>
        <ScrollView>
          <Input
            id="company_name"
            label="Company Name"
            keyboardType="default"
            required
            autoCapitalize="none"
            errorText="Please enter Company Name."
            initialValue=""
          />
        <Input
          id="company Address"
          label="Address"
          keyboardType="default"
          required
          autoCapitalize="none"
          errorText="Please enter company address."
          initialValue=""
        />
        <View>

       <Picker
      selectedValue={state.country}
      onValueChange={country => setState({ country })}
      style={{ width: 160, postion: 'absolute',fontSize:10 }}
      mode="dropdown"
      itemStyle={{ color:'red', fontWeight:'900', fontSize: 18, padding:30}}>
        {  props.countryList }
    </Picker>
    {/* <Picker
      selectedValue={this.state.hand}
      onValueChange={hand => this.setState({ hand })}
      style={{ width: 160, postion: 'absolute',fontSize:10 }}
      mode="dropdown"
      itemStyle={{ color:'red', fontWeight:'900', fontSize: 18, padding:30}}>
      <Picker.Item label="Right Hand" value="right" />
      <Picker.Item label="Left Hand" value="left" />
    </Picker>
    <Picker
      selectedValue={this.state.hand}
      onValueChange={hand => this.setState({ hand })}
      style={{ width: 160, postion: 'absolute',fontSize:10 }}
      mode="dropdown"
      itemStyle={{ color:'red', fontWeight:'900', fontSize: 18, padding:30}}>
      <Picker.Item label="Right Hand" value="right" />
      <Picker.Item label="Left Hand" value="left" />
    </Picker> */}
    </View>
    <Input
        id="pin_code"
        label="Pin Code"
        keyboardType="number-pad"
        secureTextEntry
        required
        minLength={5}
        autoCapitalize="none"
        errorText="Please enter a pin code."
        initialValue=""
      />
      {/* <Picker
      selectedValue={this.state.hand}
      onValueChange={hand => this.setState({ hand })}
      style={{ width: 160, postion: 'absolute',fontSize:10 }}
      mode="dropdown"
      itemStyle={{ color:'red', fontWeight:'900', fontSize: 18, padding:30}}>
      <Picker.Item label="Right Hand" value="right" />
      <Picker.Item label="Left Hand" value="left" />
    </Picker> */}
    <Input
          id="branch_name"
          label="Branch Name"
          keyboardType="default"
          required
          autoCapitalize="none"
          errorText="Please enter Branch Name."
          initialValue=""
        />
        <Input
          id="branch_ddress"
          label="Branch Address"
          keyboardType="default"
          required
          autoCapitalize="none"
          errorText="Please enter branch address."
          initialValue=""
        />
        <View>

       {/* <Picker
      selectedValue={this.state.hand}
      onValueChange={hand => this.setState({ hand })}
      style={{ width: 160, postion: 'absolute',fontSize:10 }}
      mode="dropdown"
      itemStyle={{ color:'red', fontWeight:'900', fontSize: 18, padding:30}}>
      <Picker.Item label="Right Hand" value="right" />
      <Picker.Item label="Left Hand" value="left" />
    </Picker>
    <Picker
      selectedValue={this.state.hand}
      onValueChange={hand => this.setState({ hand })}
      style={{ width: 160, postion: 'absolute',fontSize:10 }}
      mode="dropdown"
      itemStyle={{ color:'red', fontWeight:'900', fontSize: 18, padding:30}}>
      <Picker.Item label="Right Hand" value="right" />
      <Picker.Item label="Left Hand" value="left" />
    </Picker>
    <Picker
      selectedValue={this.state.hand}
      onValueChange={hand => this.setState({ hand })}
      style={{ width: 160, postion: 'absolute',fontSize:10 }}
      mode="dropdown"
      itemStyle={{ color:'red', fontWeight:'900', fontSize: 18, padding:30}}>
      <Picker.Item label="Right Hand" value="right" />
      <Picker.Item label="Left Hand" value="left" />
    </Picker> */}
    </View>
    <Input
        id="b_pin_code"
        label="Pin Code"
        keyboardType="number-pad"
        secureTextEntry
        required
        autoCapitalize="none"
        errorText="Please enter a pin code."
        initialValue=""
      />
      <Input
          id="name"
          label="Name"
          keyboardType="default"
          secureTextEntry
          required
          autoCapitalize="none"
          errorText="Please enter your name."
          initialValue=""
        />
        <Input
          id="username"
          label="User Name"
          keyboardType="default"
          secureTextEntry
          required
          autoCapitalize="none"
          errorText="Please enter a username."
          initialValue=""
        />
        <Input
          id="password"
          label="Password"
          keyboardType="default"
          secureTextEntry
          required
          autoCapitalize="none"
          errorText="Please enter a valid password."
          initialValue=""
        />
        <Input
          id="confirm_password"
          label="Confirm Password"
          keyboardType="default"
          secureTextEntry
          required
          autoCapitalize="none"
          errorText="Please enter a valid password."
          initialValue=""
        />
        <Input
          id="email"
          label="Email"
          keyboardType="default"
          secureTextEntry
          required
          email
          autoCapitalize="none"
          errorText="Please enter a valid email."
          initialValue=""
        />
        <Input
          id="mobile"
          label="mobile"
          keyboardType="default"
          secureTextEntry
          required
          autoCapitalize="none"
          errorText="Please enter a contact no."
          initialValue=""
        />
        <View style={styles.buttonContainer}>
          <Button
            title="Login"
            color={Colors.primary}
          />
        </View>
        <View style={styles.buttonContainer}>
          <Button
            title="Switch to Sign Up"
            color={Colors.accent}
            onPress={() => {}}
          />
        </View>
      </ScrollView>
    </Card>
    </View>
</KeyboardAvoidingView>
};

const styles = StyleSheet.create({
    screen: {
      flex: 1
    },
    gradient: {
      flex: 1,
      justifyContent: 'center',
      alignItems: 'center'
    },
    authContainer: {
      width: '80%',
      maxWidth: 400,
      padding: 20
    },
    buttonContainer: {
      marginTop: 10
    },
    container: {
      flex: 1,
      justifyContent: 'center',
      alignItems: 'center',

    },
    inputContainer: {
        borderBottomColor: '#F5FCFF',
        borderRadius:5,
        borderBottomWidth: 1,
        width:250,
        height:45,
        marginBottom:20,
        flexDirection: 'row',
        alignItems:'center'
    },
    inputs:{
        height:45,
        marginLeft:16,
        borderBottomColor: '#FFFFFF',
        flex:1,
    },
    inputIcon:{
      width:30,
      height:30,
      marginLeft:15,
      justifyContent: 'center'
    },
    buttonContainer: {
      color: '#efefef',
      height:45,
      flexDirection: 'row',
      justifyContent: 'center',
      alignItems: 'center',
      marginBottom:20,
      width:250,
      borderRadius:5,

    },
    loginButton: {
    },
    loginText: {
      color: 'white',
    },
    logo: {

      height: 100,
      width: 100,
      borderRadius:50,
      marginBottom:30
    }
  });

export default Register;

