import React from 'react';
import { View, Text, TextInput, StyleSheet } from 'react-native';

const Input = props => {
  return (
    <View style={styles.inputContainer}>
      <Text style={styles.label}>{props.label}</Text>
      <TextInput
        {...props}
        style={styles.inputs}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  inputContainer: {
    borderBottomColor:"#DBD3D2",
    borderBottomWidth:2,
    marginBottom:20,
    justifyContent:"center",
  },
  inputs: {
    height:40,
    color:"#AD9C99",
    fontFamily:"open-sans"
  },
  label:{
    color: '#5A6772',
    fontFamily:'open-sans-bold',
  },
  errorContainer: {
    marginVertical: 5
  },
  errorText: {
    color: 'red',
    fontSize: 13
  }
});

export default Input;
