import React from 'react';
import { Text, TouchableOpacity } from 'react-native';

const TouchableButton = props => {
    return (
        <TouchableOpacity
            activeOpacity={0.5}
            {...props}>
            <Text style={{ fontSize: 20 }}>
                { props.children }
            </Text>
        </TouchableOpacity>
    );
};


export default TouchableButton;
