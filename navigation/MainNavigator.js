import { createStackNavigator } from 'react-navigation-stack';
import { createAppContainer } from 'react-navigation';
import RegisterView from '../screens/RegisterView';
import ForgotPassword from '../screens/ForgotPassword';
import LoginView from '../screens/LoginView';
import RegistrationTerms from '../screens/RegistrationTerms';
import VerifyEmail from '../screens/VerifyEmail';


const MealsNavigator = createStackNavigator({
  Login: {
    screen:LoginView,
    navigationOptions:{
      headerShown: false,
    }
  },
  RegisterView: {
    screen: RegisterView,
    name: 'Register View'
  },
  ForgotPassword: {
    screen:ForgotPassword,
    name:'Forgot Password'
  },
  RegistrationTerms: {
    screen: RegistrationTerms,
    name:'Sign Up'
  },
  VerifyEmail: {
    screen: VerifyEmail,
    name:'Verify Email'
  }
});

export default createAppContainer(MealsNavigator);
