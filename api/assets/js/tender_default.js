/**
 * Salasarcore Solution
 *
 * @package		salasarauction
 * @author		SCS Dev Team
 * @copyright	Copyright (c) 2008 - 2014, EllisLab, Inc.
 * @license		
 * @link		http://salasarauction.com
 * @since		Version 1.0
 */

/*
 * Check the user inputs 
 * 
 */
function chkME() {


    return true;
}
/*
 * @param lotno
 */
function highlightLot(obj) {
		
	var lotno = obj.id;
	var parts = lotno.split('_');
	var periods = $('#timeleft_' + parts[1]).countdown('getTimes'); 
	
	if ($.countdown.periodsToSeconds(periods) < "300") { 		 
		$('#timeleft_' + parts[1]).toggleClass('highlight'); 
	} 	
}

function CloseLot(obj)
{
	setTimeout(function() {
		var lotno = obj.id;
		var parts = lotno.split('_');
		$.ajax({
			url: lot_close_url + "/" +  parts[1],
			dataType:'json',
			success: function(data) {
				if(data.status)
					$('table#lot_table tr#lot_' + parts[1] + '').remove();
				else
					{
						shortly = new Date();
						shortly.setSeconds(secondConvert(data.auction_data.TOTTIME));
						$('#timeleft_' + parts[1]).countdown('option', {until: shortly});
					}
				}
		});
	}, 1000);
    
}

function btnBidClicked(trID){
	 $("#IdMsg").html('');
	//trID = $(obj).attr("data-id");
	//tableData = $("#lot_" + trID + "").children("td").map(function() {
	//	return $(obj).text();
	//}).get();
	var $td= $("#lot_" + trID + "").closest('tr').children('td');  
	var sr= $td.eq(0).text();  
	$("#currency").html("<span class='h4 text-info'>"+$td.eq(11).text()+"</span>");
	 // alert(sr);
	/* if (parseFloat($("#MY_" + trID).html()) == "0")
		sum = parseFloat(parseFloat($("#Sbid" + trID).html()) + parseFloat($("#Incr" + trID).html())).toFixed(2);
	else
		sum = parseFloat(parseFloat($("#MY_" + trID).html()) + parseFloat($("#Incr" + trID).html())).toFixed(2);
	$("#txtMyBid").val(sum);  */
	$('#modal').modal('show');

}
function show_popup_page(show_popup){
	$.ajax({
			url: show_popup,
			dataType:'json',
			type:'post',
			success: function(data) {
				if(data!='no'){
					window.location.href=data;
			   }
			}
		
	});
}


