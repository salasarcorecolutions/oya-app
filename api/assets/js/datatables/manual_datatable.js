var oTable;
$(document).ready(function() { 
	//--------------- Data tables ------------------//
	var filename = window.location.pathname.substr(window.location.pathname.lastIndexOf("/")+1);
	
	var bFilter = true;
    if($('table').hasClass('nofilter')){
        bFilter = false;
    }
    var columnSort = new Array; 
    
	
	if($('table').hasClass('dynamicTable')){
		$("table.dynamicTable").find('thead tr th').each(function(){
			if($(this).attr('data-bSortable') == 'false') {
				columnSort.push({ "bSortable": false });
			} else {
				if($(this).html() == "Action" || $(this).html() == "Actions")
				{
					columnSort.push({ "bSortable": false });
				}else{
					columnSort.push({ "bSortable": true });
				}
			}
		});
		method = $(".dynamicTable").attr("callfunction");
		if(method == "" || method == null){
			method = filename+"/fetch";
		}
		noofrecords = $(".dynamicTable").attr("noofrecords");		
		oTable = $('.dynamicTable').dataTable({
			"sPaginationType": "full_numbers",			
			"bJQueryUI": false,
			"bAutoWidth": false,
			"bLengthChange": false,
			"bProcessing": true,
			"bServerSide": true,
			"iDisplayLength":noofrecords,
			"aaSorting":[],
			"sAjaxSource": method,
			"fnInitComplete": function(oSettings, json) {
				$('.dataTables_filter>label>input').parent().remove();				
		    },
		    "aoColumnDefs": [{ "bSortable": bFilter, "aTargets": [ -1 ] }],
		    "aoColumns": columnSort,
		    "fnDrawCallback": function( oSettings ) {
		    	/* if (typeof datatablecomplete == 'function') { 
		    		datatablecomplete("dynamicTable");
		    	}  */
		    },
		    "fnServerParams": function ( aoData ) {
		    	var searchCount = 0;
		    	$(".extraFields").each(function(){
		    		aoData.push( { "name": "extraSearchkey_"+searchCount, "value": $(this).val() } );
		    		searchCount++;
		    	});
		     }
		});
	
		
		$(".filter select").bind("change", function()  {
			oTable.fnFilter(this.value, oTable.oApi._fnVisibleToColumnIndex(oTable.fnSettings(),$(".searchInput").index(this)));
		});
		/* $(".filter input").bind("blur", function () {
            oTable.fnFilter(this.value, oTable.oApi._fnVisibleToColumnIndex(oTable.fnSettings(),$(".searchInput").index(this)));
        }); */
		$(".filter input").keyup( function () {
			oTable.fnFilter(this.value, oTable.oApi._fnVisibleToColumnIndex(oTable.fnSettings(),$(".searchInput").index(this)));
		});		
	}
	
	if($('table').hasClass('staticTable')){
		$('.staticTable').dataTable();
	}
});		

function clearSearchFilters() {
	$('.filter input').val('');
	$('.filter select').prop('selectedIndex', 0);
	var oSettings = oTable.fnSettings();
	for (iCol = 0; iCol < oSettings.aoPreSearchCols.length; iCol++) {
		oSettings.aoPreSearchCols[ iCol ].sSearch = '';
	}
	oTable.fnDraw();
	/* if (typeof calbackfunction == 'function') {
		calbackfunction();
	} */
}
