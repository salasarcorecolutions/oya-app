<?php
defined('BASEPATH') OR exit('No direct script access allowed');

function generate_unique_seo($url){
    $seo_patterns = array('/[^a-zA-Z0-9 -]/', '/[ -]+/', '/^-|-$/','/quot/');
    $seo_replace = array('', '-', '', '');

    $url = preg_replace($seo_patterns,$seo_replace,$url);
    $seo_name = strtolower($url);
    return $seo_name;
}

?>