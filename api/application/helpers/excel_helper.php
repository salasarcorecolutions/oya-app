<?php
$styleNoBorderBackground=array(
	'fill' => array(
		'type'  => PHPExcel_Style_Fill::FILL_SOLID,
		'color' => array('rgb' => 'C5D9F1')
	),
);
$styleFontBold= array(
'font'  => array(
	'bold'  => true,
	'color' => array('rgb' => '000000'),
	'size'  => 10,
	'name'  => 'Verdana'
)
);
 $horizontal_text_center = array(
	'alignment' => array(
		'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
	)
);

$styleBorderThinArray=array(
	'borders' => array(
		'allborders' => array(
			'style' => PHPExcel_Style_Border::BORDER_THIN
		)
	)
);
$styleBorderMediumArray=array(
	'borders' => array(
		'allborders' => array(
			'style' => PHPExcel_Style_Border::BORDER_MEDIUM
		)
	)
);
$styleArray = array(
'font'  => array(
	'bold'  => true,
	'size'  => 12,
	'name'  => 'Verdana'
));

$styleRedArray = array(
'font'  => array(
	'bold'  => true,
	'color' => array('rgb' => 'FF0000'),
	'name'  => 'Verdana'
));
?>