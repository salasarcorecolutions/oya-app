<?php
/*
 |--------------------------------------------------------------------------
 | UTILITY HELPERS
 |--------------------------------------------------------------------------
 |
 | If your server is behind a reverse proxy, you must whitelist the proxy IP
 | addresses from which CodeIgniter should trust the HTTP_X_FORWARDED_FOR
 | header in order to properly identify the visitor's IP address.
 | Comma-delimited, e.g. '10.0.1.200,10.0.1.201'
 |
 
    FUNCTION LIST
    1. is_file_exist : CHECKS IF THE FILE EXISTS 
    2. is_date_valid    : Checks a vlid  Date

*/

/**
 * Check excel file exist on server or not 
 * @param string $url
 * @return string
 */
function is_file_exist($url){
	$ch = curl_init($url);    
	curl_setopt($ch, CURLOPT_NOBODY, true);
	curl_exec($ch);
	$code = curl_getinfo($ch, CURLINFO_HTTP_CODE);

	$tmpfname = '';
	if($code == 200){
		$ini_val = ini_get('upload_tmp_dir');
		$filecontent = file_get_contents($url);
		$tmpfname = tempnam($ini_val,"tmpxls");
		file_put_contents($tmpfname,$filecontent);
	}
	curl_close($ch);
	return $tmpfname;
}

/**
 * Check a valid Date
 * @param datetime $str
 * @return boolean
 */
function is_date_valid( $str ) {
    try {
        $dt = new DateTime( trim($str) );
    }
    catch( Exception $e ) {
        return false;
    }
    $month = $dt->format('m');
    $day = $dt->format('d');
    $year = $dt->format('Y');
    if( checkdate($month, $day, $year) ) {
        return true;
    }
    else {
        return false;
    }
}

/**
 * Checks if $userDate date is between $startDate, $endDate
 * @param DateTime $startDate
 * @param DateTime $endDate
 * @param DateTime $userDate
 * @return boolean
 */

function isDateInRange($startDate, $endDate, $userDate) {
    $startT = strtotime($startDate);
    $endT = strtotime($endDate);
    $userT = strtotime($userDate);
    return (($userT >= $startT) && ($userT <= $endT));
}

/**
 * 
 * @param String $data
 * @return string
 */
function RenameUploadFile($str) {
	return  preg_replace("/[^a-z0-9\._]+/", "", strtolower(uniqid()."_".$str));
}

/**
 * 
 * @param String $filename
 * @param String $path_to_image_directory
 * @param String $path_to_thumbs_directory
 * @param String $final_width_of_image
 */
function compress_image($filename,$path_to_image_directory,$path_to_thumbs_directory='',$final_width_of_image=300){
	$im = null;
	$image_type = 'jpg';
	if(preg_match('/[.](jpg)$/', $filename)) {
		$im = imagecreatefromjpg($path_to_image_directory . $filename);
	} else if (preg_match('/[.](jpeg)$/', $filename)) {
		$im = imagecreatefromjpeg($path_to_image_directory . $filename);
	} else if (preg_match('/[.](gif)$/', $filename)) {
		$image_type = 'gif';
		$im = imagecreatefromgif($path_to_image_directory . $filename);
	} else if (preg_match('/[.](png)$/', $filename)) {
		$image_type = 'png';
		$im = imagecreatefrompng($path_to_image_directory . $filename);
	}
	 
	$ox = imagesx($im);
	$oy = imagesy($im);
	 
	$nx = $final_width_of_image;
	$ny = floor($oy * ($final_width_of_image / $ox));
	$nm = imagecreatetruecolor($nx, $ny);
	imagecopyresampled($nm, $im, 0,0,0,0,$nx,$ny,$ox,$oy);
	if($image_type == "png"){
		$background = imagecolorallocate($nm, 0, 0, 0);
		// removing the black from the placeholder
		imagecolortransparent($nm, $background);
		@imagepng($nm, $path_to_thumbs_directory.$filename,9);
	}else if($image_type == "gif"){
		$background = imagecolorallocate($nm, 0, 0, 0);
        // removing the black from the placeholder
        imagecolortransparent($nm, $background);
		@imagegif($nm, $path_to_thumbs_directory.$filename);
	}else{
		@imagejpeg($nm, $path_to_thumbs_directory.$filename,75);
	}
	imagedestroy($nm);
}

/**
 * Generates randam code string depending on length
 * @param integer $length
 * @param string $type
 * @return string
 */
function rand_string( $length, $type = 'numeric' ) {
	$chars = "";
	if($type == 'numeric')
	{
		$chars = "0123456789";
	}
	else if($type == 'alpha')
	{
		$chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
	}
	else if($type == 'alphanumeric')
	{
		$chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
	}
    return substr(str_shuffle($chars),0,$length);

}



/**
 * 
 * Converts user time zones to  server timezone
 * @param string $user_datetime
 * @param string $user_tz
 * @param string $format- user time 
 * @return string
 */
function usertz_to_servertz($user_datetime, $user_tz, $format='Y-m-d H:i:s')
{
	$server_tz = date_default_timezone_get();
	
	$date = new DateTime($user_datetime, new DateTimeZone($user_tz));
	$date->setTimezone(new DateTimeZone($server_tz));
	return $date->format($format);
}

function only_closed_lots_clean($string) {
   return preg_replace('/[^A-Za-z0-9\- ]/', '', $string); // Removes special chars.
}

function my_money_format($value)
{
  	if (function_exists('money_format'))
  	{
        return money_format('%!i', $value);
    }
	else
	{
		return number_format($value,2,'.',',');
	}
}

/**
 * Converts date and time from server time zone to user time zone
 * @param string $user_datetime
 * @param string $user_tz
 * @param string $format
 * @return string
 */
function servertz_to_usertz($user_datetime, $user_tz, $format='F j, Y, g:i a')
{
	$server_tz = date_default_timezone_get();

	$date = new DateTime($user_datetime, new DateTimeZone($server_tz));
	$date->setTimezone(new DateTimeZone($user_tz));
	return $date->format($format);
}

/**
 * Remove all hackers character from input string
 * @param string $string
 * @return string
 */

function makeSafe($string){
        $string=trim($string);
		$string=str_replace("'","",$string);
		$string=str_replace("\\","",$string);
		$string=str_replace("//","",$string);
		//$string=str_replace("-","",$string);
		//$string=str_replace(")","",$string);
		//$string=str_replace("(","",$string);
		$string=str_replace("0x27","",$string);
		$string=str_replace("0x7e","",$string);
		$string=str_replace("information_schema","",$string);
		
		
		$string=(get_magic_quotes_gpc() ? stripslashes($string) : $string);
		return (trim($string));
		
	}

	
/**
 * Get the file size 
 * @param string $file_name -filename with path
 * @return number in bytes
 */
function file_size($file_name) {   
    if(is_file($file_name))
        return filesize($file_name);
    else
        return 0;  
} 


?>