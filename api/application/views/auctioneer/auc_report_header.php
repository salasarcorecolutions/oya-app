<?php
$imgpath = "productpic";
?>
<!DOCTYPE HTML>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title><?php if(isset($pagetitle)){echo $pagetitle;}else{echo "Oya Auction";} ?></title>
	<link href="<?php echo base_url('auctioneer_assets/css/bootstrap.min.css'); ?>" rel="stylesheet">
	<!--Nifty Stylesheet [ REQUIRED ]-->
	<link href="<?php echo base_url('auctioneer_assets/css/nifty.min.css'); ?>" rel="stylesheet">
	<link href="<?php echo base_url('auctioneer_assets/css/dhtmlwindow.css'); ?>" rel="stylesheet">
	<link href="<?php echo base_url('auctioneer_assets/css/modal.css'); ?>" rel="stylesheet">
	<!--Font Awesome [ OPTIONAL ]-->
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
	<link href="<?php echo base_url('auctioneer_assets/css/nifty.min.css'); ?>" rel="stylesheet">
	<!--Demo script [ DEMONSTRATION ]-->
	<!-- <link href="<?php echo base_url('auctioneer_assets/css/demo/nifty-demo.min.css'); ?>" rel="stylesheet"> -->

	<link href="<?php echo base_url('auctioneer_assets/plugins/bootstrap-timepicker/bootstrap-timepicker.min.css'); ?>" rel="stylesheet">

	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/css/bootstrap-datepicker.min.css" integrity="sha256-siyOpF/pBWUPgIcQi17TLBkjvNgNQArcmwJB8YvkAgg=" crossorigin="anonymous" />

	<!-- Favicons
	================================================== -->
	<link rel="shortcut icon" href="<?php echo base_url('auctioneer_assets/images/favicon.png'); ?>" type="image/x-icon">
	
	<!-- JS -->
	<script src="<?php echo base_url('auctioneer_assets/js/jquery-2.1.1.min.js'); ?>"></script>

	<!--BootstrapJS [ RECOMMENDED ]-->
	<script src="<?php echo base_url('auctioneer_assets/js/bootstrap.min.js'); ?>"></script>

	<script src="<?php echo base_url('auctioneer_assets/js/fastclick.min.js'); ?>"></script>
	<script src="<?php echo base_url('auctioneer_assets/js/bootbox.min.js'); ?>"></script>

<script language="javascript">
	function refreshParent(page) {
		location.href = "index.php?src="+page;
	}
</script>

</head>
<body style="position: relative;">