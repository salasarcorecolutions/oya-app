<style>
	.cke_toolbar_break{
		display: none;
	}
	.text-danger i {
		padding-left: 10px;
	}
	.control-label{
		color: #25476a;
		font-weight: bold;
	}
</style>
<!--CONTENT CONTAINER-->
<!--===================================================-->
<div id="content-container">

<!--Page Title-->
<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
<div id="page-title">
	<h1 class="page-header text-overflow"><?php echo $pagetitle;?></h1>
</div>
<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
<!--End page title-->

<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
<!--End breadcrumb-->
	<div id="page-content">
		<div class="panel panel-primary">
			<div class="panel-heading">
				<h3 class="panel-title">Company Details</h3>
			</div>
			<div class="panel-body">
                <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover table-condensed">
                        <thead>
                            <th>Company Name</th>
                            <th>Address</th>
                            <th>City</th>
                            <th>State</th>
                            <th>Country</th>
                            <th>GST</th>
                            <th>Action</th>
                        </thead>
                        <tbody>
                            <td><?php echo $company_details['vendor_name']; ?></td>
                            <td><?php echo $company_details['address1'].' '.$company_details['address2']; ?></td>
                            <td><?php echo $company_details['city_name']; ?></td>
                            <td><?php echo $company_details['state_name']; ?></td>
                            <td><?php echo $company_details['country_name']; ?></td>
                            <td><?php echo $company_details['gst']; ?></td>
                            <td><?php if ($add_edit_permission['status'] == 'success'){ ?><a class="btn btn-primary btn-labeled fa fa-edit" id="edit_company"
                                    vendor_name="<?php echo $company_details['vendor_name'] ?>"
                                    address1="<?php echo $company_details['address1'] ?>"
                                    address2="<?php echo $company_details['address2'] ?>"
                                    country="<?php echo $company_details['country'] ?>"
                                    state="<?php echo $company_details['state'] ?>"
                                    city="<?php echo $company_details['city'] ?>"
                                    pincode="<?php echo $company_details['pin'] ?>"
                                    gst="<?php echo $company_details['gst'] ?>"
                                    fax="<?php echo $company_details['fax'] ?>"
                                    onclick="editCompanyDetails()"> Edit
                                </a>
                            <?php } ?>
                            </td>
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="panel panel-warning">
			<div class="panel-heading">
				<h3 class="panel-title">Branch Details
                    <div class="pull-right">
                        <?php if ($add_edit_permission['status'] == 'success'){ ?>
                            <a class="btn btn-default btn-labeled fa fa-plus" onclick="addBranch()">Add</a>
                            <a class="btn btn-default btn-labeled fa fa-edit" onclick="editBranch()">Edit</a>
                        <?php } ?>
                    </div>
                </h3>
			</div>
		
            <div class="panel-body">
          
                <h4>Branch Details</h4>
                <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover table-condensed">
                        <thead>
                            <th>#</th>
                            <th>Plant Name</th>
                            <th>Address</th>
                            <th>City</th>
                            <th>State</th>
                            <th>Country</th>
                        </thead>
                        <tbody>
                            <?php foreach ($branch_details as $value){ ?>
                                <tr>
                                    <td><input name="branch_id" type='radio' value="<?php echo $value['company_plant_id'] ?>" /></td>
                                    <td><?php echo $value['plant_name']; ?></td>
                                    <td><?php echo $value['address1'].' '.$value['address2']; ?></td>
                                    <td><?php echo $value['city_name']; ?></td>
                                    <td><?php echo $value['state_name']; ?></td>
                                    <td><?php echo $value['country_name']; ?></td>
                                </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                </div>
			</div>
			</div>
		</div>
	</div>
</div>
<div class="modal" id="company_model" role="dialog" tabindex="-1" aria-labelledby="demo-default-modal" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<!--Modal header-->
			<div class="modal-header">
				<button data-dismiss="modal" class="close" type="button">
					<span aria-hidden="true">&times;</span>
				</button>
				<h4 class="modal-title"><span id="modal_title"></span></h4>
			</div>
            <!--Modal body-->
            <form class="form-horizontal" id="company_form">
                <div class="modal-body">
                    <div class="panel">
                        <div class="panel-body">
                            <input type="hidden" id="company_branch" name="company_branch" />
                            <input type="hidden" id="branch_id" name="branch_id" />
                            <div class="form-group">
                                <label id="company_branch_label" for="company_name" class="col-sm-5 control-label"></label>
                                <div class="col-sm-7">
                                    <input type="text" class="form-control" id="company_branch_name" name="company_branch_name"
                                    placeholder="Company Name">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="address1" class="col-sm-5 control-label">Address Line 1</label>
                                <div class="col-sm-7">
                                    <input type="text" class="form-control" id="address1" name="address1"
                                    placeholder="Address Line 1">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="address2" class="col-sm-5 control-label">Address Line 2</label>
                                <div class="col-sm-7">
                                    <input type="text" class="form-control" id="address2" name="address2"
                                    placeholder="Address Line 2">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="country" class="col-sm-5 control-label">Country</label>
                                <div class="col-sm-7">
                                   <select id="country" name="country">
                                       <option></option>
                                       <?php foreach ($country as $c){ ?>
                                        <option value="<?php echo $c['id']; ?>"><?php echo $c['name']; ?></option>
                                       <?php } ?>
                                   </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="state" class="col-sm-5 control-label">State</label>
                                <div class="col-sm-7">
                                    <select id="state" name="state">
                                        <option></option>
                                   </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="city" class="col-sm-5 control-label">City</label>
                                <div class="col-sm-7">
                                   <select id="city" name="city">
                                       <option></option>
                                   </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="pincode" class="col-sm-5 control-label">Pin Code</label>
                                <div class="col-sm-7">
                                    <input type="text" class="form-control" id="pincode" name="pincode"
                                    placeholder="Pin Code">
                                </div>
                            </div>
                            <div id="gst_div" class="form-group">
                                <label for="gst" class="col-sm-5 control-label">GST</label>
                                <div class="col-sm-7">
                                    <input type="text" class="form-control" id="gst" name="gst"
                                    placeholder="GST">
                                </div>
                            </div>
                            <div id="fax_div" class="form-group">
                                <label for="fax" class="col-sm-5 control-label">Fax</label>
                                <div class="col-sm-7">
                                    <input type="text" class="form-control" id="fax" name="fax"
                                    placeholder="FAX">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!--Modal footer-->
                <div class="modal-footer">
                    <button data-dismiss="modal" class="btn btn-default" type="button">Close</button>
                    <button class="btn btn-primary" id="btnSave">Save changes</button>
                </div>
            </form>
		</div>
	</div>
</div>


<!--===================================================-->
<!--END CONTENT CONTAINER-->
<style type='text/css'>
.table>tbody>tr>td,
.table>tbody>tr>th,
.table>tfoot>tr>td,
.table>tfoot>tr>th, 
.table>thead>tr>td,
.table>thead>tr>th
{
	padding: 4px !important;
}
fieldset
{
	border: 1px solid #ddd !important;
	margin: 0;
	padding: 10px;
	position: relative;
	border-radius:4px;
	background-color:#f5f5f5;
	padding-left:10px!important;
}

legend
{
	font-size:14px;
	font-weight:bold;
	margin-bottom: 0px;
	width: 35%; 
	border: 1px solid #ddd;
	border-radius: 4px;
	padding: 5px 5px 5px 10px; 
	background-color: #ffffff;
}
</style>

<script src="https://cdn.ckeditor.com/4.14.0/standard/ckeditor.js"></script>

<script type='text/javascript'>
$(document).ready(function(){
    $("#country").select2({
        placeholder: "Select Country",
        width: "100%",
        dropdownParent: $('#company_model')
    })
    $("#state").select2({
        placeholder: "Select State",
        width: "100%",
        dropdownParent: $('#company_model')
    })
    $("#city").select2({
        placeholder: "Select City",
        width: "100%",
        dropdownParent: $('#company_model')
    })
});


function clearForm()
{
    $("#company_branch_name").val('');
    $("#address1").val('');
    $("#address2").val('');
    $("#country").val('').trigger('change');
    $("#state").val('').trigger('change');
    $("#city").val('').trigger('change');
    $("#pincode").val('');
    $("#gst").val('');
    $("#fax").val('');
    $("#company_branch").val('');
    $("#branch_id").val('');
}

function editCompanyDetails()
{
    clearForm();
    $("#gst_div").show();
    $("#fax_div").show();
    $("#company_branch_name").val($("#edit_company").attr("vendor_name"));
    $("#address1").val($("#edit_company").attr("address1"));
    $("#address2").val($("#edit_company").attr("address2"));
    $("#country").val($("#edit_company").attr("country")).trigger('change');
    $("#state").val($("#edit_company").attr("state")).trigger('change');
    $("#city").val($("#edit_company").attr("city")).trigger('change');
    $("#pincode").val($("#edit_company").attr("pincode"));
    $("#gst").val($("#edit_company").attr("gst"));
    $("#fax").val($("#edit_company").attr("fax"));
    $("#company_branch").val('C');
    $("#modal_title").html('Edit Company Details');
    $("#company_branch_label").html("Company Name");
    $("#company_model").modal('show');
}

function addBranch()
{
    clearForm();
    $("#gst_div").hide();
    $("#fax_div").hide();
    $("#company_branch").val('B');
    $("#modal_title").html('Add Branch Details');
    $("#company_branch_label").html("Branch Name");
    $("#company_model").modal('show');
}

function editBranch()
{
    clearForm();
    var radioValue = $("input[name='branch_id']:checked").val();
    if (radioValue){
        $.ajax({
            url:"<?php echo base_url();?>auctioneer/company_plant/branch_details_by_id",
            type: 'post',
            data: {
                branch_id: radioValue
            },
            dataType:'json',
            success: function (response)
            {
                $("#company_branch_name").val(response.plant_name);
                $("#address1").val(response.address1);
                $("#address2").val(response.address2);
                $("#country").val(response.country).trigger('change');
                $("#state").val(response.state).trigger('change');
                $("#city").val(response.city).trigger('change');
                $("#pincode").val(response.pin);
                $("#gst_div").hide();
                $("#fax_div").hide();
                $("#company_branch").val('B');
                $("#branch_id").val(radioValue);
                $("#modal_title").html('Edit Branch Details');
                $("#company_branch_label").html("Branch Name");
                $("#company_model").modal('show');
            }
        });
    } else {
        Display_msg('Select Branch', 'error');
    }
}


$("#country").change(function(){
    $.ajax({
        url:"<?php echo base_url();?>auctioneer/company_plant/state_list",
        type: 'post',
        data: {
            country_id: $("#country").val()
        },
        async:false,
        dataType:'json',
        success: function (response)
        {
            console.log(response)
            var i;
            var html = '<option></option>';
            for (i = 0; i < response.length; i++){
                html += '<option value="'+response[i]['id']+'" >'+response[i]['state']+'</option>';
            }
            $("#state").html(html);
        }
    });
})

$("#state").change(function(){
    $.ajax({
        url:"<?php echo base_url();?>auctioneer/company_plant/city_list",
        type: 'post',
        data: {
            state_id: $("#state").val()
        },
        async:false,
        dataType:'json',
        success: function (response)
        {
            var i;
            var html = '<option></option>';
            for (i = 0; i < response.length; i++){
                html += '<option value="'+response[i]['id']+'" >'+response[i]['name']+'</option>';
            }
            $("#city").html(html);
        }
    });
})

var vRules 	= 	{
    company_branch_name: {required:true},
    address1: {required:true},
    country: {required:true},
    state: {required:true},
    city: {required:true},
    pincode: {required:true}
};
var vMessage = {
    company_branch_name: {required:"<b class='text-danger'>Please Enter Name</b>"},
    address1: {required:"<b class='text-danger'>Please Enter Address</b>"},
    country: {required:"<b class='text-danger'>Please Select Country</b>"},
    state: {required:"<b class='text-danger'>Please Select State</b>"},
    city: {required:"<b class='text-danger'>Please Select City</b>"},
    pincode: {required:"<b class='text-danger'>Please Enter Pin Code</b>"}
};

$("#company_form").validate({
    rules: vRules,
    messages: vMessage,
    submitHandler: function(form){
        $(form).ajaxSubmit({
            url: '<?php echo base_url('auctioneer/company_plant/update_company_plant_details');?>',
            type:"post",
            dataType: 'json',
            success: function (resObj) {
                Display_msg(resObj.message,resObj.status);
                if (resObj.status == 'success'){
                    $("#company_model").modal('hide');
                    setTimeout(function(){ location.reload(1); }, 3000);
                }
            },
            error: function(e){
                alert('Problem in fetching auctioneer data. Please try again.');
            }
        });
    }
});


</script>
