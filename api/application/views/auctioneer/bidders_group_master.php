<!--CONTENT CONTAINER-->
<!--===================================================-->
<div id="content-container">

<!--Page Title-->
<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
<div id="page-title">
	<h1 class="page-header text-overflow"><?php echo $pagetitle; ?></h1>
</div>
<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
<!--End page title-->

<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
<!--End breadcrumb-->
	<div id="page-content">
		<div class="panel">
			<div class="panel-heading">
				<h3 class="panel-title"><?php echo $pagetitle; ?>
                    <div class="pull-right">
                        <button id="add_group" class="btn btn-primary btn-labeled fa fa-plus">Add Group</button>
                        <button id="edit_group" class="btn btn-info btn-labeled fa fa-edit">Edit Group</button>
                        <button id="delete_group" class="btn btn-danger btn-labeled fa fa-trash">Delete Group</button>
                        <button id="add_bidders" class="btn btn-mint btn-labeled fa fa-plus">Add Bidders</button>
                    </div>
                </h3>
			</div>
			<div class="panel-body">
                <table id="bidder_group_table" class="table table-striped table-bordered table-hover table-responsive">
                    <thead>
                        <th>#</th>
                        <th>Group Name</th>
                        <th>Group Description</th>
                        <th>Updated By</th>
                    </thead>
                    <tbody id="bidder_groups">
                    </tbody>
                </table>
			</div>
		</div>
	</div>
</div>

<!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title" id="modaltitle"></h4>
            </div>
            <form id="bidders_group_frm">
                <div class="modal-body">
                    <input type="hidden" id="group_id" name="group_id" />
                    <div class="col-md-12">
                        <div class="col-md-4">
                            <label>Group Name :</label>
                        </div>
                        <div class="col-md-8">
                            <input class="form-control" id="group_name" type="text" name="group_name" />
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="col-md-4">
                            <label>Group Description :</label>
                        </div>
                        <div class="col-md-8">
                            <textarea class="form-control" id="group_description" type="text" name="group_description"></textarea>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-success">Submit</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </form>
        </div>
    </div>
</div>


<!--===================================================-->
<!--END CONTENT CONTAINER-->
<style type='text/css'>
.table>tbody>tr>td,
.table>tbody>tr>th,
.table>tfoot>tr>td,
.table>tfoot>tr>th,
.table>thead>tr>td,
.table>thead>tr>th
{
	padding: 4px !important;
}
fieldset
{
	border: 1px solid #ddd !important;
	margin: 0;
	padding: 10px;
	position: relative;
	border-radius:4px;
	background-color:#f5f5f5;
	padding-left:10px!important;
}

legend
{
	font-size:14px;
	font-weight:bold;
	margin-bottom: 0px;
	width: 35%;
	border: 1px solid #ddd;
	border-radius: 4px;
	padding: 5px 5px 5px 10px;
	background-color: #ffffff;
}
</style>

<script src="https://cdn.ckeditor.com/4.14.0/standard/ckeditor.js"></script>

<script type='text/javascript'>
var t;
$(document).ready(function(){
    t = $("#bidder_group_table").DataTable();
    getGroupData();
});


function getGroupData()
{
    $.ajax({
        url:'<?php echo base_url('auctioneer/bidders/bidders_group_data')?>',
        dataType: 'json',
        success:function(data)
        {
            if (data){
                var i;
                for (i=0; i<data.length; i++){
                    t.row.add( [
                        '<input type="radio" name="radio_group_id" value="'+data[i]['bidder_group_master_id']+'" />',
                        data[i]['group_name'],
                        data[i]['group_description'],
                        data[i]['c_name']
                    ] ).draw( false );
                }
            }
        },
        error: function(){
            alert("Problem in getting Bidder's group data. Please try again.");
        }
    });
}

function clearForm()
{
    $("#group_id").val('');
    $("#group_name").val('');
    $("#group_description").val('');
}

$("#add_group").click(function(){
    clearForm();
    $("#modaltitle").html("Add Group");
    $("#myModal").modal('show');
});

$("#edit_group").click(function(){
    var id = $('input[name="radio_group_id"]:checked').val();
    if (id){
        $.ajax({
            url:'<?php echo base_url('auctioneer/bidders/bidder_group_details_by_id')?>',
            data: {"id":id},
            type: "POST",
            dataType: 'json',
            success:function(response){
                clearForm();
                $("#modaltitle").html("Edit Group");
                $("#group_id").val(id);
                $("#group_name").val(response.group_name);
                $("#group_description").val(response.group_description);
                $("#myModal").modal('show');
            },
            error: function(){
                alert('Problem in fetching group details. Please try again');
            }
        });
    } else {
        Display_msg('Select Group', 'failed');
    }

});

$("#delete_group").click(function(){
    var id = $('input[name="radio_group_id"]:checked').val();
    if (id){
        $.ajax({
            url:'<?php echo base_url('auctioneer/bidders/delete_bidder_group')?>',
            data: {"id":id},
            type: "POST",
            dataType: 'json',
            success:function(response){
                Display_msg(response.message,response.status);
                if (response.status == 'success'){
                    t.rows().remove().draw();
                    getGroupData();
                }
            },
            error: function(){
                alert('Problem in deleting group details. Please try again');
            }
        });
    } else {
        Display_msg('Select Group', 'failed');
    }
});

$("#bidders_group_frm").validate({
    rules:{
        group_name:{required:true},
        group_description:{required:true},
    },
    messages:{
        group_name:{required:"<strong class='text-danger'>Please Enter Group Name</strong>"},
        group_description:{required:"<strong class='text-danger'>Please Enter Group Description</strong>"},
    },
    submitHandler: function(form){
        $(form).ajaxSubmit({
            url: '<?php echo base_url();?>auctioneer/bidders/add_edit_bidder_group',
            type:"post",
            dataType: 'json',
            success: function (response) {
                Display_msg(response.message,response.status);
                if (response.status == 'success'){
                    t.rows().remove().draw();
                    getGroupData();
                    $("#myModal").modal('hide');
                }
            },
            error: function(e){
                alert('Problem in updating data please try again');
            }
        });
    }
});

$("#add_bidders").click(function() {
    var id = $('input[name="radio_group_id"]:checked').val();
    if (id){
        location.replace('<?php echo base_url('auctioneer/bidders/bidder_group_relation/'); ?>'+id);
    } else {
        Display_msg('Select Group', 'failed');
    }
});

</script>
