<!--CONTENT CONTAINER-->
<!--===================================================-->
<div id="content-container">
    <!--Page Title-->
    <!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
    <div id="page-title">
        <h1 class="page-header text-overflow"><?php echo $pagetitle; ?></h1>
    </div>
    <!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
    <!--End page title-->

    <!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
    <!--End breadcrumb-->
	<div id="page-content">
		<div class="panel">
			<div class="panel-body">
			    <form id="frm">
					<div class="col-lg-12">
						<div class="col-lg-2">
							<h5 style="margin:0px;">Select image to upload:</h5>
						</div>
						<div class="col-lg-2">
							<input class='form-control' type="file" name="upload_file" id="fileToUpload">
						</div>
						<div class="col-lg-2">
							<button class="btn btn-success" type="submit" value="Upload Image" name="submit">Upload</button>
						</div>
					</div>
                </form>
			</div>
		</div>
    </div>
</div>
<!--===================================================-->
<!--END CONTENT CONTAINER-->
<script>

	$("#frm").validate({
		rules:{
			upload_file:{required:true},
		},
		messages:{
			upload_file:{required:"<strong class='text-danger'>Upload file to continue</strong>"},
		},
		submitHandler: function(form)
		{
			$(form).ajaxSubmit({
				url: '<?php echo base_url();?>auctioneer/dashboard/upload_vendor_logo',
				type:"post",
				dataType: 'json',
				success: function (response) {
					alert(response.message)
					if (response.status == 'success'){
						setTimeout(function(){ location.reload(1); }, 3000);
					}
				},
				error: function(e){
					alert('Problem in updating data please try again');
				}
			});
		}
	});

</script>