<!--MAIN NAVIGATION-->
<!--===================================================-->
<nav id="mainnav-container">
	<div id="mainnav">
		<div id="mainnav-menu-wrap">
			<div class="nano">
				<div class="nano-content">
					<ul id="mainnav-menu" class="list-group">
						<!--Menu list item-->
						<li>
							<a href="<?php echo base_url('')?>">
								<i class="fa fa-home"></i>
								<span class="menu-title">
									<strong>Home</strong>
								</span>
							</a>
						</li>		
						<li>
							<a href="<?php echo base_url('auctioneer/dashboard')?>">
								<i class="fa fa-dashboard"></i>
								<span class="menu-title">
									<strong>Dashboard</strong>												
								</span>
							</a>
						</li>
						

						<!--Category name-->
						<!--Interested Bidders -->
						<li>
							<a href="#">
								<i class="fa fa-user"></i>
									<span class="menu-title">Bidders</span>
								<i class="arrow"></i>
							</a>
							<!--Submenu-->
							<ul class="collapse">
								<li><a href="<?php echo base_url('auctioneer/bidders/interested_auction_bidders')?>"><i class="fa fa-thumbs-up"></i><span class="menu-title">Interested bidders</span></a></li>
								<li><a href="<?php echo base_url('auctioneer/bidders/invite_bidders');?>">Invite Bidders To Participate</a></li>
								<li><a href="<?php echo base_url('auctioneer/bidders/invite_to_associate_bidders');?>">Invite Bidders To Associate</a></li>
								<li><a href="<?php echo base_url('auctioneer/bidders/associate_bidder_list');?>">Associate Bidders List</a></li>
								<li><a href="<?php echo base_url('auctioneer/bidders/bidders_group_master');?>">Bidder's Group Master</a></li>

							</ul>
						</li>	
						<li>
							<a href="#">
								<i class="fa fa-users"></i>
								<span class="menu-title">Employee</span>
								<i class="arrow"></i>
							</a>
							<!--Submenu-->
							<ul class="collapse">
								<li><a href="<?php echo base_url('auctioneer/sub_user/view_users');?>">Employee List</a></li>
							</ul>
						</li>
						<li>
							<a href="#">
								<i class="fa fa-users"></i>
								<span class="menu-title">My Associate</span>
								<i class="arrow"></i>
							</a>
							<!--Submenu-->
							<ul class="collapse">
								<?php if ($this->session->userdata('registration_type') == 1){ ?>
									<li><a href="<?php echo base_url('auctioneer/my_associate/associate_vendor_list');?>">Associate Vendor List</a></li>
								<?php } ?>
								<?php if ($this->session->userdata('registration_type') == 0){ ?>
									<li><a href="<?php echo base_url('auctioneer/my_associate/associate_auctioneer_list');?>">Associate Auctioneer List</a></li>
									<li><a href="<?php echo base_url('auctioneer/my_associate/associate_auctioneer');?>">Associate Auctioneer</a></li>
								<?php } ?>
							</ul>
						</li>
						<li class="list-divider"></li>

								<li>
										<a href="#">
											<i class="fa fa-shopping-cart"></i>
											<span class="menu-title"> Permission </span>
											<i class="arrow"></i>
											
										</a>
										<ul class="collapse">

											<li>
												<a href="<?php echo base_url('auctioneer/sub_user/roles');?>">
													<i class="fa fa-user-plus" aria-hidden="true"></i>
													<span class="menu-title">Roles</span>
												</a>
											</li>
										</ul>
								</li>

						<!-- Vendor List -->

					
						<li class="list-divider"></li>
						<!--Category name-->
						<li class="list-header">Auction House</li>
						<li>
							<a href="#">
								<i class="fa fa-gavel"></i>
								<span class="menu-title">E-Tender</span>
								<i class="arrow"></i>
							</a>
							<!--Submenu-->
							<ul class="collapse">
								<li><a href="<?php echo base_url('auctioneer/tender/index');?>">All E-Tenders</a></li>
								<li><a href="<?php echo base_url('auctioneer/tender_reports/view_all_tender_reports');?>">All E-Tenders Reports</a></li>
							</ul>
						</li>
						<li>
							<a href="#">
								<i class="fa fa-gavel"></i>
								<span class="menu-title">Auction</span>
								<i class="arrow"></i>
							</a>
							<!--Submenu-->
							<ul class="collapse">
								<li><a href="<?php echo base_url('auctioneer/auction/index');?>">Auctions</a></li>
								<li><a href="<?php echo base_url('auctioneer/auction_reports/view_all_yankee_reports');?>">Auction Reports</a></li>
							</ul>
						</li>
						<li>
							<a href="#">
								<i class="fa fa-gavel"></i>
								<span class="menu-title">Salvage Auction</span>
								<i class="arrow"></i>
							</a>
							<!--Submenu-->
							<ul class="collapse">
								<li><a href="<?php echo base_url('auctioneer/salvage/index');?>">All Salvage Auctions</a></li>
								<li><a href="<?php echo base_url('auctioneer/salvage_auction_reports/view_all_salvage_reports');?>">All Salvage Auction Reports</a></li>
							</ul>
						</li>
						<li class="list-divider"></li>
						<!--Category name-->
						<li>
							<a href="#">
								<i class="fa fa-university"></i>
								<span class="menu-title">Add Company</span>
								<i class="arrow"></i>
							</a>
							<!--Submenu-->
							<ul class="collapse">
								<li><a href="<?php echo base_url('auctioneer/company_plant/company_branch_profile');?>">Company/Plant Details</a></li>
							</ul>
						</li>
						<li class="list-divider"></li>
						<li>
							<a href="<?php echo base_url('auctioneer/messages')?>">
								<i class="fa fa-dashboard"></i>
								<span class="menu-title">
									<strong>Messages</strong>												
								</span>
							</a>
						</li>
						<li>
							<a href="#">
								<i class="fa fa-gavel"></i>
								<span class="menu-title">Utility</span>
								<i class="arrow"></i>
							</a>
							<!--Submenu-->
							<ul class="collapse">
								<li><a href="<?php echo base_url('auctioneer/dashboard/theme');?>">Theme Settings</a></li>
								<li><a href="<?php echo base_url('auctioneer/dashboard/upload_logo');?>">Upload Logo</a></li>
								<li><a href="<?php echo base_url('auctioneer/dashboard/system_logs');?>">System Logs</a></li>
								<li><a href="<?php echo base_url('auctioneer/auction/upload_common_documents');?>">Upload Common Documents</a></li>
							</ul>
						</li>
					</ul>
				</div>
			</div>
		</div>
		<!--================================-->
		<!--End menu-->
	</div>
</nav>
<!--===================================================-->
<!--END MAIN NAVIGATION-->
