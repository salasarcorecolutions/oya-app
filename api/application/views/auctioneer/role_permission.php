<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Role Permission Set</title>
</head>
<body>
	<div id="container" class="effect mainnav-lg">
		<div class="boxed">
			<!--CONTENT CONTAINER-->
			<!--===================================================-->
			<div id="content-container">

				<!--Page Title-->
				<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
				<div id="page-title"><h1 class="page-header text-overflow">Role Permission Set of <?php echo $role_name; ?></h1></div>
				<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
				<!--End page title-->
				<!--Breadcrumb-->
				<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
				<ol class="breadcrumb">
					<li><a href="<?php echo base_url()?>dashboard">Home</a></li>
					<li class="active">Role Permission Sets</li>
				</ol>
				<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
				<!--End breadcrumb-->
				<!--Page content-->
				<!--===================================================-->
				<div id="page-content">

					<div class="row">
						<div class="col-lg-12">
							<!--Invoice table-->
							<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
							<div class="panel">
								<div class="panel-heading">
									<div class="panel-control">
										<a class="fa fa-question-circle fa-lg fa-fw unselectable add-tooltip" href="#" data-original-title="<h4 class='text-thin'>Information</h4><p style='width:150px'>This master used for creating religions.</p>" data-html="true" title=""></a>
									</div>
									<h3 class="panel-title">Role Permission of <?php echo $role_name; ?></h3>
								</div>
								<div class="panel-body">
									<div class="table-responsive">
									<?php /*------ Added Select All Checkbox --------------------------*/ ?>
									<label><input name='checkall' type='checkbox' value='' class='selectall'
									<?php foreach($modules as $items):?>
									value="<?php $items['permission_sets']; ?>"
									<?php endforeach ?>
									/> Give All Permissions</label>
									<?php /*------------------------------------------------------------*/ ?>
										<table class="table table-bordered table-hover" id="cTable">
											<thead>
												<tr>

													<th width="20%">Main Head</th>
													<th width="20%">Sub Head[CODE]</th>
													<th width="60%">Permission Sets</th>
												</tr>
											</thead>
											<tbody>
											<?php foreach($modules as $items):?>
												<tr>
													<td><?php echo $items["head_name"];?></td>
													<td>
														<label><input type="checkbox" name="checkbox1" class="child_checkbox1"/>&nbsp;
														<?php echo $items["sub_head_name"];?>[<?php echo $items["sub_head_code"];?>]</label>
													</td>
													<td class="sub_head">
													<?php foreach($items['permission_sets'] as  $value):?>
												
														<label><input type="checkbox" <?php echo in_array($value['permission_id'], $assigned_permission)?' checked ':'';?> value="<?php echo $value['permission_id']?>" name="checkbox11"
														class="child_checkbox11"/> &nbsp;<?php echo $value['permission_name']?></label>
														<br />
													<?php endforeach ?>
													</td>
												</tr>
												 <?php endforeach ?>
											</tbody>
										</table>
									</div>
								</div>
							</div>
							<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
							<!--End Invoice table-->
						</div>
					</div>
				</div>
				<!--===================================================-->
				<!--End page content-->
			</div>
			<!--===================================================-->
			<!--END CONTENT CONTAINER-->
			<!--MAIN NAVIGATION-->
			<!--===================================================-->
		</div>
		<!--END BOXED DIV-->
	</div>
	<!--JAVASCRIPT-->
	<!--=================================================-->

	<script>
	$(document).ready(function() {
		/*-----------------------------Give all permission checkbox code start------------------------------*/
			$(".selectall").click(function () {
				$('.selectall').prop('checked', this.checked);
				$('.child_checkbox1').prop('checked', this.checked);
				$('.child_checkbox11').prop('checked', this.checked);
				$(".child_checkbox11").each(function(){
					$(this).change();
				});
			});
			
		/*------------------------- submodule checkbox selection------------------------------------------*/
			$(".child_checkbox1").change(function () {
				if($(this).is(":checked")){
					$(this).parent("label").parent("td").parent("tr").find("td.sub_head>label>.child_checkbox11").each(function( index ) {
						$(this).prop('checked', true).change();
					});
				}else{
					$(this).parent("label").parent("td").parent("tr").find("td.sub_head>label>.child_checkbox11").each(function( index ) {
						$(this).prop('checked', false).change();
					});
				}
			}); 
		/*---------------------- submodule checkbox selection end---------------------------------------*/

    $('.child_checkbox11').change(function() {
		if($(this).is(":checked"))
			action="add";
		else
			action="remove";
			$.post("<?php echo base_url();?>auctioneer/sub_user/update_permission",
			{
				permission_id: $(this).val(),
				role_id: <?php echo $role_id;?>,
				act: action,
			},
	        function(data, status){
                $.niftyNoty({
                    type: 'success',
                    container : 'floating',
                    html : '<h4 class="alert-title">Success</h4><p class="alert-message">'+data+'</p>',
                    closeBtn : false,
                    timer: 4000
                });
	        });
	});
});
	</script>
	</body>
</html>
