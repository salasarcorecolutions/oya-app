<style>
	.cke_toolbar_break{
		display: none;
	}
	.text-danger i {
		padding-left: 10px;
	}
	.control-label{
		color: #25476a;
		font-weight: bold;
	}
</style>
<!--CONTENT CONTAINER-->
<!--===================================================-->
<div id="content-container">

<!--Page Title-->
<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
<div id="page-title">
	<h1 class="page-header text-overflow"><?php echo $pagetitle;?></h1>
</div>
<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
<!--End page title-->

<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
<!--End breadcrumb-->
	<div id="page-content">
		<div class="panel">
			<div class="panel-heading">
				<h3 class="panel-title col-sm-5"><?php echo $pagetitle;?></h3>
			</div>
			<div class="panel-body">
				<form name="frm" id="frm">
				  <fieldset class="col-md-12">
						<legend>Generate Invitation</legend>
						<div class="panel panel-default">
							<div class="panel-body">
								<div class="col-sm-6">
									<div class="form-group">
										<label class="control-label">Email To<span class="text-danger"><i class="fa fa-asterisk" aria-hidden="true"></i></span></label>
										<small>(For Multiple Email Ids add comma seprated email ids.)</small>
										<textarea class="form-control" name="email_to" id="email_to" onchange="get_msg()"></textarea>
									</div>
								</div>
								<div class="col-sm-6">
									<div class="form-group">
										<label class="control-label">Link Validity<span class="text-danger"><i class="fa fa-asterisk" aria-hidden="true"></i></span></label>
										<div class="row">
										<div class="col-sm-4">
											<input type="text" name="link_validity" id="link_validity" class="form-control" placeholder="eg.10" onchange='get_validity()'/>
										</div>
										<div class="col-sm-8">
											<select id="duration" name="duration"  class="form-control addSelect2" onchange='get_validity()' style="width:100%">
												<option value="Minutes">Min</option>
												<option value="Hours">Hours</option>
												<option value="Day">Day</option>
												<option value="Month">Month</option>
											</select>
										</div>
										</div>
									</div>
								</div>
								<div class="col-sm-12">
									<div class="form-group">
										<label class="control-label">Message<span class="text-danger"><i class="fa fa-asterisk" aria-hidden="true"></i></span></label>
										<textarea  class='form-control ckeditor' id="message" placeholder="Message" name='message'></textarea>
									</div>
								</div>
								<input type='hidden' name='validity_link' id='validity_link'/>
								<div class="col-sm-12" style="text-align: center">
									<button class="btn btn-success" type="button" onclick="submit_form()">Send</button>
								</div>
							</div>
						</div>
					</fieldset>
				</form>
			</div>
		</div>

	</div>
</div>
<!--===================================================-->
<!--END CONTENT CONTAINER-->
<style type='text/css'>
.table>tbody>tr>td,
.table>tbody>tr>th,
.table>tfoot>tr>td,
.table>tfoot>tr>th, 
.table>thead>tr>td,
.table>thead>tr>th
{
	padding: 4px !important;
}
fieldset
{
	border: 1px solid #ddd !important;
	margin: 0;
	padding: 10px;
	position: relative;
	border-radius:4px;
	background-color:#f5f5f5;
	padding-left:10px!important;
}

legend
{
	font-size:14px;
	font-weight:bold;
	margin-bottom: 0px;
	width: 35%; 
	border: 1px solid #ddd;
	border-radius: 4px;
	padding: 5px 5px 5px 10px; 
	background-color: #ffffff;
}
</style>

<script src="https://cdn.ckeditor.com/4.14.0/standard/ckeditor.js"></script>

<script type='text/javascript'>
$(document).ready(function(){
get_msg();

});

 
function get_validity()
{
	if ($("#link_validity").val()!='' && $('#duration').val() !='')
	{
		
		$.ajax({
			url:'<?php echo base_url('auctioneer/bidders/get_validity')?>',
			type:'POST',
			data:{'link_validity':$('#link_validity').val(),'duration':$('#duration').val()},
			success:function(data)
			{
				$("#validity_link").val(data);
				$(".link_validity").html('** This link will expire on ');
				get_msg();
			}
			
		});
	}

}
function base64url_encode($data)
{
  // First of all you should encode $data to Base64 string
  var b64 = btoa($data);

  // Make sure you get a valid result, otherwise, return FALSE, as the base64_encode() function do
  if (b64 === false) {
    return false;
  }

  var url = b64;
  return url;
  
}


function get_msg()
{
    var email = $("#email_to").val();
    var vendor_id = <?php echo $this->session->userdata('vendor_id')?>;
    var expire_date = $("#validity_link").val();
    var string = email+'/'+vendor_id+'/'+expire_date;
    var encode_string = base64url_encode(string);
    var msg = '<p> Dear User ,</p>';
    msg+='<p> You are invited to associate with company  </p>';
    msg+='<p> Please click on below link To associate with company </p>';
    $("#message").html(msg);
}

function submit_form()
{
	var form = $("#frm");
    form.validate({
        ignore:[],
        rules: {
            email_to : "required",
            link_validity:"required",
            duration:"required",
        },
        messages: {
            email_to : "<b class='text-danger'>Enter Email</b>",
            duration : "<b class='text-danger'>Enter Duration</b>",
            link_validity : "<b class='text-danger'>Enter Validity</b>",
        }
    });

    if(form.valid())
    {
        $('#frm').ajaxSubmit({
            url:"<?php echo base_url();?>auctioneer/bidders/send_associate_request_to_buyer",
            type: 'post',
            dataType:'json',
            success: function (response)
            {
                if (response.status=="success"){
					Display_msg(response.msg,response.status);

                    window.location.reload();
                } else {
                    $('#opMessage').show();
					Display_msg(response.msg,response.status);
                    $('#opMessage').html('<div class="alert alert-danger fade in"><button data-dismiss="alert" class="close"><span>×</span></button><strong>Oh!</strong> '+response.msg+'</div>');
                    $('#opMessage').delay(4000).fadeOut();
                }
            }
        });
    }
}

</script>
