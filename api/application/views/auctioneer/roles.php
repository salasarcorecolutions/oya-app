<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Roles</title>
</head>

<body>
	<div id="container" class="effect mainnav-lg">
		<div class="boxed">
			<!--CONTENT CONTAINER-->
			<!--===================================================-->
			<div id="content-container">

				<!--Page Title-->
				<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
				<div id="page-title">
					<h1 class="page-header text-overflow">Roles</h1>
				</div>
				<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
				<!--End page title-->
				<!--Breadcrumb-->
				<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
				<ol class="breadcrumb">
					<li><a href="<?php echo base_url()?>dashboard">Home</a></li>
					<li class="active">Roles</li>
				</ol>
				<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
				<!--End breadcrumb-->
				<!--Page content-->
				<!--===================================================-->
				<div id="page-content">
					<div class="row">
						<div class="col-lg-12">
							<!--Invoice table-->
							<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
							<div class="panel">
								<div class="panel-heading">
									<div class="panel-control">
										<a class="fa fa-question-circle fa-lg fa-fw unselectable add-tooltip" href="#" data-original-title="<h4 class='text-thin'>Information</h4><p style='width:150px'>This master used for creating religions.</p>" data-html="true" title=""></a>
									</div>
									<h3 class="panel-title">Roles</h3>
								</div>
								<div class="panel-body">
									<div class="pad-btm form-inline">
										<div class="row">
											<div class="col-sm-6 table-toolbar-left">
												<?php
												if ($this->common_model->check_permission('Role Add'))
												{
													?>
													<button class="btn btn-purple btn-labeled fa fa-plus" data-toggle="modal" data-target="#Modal_Religion">Add</button>
													<?php
												}
												?>
											</div>
										</div>
									</div>
									<div class="table-responsive">
										<table class="table table-bordered table-hover" id="cTable">
											<thead>
												<tr>
													<th>Role Name</th>
													<th>Description</th>
													<th>Users</th>
													<th>Updated By</th>
													<th class="text-right" style="width:30px">Actions</th>
												</tr>
											</thead>
											<tbody>
												<?php foreach($roles as $items):?>
												<tr id="T<?php echo $items["role_id"];?>">
													<td><?php echo $items["role_name"];?></td>
													<td><?php echo $items["role_desc"];?></td>
													<td>
														<a data-container="body"  data-id="<?php echo $items["role_id"];?>" data-original-title="Add" href="#" data-toggle="tooltip" class="btnAddUser btn btn-xs btn-purple add-tooltip"><i class="fa fa-plus "></i></a>
														<?php foreach($items["role_users"] as $users):?>
                                                            <a data-container="body"  emp_id="<?php echo $users["id"];?>" role_id="<?php echo $items["role_id"];?>"  data-original-title="Delete from Group" href="#" data-toggle="tooltip" class="btnDeleteUser btn btn-success btn-rounded add-tooltip"> <?php echo $users["vendor_name"];?> <i class="fa fa-times"></i></a>
                                                        <?php endforeach ?>
												</td>
												<td><?php echo @$items["updated_by"];?></td>
												<td class="text-right" nowrap>
													<?php
													if ($this->common_model->check_permission('Role Edit'))
													{
														?>
															<a data-container="body"  data-id="<?php echo $items["role_id"];?>" data-value="<?php echo $items["role_name"];?>" data-desc="<?php echo $items["role_desc"];?>" data-original-title="Edit" href="#" data-toggle="tooltip" class="btnEdit btn btn-xs btn-default add-tooltip"><i class="fa fa-pencil"></i></a>
														<?php
													}
													?>
													<?php
													if($this->common_model->check_permission('Role Delete'))
													{
														?>
															<a data-container="body"  data-id="<?php echo $items["role_id"];?>" data-original-title="Delete" href="#" data-toggle="tooltip" class="btnDelete btn btn-xs btn-danger add-tooltip"><i class="fa fa-times"></i></a>
														<?php
													}
													?>
													<?php
													if($this->common_model->check_permission('Sub User Roles Permission'))
													{
														?>
														<a data-container="body"  data-id="<?php echo $items["role_id"];?>" data-original-title="Set Permissions" href="<?php echo base_url();?>auctioneer/sub_user/permissions/<?php echo $items["role_id"];?>" data-toggle="tooltip" class="btn btn-xs btn-danger add-tooltip"><i class="fa fa-list"></i></a>
													<?php } ?>
												</td>
											</tr>
										<?php endforeach ?>
									</tbody>
								</table>
							</div>
						</div>
					</div>
					<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
					<!--End Invoice table-->

				</div>
			</div>
		</div>
		<!--===================================================-->
		<!--End page content-->
	</div>
	<!--===================================================-->
	<!--END CONTENT CONTAINER-->

	<!--MAIN NAVIGATION-->
	<!--===================================================-->
</div>
<!--END BOXED DIV-->
</div>
<div class="modal" id="Modal_Religion" role="dialog" tabindex="-1" aria-labelledby="demo-default-modal" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<!--Modal header-->
			<div class="modal-header">
				<button data-dismiss="modal" class="close" type="button">
					<span aria-hidden="true">&times;</span>
				</button>
				<h4 class="modal-title">Roles</h4>
			</div>
			<!--Modal body-->
			<div class="modal-body">
				<div class="panel">
					<div id="opMessage"></div>
					<!--Horizontal Form-->
					<!--===================================================-->
					<form class="form-horizontal" id="form-sal_br">
						<div class="panel-body">
							<div class="form-group">
								<label for="role_name" class="col-sm-5 control-label">Roles Name</label>
								<div class="col-sm-7">
									<input type="text" class="form-control" id="role_name" name="role_name"
									placeholder="Role Name">
								</div>
							</div>
							<div class="form-group">
								<label for="role_name" class="col-sm-5 control-label">Roles Location</label>
								<div class="col-sm-7">
									<input type="text" class="form-control" id="role_desc" name="role_desc"
									placeholder="Description">
								</div>
							</div>
						</div>
					</form>
					<!--===================================================-->
					<!--End Horizontal Form-->
				</div>
			</div>
			<!--Modal footer-->
			<div class="modal-footer">
				<button data-dismiss="modal" class="btn btn-default" type="button">Close</button>
				<button class="btn btn-primary" id="btnSave">Save changes</button>
			</div>
		</div>
	</div>
</div>

<div class="modal" id="Modal_Role_User" role="dialog" tabindex="-1" aria-labelledby="demo-default-modal" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">

			<!--Modal header-->
			<div class="modal-header">
				<button data-dismiss="modal" class="close" type="button">
					<span aria-hidden="true">&times;</span>
				</button>
				<h4 class="modal-title">Add User</h4>
			</div>
			<!--Modal body-->
			<div class="modal-body">
				<div class="panel">
					<div id="msgUser"></div>
					<!--Horizontal Form-->
					<!--===================================================-->
				<form class="form-horizontal" id="form-user">
						<input name="add_role_id" id="add_role_id" type="hidden" />
						<div class="panel-body">
							<div class="form-group">
								<label for="emp_id" class="col-sm-5 control-label">Select User</label>
								<div class="col-sm-7">
									<select class="form-control Select-default" id="emp_id" name="emp_id">
										<option value="">-- All --</option>
										<?php foreach($users_list as $items):?>
										<option value="<?php echo $items["id"];?>" ><?php echo $items["c_name"];?></option>
									<?php endforeach ?>
								</select>
							</div>
						</div>
					</div>
				</form>
			</div>
		</div>
		<!--Modal footer-->
		<div class="modal-footer">
			<button data-dismiss="modal" class="btn btn-default" type="button">Close</button>
			<button class="btn btn-primary" id="btnUser">Save changes</button>
		</div>
	</div>
</div>
</div>

<!--JAVASCRIPT-->
<!--=================================================-->
<script>
var data_op="add";
var object_id='';

$(".btnEdit").click(function(e){
	e.preventDefault();
	var data_id=$(this).attr("data-id");
	$('#role_name').val($(this).attr("data-value"));
	$('#role_desc').val($(this).attr("data-desc"));
	object_id=$(this).attr("data-id");
	data_op='edit';
	$('#Modal_Religion').modal('show');
});
$(".btnAddUser").click(function(e){
	e.preventDefault();

	$('#add_role_id').val($(this).attr("data-id"));
	$('#Modal_Role_User').modal('show');
});

$(".btnDelete").click(function(e){
	e.preventDefault();
	var ans=confirm("Are you sure to delete?");
	if(ans)
	{
		$.post("<?php echo base_url();?>auctioneer/sub_user/role_op",
		{
			object_id: $(this).attr("data-id"),
			data_op: 'del'

		},
		function(data, status){
			$.niftyNoty({
				type: 'success',
				container : 'floating',
				html : '<h4 class="alert-title">Success</h4><p class="alert-message">'+data+'</p>',
				closeBtn : false,
				timer: 4000
			});
			location.reload();
		});
	}
});

$("#btnSave").click(function(e){
	e.preventDefault();
	var form=$("#form-sal_br");
	form.validate(
	{
		ignore:[],
		rules: {
			role_name: "required",
			role_desc: "required"
		},
		messages: {
			role_name: "<b class='text-danger'>Please enter your Role Name</b>",
			role_desc: "<b class='text-danger'>Please enter Description</b>"

		}
	});
	if(form.valid())
	{
		$.post("<?php echo base_url();?>auctioneer/sub_user/role_op",
		{
			role_name: $('#role_name').val(),
			role_desc: $('#role_desc').val(),
			data_op: data_op,
			object_id:object_id

		},
		function(data, status){
			$('#opMessage').html('<div class="alert alert-danger fade in"><button data-dismiss="alert" class="close"><span>�</span></button><strong>Oh!</strong> '+data+'</div>');
			$.niftyNoty({
				type: 'success',
				container : 'floating',
				html : '<h4 class="alert-title">Success</h4><p class="alert-message">'+data+'</p>',
				closeBtn : false,
				timer: 4000
			});
			location.reload();
		});
	}
});
$("#btnUser").click(function(e){
	e.preventDefault();
	var form = $("#form-user");
	form.validate({
		ignore:[],
		rules: {
			emp_id: "required"
		},
		messages: {
			emp_id: "<b class='text-danger'>Please Select user</b>"
		}
	});
	if (form.valid())
	{
		$.post("<?php echo base_url();?>auctioneer/sub_user/add_user_role",
		{
			user_id: $('#emp_id').val(),
			role_id: $('#add_role_id').val()
		},
		function(data, status){
			$('#msgUser').html('<div class="alert alert-danger fade in"><button data-dismiss="alert" class="close"><span>�</span></button><strong>Oh!</strong> '+data+'</div>');
			$.niftyNoty({
				type: 'success',
				container : 'floating',
				html : '<h4 class="alert-title">Success</h4><p class="alert-message">'+data+'</p>',
				closeBtn : false,
				timer: 4000
			});
            location.reload();
        });
	}
});
$(".btnDeleteUser").click(function(e){
	e.preventDefault();
	var ans = confirm("Are you sure to remove from role ?");
	if (ans)
	{
		$.post("<?php echo base_url();?>auctioneer/sub_user/user_role_remove",
		{
			user_id: $(this).attr("emp_id"),
			role_id: $(this).attr("role_id")
		},
		function(data, status){
			$.niftyNoty({
				type: 'success',
				container : 'floating',
				html : '<h4 class="alert-title">Success</h4><p class="alert-message">'+data+'</p>',
				closeBtn : false,
				timer: 4000
			});
			location.reload();
		});
	}
});

</script>
</body>
</html>
