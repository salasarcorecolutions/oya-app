<style>
	.control-label{
		color: #25476a;
		font-weight: bold;
	}
	.cke_toolbar_break{
		display: none;
	}
	.text-danger i {
		padding-left: 10px;
	}
</style>
<!--CONTENT CONTAINER-->
<!--===================================================-->
<div id="content-container">

<!--Page Title-->
<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
<div id="page-title">
	<h1 class="page-header text-overflow"><?php echo $pagetitle;?></h1>
</div>
<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
<!--End page title-->

<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
<!--End breadcrumb-->
	<div id="page-content">
		<div class="panel">
			<div class="panel-heading">
				<h3 class="panel-title col-sm-5"><?php echo $pagetitle;?></h3>
			</div>
			<div class="panel-body">
				<form name="frm" id="frm">
				  	<fieldset class="col-md-12">
						<legend>Generate Invitation</legend>
						<div class="panel panel-default">
							<div class="panel-body">
								<div class="col-sm-4">
									<div class="form-group">
										<label class="control-label">Auction Type<span class="text-danger"><i class="fa fa-asterisk" aria-hidden="true"></i></span></label>
										<select id="auction_type" name="auction_type" class="form-control addSelect2" style="width:100%"  >
											<option value="5">E- Tender</option>
											<option value="4">Auction</option>
										</select>
									</div>
								</div>
								<div class="col-sm-4">
									<div class="form-group">
										<label class="control-label">Auction Saleno<span class="text-danger"><i class="fa fa-asterisk" aria-hidden="true"></i></span></label>
										<select id="saleno" name="saleno" class="form-control addSelect2" style="width:100%" onchange="get_saleno(this.value)">
										</select>
									</div>
								</div>

								<div class="col-sm-4">
									<div class="form-group">
										<label class="control-label">Lot No<span class="text-danger"><i class="fa fa-asterisk" aria-hidden="true"></i></span></label>
										<select id="lotno" name="lotno[]"  class="form-control addSelect2" style="width:100%" multiple >
											<option value="">--Select Lot--</option>
										</select>
									</div>
								</div>
								
							
								<div style="padding: 0px" class="form-group col-sm-4">
									<label class="col-sm-4 control-label">Link Validity <span class="text-danger"><i class="fa fa-asterisk" aria-hidden="true"></i></span></label>
									<div class="col-sm-4">
										<input type="text" name="link_validity" id="link_validity" class="form-control" placeholder="eg.10" onchange='get_validity()'/>
									</div>
									<div class="col-sm-4">
										<select id="duration" name="duration"  class="form-control addSelect2" onchange='get_validity()' style="width:100%">
											<option value="Minutes">Min</option>
											<option value="Hours">Hours</option>
											<option value="Day">Day</option>
											<option value="Month">Month</option>
										</select>
									</div>
								</div>
								<div style="padding: 0px" class="form-group col-sm-8">
									<label class="col-sm-3 control-label">Invitation Send To <span class="text-danger"><i class="fa fa-asterisk" aria-hidden="true"></i></span></label>
									<div class="col-sm-9">
										<select class='form-control addSelect2' id="compname" name='compname'>
											<?php foreach($bidder_details as $value){ ?>
												<option value="<?php echo $value['bidder_id'] ?>"><?php echo $value['compname']; ?></option>
											<?php } ?>
										</select>
									</div>
								</div>
								<div class=" col-sm-12">
									<div class="form-group">
										<label class="control-label">Message<span class="text-danger"><i class="fa fa-asterisk" aria-hidden="true"></i></span></label>
										<textarea  class='form-control ckeditor' id="message" placeholder="Message" name='message'>
										</textarea>
									</div>
								</div>
								<input type='hidden' name='validity_link' id='validity_link'/>
								<input type='hidden' name='auc_saleno' id='auc_saleno'/>
								<div class="col-sm-6">
									<button class="btn btn-success pull-right" type="button" onclick="submit_form()">Send</button>
								</div>
								
							</div>
						</div>
					</fieldset>
					
				</form>
			</div>
		</div>

	</div>
</div>
<!--===================================================-->
<!--END CONTENT CONTAINER-->
<style type='text/css'>
.table>tbody>tr>td,
.table>tbody>tr>th,
.table>tfoot>tr>td,
.table>tfoot>tr>th, 
.table>thead>tr>td,
.table>thead>tr>th
{
	padding: 4px !important;
}
fieldset
{
	border: 1px solid #ddd !important;
	margin: 0;
	padding: 10px;
	position: relative;
	border-radius:4px;
	background-color:#f5f5f5;
	padding-left:10px!important;
}

legend
{
	font-size:14px;
	font-weight:bold;
	margin-bottom: 0px;
	width: 35%; 
	border: 1px solid #ddd;
	border-radius: 4px;
	padding: 5px 5px 5px 10px; 
	background-color: #ffffff;
}
</style>
<script src="https://cdn.ckeditor.com/4.14.0/standard/ckeditor.js"></script>
<script type='text/javascript'>
$(document).ready(function(){
	get_msg();
	$('.addSelect2').select2();
	$("#lotno").select2({
		placeholder: "Select Lot",
		allowClear: true
	});
	$('select#auction_type').trigger('change');
	$('select#compname').trigger('change');

	$('select#compname').change(function (event){
		get_msg();
	});

});
$('select#auction_type').change(function (event){
	loadlist($('select#saleno').get(0),'<?php echo base_url();?>auctioneer/bidders/get_auction/'+$('select#auction_type').val()+'/<?php echo $user_id?>','saleno','auction_id','-- Select Saleno --');
	get_msg();
});
function get_saleno(value) 
{
	var saleno = value;
	loadlist($('select#lotno').get(0),'<?php echo base_url();?>auctioneer/bidders/get_auction_lots/'+$('select#auction_type').val()+'/'+saleno+'','product','lotno','-- Select Lot No --');
	get_msg();
}
 function loadlist(selobj,url,nameattr,valueattr,selecthtml)
{
	$(selobj).empty();
	$(selobj).append($('<option></option>').val('').html(selecthtml));
	$.getJSON(url,{},function(data)
	{
		$.each(data, function(i,obj)
		{
			$(selobj).append($('<option></option>').val(obj[valueattr]).html(obj[nameattr]));
		});
	});
}
function base64url_encode($data)
{
  // First of all you should encode $data to Base64 string
  var b64 = btoa($data);
  var url='';
  // Make sure you get a valid result, otherwise, return FALSE, as the base64_encode() function do
  if (b64 === false) {
    return false;
  }

  // Convert Base64 to Base64URL by replacing “+” with “-” and “/” with “_”
  
  url = b64.replace("=", "");
  return url;

}

function get_validity()
{
	if ($("#link_validity").val()!='' && $('#duration').val() !='')
	{
		$.ajax({
			url:'<?php echo base_url('auctioneer/bidders/get_validity')?>',
			type:'POST',
			data:{'link_validity':$('#link_validity').val(),'duration':$('#duration').val()},
			success:function(data)
			{
				$("#validity_link").val(data);
				$(".link_validity").html('** This link will expire on ');

			}
			
		});
	}

}
function get_msg()
{
  var msg='';
  var auction_id = $("#saleno").val();
  var saleno = $( "#saleno option:selected" ).text();
  $('#auc_saleno').val(saleno);
  var lotno = $("#lotno").val();
  var compname = $("#compname").val();
  var auction_type = $("#auction_type").val();
  var expire_time = $("#link_validity").val();
  var duration = $("#duration").val();
  var vendor_id ='<?php echo $this->session->userdata('vendor_id')?>';
  var expire_date = $("#validity_link").val();
  var bidder_id = '<?php echo $this->session->userdata('bidder_id')?>';
  var string = auction_id+'/'+auction_type+'/'+vendor_id+'/'+expire_date+'/'+lotno+'/'+compname+'';
  var encode_string = base64url_encode(string);
  var msg = '<p> Dear User ,</p>';
  msg+='<p> We are inviting you to bid for an auction </p>';
  msg+='<p> If You are interested to bid please click on below link To see auction details </p><br/>';
  msg+='<p> <a href="<?php echo base_url()?>home/view_details/'+encode_string+'" target="_blank"> Click On Link</a></p>';
  
  $("#message").html(msg);

}
function submit_form()
{
	var form = $("#frm");
	form.validate({
		ignore:[],
		rules: {
			auction_type : "required",
			saleno : "required",
			'lotno[]' : "required",
			link_validity:"required",
			duration:"required",
			compname:"required",
			message:"required"
		},
		messages: {
			auction_type : "<b class='text-danger'>Enter Type</b>",
			saleno : "<b class='text-danger'>Enter Saleno</b>",
			'lotno[]' : "<b class='text-danger'>Enter Lot No</b>",
			compname : "<b class='text-danger'>Enter Company Name</b>",
			message : "<b class='text-danger'>Enter Message</b>",
			duration : "<b class='text-danger'>Enter Duration</b>",
			link_validity : "<b class='text-danger'>Enter Validity</b>",
		}
	});
	if(form.valid())
	{
		$('#frm').ajaxSubmit({
			url:"<?php echo base_url();?>auctioneer/bidders/send_email_to_buyer",
			type: 'post',
			dataType:'json',
			success: function (response){
				if (response.status=="success"){
					$.niftyNoty({
						type: 'success',
						container : 'floating',
						html : '<h4 class="alert-title">Success</h4><p class="alert-message">'+response.msg+'</p>',
						closeBtn : false,
						timer: 7000
					});
					window.location.reload();
				} else {
					$('#opMessage').show();
					$('#opMessage').html('<div class="alert alert-danger fade in"><button data-dismiss="alert" class="close"><span>×</span></button><strong>Oh!</strong> '+response.msg+'</div>');
					$('#opMessage').delay(4000).fadeOut(); 						
				}
			}
		});
		
	}
}

</script>
