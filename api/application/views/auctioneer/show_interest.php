<script language="javascript" type="text/javascript" src="<?php echo base_url('auctioneer_assets/js/ajax.js'); ?>"></script>
<style>

  a.btn{
	 width: 100%;
	  text-align:left;
	  margin-top:5px;
  }
  .btn-group{
		  margin-left:19px;
	  }
	.fa-upload{
		margin-left:5px !important;
	}
	.auction-msg{
		float:right !important;
	}
	.auction-live{
		float:right !important;
	}
	.btn-group:first-child{
		margin-left:0px;
	}
  @media (max-width:990px){
	  a.btn{
		  margin-top:10px;
		  width:250px;
	  }
	  .btn-group{
		  margin-left:0;
	  }
	  .fa-upload{
		margin-left:0 !important;
	  }
	  .auction-msg{
		float:left !important;
	}
	.auction-live{
		float:left !important;
	}
  }
</style>

<div id="content-container">
	<div id="page-title">
		<h1 class="page-header text-overflow">Interested Auction Bidder</h1>
	</div>
	<ol class="breadcrumb">
		<li><a href="<?php echo base_url('dashboard'); ?>">Home</a></li>
		<li class="active">Interested Auction Bidder</li>
	</ol>
	<div id="page-content">	
		<div class="panel">
			<div class="panel-heading">
				<h3 class="panel-title">Interested Auction Bidder</h3>
			</div>
			
			<div class="panel-body">
				<div class="pad-btm form-inline">
				<div class="row">
					
				</div>
				<form name="fwd_auction_form" method="post" >
					<div class="row filter">
						<div class="col-sm-2">
							<input class="form-control searchInput" placeholder="Sale No" type="text" name="search_sale_no" id="search_sale_no" value="" />
						</div>
						<div class="col-sm-2">
							<input class="form-control searchInput" placeholder="Bidder Name" type="text" name="search_bidder_name" id="search_bidder_name" value="" />
						</div>
						<div class="col-sm-2">
						</div>
						<div class="col-sm-2">
						</div>

						<div class="col-sm-2">
							<a class="btn btn-success btn-labeled fa fa-refresh " onclick="refresh_datatable();">Refresh </a>
						</div>
						<div class="col-sm-2">
							<a class="btn btn-danger btn-labeled fa fa-ban" onclick="clearSearchFilters();">Clear Search </a>
						</div>
					</div>
				
					<div class="table-responsive">
						<table callfunction="<?php echo base_url('auctioneer/bidders/interested_bidder_ajax');?>" class="table table-striped table-bordered table-hover no-footer dtr-inline dynamicTable" noofrecords="10">
							<thead>
								<tr>
									<th data-bSortable="false">#</th>
									<th>Bidder Name</th>
									<th>Company Name</th>
									<th>Contact No.</th>
									<th>Email</th>
									<th>Auction No.</th>
									<th>Auction Lot ID.</th>
									<th>Auction Type</th>
									<th>Approval</th>
								</tr>
							</thead>
							<tbody>
							</tbody>
							<input type="hidden" id="vendorid" name="vendorid">
						</table>
					</div>

				</form>
			</div>	
		</div>
	</div>	
</div>
<script>
    function allow_bidder(auction_id, auction_type, bidder_id,lot_id,vendor_id)
	{
		var vendor_id = "<?php echo $vendor_id;?>";
		$.ajax({
			url: "<?php echo base_url();?>auctioneer/bidders/allow_interested_bidder",
			type: "POST",
			dataType:"json",
			data:
			{
				'vendor_id': vendor_id,
				'auction_id': auction_id,
				'auction_type': auction_type,
				'bidder_id': bidder_id,
				'lot_id':lot_id
			},
			success: function(response)
			{
				Display_msg(response.message,response.status);
				refresh_datatable();
			}
		});
	}
    function deny_bidder(auction_id, auction_type, bidder_id,lot_id,vendor_id)
	{
		var vendor_id = "<?php echo $vendor_id;?>";
		$.ajax({
			url: "<?php echo base_url();?>auctioneer/bidders/deny_interested_bidder",
			type: "POST",
			dataType:"json",
			data:
			{ 
				'vendor_id': vendor_id,
				'auction_id': auction_id,
				'auction_type': auction_type,
				'bidder_id': bidder_id,
				'lot_id':lot_id
			},
			success: function(response)
			{
				Display_msg(response.message,response.status);
				refresh_datatable();
			}
		});
	}

</script>