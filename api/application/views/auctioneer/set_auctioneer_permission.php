<!--CONTENT CONTAINER-->
<!--===================================================-->
<div id="content-container">
	<!--Page Title-->
	<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
	<div id="page-head">
	<div id="page-title">
		<h1 class="page-header text-overflow"><?php echo $pagetitle; ?></h1>
	</div>
	<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
	<!--End page title-->

	<!--Breadcrumb-->
	<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
	<ol class="breadcrumb">
		<li><a href="<?php echo base_url('dashboard');?>">Home</a></li>
		<li class="active"><?php echo $pagetitle; ?></li>
	</ol>
	</div>
	<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
	<!--End breadcrumb-->
	<input type="hidden" id="t11" name="auctionId">
	<div id="page-content">	
		<div class="float_btn">
			<a class="btn btn-primary btn-icon btn-circle add-tooltip" data-placement="top" data-toggle="tooltip" data-original-title="Back" href='<?php echo base_url('dashboard'); ?>' ><i class="fa fa-arrow-left" aria-hidden="true"></i></a>
		</div>
		<div class="panel">
			<div class="panel-heading">
				<h3 class="panel-title"><?php echo $pagetitle; ?></h3>
			</div>
			<div class="panel-body">
                <?php
                    $checked = '';
                    foreach ($permission_list as $value){
                    if ( ! empty($auctioneer_permission))
                    {
                        foreach ($auctioneer_permission as $ap)
                        {
                            if ($ap['permission_id'] == $value['permission_id'])
                            {
                                $checked = 'checked';
                                break;
                            }
                            else
                            {
                                $checked = '';
                            }
                        }
                    }
                    ?>
                        <div class="col-lg-3">
                            <label class='col-lg-12'>
                                <div class="col-lg-2">
                                    <input class="permission_check" type="checkbox" value="<?php echo $value['permission_id']; ?>" <?php echo $checked; ?> />
                                </div>
                                <div class="col-lg-10">
                                    <p class="text-15px mar-no text-semibold text-dark"><?php echo $value['permission_name']; ?></p>
                                </div>
                            </label>
                        </div>
                <?php } ?>
			</div>
		</div>
	</div>
</div>

<!--===================================================-->
<!--END CONTENT CONTAINER-->
<script>
    $(".permission_check").click(function(){
        var checked;
        if ($(this).prop("checked") == true){
            checked = 1;
        } else {
            checked = 0;
        }
        $.ajax({
            url: "<?php echo base_url('auctioneer/my_associate/update_auctioneer_permission'); ?>",
            type: "POST",
            dataType:"json",
            cache: false,
            clearForm: false,
            data: {
                'auctioneer_id' :"<?php echo $auctioneer_id; ?>",
                'permission_id': $(this).val(),
                'permission': checked
            },
            success:function(response){
                Display_msg(response.message,response.status);
            },
            error: function(){
                Display_msg('Problem in giving permission to auctioneer. Please Try Again','failed');
            }
        });
    });

</script>