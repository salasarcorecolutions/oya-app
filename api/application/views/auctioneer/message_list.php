<!--CONTENT CONTAINER-->
<!--===================================================-->

<div id="content-container">

<!--Page Title-->
<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
<div id="page-title">
	<h1 class="page-header text-overflow"><?php echo $pagetitle; ?></h1>
</div>
<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
<!--End page title-->

<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
<!--End breadcrumb-->
	<div id="page-content">
	<div class="panel panel-default panel-left">
	<div id="demo-email-list" class="panel-body">
				<div class="row">
					<div class="col-sm-7">

						<!-- Mail toolbar -->
						<!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->

						<!--Split button dropdowns-->
						<!--<div class="btn-group">
							<div id="demo-checked-all-mail" class="btn btn-default">
								<label class="form-checkbox form-normal form-primary">
									<input class="form-input" type="checkbox" name="mail-list">
								</label>
							</div>
							<button data-toggle="dropdown" class="btn btn-default dropdown-toggle dropdown-toggle-icon"><i class="dropdown-caret fa fa-caret-down"></i></button>
							<ul class="dropdown-menu">
								<li><a href="javascript:void(0)" id="demo-select-all-list">All</a></li>
								<li><a href="javascript:void(0)" id="demo-select-none-list">None</a></li>
								<li><a href="javascript:void(0)" id="demo-select-toggle-list">Toggle</a></li>
								<li class="divider"></li>
								<li><a href="javascript:void(0)" id="demo-select-read-list">Read</a></li>
								<li><a href="javascript:void(0)" id="demo-select-unread-list">Unread</a></li>
								<li><a href="javascript:void(0)" id="demo-select-starred-list">Starred</a></li>
							</ul>
						</div>-->

						<!--Refresh button-->
						<button id="demo-mail-ref-btn" data-toggle="panel-overlay" data-target="#demo-email-list" class="btn btn-default" type="button" onclick="window.location.reload();">
							<i class="fa fa-refresh"></i>
						</button>

						<!--Dropdown button (More Action)-->
						<div class="btn-group">
							<button data-toggle="dropdown" class="btn btn-default dropdown-toggle" type="button">
								More <i class="dropdown-caret fa fa-caret-down"></i>
							</button>
							<ul class="dropdown-menu">
								<li><a href="#" onclick="mark_read();">Mark as read</a></li>
								<li><a href="#" onclick="mark_read();">Mark as unread</a></li>
								
							</ul>
						</div>
						<a href="#" class="btn btn-success" data-target="#myModal" data-toggle="modal">
							Compose Mail
						</a>
					</div>
					<hr class="hr-sm visible-xs">
					<div class="col-sm-5 clearfix">
						<div class="pull-right">

							<!--Pager buttons-->
							
							<!-- <div class="btn-group btn-group">
								<button class="btn btn-default" type="button">
									<span class="fa fa-chevron-left"></span>
								</button>
								<button class="btn btn-default" type="button">
									<span class="fa fa-chevron-right"></span>
								</button>
							</div> -->
						</div>
					</div>
				</div>
				<hr class="hr-sm">

				<!--Mail list group-->
				<ul id="demo-mail-list" class="mail-list adds-wrapper">

					<!--Mail list item-->
					<?php
						/* if ( ! empty($message_list))
						{
							foreach($message_list as $key=>$value)
							{
								$class ="";
								if ( ! empty($value['attachment']))
								{
									$class ="mail-attach";
								} */
						?>
							<!-- <li class="mail-list-unread <?php echo $class?> item-list" >
								<div class="mail-control">
									<label class="demo-cb-mail form-checkbox form-normal form-primary">
										<input type="checkbox" name="check[]" class="check" value="<?php echo $value['msg_id']?>">
									</label>
								</div>
								<div><a href="#"></a></div>
								<div class="mail-from"><a href="#"><?php echo $value['vendor_name']?></a></div>
								<div class="mail-time"><?php echo usertz_to_servertz($value['msg_date_time'],$this->session->userdata('user_tz'),'d-m-Y H:i A')?></div>
								<div class="mail-attach-icon"></div>
								
								<div class="mail-subject">
									<a href="#" onclick="read_msg(<?php echo $value['msg_id']?>);">
									<?php if ($value['unread'] > 0):?>
									<span class="label label-warning">
										Unread (<?php echo $value['unread']?>)
									</span>
									<?php endif;?>
									<?php echo html_entity_decode($value['text_message'])?></a>
								</div>
							</li> -->
						<?php
						/* 	}
						} */
					?>
					

					<!--Mail list item-->
					<!-- <li class="mail-starred">
						<div class="mail-control">
							<label class="demo-cb-mail form-checkbox form-normal form-primary">
								<input type="checkbox">
							</label>
						</div>
						<div class="mail-star"><a href="#"></a></div>
						<div class="mail-from"><a href="#">Shopping Mall</a></div>
						<div class="mail-time">10:45 AM</div>
						<div class="mail-attach-icon"></div>
						<div class="mail-subject">
							<a href="mailbox-message.html">Tracking Your Order - Shoes Store Online</a>
						</div>
					</li> -->

					<!--Mail list item-->
					<!-- <li class="mail-list-unread mail-starred mail-attach">
						<div class="mail-control">
							<label class="demo-cb-mail form-checkbox form-normal form-primary">
								<input type="checkbox">
							</label>
						</div>
						<div class="mail-star"><a href="#"></a></div>
						<div class="mail-from"><a href="#">Dropbox</a></div>
						<div class="mail-time">07:18 AM</div>
						<div class="mail-attach-icon"></div>
						<div class="mail-subject">
							<a href="mailbox-message.html">Reset your account password</a>
						</div>
					</li> -->

					<!--Mail list item-->
					<!-- <li class="mail-list-unread">
						<div class="mail-control">
							<label class="demo-cb-mail form-checkbox form-normal form-primary">
								<input type="checkbox">
							</label>
						</div>
						<div class="mail-star"><a href="#"></a></div>
						<div class="mail-from"><a href="#">Server Host</a></div>
						<div class="mail-time">01:51 PM</div>
						<div class="mail-attach-icon"></div>
						<div class="mail-subject">
							<a href="mailbox-message.html">
								<span class="label label-danger">
								Bussines
								</span>
								Regarding to your website issues.
							</a>
						</div>
					</li> -->

					<!--Mail list item-->
					<!-- <li>
						<div class="mail-control">
							<label class="demo-cb-mail form-checkbox form-normal form-primary">
								<input type="checkbox">
							</label>
						</div>
						<div class="mail-star"><a href="#"></a></div>
						<div class="mail-from"><a href="#">Lisa D. Smith</a></div>
						<div class="mail-time">Yesterday</div>
						<div class="mail-attach-icon"></div>
						<div class="mail-subject">
							<a href="mailbox-message.html">Hi John! How are you?</a>
						</div>
					</li> -->

					<!--Mail list item-->
					<!-- <li class="mail-starred">
						<div class="mail-control">
							<label class="demo-cb-mail form-checkbox form-normal form-primary">
								<input type="checkbox">
							</label>
						</div>
						<div class="mail-star"><a href="#"></a></div>
						<div class="mail-from"><a href="#">Johny Juan</a></div>
						<div class="mail-time">Yesterday</div>
						<div class="mail-attach-icon"></div>
						<div class="mail-subject">
							<a href="mailbox-message.html">
								<span class="label label-info">
								Partner
								</span>
								Repair Status Unregistered User
							</a>
						</div>
					</li> -->

					<!--Mail list item-->
					<!-- <li class="mail-attach">
						<div class="mail-control">
							<label class="demo-cb-mail form-checkbox form-normal form-primary">
								<input type="checkbox">
							</label>
						</div>
						<div class="mail-star"><a href="#"></a></div>
						<div class="mail-from"><a href="#">Bobby Marz</a></div>
						<div class="mail-time">Oct 10</div>
						<div class="mail-attach-icon"></div>
						<div class="mail-subject"><a href="mailbox-message.html">Bugs in your system.</a></div>
					</li> -->

					<!--Mail list item-->
					<!-- <li class="mail-list-unread mail-starred">
						<div class="mail-control">
							<label class="demo-cb-mail form-checkbox form-normal form-primary">
								<input type="checkbox">
							</label>
						</div>
						<div class="mail-star"><a href="#"></a></div>
						<div class="mail-from"><a href="#">Lucy Moon</a></div>
						<div class="mail-time">Oct 10</div>
						<div class="mail-attach-icon"></div>
						<div class="mail-subject"><a href="mailbox-message.html">We need to meet up soon</a></div>
					</li> -->

					<!--Mail list item-->
					<!-- <li class="mail-list-unread">
						<div class="mail-control">
							<label class="demo-cb-mail form-checkbox form-normal form-primary">
								<input type="checkbox">
							</label>
						</div>
						<div class="mail-star"><a href="#"></a></div>
						<div class="mail-from"><a href="#">Michael Robert</a></div>
						<div class="mail-time">Oct 10</div>
						<div class="mail-attach-icon"></div>
						<div class="mail-subject"><a href="mailbox-message.html">This is an example if there is a really really long text. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, </a></div>
					</li> -->

					<!--Mail list item-->
					<!-- <li class="mail-starred mail-attach">
						<div class="mail-control">
							<label class="demo-cb-mail form-checkbox form-normal form-primary">
								<input type="checkbox">
							</label>
						</div>
						<div class="mail-star"><a href="#"></a></div>
						<div class="mail-from"><a href="#">Shopping Mall</a></div>
						<div class="mail-time">Oct 9</div>
						<div class="mail-attach-icon"></div>
						<div class="mail-subject"><a href="mailbox-message.html">Tracking Your Order - Shoes Store Online</a></div>
					</li> -->

					<!--Mail list item-->
					<!-- <li class="mail-list-unread mail-starred">
						<div class="mail-control">
							<label class="demo-cb-mail form-checkbox form-normal form-primary">
								<input type="checkbox">
							</label>
						</div>
						<div class="mail-star"><a href="#"></a></div>
						<div class="mail-from"><a href="#">Dropbox</a></div>
						<div class="mail-time">Oct 8</div>
						<div class="mail-attach-icon"></div>
						<div class="mail-subject"><a href="mailbox-message.html">Reset your account password</a></div>
					</li> -->

					<!--Mail list item-->
					<!-- <li class="mail-list-unread">
						<div class="mail-control">
							<label class="demo-cb-mail form-checkbox form-normal form-primary">
								<input type="checkbox">
							</label>
						</div>
						<div class="mail-star"><a href="#"></a></div>
						<div class="mail-from"><a href="#">Server Host</a></div>
						<div class="mail-time">Oct 7</div>
						<div class="mail-attach-icon"></div>
						<div class="mail-subject">
							<a href="mailbox-message.html">
								<span class="label label-danger">
									Bussines
								</span>
								Regarding to your website issues.
							</a>
						</div>
					</li> -->

					<!--Mail list item-->
					<!-- <li class="mail-starred">
						<div class="mail-control">
							<label class="demo-cb-mail form-checkbox form-normal form-primary">
								<input type="checkbox">
							</label>
						</div>
						<div class="mail-star"><a href="#"></a></div>
						<div class="mail-from"><a href="#">Lisa D. Smith</a></div>
						<div class="mail-time">Oct 5</div>
						<div class="mail-attach-icon"></div>
						<div class="mail-subject"><a href="mailbox-message.html">Hi John! How are you?</a></div>
					</li> -->


					<!--Mail list item-->
					<!-- <li class="mail-starred">
						<div class="mail-control">
							<label class="demo-cb-mail form-checkbox form-normal form-primary">
								<input type="checkbox">
							</label>
						</div>
						<div class="mail-star"><a href="#"></a></div>
						<div class="mail-from"><a href="#">Johny Juan</a></div>
						<div class="mail-time">Oct 5</div>
						<div class="mail-attach-icon"></div>
						<div class="mail-subject">
							<a href="mailbox-message.html">
								<span class="label label-info">
									Partner
								</span>
								Repair Status Unregistered User
							</a>
						</div>
					</li> -->


					<!--Mail list item-->
					<!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->
				<!-- 	<li class="mail-attach">
						<div class="mail-control">
							<label class="demo-cb-mail form-checkbox form-normal form-primary">
								<input type="checkbox">
							</label>
						</div>
						<div class="mail-star"><a href="#"></a></div>
						<div class="mail-from"><a href="#">Bobby Marz</a></div>
						<div class="mail-time">Oct 3</div>
						<div class="mail-attach-icon"></div>
						<div class="mail-subject"><a href="mailbox-message.html">Bugs in your system.</a></div>
					</li> -->


					<!--Mail list item-->
					<!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->
					<!-- <li class="mail-list-unread mail-starred">
						<div class="mail-control">
							<label class="demo-cb-mail form-checkbox form-normal form-primary">
								<input type="checkbox">
							</label>
						</div>
						<div class="mail-star"><a href="#"></a></div>
						<div class="mail-from"><a href="#">Lucy Moon</a></div>
						<div class="mail-time">Oct 1</div>
						<div class="mail-attach-icon"></div>
						<div class="mail-subject"><a href="mailbox-message.html">We need to meet up soon</a></div>
					</li> -->
				</ul>
				<input type="hidden" name="row" id="row" value="0"/>
				<input type="hidden" name="all" id="all" value="<?php echo $message_cnt?>"/> 
			</div>
		</div>
	</div>
	<div class="col-sm-6 pull-right">
		<button type="button" class="btn col-sm-3 pull-right load-more" style="background:#004DDA;color:white;margin-top:10px;" onclick="load_more();">  Load More.. </button>
	</div>
</div>
<div class="modal fade" id="myModal" role="dialog" tabindex="-1" aria-labelledby="demo-default-modal" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">

				<!--Modal header-->
				<div class="modal-header">
					<button data-dismiss="modal" class="close" type="button">
					<span aria-hidden="true">&times;</span>
					</button>
					<h4 class="modal-title">Compose Mail</h4>
				</div>

				<!--Modal body-->
				<div class="modal-body compose_mail">
				<form name="compose_msg" id="compose_msg">
					<div class='col-lg-12'>
						<div class='col-lg-3'>
							<label class='control-label'>To</label>
						</div>
						<div class='col-lg-9'>
						<select type='text' class='form-control' name="vendor_id">
							<option value="">--Select One--</option>
							<?php
								if( ! empty($vendor))
								{
									foreach($vendor as $v)
									{
								?>
										<option value="<?php echo $v['id']?>"><?php echo $v['vendor_name']?></option>
								<?php
									}
								}
							?>
						</select>
						</div>
						<div class='col-lg-3'>
							<label class='control-label'>Subject</label>
						</div>
						<div class='col-lg-9'>
							<input type='text' class='form-control' name="sub">
						</div>
						<div class='col-lg-3'>
							<label class='control-label'>Attachments</label>
						</div>
						<div class='col-lg-9'>
							<span class="pull-right btn btn-success btn-file">
								Browse... <input type="file" name="upload_file[]" id="upload_file" multiple/>
							</span>
						</div>
					</div>
					<br>
					<div class='col-lg-12'>
						<div class="page-wrapper box-content">
						<textarea class="message_content ckeditor" name="msg" id="msg"></textarea>
						</div>
					</div>
				</div>
				</form>
				<!--Modal footer-->
				<div class="modal-footer">
					<button data-dismiss="modal" class="btn btn-default" type="button">Close</button>
					<button class="btn btn-primary" onclick="submit_form();">Save changes</button>
				</div>
			</div>
		</div>
	</div>
<!--===================================================-->
<!--END CONTENT CONTAINER-->
<style type='text/css'>

</style>



<script src="https://cdn.ckeditor.com/4.14.0/standard/ckeditor.js"></script> 

<script type='text/javascript'>
$(document).ready(function(){
load_more();
});
function submit_form()
{
	
		$("#msg").val(CKEDITOR.instances.msg.getData());

		var form = $("#compose_msg");
		form.validate({
			ignore:[],
			rules: {
				sub: "required",
				vendor_id:"required",
				msg:{ 
					required: function() 
                        {
                         CKEDITOR.instances.msg.updateElement();
                        },

                     minlength:10
				},
			},
			messages: {
				sub: "<b class='text-danger'>Please Enter Subject Line</b>",
				vendor_id:"<b class='text-danger'>Please Select Vendor</b>",
				msg:"<b class='text-danger'>Please Enter Message</b>",
				
			}
		});

		if (form.valid()){
			$('#compose_msg').ajaxSubmit({
				url:"<?php echo base_url('auctioneer/messages/compose_msg');?>",
				type: 'post',
				dataType:'json',
				cache: false,
				clearForm: false,
				enctype: 'multipart/form-data',
				success: function (response)
				{
					if (response.status=="success")
					{
						
						$("#myModal").modal("hide");
						window.location.reload();
					}
					else
					{
						alert('Please Try Again After Some Time');
						$("#myModal").modal("hide");
						//get_inbox_msg();
					}
				},
				error:function()
				{
					alert('Please Try Again After Some Time');
				}
			});
	
		}
} 
function read_msg(msg_id)
{
	$.ajax({
		url:"<?php echo base_url('auctioneer/messages/read_msg');?>",
		type: 'post',
		data:{'msg_id':msg_id},
		cache:false,
		clearForm:false,
		dataType: 'json',
		success: function (response)
		{
			if (response.status == 'success')
			{
				window.location.href="<?php echo base_url('auctioneer/messages/view_message/')?>"+msg_id
			}
		}
	});
}
function mark_read()
{
	var msg_id = 0;
	$("input:checkbox[class=check]:checked").each(function () {
		var val = $(this).val();
		if($(this).prop("checked") == true){
			msg_id+=','+val;
			action(msg_id,'read');
		}
		else if($(this).prop("checked") == false){
			msg_id+=','+val;
			action(msg_id,'unread');
		}
	});
}
function action(msg_id,option)
{
	$.ajax({
		url:"<?php echo base_url('auctioneer/messages/multiple_read_unread');?>",
		type: 'post',
		data:{'option':option,'msg_ids':msg_id},
		cache:false,
		clearForm:false,
		dataType: 'json',
		success: function (response)
		{
			if (response.status == 'success')
			{
				window.location.reload();
			}
		}
	});
}
var i=0;
function load_more(record='')
{

  
  
    if (record)
    {
      $("#row").val(0);
    }
    var row = Number($('#row').val());
    var allcount = Number($('#all').val());
    var rowperpage = 10;

    if(row <= allcount){
      
        $.ajax({
            url: '<?php echo base_url('auctioneer/messages/get_list')?>',
            type: 'post',
            data: {'start':row,'limit':rowperpage},
            dataType:'json',
            beforeSend:function(){
                $(".load-more").text("Loading...");
            },
            success: function(response){

                // Setting little delay while displaying new content
                setTimeout(function() {
                    // appending posts after last post with class="post"
                    if (response)
                    {
						if (response)
						{
							var html = '';
							$.each(response ,function(i,value)
							{
								var cls ="";
								if ( value['attachment'])
								{
									cls ="mail-attach";
								}
						
							html+='<li class="mail-list-unread '+cls+' item-list" >'+
								'<div class="mail-control">'+
									'<label class="demo-cb-mail form-checkbox form-normal form-primary">'+
										'<input type="checkbox" name="check[]" class="check" value="'+value['msg_id']+'">'+
									'</label>'+
								'</div>'+
								'<div><a href="#"></a></div>'+
								'<div class="mail-from"><a href="#">'+value['vendor_name']+'</a></div>'+
								'<div class="mail-time">'+value['date_time']+'</div>'+
								'<div class="mail-attach-icon"></div>'+
								
								'<div class="mail-subject">'+
									'<a href="#" onclick="read_msg('+value['msg_id']+');">';
									if (value['unread'] > 0)
									{
										html+='<span class="label label-warning">'+
										'Unread ('+value['unread']+')'+
									'</span>';
									}
									
									html+=value['message_text'];
									html+='</a>'+
								'</div>'+
							'</li>';
							});
						}
                    }
 
                    if (row > 0)
                    {
                      
                      $(".item-list:last").after(html).show().fadeIn("slow");
                      
                    
                    }else{
                     
                      $(".adds-wrapper").html(html).show().fadeIn("slow");
                    } 
                   

                    var rowno = row + rowperpage;
                   
                    $("#row").val(rowno);
                    

                    // checking row value is greater than allcount or not
                   
                    if(rowno >= allcount){

                        // hide 
                        $('.load-more').hide();
                    }else{
                        $('.load-more').show();
                        $(".load-more").text("Load more..");
                    }
                }, 2000);

            }
        });
    }else{
        $('.load-more').text("Loading...");

        // Setting little delay while removing contents
        setTimeout(function() {

            // When row is greater than allcount then remove all class='post' element after 3 element
            $('.item-list:nth-child(3)').nextAll('.item-list').remove();

            // Reset the value of row
            $("#row").val(0);

            // Change the text and background
            $('.load-more').text("Load more..");
            $('.load-more').css("background","#15a9ce");
            
        }, 2000);


    }

}

</script>
