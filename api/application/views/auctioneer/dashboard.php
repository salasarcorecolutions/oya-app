<!--CONTENT CONTAINER-->
<!--===================================================-->
<div id="content-container">

	<!--Page Title-->
	<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
	<div id="page-title">

	<?php if (empty ($this->session->userdata('vendor_logo'))){ ?>
		<h1 class="page-header text-overflow">Oya Dashboard</h1>
	<?php } else { ?>
		<h1 class="page-header text-overflow">Dashboard</h1>
	<?php } ?>
	</div>
	<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
	<!--End page title-->
	<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
	<!--End breadcrumb-->
	<div id="page-content">
	<div class="row">
						<div class="col-sm-6 col-lg-3">
					
							<!--Registered User-->
							<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
							<div class="panel media pad-all">
								<div class="media-left">
									<span class="icon-wrap icon-wrap-sm icon-circle bg-success">
									<i class="fa fa-user fa-2x"></i>
									</span>
								</div>
					
								<div class="media-body">
									<p class="text-2x mar-no text-thin"><?php echo ( ! empty($count['associated_bidder_count']))?$count['associated_bidder_count']:0;?></p>
									<p class="text-muted mar-no"><a href="<?php echo base_url('auctioneer/bidders/associate_bidder_list');?>">Associated Bidders</a></p>
								</div>
							</div>
							<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
					
						</div>
						<div class="col-sm-6 col-lg-3">
					
							<!--New Order-->
							<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
							<div class="panel media pad-all">
								<div class="media-left">
									<span class="icon-wrap icon-wrap-sm icon-circle bg-info">
									<i class="fa fa-shopping-cart fa-2x"></i>
									</span>
								</div>
					
								<div class="media-body">
									<p class="text-2x mar-no text-thin"><?php echo ( ! empty($count['total_count']['auction_count']))?$count['total_count']['auction_count']:0;?></p>
									<p class="text-muted mar-no"><a href="<?php echo base_url('auctioneer/auction')?>">Auctions</a></p>
								</div>
							</div>
							<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
					
						</div>
						<div class="col-sm-6 col-lg-3">
					
							<!--Comments-->
							<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
							<div class="panel media pad-all">
								<div class="media-left">
									<span class="icon-wrap icon-wrap-sm icon-circle bg-warning">
									<i class="fa fa-comment fa-2x"></i>
									</span>
								</div>
					
								<div class="media-body">
									<p class="text-2x mar-no text-thin"><?php echo ( ! empty($count['total_count']['tender_count']))?$count['total_count']['tender_count']:0?></p>
									<p class="text-muted mar-no"><a href="<?php echo base_url('auctioneer/tender')?>">Tenders</a></p>
								</div>
							</div>
							<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
					
						</div>
						<div class="col-sm-6 col-lg-3">
					
							<!--Sales-->
							<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
							<div class="panel media pad-all">
								<div class="media-left">
									<span class="icon-wrap icon-wrap-sm icon-circle bg-danger">
									<i class="fa fa-dollar fa-2x"></i>
									</span>
								</div>
					
								<div class="media-body">
									<p class="text-2x mar-no text-thin"><?php echo ( ! empty($count['total_count']['product_count']))?$count['total_count']['product_count']:0;?></p>
									<p class="text-muted mar-no"><a href="<?php echo base_url('auctioneer/salvage')?>">Products</a></p>
								</div>
							</div>
							<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
					
						</div>
					</div>
		<div class="row" >
			<div class="col-sm-6">
				<div class="panel height_400 scrolldown">
					<div class="panel-heading">
						<h3 class="panel-title">MESSAGES</h3>
					</div>
		
					<!--Bordered Table-->
					<!--===================================================-->
					<div class="panel-body">
						<div class="table-responsive">
							<table class="table table-striped" >
								<thead>
									<tr>
										<th class="text-center">#</th>
										<th class="text-center">Received Message From</th>
										<th class="text-center">Message Sub</th>
										<th class="text-center">Received DateTime</th>
										<th class="text-center">Message Status</th>
									</tr>
								</thead>
								<tbody id="message_body">
								
								</tbody>
							</table>
							<input type="hidden" id="row" value="0">
							<input type="hidden" id="all" value="<?php echo $count['msg_count']?>">
							<a href="" class="pull-right label label-info load-more"  onclick="load_messages()">Show More</a>
						</div>
					</div>
					<!--===================================================-->
					<!--End Bordered Table-->
		
				</div>
			</div>
			<div class="col-sm-6">
				<div class="panel height_400 scrolldown">
					<div class="panel-heading">
						<h3 class="panel-title">SYSTEM LOGS</h3>
					</div>
		
					<!--Bordered Table-->
					<!--===================================================-->
					<div class="panel-body">
						<div class="table-responsive">
							<table class="table table-striped table-hovered  ">
								<thead>
									<tr>
										<th class="text-center">#</th>
										<th class="text-center">Log</th>
										<th class="text-center">Saleno</th>
										<th class="text-center">IP</th>
										
										<th class="text-center">Updated On</th>
										<th class="text-center">Updated By</th>

									</tr>
								</thead>
								<tbody id="log">
								
								</tbody>
							</table>
							<input type="hidden" id="log_row" value="0">
							<input type="hidden" id="log_all" value="<?php echo $count['log_count']?>">
							
							<a href="#" class="pull-right label label-info log-load-more" onclick="get_system_log()" >Show More</a>
						</div>
					</div>
					<!--===================================================-->
					<!--End Bordered Table-->
		
				</div>
			</div>
			<div class="col-sm-6">
				<div class="panel height_400 scrolldown">
					<div class="panel-heading">
						<h3 class="panel-title">ONLINE USERS</h3>
					</div>
		
					<!--Bordered Table-->
					<!--===================================================-->
					<div class="panel-body">
						<div class="table-responsive">
							<table class="table table-striped table-hovered">
								<thead>
									<tr>
										<th class="text-center">#</th>
										<th class="text-center">Login Time</th>
										<th class="text-center">Company Name</th>
										<th class="text-center">User Name</th>
										<th class="text-center">IP</th>
										<th class="text-center">Browser</th>
										<th class="text-center">Screen</th>
										<th class="text-center">Mobile</th>


									</tr>
								</thead>
								<tbody id="online_user">
									
								</tbody>
							</table>
							<input type="hidden" id="online_row" value="0">
							<input type="hidden" id="online_all" value="<?php echo $count['online_bidder_count']?>">
							<a href="" class="pull-right label label-info status-load-more" onclick="get_online_bidder()">Show More</a>
						</div>
					</div>
					<!--===================================================-->
					<!--End Bordered Table-->
		
				</div>
			</div>
		</div>
	</div>

</div>
<!--===================================================-->
<!--END CONTENT CONTAINER-->
<style type='text/css'>
.table>tbody>tr>td,
.table>tbody>tr>th,
.table>tfoot>tr>td, 
.table>tfoot>tr>th, 
.table>thead>tr>td, 
.table>thead>tr>th
{
	padding: 4px !important; 
}
.height_400{height:400px;overflow-y: scroll;}
</style>

<script type='text/javascript'>
$(document).ready(function(){
	load_messages('first');
	get_online_bidder('first');
	get_system_log('first');
});

var i =0;
var c =0;
function load_messages(record='')
{
	event.preventDefault();
	$(".load-more").show();
	if (record)
    {
      $("#row").val(0);
    }
    var row = Number($('#row').val());
    var allcount = Number($('#all').val());
    var rowperpage = 5;
   
    if(row <= allcount){
		$.ajax({
            url: '<?php echo base_url('auctioneer/dashboard/get_messages')?>',
            type: 'get',
            dataType:'json',
            data:{'start':row,'limit':rowperpage},
            beforeSend:function(){
                $(".load-more").text("Loading...");
            },
            success: function(response){

                // Setting little delay while displaying new content
                setTimeout(function() {
					// appending posts after last post with class="post"
					if (response)
                    {
                     	var html='';
						
					
                        $.each(response,function(i,v)
                        {
							  i++;
							  c = c+1;
							
							if ( ! Number.isNaN(i))
							{
								if (v.read ==0)
								{
								 var status ='unread';
								}
								else{
									var status = 'read';
								}
								html+='<tr><td class="text-center">'+c+'</td>'+
								'<td class="text-center">'+v.compname+'</td>'+
								'<td class="text-center">'+v.msg_sub+'</td>'+
								'<td class="text-center">'+v.msg_date_time+'</td>'+
								'<td class="text-center">'+status+'</td>'+
								'</tr>';
			
							}
                        });
                      
                      
					}
					else
					{
						html ='';
						html+='<tr><td colspan="5" align="center">No Messages Found</td></tr>';
					}

					if (row > 0)
                    {
                      
                      $("#message_body tr:last").after(html).show().fadeIn("slow");
                      
                    
                    }else{
                     
                      $("#message_body").html(html).show().fadeIn("slow");
                    } 
                   

                    var rowno = row + rowperpage;
                  
                    $("#row").val(rowno);
                 
                    // checking row value is greater than allcount or not
                   
                    if(rowno >= allcount){
						
                        // hide 
                        $('.load-more').hide();
                    }else{
                        $('.load-more').show();
                        $(".load-more").text("Load more..");
                    }
					
				}, 2000);
			}
		});
	}
	else{
		$('.load-more').text("Loading...");

		// Setting little delay while removing contents
		setTimeout(function() {

			// When row is greater than allcount then remove all class='post' element after 3 element
			

			// Reset the value of row
			$("#row").val(0);

			// Change the text and background
			$('.load-more').text("Load more..");
			$('.load-more').css("background","#15a9ce");
			
		}, 2000);


	}
}

function get_online_bidder(record='')
{
	event.preventDefault();
	$(".status-load-more").show();
	if (record)
    {
      $("#online_row").val(0);
    }
    var row = Number($('#online_row').val());
	var allcount = Number($('#online_all').val());
	
	
    var rowperpage = 5;
   
    if(row <= allcount){
		$.ajax({
            url: '<?php echo base_url('auctioneer/dashboard/get_online_bidders')?>',
            type: 'get',
            dataType:'json',
            data:{'start':row,'limit':rowperpage},
            beforeSend:function(){
                $(".status-load-more").text("Loading...");
            },
            success: function(response){

                // Setting little delay while displaying new content
                setTimeout(function() {
					// appending posts after last post with class="post"
					if (response.length > 0)
                    {
                     	var html='';
						
					
                        $.each(response,function(i,v)
                        {
							  i++;
							  c = c+1;
							
							if ( ! Number.isNaN(i))
							{
								let arr =JSON.parse(v.browser_info);
								var mobile = 'NO';
								if (arr['mobile'] === true)
								{
									var mobile = 'YES';
								}
								html+='<tr>'+
								'<td>'+c+'</td>'+
								'<td>'+v.logintime+'</td>'+
								'<td class="text-center">'+v.compname+'</td>'+
								'<td class="text-center">'+v.conperson+'</td>'+
								'<td class="text-center">'+v.ip+'</td>'+
								'<td class="text-center">'+arr['browser']+'</td>'+
								'<td class="text-center">'+arr['screen']+'</td>'+
								'<td class="text-center">'+mobile+'</td>'+
								'</tr>';
			
							}
                        });
                      
                      
					}
					else
					{
						html ='';
						html+='<tr><td colspan="8" align="center">No User Found</td></tr>';
					}

					if (row > 0)
                    {
                      
                      $("#online_user tr:last").after(html).show().fadeIn("slow");
                      
                    
                    }else{
                     
                      $("#online_user").html(html).show().fadeIn("slow");
                    } 
                   

                    var rowno = row + rowperpage;
                  
                    $("#online_row").val(rowno);
                 
                    // checking row value is greater than allcount or not
					
                    if(rowno >= allcount){
						
                        // hide 
                        $('.status-load-more').hide();
                    }else{
						
						$(".status-load-more").text("Load more..");
                        $('.status-load-more').show();
                        
                    }
					
				}, 2000);
			}
		});
	}
	else{
		
		$('.status-load-more').text("Loading...");

		// Setting little delay while removing contents
		setTimeout(function() {

			// When row is greater than allcount then remove all class='post' element after 3 element
			

			// Reset the value of row
			$("#online_row").val(0);

			// Change the text and background
			$('.status-load-more').text("Load more..");
			$('.status-load-more').css("background","#15a9ce");
			
		}, 2000);


	}
}
var log = 0;
function get_system_log(record='')
{
	event.preventDefault();
	$(".log-load-more").show();
	if (record)
    {
      $("#log_row").val(0);
    }
    var row = Number($('#log_row').val());
	var allcount = Number($('#log_all').val());
	
    var rowperpage = 5;
   
    if(row <= allcount){
		$.ajax({
            url: '<?php echo base_url('auctioneer/dashboard/get_system_log')?>',
            type: 'get',
            dataType:'json',
            data:{'start':row,'limit':rowperpage},
            beforeSend:function(){
                $(".log-load-more").text("Loading...");
            },
            success: function(response){

                // Setting little delay while displaying new content
                setTimeout(function() {
					// appending posts after last post with class="post"
					if (response.length > 0)
                    {
                     	var html='';
						
					
                        $.each(response,function(i,v)
                        {
							  i++;
							 log = log+1;
							
							if ( ! Number.isNaN(i))
							{
								html+='<tr><td class="text-center">'+log+'</td>'+
								'<td class="text-center">'+v.log_title+'</td>'+
								'<td class="text-center">'+v.auction_saleno+'</td>'+
								'<td class="text-center">'+v.ip+'</td>'+
								
								'<td class="text-center">'+v.date_updated+'</td>'+
								'<td class="text-center">'+v.c_name+'</td>'+
								'</tr>';
			
							}
                        });
                      
                      
					}
					else
					{
						html ='';
						html+='<tr><td colspan="5" align="center">No Log Found</td></tr>';
					}

					if (row > 0)
                    {
                      
                      $("#log tr:last").after(html).show().fadeIn("slow");
                      
                    
                    }else{
                     
                      $("#log").html(html).show().fadeIn("slow");
                    } 
                   

                    var rowno = row + rowperpage;
                  
                    $("#log_row").val(rowno);
                 
                    // checking row value is greater than allcount or not
					
                    if(rowno >= allcount){
						
                        // hide 
                        $('.log-load-more').hide();
                    }else{
                        $('.log-load-more').show();
                        $(".log-load-more").text("Load more..");
                    }
					
				}, 2000);
			}
		});
	}
	else{
		$('.log-load-more').text("Loading...");

		// Setting little delay while removing contents
		setTimeout(function() {

			// When row is greater than allcount then remove all class='post' element after 3 element
			

			// Reset the value of row
			$("#log_row").val(0);

			// Change the text and background
			$('.log-load-more').text("Load more..");
			$('.log-load-more').css("background","#15a9ce");
			
		}, 2000);


	}
}

</script>