
<style>
	.control-label{
		color: #25476a;
		font-weight: bold;
	}
	.text-danger i {
		padding-left: 10px;
	}
</style><!--CONTENT CONTAINER-->
<!--===================================================-->
<div id="content-container">
	<!--Page Title-->
	<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
	<div id="page-title">
		<h1 class="page-header text-overflow">Add Company Branch</h1>
	</div>
	<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
	<!--End page title-->

	<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
	<!--End breadcrumb-->
	<div id="page-content">
		<div class="panel">
			
			<div class="panel-body">
				<form name="frm" id="frm">
				  
					  <?php if ($this->session->userdata('registration_type') == 1){ ?>
						
						<div class="panel panel-primary margin-bottom-40">
							<div class="panel-heading">
								<h3 class="panel-title"><i class="fa fa-bank"></i> Add Company Details</h3>
							</div>
							<div class="panel-body">
								<div class="col-sm-4">
									<div class="form-group">
										<label class="control-label">Company<span class="text-danger"><i class="fa fa-asterisk" aria-hidden="true"></i></span></label>
										<input type="hidden" id="vendor_id" name="vendor_id" value=""/>
										<select id="company" name="company" class="form-control addSelect2" style="width:100%" onchange="get_company_id(this.value);fetch_company_details(this.value);">
										</select>
									</div>
								</div>

								<div class="col-sm-4">
									<div class="form-group">
										<label class="control-label">Address Line 1<span class="text-danger"><i class="fa fa-asterisk" aria-hidden="true"></i></span></label>
										<input type='text' class='form-control' id="cmp_add1" name='cmp_add1'/>
										
									</div>
								</div>
								
								<div class="col-sm-4">
									<div class="form-group">
										<label class="control-label">Address Line 2<span class="text-danger"><i class="fa fa-asterisk" aria-hidden="true"></i></span></label>
										<input type='text' class='form-control' id="cmp_add2" name='cmp_add2'/>

									</div>
								</div>

								<div class="col-sm-4">
									<div class="form-group">
										<label class="control-label">Select Country<span class="text-danger"><i class="fa fa-asterisk" aria-hidden="true"></i></span></label>
										<select  class="form-control select2" name="cmp_country" id="cmp_country" onchange="fetch_state(this.id)" attr="cmp">
											<option value="">--select Country--</option>
											<?php if ( ! empty($country)){
													foreach ($country as $singlecountry){
											?>
													<option value="<?php echo $singlecountry['name']?>"><?php echo $singlecountry['name'];?></option>
											<?php
												}
											}
											?>
										</select>
									</div>
								</div>

								<div class="col-sm-4">
									<div class="form-group">
										<label class="control-label">Select State<span class="text-danger"><i class="fa fa-asterisk" aria-hidden="true"></i></span></label>
										<select  class="form-control select2 slpick" name="cmp_state" id="cmp_state" onchange="fetch_city(this.id);" attr= "cmp">
											<option value="">--select state--</option>
										</select>

									</div>
								</div>
								
								<div class="col-sm-4">
									<div class="form-group">
										<label class="control-label">Select City<span class="text-danger"><i class="fa fa-asterisk" aria-hidden="true"></i></span></label>
										<select  class="form-control select2" name="cmp_city" id="cmp_city">
											<option value="">--select city--</option>
										</select>

									</div>
								</div>


							</div>
						</div>
					
					<?php } ?>

					<div class="panel panel-primary margin-bottom-40">
						<div class="panel-heading">
							<h3 class="panel-title"><i class="fa fa-bank"></i> Add Branch Details</h3>
						</div>
						<div class="panel-body">

							<div class="row">
							<div class="col-sm-3">
									<div class="form-group">
										<label class="control-label">Branch Name<span class="text-danger"><i class="fa fa-asterisk" aria-hidden="true"></i></span></label>
										<input type="hidden" id="branch_id" name="branch_id" value=""/>
										<select style='width:100%;' class="select2" id="branch_name" name="branch_name" onchange="fetch_branch_details(this.value)" >
										</select>

									</div>
								</div>

								<div class="col-sm-3">
									<div class="form-group">
										<label class="control-label">Address Line 1<span class="text-danger"><i class="fa fa-asterisk" aria-hidden="true"></i></span></label>
										<input type='text' class='form-control' id="branch_add1" name='branch_add1'/>
									</div>
								</div>

								<div class="col-sm-3">
									<div class="form-group">
										<label class="control-label">Address Line 2<span class="text-danger"><i class="fa fa-asterisk" aria-hidden="true"></i></span></label>
										<input type='text' class='form-control' id="branch_add2" name='branch_add2'/>

									</div>
								</div>
							
								<div class="col-sm-3">
									<div class="form-group">
										<label class="control-label">Select Country<span class="text-danger"><i class="fa fa-asterisk" aria-hidden="true"></i></span></label>
										<select  class="form-control select2" name="branch_country" id="branch_country" onchange="fetch_state(this.id)" attr="branch">
											<option value="">Select Country </option>
											<?php if ( ! empty($country)){
													foreach($country as $singlecountry){
													?>
														<option value="<?php echo $singlecountry['name']?>"><?php echo $singlecountry['name'];?></option>
													<?php
													}
												}
											?>
										</select>

									</div>
								</div>
								
								<div class="col-sm-3" style='clear:both;'>
									<div class="form-group">
										<label class="control-label">Select State<span class="text-danger"><i class="fa fa-asterisk" aria-hidden="true"></i></span></label>
										<select  class="form-control select2" name="branch_state" id="branch_state" onchange="fetch_city(this.id)" attr="branch">
											<option value="">Select State</option>
										</select>

									</div>
								</div>
						
								<div class="col-sm-3">
									<div class="form-group">
										<label class="control-label">Select city<span class="text-danger"><i class="fa fa-asterisk" aria-hidden="true"></i></span></label>
										<select  class="form-control select2" name="branch_city" id="branch_city">
											<option value="">Select City</option>
										</select>

									</div>
								</div>
								
								
								<div class="col-sm-3">
									<div class="form-group">
										<label class="control-label">Enter Pin Code<span class="text-danger"><i class="fa fa-asterisk" aria-hidden="true"></i></span></label>
										<input type='number' min="0" class='form-control' id="pin" name='pin'/>
									</div>
								</div>
								<div class="col-sm-6" style="margin-top:20px;">
									<button class="btn btn-success pull-right" type="submit">Add</button>
								</div>
								<div class="col-sm-6" style="margin-top:20px;">
									<a type='button' href="<?php echo base_url('dashboard')?>" class="btn btn-danger">Cancel</a>
								</div>
							</div>
							</form>

							
						</div>
					</div>

					<div class="panel panel-primary margin-bottom-40">
						<div class="panel-heading">
							<h3 class="panel-title"><i class="fa fa-bank"></i>Company Branch List</h3>
						</div>
						<div class="panel-body">
							<div class="table-responsive">
                                <table callfunction='<?php echo base_url("auctioneer/company_plant/company_plant_details_ajax"); ?>' class='table-responsive table table-striped table-bordered table-hover no-footer dtr-inline  dynamicTable' >
                                    <thead>
                                        <th data-bSortable="false">SR NO</th>
                                        <th data-bSortable="false">COMPANY NAME</th>
                                        <th data-bSortable="false">PLANT NAME</th>
                                        <th data-bSortable="false">PLANT ADDRESS</th>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                </table>
							</div>
						</div>
					</div>

			</div>
		</div>

	</div>
</div>
<!--===================================================-->
<!--END CONTENT CONTAINER-->
<style type='text/css'>
.table>tbody>tr>td,
.table>tbody>tr>th,
.table>tfoot>tr>td,
.table>tfoot>tr>th, 
.table>thead>tr>td,
.table>thead>tr>th
{
	padding: 4px !important;
}
fieldset
{
	border: 1px solid #ddd !important;
	margin: 0;
	padding: 10px;
	position: relative;
	border-radius:4px;
	background-color:#f5f5f5;
	padding-left:10px!important;
}

legend
{
	font-size:14px;
	font-weight:bold;
	margin-bottom: 0px;
	width: 35%; 
	border: 1px solid #ddd;
	border-radius: 4px;
	padding: 5px 5px 5px 10px; 
	background-color: #ffffff;
}
</style>


<script type='text/javascript'>
 $( function() {
	$("#branch_name").select2({
		tags: true,
		placeholder: "Select Branch Name"
	});
	$(".addSelect2").select2({
		tags: true,
		placeholder: 'Select Company Name',
		minimumInputLength: 5,
		allowClear: true,
		ajax: {
			url: "<?php echo base_url('auctioneer/company_plant/fetch_company_data')?>",
			dataType:'json',
			data: function (params) {
				return {
					q: params.term, // search term
					page: params.page
				};
			},
			processResults: function (data, params){
				return {
					results: data.results,
					pagination: {
						more: (params.page * 30) < data.total_count
					}
				};
			},
			createTag: function (params) {
				var term = $.trim(params.term);
				if (term === '') {
				  return null;
				}
				return {
				  id: term,
				  text: term,
				  newTag: false // add additional parameters
				}
			}
		},
	});

	$.validator.setDefaults({ ignore: '' });

	var vRules 	= 	{
		company:{required:true,maxlength:100},
		cmp_add1:{required:true},
		cmp_country:{required:true},
		cmp_state:{required:true},
		cmp_city:{required:true},
		branch_name:{required:true},
		branch_add1:{required:true},
		branch_country:{required:true},
		branch_state:{required:true},
		branch_city:{required:true},
		pin:{required:true},
	};
	var vMessage = {
		company:{required:"<b class='text-danger'>Please Enter Company Name</b>"},
		cmp_add1:{required:"<b class='text-danger'>Please Enter address</b>"},
		cmp_country:{required:"<b class='text-danger'>Please select country</b>"},
		cmp_state:{required:"<b class='text-danger'>Please select State</b>"},
		cmp_city:{required:"<b class='text-danger'>Please select City</b>"},
		branch_name:{required:"<b class='text-danger'>Please Enter Branch Name</b>"},
		branch_add1:{required:"<b class='text-danger'>Please Enter Branch Address</b>"},
		branch_country:{required:"<b class='text-danger'>Please select branch country</b>"},
		branch_state:{required:"<b class='text-danger'>Please select branch state</b>"},
		branch_city:{required:"<b class='text-danger'>Please select branch city</b>"},
		pin:{required:"<b class='text-danger'>Please enter pincode</b>"},
	};
	$("#frm").validate({
		rules: vRules,
		messages: vMessage,
		submitHandler: function(form){
			$(form).ajaxSubmit({
				url: '<?php echo base_url('auctioneer/company_plant/add_company_details');?>',
				type:"post",
				dataType: 'json',
				success: function (resObj) {
					alert(resObj.message);
					window.location.reload();
				},
				error: function(e){
					alert(e);
				}
			});
		}
	});
});
function get_company_id(value){
	$("#vendor_id").val('');
	if ( ! isNaN(value))
	{
		if(value){
			$("#vendor_id").val(value);
			jQuery.ajax({
				url: '<?php echo base_url('auctioneer/company_plant/fetch_branch_option')?>',
				type:"post",
				data: {'vendor_id':value},
				success: function (resObj) {
					$("#branch_name").html('<option value="">Select Branch</option>'+resObj);
				},
				error: function(e){
					alert(e);
				}
			});
		}
	}
}

async function fetch_company_details(value)
{
	$("#cmp_add1").val('');
	$("#cmp_add2").val('');
	$('#cmp_country').val('');
	$('#cmp_city').val('');
	$('#cmp_state').val('');
	jQuery.ajax({
		url: '<?php echo base_url('auctioneer/company_plant/fetch_company_details')?>',
		type:"post",
		data: {'vendor_id':value},
		dataType:'json',
		success: function (resObj) {
			if (resObj){
				$("#cmp_add1").val(resObj.address1);
				$("#cmp_add2").val(resObj.address2);
				$('#cmp_country').val(resObj.country);
				fetch_state("cmp_country",resObj.state);
				fetch_city("cmp_state",resObj.city,resObj.state);
			}
		},
		error: function(e){
			alert(e);
		}
	});
}

async function fetch_branch_details(value)
{
	$("#branch_add1").val('');
	$("#branch_add2").val('');
	$('#branch_country').val('');
	$('#branch_city').val('');
	$('#branch_state').val('');
	$("#branch_id").val('');
	if ( ! isNaN(value))
	{
		$("#branch_id").val(value);
	}
	jQuery.ajax({
		url: '<?php echo base_url('auctioneer/company_plant/fetch_branch_details')?>',
		type:"post",
		data: {'branch_id':value},
		dataType:'json',
		success: function (resObj) {
			if (resObj){
				$("#branch_add1").val(resObj.address1);
				$("#branch_add2").val(resObj.address2);
				$('#branch_country').val(resObj.country);
				fetch_state("branch_country",resObj.state);
				fetch_city("branch_state",resObj.city,resObj.state);
			}
		},
		error: function(e){
			alert(e);
		}
	});
}
async function fetch_state(id,state='')
{
	var country_name = $("#"+id).val();
	var attr = $("#"+id).attr('attr');
	if (country_name){
		await jQuery.ajax({
			url: '<?php echo base_url('auctioneer/company_plant/fetch_state')?>',
			type:"post",
			data: {'country_name':country_name,state_name:state},
			success: function (resObj) {

				$("#"+attr+"_state").html('<option value="">Select State</option>'+resObj);
				$("#"+attr+"_state").selectpicker('refresh');
				$("#"+attr+"_state").selectpicker('render');

			},
			error: function(){
				$("#"+attr+"_state").html('<option value="">Select State</option>');
				$("#"+attr+"_state").selectpicker('refresh');
				$("#"+attr+"_state").selectpicker('render');
			}
		});
	} else {
		$("#"+attr+"_state").html('<option value="">Select State</option>');
		$("#"+attr+"_state").selectpicker('refresh');
		$("#"+attr+"_state").selectpicker('render');
	}

}

async function fetch_city(id,city='',state='')
{
	if(state) {
		var state_name = state;
	} else {
		var state_name = $("#"+id).val();
	}
	var attr = $("#"+id).attr('attr');
	if(state_name !== ""){
		await jQuery.ajax({
			url: '<?php echo base_url('auctioneer/company_plant/fetch_city');?>',
			type:"post",
			data: {'state_name':state_name,'city_name':city},
			success: function (resObj) {

				$("#"+attr+"_city").html('<option value="">Select City</option>'+resObj);
				$("#"+attr+"_city").selectpicker('refresh');
				$("#"+attr+"_city").selectpicker('render');

			},
			error: function(){
				$("#"+attr+"_city").html('<option value="">Select City</option>');
				$("#"+attr+"_city").selectpicker('refresh');
				$("#"+attr+"_city").selectpicker('render');
			}
		});
	} else {
		$("#"+attr+"_city").html('<option value="">Select City</option>');
		$("#"+attr+"_city").selectpicker('refresh');
		$("#"+attr+"_city").selectpicker('render');
	}
}

</script>
