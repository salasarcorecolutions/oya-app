<!-- In order to let it work in CI, need to fix it to only get the viwes -->

<!--CONTENT CONTAINER-->
<!--===================================================-->
<div id="content-container">
	<!--Page Title-->
	<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
	<div id="page-head">
	<div id="page-title">
		<h1 class="page-header text-overflow"><?php echo $pagetitle ?></h1>
	</div>
	<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
	<!--End page title-->


	<!--Breadcrumb-->
	<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
	<ol class="breadcrumb">
		<li><a href="<?php echo base_url('auction/index');?>">Home</a></li>
		<li class="active"><a href="<?php echo base_url('auctioneer/auction/index');?>">Yankee Auction</a></li>
		<li class="active"><a href="<?php echo base_url('auctioneer/auction/view_lots/'.$auctionid);?>">Yankee Auction Lots</a></li>
		<li class="active"><?php echo $pagetitle ?></li>
	</ol>
</div>
	<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
	<!--End breadcrumb-->
	<input type="hidden" id="t11" name="auctionId">
	<div id="page-content">	
		<div class="float_btn">
			<a class="btn btn-primary btn-icon btn-circle add-tooltip" data-placement="top" data-toggle="tooltip" data-original-title="Back" href='<?php echo base_url('auctioneer/auction/view_lots/'.$auctionid); ?>' ><i class="fa fa-arrow-left" aria-hidden="true"></i></a>
		</div>
		<div class="panel">
			<div class="panel-heading">
				<h3 class="panel-title"><?php echo $pagetitle ?></h3>
			</div>
			
			<div class="panel-body">
				<div class="table-responsive">
					<table class="table table-striped table-bordered table-hover no-footer dtr-inline ">
						<thead>
							<tr>
								<th>Sr</th>
								<th>Client Name</th>
								<th>Date </th>
								<th>Time</th>
								<th>Amount</th>
								<th>Type</th>
							</tr>
						</thead>
						<tbody>
						<?php
						if( ! empty($bid_history))
						{
							$sr = 1;
							foreach($bid_history as $single_bid)
							{ 
								?>
								<tr>
									<td><?php echo $sr++; ?></td>
									<td><?php echo $single_bid['compname']; ?></td>
									<td><?php echo $single_bid['bid_date']; ?></td>
									<td><?php echo $single_bid['bid_time']; ?></td>
									<td><?php echo $single_bid['amount']; ?></td>
									<td><?php echo $single_bid['bidtype']; ?></td>
								</tr>							
							<?php
							}
						}
						?>
						</tbody>
					</table>
				</div>
			</div>	
		</div>
	</div>	
</div>

<!--===================================================-->
<!--END CONTENT CONTAINER-->
<script>


</script>