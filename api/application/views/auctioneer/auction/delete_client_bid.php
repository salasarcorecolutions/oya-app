<!-- In order to let it work in CI, need to fix it to only get the viwes -->

<!--CONTENT CONTAINER-->
<!--===================================================-->
<div id="content-container">
	<!--Page Title-->
	<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
	<div id="page-head">
	<div id="page-title">
		<h1 class="page-header text-overflow"><?php echo $pagetitle ?></h1>
	</div>
	<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
	<!--End page title-->


	<!--Breadcrumb-->
	<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
	<ol class="breadcrumb">
		<li><a href="<?php echo base_url('auction/index');?>">Home</a></li>
		<li class="active"><a href="<?php echo base_url('auctioneer/auction/index');?>">Yankee Auction</a></li>
		<li class="active"><a href="<?php echo base_url('auctioneer/auction/view_lots/'.$auctionid);?>">Yankee Auction Lots</a></li>
		<li class="active"><?php echo $pagetitle ?></li>
	</ol>
</div>
	<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
	<!--End breadcrumb-->
	<input type="hidden" id="t11" name="auctionId">
	<div id="page-content">
		<div class="float_btn">
			<a class="btn btn-primary btn-icon btn-circle add-tooltip" data-placement="top" data-toggle="tooltip" data-original-title="Back" href='<?php echo base_url('auctioneer/auction/view_lots/'.$auctionid); ?>' ><i class="fa fa-arrow-left" aria-hidden="true"></i></a>
		</div>
		<div class="panel">
			<div class="panel-heading">
				<h3 class="panel-title"><?php echo $pagetitle ?></h3>
			</div>

			<div class="panel-body">
				<div class="table-responsive">
					<table class="table table-striped table-bordered table-hover no-footer dtr-inline ">
						<thead>
							<tr>
								<th>Sr</th>
								<th>Client Name</th>
								<th>Date </th>
								<th>Time</th>
								<th>Amount</th>
								<th>Type</th>
								<th>Action</th>
							</tr>
						</thead>
						<tbody>
						<?php
						if ( ! empty($bid_history))
						{
							$sr = 1;
							$edit_permission = false;
							if ($this->common_model->check_permission('Client Bid Delete'))
							{
								$edit_permission = true;
							}
							foreach ($bid_history as $single_bid)
							{
								$action = '';
								if ($edit_permission && $sr == 1)
								{
									$action = '<a href="javascript:void(0);" onclick="delete_client_bid('.$single_bid['id'].')">Delete</a>';
								}

								?>
								<tr>
									<td><?php echo $sr++; ?></td>
									<td><?php echo $single_bid['compname']; ?></td>
									<td><?php echo $single_bid['bid_date']; ?></td>
									<td><?php echo $single_bid['bid_time']; ?></td>
									<td><?php echo $single_bid['amount']; ?></td>
									<td><?php echo $single_bid['bidtype']; ?></td>
									<td><?php echo $action;?></td>
								</tr>
							<?php
							}
						}
						?>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>

<!--===================================================-->
<!--END CONTENT CONTAINER-->
<script>

function delete_client_bid(id)
{
	if (id == '' || id == undefined){
		Display_msg('Invalid document for deletion.','error');
		return false;
	} else {
		if (id > 0){
			var ans = confirm('Are you sure you want to delete this bid? This action can not be reverted.');
			if (ans){
				$.ajax({
					url: "<?php echo base_url('auctioneer/auction/delete_client_bid/'.$auctionid.'/'.$lotno); ?>",
					type: "POST",
					dataType:"json",
					cache: false,
					clearForm: false,
					data:{"id":id},
					success:function(response){
						if (response.status == "success"){
							Display_msg(response.message,response.status);
							setTimeout(function(){
								window.location.reload();
							}, 3000);
						} else {
							Display_msg(response.message,response.status);
							return false;
						}
					}
				});
			}
		} else {
			Display_msg('Please Select Auction.','error');
			return false;
		}
	}
}
</script>