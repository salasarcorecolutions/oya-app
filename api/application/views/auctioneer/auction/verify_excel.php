<style>
.table>tbody>tr>td, .table>tbody>tr>th, .table>tfoot>tr>td, .table>tfoot>tr>th, .table>thead>tr>td, .table>thead>tr>th {
    padding: 2px !important;
}
</style>
<!--CONTENT CONTAINER-->
<!--===================================================-->
<div id="content-container">
	<!--Page Title-->
	<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
	<div id="page-head">
	<div id="page-title">
		<h1 class="page-header text-overflow">Yankee Auction Lots</h1>
	</div>
	<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
	<!--End page title-->


	<!--Breadcrumb-->
	<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
	<ol class="breadcrumb">
		<li><a href="<?php echo base_url('auction/index');?>">Home</a></li>
		<li><a href="<?php echo base_url('auctioneer/auction/index');?>">Yankee Auction</a></li>
		<li class="active">Yankee Lots</li>
	</ol>
</div>
	<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
	<!--End breadcrumb-->
	<input type="hidden" id="t11" name="auctionId">
	<div id="page-content">
		<div class="float_btn">
			<a class="btn btn-primary btn-icon btn-circle add-tooltip" data-placement="top" data-toggle="tooltip" data-original-title="Back" href='<?php echo base_url('auctioneer/auction/index'); ?>' ><i class="fa fa-arrow-left" aria-hidden="true"></i></a>
		</div>
		<div class="panel">
			<div class="panel-heading">
				<h3 class="panel-title">Yankee Lots</h3>
			</div>

			<div class="panel-body">
				<div class="table-responsive">
					<table class="table table-striped table-bordered table-hover no-footer dtr-inline">
						<thead>
							<tr>
								<th>Sale No</th>
								<th>Start Time</th>
								<th>End Time</th>
								<th>Particulars</th>
							</tr>
							<tr>
								<td><?php echo $auction_details['ynk_saleno'] ?></td>
								<td><?php echo date('d-m-Y H:i:s',strtotime($auction_details['start_date']." ".$auction_details['stime'])) ?></td>
								<td><?php echo date('d-m-Y H:i:s',strtotime($auction_details['end_date']." ".$auction_details['etime'])) ?></td>
								<td><?php echo $auction_details['particulars'] ?></td>
							</tr>
						</thead>
					</table>
				</div>

				<div class="table-responsive">
					<table class="table table-striped table-bordered table-hover no-footer dtr-inline">
						<thead>
							<tr>
							<?php
								if ( ! empty($lot_headers))
								{
									foreach($lot_headers as $header_name)
									{
										?>
										<th><?php echo strtoupper($header_name); ?></th>
										<?php
									}
								}
							?>

							</tr>
						</thead>
						<tbody>
							<?php
								$all_valid = true;
								if ( ! empty($lot_details))
								{
									/* The arrays are maintained as per the excel format to check conditions on fields.*/
									$required_field_array = array(0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,20,21);
									$date_field_array = array(4,5,17);
									$yes_no_field_array = array(16,19,20);

									foreach($lot_details as $single_lot)
									{
										$col = 0; //Column number of each field.
										foreach($single_lot as $key => $single_field)
										{
											/* Check Required Field */
											if (in_array($key, $required_field_array))
											{
                                                /* If the particular field is empty in LOT then to check if it's requrired or not. */
												if (trim($single_field) == '')
												{
													$single_lot[$key] = '<span style="color:red;">'.trim($single_field).' This field is required</span>';
													$all_valid = false;
												}
											}

											/* Check Date Field */
											if (in_array($key, $date_field_array))
											{
												if ( ! is_date_valid($single_field))
												{
													$single_lot[$key] = '<span style="color:red;">'.trim($single_field).'<br>Invalid Date</span>';
													$all_valid = false;
												}
												else
												{
													if ( ! isDateInRange($auction_details['start_date'],$auction_details['end_date'],$single_field))
													{
														$single_lot[$key] = '<span style="color:red;">'.trim($single_field).'<br>Invalid date range</span>';
														$all_valid = false;
													}
												}
											}

											/* Check Yes No Field */
											if (in_array($key, $yes_no_field_array))
											{
												if($single_field != 'Y' && $single_field != 'N')
												{
													$single_lot[$key] = '<span style="color:red">'.trim($single_field).'<br>Invalid Value Please enter Y or N</span>';
													$all_valid = false;
												}
											}
										}

										?>
										<tr>
											<td align="center" nowrap><?php echo $single_lot[$col++]; ?></td>	<!-- 0 - Lot No -->
											<td align="center" nowrap><?php echo $single_lot[$col++]; ?></td>	<!-- 1 - Vendor Lot No -->
											<td><?php echo $single_lot[$col++]; ?></td>							<!-- 2 - Description -->
											<td align="center" nowrap><?php echo $single_lot[$col++]; ?></td>	<!-- 3 - PLANT -->
											<td align="center" nowrap><?php echo $single_lot[$col++]; ?></td>	<!-- 4 - Lot Start Date -->
											<td align="center" nowrap><?php echo $single_lot[$col++]; ?></td>	<!-- 5 - Lot End Date -->
											<td align="center" nowrap><?php echo $single_lot[$col++]; ?></td>	<!-- 6 - Bid Start Time -->
											<td align="center" nowrap><?php echo $single_lot[$col++]; ?></td>	<!-- 7 - Bid End Time -->
											<td align="center" nowrap><?php echo $single_lot[$col++]; ?></td>	<!-- 8 - Bid Inc Time -->
											<td align="center" nowrap><?php echo $single_lot[$col++]; ?></td>	<!-- 9 - CMD -->
											<td align="center" nowrap><?php echo $single_lot[$col++]; ?></td>	<!-- 10 - Total Qty -->
											<td align="center" nowrap><?php echo $single_lot[$col++]; ?></td>	<!-- 11 - Total Qty Unit -->
											<td align="center" nowrap><?php echo $single_lot[$col++]; ?></td>	<!-- 12 - Bid Qty -->
											<td align="center" nowrap><?php echo $single_lot[$col++]; ?></td>	<!-- 13 - Bid Qty Unit -->
											<td align="center" nowrap><?php echo $single_lot[$col++]; ?></td>	<!-- 14 - Start Bid -->
											<td align="center" nowrap><?php echo $single_lot[$col++]; ?></td>	<!-- 15 - Increment -->
											<td align="center" nowrap><?php echo $single_lot[$col++]; ?></td>	<!-- 16 - Currency  -->
											<td align="center" nowrap><?php echo $single_lot[$col++]; ?></td>	<!-- 17 - Proxy -->
											<td align="center" nowrap><?php echo $single_lot[$col++]; ?></td>	<!-- 18 - Proxy Date -->
											<td align="center" nowrap><?php echo $single_lot[$col++]; ?></td>	<!-- 19 - Remarks -->
											<td align="center" nowrap><?php echo $single_lot[$col++]; ?></td>	<!-- 20 - Show Currency -->
											<td align="center" nowrap><?php echo $single_lot[$col++]; ?></td>	<!-- 21 - Part Bid -->
											<td align="center" nowrap><?php echo $single_lot[$col++]; ?></td>	<!-- 22 - Min Part Bid -->
											<td align="center" nowrap><?php echo $single_lot[$col++]; ?></td>	<!-- 23 - Tax 1 Name -->
											<td align="center" nowrap><?php echo $single_lot[$col++]; ?></td>	<!-- 24 - Tax 1 Rate -->
											<td align="center" nowrap><?php echo $single_lot[$col++]; ?></td>	<!-- 25 - Tax 2 Name -->
											<td align="center" nowrap><?php echo $single_lot[$col++]; ?></td>	<!-- 26 - Tax 2 Rate -->
											<td align="center" nowrap><?php echo $single_lot[$col++]; ?></td>	<!-- 27 - Tax 3 Name -->
										<tr>
										<?php
									}
								}
							?>
						</tbody>
						<?php
						if ( ! empty($lot_details))
						{
							if($all_valid)
							{
								?>
								<tr>
									<td colspan='<?php echo $col;?>' align='center'>
										<form name='form_save_excel_lots' id='form_save_excel_lots' >
											<input type='hidden' id='auctionid' name='auctionid' value='<?php echo $auctionid; ?>' />
											<input type='submit' class='btn btn-primary' value='Save Lots' id='save_excel_export' name='save_excel_export' />
										</form>
									</td>
								</tr>
								<?php
							}
						}
						?>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>

<!--===================================================-->
<!--END CONTENT CONTAINER-->
<script>

$(function(){

	var vRules = {};
	var vMessages = {};
	$("#form_save_excel_lots").validate(
	{
		rules: vRules,
		messages: vMessages,
		submitHandler: function(form)
		{
			$(form).ajaxSubmit(
			{
				url:"<?php echo base_url('auctioneer/auction/save_excel');?>",
				type: 'post',
				dataType:'json',
				cache: false,
				clearForm: true,
				success: function (response)
				{
					if(response.status=="success")
					{
						Display_msg(response.message,response.status);
						setTimeout(function()
						{
							window.location.href="<?php echo base_url('auctioneer/auction/view_lots/'.$auctionid);?>";
						}, 3000);
					}
					else
					{
						Display_msg(response.message,response.status);
						return false;
					}
				}
			});
		}
	})
});
</script>
