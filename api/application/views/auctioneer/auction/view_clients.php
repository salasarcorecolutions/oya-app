<!--CONTENT CONTAINER-->
<!--===================================================-->
<div id="content-container">
	<!--Page Title-->
	<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
	<div id="page-head">
	<div id="page-title">
		<h1 class="page-header text-overflow">Auction Bidders</h1>
	</div>
	<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
	<!--End page title-->

	<!--Breadcrumb-->
	<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
	<ol class="breadcrumb">
		<li><a href="<?php echo base_url('auction/index');?>">Home</a></li>
		<li class="active"><a href="<?php echo base_url('auctioneer/auction/index');?>">Auctions</a></li>
		<li class="active">Auction Bidders</h1</li>
	</ol>
</div>
	<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
	<!--End breadcrumb-->
	<input type="hidden" id="t11" name="auctionId">
	<div id="page-content">
		<div class="panel">
		
			<div class="panel-body">
					<div class="pad-btm form-inline">
					<div class="row">
						<div class="col-sm-12 table-toolbar-right">
							<?php 
							if ($this->common_model->check_permission('Client Add'))
							{
								?><a class="btn btn-primary btn-labeled fa fa-plus" onclick="addClient()" title="Add Client" name="btn_add_client" id="btn_add_client">Add Bidders</a><?php
							}
							if ($this->common_model->check_permission("Tender Client Add")) {
                                ?><a class="btn btn-primary btn-labeled fa fa-pencil" title="Edit Client Lots" onclick="editClientLots()" name="btn_edit_client_lots" id="btn_edit_client_lots">Edit Bidders Lots</a><?php
                            }
                            if ($this->common_model->check_permission("Tender Client Add")) {
                                ?><a class="btn btn-primary btn-labeled fa fa-credit-card" title="Edit CMD EMD" name="btn_edit_cmd_emd" onclick="editCmdEmd()" id="btn_edit_cmd_emd">Edit CMD EMD</a><?php
                            }
                            if ($this->common_model->check_permission("Tender Client Add")) {
                                ?><a class="btn btn-danger btn-labeled fa fa-trash" title="Delete Client" name="btn_delete_client" onclick="delete_client()" id="btn_delete_client">Delete Bidders</a><?php
                            }

							?>
						</div>
					</div>
				</div>
				<div class="table-responsive">
					<table class="table table-striped table-bordered table-hover no-footer dtr-inline ">
						<thead>
							<tr>
								<th style='width:3%;'>#</th>
								<th style='width:20%;'>BIDDER</th>
								<th>CMD/EMD AMOUNT</th>
								<th>BANK NAME</th>
								<th>REF NO</th>
								<th>DD DATE</th>
								<th>TOTAL LIMIT	</th>
								<th>DATE UPDATED</th>
								<th>LOTS</th>
							</tr>
						</thead>
						<tbody>
						<?php

						if ( ! empty($client_list))
						{
							$sr = 1;
							$edit_permission = false;
							$delete_permission = false;
							if ($this->common_model->check_permission('Client Edit'))
							{
								$edit_permission = true;
							}
							if ($this->common_model->check_permission('Client Delete'))
							{
								$delete_permission = true;
							}
							foreach ($client_list as $single_client)
							{
								?>
								<tr>
									<td><input type="radio" name="select_client" value="<?php echo $single_client['id']; ?>" /></td>
									<td><?php echo $single_client['compname'].' ['.$single_client['conperson'].']'; ?></td>
									<td>
										<?php
										if($single_client['emd_cmd_type'] == 'C')
										{
											echo "CMD : ".$single_client['cmdamt'];
										}
										else
										{
											echo "EMD : ".$single_client['emdamt'];
										}
										?>
									</td>
									<td>
										<?php
										if($single_client['emd_cmd_type'] == 'C')
										{
											echo $single_client['cmdbank'];
										}
										else
										{
											echo $single_client['emdbank'];
										}
										?>
									</td>
									<td>
										<?php
										if($single_client['emd_cmd_type'] == 'C')
										{
											echo $single_client['cmdddno'];
										}
										else
										{
											echo $single_client['emdddno'];
										}
										?>
									</td>
									<td>
										<?php
										if($single_client['emd_cmd_type'] == 'C')
										{
											echo $single_client['cmddate'];
										}
										else
										{
											echo $single_client['emddate'];
										}
										?>
									</td>
									<td><?php echo $single_client['totallimit']; ?></td>
									<td><?php echo $single_client['date_added']; ?></td>
									<td><?php echo $single_client['alloted_lots']; ?></td>
								</tr>
							<?php
							}
						}
						else
						{
							?>
							<tr>
								<td colspan='10' align='center'>No Records Found</td>
							</tr>
							<?php
						}
						?>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>

<!-- Modal -->
<div id="edit_client_lots_modal" class="modal fade" role="dialog">
	<div class="modal-dialog">
		<!-- Modal content-->
		<div class="modal-content">
      		<div class="modal-header">
        		<button type="button" class="close" data-dismiss="modal">&times;</button>
        		<h4 class="modal-title">Edit Client Lots</h4>
			  </div>
			  <form id="client_lots_form">
			<div class="modal-body">
				<input type="hidden" name="client_auction_id" value="<?php echo $auctionid; ?>" />
				<input id="auction_client_id" type="hidden" name="auction_client_id" />
				<div class="table-responsive">
					<table class="table table-bordered table-striped">
						<thead>
							<tr>
								<th>
									<input type="checkbox" class="check_all" name="check_all" id="check_all" onchange="check_all_checkbox(this);">
								</th>
								<th>LOT NO</th>
								<th>VENDOR LOT NO</th>
								<th style='width:40%'>PRODUCT</th>
								<th>STARTING BID</th>
								<th>INCREMENT</th>
								<th>CMD</th>
							</tr>
						</thead>
						<tbody>
						<?php
						$bidded_lot_array = array();
						if ( ! empty($bidded_lots))
						{
							$bidded_lot_array = explode(',',$bidded_lots);
						}

						if ( ! empty($lot_details))
						{
							foreach ($lot_details as $single_lot)
							{
								$checked = '';
							?>
							<tr>
								<td>
									<?php
									if ( ! in_array($single_lot['ynk_lotno'], $bidded_lot_array))
									{
									?>
										<input <?php echo $checked; ?> type="checkbox" class="checked_lot" name="client_lot[<?php echo $single_lot['ynk_lotno']?>]" id="client_lot<?php echo $single_lot['ynk_lotno']?>" value="<?php echo $single_lot['ynk_lotno']; ?>">
										<?php
									}
									?>
								</td>
								<td><?php echo $single_lot['ynk_lotno']; ?></td>
								<td><?php echo $single_lot['vendorlotno']; ?></td>
								<td><?php echo $single_lot['product']; ?></td>											
								<td>
									<?php
									if (empty($single_lot['startbid']))
									{
										?><input type="text" name="lot_start_bid[<?php echo $single_lot['ynk_lotno']?>]" lot_no="<?php echo $single_lot['ynk_lotno']?>" class="client_start_bid lot_<?php echo $single_lot['ynk_lotno']?>" value="0.00" style="width: 100px;" disabled /><?php
									}
									else
									{
										echo $single_lot['startbid'];
										?><input type="hidden" name="lot_start_bid[<?php echo $single_lot['ynk_lotno']?>]" lot_no="<?php echo $single_lot['ynk_lotno']?>" class="client_start_bid lot_<?php echo $single_lot['ynk_lotno']?>" value="0.00" style="width: 100px;" disabled /><?php
									}
									?>
								</td>
								<td><?php echo $single_lot['incrby']; ?></td>
								<td><?php echo $single_lot['cmd']; ?></td>
								</tr>
								<?php
						}
					}
					?>
					</tbody>
				</table>
			</div>
			</div>
			<div class="modal-footer">
				<button type="submit" class="btn btn-primary">Submit</button>
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
			</div>
			  </form>
    	</div>
  	</div>
</div>


<!-- Modal -->
<div id="edit_cmd_emd_modal" class="modal fade" role="dialog">
	<div class="modal-dialog">
		<!-- Modal content-->
		<div class="modal-content">
      		<div class="modal-header">
        		<button type="button" class="close" data-dismiss="modal">&times;</button>
        		<h4 class="modal-title">Edit CMD EMD</h4>
			  </div>
			  <form id="edit_cmd_emd_form">
			<div class="modal-body">
				<div class="row">
				<input type="hidden" name="cmd_emd_auction_id" value="<?php echo $auctionid; ?>" />
				<input id="cmd_emd_client_id" type="hidden" name="cmd_emd_client_id" />
					<label class="col-sm-3 control-label">Deposit Type : </label>
					<div class="col-sm-3">
						<select id="emd_cmd_type" class="form-control"  name="emd_cmd_type">
							<option value='C'>CMD</option>
							<option value='E'>EMD</option>
						</select>
					</div>
					<label class="col-sm-2 control-label">Amount <span class="text-danger">*</span>:</label>
					<div class="col-sm-3">
						<div class="form-group">
							<input class="form-control" type="text" name="amount" id="amount"  size="11" />
						</div>
					</div>
					<label class="col-sm-3 control-label">Bank Name : </label>
					<div class="col-sm-3">
						<div class="form-group">
							<input class="form-control" type="text" name="bank_name" id="bank_name" size="15" maxlength="100" />
						</div>
					</div>
					<!-- Ref No. is CMD / EMD No. -->
					<label class="col-sm-2 control-label">Ref No</label>
					<div class="col-sm-3">
						<div class="form-group">
							<input class="form-control" type="text" name="ref_no" id="ref_no" size="11" />
						</div>
					</div>
					<label class="col-sm-3 control-label">Date : </label>
					<div class="col-sm-3">
						<div class="form-group">
							<input class="form-control datepickerclass" readonly type="text" name="transaction_date" id="transaction_date" />
						</div>
					</div>
					<!-- Ref No. is CMD / EMD No. -->

					<label class="col-sm-2 control-label">Bid Limited ?</label>
					<div class="col-sm-3">
						<div class="form-group">
							<select class="form-control" type="text" name="bidlimited" id="bidlimited">
								<option value="0">NO</option>
								<option value="1">YES</option>
							</select>
						</div>
					</div>

					<div id="bidder_total_limit" style="display: none;">
						<label class="col-sm-2 control-label">Total Limit </label>
						<div class="col-sm-3">
							<div class="form-group">
								<input class="form-control" type="text" name="totallimit" id="totallimit" />
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="submit" class="btn btn-primary">Submit</button>
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
			</div>
			  </form>
    	</div>
  	</div>
</div>


<!--===================================================-->
<!--END CONTENT CONTAINER-->
<script>
$(document).ready(function(){
	$("#bidlimited").trigger('change');
})

function addClient()
{
	$.ajax({
		url: "<?php echo base_url('auctioneer/auction/check_auction_permission'); ?>",
		type: "POST",
		dataType:"json",
		cache: false,
		clearForm: false,
		data:{
			"id":"<?php echo $auctionid; ?>",
			"permission": "Client Add"
		},
		success:function(response){
			if (response.status == "success"){
				location.replace("<?php echo base_url('auctioneer/auction/add_edit_clients/'.$auctionid); ?>");
			} else {
				Display_msg(response.message,response.status);
			}
		}
	});
}
href="'.base_url('auctioneer/auction/add_edit_clients/'.$auctionid."/".$single_client['id']).'"

function edit_client(id,auctionid)
{
	if (id > 0)
	{
		var ans = confirm('Are you sure you want to edit this client?');
		if (ans){
			$.ajax({
				url: "<?php echo base_url('auctioneer/auction/check_auction_permission'); ?>",
				type: "POST",
				dataType:"json",
				cache: false,
				clearForm: false,
				data:{
					"id":"<?php echo $auctionid; ?>",
					"permission": "Client Edit"
				},
				success:function(response){
					if (response.status == "success"){
						$.ajax({
							url: "<?php echo base_url('auctioneer/auction/delete_auction_client'); ?>",
							type: "POST",
							dataType:"json",
							cache: false,
							clearForm: false,
							data:{"client_id":id,'auctionid':auctionid},
							success:function(response)
							{
								if(response.status == "success")
								{
									Display_msg(response.message,response.status);
									setTimeout(function(){
										window.location.reload();
									}, 3000);
								}
								else
								{
									Display_msg(response.message,response.status);
									return false;
								}

							}
						});
					} else {
						Display_msg(response.message,response.status);
					}
				}
			});
		}
	}
	else
	{
		Display_msg('Please Select Auction.','error');
		return false;
	}
}

function delete_client()
{
	var radioValue = $("input[name='select_client']:checked").val();
	if (radioValue)
	{
		var ans = confirm('Are you sure you want to delete this client?');
		if (ans)
		{
			$.ajax({
				url: "<?php echo base_url('auctioneer/auction/check_auction_permission'); ?>",
				type: "POST",
				dataType:"json",
				cache: false,
				clearForm: false,
				data:{
					"id":"<?php echo $auctionid; ?>",
					"permission": "Client Delete"
				},
				success:function(response){
					if (response.status == "success"){
						$.ajax({
							url: "<?php echo base_url('auctioneer/auction/delete_auction_client'); ?>",
							type: "POST",
							dataType:"json",
							cache: false,
							clearForm: false,
							data:{"client_id":radioValue,'auctionid':"<?php echo $auctionid; ?>"},
							success:function(response)
							{
								if(response.status == "success")
								{
									Display_msg(response.message,response.status);
									setTimeout(function()
									{
										window.location.reload();
									}, 3000);
								}
								else
								{
									Display_msg(response.message,response.status);
									return false;
								}
							}
						});
					} else {
						Display_msg(response.message,response.status);
					}
				}
			});
		}
	}
	else
	{
		Display_msg('Please Select Auction.','error');
		return false;
	}
}

function editClientLots()
{
	var radioValue = $("input[name='select_client']:checked").val();
	if (radioValue){
		$.ajax({
			url: "<?php echo base_url('auctioneer/auction/get_client_lots'); ?>",
			type: "POST",
			dataType:"json",
			cache: false,
			clearForm: false,
			data:{
				"id":"<?php echo $auctionid; ?>",
				"client_id": radioValue
			},
			success:function(response)
			{
				$('.checked_lot').each(function(){
					$(this).prop('checked',false);
					$(this).change();
				});
				if (response){
					$("#auction_client_id").val(radioValue);
					for (var i = 0; i < response.length; i++){
						$("#client_lot"+response[i]['ynk_lotid']). prop("checked", true);
					}
					$("#edit_client_lots_modal").modal('show');
				}
			},
			error: function(){
				alert('Problem in fetching client lots. Please try again.')
			}
		});
	} else {
		Display_msg('Please Select Client.','error');
	}
}

function check_all_checkbox(obj){
	if($(obj).is(":checked")){
		$('.checked_lot').each(function(){
			$(this).prop('checked',true);
			$(this).change();
		});
	} else {
		$('.checked_lot').each(function(){
			$(this).prop('checked',false);
			$(this).change();
		});
	}
}

$("#client_lots_form").validate({
	submitHandler: function(form){
		$(form).ajaxSubmit({
			url:"<?php echo base_url('auctioneer/auction/update_client_lots/');?>",
			type: 'post',
			dataType:'json',
			cache: false,
			success: function (response) {
				Display_msg(response.message,response.status);
				if (response.status == 'success'){
					setTimeout(function(){ location.reload(1); }, 3000);
				}
			}
		});
	}
})

$("#bidlimited").change(function(){
	if ($("#bidlimited").val() == 0){
		$("#bidder_total_limit").hide();
	} else {
		$("#bidder_total_limit").show();
	}
});

function editCmdEmd()
{
	var radioValue = $("input[name='select_client']:checked").val();
	if (radioValue){
		$.ajax({
			url: "<?php echo base_url('auctioneer/auction/get_client_cmd_emd'); ?>",
			type: "POST",
			dataType:"json",
			cache: false,
			clearForm: false,
			data:{
				"id":"<?php echo $auctionid; ?>",
				"client_id": radioValue
			},
			success:function(response)
			{
				if (response){
					clearEmdForm();
					$("#cmd_emd_client_id").val(radioValue);
					$("#emd_cmd_type").val(response.emd_cmd_type);
					if (response.emdamt > 0){
						var amount = response.emdamt;
						var bank_name = response.emdbank;
						var ref_no = response.emdddno;
						var date = response.emddate.split(' ');
						var transaction_date = date[0];
					} else {
						var amount = response.cmdamt;
						var bank_name = response.cmdbank;
						var ref_no = response.cmdddno;
						var date = response.cmddate.split(' ');
						var transaction_date = date[0];
					}
					$("#amount").val(amount);
					$("#bank_name").val(bank_name);
					$("#ref_no").val(ref_no);
					$("#transaction_date").val(transaction_date);
					if (response.bidlimited){
						$("#bidlimited").val(1).trigger('change');
					} else {
						$("#bidlimited").val(0).trigger('change');
					}
					$("#totallimit").val(response.totallimit);
					$("#edit_cmd_emd_modal").modal('show');
				}
			},
			error: function(){
				alert('Problem in fetching cmd emd data. Please try again.')
			}
		});
	} else {
		Display_msg('Please Select Client.','error');
	}
}

function clearEmdForm()
{
	$("#emd_cmd_type").val('');
	$("#amount").val('');
	$("#bank_name").val('');
	$("#ref_no").val('');
	$("#transaction_date").val('');
	$("#bidlimited").val('');
	$("#totallimit").val('');
}

$("#edit_cmd_emd_form").validate({
	submitHandler: function(form){
		$(form).ajaxSubmit({
			url:"<?php echo base_url('auctioneer/auction/update_client_cmd_emd_details/');?>",
			type: 'post',
			dataType:'json',
			cache: false,
			success: function (response) {
				Display_msg(response.message,response.status);
				clearEmdForm();
				$("#edit_cmd_emd_modal").modal('hide');
			}
		});
	}
})

function deleteClient()
{
	var radioValue = $("input[name='select_client']:checked").val();
	if (radioValue){
		$.ajax({
			url: "<?php echo base_url('auctioneer/auction/delete_aucton_client'); ?>",
			type: "POST",
			dataType:"json",
			cache: false,
			clearForm: false,
			data:{
				"id":"<?php echo $auctionid; ?>",
				"client_id": radioValue
			},
			success:function(response)
			{
				Display_msg(response.message,response.status);
				if (response.status == 'success'){
					setTimeout(function(){ location.reload(1); }, 3000);
				}
			},
			error: function(){
				alert('Problem in fetching cmd emd data. Please try again.')
			}
		});
	} else {
		Display_msg('Please Select Client.','error');
	}
}

$("#bidlimited").change(function(){
	if ($("#bidlimited").val() == 0){
		$("#bidder_total_limit").hide();
	} else {
		$("#bidder_total_limit").show();
	}
})



</script>
