<script language="javascript" type="text/javascript" src="<?php echo base_url('auctioneer_assets/js/ajax.js'); ?>"></script>
<style>

  .btn-group{
		margin-left:19px;
	}
	.fa-upload{
	margin-left:5px !important;
	}
	.auction-msg{
		float:right !important;
	}
	.auction-live{
		float:right !important;
	}
	.btn-group:first-child{
		margin-left:0px;
	}
  @media (max-width:990px){
	  a.btn{
		  margin-top:10px;
		  width:250px;
	  }
	  .btn-group{
		  margin-left:0;
	  }
	  .fa-upload{
		margin-left:0 !important;
	  }
	  .auction-msg{
		float:left !important;
	}
	.auction-live{
		float:left !important;
	}
  }
  .ellipsis {
        width: 200px;
    }

	.ellipsis span {
		display: -webkit-box;
		-webkit-line-clamp: 4;
		-webkit-box-orient: vertical;
		overflow: hidden;
	}
</style>
<!--CONTENT CONTAINER-->
<!--===================================================-->
<div id="content-container">
	<!--Page Title-->
	<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
	<div id="page-head">
	
	<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
	<!--End page title-->


	<!--Breadcrumb-->
	<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
	<ol class="breadcrumb">
		<li><a href="<?php echo base_url('auction/index'); ?>">Home</a></li>
		<li class="active">Auction</li>
	</ol>
</div>
	<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
	<!--End breadcrumb-->

	<input type="hidden" id="t11" name="auctionId">
	<div id="page-content">
		<div class="panel">
			<div class="panel-heading">
				<h3 class="panel-title">Auction</h3>
			</div>
			<div class="panel-body">
				<div class="pad-btm form-inline">
					<div class="row">
						<?php if ($this->common_model->check_permission('Add Auction')): ?>
							<a class="btn btn-sm btn-success btn-labeled fa fa-plus" href="javascript:void(0);" title="Add Auction" name="btn_add" id="btn_add" onclick="javascript:auction_actions('ADD')">Add</a>
						<?php endif; ?>
						<?php if ($this->common_model->check_permission('Edit Auction')): ?>
							<a class="btn btn-sm btn-primary btn-labeled fa fa-edit" href="javascript:void(0);" title="Edit Auction" name="btn_edit" id="btn_edit" onclick="javascript:auction_actions('EDIT')">Edit</a>
						<?php endif; ?>
						
							<a class="btn btn-sm btn-primary btn-labeled fa fa-money" href="javascript:void(0);" title="Update Auction Currency" name="update_currency" onclick="javascript:auction_actions('UPDATE_CURRENCY')">Currency Rate</a>
						
						<?php if ($this->common_model->check_permission('Lot View')): ?>
							<a class="btn btn-sm btn-primary btn-labeled fa fa-archive" href="javascript:void(0);" title="View Lots" name="btn_edit" id="btn_edit" onclick="javascript:auction_actions('LOTS')">Lots</a>
						<?php endif; ?>
						<?php if($this->common_model->check_permission('Client View')): ?>
							<a class="btn btn-sm btn-primary btn-labeled fa fa-user" href="javascript:void(0);" title="Auction Clients" name="btn_auction_client" onclick="javascript:auction_actions('AUCTION_CLIENT')">Bidders</a>
						<?php endif; ?>

						<?php if ($this->common_model->check_permission('Document View')): ?>
							<a class="btn btn-sm btn-primary btn-labeled fa fa-upload" href="javascript:void(0);" title="Upload Documents" name="btn_upload_docs" onclick="javascript:auction_actions('UPLOAD_DOCUMENTS')">Documents</a>
						<?php endif; ?>

						<?php if ($this->common_model->check_permission('Message View')): ?>
							<a class="btn btn-sm btn-primary btn-labeled fa fa-envelope" href="javascript:void(0);" title="Auction Message" name="btn_auction_msg" onclick="javascript:auction_actions('AUCTION_MESSAGE')">Messages</a>
						<?php endif; ?>

						<a class="btn btn-sm btn-primary btn-labeled fa fa-picture-o" href="javascript:void(0);" title="Auction Image" name="btn_auction_img" onclick="javascript:auction_actions('AUCTION_IMAGES')">Images</a>

						<?php if ($this->common_model->check_permission('Auction Live')): ?>
							<a class="btn btn-sm btn-success btn-labeled fa fa-gavel" href="javascript:void(0);" title="Live Auction" name="btn_live_auction" onclick="javascript:auction_actions('LIVE_AUCTION')">Live</a>
						<?php endif; ?>
						<a class="btn btn-sm btn-success btn-labeled fa fa-refresh" onclick="refresh_datatable();">Refresh </a>
						<?php if ($this->common_model->check_permission('Auction Delete')): ?>
							<a class="btn btn-sm btn-danger btn-labeled fa fa-trash" href="javascript:void(0);" title="Delete Auction" name="btn_delete" onclick="javascript:delete_auction()">Delete</a>
						<?php endif; ?>
						<a class="btn btn-sm btn-mint btn-labeled fa fa-paper-plane" href="javascript:void(0);" title="Auction Invitation" name="btn_delete" onclick="auctionInvitaion()">Send Invite</a>
					</div>
				</div>


				<form name="fwd_auction_form" method="post" >
					<div class="row filter">
						<div class="col-sm-2">
							<input class="form-control searchInput" placeholder="Sale No" type="text" name="search_sale_no" id="search_sale_no" value="" />
						</div>
						<div class="col-sm-2">
							<input class="form-control datepickerclass searchInput" readonly placeholder="Start Date" type="text" name="search_start_date" id="search_start_date" value="" />
						</div>
						<div class="col-sm-2">
							<input class="form-control datepickerclass searchInput" readonly placeholder="End Date" type="text" name="search_end_date" id="search_end_date" value="" />
						</div>
						<div class="col-sm-2">
						<select class="form-control searchInput" id="search_auction_type" name="search_auction_type">
							<option value="">--Select Auction Type</option>
							<option value="Forward Auction">Forward Auction</option>														
							<option value="Reverse Auction">Reverse Auction</option>														
							<option value="Dutch Auction">Dutch Auction</option>														
							<option value="Yankee Auction(Forward)">Yankee Auction (Forward)</option>														
							<option value="Yankee Auction(Reverse)">Yankee Auction (Reverse)</option>
						</select>
							
						</div>
						<div class="col-sm-2">
							<input class="form-control searchInput" placeholder="Particulars" type="text" name="search_particulars" id="search_particulars" value="" />
						</div>

						<div class="col-sm-2 ">
							<a class="btn btn-danger btn-labeled fa fa-ban" onclick="clearSearchFilters();">Clear Search </a>
						</div>
					</div>
					<div class="">
						<table callfunction="<?php echo base_url('auctioneer/auction/fetch');?>" class="table table-striped table-bordered table-hover no-footer dtr-inline dynamicTable">
							<thead>
								<tr>
									<th data-bSortable="false">#</th>
									<th>SALE NO</th>
									<th>START DATE</th>
									<th>END DATE</th>
									<th>AUCTION TYPE</th>
									<th>COMPANY NAME</th>
									<th>PLANT NAME</th>
									<th>PARTICULARS</th>
									<th>PUBLISHED</th>
									<th>LOGIC</th>
								</tr>
							</thead>
							<tbody>
							</tbody>
							<input type="hidden" id="vendorid" name="vendorid">
						</table>
					</div>

				</form>
			</div>
		</div>
	</div>
</div>

<!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
	<div class="modal-dialog">
		<!-- Modal content-->
		<div class="modal-content">
			<form id="send_invite_form">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title">Send Invitation To Bidders</h4>
				</div>
				<div class="modal-body">
					<input type="hidden" id="auc_id" name="auc_id" />
					<div class="col-md-4">
						<label>Invitation Send To</label>
					</div>
					<div class="col-md-8">
						<select class="form-control" id="invitation_type" name="invitation_type">
							<option value="B">Bidders added in Auction</option>
							<option value="S">Selected Bidders</option>
							<option value="A">All associate bidders</option>
						</select>
					</div>
					<div id="select_bidder_div" style="display: none;">
						<select id="selected_bidders" name="selected_bidders[]" multiple>
							<option value=""></option>
						</select>
					</div>
				</div>
				<div class="modal-footer">
					<button type="submit" class="btn btn-success">Submit</button>
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				</div>
			</form>
		</div>
	</div>
</div>


<!--===================================================-->
<!--END CONTENT CONTAINER-->
<script>
	$(document).ready(function(){
		$("#selected_bidders").select2({
			dropdownParent: $('#myModal'),
			width: "100%",
			placeholder: "Select Bidder"
		});
	})
function auction_actions(act){
	if(act == 'ADD')
	{
		window.location.href = '<?php echo base_url("auctioneer/auction/addedit") ?>';
	}
	else if (act == 'EDIT')
	{
		var id = $("#list_ynk_auctionid:checked").val();
		if ( ! id)
		{
			Display_msg('Please Select Auction.','error');
			return false;
		}
		else
		{
			$.ajax({
				url: "<?php echo base_url('auctioneer/auction/check_auction_permission'); ?>",
				type: "POST",
				dataType:"json",
				cache: false,
				clearForm: false,
				data:{
					"id":id,
					"permission": "Edit Auction"
				},
				success:function(response){
					if (response.status == "success"){
						window.location.href = '<?php echo base_url("auctioneer/auction/addedit/") ?>'+id;
					} else {
						Display_msg(response.message,response.status);
					}
				}
			});
		}
	}
	else if (act == 'UPDATE_CURRENCY')
	{
		var id = $("#list_ynk_auctionid:checked").val()
		if ( ! id)
		{
			Display_msg('Please Select Auction.','error');
			return false;
		}
		else
		{
			window.location.href = '<?php echo base_url("auctioneer/auction/currency_data/") ?>'+id;
		}
	}
	else if (act == 'UPLOAD_DOCUMENTS')
	{
		var id = $("#list_ynk_auctionid:checked").val()
		if ( ! id)
		{
			Display_msg('Please Select Auction.','error');
			return false;
		}
		else
		{
			$.ajax({
				url: "<?php echo base_url('auctioneer/auction/check_auction_permission'); ?>",
				type: "POST",
				dataType:"json",
				cache: false,
				clearForm: false,
				data:{
					"id":id,
					"permission": "Document View"
				},
				success:function(response){
					if (response.status == "success"){
						window.location.href = '<?php echo base_url("auctioneer/auction/upload_documents/") ?>'+id;
					} else {
						Display_msg(response.message,response.status);
					}
				}
			});
		}
	}
	else if (act == 'LOTS')
	{
		var id = $("#list_ynk_auctionid:checked").val()
		if ( ! id)
		{
			Display_msg('Please Select Auction.','error');
			return false;
		}
		else
		{
			$.ajax({
				url: "<?php echo base_url('auctioneer/auction/check_auction_permission'); ?>",
				type: "POST",
				dataType:"json",
				cache: false,
				clearForm: false,
				data:{
					"id":id,
					"permission": "Lot View"
				},
				success:function(response){
					if (response.status == "success"){
						window.location.href = '<?php echo base_url("auctioneer/auction/view_lots/") ?>'+id;
					} else {
						Display_msg(response.message,response.status);
					}
				}
			});
		}
	}
	else if (act == 'AUCTION_MESSAGE')
	{
		var id = $("#list_ynk_auctionid:checked").val()
		if ( ! id)
		{
			Display_msg('Please Select Auction.','error');
			return false;
		}
		else
		{
			$.ajax({
				url: "<?php echo base_url('auctioneer/auction/check_auction_permission'); ?>",
				type: "POST",
				dataType:"json",
				cache: false,
				clearForm: false,
				data:{
					"id":id,
					"permission": "Message View"
				},
				success:function(response){
					if (response.status == "success"){
						window.location.href = '<?php echo base_url("auctioneer/auction/auction_message/") ?>'+id;
					} else {
						Display_msg(response.message,response.status);
					}
				}
			});
		}
	}
	else if (act == 'AUCTION_IMAGES')
	{
		var id = $("#list_ynk_auctionid:checked").val()
		if(! id)
		{
			Display_msg('Please Select Auction.','error');
			return false;
		}
		else
		{
			window.location.href = '<?php echo base_url("auctioneer/auction/auction_images/y/") ?>'+id;
		}
	}
	else if (act == 'AUCTION_CLIENT')
	{
		var id = $("#list_ynk_auctionid:checked").val()
		if ( ! id)
		{
			Display_msg('Please Select Auction.','error');
			return false;
		}
		else
		{
			$.ajax({
				url: "<?php echo base_url('auctioneer/auction/check_auction_permission'); ?>",
				type: "POST",
				dataType:"json",
				cache: false,
				clearForm: false,
				data:{
					"id":id,
					"permission": "Client View"
				},
				success:function(response){
					if (response.status == "success"){
						window.location.href = '<?php echo base_url("auctioneer/auction/view_clients/") ?>'+id;
					} else {
						Display_msg(response.message,response.status);
					}
				}
			});
		}
	}
	else if (act == 'LIVE_AUCTION')
	{
		var id = $("#list_ynk_auctionid:checked").val()
		if( ! id)
		{
			Display_msg('Please Select Auction.','error');
			return false;
		}
		else
		{
			$.ajax({
				url: "<?php echo base_url('auctioneer/auction/check_auction_permission'); ?>",
				type: "POST",
				dataType:"json",
				cache: false,
				clearForm: false,
				data:{
					"id":id,
					"permission": "Auction Live"
				},
				success:function(response){
					if (response.status == "success"){
						window.location.href = '<?php echo base_url("auctioneer/auction/live_auction/") ?>'+id;
					} else {
						Display_msg(response.message,response.status);
					}
				}
			});
		}
	}
	else
	{
		Display_msg('Please Select Action.','error');
		return false;
	}
}

function delete_auction()
{
	var id = $("#list_ynk_auctionid:checked").val();
	if(! id)
	{
		Display_msg('Please Select Auction.','error');
		return false;
	}
	else
	{
		if(id > 0)
		{
			var ans = confirm('Are you sure you want to delete this auction?');
			if(ans)
			{
				$.ajax({
					url: "<?php echo base_url('auctioneer/auction/check_auction_permission'); ?>",
					type: "POST",
					dataType:"json",
					cache: false,
					clearForm: false,
					data:{
						"id":id,
						"permission": "Auction Delete"
					},
					success:function(response){
						if (response.status == "success"){
							$.ajax({
								url: "<?php echo base_url('auctioneer/auction/delete_auction'); ?>",
								type: "POST",
								dataType:"json",
								cache: false,
								clearForm: false,
								data:{"id":id},
								success:function(response)
								{
									if(response.status == "success")
									{
										Display_msg(response.message,response.status);
										refresh_datatable();
									}
									else
									{
										Display_msg(response.message,response.status);
										return false;
									}
								}
							});
						} else {
							Display_msg(response.message,response.status);
						}
					}
				});

			}
		}
		else
		{
			Display_msg('Please Select Auction.','error');
			return false;
		}
	}
}

function publish_status(id,status) {
	var msg = '';
	if(status == 'N') {
		msg = 'Are you sure that you want to unpublish this auction?';
	} else {
		msg = 'Are you sure that you want to publish this auction?';
	}
	var ans = confirm(msg);
	if(ans) {
		$.ajax({
			url: "<?php echo base_url('auctioneer/auction/check_auction_permission'); ?>",
			type: "POST",
			dataType:"json",
			cache: false,
			clearForm: false,
			data:{
				"id":id,
				"permission": "Auction Publish"
			},
			success:function(response){
				if (response.status == "success"){
					$.ajax({
						url: "<?php echo base_url('auctioneer/auction/update_publish_status'); ?>",
						type: "POST",
						dataType:"json",
						cache: false,
						clearForm: false,
						data:{"id":id,'status':status},
						success:function(response)
						{
							if(response.status == "success")
							{
								Display_msg(response.message,response.status);
								refresh_datatable();
							}
							else
							{
								Display_msg(response.message,response.status);
								return false;
							}
						}
					});
				} else {
					Display_msg(response.message,response.status);
				}
			}
		});
	}
}

$(function(){
	var vRules = {
		upload_file:{required:true}
	};
	var vMessages = {
		upload_file:{required:"<p class='text-danger'>Please select file.</p>"}
	};

	$("#upload_file_form").validate({
		rules: vRules,
		messages: vMessages,
		submitHandler: function(form){
			$(form).ajaxSubmit({
				url:"<?php echo base_url('auctioneer/auction/upload_file/');?>",
				type: 'post',
				dataType:'json',
				cache: false,
				clearForm: true,
				success: function (response) {
					if(response.status=="success")
					{
						Display_msg(response.message,response.status);
						refresh_datatable();
						$('#upload_file_modal').modal('hide');
						setTimeout(function()
						{
							window.location.href="<?php echo base_url('auctioneer/auction/index');?>";
						}, 3000);
					}
					else
					{
						Display_msg(response.message,response.status);
						return false;
					}
				}
			});
		}
	})
});

$("#send_invite_form").validate({
	submitHandler: function(form){
		$(form).ajaxSubmit({
			url:"<?php echo base_url('auctioneer/auction/send_auction_invitation/');?>",
			type: 'post',
			dataType:'json',
			cache: false,
			clearForm: true,
			success: function (response) {
				Display_msg(response.message,response.status);
				if (response.status == "success"){
					$("#invitation_type").val("B").trigger('change');
					$("#myModal").modal('hide');
				}
			}
		});
	}
})


function auctionInvitaion()
{
	var id = $("#list_ynk_auctionid:checked").val();
	if ( ! id)
	{
		Display_msg('Please Select Auction.','error');
		return false;
	}
	else
	{
		$("#auc_id").val(id);
		$("#myModal").modal('show');
	}
}

$("#invitation_type").change(function(){
	if ($("#invitation_type").val() == "S"){
		$.ajax({
			url: "<?php echo base_url('auctioneer/auction/bidders_list'); ?>",
			type: "POST",
			dataType:"json",
			success:function(response)
			{
				if (response.length > 0){
					var i, html = "<option value=''></option>";
					for (i = 0; i < response.length; i++){
						html += "<option value='"+response[i]['id']+"'>"+response[i]['compname']+" ("+response[i]['conperson']+")</option>";
					}
					$("#selected_bidders").html(html);
					$("#select_bidder_div").show();
				} else {
					Display_msg("No bidder is associated. Associate bidders to send the invitation","failed");
				}
			},
			error: function(){
				alert('Problem in getting bidders. Please try again.');
			}
		});
	} else {
		$("#select_bidder_div").hide();
	}
})

</script>