<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <style>
        .title_style{
            font-family: sans-serif;
            margin: 3% 0;
            color: #000084;
            text-align: center;
            width: 100%;
        }
        .img_style{
            display: flex;
            justify-content: center;
            width: 100%;
        }
        .img_style img{
            width: 35%;
        }
        @media (max-width: 780px) {
            .img_style img{
            width: 90%;
        }
        .title_style{
            margin: 25% 0;
            }
        }
    </style>
</head>
<body>
    <div class="title_style">
        <h1>You Are Not Authorized To Access This Page</h1>
    </div>
    <div class="img_style">
	  	<img src="<?php echo base_url();?>images/not_aut.png">
    </div>

</body>
</html>