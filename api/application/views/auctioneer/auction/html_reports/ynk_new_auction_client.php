<?php $this->load->view('auctioneer/auc_report_header')?>
<div class="panel panel-primary">
					<div class="panel-heading">
						<h3 class="panel-title">
						<strong>NEW AUCTION CLIENT REPORT FOR AUCTION :  <?php echo $common_auction_details['ynk_saleno']; ?> </strong>
						</h3>
					</div>
					<!--Horizontal Form-->
					<!--===================================================-->
					<div class="panel-body">
					<page>
					<page_footer>
							<table style='width: 100%;'>
								<tr>
									<td align='left' style='float:left;width:200px;'>
										<?php echo (new DateTime(null, new DateTimeZone($_SESSION['user_tz'])))->format('D dS \of M Y h:i:s A'); ?>
									</td>
									<td align='right' style='float:right;width:545px;'>
										<span style='font-size:20px;color:#2a5395;'><strong><?php echo PRODUCT ?></strong></span>
									</td>
								</tr>
							</table>
						</page_footer>
						<div class="table-responsive">
							<table class="table table-striped table-bordered table-hover no-footer dtr-inline">
								<tbody>
								
									<tr>

										<td width='50%' align='center' >
											<strong><?php echo strtoupper($common_auction_details['vendor_name'])?></strong>
										</td>
									
										<td width='50%' align='center' >
											<strong>AUCTION DETAILS</strong>
										</td>
									</tr>

								</tbody>		
							</table>
						</div>
						<div class="table-responsive">
							<table class="table table-striped table-bordered table-hover no-footer dtr-inline">
							<thead>
								<tr>
									<th>E-Auction Sale No</th>
									<th>Auction Type</th>
									<th>Auction Message </th>
									<th>Auction Start Date</th>
									<th>Auction Particulars </th>
									<th>No. of Lots</th>
								</tr>
							</thead>		
							<tbody>
							
								<tr>
									<td align='left'>
										<?php echo $common_auction_details['ynk_saleno']?>
									</td>
							
									<td align='left'>
										<?php echo $common_auction_details['auctiontype']?>
									</td>
							
									<td align='left'>
										<?php echo $common_auction_details['auction_msg']?>
									</td>
							
									<td align='left'>
										<?php echo $common_auction_details['sdt']?>
									</td>
								
									<td align='left'>
										<?php echo $common_auction_details['particulars']?>
									</td>
								
									<td align='left'>
										<?php echo $common_auction_details['no_of_lots']?>
									</td>
								</tr>
							</tbody>		
						</table>
						</div>
						<div class="table-responsive">
							<table class="table table-striped table-bordered table-hover no-footer dtr-inline">
							<thead>


			<tr>
				<th align='center'>SR. No</th>
				<th align='center'>COMPANY NAME</th>
				<th align='center'>ADDRESS</th>
				<th align='center'>EMAIL</th>
			</tr>
		</thead>
		<tbody>			
			<?php
				if ( ! empty($get_new_auction_client))
				{
					$count =1;
					foreach ($get_new_auction_client as $key=>$value)
					{
				?>
				<tr>
					<td align='center' class='wrappable' style="width:150px;">
						<?php echo $count++; ?>
					</td>
					<td align='center' class='wrappable' style="width:160px;" > 
					<?php

						echo $value['cname'];

					?>
					</td>
					<td align='center' class='wrappable' style="width:230px;"  >&nbsp; 
					<?php echo $value['addr']; ?>
					</td>
					<td align='center' class='wrappable' style="width:180px;" >&nbsp;
					<?php 
						echo $value['email'];
						
					?>
					</td>
				</tr>
			
				<?php
					}
					
				} else {
					?>
					<tr><td align='center' colspan="4" style="width: 735px;">No Record Found.</td></tr>
				<?php

				}
			
				?>
		</tbody>
	</table>
	</div>
</page>
</div>	
	<footer id="footer">
		<div class="hide-fixed pull-right pad-rgt">&#0169; <?php echo date("Y");?> A One Salasar</div>
		<p class="pad-lft">&nbsp;</p>
	</footer>
</body>
</html>