
<?php $this->load->view('auctioneer/auc_report_header')?>
<div class="panel panel-primary">
					<div class="panel-heading">
						<h3 class="panel-title">
						<strong>LOT EXTENSION HISTORY FOR AUCTION :  <?php echo $common_auction_details['ynk_saleno']; ?> </strong>
						</h3>
					</div>
					<!--Horizontal Form-->
					<!--===================================================-->
					<div class="panel-body">
					<page>
					<page_footer>
							<table style='width: 100%;'>
								<tr>
									<td align='left' style='float:left;width:200px;'>
									
									<?php echo (new DateTime(null, new DateTimeZone($_SESSION['user_tz'])))->format('D dS \of M Y h:i:s A'); ?>
									
									</td>
									<td align='right' style='float:right;width:545px;'>
										<span style='font-size:20px;color:#2a5395;'><strong><?php echo PRODUCT ?></strong></span>
									</td>
								</tr>
							</table>
						</page_footer>
						<div class="table-responsive">
							<table class="table table-striped table-bordered table-hover no-footer dtr-inline">
								<tbody>
								
									<tr>

										<td width='50%' align='center' >
											<strong><?php echo strtoupper($common_auction_details['vendor_name'])?></strong>
										</td>
									
										<td width='50%' align='center' >
											<strong>AUCTION DETAILS</strong>
										</td>
									</tr>

								</tbody>		
							</table>
						</div>
						<div class="table-responsive">
							<table class="table table-striped table-bordered table-hover no-footer dtr-inline">
							<thead>
								<tr>
									<th>E-Auction Sale No</th>
									<th>Auction Type</th>
									<th>Auction Message </th>
									<th>Auction Start Date</th>
									<th>Auction Particulars </th>
									<th>No. of Lots</th>
								</tr>
							</thead>		
							<tbody>
								

								<tr>
									<td align='left'>
										<?php echo $common_auction_details['ynk_saleno']?>
									</td>
							
									<td align='left'>
										<?php echo $common_auction_details['auctiontype']?>
									</td>
							
									<td align='left'>
										<?php echo $common_auction_details['auction_msg']?>
									</td>
								
									<td align='left'>
										<?php echo $common_auction_details['sdt']?>
									</td>
								
									<td align='left'>
										<?php echo $common_auction_details['particulars']?>
									</td>
								
									<td align='left'>
										<?php echo $common_auction_details['no_of_lots']?>
									</td>
								</tr>
							</tbody>		
						</table>
						</div>
					

	<?php
		if ( ! empty($get_fwd_lot_extension_history))
		{
			foreach ($get_fwd_lot_extension_history as $key=>$value)
			{
				
	?>	
			
			<div class="table-responsive">
							<table class="table table-striped table-bordered table-hover no-footer dtr-inline">

							<thead>	
			<tr>
				<th style="width:100px;" colspan="6" align='center'>
					<b>Lot No:</b>  <?php echo $value['lotno'] ;?>   <br> <b>Product:</b><?php echo $value['prod'];?>
				</th>
			</tr>
		</thead>
		<tbody>	
		<tr>
			<td style="width:100px;" align='center' class='wrappable' ><b>Start Time : </b> <br> <?php echo $value['st'];?> </td>
			<td style="width:100px;" align='center' class='wrappable'  ><b>Total Qty : </b> <br> <?php echo $value['tq'];?></td>
			<td style="width:100px;" align='center' class='wrappable' ><b>Star Bid : </b>  <?php echo $value['sb'];?></td>
			<td style="width:100px;" align='center' class='wrappable'> <b>Proxy Bid : </b> <?php echo $value['proxy'] ;?> </td>
			<td style="width:100px;" align='center' class='wrappable' style="width:173px;" ><b>Plant : </b> <?php echo $value['plant'] ;?> </td>
		</tr>
		<tr>
			<td align='center' class='wrappable' ><b>End Time : </b> <br> <?php echo $value['et'];?></td>
			<td align='center' class='wrappable' ><b>Bid Qty : </b> <br> <?php echo $value['bq'] ;?></td>
			<td align='center' class='wrappable' ><b>Min Decr : </b><?php echo $value['iv'];?></td>
			<td align='center' class='wrappable'><b>Proxy  Date : </b> <br> <?php echo $value['pd'];?></td>
			<td align='center' class='wrappable' style="width:173px;"   ><b>Material:  </b> <?php echo $value['vendorlotno'];?></td>
		</tr>
		<tr>
			<td align='center' class='wrappable' style='font-weight:bold;width:100px;'>SR NO</td>
			<td align='center' class='wrappable' style='font-weight:bold;width:120px;'>EXTENSION (HH:MM:SS)</td>
			<td align='center' class='wrappable' style='font-weight:bold;width:140px;'>UPDATED BY</td>
			<td align='center' class='wrappable' style='font-weight:bold;width:140px;'>UPDATED ON</td>
			<td align='center' class='wrappable' style='font-weight:bold;width:210px;'>RESASON</td>
		</tr>
		<?php 
			if (isset($value['lot_details']))
			{
				$i = 1;
				foreach ($value['lot_details']  as $lot_details)
				{
		?>
					<tr>		
						<td  align='center' class='wrappable' style="width:100px;"><?php echo $i ?></td>
						<td align='center' class='wrappable ' style="width:120px;"><?php echo gmdate("H:i:s",$lot_details['extended_by']); ?></td>
						<td align='center' class='wrappable' style="width:140px;" ><?php echo $lot_details['action_by']; ?> </td>
						<td align='center' class='wrappable' style="width:140px;" > <?php echo $lot_details['update_on']; ?> Hrs</td>
						<td align='center' class='wrappable' style="width:210px;" ><?php echo $lot_details['reason']; ?></td>
					</tr>
		
		<?php
				$i++;
				}
			}
			else
			{
		?>
				<tr>
					<td colspan='6' class='wrappable' align='center'> No Extension </td>
				</tr>
		<?php
				
			}
			
		
		?>
		</tbody>
	</table>
		</div>
	<?php 
	
	
			}
		}
		
	?>
</page>
