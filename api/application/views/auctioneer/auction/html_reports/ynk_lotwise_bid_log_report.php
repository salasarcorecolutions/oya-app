<?php $this->load->view('auctioneer/auc_report_header')?>
<div class="panel panel-primary">
					<div class="panel-heading">
						<h3 class="panel-title"><strong>LOTWISE BID LOG FOR AUCTION :  <?php echo $common_auction_details['ynk_saleno']; ?> </strong></h3>
					</div>
					<!--Horizontal Form-->
					<!--===================================================-->
					<div class="panel-body">


<page>
   <page_footer>
        <table style='width: 100%;'>
			<tr>
				<td align='left' style='float:left;width:200px;'>
			
				<?php echo (new DateTime(null, new DateTimeZone($_SESSION['user_tz'])))->format('D dS \of M Y h:i:s A'); ?>

				</td>
				<td align='right' style='float:right;width:545px;'>
					<span style='font-size:20px;color:#2a5395;'><strong><?php echo PRODUCT ?></strong></span>
				</td>
			</tr>
		</table>

    </page_footer>
	<div class="table-responsive">
		<table class="table table-striped table-bordered table-hover no-footer dtr-inline">
			<tbody>
			
				<tr>

					<td width='50%' align='center' >
						<strong><?php echo strtoupper($common_auction_details['vendor_name'])?></strong>
					</td>
				
					<td width='50%' align='center' >
						<strong>AUCTION DETAILS</strong>
					</td>
				</tr>

			</tbody>		
		</table>
	</div>
<div class="table-responsive">
	<table class="table table-striped table-bordered table-hover no-footer dtr-inline">
		<thead>
			<tr>
			<th>E-Auction Sale No</th>
			<th>Auction Type</th>
			<th>Auction Message </th>
			<th>Auction Start Date</th>
			<th>Auction Particulars </th>
			<th>No. of Lots</th>
			</tr>
		</thead>			
	<tbody>
			<tr>
				<td align='left'>
					<?php echo $common_auction_details['ynk_saleno']?>
				</td>
			
				<td align='left'>
					<?php echo $common_auction_details['auctiontype']?>
				</td>
			
				<td align='left'>
					<?php echo $common_auction_details['auction_msg']?>
				</td>
			
				<td align='left'>
					<?php echo $common_auction_details['sdt']?>
				</td>
			
				<td align='left'>
					<?php echo $common_auction_details['particulars']?>
				</td>
			
				<td align='left'>
					<?php echo $common_auction_details['no_of_lots']?>
				</td>
			</tr>
		</tbody>
	</table>
</div>
	<br>
	<?php

		if ( ! empty($get_lotwise_bid_log_report))
		{
			foreach ($get_lotwise_bid_log_report as $key=>$value)
			{
			?>
											<div class="table-responsive">
										<table class="table table-striped table-bordered table-hover no-footer dtr-inline">
				<thead>
					<tr>
						<th align='center' class='wrappable' colspan="7"  >
							<b>Lot No:</b>  <?php echo $value['lotno'] ;?>   <br> <b>Product:</b><?php echo $value['prod'];?>
						</th>
					</tr>
				</thead>
				<tbody>
				<tr>
					<td align='center' class='wrappable' ><b>Start Time : </b> <br> <?php echo $value['st'];?> </td>
					<td align='center' class='wrappable'  ><b>Total Qty : </b> <br> <?php echo $value['tq'];?></td>
					<td align='center' class='wrappable' ><b>Star Bid : </b>  <?php echo $value['sb'];?></td>
					<td align='center' class='wrappable'> <b>Proxy Bid : </b> <?php echo $value['proxy'] ;?> </td>
					<td align='center' colspan="3" class='wrappable'><b>Plant : </b> <?php echo $value['plant'] ;?> </td>
				</tr>
				<tr>
					<td align='center' class='wrappable' ><b>End Time : </b> <br> <?php echo $value['et'];?></td>
					<td align='center' class='wrappable' ><b>Bid Qty : </b> <br> <?php echo $value['bq'] ;?></td>
					<td align='center' class='wrappable' ><b>Min Decr : </b><?php echo $value['iv'];?></td>
					<td align='center' class='wrappable'><b>Proxy  Date : </b> <br> <?php echo ($value['pd']!='' && $value['pd']!='1970-01-01')? date('d-m-Y',strtotime($value['pd'])):''?></td>
					<td align='center' colspan="3" class='wrappable'><b>Material:  </b> <?php echo $value['vendorlotno'];?></td>
				</tr>
				<tr>
					<td  align='center' class='wrappable' style="font-weight:bold;width:200px;"  > Bidder Name</td>
					<td align='center' class='wrappable' style="font-weight:bold;width:120px;" > Date</td>
					<td align='center' class='wrappable' style="font-weight:bold;width:116px;" > Time</td>
					<td align='center' class='wrappable' style="font-weight:bold;width:116px;" > Quantity</td>
					<td align='center' class='wrappable' style="font-weight:bold;width:90px;" > Amount</td>
					<td align='center' class='wrappable' style="font-weight:bold;width:90px;" > Bid Amount</td>
					<td align='center' class='wrappable' style="font-weight:bold;width:90px;" > Type</td>
				</tr>

	<?php
				if (isset($value['lot_details']))
				{
					foreach ($value['lot_details']  as $lot_details)
					{

	?>
					<tr>
						<td  align='center' class='wrappable' style="width:200px;"><?php echo $lot_details['cname']; ?></td>
						<td align='center' class='wrappable ' style="width:120px;"><?php echo $lot_details['dt']; ?></td>
						<td align='center' class='wrappable' style="width:90px;" ><?php echo $lot_details['tm']; ?> Hrs.</td>
						<td align='center' class='wrappable' style="width:90px;" ><?php echo $lot_details['bid_qty']?></td>
						<td align='center' class='wrappable' style="width:90px;" > <?php echo $lot_details['bid_amt']; ?></td>
						<td align='center' class='wrappable' style="width:80px;" ><?php echo $lot_details['bid_amount']; ?></td>
						<td align='center' class='wrappable' style="width:90px;" ><?php echo $lot_details['typ']; ?></td>
					</tr>

	<?php
					}
					
				}
				else
				{
	?>
					<tr><td colspan='7' align='center' class='wrappable'>No Bid</td></tr>
	<?php
				}
	?>
				</tbody>
				</table>
</div>
			

	<?php
	
			}
		}

	?>
	
</page>
</div>
	<footer id="footer">
		<div class="hide-fixed pull-right pad-rgt">&#0169; <?php echo date("Y");?> A One Salasar</div>
		<p class="pad-lft">&nbsp;</p>
	</footer>
</body>
</html>
<script type="text/javascript">
$(document).ready(function(){
var live_auction = '<?php echo $live_auction_report?>';

if (live_auction ==='no')
 {	
	 alert();
	setTimeout(function() {
		location.reload();
	}, 1000);	
 }
});
</script>