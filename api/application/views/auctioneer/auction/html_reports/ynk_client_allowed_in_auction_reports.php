
<?php $this->load->view('auctioneer/auc_report_header')?>
<div class="panel panel-primary">
					<div class="panel-heading">
						<h3 class="panel-title">
						<strong>CLIENT ALLOWED IN AUCTION STATUS FOR AUCTION :  <?php echo $common_auction_details['ynk_saleno']; ?> </strong>
						</h3>
					</div>
					<!--Horizontal Form-->
					<!--===================================================-->
					<div class="panel-body">
					<page>
					<page_footer>
							<table style='width: 100%;'>
								<tr>
									<td align='left' style='float:left;width:200px;'>
									
									<?php echo (new DateTime(null, new DateTimeZone($_SESSION['user_tz'])))->format('D dS \of M Y h:i:s A'); ?>
									
									</td>
									<td align='right' style='float:right;width:545px;'>
										<span style='font-size:20px;color:#2a5395;'><strong><?php echo PRODUCT ?></strong></span>
									</td>
								</tr>
							</table>
						</page_footer>
						<div class="table-responsive">
							<table class="table table-striped table-bordered table-hover no-footer dtr-inline">
								<tbody>
								
									<tr>

										<td width='50%' align='center' >
											<strong><?php echo strtoupper($common_auction_details['vendor_name'])?></strong>
										</td>
									
										<td width='50%' align='center' >
											<strong>AUCTION DETAILS</strong>
										</td>
									</tr>

								</tbody>		
							</table>
						</div>
						<div class="table-responsive">
							<table class="table table-striped table-bordered table-hover no-footer dtr-inline">
							<thead>
								<tr>
									<th>E-Auction Sale No</th>
									<th>Auction Type</th>
									<th>Auction Message </th>
									<th>Auction Start Date</th>
									<th>Auction Particulars </th>
									<th>No. of Lots</th>
								</tr>
							</thead>		
							<tbody>
								

								<tr>
									<td align='left'>
										<?php echo $common_auction_details['ynk_saleno']?>
									</td>
							
									<td align='left'>
										<?php echo $common_auction_details['auctiontype']?>
									</td>
							
									<td align='left'>
										<?php echo $common_auction_details['auction_msg']?>
									</td>
								
									<td align='left'>
										<?php echo $common_auction_details['sdt']?>
									</td>
								
									<td align='left'>
										<?php echo $common_auction_details['particulars']?>
									</td>
								
									<td align='left'>
										<?php echo $common_auction_details['no_of_lots']?>
									</td>
								</tr>
							</tbody>		
						</table>
						</div>
						<div class="table-responsive">
							<table class="table table-striped table-bordered table-hover no-footer dtr-inline">

							<thead>


            <tr>
                <th><font size="1"><b>CLIENT ID</b></font></th>
                <th><font size="1"><b>NAME</b></font></th>
                <th><font size="1"><b>ADDRESS</b></font></th>
                <th><font size="1"><b>EMAIL</b></font></th>
                <th><b>LOT(s) PERMITTED</b></th>
            </tr>
		</thead>
		<tbody>
			<?php
				if ( ! empty($client_allowed))
				{
					$i=0;
					foreach ($client_allowed as $row)
					{
            ?>
                <tr>
					<td style="width:70px;"><?php echo str_replace('-',$row['bid'],$common_auction_details['ynk_saleno']); ?></td>
					<td style="width:70px;"><?php echo $row['cname']; ?></td>
					<td style="width:75px;"><?php echo $row['addr']; ?></td>
					<td style="width:135px;"><?php echo $row['em']; ?></td>
					<td style="width:264px;" valign=top>
					<div class="table-responsive">
							<table class="table table-striped table-bordered table-hover no-footer dtr-inline">

							<thead>
								<tr>
									<th >Lot No</th>
									<th >Description</th>
									<th >Qty</th>
									<th >Strating Bid</th>
									<th >Amount</th>
									<th >Remarks</th>
								</tr>
							</thead>
							<tbody>
                            <?php
                            if ( ! empty($client_lot_details[$row['bid']]))
                            {
                                foreach ($client_lot_details[$row['bid']] as $value){ ?>
                                <tr>
                                    <td ><?php echo $value['ynk_lotno']; ?></td>
                                    <td ><?php echo $value['product']; ?>&nbsp;</td>
                                    <td ><?php echo $value['totalqty'] ." ".$value['totunit'];?></td>
                                    <td ><?php echo $value['startbid']; ?></td>
                                    <td ><?php echo number_format($value['totalqty']*$value['startbid'],2); ?></td>
                                    <td ><?php echo $value['remarks']; ?>&nbsp;</td>
                                </tr>
							<?php }
							} else { ?>
							<tr>
								<td align='center' style="width: 262px;" colspan="6">No data is available</td>
							</tr>
							<?php } ?>
							</tbody>
						</table>
					</div>
                    </td>
                </tr>
				<?php

                    }
                }
				else
				{
			?>
                <tr>
                    <td align='center' class='wrappable' style="width:739px;" colspan="5">No Records Found</td>
                </tr>
			<?php
				}
			
			
			?>
		</tbody>
	</table>
	</div>
</page>
</div>	
	<footer id="footer">
		<div class="hide-fixed pull-right pad-rgt">&#0169; <?php echo date("Y");?> A One Salasar</div>
		<p class="pad-lft">&nbsp;</p>
	</footer>
</body>
</html>