<style>
tr {
	page-break-inside: avoid;
}
thead {
	display: table-header-group;
}
tfoot {
	display: table-row-group;
}
tr {
	page-break-inside: avoid;
}
td.wrappable,table.data_table td.wrappable {
    white-space: normal;	
}            
table td,table th {
	font-size:10px;
	
	
}
</style>


<page backbottom='110px' backtop="0px" backleft="0px" backright="0px" style="font-size:10px;">
   <page_footer>
        <table style='width: 100%;'>
			<tr>
				<td align='left' style='float:left;width:200px;'>
				Page :  [[page_cu]]/[[page_nb]] <br/>
				<?php echo (new DateTime(null, new DateTimeZone($_SESSION['user_tz'])))->format('D dS \of M Y h:i:s A'); ?>
				
				</td>
				<td align='right' style='float:right;width:545px;'>
					
				</td>
			</tr>
		</table>
    </page_footer>
	<br>
	<table border='0.5' cellspacing='0' cellpadding='0' width='100%'>
		<tbody>
			<tr>
				<td style="width:550px;" >
								
				</td>
				<td align='center' style="width:180px;">
					<span style='font-size:20px;color:#2a5395;'><strong><?php echo PRODUCT ?></strong></span>
				</td>
			</tr>
			<tr>
				<td colspan='2' align='center' >
					<strong>LOT WISE BID PROXY REPORT FOR AUCTION :  <?php echo $common_auction_details['saleno']; ?> </strong>
				</td>
			</tr>
			<tr>
				<td colspan='2' align='center' >
					<strong><?php echo strtoupper($common_auction_details['vendor_name'])?></strong>
				</td>
			</tr>
			<tr>
				<td colspan='2' align='center' >
					<strong>YANKEE AUCTION DETAILS</strong>
				</td>
			</tr>
			<tr>
				<td colspan='2' align='left'>
					<strong>E-Auction Sale No :</strong><?php echo $common_auction_details['saleno']?>
				</td>
			</tr>
			<tr>
				<td colspan='2' align='left'>
					<strong>Auction Type : </strong><?php echo $common_auction_details['auctiontype']?>
				</td>
			</tr>
			<tr>
				<td colspan='2' align='left'>
					<strong>Message : </strong><?php echo $common_auction_details['auction_msg']?>
				</td>
			</tr>
			<tr>
				<td colspan='2' align='left'>
					<strong>Auction Start Date : </strong><?php echo $common_auction_details['sdt']?>
				</td>
			</tr>
			<tr>
				<td colspan='2' align='left'>
					<strong>Particulars : </strong><?php echo $common_auction_details['particulars']?>
				</td>
			</tr>
			<tr>
				<td colspan='2' align='left'>
					<strong>No. of Lots : </strong><?php echo $common_auction_details['no_of_lots']?>
				</td>
			</tr>
		</tbody>
	</table>
	<br>
	<?php



			$count =1;
	?>
	<table border='0.5' cellspacing='0' cellpadding='0' width='100%'>
		<thead>
			<tr>
				<th align='center' bgcolor="#e0e0e0">SR. No</th>
				<th align='center' bgcolor="#e0e0e0">COMPANY NAME</th>
				<th align='center' bgcolor="#e0e0e0">EMAIL</th>
				<th align='center' bgcolor="#e0e0e0">LOT(s)</th>
			</tr>
		</thead>
		<tbody>

	<?php
    if (! empty($get_lotwise_bid_proxy)) {
        foreach ($get_lotwise_bid_proxy as $key=>$value) {
            if (isset($value['proxy'])) {
                ?>


			<tr>
			<td align='center' class='wrappable' style='width:40px;'><?php echo $count++?></td>
			<td align='center' class='wrappable' style='width:158px;'><?php echo $value['compname']?></td>
			<td align='center' class='wrappable' style='width:159px;'><?php echo $value['email']?></td>
			<td align='center' class='wrappable'>
				<table  border='0.5' cellspacing='0' cellpadding='0'>
					<tr>
						<th align='center' bgcolor="#e0e0e0" class='wrappable' style='width:50px;'>LOT NO</th>
						<th align='center' bgcolor="#e0e0e0" class='wrappable' style='width:150px;'>PROXY AMOUNT</th>
						<th align='center' bgcolor="#e0e0e0" class='wrappable' style='width:150px;'>DATE UPDATED</th>
					</tr>
					<?php

                        
                        
                            foreach ($value['proxy'] as $key=>$lot_proxy) {
                                ?>
								<tr>
									<td align='center' class='wrappable' style='width:50px;'><?php echo $lot_proxy['lotno']; ?></td>
									<td align='center' class='wrappable' style='width:145px;'><?php  echo $lot_proxy['proxy_amt']; ?></td>
									<td align='center' class='wrappable' style='width:150px;'><?php echo $lot_proxy['date_updated']; ?></td>
								</tr>
					
					<?php
                            } ?>
					
					
				</table>
			</td>
			</tr>	
			<?php
            } ?>
						
			
		
	<?php
        }
    }  else {
	?><tr><td colspan="4" align='center' style="width:735px;">No data is available</td></tr><?php } ?>
			</tbody>
		
	</table>
	<?php

	?>
</page>
