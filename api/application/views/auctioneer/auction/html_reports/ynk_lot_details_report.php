<?php $this->load->view('auctioneer/auc_report_header')?>
<div class="panel panel-primary">
					<div class="panel-heading">
						<h3 class="panel-title">
						<strong>LOT DETAILS FOR AUCTION :  <?php echo $common_auction_details['ynk_saleno']; ?> </strong>
						</h3>
					</div>
					<!--Horizontal Form-->
					<!--===================================================-->
					<div class="panel-body">
					<page>
					<page_footer>
							<table style='width: 100%;'>
								<tr>
									<td align='left' style='float:left;width:200px;'>
									
									<?php echo (new DateTime(null, new DateTimeZone($_SESSION['user_tz'])))->format('D dS \of M Y h:i:s A'); ?>
									
									</td>
									<td align='right' style='float:right;width:545px;'>
										<span style='font-size:20px;color:#2a5395;'><strong><?php echo PRODUCT ?></strong></span>
									</td>
								</tr>
							</table>
						</page_footer>
						<div class="table-responsive">
							<table class="table table-striped table-bordered table-hover no-footer dtr-inline">
								<tbody>
								
									<tr>

										<td width='50%' align='center' >
											<strong><?php echo strtoupper($common_auction_details['vendor_name'])?></strong>
										</td>
									
										<td width='50%' align='center' >
											<strong>AUCTION DETAILS</strong>
										</td>
									</tr>

								</tbody>		
							</table>
						</div>
						<div class="table-responsive">
							<table class="table table-striped table-bordered table-hover no-footer dtr-inline">
							<thead>
								<tr>
									<th>E-Auction Sale No</th>
									<th>Auction Type</th>
									<th>Auction Message </th>
									<th>Auction Start Date</th>
									<th>Auction Particulars </th>
									<th>No. of Lots</th>
								</tr>
							</thead>		
							<tbody>
								

								<tr>
									<td align='left'>
										<?php echo $common_auction_details['ynk_saleno']?>
									</td>
							
									<td align='left'>
										<?php echo $common_auction_details['auctiontype']?>
									</td>
							
									<td align='left'>
										<?php echo $common_auction_details['auction_msg']?>
									</td>
								
									<td align='left'>
										<?php echo $common_auction_details['sdt']?>
									</td>
								
									<td align='left'>
										<?php echo $common_auction_details['particulars']?>
									</td>
								
									<td align='left'>
										<?php echo $common_auction_details['no_of_lots']?>
									</td>
								</tr>
							</tbody>		
						</table>
						</div>
						<div class="table-responsive">
							<table class="table table-striped table-bordered table-hover no-footer dtr-inline">

							<thead>

			<tr>
				<th align="center">SR. NO.</th>
				<th align="center">AUCTIONEER<br> LOT NO.</th>
				<?php if ($is_lot_group == 'Yes'){ ?>
					<th align="center">SUB LOT NO</th>
				<?php }	?>
				<th align="center">VENDOR<br> LOT NO</th>
				<th align="center">DESCRIPTION</th>
				<th align="center">BST</th>
				<th align="center">BET</th>
				<th align="center">QUANTITY.</th>
				<th align="center" nowrap>BID <br> QUANTITY</th>
				<th align="center">START BID</th>
				<th align="center">INCREMENT</th>
				<th align="center">PART BID</th>
				<th align="center">MID PART<br> QUANTITY</th>
			</tr>
		</thead>
		<tbody>		
			<?php 
				if ( ! empty($lot_details))
				{
					$i=0;
					foreach ($lot_details as $row)
					{
						$c="";
						if ($is_lot_group == 'Yes'){
							if ( ! empty($sub_lot)){
								if ( ! empty($sub_lot[$row['ynk_lotno']])){
									$c = count($sub_lot[$row['ynk_lotno']])+1;
								}
							}
						} else {
							$c="";
						}
			?>
				<tr>
					<td style="width: 50px;"><?php echo ++$i; ?></td>
					<td style="width: 50px"><?php echo $row['ynk_lotno']; ?></td>
					<?php
						if ($is_lot_group == 'Yes'){
						?>
						<td></td>
						<?php
						}
						?>
					<td style="width: 50px"><?php echo $row['vendorlotno']; ?></td>
					<td style="width: 88px"><?php echo $row['product']; ?></td>
					<td style="width: 50px" nowrap  rowspan='<?php echo $c?>'><?php echo $row['stime']; ?> Hrs.</td>
					<td style="width: 50px" nowrap rowspan='<?php echo $c?>'><?php echo $row['etime']; ?>  Hrs.</td>
					<td style="width: 50px" nowrap rowspan='<?php echo $c?>'><?php echo $row['totalqty']." ".$row['totunit']; ?></td>
					<td style="width: 50px" rowspan='<?php echo $c?>'><?php echo $row['bidqty']." ".$row['bidunit']; ?></td>
					<td style="width: 50px" align="center" rowspan='<?php echo $c?>'><?php echo $row['startbid']; ?></td>
					<td style="width: 50px" align="center" rowspan='<?php echo $c?>'><?php echo $row['incrby'];?></td>
					<td style="width: 50px" align="center" rowspan='<?php echo $c?>'><?php echo ($row['part_bid'] == "Y") ? "Yes" : "No" ;?></td>
					<td style="width: 50px" align="center" rowspan='<?php echo $c?>'><?php echo $row['min_part_bid'];?></td>
				</tr>
				<?php 
					if ($is_lot_group == 'Yes'){ 
						if ( ! empty($sub_lot)){
							if ( ! empty($sub_lot[$row['ynk_lotno']])){
								foreach ($sub_lot[$row['ynk_lotno']] as $key=>$value){
		
					?>
						<tr>
							<td></td>
							<td></td>
							<td><?php echo $value['lotno']; ?></td>
							<td><?php echo $value['vlotno']; ?></td>
							<td><?php echo $value['prod']; ?></td>
						</tr>
					<?php
											}
										}
								}
						}
					}
				
			
				}
				else
				{
			?>
					<tr>
						<td align='center' class='wrappable' style="width:739px;" colspan="13">No Records Found</td>
					</tr>
			<?php
				}
			
			
			?>
		</tbody>
	</table>
</div>
</page>
</div>	
	<footer id="footer">
		<div class="hide-fixed pull-right pad-rgt">&#0169; <?php echo date("Y");?> A One Salasar</div>
		<p class="pad-lft">&nbsp;</p>
	</footer>
</body>
</html>