<?php $this->load->view('auctioneer/auc_report_header')?>
<div class="panel panel-primary">
					<div class="panel-heading">
						<h3 class="panel-title"><strong>BID SUMMARY REPORT FOR AUCTION :  <?php echo $common_auction_details['ynk_saleno']; ?> </strong></h3>
					</div>
					<!--Horizontal Form-->
					<!--===================================================-->
					<div class="panel-body">
						<div>
							<page_footer>
									<table style='width: 100%;'>
										<tr>
											<td align='left' style='float:left;width:200px;'>
											
											<?php echo (new DateTime(null, new DateTimeZone($_SESSION['user_tz'])))->format('D dS \of M Y h:i:s A'); ?>
											
											</td>
											<td align='right' style='float:right;width:545px;'>
												<span style='font-size:20px;color:#2a5395;'><strong><?php echo PRODUCT ?></strong>
											</td>
										</tr>
									</table>
								</page_footer>
								<div class="table-responsive">
										<table class="table table-striped table-bordered table-hover no-footer dtr-inline">
									<tbody>
									
										<tr>

											<td width='50%' align='center' >
												<strong><?php echo strtoupper($common_auction_details['vendor_name'])?></strong>
											</td>
										
											<td width='50%' align='center' >
												<strong>AUCTION DETAILS</strong>
											</td>
										</tr>
						
									</tbody>		
								</table>
							</div>
							<div class="table-responsive">
									<table class="table table-striped table-bordered table-hover no-footer dtr-inline">
										<thead>
											<tr>
											<th>E-Auction Sale No</th>
											<th>Auction Type</th>
											<th>Auction Message </th>
											<th>Auction Start Date</th>
											<th>Auction Particulars </th>
											<th>No. of Lots</th>
											</tr>
										</thead>	
										<tbody>
											<tr>
												<th><?php echo $common_auction_details['ynk_saleno']?></th>
												<td><?php echo $common_auction_details['auctiontype']?></td>
												<td><?php echo $common_auction_details['auction_msg']?></td>
												<td><?php echo $common_auction_details['sdt']?></td>
												<td><?php echo $common_auction_details['particulars']?></td>
												<td><?php echo $common_auction_details['no_of_lots']?></td>
											</tr>
										</tbody>
									</table>
							</div>	
								
								<?php
									if (!empty($get_bid_summary_report))
									{
										foreach ($get_bid_summary_report as $value)
										{
								?>
								<div class="table-responsive">
										<table class="table table-striped table-bordered table-hover no-footer dtr-inline">
												<thead>
													<tr>
														<th style="width:550px;" align='center' class='wrappable' colspan="8"  > <strong>Lot No :</strong> <?php echo $value['lotno'] ;?>   <br/> Product : <?php echo $value['prod'];?>
															</th>		
													</tr>
												</thead>
												<tbody>
													<tr>
														<td align='center' class='wrappable' style="width:75px;"><strong>Start Time : </strong> <br> <?php echo $value['st'];?> </td>
														<td align='center' class='wrappable' style="width:85px;"  colspan="2"><strong>Total Qty : </strong>  <?php echo $value['tq'];?></td>
														<td align='center' class='wrappable' style="width:70px;"  ><strong>Start Bid : </strong><br><?php echo $value['sb'];?></td>
														<td align='center' class='wrappable' style="width:105px;"  colspan="2"><strong>Proxy Bid : </strong><?php echo $value['proxy'];?></td>
														<td align='center' class='wrappable' style="width:105px;" colspan="2"> <strong> Plant : </strong> <?php echo $value['plant'];?> </td>
													</tr>
													<tr>
														<td align='center' class='wrappable' style="width:75px;"  > <strong>End Time : </strong>  <br> <?php echo $value['et'];?></td>
														<td align='center' class='wrappable' style="width:85px;"  colspan="2"> <strong>Bid Qty : </strong>  <?php echo $value['bq'];?></td>
														<td align='center' class='wrappable' style="width:70px;"    ><strong>Min. Incr : </strong><br><?php echo $value['iv'];?></td>
														<td align='center' class='wrappable' style="width:105px;"  colspan="2" ><strong>Proxy Date : </strong><?php echo $value['pd'];?>&nbsp;&nbsp;</td>
														<td align='center' class='wrappable' style="width:105px;"  colspan="2"><strong> Material : </strong><?php echo $value['vendorlotno'] ;?> </td>
													</tr>
													<tr>
														<td align='center' class='wrappable' style="font-weight:bold;width:50px;">STATUS</td>
														<td align='center' class='wrappable' style="font-weight:bold;width:160px;">BIDDER NAME</td>
														<td align='center' class='wrappable' style="font-weight:bold;width:80px;">DATE</td>
														<td align='center' class='wrappable' style="font-weight:bold;width:80px;" >TIME</td>
														<td align='center' class='wrappable' style="font-weight:bold;width:80px;">AMOUNT</td>
														<td align='center' class='wrappable' style="font-weight:bold;width:80px;">BID AMOUNT</td>
														<td align='center' class='wrappable' style="font-weight:bold;width:80px;">ALOTTED QTY</td>
														<td align='center' class='wrappable' style="font-weight:bold;width:70px;"> NO. OF BIDS </td>
													</tr    >

													<?php
														if (isset($value['lot_details']))
														{
															foreach ($value['lot_details'] as $key=>$lot_bid)
															{
													?>
															<tr>
																<td align="center" class='wrappable' style="width:50px;color:red;" >
																	<?php echo $lot_bid['h']; ?>
																</td>
																<td align='center' class='wrappable' style="width:160px;"  >
																	<?php echo $lot_bid['cname']; ?>
																</td>
																<td align="center" class='wrappable' style="width:70px;" >
																	<?php echo $lot_bid['dt']; ?>
																</td>
																<td align="center" class='wrappable' style="width:80px;" >
																	<?php echo $lot_bid['tm']; ?> Hrs.
																</td>
																<td align="center"class='wrappable' style="width:80px;">
																	<?php echo $lot_bid['amt']; ?>
																</td>
																<td align="center"class='wrappable' style="width:80px;">
																	<?php echo $lot_bid['bid_amount']; ?>
																</td>
																<td align="center"class='wrappable' style="width:80px;">
																	<?php echo $lot_bid['bid_qty']; ?>
																</td>
																<td align="center" class='wrappable' style="width:70px;"  >
																	<?php echo $lot_bid['nob']?> 
																</td>
															</tr>
																
													<?php
															}

														}
														else
														{
													?>
															<tr>
																<td align="center" class='wrappable' colspan="7"  style="width:700px;" >No Bid</td>
															</tr>
													
													<?php
														}
													
													?>
												
												</tbody>
										</table>
								</div>
									
											
								
								<?php
											
										}
										
									}
								
								
								?>
								
							</div>
					</div>
</div>	
	<footer id="footer">
		<div class="hide-fixed pull-right pad-rgt">&#0169; <?php echo date("Y");?> A One Salasar</div>
		<p class="pad-lft">&nbsp;</p>
	</footer>
</body>
</html>