<?php $this->load->view('auctioneer/auc_report_header')?>
<div class="panel panel-primary">
					<div class="panel-heading">
						<h3 class="panel-title">
						<strong>H1 AND TOTAL BIDS FOR AUCTION :  <?php echo $common_auction_details['ynk_saleno']; ?> </strong>
						</h3>
					</div>
					<!--Horizontal Form-->
					<!--===================================================-->
					<div class="panel-body">
					<page>
					<page_footer>
							<table style='width: 100%;'>
								<tr>
									<td align='left' style='float:left;width:200px;'>
									
									<?php echo (new DateTime(null, new DateTimeZone($_SESSION['user_tz'])))->format('D dS \of M Y h:i:s A'); ?>
									
									</td>
									<td align='right' style='float:right;width:545px;'>
										<span style='font-size:20px;color:#2a5395;'><strong><?php echo PRODUCT ?></strong></span>
									</td>
								</tr>
							</table>
						</page_footer>
						<div class="table-responsive">
							<table class="table table-striped table-bordered table-hover no-footer dtr-inline">
								<tbody>
								
									<tr>

										<td width='50%' align='center' >
											<strong><?php echo strtoupper($common_auction_details['vendor_name'])?></strong>
										</td>
									
										<td width='50%' align='center' >
											<strong>AUCTION DETAILS</strong>
										</td>
									</tr>

								</tbody>		
							</table>
						</div>
						<div class="table-responsive">
							<table class="table table-striped table-bordered table-hover no-footer dtr-inline">
							<thead>
								<tr>
									<th>E-Auction Sale No</th>
									<th>Auction Type</th>
									<th>Auction Message </th>
									<th>Auction Start Date</th>
									<th>Auction Particulars </th>
									<th>No. of Lots</th>
								</tr>
							</thead>		
							<tbody>
								<tr>
									<td align='left'>
										<?php echo $common_auction_details['ynk_saleno']?>
									</td>
							
									<td align='left'>
										<?php echo $common_auction_details['auctiontype']?>
									</td>
							
									<td align='left'>
										<?php echo $common_auction_details['auction_msg']?>
									</td>
								
									<td align='left'>
										<?php echo $common_auction_details['sdt']?>
									</td>
								
									<td align='left'>
										<?php echo $common_auction_details['particulars']?>
									</td>
								
									<td align='left'>
										<?php echo $common_auction_details['no_of_lots']?>
									</td>
								</tr>
							</tbody>		
						</table>
						</div>
						<div class="table-responsive">
							<table class="table table-striped table-bordered table-hover no-footer dtr-inline">

							<thead>
								<tr>
									<th align='center'>LOT NO</th>
									<th align='center'>DESCRIPTION</th>
									<th align='center'>PLANT</th>
									<th align='center'>TOTAL QTY.</th>
									<th align='center'>STARTING RATE</th>
									<th align='center'>BID QTY</th>
									<th align='center'>ALLOTED QTY</th>
									<th align='center'><?php echo $bid_logic=='F' ? 'H1' : 'L1'; ?> RATE</th>
									<th align='center'><?php echo $bid_logic=='F' ? 'H1' : 'L1'; ?> BIDDER</th>
									<th align='center'>NO OF BIDS</th>
									<th align='center'>NO OF BIDDER</th>
									<th align='center'>NO OF PARTICIPANTS</th>
								</tr>
							</thead>
							<tbody>		
								<?php 
									if ( ! empty($get_h1_n_total_bids))
									{
										foreach ($get_h1_n_total_bids as $key=>$value)
										{
											
								?>
										<tr>
											<td align='center' class='wrappable' style="width:25px;" >
												<?php echo $value['lotno'] ; ?>
											</td>
											<td align='center' class='wrappable' style="width:76px;" >
												<?php echo $value['prod']; ?>
											</td>
											<td align='center' class='wrappable' style="width:65px;" >
												<?php echo $value['plant']; ?>
											</td>
											<td  align='center' class='wrappable' style="width:50px;" >
												<?php echo $value['total_quat_unit']; ?>
											</td>
											<td  align='center' class='wrappable' style="width:50px;" >
												<?php echo $value['start_bid']; ?>
											</td>
											<td  align='center' class='wrappable' style="width:40px;" >
												<?php echo $value['bid_quat_unit']; ?>
											</td>
											<td  align='center' class='wrappable' style="width:40px;" >
												<?php echo ! empty($value['bid_qty']) ? $value['bid_qty'] : 0; ?>
											</td>
											<td  align='center' class='wrappable' style="width:60px;"  >
												<?php echo $value['amt']; ?>
											</td>
											<td  align='center' class='wrappable' style="width:70px;" >
												<?php echo $value['cname']; ?>
											</td>
											<td align='center' class='wrappable' style="width:40px;" >
												<?php echo $value['nob']; ?>
											</td>
											<td align='center' class='wrappable' style="width:40px;" >
												<?php echo $value['cnt_bidders']; ?>
											</td>
											<td align='center' class='wrappable' style="width:40px;" >
												<?php echo $value['nop']; ?>
											</td>
										</tr>

								<?php
								
										}
									} 
									else
									{
								?>
										<tr>
											<td align='center' class='wrappable' style="width:740px;" colspan="12">No Records Found</td>
										</tr>
								<?php
									}
								
								
								?>
							</tbody>
						</table>
						</div>
					</page>
</div>	
	<footer id="footer">
		<div class="hide-fixed pull-right pad-rgt">&#0169; <?php echo date("Y");?> A One Salasar</div>
		<p class="pad-lft">&nbsp;</p>
	</footer>
</body>
</html>