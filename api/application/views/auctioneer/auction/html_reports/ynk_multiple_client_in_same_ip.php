<?php $this->load->view('auctioneer/auc_report_header')?>
<div class="panel panel-primary">
					<div class="panel-heading">
						<h3 class="panel-title">
						<strong>MULTIPLE CLIENTS WITH SAME IP REPORT FOR AUCTION :  <?php echo $common_auction_details['ynk_saleno']; ?> </strong>
						</h3>
					</div>
					<!--Horizontal Form-->
					<!--===================================================-->
					<div class="panel-body">
					<page>
					<page_footer>
							<table style='width: 100%;'>
								<tr>
									<td align='left' style='float:left;width:200px;'>
										<?php echo (new DateTime(null, new DateTimeZone($_SESSION['user_tz'])))->format('D dS \of M Y h:i:s A'); ?>
									</td>
									<td align='right' style='float:right;width:545px;'>
										<span style='font-size:20px;color:#2a5395;'><strong><?php echo PRODUCT ?></strong></span>
									</td>
								</tr>
							</table>
						</page_footer>
						<div class="table-responsive">
							<table class="table table-striped table-bordered table-hover no-footer dtr-inline">
								<tbody>
								
									<tr>

										<td width='50%' align='center' >
											<strong><?php echo strtoupper($common_auction_details['vendor_name'])?></strong>
										</td>
									
										<td width='50%' align='center' >
											<strong>AUCTION DETAILS</strong>
										</td>
									</tr>

								</tbody>		
							</table>
						</div>
						<div class="table-responsive">
							<table class="table table-striped table-bordered table-hover no-footer dtr-inline">
							<thead>
								<tr>
									<th>E-Auction Sale No</th>
									<th>Auction Type</th>
									<th>Auction Message </th>
									<th>Auction Start Date</th>
									<th>Auction Particulars </th>
									<th>No. of Lots</th>
								</tr>
							</thead>		
							<tbody>
							
								<tr>
									<td align='left'>
										<?php echo $common_auction_details['ynk_saleno']?>
									</td>
							
									<td align='left'>
										<?php echo $common_auction_details['auctiontype']?>
									</td>
							
									<td align='left'>
										<?php echo $common_auction_details['auction_msg']?>
									</td>
							
									<td align='left'>
										<?php echo $common_auction_details['sdt']?>
									</td>
								
									<td align='left'>
										<?php echo $common_auction_details['particulars']?>
									</td>
								
									<td align='left'>
										<?php echo $common_auction_details['no_of_lots']?>
									</td>
								</tr>
							</tbody>		
						</table>
						</div>

	
						<div class="table-responsive">
							<table class="table table-striped table-bordered table-hover no-footer dtr-inline">
							<thead>		
								<tr>
									<th align='center'>SR. NO</th>
									<th align='center'>BIDDER NAME</th>
									<th align='center'>ADDRESS</th>
									<th align='center'>EMAIL</th>
									<th align='center'>IP ADDRESS</th>
								</tr>
							</thead>
							<tbody>				
								<?php
									if ( ! empty($get_multiple_client_in_same_ip))
									{
										$count =1;
										foreach ($get_multiple_client_in_same_ip as $key=>$value)
										{
								?>
										<tr>
											<td align='center' class='wrappable' style='width:150px;'>
												<?php echo $count++; ?>
											</td>
											<td align='center' class='wrappable' style='width:150px;'>
												<?php echo $value['compname']; ?>
											</td>
											<td align='center' class='wrappable' style='width:150px;'>
												<?php echo $value['adddress']; ?>
											</td>
											<td align='center' class='wrappable' style='width:130px;'>
												<?php echo $value['email']; ?>
											</td>
											<td align='center' class='wrappable' style='width:145px;'>
												<?php echo $value['ip']; ?>
											</td>
										</tr>
								
								<?php
										}
								
									}
									else
									{
								?>
										<tr class='hd'><td  align='center' class='wrappable' style='width:750px;' colspan="5">No Records Found</td></tr>
								<?php
									}
								
									?>
							</tbody>
						</table>
						</div>
		</page>
	</div>	
		<footer id="footer">
			<div class="hide-fixed pull-right pad-rgt">&#0169; <?php echo date("Y");?> A One Salasar</div>
			<p class="pad-lft">&nbsp;</p>
		</footer>
</body>
</html>