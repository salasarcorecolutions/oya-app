<style>
	 a.btn{
		text-align:left;
		margin-top:5px;
		width: 100%;
	}
	.media-left{
		padding: 0px 10px;
	}
	.middle input {
		position: absolute;
		left: 11px;
		top: 4px;
	}
	.media-left i{
		font-size: 26px;
	}
	.middle{
		position: relative;
		border: 3px solid #25476a;
		border-radius: 7px;
	}
	.text-15px{
		font-size: 15px;
	}
</style>
<!--CONTENT CONTAINER-->
<!--===================================================-->
<div id="content-container">
	<!--Page Title-->
	<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
	<div id="page-title">
		<h1 class="page-header text-overflow"><?php echo $pagetitle ?></h1>
	</div>
	<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
	<!--End page title-->
	<!--Breadcrumb-->
	<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
	<ol class="breadcrumb">
		<li><a href="admin.php">Home</a></li>
		<li><a class="active"><?php echo $pagetitle ?></a></li>
	</ol>
	<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
	<!--End breadcrumb-->

	<!--Page content-->
	<!--===================================================-->
	<div id="page-content">
		<div class="row">
			<div class="col-sm-12">
				<!--Primary Panel-->
				<!--===================================================-->
				<div class="panel panel-primary">
					<div class="panel-heading">
						<h3 class="panel-title"><?php echo $pagetitle ?></h3>
					</div>
					<!--Horizontal Form-->
					<!--===================================================-->
					<div class="panel-body" >
						<form method="post" name="frm" id="frm"   target="_blank" >
							<div class="col-lg-12">
								<div class="col-sm-3">
									<div class="form-group">
										<label class="control-label">Auction Id :</label>
										<select class="form-control" size="1" name="cboauctionid" id="cboauctionid" onChange="javascript:ajax_get_auction_lot_client()" >
										<option value="" >--select--</option>
										<?php
										if ( ! empty($get_all_ynk_auction))
										{
											foreach($get_all_ynk_auction as $single_auc)
											{
											?>
											<option value="<?php echo $single_auc['ynk_auctionid']?>"><?php echo $single_auc['ynk_saleno'] .'  [' .$single_auc['start_date'].' ]  [ '.$single_auc['vendor_name'].' ]  [ '.$single_auc['particulars'].' ] '?></option>											<?php
											}
										}
										?>
										</select>
									</div>
								</div>
								<div class="col-sm-3">
									<div class="form-group">
										<label class="control-label">Lot No :</label>
										<select class="form-control" size="1" name="lotno" id="lotno" >
											<option value="" >--select--</option>
										</select>
									</div>
								</div>
								<div class="col-sm-2">
									<div class="form-group">
										<a id='ckbCheckAll' class="btn btn-info btn-labeled fa fa-check-circle-o" >Select All</a>
									</div>
								</div>
								<div class="col-sm-2">
									<div class="form-group">
										<?php //if (has_page_permission("yankee_auction","allow_yankee_reports_excel")) { ?><input type="button" class="btn btn-success btn-labeled fa fa-file-excel-o" value="Export Excel" onclick="export_in_excel()"> <?php //} ?>
									</div>
								</div>
								<div class="col-sm-2">
									<div class="form-group">
										<?php //if (has_page_permission("yankee_auction","allow_yankee_reports_pdf")) { ?><input type="button" class="btn btn-danger btn-labeled fa fa-file-pdf-o" value="Export PDF" onclick="export_in_pdf()"> <?php //} ?>
									</div>
								</div>
								<input type='hidden' name="auctionid" id="auctionid" value="">
								<input type='hidden' name="clientid" id="clientid" value="">
							</div>
							<div id="checkboxlist">
								<div class="col-lg-12 normal_auction">
									<div class="row">
										<div class="form-group">
											<?php if(TRUE) echo "
												<div class='col-lg-3'>
													<label for='cmd_emd'>
														<div class='panel media middle pad-all'>
														<input type='checkbox' 	class='checked_lot' id='cmd_emd' name='cmd_emd' value='cmd_emd'>
															<div class='media-left'>
																<span class='icon-wrap icon-wrap-sm icon-circle bg-primary'>
																<i class='fa fa-file-text-o' aria-hidden='true'></i>
																</span>
															</div>
															<a id='cmd_emd_report_html' class='media-body'>
																<p class='text-15px mar-no text-semibold text-dark'>CMD EMD</p>
																<p class='text-primary mar-no'>Report</p>
															</a>
														</div>
													</label>
												</div>";
											?>

											<?php if(TRUE) echo "
												<div class='col-lg-3'>
													<label for='client_wise_lot'>
														<div class='panel media middle pad-all'>
															<input type='checkbox' class='checked_lot'  id='client_wise_lot' name='client_wise_lot' value='client_wise_lot'>
															<div class='media-left'>
																<span class='icon-wrap icon-wrap-sm icon-circle bg-primary'>
																<i class='fa fa-file-text-o' aria-hidden='true'></i>
																</span>
															</div>
															<div id='client_wise_lot_html' class='media-body'>
																<p class='text-15px mar-no text-semibold text-dark'>Client wise Lot</p>
																<p class='text-primary mar-no'>Report</p>
															</div>
														</div>
													</label>
												</div>";
											?>

											<?php if(TRUE) echo "

												<div class='col-lg-3'>
														<label for='lotwise_bid_log'>
															<div class='panel media middle pad-all'>
																<input type='checkbox' class='checked_lot' id='lotwise_bid_log' name='lotwise_bid_log' value='lotwise_bid_log'>
																<div class='media-left'>
																	<span class='icon-wrap icon-wrap-sm icon-circle bg-primary'>
																	<i class='fa fa-file-text-o' aria-hidden='true'></i>
																	</span>
																</div>
																<div id='lotwise_bid_lot_html' class='media-body'>
																	<p class='text-15px mar-no text-semibold text-dark'>Lotwise Bid Log</p>
																	<p class='text-primary mar-no'>Report</p>
																</div>
															</div>
														</label>
												</div>";
											?>

											<?php if(TRUE) echo "
													<div class='col-lg-3'>
															<label for='bid_summary'>
																<div class='panel media middle pad-all'>
																<input type='checkbox' class='checked_lot' id='bid_summary' name='bid_summary' value='bid_summary'>
																	<div class='media-left'>
																		<span class='icon-wrap icon-wrap-sm icon-circle bg-primary'>
																		<i class='fa fa-file-text-o' aria-hidden='true'></i>
																		</span>
																	</div>
																	<div id='bid_summary_html' class='media-body'>
																		<p class='text-15px mar-no text-semibold text-dark'>Bid Summary</p>
																		<p class='text-primary mar-no'>Report</p>
																	</div>
																</div>
															</label>
													</div>";
											?>

											<?php if(TRUE) echo "
												<div class='col-lg-3'>
														<label for='h1_n_total_bids'>
															<div class='panel media middle pad-all'>
															<input type='checkbox' class='checked_lot' id='h1_n_total_bids' name='h1_n_total_bids' value='h1_n_total_bids'>
																<div class='media-left'>
																	<span class='icon-wrap icon-wrap-sm icon-circle bg-primary'>
																	<i class='fa fa-file-text-o' aria-hidden='true'></i>
																	</span>
																</div>
																<div id='h1_total_bids_html' class='media-body'>
																	<p class='text-15px mar-no text-semibold text-dark'>H1 and total bids</p>
																	<p class='text-primary mar-no'>Report</p>
																</div>
															</div>
														</label>
												</div>";
											?>

											<?php if(TRUE) echo "
												<div class='col-lg-3'>
														<label for='clients_n_ips'>
															<div class='panel media middle pad-all'>
															<input type='checkbox' class='checked_lot'  id='clients_n_ips' name='clients_n_ips' value='clients_n_ips'>
																<div class='media-left'>
																	<span class='icon-wrap icon-wrap-sm icon-circle bg-primary'>
																	<i class='fa fa-file-text-o' aria-hidden='true'></i>
																	</span>
																</div>
																<div id='clients_n_ips_html' class='media-body'>
																	<p class='text-15px mar-no text-semibold text-dark'>Clients & their IPs</p>
																	<p class='text-primary mar-no'>Report</p>
																</div>
															</div>
														</label>
												</div>";
											?>

											<?php if(TRUE) echo "
												<div class='col-lg-3'>
														<label for='multiple_client_in_same_ip'>
															<div class='panel media middle pad-all'>
															<input type='checkbox' class='checked_lot' id='multiple_client_in_same_ip' name='multiple_client_in_same_ip' value='Client & Their Ips'>
																<div class='media-left'>
																	<span class='icon-wrap icon-wrap-sm icon-circle bg-primary'>
																	<i class='fa fa-file-text-o' aria-hidden='true'></i>
																	</span>
																</div>
																<div id='client_n_ips_html' class='media-body'>
																	<p class='text-15px mar-no text-semibold text-dark'>Client & Their Ips</p>
																	<p class='text-primary mar-no'>Report</p>
																</div>
															</div>
														</label>
												</div>";
											?>

											<?php if(TRUE) echo "
												<div class='col-lg-3'>
														<label for='terms_acceptance'>
															<div class='panel media middle pad-all'>
															<input type='checkbox' class='checked_lot'  id='terms_acceptance' name='terms_acceptance' value='terms_acceptance'>
																<div class='media-left'>
																	<span class='icon-wrap icon-wrap-sm icon-circle bg-primary'>
																	<i class='fa fa-file-text-o' aria-hidden='true'></i>
																	</span>
																</div>
																<div id='terms_and_acceptance_html' class='media-body'>
																	<p class='text-15px mar-no text-semibold text-dark'>Terms Acceptance Report</p>
																	<p class='text-primary mar-no'>Report</p>
																</div>
															</div>
														</label>
												</div>";
											?>


											<?php if(TRUE) echo "
												<div class='col-lg-3'>
														<label for='client_wise_bid_yes_no'>
															<div class='panel media middle pad-all'>
															<input type='checkbox' class='checked_lot' id='client_wise_bid_yes_no' name='client_wise_bid_yes_no' value='client_wise_bid_yes_no'>
																<div class='media-left'>
																	<span class='icon-wrap icon-wrap-sm icon-circle bg-primary'>
																	<i class='fa fa-file-text-o' aria-hidden='true'></i>
																	</span>
																</div>
																<div id='client_bidding_status_html' class='media-body'>
																	<p class='text-15px mar-no text-semibold text-dark'>Client Bidding Status</p>
																	<p class='text-primary mar-no'>Report</p>
																</div>
															</div>
														</label>
												</div>";
											?>

											<?php if(TRUE) echo "
												<div class='col-lg-3'>
													<label for='new_client'>
														<div class='panel media middle pad-all'>
														<input type='checkbox' class='checked_lot' id='new_client'  name='new_client' value='new_client'>
															<div class='media-left'>
																<span class='icon-wrap icon-wrap-sm icon-circle bg-primary'>
																<i class='fa fa-file-text-o' aria-hidden='true'></i>
																</span>
															</div>
															<div id='new_auction_client_html' class='media-body'>
																<p class='text-15px mar-no text-semibold text-dark'>New Auction Client</p>
																<p class='text-primary mar-no'>Report</p>
															</div>
														</div>
													</label>
												</div>";
											?>

											<?php if(TRUE) echo "
												<div class='col-lg-3'>
													<label for='new_client'>
														<div class='panel media middle pad-all'>
														<input type='checkbox' class='checked_lot' id='no_bid_lots' name='no_bid_lots' value='no_bid_lots'>
															<div class='media-left'>
																<span class='icon-wrap icon-wrap-sm icon-circle bg-primary'>
																<i class='fa fa-file-text-o' aria-hidden='true'></i>
																</span>
															</div>
															<div id='no_bid_lots_html' class='media-body'>
																<p class='text-15px mar-no text-semibold text-dark'>No Bid Lots</p>
																<p class='text-primary mar-no'>Report</p>
															</div>
														</div>
													</label>
												</div>";
											?>
											<?php if(TRUE) echo "
											<div class='col-lg-3'>
												<label for='lot_extension_history'>
													<div class='panel media middle pad-all'>
													<input type='checkbox' class='checked_lot'  id='lot_extension_history' name='lot_extension_history' value='lot_extension_history'>
														<div class='media-left'>
															<span class='icon-wrap icon-wrap-sm icon-circle bg-primary'>
															<i class='fa fa-file-text-o' aria-hidden='true'></i>
															</span>
														</div>
														<div id='lot_extension_history_html' class='media-body'>
															<p class='text-15px mar-no text-semibold text-dark'>Lot Extension History</p>
															<p class='text-primary mar-no'>Report</p>
														</div>
													</div>
												</label>
											</div>";
											?>
											<?php if(TRUE) echo "
												<div class='col-lg-3'>
													<label for='clients'>
														<div class='panel media middle pad-all'>
														<input type='checkbox' class='checked_lot'  id='clients' name='clients' value='clients'>
															<div class='media-left'>
																<span class='icon-wrap icon-wrap-sm icon-circle bg-primary'>
																<i class='fa fa-file-text-o' aria-hidden='true'></i>
																</span>
															</div>
															<div id='clients_html' class='media-body'>
																<p class='text-15px mar-no text-semibold text-dark'>Clients</p>
																<p class='text-primary mar-no'>Report</p>
															</div>
														</div>
													</label>
												</div>";
											?>
										<?php if(TRUE) echo "
											<div class='col-lg-3'>
												<label for='rejected_bids'>
													<div class='panel media middle pad-all'>
														<input type='checkbox' class='checked_lot' id='rejected_bids' name='rejected_bids' value='1'>
														<div class='media-left'>
															<span class='icon-wrap icon-wrap-sm icon-circle bg-primary'>
															<i class='fa fa-file-text-o' aria-hidden='true'></i>
															</span>
														</div>
														<div id='rejected_bids_html' class='media-body'>
															<p class='text-15px mar-no text-semibold text-dark'>Rejected Bids</p>
															<p class='text-primary mar-no'>Report</p>
														</div>
													</div>
												</label>
											</div>";
										?>
										<?php if (TRUE) echo "
											<div class='col-lg-3'>
												<label for='lotwise_bid_log_ip'>
													<div class='panel media middle pad-all'>
														<input type='checkbox' class='checked_lot'  id='lotwise_bid_log_ip' name='lotwise_bid_log_ip' value='lotwise_bid_log_ip'>
														<div class='media-left'>
															<span class='icon-wrap icon-wrap-sm icon-circle bg-primary'>
															<i class='fa fa-file-text-o' aria-hidden='true'></i>
															</span>
														</div>
														<div id='lotwise_bid_log_html' class='media-body'>
															<p class='text-15px mar-no text-semibold text-dark'>Lotwise Bid Log</p>
															<p class='text-primary mar-no'>With IP Address Report</p>
														</div>
													</div>
												</label>
											</div>";
										?>
										<?php if(TRUE) echo "
											<div class='col-lg-3'>
												<label for='lot_details'>
													<div class='panel media middle pad-all'>
														<input type='checkbox' class='checked_lot'  id='lot_details' name='lot_details' value='lot_details'>
														<div class='media-left'>
															<span class='icon-wrap icon-wrap-sm icon-circle bg-primary'>
															<i class='fa fa-file-text-o' aria-hidden='true'></i>
															</span>
														</div>
														<div id='lot_details_html' class='media-body'>
															<p class='text-15px mar-no text-semibold text-dark'>Lot Details</p>
															<p class='text-primary mar-no'>Report</p>
														</div>
													</div>
												</label>
											</div>";
										?>
										<?php if(TRUE) echo "
											<div class='col-lg-3'>
												<label for='client_allowed_in_auction'>
													<div class='panel media middle pad-all'>
													<input type='checkbox' class='checked_lot'  id='client_allowed_in_auction' name='client_allowed_in_auction' value='client_allowed_in_auction'>
														<div class='media-left'>
															<span class='icon-wrap icon-wrap-sm icon-circle bg-primary'>
															<i class='fa fa-file-text-o' aria-hidden='true'></i>
															</span>
														</div>
														<div id='client_allowed_in_auction_html' class='media-body'>
															<p class='text-15px mar-no text-semibold text-dark'>Client Allowed In Auction/Status</p>
															<p class='text-primary mar-no'>Report</p>
														</div>
													</div>
												</label>
											</div>";
										?>
										</div>
									</div>
									<div class="row">
										<div class="form-group">
											<?php if (TRUE) { ?><input type="button" class="btn btn-success" value="Export In Excel" onclick="export_in_excel()"> <?php } ?>
										</div>
									</div>
									<div class="row">
										<div class="form-group">
											<?php if (TRUE) { ?><input type="button" class="btn btn-info" value="Export In PDF" onclick="export_in_pdf()"> <?php } ?>
										</div>
									</div>
								</div>
							</div>
							<input type="hidden" name="save_file" id="save_file" value="0"/>
						</form>
					</div>
					<!--===================================================-->
					<!--End Horizontal Form-->
				</div>
				<!--===================================================-->
				<!--End Primary Panel-->
			</div>
		</div>
	</div>
	<!--===================================================-->
	<!--End page content-->
</div>
<!--===================================================-->
<!--END CONTENT CONTAINER-->
<script type="text/javascript">
function ajax_get_auction_lot_client()
{
	if (document.getElementById("cboauctionid").selectedIndex==0)
	{
		$("#lotno").html("<option value='' selected>--select--</option>");
	}
	else
	{
		var auction_id = document.getElementById("cboauctionid").value;
		if (auction_id !== "")
		{
			$.ajax({
				url:"<?php echo base_url('auctioneer/auction_reports/get_auction_lots_n_client');?>",
				type:"POST",
				dataType:"json",
				data:{"auction_id":auction_id},
				success:function(response){
					if (response.ynk.lots){
						$("#lotno").html('');
						$("#lotno").html('<option value="">--Select One--</option>');
						response.ynk.lots.forEach(function(element) {
							$("#lotno").append('<option value="'+element.ynk_lotno+'">'+element.ynk_lotno+'['+element.vendorlotno+']</option>');
						});
					}
				}
			});
		}
	}
}

function export_in_pdf(){
	var theForm = $("#frm");
	if (document.getElementById("cboauctionid").selectedIndex==0){
		alert("Please select Auction");
		document.getElementById("cboauctionid").focus();
		return false;
	}
	var chkArray = [];
	var selected;
	/* look for all checkboes that have a parent id called 'checkboxlist' attached to it and check if it was checked */
	$(".checked_lot:checkbox:checked").each(function() {
		chkArray.push($(this).val());
		/* we join the array separated by the comma */
	});

	/* check if there is selected checkboxes, by default the length is 1 as it contains one single comma */
	if (chkArray.length > 0){
		if (chkArray.length == 1){
			$('#save_file').val('0');
		} else {
			$('#save_file').val('1');
		}
		$(theForm).attr('action', '<?php echo base_url('auctioneer/auction_reports/get_ynk_reports_in_pdf')?>');
		$(theForm).submit();
	} else {
		alert("Please at least check one of the checkbox");
	}

}

function export_in_excel()
{
	var theForm = $("#frm");
	if (document.getElementById("cboauctionid").selectedIndex==0)
	{
		alert("Please select Auction");
		document.getElementById("cboauctionid").focus();
		return false;
	}
	var chkArray = [];

	/* look for all checkboes that have a parent id called 'checkboxlist' attached to it and check if it was checked */
	$("#checkboxlist input:checked").each(function() {
		chkArray.push($(this).val());
	});

	/* we join the array separated by the comma */
	var selected;
	selected = chkArray.join(',');

	/* check if there is selected checkboxes, by default the length is 1 as it contains one single comma */
	if (selected.length > 0){
		$(theForm).attr('action', '<?php echo base_url('auctioneer/auction_reports/get_ynk_reports_in_excel')?>');
		$(theForm).submit();
	} else {
		alert("Please at least check one of the checkbox");
	}

}
var clicked = false;
$("#ckbCheckAll").on("click", function() {
	$(".checked_lot").prop("checked", !clicked);
	clicked = !clicked;
	this.innerHTML = clicked ? 'Deselect All' : 'Select All';
	if ($('#ckbCheckAll').hasClass('btn-info')){
		$('#ckbCheckAll').removeClass('btn-info')
		$("#ckbCheckAll").addClass('btn-primary');
	} else {
		$('#ckbCheckAll').removeClass('btn-primary')
		$("#ckbCheckAll").addClass('btn-info');
	}
});

$("#cmd_emd_report_html").click(function(){
	var auction_id = document.getElementById("cboauctionid").value;
	var lotNo = $("#lotno").val();
	if (auction_id !== "")
	{
		$.ajax({
			url:"<?php echo base_url('auctioneer/auction_reports/check_current_auction');?>",
			type:"POST",
			dataType:"json",
			data:{"auction_id":auction_id},
			success:function(response){
				if (response.status == 'success'){
					window.open("<?php echo base_url().'auctioneer/auction_reports/cmd_emd_html/'; ?>"+auction_id+"/"+lotNo);
				} else {
					alert(response.message);
				}
			},
			error: function(){
				alert('Problem in fetching report please try again.');
			}
		});
	}
	else
	{
		alert('Select auction');
	}
})

$("#client_wise_lot_html").click(function(){
	var auction_id = document.getElementById("cboauctionid").value;
	var lotNo = $("#lotno").val();
	if (auction_id !== "")
	{
		$.ajax({
			url:"<?php echo base_url('auctioneer/auction_reports/check_current_auction');?>",
			type:"POST",
			dataType:"json",
			data:{"auction_id":auction_id},
			success:function(response){
				if (response.status == 'success'){
					window.open("<?php echo base_url().'auctioneer/auction_reports/client_wise_lot_html/'; ?>"+auction_id+"/"+lotNo);
				} else {
					alert(response.message);
				}
			},
			error: function(){
				alert('Problem in fetching report please try again.');
			}
		});
	}
	else
	{
		alert('Select auction');
	}
})

$("#lotwise_bid_lot_html").click(function(){
	var auction_id = document.getElementById("cboauctionid").value;
	var lotNo = $("#lotno").val();
	if (auction_id !== "")
	{
		$.ajax({
			url:"<?php echo base_url('auctioneer/auction_reports/check_current_auction');?>",
			type:"POST",
			dataType:"json",
			data:{"auction_id":auction_id},
			success:function(response){
				if (response.status == 'success'){
					window.open("<?php echo base_url().'auctioneer/auction_reports/lotwise_bid_lot_html/'; ?>"+auction_id+"/"+lotNo);
				} else {
					alert(response.message);
				}
			},
			error: function(){
				alert('Problem in fetching report please try again.');
			}
		});
	}
	else
	{
		alert('Select auction');
	}
})

$("#bid_summary_html").click(function(){
	var auction_id = document.getElementById("cboauctionid").value;
	var lotNo = $("#lotno").val();
	if (auction_id !== "")
	{
		$.ajax({
			url:"<?php echo base_url('auctioneer/auction_reports/check_current_auction');?>",
			type:"POST",
			dataType:"json",
			data:{"auction_id":auction_id},
			success:function(response){
				if (response.status == 'success'){
					window.open("<?php echo base_url().'auctioneer/auction_reports/bid_summary_html/'; ?>"+auction_id+"/"+lotNo);
				} else {
					alert(response.message);
				}
			},
			error: function(){
				alert('Problem in fetching report please try again.');
			}
		});
	}
	else
	{
		alert('Select auction');
	}
})

$("#h1_total_bids_html").click(function(){
	var auction_id = document.getElementById("cboauctionid").value;
	var lotNo = $("#lotno").val();
	if (auction_id !== "")
	{
		$.ajax({
			url:"<?php echo base_url('auctioneer/auction_reports/check_current_auction');?>",
			type:"POST",
			dataType:"json",
			data:{"auction_id":auction_id},
			success:function(response){

				if (response.status == 'success'){
					window.open("<?php echo base_url().'auctioneer/auction_reports/h1_total_bids_html/'; ?>"+auction_id+"/"+lotNo);
				} else {
					alert(response.message);
				}
			},
			error: function(){
				alert('Problem in fetching report please try again.');
			}
		});
	}
	else
	{
		alert('Select auction');
	}
})

$("#client_n_ips_html").click(function(){
	var auction_id = document.getElementById("cboauctionid").value;
	var lotNo = $("#lotno").val();
	if (auction_id !== "")
	{
		$.ajax({
			url:"<?php echo base_url('auctioneer/auction_reports/check_current_auction');?>",
			type:"POST",
			dataType:"json",
			data:{"auction_id":auction_id},
			success:function(response){
				
				if (response.status == 'success'){
					window.open("<?php echo base_url().'auctioneer/auction_reports/client_n_ips_html/'; ?>"+auction_id+"/"+lotNo);
				} else {
					alert(response.message);
				}
			},
			error: function(){
				alert('Problem in fetching report please try again.');
			}
		});
	}
	else
	{
		alert('Select auction');
	}
})

$("#clients_n_ips_html").click(function(){
	var auction_id = document.getElementById("cboauctionid").value;
	var lotNo = $("#lotno").val();
	if (auction_id !== "")
	{
		$.ajax({
			url:"<?php echo base_url('auctioneer/auction_reports/check_current_auction');?>",
			type:"POST",
			dataType:"json",
			data:{"auction_id":auction_id},
			success:function(response){
				
				if (response.status == 'success'){
					window.open("<?php echo base_url().'auctioneer/auction_reports/clients_n_ips_html/'; ?>"+auction_id+"/"+lotNo);
				} else {
					alert(response.message);
				}
			},
			error: function(){
				alert('Problem in fetching report please try again.');
			}
		});
	}
	else
	{
		alert('Select auction');
	}
})

$("#terms_and_acceptance_html").click(function(){
	var auction_id = document.getElementById("cboauctionid").value;
	var lotNo = $("#lotno").val();
	if (auction_id !== "")
	{
		$.ajax({
			url:"<?php echo base_url('auctioneer/auction_reports/check_current_auction');?>",
			type:"POST",
			dataType:"json",
			data:{"auction_id":auction_id},
			success:function(response){
				
				if (response.status == 'success'){
					window.open("<?php echo base_url().'auctioneer/auction_reports/terms_and_acceptance_html/'; ?>"+auction_id+"/"+lotNo);
				} else {
					alert(response.message);
				}
			},
			error: function(){
				alert('Problem in fetching report please try again.');
			}
		});
	}
	else
	{
		alert('Select auction');
	}
})

$("#client_bidding_status_html").click(function(){
	var auction_id = document.getElementById("cboauctionid").value;
	var lotNo = $("#lotno").val();
	if (auction_id !== "")
	{
		$.ajax({
			url:"<?php echo base_url('auctioneer/auction_reports/check_current_auction');?>",
			type:"POST",
			dataType:"json",
			data:{"auction_id":auction_id},
			success:function(response){
				
				if (response.status == 'success'){
					window.open("<?php echo base_url().'auctioneer/auction_reports/client_bidding_status_html/'; ?>"+auction_id+"/"+lotNo);
				} else {
					alert(response.message);
				}
			},
			error: function(){
				alert('Problem in fetching report please try again.');
			}
		});
	}
	else
	{
		alert('Select auction');
	}
})

$("#new_auction_client_html").click(function(){
	var auction_id = document.getElementById("cboauctionid").value;
	var lotNo = $("#lotno").val();
	if (auction_id !== "")
	{
		$.ajax({
			url:"<?php echo base_url('auctioneer/auction_reports/check_current_auction');?>",
			type:"POST",
			dataType:"json",
			data:{"auction_id":auction_id},
			success:function(response){
				
				if (response.status == 'success'){
					window.open("<?php echo base_url().'auctioneer/auction_reports/new_auction_client_html/'; ?>"+auction_id+"/"+lotNo);
				} else {
					alert(response.message);
				}
			},
			error: function(){
				alert('Problem in fetching report please try again.');
			}
		});
	}
	else
	{
		alert('Select auction');
	}
})

$("#no_bid_lots_html").click(function(){
	var auction_id = document.getElementById("cboauctionid").value;
	var lotNo = $("#lotno").val();
	if (auction_id !== "")
	{
		$.ajax({
			url:"<?php echo base_url('auctioneer/auction_reports/check_current_auction');?>",
			type:"POST",
			dataType:"json",
			data:{"auction_id":auction_id},
			success:function(response){
				
				if (response.status == 'success'){
					window.open("<?php echo base_url().'auctioneer/auction_reports/no_bid_lots_html/'; ?>"+auction_id+"/"+lotNo);
				} else {
					alert(response.message);
				}
			},
			error: function(){
				alert('Problem in fetching report please try again.');
			}
		});
	}
	else
	{
		alert('Select auction');
	}
})

$("#lot_extension_history_html").click(function(){
	var auction_id = document.getElementById("cboauctionid").value;
	var lotNo = $("#lotno").val();
	if (auction_id !== "")
	{
		$.ajax({
			url:"<?php echo base_url('auctioneer/auction_reports/check_current_auction');?>",
			type:"POST",
			dataType:"json",
			data:{"auction_id":auction_id},
			success:function(response){
				
				if (response.status == 'success'){
					window.open("<?php echo base_url().'auctioneer/auction_reports/lot_extension_history_html/'; ?>"+auction_id+"/"+lotNo);
				} else {
					alert(response.message);
				}
			},
			error: function(){
				alert('Problem in fetching report please try again.');
			}
		});
	}
	else
	{
		alert('Select auction');
	}
})

$("#clients_html").click(function(){
	var auction_id = document.getElementById("cboauctionid").value;
	var lotNo = $("#lotno").val();
	if (auction_id !== "")
	{
		$.ajax({
			url:"<?php echo base_url('auctioneer/auction_reports/check_current_auction');?>",
			type:"POST",
			dataType:"json",
			data:{"auction_id":auction_id},
			success:function(response){
				
				if (response.status == 'success'){
					window.open("<?php echo base_url().'auctioneer/auction_reports/clients_html/'; ?>"+auction_id+"/"+lotNo);
				} else {
					alert(response.message);
				}
			},
			error: function(){
				alert('Problem in fetching report please try again.');
			}
		});
	}
	else
	{
		alert('Select auction');
	}
})

$("#rejected_bids_html").click(function(){
	var auction_id = document.getElementById("cboauctionid").value;
	var lotNo = $("#lotno").val();
	if (auction_id !== "")
	{
		$.ajax({
			url:"<?php echo base_url('auctioneer/auction_reports/check_current_auction');?>",
			type:"POST",
			dataType:"json",
			data:{"auction_id":auction_id},
			success:function(response){
				
				if (response.status == 'success'){
					window.open("<?php echo base_url().'auctioneer/auction_reports/rejected_bids_html/'; ?>"+auction_id+"/"+lotNo);
				} else {
					alert(response.message);
				}
			},
			error: function(){
				alert('Problem in fetching report please try again.');
			}
		});
	}
	else
	{
		alert('Select auction');
	}
})

$("#lotwise_bid_log_html").click(function(){
	var auction_id = document.getElementById("cboauctionid").value;
	var lotNo = $("#lotno").val();
	if (auction_id !== "")
	{
		$.ajax({
			url:"<?php echo base_url('auctioneer/auction_reports/check_current_auction');?>",
			type:"POST",
			dataType:"json",
			data:{"auction_id":auction_id},
			success:function(response){
				
				if (response.status == 'success'){
					window.open("<?php echo base_url().'auctioneer/auction_reports/lotwise_bid_log_html/'; ?>"+auction_id+"/"+lotNo);
				} else {
					alert(response.message);
				}
			},
			error: function(){
				alert('Problem in fetching report please try again.');
			}
		});
	}
	else
	{
		alert('Select auction');
	}
})

$("#lot_details_html").click(function(){
	var auction_id = document.getElementById("cboauctionid").value;
	var lotNo = $("#lotno").val();
	if (auction_id !== "")
	{
		$.ajax({
			url:"<?php echo base_url('auctioneer/auction_reports/check_current_auction');?>",
			type:"POST",
			dataType:"json",
			data:{"auction_id":auction_id},
			success:function(response){
				
				if (response.status == 'success'){
					window.open("<?php echo base_url().'auctioneer/auction_reports/lot_details_html/'; ?>"+auction_id+"/"+lotNo);
				} else {
					alert(response.message);
				}
			},
			error: function(){
				alert('Problem in fetching report please try again.');
			}
		});
	}
	else
	{
		alert('Select auction');
	}
})

$("#client_allowed_in_auction_html").click(function(){
	var auction_id = document.getElementById("cboauctionid").value;
	var lotNo = $("#lotno").val();
	if (auction_id !== "")
	{
		$.ajax({
			url:"<?php echo base_url('auctioneer/auction_reports/check_current_auction');?>",
			type:"POST",
			dataType:"json",
			data:{"auction_id":auction_id},
			success:function(response){
				if (response.status == 'success'){
					window.open("<?php echo base_url().'auctioneer/auction_reports/client_allowed_in_auction_html/'; ?>"+auction_id+"/"+lotNo);
				} else {
					alert(response.message);
				}
			},
			error: function(){
				alert('Problem in fetching report please try again.');
			}
		});
	}
	else
	{
		alert('Select auction');
	}
})

</script>


<style type="text/css">
#livesearch
{
	margin:0px;
	width:200px;
	text-align: left;
	padding:5px;
	z-index:10;
	position:absolute;
	background:#fff;
}
#livesearch a{padding:2px;margin:1px;width:100%}
#livesearch a:hover{background:#00FF00}
#txt1
{
	margin:0px;
}
</style>
<style>
.btn-primary{
	min-width:288px !important;
}
</style>