
<!--CONTENT CONTAINER-->
<!--===================================================-->
<div id="content-container">
	<!--Page Title-->
	<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
	<div id="page-head">
	<div id="page-title">
		<h1 class="page-header text-overflow">Yankee Auction Message</h1>
	</div>
	<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
	<!--End page title-->


	<!--Breadcrumb-->
	<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
	<ol class="breadcrumb">
		<li><a href="<?php echo base_url('auction/index');?>">Home</a></li>
		<li class="active"><a href="<?php echo base_url('auctioneer/auction/index');?>">Yankee Auction</a></li>
		<li class="active">Yankee Auction Message</li>
	</ol>
</div>
	<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
	<!--End breadcrumb-->
	<input type="hidden" id="t11" name="auctionId">
	<div id="page-content">	
		<div class="float_btn">
			<a class="btn btn-primary btn-icon btn-circle add-tooltip" data-placement="top" data-toggle="tooltip" data-original-title="Back" href='<?php echo base_url('auctioneer/auction/index'); ?>' ><i class="fa fa-arrow-left" aria-hidden="true"></i></a>
		</div>
		<div class="panel">
			<div class="panel-heading">
				<h3 class="panel-title">Yankee Auction Message</h3>
			</div>
			
			<div class="panel-body">
				<form name="form_auc_msg" id="form_auc_msg" method="post" >
					<input type='hidden' id="ynk_auction_id" name="ynk_auction_id" value='<?php echo $auctionid; ?>' />
					<input type='hidden' id="ynk_message_id" name="ynk_message_id" value='' />
					<div class="row filter">
						<div class="col-sm-6">
							<textarea type='text' class="form-control" rows='4' placeholder="Message" name="mesg" id="mesg" ></textarea>
						</div>
						<div class="col-sm-2">
							<input type='text' class="form-control " placeholder="Message Order" name="ord" id="ord"  />
						</div>
						<div class="col-sm-2">
							<select class="form-control " name="sh" id="sh">
								<option value='S'>Show</option>
								<option value='H'>Hide</option>
							</select>
						</div>
						<div class="col-sm-2">
							<input type='submit' class="btn btn-info" placeholder="Submit" name="btn_submit" id="btn_submit" value='Add Message'  />
						</div>
					</div>
				</form>
				<br>
				<div class="table-responsive">
					<table class="table table-striped table-bordered table-hover no-footer dtr-inline ">
						<thead>
							<tr>
								<th>Sr</th>
								<th>Message</th>
								<th>Order </th>
								<th>Show/Hide </th>
								<th>Date</th>
								<th>Action</th>
							</tr>
						</thead>
						<tbody>
						<?php
						if( ! empty($message_list))
						{
							$sr = 1;
							$edit_permission = false;
							if ($this->common_model->check_permission('Message Edit'))
							{
								$edit_permission = true;
							}
							foreach ($message_list as $single_message)
							{ 
								$action = '';
								if($edit_permission)
								{
									$action = '<a href="javascript:void(0);" onclick="edit_message('.$single_message['ynk_message_id'].','.$auctionid.')">Edit</a>';
								}
								
								?>
								<tr>
									<td><?php echo $sr++; ?></td>
									<td><?php echo $single_message['mesg']; ?></td>
									<td><?php echo $single_message['ord']; ?></td>
									<td>
										<?php
										if($single_message['sh'] == 'S')
										{ 
                                        ?>
											<a href="javascript:void(0);" onclick="change_document_publish_status('<?php echo $single_message['ynk_message_id']; ?>','H');">
                                            <img src="<?php echo base_url('auctioneer_assets/images/publish.png');?>">
											</a> 
                                        <?php
										}
										else
										{ 
                                        ?>
											<a href="javascript:void(0);" onclick="change_document_publish_status('<?php echo $single_message['ynk_message_id']; ?>','S');">
                                            <img src="<?php echo base_url('auctioneer_assets/images/publish_x.png');?>">
											</a> 
                                        <?php
										}
										?>
									<td><?php echo $single_message['dttm']; ?></td>
									<td><?php echo $action;?></td>
								</tr>							
							<?php
							}
						}
						?>
						</tbody>
					</table>
				</div>
			</div>	
		</div>
	</div>	
</div>

<!--===================================================-->
<!--END CONTENT CONTAINER-->
<script>

function edit_message(id,auctionid)
{
	if(id == '' || id == undefined)
	{
		Display_msg('Invalid document for deletion.','error');
		return false;
	}
	else
	{
		if(id > 0)
		{
			var ans = confirm('Are you sure you want to edit this message?');
			if(ans)
			{
				$.ajax(
				{
					url: "<?php echo base_url('auctioneer/auction/get_auction_message'); ?>",
					type: "POST",
					dataType:"json",
					cache: false,
					clearForm: false,
					data:{"id":id,'auctionid':auctionid},
					success:function(response)
					{
						if(response.status == "success")
						{
							if(response.data)
							{
								$('#mesg').val(response.data.mesg);
								$('#ord').val(response.data.ord);
								$('#sh').val(response.data.sh);
								$('#message_id').val(response.data.message_id);
								$('#btn_submit').val('Update Message');
							}
							
						}
						else
						{
							Display_msg(response.message,response.status);
							return false;
						}
					}
				});
			}
		}
		else
		{
			Display_msg('Please Select Auction.','error');
			return false;
		}
	}
}

function change_document_publish_status(id,status){
	var msg = '';
	if(status == 'H')
	{
		msg = 'Are you sure to unpublish this message?';
	}
	else
	{
		msg = 'Are you sure to publish this message?';
	}
	var ans = confirm(msg);
	if(ans)
	{
		$.ajax(
		{
			url: "<?php echo base_url('auctioneer/auction/add_edit_message'); ?>",
			type: "POST",
			dataType:"json",
			cache: false,
			clearForm: false,
			data:{"message_id":id,'sh':status},
			success:function(response)
			{
				if(response.status == "success")
				{
					Display_msg(response.message,response.status);
					setTimeout(function()
					{
						window.location.reload();
					}, 3000);
				}
				else
				{
					Display_msg(response.message,response.status);
					return false;
				}
			}
		});
	}
}

$(function(){
	var vRules = {
		mesg:{required:true},
		ord:{required:true}
	};
	var vMessages = {
		mesg:{required:"<p class='text-danger'>Please enter message</p>"},
		ord:{required:"<p class='text-danger'>Please enter message order</p>"}
	};

	$("#form_auc_msg").validate({
		rules: vRules,
		messages: vMessages,
		submitHandler: function(form){
			$(form).ajaxSubmit({
				url:"<?php echo base_url('auctioneer/auction/add_edit_message/');?>",
				type: 'post',
				dataType:'json',
				cache: false,
				success: function (response) {               
					if(response.status=="success")
					{
						Display_msg(response.message,response.status);
						setTimeout(function()
						{
							window.location.reload();
						}, 3000);
					}
					else
					{	
						Display_msg(response.message,response.status);
						return false;
					}
				}
			});
		}
	})
});


</script>