<!--CONTENT CONTAINER-->
<!--===================================================-->
<div id="content-container">
	<!--Page Title-->
	<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
	<div id="page-head">
	<div id="page-title">
		<h1 class="page-header text-overflow"><?php echo $pagetitle ?></h1>
	</div>
	<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
	<!--End page title-->


	<!--Breadcrumb-->
	<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
	<ol class="breadcrumb">
		<li><a href="<?php echo base_url('auction/index');?>">Home</a></li>
		<li><a href="<?php echo base_url('auctioneer/auction/index');?>">Auction</a></li>
		<li><a href="<?php echo base_url('auctioneer/auction/view_lots/'.$auctionid);?>">Auction Lots</a></li>
		<li class="active"><?php echo $pagetitle ?></li>
	</ol>
</div>
	<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
	<!--End breadcrumb-->
	<input type="hidden" id="t11" name="auctionId">
	<div id="page-content">	
	<div class="float_btn">
		<a class="btn btn-primary btn-icon btn-circle add-tooltip" data-placement="top" data-toggle="tooltip" data-original-title="Back" href='<?php echo base_url('auctioneer/auction/view_lots/'.$auctionid); ?>' ><i class="fa fa-arrow-left" aria-hidden="true"></i></a>
	</div>
		<div class="panel">
			<div class="panel-heading">
				<h3 class="panel-title"><?php echo $pagetitle ?></h3>
			</div>
			
			<div class="panel-body">
				<fieldset>
					<legend>Auction Details</legend>
					<div class="table-responsive">
						<table class="table table-striped table-bordered table-hover no-footer dtr-inline">
							<thead>
								<tr>
									<th>Sale No</th>
									<th>Start Time</th>
									<th>End Time</th>
									<th>Particulars</th>
								</tr>
								<tr>
									<td><?php echo $auction_details['ynk_saleno'] ?></td>
									<td><?php echo date('d-m-Y H:i:s',strtotime($auction_details['start_date']." ".$auction_details['stime'])) ?></td>
									<td><?php echo date('d-m-Y H:i:s',strtotime($auction_details['end_date']." ".$auction_details['etime'])) ?></td>
									<td><?php echo $auction_details['particulars'] ?></td>
								</tr>
							</thead>
						</table>
					</div>				
				</fieldset>
				<fieldset>
					<legend>Lot Details</legend>
					<div class="table-responsive">					
						<table class="table table-striped table-bordered table-hover no-footer dtr-inline">
							<thead>
								<tr>
									<th>Lot No</th>
									<th>Vendor Lot No</th>
									<th>Description</th>
									<th>Start Date</th>
									<th>End Date</th>
								</tr>
								<tr>
									<td><?php echo $lot_details['ynk_lotno'] ?></td>
									<td><?php echo $lot_details['vendorlotno'] ?></td>
									<td><?php echo $lot_details['product'] ?></td>
									<td><?php echo date('d-m-Y H:i:s',strtotime($lot_details['lot_open_date'])) ?></td>
									<td><?php echo date('d-m-Y H:i:s',strtotime($lot_details['lot_close_date'])) ?></td>
								</tr>
							</thead>
						</table>
					</div>
				</fieldset>
				<fieldset>
					<legend>Upload Lot Image</legend>
					<form name="auction_lot_images" id="auction_lot_images" method="post" >
						<input type='hidden' id="auctionid" name="auctionid" value='<?php echo $auctionid; ?>' />
						<input type='hidden' id="lotno" name="lotno" value='<?php echo $lotno; ?>' />
						<div class="row filter">
							<div class="col-sm-2">
								<select class="form-control " name="image_size" id="image_size">
									<option value=''>Select Image Width</option>
									<option value='400'>400 PX</option>
									<option value='600'>600 PX</option>
									<option value='800'>800 PX</option>
									<option value='1000'>1000 PX</option>
								</select>
							</div>
							<div class="col-sm-2">
								<input type='file' class="form-control " name="upload_file[]" id="upload_file" multiple accept="image/*" onchange="check_no_of_files()" />
								<small>Max 5 Images at a time</small>
							</div>
							<div class="col-sm-2">
								<input type='submit' class="btn btn-info" placeholder="Submit" name="btn_submit" id="btn_submit"  />								
							</div>
						</div>
					</form>
				</fieldset>
				<fieldset>
					<legend>Uploaded Images</legend>
					<div class="row">
						<?php
						$s3 = new S3upload();
						if( ! empty($uploaded_images))
						{
							$cnt = 0;
							foreach($uploaded_images as $single_image)
							{
								$clear = '';
								if($cnt > 0 && $cnt % 4 == 0){
									$clear = 'clear:both;';
								}
								$url = $s3->getUrl($single_image['img_name'],'images/lotpic/yankee');
								if(is_file_exist($url))
								{
									?>
									<div style="position: relative;height:200px;" class='col-sm-3 col-md-3 col-lg-3 col-xs-3'>
										<a target='_blank' href='<?php echo $url;?>' style='border: 1px solid #ddd;display: inline-block;padding: 10px;margin:5px'>
											<img src='<?php echo $url;?>' style='border: 1px solid #ddd;max-height:170px;width:100%;'/>										
										</a>&nbsp;&nbsp;&nbsp;
										<a style="position: absolute;top: 6px;left: 19px;" title='Remove Image' href='javascript:void(0);' onclick="remove_lot_image('<?php echo $single_image['id'];?>',this)">
											<img src='<?php echo base_url('auctioneer_assets/images/publish_x.png'); ?>' />
										</a>
									</div>
									<?php
								}
								$cnt++;
							}
						}
						?>
					</div>
				</fieldset>
				
			</div>	
		</div>
	</div>	
</div>

<!--===================================================-->
<!--END CONTENT CONTAINER-->
<script>
function check_no_of_files()
{
	var fileUpload = $("#upload_file");
	if (parseInt(fileUpload.get(0).files.length) > 5)
	{
		alert("You can only upload a maximum of 5 files");
		$("#upload_file").val('');
		return false;
	}
	else
	{
		return true;
	}
}

function remove_lot_image(id,obj)
{
	if( ! id)
	{
		Display_msg('Invalid image for deletion.','error');
		return false;
	}
	else
	{
		if(id > 0)
		{
			var ans = confirm('Are you sure you want to delete this image?');
			if(ans){		
				$.ajax({
					url: "<?php echo base_url('auctioneer/auction/remove_lot_image'); ?>",
					type: "POST",
					dataType:"json",
					cache: false,
					clearForm: false,
					data:{"img_id":id,'auctionid':'<?php echo $auctionid; ?>','lotno':'<?php echo $lotno; ?>'},
					success:function(response){
						if(response.status == "success"){
							Display_msg(response.message,response.status);
							$(obj).parent().remove();
						} else {
							Display_msg(response.message,response.status);
							return false;
						}
					}
				});
			}
		}
		else
		{
			Display_msg('Please Select Auction.','error');
			return false;
		}
	}
}

$(function(){
	var vRules = {
		image_size:{required:true}
	};
	var vMessages = {
		image_size:{required:"<p class='text-danger'>Please select image width.</p>"},
	};

	$("#auction_lot_images").validate({
		rules: vRules,
		messages: vMessages,
		submitHandler: function(form){
			if(check_no_of_files()){
				$(form).ajaxSubmit({
					url:"<?php echo base_url('auctioneer/auction/upload_lot_image/'.$auctionid.'/'.$lotno);?>", 
					type: 'post',
					dataType:'json',
					cache: false,
					enctype: 'multipart/form-data',
					clearForm: false,
					success: function (response) {               
						if(response.status=="success"){
							Display_msg(response.message,response.status);
							setTimeout(function(){
								window.location.reload();
							}, 3000);
						} else {	
							Display_msg(response.message,response.status);
							return false;
						}
					}
				});
			}
		}
	})
});


</script>