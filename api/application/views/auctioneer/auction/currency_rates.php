<script language="javascript" type="text/javascript" src="<?php echo base_url('assets/js/ajax.js'); ?>"></script> 

<!--CONTENT CONTAINER-->
<!--===================================================-->
<div id="content-container">
	<!--Page Title-->
	<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
	<div id="page-head">
	
	<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
	<!--End page title-->


	<!--Breadcrumb-->
	<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
	<ol class="breadcrumb">
		<li><a href="<?php echo base_url('auction/index'); ?>">Home</a></li>
		<li class="active">Auction Currency</li>
    </ol>
	</div>
	<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
	<!--End breadcrumb-->

	<div id="page-content">
    <div class="float_btn">
		<a class="btn btn-primary btn-icon btn-circle add-tooltip" data-placement="top" data-toggle="tooltip" data-original-title="Back" href='<?php echo base_url('auctioneer/auction/index'); ?>' ><i class="fa fa-arrow-left" aria-hidden="true"></i></a>
	</div>
		<div class="panel">
			<div class="panel-heading">
				<h3 class="panel-title">Auction Currency</h3>
			</div>
			<div class="panel-body">
            <form name="upload_currency_form" id="upload_currency_form">
				<input type='hidden' id='auction_id' name='auction_id' value='<?php echo $id; ?>'/>
				<div>
					<table class="table table-striped table-bordered table-hover table-responsive">
						<thead>
                            <tr>
                                
                                <th>ALLOWED CURRENCY</th>
								<th>DEFAULT CURRENCY</th>
                                <th>CONVERSION RATE</th>
                            </tr>
						</thead>
						<tbody id="currency_conversion_table">
                            <?php foreach ($currency_data['secondary'] as $value){ ?>
                            <tr>
                               
                                <td><?php echo $value['currency_name']; ?></td> 
								<td><?php echo $currency_data['primary']['currency_name']; ?></td>
                                <td><input class="form-control" min="0" step="any" type="number" name="<?php echo $value['id']; ?>" value="<?php echo $value['currency_conversion_rate']; ?>" placeholder="Please enter the conversion rate" /></td>
                            </tr>
                            <?php } ?>
						</tbody>
					</table>
                </div>
                <center>
                    <button type="submit" id="currency_btn" class="btn btn-success">Update</button>
                    <a class="btn btn-danger" href="<?php echo base_url('auctioneer/auction/index');?>">Back</a>
                </center>
			</form>
			</div>
		</div>
	</div>
</div>

<!--===================================================-->
<!--END CONTENT CONTAINER-->
<script>
    $("#upload_currency_form").submit(function(e){
        e.preventDefault();
        var form = $(this);
        $.ajax({
            url:"<?php echo base_url('auctioneer/auction/update_currency_data/');?>",
            type: 'post',
            data: form.serialize(),
            dataType:'json',
            success: function (response) {
                if (response.status=="success")
                {
                    Display_msg(response.message,response.status);
                    setTimeout(function(){
                        window.location.href="<?php echo base_url('auctioneer/auction/index');?>";
                    }, 3000);
                }
                else
                {
                    Display_msg(response.message,response.status);
                    return false;
                }
            }
        });
    });

</script>