
<!--CONTENT CONTAINER-->
<!--===================================================-->
<div id="content-container">
	<!--Page Title-->
	<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
	<div id="page-head">
	<div id="page-title">
		<h1 class="page-header text-overflow"><?php echo $pagetitle ?></h1>
	</div>
	<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
	<!--End page title-->


	<!--Breadcrumb-->
	<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
	<ol class="breadcrumb">
		<li><a href="<?php echo base_url('auction/index');?>">Home</a></li>
		<li><a href="<?php echo base_url('auctioneer/auction/index');?>">Yankee Auction</a></li>
		<li><a href="<?php echo base_url('auctioneer/auction/view_lots/'.$auctionid);?>">Yankee Auction Lots</a></li>
		<li class="active"><?php echo $pagetitle ?></li>
	</ol>
</div>
	<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
	<!--End breadcrumb-->
	<input type="hidden" id="t11" name="auctionId">
	<div id="page-content">
		<div class="float_btn">
			<a class="btn btn-primary btn-icon btn-circle add-tooltip" data-placement="top" data-toggle="tooltip" data-original-title="Back" href='<?php echo base_url('auctioneer/auction/view_lots/'.$auctionid); ?>' ><i class="fa fa-arrow-left" aria-hidden="true"></i></a>
		</div>
		<div class="panel">
			<div class="panel-heading">
				<h3 class="panel-title"><?php echo $pagetitle ?></h3>
			</div>

			<div class="panel-body">
				<div class="table-responsive">
					<table class="table table-striped table-bordered table-hover no-footer dtr-inline">
						<thead>
							<tr>
								<th>Sale No</th>
								<th>Start Time</th>
								<th>End Time</th>
								<th>Particulars</th>
							</tr>
							<tr>
								<td><?php echo $auction_details['ynk_saleno'] ?></td>
								<td><?php echo date('d-m-Y H:i:s',strtotime($auction_details['start_date']." ".$auction_details['stime'])) ?></td>
								<td><?php echo date('d-m-Y H:i:s',strtotime($auction_details['end_date']." ".$auction_details['etime'])) ?></td>
								<td><?php echo $auction_details['particulars'] ?></td>
							</tr>
						</thead>
					</table>
				</div>

				<div class="table-responsive">
					<form id="form_lot_update_timing" name="form_lot_update_timing" method="post">
						<input type="hidden" id="auctionid" name="auctionid" value="<?php echo $auctionid; ?>" />
						<table class="table table-striped table-bordered table-hover no-footer dtr-inline">
							<thead>
								<tr>
									<th width="20"><input name="checkbox_lot_all" type="checkbox" onchange="check_all_checkbox(this);" ></th>
									<th>Lot No</th>
									<th>Vendor Lot No</th>
									<th>Start Price</th>
									<th>Increment By</th>
									<th>Start Date<br/>(YYYY-MM-DD)</th>
									<th>Start Time<br/>(HH:MM:SS)</th>
									<th>End Date<br/>(YYYY-MM-DD)</th>
									<th>End Time<br/>(HH:MM:SS)</th>
								</tr>
							</thead>
							<tbody>
								<?php
								if( ! empty($lot_details))
								{
									foreach($lot_details as $single_lot)
									{
										?>
										<tr>
											<td><input class="chk_all chkbox" name="checkbox_lot_<?php echo $single_lot['ynk_lotno']?>" type="checkbox" onchange="enable_disable_lots('<?php echo $single_lot['ynk_lotno']?>',this);" ></td>
											<td><?php echo $single_lot['ynk_lotno']?></td>
											<td><?php echo $single_lot['vendorlotno']?></td>
											<td>
												<?php
												$readonly = '';
												$highlightReadOnly = '';
												if($single_lot['bid_count'] > 0){
													$readonly = 'readonly="readonly"';
													$highlightReadOnly = 'color:red;';
												}
												?>
												<input disabled="disabled" class="lot_<?php echo $single_lot['ynk_lotno']?>" name="startbid[<?php echo $single_lot['ynk_lotno']?>]" required style="width:100px;<?php echo $highlightReadOnly?>" placeholder="Start Price" title='Enter Start Amount' value="<?php echo $single_lot['startbid']?>" <?php echo $readonly?> autocomplete="off" />
											</td>
											<td>
												<input disabled="disabled" class="lot_<?php echo $single_lot['ynk_lotno']?>" name="incrby[<?php echo $single_lot['ynk_lotno']?>]" required style="width:100px;" value="<?php echo $single_lot['incrby']?>" placeholder="Increment By" title='Enter Increment Amount'  autocomplete="off"  />
											</td>
											<td>
												<input disabled="disabled" class="lot_<?php echo $single_lot['ynk_lotno']?>" name="startdate[<?php echo $single_lot['ynk_lotno']?>]" required style="width:100px;" placeholder="YYYY-MM-DD" value="<?php echo date("Y-m-d",strtotime($single_lot['lot_open_date']))?>" title='Enter State Date'  autocomplete="off"  />
											</td>
											<td>
												<input disabled="disabled" class="lot_<?php echo $single_lot['ynk_lotno']?>" name="stime[<?php echo $single_lot['ynk_lotno']?>]" maxlength="8" required style="width:100px;" placeholder="HH:MM:SS" value="<?php echo $single_lot['stime']?>" title='Enter State Time' autocomplete="off"  />
											</td>
											<td>
												<input disabled="disabled" class="lot_<?php echo $single_lot['ynk_lotno']?>" name="enddate[<?php echo $single_lot['ynk_lotno']?>]" required style="width:100px;" placeholder="YYYY-MM-DD" value="<?php echo date("Y-m-d",strtotime($single_lot['lot_close_date']))?>" title='Enter End Date' autocomplete="off"  />
											</td>
											<td>
												<input disabled="disabled" class="lot_<?php echo $single_lot['ynk_lotno']?>" name="etime[<?php echo $single_lot['ynk_lotno']?>]" maxlength="8" required style="width:100px;" placeholder="HH:MM:SS" value="<?php echo $single_lot['etime']?>" title='Enter End Time' autocomplete="off"   />
											</td>
										</tr><?php
									}
								}
								?>
							</tbody>
							<tfoot>
								<?php
								if( ! empty($lot_details))
								{ ?>
									<tr>
										<td colspan = '9' class="text-center">
											<input type="submit" class="btn btn-primary" Value="Submit"/>
											<a class="btn btn-warning" href='<?php echo base_url('auctioneer/auction/view_lots/'.$auctionid) ;?>' >Back</a>
										</td>
									</tr>
									<?php
								}
								?>
							</tfoot>
						</table>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
<!--===================================================-->
<!--END CONTENT CONTAINER-->
<script>
/* Check All Lots */
function check_all_checkbox(obj){
	if($(obj).is(":checked"))
	{
		$('.chk_all').each(function()
		{
			$(this).prop('checked',true);
			$(this).change();
		});
	}
	else
	{
		$('.chk_all').each(function()
		{
			$(this).prop('checked',false);
			$(this).change();
		});
	}

}

/* Selected Lots Enable Disable */
function enable_disable_lots(lotno,obj)
{
	if($(obj).is(":checked"))
	{
		$(".lot_"+lotno).each(function()
		{
			$(this).removeAttr("disabled");
		});
	}
	else
	{
		$(".lot_"+lotno).each(function()
		{
			$(this).attr("disabled","disabled");
		});
	}
}

$(function()
{

	var $chkboxes = $('.chkbox');
    var lastChecked = null;

	$chkboxes.click(function(e) {
		console.log('in');
        if (!lastChecked) {
            lastChecked = this;
            return;
        }

        if (e.shiftKey) {
            var start = $chkboxes.index(this);
            var end = $chkboxes.index(lastChecked);

            $chkboxes.slice(Math.min(start,end), Math.max(start,end)+ 1).prop('checked', lastChecked.checked);
			$chkboxes.change();
        }

        lastChecked = this;
    });
	/* Select Multiple Checkbox With Shift Key - End */

	/* Form Submit */
	var vRules = {};
	var vMessages = {};

	$("#form_lot_update_timing").validate({
		rules: vRules,
		messages: vMessages,
		submitHandler: function(form){
			$(form).ajaxSubmit({
				url:"<?php echo base_url('auctioneer/auction/update_lot_timings/'.$auctionid);?>",
				type: 'post',
				dataType:'json',
				cache: false,
				clearForm: false,
				success: function (response) {
					if (response.status=="success"){
						Display_msg(response.message,response.status);
						setTimeout(function(){
							window.location.reload();
						}, 3000);
					} else {
						Display_msg(response.message,response.status);
						return false;
					}
				}
			});
		}
	})
});

</script>
