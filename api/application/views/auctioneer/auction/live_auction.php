<style>
#newsCont {line-height:20px;margin:5px 0px;border:1px dashed #A81213;background:url("images/highlight.png");box-shadow:0 1px 3px rgba(0, 0, 0, 0.1);text-shadow:0 1px 0 rgba(255, 255, 255, 0.55)}
#news {line-height: 20px;}
.tickerHead{background: #A81213;width:80px;line-height: 20px;overflow: hidden;margin: 0;	padding: 3px;font-weight:bold;color:#fff;float:left}
.ticker {width:500px;height: 20px;overflow: hidden;margin: 0;	padding: 0;	list-style: none;float:left}

#buttons li {
  float: left;
  list-style: none;
  text-align: center;
  width:auto;
  margin-right:5px;
  
}
#buttons li a {
  text-decoration: none;
  color: #FFFFFF;
  display: block;
  font-size:14px;
  border-radius:5px;
}
</style>

<link href="<?php echo base_url(); ?>auctioneer_assets/css/basictable.css" rel="stylesheet">
<link href="<?php echo base_url(); ?>auctioneer_assets/css/basictable-style.css" rel="stylesheet">
<link href="<?php echo base_url(); ?>auctioneer_assets/css/jquery.classycountdown.css" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>auctioneer_assets/css/jquery.countdown.css">
<div id="content-container">
	
<div class="page-content" style="padding:10px">
         	<div class="panel">
         	<div class="panel-heading">
					<h3 class="panel-title"><i class="icon-hammer"></i> LIVE AUCTION NO: <?php echo $auction_details['ynk_saleno']; ?> <span class="pull-right samll"><strong>Vendor: </strong><?php echo $auction_details['vendor_name']; ?> | <strong>Auction Type:</strong> <?php echo $auction_details['auctiontype']; ?> </span></h3>
			</div>
         	
         	<div class="panel-body" style="padding-top: 10px">
         	<div class="row">
				<div class="col-lg-12">
					<div class="col-xs-12">
						<div class="wrapper" style="border-top:1px solid #ddd;padding:5px;">
							<ul id="buttons">
								<li><a class="btn btn-primary btn-labeled fa fa-columns" href="<?php echo base_url('auctioneer/dashboard');?>">Dashboard</a></li>
								<li><a class="btn btn-info btn-labeled fa fa-list" href="javascript:void(0);" onclick="online_status();">Bidder Online Status</a></li>
								<li><a class="btn btn-danger btn-labeled fa fa-clock-o" href="javascript:void(0);" onclick="closed_lots();">Closed Lots</a></li>
								<li><a class="btn btn-success btn-labeled fa fa-list" href="javascript:void(0);" onclick="bid_log();">Bid Log</a></li>

							</ul>
						</div>
					</div>
				</div>
			</div>
         	<!-- MESSAGES -->
          
			<?php //print_r($auction_details);?>
					 <div class="row messages">
    					<div class="col-lg-12">
        					<div id="newsCont" class="clearfix">
            				<div class="tickerHead">MESSAGES</div>
                				<div class='col-lg-11'>
                					<marquee scrollamount=3 class='text-info'>
                						<div id="news" class="ticker" style="font-size: 15px"><?php echo $auc_msg; ?></div>
                					</marquee>
                				</div>
            				</div>
    						 
    					</div>
					</div>
					
					<div class="table-responsive">
						<table class="table table-bordered b-t b-light text-sm table-striped" id="lot_table"> 
							<thead>                                                  
								<tr>  
									<th>#</th> 
									<th><?php echo $lot_settings['ltno']==""?'LOT NO':$lot_settings['ltno']; ?> </th> 
									
									<th class="<?= (@$lot_settings['h_product']=="Y" && @$lot_settings['h_product_image']=="Y")?"hide":"show";?>">
										<?php echo @$lot_settings['product']==""?'PRODUCT':$lot_settings['product']; ?>
									</th>
									<th>DETAILS</th>
									<th>BID STATUS</th>
									<?php
									
									if ( $auction_details['allowed_max_extension'] != 0)
									{
										?><th>Extension</th><?php
									}
									?>
									<th style="width:150px">TIME LEFT</th>
									
								</tr>
							</thead>
							<tbody id="lot_list">
							<?php
							for ($i=0;$i<count($live_lot);$i++)
							{
							?>
								<tr id="lot_<?php echo $live_lot[$i]['ynk_lotno'];?>" data_id="<?php echo $live_lot[$i]['ynk_lotno'];?>">
									<td><input type="radio" name="lotno" value="<?php echo $live_lot[$i]['ynk_lotno'];?>"></td>
									
									<td>
										<?php
										if ($live_lot[$i]['ynk_lotno'] != $live_lot[$i]['vendorlotno'] && $live_lot[$i]['vendorlotno'] != ''){
											?><?php echo $live_lot[$i]['ynk_lotno'];?><hr style="margin: 5px 0 5px 0;">[<?php echo $live_lot[$i]['vendorlotno'];?>]<?php
										} else {
											?><?php echo $live_lot[$i]['ynk_lotno'];?><?php
										}
										?>
									</td>
									
									<td class="<?= (@$lot_settings['h_product']=="Y" && @$lot_settings['h_product_image']=="Y")?"hide":"show";?>">
									<?php if(@$lot_settings['h_product_image']!="Y"){?>
										<div id="IMG<?php echo $live_lot[$i]['ynk_lotno'];?>">
										<?php 
										foreach($live_lot[$i]['imgs'] as $imgs) {
										      echo '<a href="'.$imgs['url'].'" target="_new" ><img src="'.$imgs['url'].'" class="img-responsive img-thumb" style="max-width:120px"/></a>';
										    
										}
										}
										?>
										
										</div>
										
										<span class="<?php echo @$lot_settings['h_product']=='Y'?'hide':'show';?>" title="<?php echo $live_lot[$i]['product_details'];?>"><?php echo nl2br($live_lot[$i]['product']);?></span>
										<span class="<?php echo @$lot_settings['h_vltno']=='Y'?'hide':'show';?>" title="<?php echo $live_lot[$i]['vendorlotno'];?>">VENDOR LOT:<?php echo $live_lot[$i]['vendorlotno'];?></span>
										
									</td>
									
									
									<td nowrap>
    									<p class="<?= @$lot_settings['h_plant']=="Y"?"hide":"show";?>">
    										<?php echo $lot_settings['plant']==""?'PLANT':$lot_settings['plant']; ?>: <?php echo $live_lot[$i]['plant'];?>
    									</p>
    									<p class="<?= @$lot_settings['h_tq']=="Y"?"hide":"show";?>" >
    										<?php echo $lot_settings['tq']==""?'QTY':$lot_settings['tq']; ?>: <?php echo (float)$live_lot[$i]['totalqty']." ".$live_lot[$i]['totunit'];?>
    									</p>
    									<p class="<?= @$lot_settings['h_bq']=="Y"?"hide":"show";?>" >
    										Min. Bid Qty: <?php echo (float)$live_lot[$i]['min_part_bid']." ".$live_lot[$i]['bidunit'];?>
    									</p>
    									<p class="<?= @$lot_settings['h_lsd']=="Y"?"hide":"show";?>"> Start: <?php echo $live_lot[$i]['lot_open_date'];?></p>
    									<p class="<?= @$lot_settings['h_led']=="Y"?"hide":"show";?>">End : <?php echo $live_lot[$i]['lot_close_date'];?> </p>
    									
    									<p class="<?= @$lot_settings['h_sb']=="Y"?"hide":"show";?>">
    										Starting Bid :<?php	$start_bid = $live_lot[$i]['startbid']/$currency['conversion_rate'];?>
    										<span id="Sbid<?php echo $live_lot[$i]['ynk_lotno'];?>" > <?php echo number_format($start_bid,$auction_details['decimal_point']).' '.$currency['bidder_prefered_currency_name'];?> </span>
    									</p>
    									<p class="<?= @$lot_settings['h_db']=="Y"?"hide":"show";?>">
    										Min.<?php echo $auction_details['bid_logic']=="F"?'Incr.':'Decr.';?>. <span id="Incr<?php echo $live_lot[$i]['ynk_lotno'];?>" > <?php echo number_format($live_lot[$i]['incrby']/$currency['conversion_rate'],$auction_details['decimal_point'], '.', '').$currency['bidder_prefered_currency_name'];?> </span>
    									</p>
    									
    									<input type="hidden" name="lot_close_date_time<?php echo $live_lot[$i]['ynk_lotno'];?>" id="lot_close_date_time<?php echo $live_lot[$i]['ynk_lotno'];?>" value="<?php echo $live_lot[$i]['lot_close_date_time'];?>"/>
									</td>
									
									<td id='h1_<?php echo $live_lot[$i]['ynk_lotno'];?>'><div id="UpdateBids_<?php echo $live_lot[$i]['ynk_lotno'];?>">Loading..</div></td>
									
									
									<?php
									if ($auction_details['allowed_max_extension'] != 0){
									    if ($auction_details['allowed_max_extension'] == $live_lot[$i]['no_of_extenstion']){
											?><td id='lot_extend_<?php echo $live_lot[$i]['ynk_lotno'];?>'>No Further<br>extension</td><?php
										}else{
										    ?><td id='lot_extend_<?php echo $live_lot[$i]['ynk_lotno'];?>'><span id='lot_extend_count_<?php echo $live_lot[$i]['ynk_lotno'];?>'><?php echo $live_lot[$i]['no_of_extenstion']?></span> / <?php echo $auction_details['allowed_max_extension']?></td><?php
										}
									}
									?>									
									<td id="timeleft_<?php echo $live_lot[$i]['ynk_lotno'];?>"></td>
									<?php 
									
                                echo "</tr>";
											
							
							}
							?>
							</tbody>
						</table>
					</div>
					     
			</div>
			</div>
           <div class="panel">
         	<div class="panel-heading">
					<h3 class="panel-title"><i class="icon-hammer"></i> UPCOMING LOTS: <?php echo $auction_details['ynk_saleno']; ?> <span class="pull-right samll"><strong>Vendor: </strong><?php echo $auction_details['vendor_name']; ?> | <strong>Auction Type:</strong> <?php echo $auction_details['auctiontype']; ?> </span></h3>
			</div>
         	
         	<div class="panel-body" style="padding-top: 10px">
         		<table class="table table-bordered b-t b-light text-sm table-striped" id="lot_upcoming_table"> 
					<thead>                                                  
					  
					   <tr>                                                 
							<th><?php echo $lot_settings['ltno']==""?'LOT NO':$lot_settings['ltno']; ?></th>  
							<th class="<?= (@$lot_settings['h_product']=="Y" && @$lot_settings['h_product_image']=="Y")?"hide":"";?>"><?php echo @$lot_settings['product']==""?'PRODUCT':$lot_settings['product']; ?></th>									
							<th class="<?= @$lot_settings['h_plant']=="Y"?"hide":"";?>"><?php echo $lot_settings['plant']==""?'PLANT':$lot_settings['plant']; ?></th>
							<th class="<?= @$lot_settings['h_tq']=="Y"?"hide":"";?>"><?php echo $lot_settings['tq']==""?'QTY':$lot_settings['tq']; ?></th>
							<th class="<?= @$lot_settings['h_bq']=="Y"?"hide":"";?>"><?php echo $lot_settings['bq']==""?'BID QTY':$lot_settings['bq']; ?></th>
							<th class="<?= @$lot_settings['h_lsd']=="Y"?"hide":"";?>"><?php echo $lot_settings['lsd']==""?'START':$lot_settings['lsd']; ?></th>
							<th class="<?= @$lot_settings['h_sb']=="Y"?"hide":"";?>"><?php echo $lot_settings['sb']==""?'START':$lot_settings['sb']; ?></th>
							<th class="<?= @$lot_settings['h_dn']=="Y"?"hide":"";?>"><?php echo $lot_settings['db']==""?'INCR/DECR':$lot_settings['db']; ?></th>
						    
							<th style="width:150px">TIME LEFT</th>	
						</tr>
					</thead>
					<tbody id="Lot_Upcoming"></tbody>
				</table>
         	</div>
         </div>
       
        <!--/.page-content--> 
      </div>
      </div>
      <input type="hidden" name="sLotID" id="sLotID" value=""/>
     
      <!--/.row--> 
  
	  <div id="Online" class="modal fade" aria-hidden="true" class="col-md-12" >
			<div class="modal-dialog"  >
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
						<h3 class="m-t-none m-b text-mute">Bidder Online Status <span id="online_lotno"></span> </h3>
					</div>
					<form role="form" class="form-horizontal" id="form-validate1">
						<div class="modal-body">
							<div class="col-sm-12">
								<section class="panel panel-default">
									<header class="">
										<span id="bidder_count" class="pull-right"></span>
									</header> 
									<table class="table table-striped table-bordered "> 
										<thead> 
											<tr> 
												<th>Bidder Name</th> 
												<th>Status</th> 
											</tr> 
										</thead> 
										<tbody id="online_status"></tbody>
									</table> 
								</section> 
							</div>
						</div>
						<div class="modal-footer">
							<button type="button" data-dismiss="modal" class="btn btn-danger">Close</button>
						</div>
					</form>
				</div>
			</div>
		</div>
		<div class="modal fade" id="ClosedLot" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true">
			<div class="modal-dialog" style="width: 80%;">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
						<h4 class="modal-title text-info">Closed Lots</h4>
					</div>
					<div class="modal-body">
						<table class='table'>
							<tr>
								<th>Lot No.</th>
								<th>Product</th>
								<th>Plant</th>
								<th>Tot Qty.</th>
								<th>Time</th>
								<th>Starting Bid</th>
								<th>H1 value</th>
							</tr>
							<tbody id="closed_lot_details">
							</tbody>
						</table>
					</div>
					<div class="modal-footer">
						<button type="button"  class="btn btn-danger" data-dismiss="modal">Close</button>
					</div>
				</div>
			</div>
		</div>
<script cache="false" src="<?php echo base_url();?>auctioneer_assets/js/charts/flot/jquery.flot.min.js"></script>
<script cache="false" src="<?php echo base_url();?>auctioneer_assets/js/charts/flot/jquery.flot.tooltip.min.js"></script>
<script cache="false" src="<?php echo base_url();?>auctioneer_assets/js/charts/flot/jquery.flot.resize.js"></script>
<script cache="false" src="<?php echo base_url();?>auctioneer_assets/js/charts/flot/jquery.flot.orderBars.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>auctioneer_assets/js/jquery.plugin.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>auctioneer_assets/js/jquery.countdown.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>auctioneer_assets/js/jquery.basictable.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>auctioneer_assets/js/jquery.classycountdown.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>auctioneer_assets/js/jquery.knob.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>auctioneer_assets/js/jquery.throttle.js"></script>
<script src="<?php echo base_url(); ?>auctioneer_assets/js/common.js"></script>
<script src="<?php echo base_url(); ?>auctioneer_assets/js/ynk_default.js?v=<?php echo rand(1,10000000)?>"></script>
<script>

/**
 * Highlight the lot on closing 5 Minute alert
 * @param obj
 */
 var increase_lot_time_in=180;
 var OpenLots='';
 var conversion_rate=1;

 var bidder_currency_code='<?php echo $auction_details['auction_currency'];?>';
function highlightLot(obj) {

	var lotno = obj.id;
	var parts = lotno.split('_');
	var periods = $('#timeleft_' + parts[1]).countdown('getTimes');
	
	if ($.countdown.periodsToSeconds(periods) < increase_lot_time_in) {
		$('#timeleft_' + parts[1]).toggleClass('highlight');
	}else{
		$('#timeleft_' + parts[1]).removeClass('highlight'); 
	}
 	
}
function online_status()
{
	$.ajax({
		url: '<?php echo base_url();?>auctioneer/auction/bidder_online_status',
		data: {"auctionid":'<?php echo $auctionid;?>'},
		type:"post",
		dataType: 'json',
		success: function (resObj) {
			var online="";
			var online_cnt=0;
			var offline="";
			var offline_cnt=0;
			var last_seen="";
			var last_seen_cnt=0;
			var j=0;
			
			for(var i=0; i<resObj.bidder_count.no_of_bidder;i++)
			{
				if(inArray(resObj.bidder[i].conperson, resObj.online_bidder)){
					online+="<tr><td>"+resObj.bidder[i].conperson+"</td>";
					online+="<td class='text-primary'><i class='fa fa-circle'></i>  Online </td>";
					online_cnt++;
				}else if(inArray(resObj.bidder[i].conperson, resObj.last_seen_bidder)){	
					last_seen+="<tr><td>"+resObj.bidder[i].conperson+"</td>";
					var lastseen =resObj.last_seen_bidder[j].lastseen ;
					var time_array=lastseen.split(":");
					last_seen+="<td class='text-info'><i class='fa fa-circle'></i> Last Seen on "+resObj.last_seen_bidder[j].login;
					if(time_array[0]!=00){
						last_seen+=" <br/>"+time_array[0]+" Hours "+time_array[1]+" Minutes Ago </td> ";
					}else{
						last_seen+=" ["+time_array[1]+" Minutes Ago]</td>";
					}
					j++;
					last_seen_cnt++;
				}else if(inArray(resObj.bidder[i].conperson, resObj.offline_bidder)){
					offline+="<tr><td>"+resObj.bidder[i].conperson+"</td>";
					offline+="<td class='text-danger'><i class='fa fa-circle'></i> Offline </td>";
					offline_cnt++;
				}
			}
			$("#bidder_count").html("<span class='label bg-primary'>Online - "+online_cnt+"</span> <span class='label bg-info'>Last Seen - "+last_seen_cnt+"</span> <span class='label bg-danger'>Offline - "+offline_cnt+"</span>");
			$("#online_status").html(online+last_seen+offline);
			$("#Online").modal('show');
		},
		error:function(e){
			alert('Request Failed');
		}
	});
}
function inArray(bidder_name, bidder_status_array) {
    var length = bidder_status_array.length;
    for(var i = 0; i < length; i++) {
        if(bidder_status_array[i].conperson == bidder_name) return true;
    }
    return false;
}
function closed_lots(){
	$.ajax({
		type: "POST",
		url: "<?php echo base_url();?>auctioneer/auction/closed_lots",
		data: {"auctionid":<?php echo $auctionid?>},
		dataType: "json",
		success: function(data){
			var html="";
			if(data.closed_data.length==0){
				html+="<tr><td colspan='7' class='text-center text-danger'>No Lots Closed</td></tr>";
			}else{
				for(var i=0; i<data.closed_data.length;i++)
				{
					html+="<tr><td>"+data.closed_data[i].ynk_lotno+"</td><td>"+data.closed_data[i].product+"</td><td>"+data.closed_data[i].plant+"</td><td>"+data.closed_data[i].totalqty+"</td><td>S:"+data.closed_data[i].stime+"<br>E:"+data.closed_data[i].etime+"</td><td>"+data.closed_data[i].startbid+"</td>";
					if (data.h1_amount[i])
					{
						if(data.h1_amount[i].lotid==data.closed_data[i].lotno)
						{
							html+="<td>"+data.h1_amount[i].amount+"</td>";
						}
					}else{
							html+="<td class='text-danger'>No Bid</td>";
							
					}
					html+="</tr>";
				}
			}	
			$("#closed_lot_details").html(html);
			$('#ClosedLot').modal('show');
		},
		error:function(){
			alert('Request Failed');
		}
	});
}
$(document).ready(function(){
	
	<?php
	for($i=0;$i<count($live_lot);$i++)
	{
	  
	?>
	
		$("#timeleft_<?php echo $live_lot[$i]['ynk_lotno'];?>").countdown({ padZeroes: true,until: +secondConvert("<?php echo $live_lot[$i]['TOTTIME'];?>"),onTick: function(){highlightLot(this);}, onExpiry: function() { CloseLot(this);}});
	
	<?php
	}
	?>
});
/**
 * Add lot dynamically using object id
 * @param $obj
 * @returns
 */
function AddLot(obj)
{
	
location.reload();

}

/**
 * upcoming lots
 * @param UpcomingLotsUrl
 * @returns
 */
function upcoming_lot(UpcomingLotsUrl){
	$.ajax({
		url:"<?=base_url()?>auctioneer/auction/upcoming_lots/<?php echo $auctionid;?>",
		dataType:'json',
		success:function(data)
		{
			$("#Lot_Upcoming").empty();
			for (i=0;i<data.UpcomingLot.length;i++)
			{
				var LotNO=data.UpcomingLot[i].ynk_lotno;
				var currency=(data.UpcomingLot[i].currency==null)?"-":data.UpcomingLot[i].currency;
				var html="";
				html += 	"<tr id=Ulot_"+data.UpcomingLot[i].ynk_lotno+" data_id="+data.UpcomingLot[i].ynk_lotno+">"+
								"<td>" + data.UpcomingLot[i].ynk_lotno + "<hr style='margin: 5px 0 5px 0;'>" +data.UpcomingLot[i].vendorlotno+ "</td>"+
								"<td width='350px'><span title='"+data.UpcomingLot[i].product_details+"'>"+data.UpcomingLot[i].product+"</span></td>"+
								"<td>"+data.UpcomingLot[i].plant+"</td>"+
								"<td>"+parseFloat(data.UpcomingLot[i].totalqty)+" "+data.UpcomingLot[i].totunit+"</td>"+
								"<td>"+parseFloat(data.UpcomingLot[i].bidqty)+" "+data.UpcomingLot[i].bidunit+"</td>"+
								"<td>S :"+data.UpcomingLot[i].stime+"<br>E :"+data.UpcomingLot[i].etime+"</td>";
								
								html+="<td><span id='Sbid"+data.UpcomingLot[i].ynk_lotno+"'>"+parseFloat(data.UpcomingLot[i].startbid).toFixed(2)+"</span></td>";
								html+="<td><span id='Incr"+data.UpcomingLot[i].ynk_lotno+"'>"+parseFloat(data.UpcomingLot[i].incrby).toFixed(2)+currency+"</span></td>";
								
								
								html+="<td id=timeleft_"+data.UpcomingLot[i].ynk_lotno+"></td>"+
							"</tr>";
				$("#Lot_Upcoming").append(html);
				$('#timeleft_' + data.UpcomingLot[i].ynk_lotno).countdown({padZeroes: true, until: +secondConvert(data.UpcomingLot[i].TOTTIME), onExpiry: function() { AddLot(this);},onTick: function(){highlightLot(this);}});
				OpenLots = OpenLots+data.UpcomingLot[i].ynk_lotno+",";
			}
			
			/* Only for Vendor To Show Report */
			if(data.show_closed_popup){
				if(data.show_closed_popup != 'no'){
					window.location.href=data.show_closed_popup;
				}
			}
		},
    });//AJAX
}

/**
 * Getting updates
 * @param LiveUpdateUrl
 */

 var user_status = "";
function waitForMsg(){
	
	$.ajax({
        type: "POST",
        url:"<?=base_url()?>auctioneer/auction/lot_update_bid_data/<?php echo $auctionid;?>" ,
		data:{"user_status":user_status},
		dataType:'json',
        cache: false,
        async: true, /* If set to non-async, browser shows page as "Loading.."*/
        cache: false,
        timeout:50000, /* Timeout in ms */
        success: function(data){ /* called when request to  completes */
		if(data){
			for (var key in data){
				
				rHtml="<table class='table table-bordered'><tr><th>Bidder</th><th>Qty. Alloted</th><th>Per / "+data[key]['bidqty']+" "+data[key]['total_unit']+"</th><th>Status</th><tr>";
				for (i=0;i<data[key]['bid_details'].length;i++)
					{
					rHtml = rHtml +"<tr class=''><td>"+data[key]['bid_details'][i]['compname']+"</td><td align='right'>"+number_formatter(data[key]['bid_details'][i]['sub_alloted_qty'])+" "+data[key]['total_unit']+"</td>";
					
						rHtml = rHtml +"<td align='right'><span id='H1Amt"+data[key]['lotno']+"'>"+parseFloat(parseFloat(data[key]['bid_details'][i]['max_bid_amt']/parseFloat(conversion_rate)).toFixed(2))+ " "+ bidder_currency_code +"</span></td>";
					
					
					rHtml = rHtml +"<td> "+data[key]['bid_details'][i]['status_in_lot']+" <input type='hidden' id='my_own_quantity_"+data[key]['lotno']+"_"+data[key]['bid_details'][i]['bidder_id']+"' value='"+data[key]['bid_details'][i]['buyer_own_quantity']+"' /></td></tr>";
					$('#txtQty_'+key).val(data[key]['bid_details'][i]['buyer_own_quantity']);
					
				}
				rHtml= rHtml+"<tr><td>FREE QTY:</td><td class='text-right'>"+data[key]['available_qty']+" "+data[key]['total_unit']+"</td><td colspan='2'></td>";
				rHtml= rHtml+"<table>";
				$('#UpdateBids_'+key).html(rHtml);
				$('#lot_extend_count_'+key).html(data[key]['no_of_extenstion']);
				
				if(data[key]['lot_close_date_time'] != $("#lot_close_date_time"+data[key]['lotno']).val() )
				{
				
				$("#timeleft_"+data[key]['lotno']+"").countdown('destroy');
				$("#timeleft_"+data[key]['lotno']+"").countdown({ padZeroes: true,until: +secondConvert(""+data[key]['TOTTIME']+""),onTick: function(){highlightLot(this);}, onExpiry: function() { CloseLot(this);}});
				$("#lot_close_date_time"+data[key]['lotno']).val(data[key]['lot_close_date_time']);
				
				}
				
			}
			
			
			user_status = data;
			OpenLots="";
			}
			$("#open_lots").val(OpenLots);
			
			setTimeout(function() {
				waitForMsg(); // Do something after 5 seconds
			}, 5000);

        },
        error: function(XMLHttpRequest, textStatus, errorThrown){
            
		setTimeout(
			waitForMsg(), /* Try again after.. */
		10000); /* milliseconds (1.5seconds) */
        }
    });
}

waitForMsg();
upcoming_lot();
function bid_log()
{
	var lot_no = $('input[name="lotno"]:checked').val();
	if (lot_no ===undefined)
	{
		lot_no = 0;
	}
	var url ="<?php echo base_url('auctioneer/auction_reports/lotwise_bid_lot_html/'.$auctionid.'/')?>"+lot_no+"/no";
	var myWindow = window.open(url, "left", "width=800,height=800");
	

}
</script>

