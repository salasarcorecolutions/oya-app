<style>
tr {
	page-break-inside: avoid;
}
thead {
	display: table-header-group;
}
tfoot {
	display: table-row-group;
}
tr {
	page-break-inside: avoid;
}
td.wrappable,table.data_table td.wrappable {
    white-space: normal;	
}            
table td,table th {
	font-size:10px;
	
	
}
</style>


<page backbottom='110px' backtop="0px" backleft="0px" backright="0px" style="font-size:10px;">
   <page_footer>
        <table style='width: 100%;'>
			<tr>
				<td align='left' style='float:left;width:200px;'>
				Page :  [[page_cu]]/[[page_nb]] <br/>
				<?php echo (new DateTime(null, new DateTimeZone($_SESSION['user_tz'])))->format('D dS \of M Y h:i:s A'); ?>
				
				</td>
				<td align='right' style='float:right;width:545px;'>
					
				</td>
			</tr>
		</table>
    </page_footer>
	<br>
	<table border='0.5' cellspacing='0' cellpadding='0' width='100%'>
		<tbody>
			<tr>
				<td style="width:554px;" >
								
				</td>
				<td align='center' style="width:180px;">
					<span style='font-size:20px;color:#2a5395;'><strong><?php echo PRODUCT ?></strong></span>
				</td>
			</tr>
			<tr>
				<td colspan='2' align='center' >
					<strong>H1 AND TOTAL BIDS FOR AUCTION :  <?php echo $common_auction_details['ynk_saleno']; ?> </strong>
				</td>
			</tr>
			<tr>
				<td colspan='2' align='center' >
					<strong><?php echo strtoupper($common_auction_details['vendor_name'])?></strong>
				</td>
			</tr>
			<tr>
				<td colspan='2' align='center' >
					<strong>AUCTION DETAILS</strong>
				</td>
			</tr>
			<tr>
				<td colspan='2' align='left'>
					<strong>E-Auction Sale No :</strong><?php echo $common_auction_details['ynk_saleno']?>
				</td>
			</tr>
			<tr>
				<td colspan='2' align='left'>
					<strong>Auction Type : </strong><?php echo $common_auction_details['auctiontype']?>
				</td>
			</tr>
			<tr>
				<td colspan='2' align='left'>
					<strong>Message : </strong><?php echo $common_auction_details['auction_msg']?>
				</td>
			</tr>
			<tr>
				<td colspan='2' align='left'>
					<strong>Auction Start Date : </strong><?php echo $common_auction_details['sdt']?>
				</td>
			</tr>
			<tr>
				<td colspan='2' align='left'>
					<strong>Particulars : </strong><?php echo $common_auction_details['particulars']?>
				</td>
			</tr>
			<tr>
				<td colspan='2' align='left'>
					<strong>No. of Lots : </strong><?php echo $common_auction_details['no_of_lots']?>
				</td>
			</tr>
		</tbody>		
	</table>
	<br>
	<table  border='0.5' cellspacing='0' cellpadding='0' width='100%'>
		<thead>
			<tr>
				<th align='center' bgcolor="#e0e0e0">LOT<br/>NO</th>
				<th align='center' bgcolor="#e0e0e0">DESCRIPTION</th>
				<th align='center' bgcolor="#e0e0e0">PLANT</th>
				<th align='center' bgcolor="#e0e0e0">TOTAL<br/>QTY.</th>
				<th align='center' bgcolor="#e0e0e0">START<br/>RATE</th>
				<th align='center' bgcolor="#e0e0e0">BID <br/> QTY</th>
				<th align='center' bgcolor="#e0e0e0">ALLOTED<br/>QTY</th>
				<th align='center' bgcolor="#e0e0e0">H1 RATE</th>
				<th align='center' bgcolor="#e0e0e0">H1 BIDDER</th>
				<th align='center' bgcolor="#e0e0e0">NO OF<br/>BIDS</th>
				<th align='center' bgcolor="#e0e0e0">NO OF<br/>BIDDER</th>
				<th align='center' bgcolor="#e0e0e0">NO OF<br/>PARTICIPANTS</th>
				<th align='center' bgcolor="#e0e0e0">TAX</th> 
			</tr>
		</thead>
		<tbody>		
			<?php 
				if ( ! empty($get_h1_n_total_bids))
				{
					foreach ($get_h1_n_total_bids as $key=>$value)
					{
						
			?>
					<tr>
						<td align='center' class='wrappable' style="width:25px;" >
							<?php echo $value['lotno'] ; ?>
						</td>
						<td align='center' class='wrappable' style="width:76px;" >
							<?php echo $value['prod']; ?>
						</td>
						<td align='center' class='wrappable' style="width:40px;" >
							<?php echo $value['plant']; ?>
						</td>
						<td  align='center' class='wrappable' style="width:50px;" >
							<?php echo $value['total_quat_unit']; ?>
						</td>
						<td  align='center' class='wrappable' style="width:40px;" >
							<?php echo $value['start_bid']; ?>
						</td>
						<td  align='center' class='wrappable' style="width:40px;" >
							<?php echo $value['bid_quat_unit']; ?>
						</td>
						<td  align='center' class='wrappable' style="width:40px;" >
							<?php echo ! empty($value['bid_qty']) ? $value['bid_qty'] : 0; ?>
						</td>
						<td  align='center' class='wrappable' style="width:60px;"  >
							<?php echo $value['amt']; ?>
						</td>
						<td  align='center' class='wrappable' style="width:70px;" >
							<?php echo $value['cname']; ?>
						</td>
						<td align='center' class='wrappable' style="width:40px;" >
							<?php echo $value['nob']; ?>
						</td>
						<td align='center' class='wrappable' style="width:40px;" >
							<?php echo $value['cnt_bidders']; ?>
						</td>
						<td align='center' class='wrappable' style="width:40px;" >
							<?php echo $value['nop']; ?>
						</td>
						<td align='center' class='wrappable' style="width:80px;" >
							<?php
								if ( ! empty($value['tax']))
								{
									foreach ($value['tax'] as $k=>$v)
									{
										echo $k.' : '.$v.'<br/>';
									}
								}
							?>
						</td>
					</tr>
						
			<?php
			
					}
				} 
				else
				{
			?>
					<tr>
						<td align='center' class='wrappable' style="width:740px;" colspan="12">No Records Found</td>
					</tr>
			<?php
				}
			
			
			?>
		</tbody>
	</table>
</page>