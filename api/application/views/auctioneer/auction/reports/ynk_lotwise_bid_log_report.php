<style>
tr {
	page-break-inside: avoid;
}
thead {
	display: table-header-group;
}
tfoot {
	display: table-row-group;
}
.other-pages{
	page-break-before: always;
}
td.wrappable,table.data_table td.wrappable {
    white-space: normal;
	page-break-before: always;
}
table td,table th {
	font-size:10px;
	page-break-after: inherit;
	
}
table{
	margin-bottom:'100px';
}
</style>


<page backbottom='110px' backtop="0px" backleft="0px" backright="0px" style="font-size:10px;" class="other-pages">
   <page_footer>
        <table style='width: 100%;'>
			<tr>
				<td align='left' style='float:left;width:200px;'>
				Page :  [[page_cu]]/[[page_nb]] <br/>
				<?php echo (new DateTime(null, new DateTimeZone($_SESSION['user_tz'])))->format('D dS \of M Y h:i:s A'); ?>

				</td>
				<td align='right' style='float:right;width:545px;'>

				</td>
			</tr>
		</table>
    </page_footer>
	<br>
	<table border='0.5' cellspacing='0' cellpadding='0' width='100%'>
		<tbody>
			<tr>
				<td style="width:550px;" >

				</td>
				<td align='center' style="width:180px;">
					<span style='font-size:20px;color:#2a5395;'><strong><?php echo PRODUCT ?></strong></span>
				</td>
			</tr>
			<tr>
				<td colspan='2' align='center' >
					<strong>LOTWISE BID LOG FOR AUCTION :  <?php echo $common_auction_details['ynk_saleno']; ?> </strong>
				</td>
			</tr>
			<tr>
				<td colspan='2' align='center' >
					<strong><?php echo strtoupper($common_auction_details['vendor_name'])?></strong>
				</td>
			</tr>
			<tr>
				<td colspan='2' align='center' >
					<strong>AUCTION DETAILS</strong>
				</td>
			</tr>
			<tr>
				<td colspan='2' align='left'>
					<strong>E-Auction Sale No :</strong><?php echo $common_auction_details['ynk_saleno']?>
				</td>
			</tr>
			<tr>
				<td colspan='2' align='left'>
					<strong>Auction Type : </strong><?php echo $common_auction_details['auctiontype']?>
				</td>
			</tr>
			<tr>
				<td colspan='2' align='left'>
					<strong>Message : </strong><?php echo $common_auction_details['auction_msg']?>
				</td>
			</tr>
			<tr>
				<td colspan='2' align='left'>
					<strong>Auction Start Date : </strong><?php echo $common_auction_details['sdt']?>
				</td>
			</tr>
			<tr>
				<td colspan='2' align='left'>
					<strong>Particulars : </strong><?php echo $common_auction_details['particulars']?>
				</td>
			</tr>
			<tr>
				<td colspan='2' align='left'>
					<strong>No. of Lots : </strong><?php echo $common_auction_details['no_of_lots']?>
				</td>
			</tr>
		</tbody>
	</table>
	<br>
	<?php
	
		if ( ! empty($get_lotwise_bid_log_report))
		{
			foreach ($get_lotwise_bid_log_report as $key=>$value)
			{
			?>
				<table border='0.5' cellspacing='0' cellpadding='0' width='100%' >
				<thead>
					<tr>
						<th align='center' class='wrappable' colspan="7"  >
							<b>Lot No:</b>  <?php echo $value['lotno'] ;?>   <br> <b>Product:</b><?php echo $value['prod'];?>
						</th>
					</tr>
				</thead>
				<tbody>
				<tr>
					<td align='center' class='wrappable' ><b>Start Time : </b> <br> <?php echo $value['st'];?> </td>
					<td align='center' class='wrappable'  ><b>Total Qty : </b> <br> <?php echo $value['tq'];?></td>
					<td align='center' class='wrappable' ><b>Star Bid : </b>  <?php echo $value['sb'];?></td>
					<td align='center' class='wrappable'> <b>Proxy Bid : </b> <?php echo $value['proxy'] ;?> </td>
					<td align='center' colspan="3" class='wrappable'><b>Plant : </b> <?php echo $value['plant'] ;?> </td>
				</tr>
				<tr>
					<td align='center' class='wrappable' ><b>End Time : </b> <br> <?php echo $value['et'];?></td>
					<td align='center' class='wrappable' ><b>Bid Qty : </b> <br> <?php echo $value['bq'] ;?></td>
					<td align='center' class='wrappable' ><b>Min Decr : </b><?php echo $value['iv'];?></td>
					<td align='center' class='wrappable'><b>Proxy  Date : </b> <br> <?php echo ($value['pd']!='' && $value['pd']!='1970-01-01')? date('d-m-Y',strtotime($value['pd'])):''?></td>
					<td align='center' colspan="3" class='wrappable'><b>Material:  </b> <?php echo $value['vendorlotno'];?></td>
				</tr>
				<tr>
					<td  align='center' class='wrappable' style="font-weight:bold;width:200px;" bgcolor="#e0e0e0"  > Bidder Name</td>
					<td align='center' class='wrappable' style="font-weight:bold;width:120px;" bgcolor="#e0e0e0" > Date</td>
					<td align='center' class='wrappable' style="font-weight:bold;width:116px;" bgcolor="#e0e0e0" > Time</td>
					<td align='center' class='wrappable' style="font-weight:bold;width:50px;" bgcolor="#e0e0e0" > Quantity</td>
					<td align='center' class='wrappable' style="font-weight:bold;width:50px;" bgcolor="#e0e0e0" > Amount</td>
					<td align='center' class='wrappable' style="font-weight:bold;width:50px;" bgcolor="#e0e0e0" > Bid Amount</td>
					<td align='center' class='wrappable' style="font-weight:bold;width:50px;" bgcolor="#e0e0e0" > Type</td>
				</tr>

	<?php
				if (isset($value['lot_details']))
				{
					foreach ($value['lot_details']  as $lot_details)
					{

	?>
					<tr>
						<td  align='center' class='wrappable' style="width:200px;"><?php echo $lot_details['cname']; ?></td>
						<td align='center' class='wrappable ' style="width:120px;"><?php echo $lot_details['dt']; ?></td>
						<td align='center' class='wrappable' style="width:90px;" ><?php echo $lot_details['tm']; ?> Hrs.</td>
						<td align='center' class='wrappable' style="width:50px;" ><?php echo $lot_details['bid_qty']; ?> </td>
						<td align='center' class='wrappable' style="width:50px;" > <?php echo $lot_details['bid_amt']; ?></td>
						<td align='center' class='wrappable' style="width:50px;" ><?php echo $lot_details['bid_amount']; ?></td>
						<td align='center' class='wrappable' style="width:50px;" ><?php echo $lot_details['typ']; ?></td>
					</tr>

	<?php
					}
					
				}
				else
				{
	?>
					<tr><td colspan='7' align='center' class='wrappable'>No Bid</td></tr>
	<?php
				}
	?>
				</tbody>
				</table>
				<br/>
				
	<?php
	
			}
		}

	?>
	
</page>
