<style>
tr {
	page-break-inside: avoid;
}
thead {
	display: table-header-group;
}
tfoot {
	display: table-row-group;
}
tr {
	page-break-inside: avoid;
}
td.wrappable,table.data_table td.wrappable {
    white-space: normal;	
}            
table td,table th {
	font-size:10px;
	
	
}
</style>


<page backbottom='110px' backtop="0px" backleft="0px" backright="0px" style="font-size:10px;">
   <page_footer>
        <table style='width: 100%;'>
			<tr>
				<td align='left' style='float:left;width:200px;'>
				Page :  [[page_cu]]/[[page_nb]] <br/>
				<?php echo (new DateTime(null, new DateTimeZone($_SESSION['user_tz'])))->format('D dS \of M Y h:i:s A'); ?>
				
				</td>
				<td align='right' style='float:right;width:545px;'>
					
				</td>
			</tr>
		</table>
    </page_footer>
	<br>
	<table border='0.5' cellspacing='0' cellpadding='0' width='100%'>
		<tbody>
			<tr>
				<td style="width:554px;" >
								
				</td>
				<td align='center' style="width:180px;">
					<span style='font-size:20px;color:#2a5395;'><strong><?php echo PRODUCT ?></strong></span>
				</td>
			</tr>
			<tr>
				<td colspan='2' align='center'>
					<strong>CLIENT ALLOWED IN AUCTION STATUS FOR AUCTION :  <?php echo $common_auction_details['ynk_saleno']; ?> </strong>
				</td>
			</tr>
			<tr>
				<td colspan='2' align='center' >
					<strong><?php echo strtoupper($common_auction_details['vendor_name'])?></strong>
				</td>
			</tr>
			<tr>
				<td colspan='2' align='center' >
					<strong>AUCTION DETAILS</strong>
				</td>
			</tr>
			<tr>
				<td colspan='2' align='left'>
					<strong>E-Auction Sale No :</strong><?php echo $common_auction_details['ynk_saleno']?>
				</td>
			</tr>
			<tr>
				<td colspan='2' align='left'>
					<strong>Auction Type : </strong><?php echo $common_auction_details['auctiontype']?>
				</td>
			</tr>
			<tr>
				<td colspan='2' align='left'>
					<strong>Message : </strong><?php echo $common_auction_details['auction_msg']?>
				</td>
			</tr>
			<tr>
				<td colspan='2' align='left'>
					<strong>Auction Start Date : </strong><?php echo $common_auction_details['sdt']?>
				</td>
			</tr>
			<tr>
				<td colspan='2' align='left'>
					<strong>Particulars : </strong><?php echo $common_auction_details['particulars']?>
				</td>
			</tr>
			<tr>
				<td colspan='2' align='left'>
					<strong>No. of Lots : </strong><?php echo $common_auction_details['no_of_lots']?>
				</td>
			</tr>
		</tbody>		
	</table>
	<br>
	<table  border='0.5' cellspacing='0' cellpadding='0' width='100%'>
		<thead>
            <tr>
                <th align='center' bgcolor="#E4E4E4"><font size="1"><b>CLIENT ID</b></font></th>
                <th align='center' bgcolor="#E4E4E4"><font size="1"><b>NAME</b></font></th>
                <th align='center' bgcolor="#E4E4E4"><font size="1"><b>ADDRESS</b></font></th>
                <th align='center' bgcolor="#E4E4E4"><font size="1"><b>EMAIL</b></font></th>
                <th align='center' bgcolor="#E4E4E4"><b>LOT(s) PERMITTED</b></th>
            </tr>
		</thead>
		<tbody>
			<?php
				if ( ! empty($client_allowed))
				{
					$i=0;
					foreach ($client_allowed as $row)
					{
            ?>
                <tr>
					<td align='center' style="width:70px;"><?php echo str_replace('-',$row['bid'],$common_auction_details['ynk_saleno']); ?></td>
					<td align='center' style="width:70px;"><?php echo $row['cname']; ?></td>
					<td align='center' style="width:75px;"><?php echo $row['addr']; ?></td>
					<td align='center' style="width:135px;"><?php echo $row['em']; ?></td>
					<td align='center' style="width:264px;" valign=top>
                        <table border='0.5' cellspacing='0' cellpadding='0' width='100%'>
							<thead>
								<tr>
									<th align='center'>Lot No</th>
									<th align='center'>Description</th>
									<th align='center'>Qty</th>
									<th align='center'>Strating Bid</th>
									<th align='center'>Amount</th>
									<th align='center'>Remarks</th>
								</tr>
							</thead>
							<tbody>
                            <?php
                            if ( ! empty($client_lot_details[$row['bid']]))
                            {
                                foreach ($client_lot_details[$row['bid']] as $value){ ?>
                                <tr>
                                    <td align='center'><?php echo $value['ynk_lotno']; ?></td>
                                    <td align='center'><?php echo $value['product']; ?>&nbsp;</td>
                                    <td align='center'><?php echo $value['totalqty'] ." ".$value['totunit'];?></td>
                                    <td align='center'><?php echo $value['startbid']; ?></td>
                                    <td align='center'><?php echo number_format($value['totalqty']*$value['startbid'],2); ?></td>
                                    <td align='center'><?php echo $value['remarks']; ?>&nbsp;</td>
                                </tr>
							<?php }
							} else { ?>
							<tr>
								<td align='center' style="width: 262px;" colspan="6">No data is available</td>
							</tr>
							<?php } ?>
							</tbody>
                        </table>
                    </td>
                </tr>
				<?php

                    }
                }
				else
				{
			?>
                <tr>
                    <td align='center' class='wrappable' style="width:739px;" colspan="5">No Records Found</td>
                </tr>
			<?php
				}
			
			
			?>
		</tbody>
	</table>
</page>