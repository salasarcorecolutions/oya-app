
<style>
tr {
	page-break-inside: avoid;
}
thead {
	display: table-header-group;
}
tfoot {
	display: table-row-group;
}
tr {
	page-break-inside: avoid;
}
td.wrappable,table.data_table td.wrappable {
    white-space: normal;	
}            
table td,table th {
	font-size:10px;
	
	
}
</style>


<page backbottom='110px' backtop="0px" backleft="0px" backright="0px" style="font-size:10px;">
   <page_footer>
        <table style='width: 100%;'>
			<tr>
				<td align='left' style='float:left;width:200px;'>
				Page :  [[page_cu]]/[[page_nb]] <br/>
				<?php echo (new DateTime(null, new DateTimeZone($_SESSION['user_tz'])))->format('D dS \of M Y h:i:s A'); ?>
				
				</td>
				<td align='right' style='float:right;width:545px;'>
					
				</td>
			</tr>
		</table>
    </page_footer>
	<br>
	
	<table border='0.5' cellspacing='0' cellpadding='0' width='100%'>
		<tbody>
			<tr>
				<td style="width:560px;" >
								
				</td>
				<td align='center' style="width:180px;">
					<span style='font-size:20px;color:#2a5395;'><strong><?php echo PRODUCT ?></strong></span>
				</td>
			</tr>
			<tr>
				<td colspan='2' align='center' >
					<strong>BID SUMMARY REPORT FOR AUCTION :  <?php echo $common_auction_details['ynk_saleno']; ?> </strong>
				</td>
			</tr>
			<tr>
				<td colspan='2' align='center' >
					<strong><?php echo strtoupper($common_auction_details['vendor_name'])?></strong>
				</td>
			</tr>
			<tr>
				<td colspan='2' align='center' >
					<strong>AUCTION DETAILS</strong>
				</td>
			</tr>
			<tr>
				<td colspan='2' align='left'>
					<strong>E-Auction Sale No :</strong><?php echo $common_auction_details['ynk_saleno']?>
				</td>
			</tr>
			<tr>
				<td colspan='2' align='left'>
					<strong>Auction Type : </strong><?php echo $common_auction_details['auctiontype']?>
				</td>
			</tr>
			<tr>
				<td colspan='2' align='left'>
					<strong>Message : </strong><?php echo $common_auction_details['auction_msg']?>
				</td>
			</tr>
			<tr>
				<td colspan='2' align='left'>
					<strong>Auction Start Date : </strong><?php echo $common_auction_details['sdt']?>
				</td>
			</tr>
			<tr>
				<td colspan='2' align='left'>
					<strong>Particulars : </strong><?php echo $common_auction_details['particulars']?>
				</td>
			</tr>
			<tr>
				<td colspan='2' align='left'>
					<strong>No. of Lots : </strong><?php echo $common_auction_details['no_of_lots']?>
				</td>
			</tr>
		</tbody>		
	</table>
	<br>
	
	<?php
		if (!empty($get_bid_summary_report))
		{
			foreach ($get_bid_summary_report as $value)
			{
	?>
			<table border='0.5' cellspacing='0' cellpadding='0'>
					<thead>
						<tr>
							<th style="width:550px;" align='center' class='wrappable' colspan="8"  > <strong>Lot No :</strong> <?php echo $value['lotno'] ;?>   <br/> Product : <?php echo $value['prod'];?>
								</th>		
						</tr>
					</thead>
					<tbody>
						<tr>
							<td align='center' class='wrappable' style="width:75px;"><strong>Start Time : </strong> <br> <?php echo $value['st'];?> </td>
							<td align='center' class='wrappable' style="width:85px;"  colspan="2"><strong>Total Qty : </strong>  <?php echo $value['tq'];?></td>
							<td align='center' class='wrappable' style="width:70px;"  ><strong>Start Bid : </strong><br><?php echo $value['sb'];?></td>
							<td align='center' class='wrappable' style="width:105px;"  colspan="2"><strong>Proxy Bid : </strong><?php echo $value['proxy'];?></td>
							<td align='center' class='wrappable' style="width:105px;" colspan="2"> <strong> Plant : </strong> <?php echo $value['plant'];?> </td>
						</tr>
						<tr>
							<td align='center' class='wrappable' style="width:75px;"  > <strong>End Time : </strong>  <br> <?php echo $value['et'];?></td>
							<td align='center' class='wrappable' style="width:85px;"  colspan="2"> <strong>Bid Qty : </strong>  <?php echo $value['bq'];?></td>
							<td align='center' class='wrappable' style="width:70px;"    ><strong>Min. Incr : </strong><br><?php echo $value['iv'];?></td>
							<td align='center' class='wrappable' style="width:105px;"  colspan="2" ><strong>Proxy Date : </strong><?php echo $value['pd'];?>&nbsp;&nbsp;</td>
							<td align='center' class='wrappable' style="width:105px;"  colspan="2"><strong> Material : </strong><?php echo $value['vendorlotno'] ;?> </td>
						</tr>
						<tr>
							<td align='center' class='wrappable' bgcolor="#e0e0e0" style="font-weight:bold;width:50px;">STATUS</td>
							<td align='center' class='wrappable' bgcolor="#e0e0e0"  style="font-weight:bold;width:160px;">BIDDER NAME</td>
							<td align='center' class='wrappable' bgcolor="#e0e0e0"  style="font-weight:bold;width:80px;">DATE</td>
							<td align='center' class='wrappable' bgcolor="#e0e0e0"  style="font-weight:bold;width:80px;" >TIME</td>
							<td align='center' class='wrappable' bgcolor="#e0e0e0"  style="font-weight:bold;width:80px;">AMOUNT</td>
							<td align='center' class='wrappable' bgcolor="#e0e0e0"  style="font-weight:bold;width:80px;">BID AMOUNT</td>
							<td align='center' class='wrappable' bgcolor="#e0e0e0"  style="font-weight:bold;width:80px;">ALOTTED QTY</td>
							<td align='center' class='wrappable' bgcolor="#e0e0e0"  style="font-weight:bold;width:70px;"> NO. OF BIDS </td>
						</tr    >

				 		<?php
				 			 if (isset($value['lot_details']))
			 				{
								foreach ($value['lot_details'] as $key=>$lot_bid)
								{
						?>
								<tr>
									<td align="center" class='wrappable' style="width:50px;color:red;" >
										<?php echo $lot_bid['h']; ?>
									</td>
									<td align='center' class='wrappable' style="width:160px;"  >
										<?php echo $lot_bid['cname']; ?>
									</td>
									<td align="center" class='wrappable' style="width:70px;" >
										<?php echo $lot_bid['dt']; ?>
									</td>
									<td align="center" class='wrappable' style="width:80px;" >
										<?php echo $lot_bid['tm']; ?> Hrs.
									</td>
									<td align="center"class='wrappable' style="width:80px;">
										<?php echo $lot_bid['amt']; ?>
									</td>
									<td align="center"class='wrappable' style="width:80px;">
										<?php echo $lot_bid['bid_amount']; ?>
									</td>
									<td align="center"class='wrappable' style="width:80px;">
										<?php echo $lot_bid['bid_qty']; ?>
									</td>
									<td align="center" class='wrappable' style="width:70px;"  >
										<?php echo $lot_bid['nob']?> 
									</td>
								</tr>
									
						<?php
								}

							}
							else
							{
						?>
								<tr>
									<td align="center" class='wrappable' colspan="7"  style="width:700px;" >No Bid</td>
								</tr>
						
						<?php
							}
						
						?>
					
					</tbody>
			</table>
			<br>
				
	
	<?php
				
			}
			
		}
	
	
	?>
	
</page>
