<style>
tr {
	page-break-inside: avoid;
}
thead {
	display: table-header-group;
}
tfoot {
	display: table-row-group;
}
tr {
	page-break-inside: avoid;
}
td.wrappable,table.data_table td.wrappable {
    white-space: normal;	
}            
table td,table th {
	font-size:10px;
	
	
}
</style>


<page backbottom='110px' backtop="0px" backleft="0px" backright="0px" style="font-size:10px;">
   <page_footer>
        <table style='width: 100%;'>
			<tr>
				<td align='left' style='float:left;width:200px;'>
				Page :  [[page_cu]]/[[page_nb]] <br/>
				<?php echo (new DateTime(null, new DateTimeZone($_SESSION['user_tz'])))->format('D dS \of M Y h:i:s A'); ?>
				
				</td>
				<td align='right' style='float:right;width:545px;'>
					
				</td>
			</tr>
		</table>
    </page_footer>
	<br>
	<table border='0.5' cellspacing='0' cellpadding='0' width='100%'>
		<tbody>
			<tr>
				<td style="width:565px;" >
								
				</td>
				<td align='center' style="width:180px;">
					<span style='font-size:20px;color:#2a5395;'><strong><?php echo PRODUCT ?></strong></span>
				</td>
			</tr>
			<tr>
				<td colspan='2' align='center' >
					<strong>MULTIPLE CLIENTS WITH SAME IP REPORT FOR AUCTION :  <?php echo $common_auction_details['ynk_saleno']; ?> </strong>
				</td>
			</tr>
			<tr>
				<?php if ($common_auction_details['vendor_name']){ ?>
				<td colspan='2' align='center' >
					<strong><?php echo strtoupper($common_auction_details['vendor_name'])?></strong>
				</td>
				<?php } ?>
			</tr>
			<tr>
				<td colspan='2' align='center' >
					<strong>AUCTION DETAILS</strong>
				</td>
			</tr>
			<tr>
				<td colspan='2' align='left'>
					<strong>E-Auction Sale No :</strong><?php echo $common_auction_details['ynk_saleno']?>
				</td>
			</tr>
			<tr>
				<td colspan='2' align='left'>
					<strong>Auction Type : </strong><?php echo $common_auction_details['auctiontype']?>
				</td>
			</tr>
			<tr>
				<td colspan='2' align='left'>
					<strong>Message : </strong><?php echo $common_auction_details['auction_msg']?>
				</td>
			</tr>
			<tr>
				<td colspan='2' align='left'>
					<strong>Auction Start Date : </strong><?php echo $common_auction_details['sdt']?>
				</td>
			</tr>
			<tr>
				<td colspan='2' align='left'>
					<strong>Particulars : </strong><?php echo $common_auction_details['particulars']?>
				</td>
			</tr>
			<tr>
				<td colspan='2' align='left'>
					<strong>No. of Lots : </strong><?php echo $common_auction_details['no_of_lots']?>
				</td>
			</tr>
		</tbody>		
	</table>
	<br>
	<table border='0.5' cellspacing='0' cellpadding='0' width='100%'>
		<thead>			
			<tr>
				<th align='center' bgcolor="#e0e0e0">SR. NO</th>
				<th align='center' bgcolor="#e0e0e0">BIDDER NAME</th>
				<th align='center' bgcolor="#e0e0e0">ADDRESS</th>
				<th align='center' bgcolor="#e0e0e0">EMAIL</th>
				<th align='center' bgcolor="#e0e0e0">IP ADDRESS</th>
			</tr>
		</thead>
		<tbody>				
			<?php
				if ( ! empty($get_multiple_client_in_same_ip))
				{
					$count =1;
					foreach ($get_multiple_client_in_same_ip as $key=>$value)
					{
			?>
					<tr>
						<td align='center' class='wrappable' style='width:150px;'>
							<?php echo $count++; ?>
						</td>
						<td align='center' class='wrappable' style='width:150px;'>
							<?php echo $value['compname']; ?>
						</td>
						<td align='center' class='wrappable' style='width:150px;'>
							<?php echo $value['adddress']; ?>
						</td>
						<td align='center' class='wrappable' style='width:130px;'>
							<?php echo $value['email']; ?>
						</td>
						<td align='center' class='wrappable' style='width:145px;'>
							<?php echo $value['ip']; ?>
						</td>
					</tr>
			
			<?php
					}
			
				}
				else
				{
			?>
					<tr class='hd'><td  align='center' class='wrappable' style='width:750px;' colspan="5">No Records Found</td></tr>
			<?php
				}
			
				?>
		</tbody>
	</table>
	
</page>
