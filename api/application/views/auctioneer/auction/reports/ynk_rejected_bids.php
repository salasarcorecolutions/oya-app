<style>
tr {
	page-break-inside: avoid;
}
thead {
	display: table-header-group;
}
tfoot {
	display: table-row-group;
}
tr {
	page-break-inside: avoid;
}
td.wrappable,table.data_table td.wrappable {
    white-space: normal;	
}            
table td,table th {
	font-size:10px;
	
	
}
</style>


<page backbottom='110px' backtop="0px" backleft="0px" backright="0px" style="font-size:10px;">
   <page_footer>
        <table style='width: 100%;'>
			<tr>
				<td align='left' style='float:left;width:200px;'>
				Page :  [[page_cu]]/[[page_nb]] <br/>
				<?php echo (new DateTime(null, new DateTimeZone($_SESSION['user_tz'])))->format('D dS \of M Y h:i:s A'); ?>
				
				</td>
				<td align='right' style='float:right;width:545px;'>
					
				</td>
			</tr>
		</table>
    </page_footer>
	<br>
	<table border='0.5' cellspacing='0' cellpadding='0' width='100%'>
		<tbody>
			<tr>
				<td style="width:550px;" >
								
				</td>
				<td align='center' style="width:180px;">
					<span style='font-size:20px;color:#2a5395;'><strong><?php echo PRODUCT ?></strong></span>
				</td>
			</tr>
			<tr>
				<td colspan='2' align='center' >
					<strong>REJECTED BIDS REPORT FOR AUCTION :  <?php echo $common_auction_details['ynk_saleno']; ?> </strong>
				</td>
			</tr>
			<tr>
				<td colspan='2' align='center'>
					<strong><?php echo strtoupper($common_auction_details['vendor_name'])?></strong>
				</td>
			</tr>
			<tr>
				<td colspan='2' align='center' >
					<strong>AUCTION DETAILS</strong>
				</td>
			</tr>
			<tr>
				<td colspan='2' align='left'>
					<strong>E-Auction Sale No :</strong><?php echo $common_auction_details['ynk_saleno']?>
				</td>
			</tr>
			<tr>
				<td colspan='2' align='left'>
					<strong>Auction Type : </strong><?php echo $common_auction_details['auctiontype']?>
				</td>
			</tr>
			<tr>
				<td colspan='2' align='left'>
					<strong>Message : </strong><?php echo $common_auction_details['auction_msg']?>
				</td>
			</tr>
			<tr>
				<td colspan='2' align='left'>
					<strong>Auction Start Date : </strong><?php echo $common_auction_details['sdt']?>
				</td>
			</tr>
			<tr>
				<td colspan='2' align='left'>
					<strong>Particulars : </strong><?php echo $common_auction_details['particulars']?>
				</td>
			</tr>
			<tr>
				<td colspan='2' align='left'>
					<strong>No. of Lots : </strong><?php echo $common_auction_details['no_of_lots']?>
				</td>
			</tr>
		</tbody>
	</table>
	<br>
	<table border='0.5' cellspacing='0' cellpadding='0' width='100%'>
		<thead>
			<tr>
				<th align='center' bgcolor="#e0e0e0">LOT<br/>NO</th>
				<th align='center' bgcolor="#e0e0e0">MATERIAL<br/>CODE</th>
				<th align='center' bgcolor="#e0e0e0">REJECTED BIDS</th>
			</tr>
		</thead>
		<tbody>
			<?php
				if ( ! empty($get_rejected_bid_lots))
				{
					$count =1;
					foreach ($get_rejected_bid_lots as $key=>$value)
					{
					?>
						<tr>
							<td style='width:50px;' align='center' class='wrappable' style="width:80px;">
								<?php echo $value['lotno'] ?>
							</td>
							<td style='width:50px;' align='center' class='wrappable' style="width:100px;" >
							<?php echo $value['vendorlotno'];?>
							</td>
							<td align='center' class='wrappable' style="width:624px;">
								<?php
									if (isset($value['lot_details']))
									{
								?>
									<table border='0.5' cellspacing='0' cellpadding='0' >
									<tr>
										<th align='center' bgcolor="#e0e0e0" class='wrappable'>BIDDER NAME</th>
										<th align='center' bgcolor="#e0e0e0" class='wrappable'>BID TIME</th>
										<th align='center' bgcolor="#e0e0e0" class='wrappable'>BID AMOUNT</th>
										<th align='center' bgcolor="#e0e0e0" class='wrappable'>REMARK</th>
									</tr>
								<?php
										foreach ($value['lot_details'] as $k=>$v)
										{

										?>
										<tr>
											<td style='width:71px;' align='center' class='wrappable'><?php echo $v['compname']; ?></td>
											<td style='width:70px;' align='center' class='wrappable'><?php echo $v['bdt']. " ".$v['bid_time']; ?></td>
											<td style='width:70px;' align='center' class='wrappable'><?php echo $v['amount']; ?></td>
											<td style='width:391px;' align='center' class='wrappable'><?php echo $v['log_note']; ?></td>
										</tr>

										<?php
										}

								?>
									</table>
								<?php
									}
									else
									{
								?>	
										<strong>No Rejected Bid</strong>
								<?php
									}


								?>
							</td>
						</tr>

					<?php
					}

				} else {
					?>
					<tr><td align='center' style="width: 735px;" colspan="3">No data is available</td></tr>
					<?php

				}
			
				?>
		</tbody>
	</table>

</page>
