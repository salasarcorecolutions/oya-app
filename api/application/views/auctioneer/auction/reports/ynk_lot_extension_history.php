<style>
tr {
page-break-inside: avoid;
}
thead {display: table-header-group;}
   tfoot {display: table-row-group;}
   tr {page-break-inside: avoid;}
td.wrappable,
table.data_table td.wrappable {
    white-space: normal;	
	word-wrap: break-word;
}            
table td,table th {
	font-size:10px;
}
</style>

<page backbottom='110px' backtop="0px" backleft="0px" backright="0px" style="font-size:10px;">
   <page_footer>
        <table style='width: 100%;'>
			<tr>
				<td align='left' style='float:left;width:200px;'>
				Page :  [[page_cu]]/[[page_nb]] <br/>
				<?php echo (new DateTime(null, new DateTimeZone($_SESSION['user_tz'])))->format('D dS \of M Y h:i:s A'); ?>
				
				</td>
				<td align='right' style='float:right;width:545px;'>
					
				</td>
			</tr>
		</table>
    </page_footer>
	<br>
	<table border='0.5' cellspacing='0' cellpadding='0' width='100%'>
		<tbody>
			<tr>
				<td style="width:548px;" >
								
				</td>
				<td align='center' style="width:180px;">
					<span style='font-size:20px;color:#2a5395;'><strong><?php echo PRODUCT ?></strong></span>
				</td>
			</tr>
			<tr>
				<td colspan='2' align='center' >
					<strong>LOT EXTENSION HISTORY FOR AUCTION :  <?php echo $common_auction_details['ynk_saleno']; ?> </strong>
				</td>
			</tr>
			<tr>
				<td colspan='2' align='center' >
					<strong><?php echo strtoupper($common_auction_details['vendor_name'])?></strong>
				</td>
			</tr>
			<tr>
				<td colspan='2' align='center' >
					<strong>AUCTION DETAILS</strong>
				</td>
			</tr>
			<tr>
				<td colspan='2' align='left'>
					<strong>E-Auction Sale No :</strong><?php echo $common_auction_details['ynk_saleno']?>
				</td>
			</tr>
			<tr>
				<td colspan='2' align='left'>
					<strong>Auction Type : </strong><?php echo $common_auction_details['auctiontype']?>
				</td>
			</tr>
			<tr>
				<td colspan='2' align='left'>
					<strong>Message : </strong><?php echo $common_auction_details['auction_msg']?>
				</td>
			</tr>
			<tr>
				<td colspan='2' align='left'>
					<strong>Auction Start Date : </strong><?php echo $common_auction_details['sdt']?>
				</td>
			</tr>
			<tr>
				<td colspan='2' align='left'>
					<strong>Particulars : </strong><?php echo $common_auction_details['particulars']?>
				</td>
			</tr>
			<tr>
				<td colspan='2' align='left'>
					<strong>No. of Lots : </strong><?php echo $common_auction_details['no_of_lots']?>
				</td>
			</tr>
		</tbody>		
	</table>
	<br>
	<?php
		if ( ! empty($get_fwd_lot_extension_history))
		{
			foreach ($get_fwd_lot_extension_history as $key=>$value)
			{
				
	?>	
			
	<table border='0.5' cellspacing='0' cellpadding='0' width='100%'>
		<thead>		
			<tr>
				<th style="width:100px;" colspan="6" align='center'>
					<b>Lot No:</b>  <?php echo $value['lotno'] ;?>   <br> <b>Product:</b><?php echo $value['prod'];?>
				</th>
			</tr>
		</thead>
		<tbody>	
		<tr>
			<td style="width:100px;" align='center' class='wrappable' ><b>Start Time : </b> <br> <?php echo $value['st'];?> </td>
			<td style="width:100px;" align='center' class='wrappable'  ><b>Total Qty : </b> <br> <?php echo $value['tq'];?></td>
			<td style="width:100px;" align='center' class='wrappable' ><b>Star Bid : </b>  <?php echo $value['sb'];?></td>
			<td style="width:100px;" align='center' class='wrappable'> <b>Proxy Bid : </b> <?php echo $value['proxy'] ;?> </td>
			<td style="width:100px;" align='center' class='wrappable' style="width:173px;" ><b>Plant : </b> <?php echo $value['plant'] ;?> </td>
		</tr>
		<tr>
			<td align='center' class='wrappable' ><b>End Time : </b> <br> <?php echo $value['et'];?></td>
			<td align='center' class='wrappable' ><b>Bid Qty : </b> <br> <?php echo $value['bq'] ;?></td>
			<td align='center' class='wrappable' ><b>Min Decr : </b><?php echo $value['iv'];?></td>
			<td align='center' class='wrappable'><b>Proxy  Date : </b> <br> <?php echo $value['pd'];?></td>
			<td align='center' class='wrappable' style="width:173px;"   ><b>Material:  </b> <?php echo $value['vendorlotno'];?></td>
		</tr>
		<tr>
			<td align='center' class='wrappable' style='font-weight:bold;width:100px;' bgcolor="#e0e0e0">SR NO</td>
			<td align='center' class='wrappable' style='font-weight:bold;width:120px;' bgcolor="#e0e0e0">EXTENSION (HH:MM:SS)</td>
			<td align='center' class='wrappable' style='font-weight:bold;width:140px;' bgcolor="#e0e0e0">UPDATED BY</td>
			<td align='center' class='wrappable' style='font-weight:bold;width:140px;' bgcolor="#e0e0e0">UPDATED ON</td>
			<td align='center' class='wrappable' style='font-weight:bold;width:210px;' bgcolor="#e0e0e0">RESASON</td>
		</tr>
		<?php 
			if (isset($value['lot_details']))
			{
				$i = 1;
				foreach ($value['lot_details']  as $lot_details)
				{
		?>
					<tr>		
						<td  align='center' class='wrappable' style="width:100px;"><?php echo $i ?></td>
						<td align='center' class='wrappable ' style="width:120px;"><?php echo gmdate("H:i:s",$lot_details['extended_by']); ?></td>
						<td align='center' class='wrappable' style="width:140px;" ><?php echo $lot_details['action_by']; ?> </td>
						<td align='center' class='wrappable' style="width:140px;" > <?php echo $lot_details['update_on']; ?> Hrs</td>
						<td align='center' class='wrappable' style="width:210px;" ><?php echo $lot_details['reason']; ?></td>
					</tr>
		
		<?php
				$i++;
				}
			}
			else
			{
		?>
				<tr>
					<td colspan='6' class='wrappable' align='center'> No Extension </td>
				</tr>
		<?php
				
			}
			
		
		?>
		</tbody>
	</table>
	<br>
	<?php 
	
	
			}
		}
		
	?>
</page>
