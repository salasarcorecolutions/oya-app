<style>
tr {
	page-break-inside: avoid;
}
thead {
	display: table-header-group;
}
tfoot {
	display: table-row-group;
}
tr {
	page-break-inside: avoid;
}
td.wrappable,table.data_table td.wrappable {
    white-space: normal;	
}            
table td,table th {
	font-size:10px;
	
	
}
</style>

<page backbottom='110px' backtop="0px" backleft="0px" backright="0px" style="font-size:10px;">
   <page_footer>
        <table style='width: 100%;'>
			<tr>
				<td align='left' style='float:left;width:200px;'>
				Page :  [[page_cu]]/[[page_nb]] <br/>
				<?php echo (new DateTime(null, new DateTimeZone($_SESSION['user_tz'])))->format('D dS \of M Y h:i:s A'); ?>
				
				</td>
				<td align='right' style='float:right;width:545px;'>
					
				</td>
			</tr>
		</table>
    </page_footer>
	<br>
	<table border='0.5' cellspacing='0' cellpadding='0' width='100%'>
		<tbody>
			<tr>
				<td style="width:550px;" >
								
				</td>
				<td align='center' style="width:180px;">
					<span style='font-size:20px;color:#2a5395;'><strong><?php echo PRODUCT ?></strong></span>
				</td>
			</tr>
			<tr>
				<td colspan='2' align='center' >
					<strong>CMD EMD REPORT FOR AUCTION :  <?php echo $common_auction_details['ynk_saleno']; ?> </strong>
				</td>
			</tr>
			<tr>
				<td colspan='2' align='center' >
					<strong><?php echo strtoupper($common_auction_details['vendor_name'])?></strong>
				</td>
			</tr>
			<tr>
				<td colspan='2' align='center' >
					<strong>AUCTION DETAILS</strong>
				</td>
			</tr>
			<tr>
				<td colspan='2' align='left'>
					<strong>E-Auction Sale No :</strong><?php echo $common_auction_details['ynk_saleno']?>
				</td>
			</tr>
			<tr>
				<td colspan='2' align='left'>
					<strong>Auction Type : </strong><?php echo $common_auction_details['auctiontype']?>
				</td>
			</tr>
			<tr>
				<td colspan='2' align='left'>
					<strong>Message : </strong><?php echo $common_auction_details['auction_msg']?>
				</td>
			</tr>
			<tr>
				<td colspan='2' align='left'>
					<strong>Auction Start Date : </strong><?php echo $common_auction_details['sdt']?>
				</td>
			</tr>
			<tr>
				<td colspan='2' align='left'>
					<strong>Particulars : </strong><?php echo $common_auction_details['particulars']?>
				</td>
			</tr>
			<tr>
				<td colspan='2' align='left'>
					<strong>No. of Lots : </strong><?php echo $common_auction_details['no_of_lots']?>
				</td>
			</tr>
		</tbody>		
	</table>
	
	<br>
	<table border='0.5' cellspacing='0' cellpadding='0' width='100%'>
		<thead>		
			<tr>
				<th align='center' bgcolor="#e0e0e0">SR.<br>No</th>
				<th align='center' bgcolor="#e0e0e0">EMD / CMD</th>
				<th align='center' bgcolor="#e0e0e0">COMPANY NAME</th>
				<th align='center' bgcolor="#e0e0e0">Lot(s) Permitted</th>
				<th align='center' bgcolor="#e0e0e0">DD No</th>
				<th align='center' bgcolor="#e0e0e0">DD date</th>
				<th align='center' bgcolor="#e0e0e0">Bank Name</th>
			</tr>
		</thead>
		<tbody>			
			<?php
				if ( ! empty($auction_report))
				{
					$count =1;
					foreach ($auction_report as $value)
					{
						
						$lots = $value['alloted_lots'];
						if(!empty($lots)){
							$lots_array = explode(",",$lots);
							sort($lots_array);
							$lots = implode(", ",$lots_array);
						}
			?>
				<tr>
					<td align='center' class='wrappable' style='width:30px;'><?php echo $count++?></td>
					<td align='center' class='wrappable' style='width:60px;'><?php echo ($value['cmdamt'] > 0) ?  $value['cmdamt'] :  $value['emdamt'] ;?></td>
					<td align='center' class='wrappable' style='width:150px;'><?php echo $value['compname']?></td>
					<td align='center' class='wrappable' style='width:240px;'><?php echo str_replace(',',', ',$value['alloted_lots'])?></td>
					<td align='center' class='wrappable' style='width:70px;'><?php echo ($value['cmdamt'] > 0) ?  $value['cmdddno'] :  $value['emdddno'] ;?></td>
					<td align='center' class='wrappable' style='width:70px;'><?php echo ($value['cmdamt'] > 0) ?  $value['cmddate'] :  $value['emddate'] ;?></td>
					<td align='center' class='wrappable' style='width:80px;'><?php echo ($value['cmdamt'] > 0) ?  $value['cmdbank'] :  $value['emdbank'] ;?></td>
					
				</tr>
			
			<?php
					}
					
				}
				else
				{
			?>
					<tr><td align='center' colspan="7" class='wrappable' style='width:735px;'>No Records found</td></tr>
			<?php
				}
			
				?>
		</tbody>
	</table>
	
</page>
