<style>
tr {
	page-break-inside: avoid;
}
thead {
	display: table-header-group;
}
tfoot {
	display: table-row-group;
}
tr {
	page-break-inside: avoid;
}
td.wrappable,table.data_table td.wrappable {
    white-space: normal;	
}            
table td,table th {
	font-size:10px;
	
	
}
</style>


<page backbottom='110px' backtop="0px" backleft="0px" backright="0px" style="font-size:10px;">
   <page_footer>
        <table style='width: 100%;'>
			<tr>
				<td align='left' style='float:left;width:200px;'>
				Page :  [[page_cu]]/[[page_nb]] <br/>
				<?php echo (new DateTime(null, new DateTimeZone($_SESSION['user_tz'])))->format('D dS \of M Y h:i:s A'); ?>
				
				</td>
				<td align='right' style='float:right;width:545px;'>
					
				</td>
			</tr>
		</table>
    </page_footer>
	<br>
	<table border='0.5' cellspacing='0' cellpadding='0' width='100%'>
		<tbody>
			<tr>
				<td style="width:554px;" >
								
				</td>
				<td align='center' style="width:180px;">
					<span style='font-size:20px;color:#2a5395;'><strong><?php echo PRODUCT ?></strong></span>
				</td>
			</tr>
			<tr>
				<td colspan='2' align='center' >
					<strong>LOT DETAILS FOR AUCTION :  <?php echo $common_auction_details['ynk_saleno']; ?> </strong>
				</td>
			</tr>
			<tr>
				<td colspan='2' align='center' >
					<strong><?php echo strtoupper($common_auction_details['vendor_name'])?></strong>
				</td>
			</tr>
			<tr>
				<td colspan='2' align='center' >
					<strong>AUCTION DETAILS</strong>
				</td>
			</tr>
			<tr>
				<td colspan='2' align='left'>
					<strong>E-Auction Sale No :</strong><?php echo $common_auction_details['ynk_saleno']?>
				</td>
			</tr>
			<tr>
				<td colspan='2' align='left'>
					<strong>Auction Type : </strong><?php echo $common_auction_details['auctiontype']?>
				</td>
			</tr>
			<tr>
				<td colspan='2' align='left'>
					<strong>Message : </strong><?php echo $common_auction_details['auction_msg']?>
				</td>
			</tr>
			<tr>
				<td colspan='2' align='left'>
					<strong>Auction Start Date : </strong><?php echo $common_auction_details['sdt']?>
				</td>
			</tr>
			<tr>
				<td colspan='2' align='left'>
					<strong>Particulars : </strong><?php echo $common_auction_details['particulars']?>
				</td>
			</tr>
			<tr>
				<td colspan='2' align='left'>
					<strong>No. of Lots : </strong><?php echo $common_auction_details['no_of_lots']?>
				</td>
			</tr>
		</tbody>		
	</table>
	<br>
	<table  border='0.5' cellspacing='0' cellpadding='0' width='100%'>
		<thead>
			<tr>
				<th align="center">AUCTIONEER<br> LOT NO.</th>
				<?php if ($is_lot_group == 'Yes'){ ?>
					<th align="center">SUB LOT NO</th>
				<?php }	?>
				<th align="center">VENDOR<br> LOT NO</th>
				<th align="center">DESCRIPTION</th>
				<th align="center">BST</th>
				<th align="center">BET</th>
				<th align="center">QUANTITY.</th>
				<th align="center" nowrap>BID <br> QUANTITY</th>
				<th align="center">START BID</th>
				<th align="center">INCREMENT</th>
				<th align="center">PART BID</th>
				<th align="center">MID PART<br> QUANTITY</th>
				<th align="center">TAX</th>
			</tr>
		</thead>
		<tbody>		
			<?php 
				if ( ! empty($lot_details))
				{
					$i=0;
					foreach ($lot_details as $row)
					{
						$c="";
						if ($is_lot_group == 'Yes'){
							if ( ! empty($sub_lot)){
								if ( ! empty($sub_lot[$row['ynk_lotno']])){
									$c = count($sub_lot[$row['ynk_lotno']])+1;
								}
							}
						} else {
							$c="";
						}
			?>
				<tr>
					<td style="width: 50px"><?php echo $row['ynk_lotno']; ?></td>
					<?php
						if ($is_lot_group == 'Yes'){
						?>
						<td></td>
						<?php
						}
						?>
					<td style="width: 50px"><?php echo $row['vendorlotno']; ?></td>
					<td style="width: 88px"><?php echo $row['product']; ?></td>
					<td style="width: 50px" nowrap  rowspan='<?php echo $c?>'><?php echo $row['stime']; ?> Hrs.</td>
					<td style="width: 50px" nowrap rowspan='<?php echo $c?>'><?php echo $row['etime']; ?>  Hrs.</td>
					<td style="width: 50px" nowrap rowspan='<?php echo $c?>'><?php echo $row['totalqty']." ".$row['totunit']; ?></td>
					<td style="width: 50px" rowspan='<?php echo $c?>'><?php echo $row['bidqty']." ".$row['bidunit']; ?></td>
					<td style="width: 50px" align="center" rowspan='<?php echo $c?>'><?php echo $row['startbid']; ?></td>
					<td style="width: 50px" align="center" rowspan='<?php echo $c?>'><?php echo $row['incrby'];?></td>
					<td style="width: 50px" align="center" rowspan='<?php echo $c?>'><?php echo ($row['part_bid'] == "Y") ? "Yes" : "No" ;?></td>
					<td style="width: 50px" align="center" rowspan='<?php echo $c?>'><?php echo $row['min_part_bid'];?></td>
					<td style="width: 50px" align="center" rowspan='<?php echo $c?>'>
					<?php 
					if ( ! empty($row['tax']))
					{
						foreach($row['tax'] as $key=>$val)
						{
							echo $key.' : '.$val.'<br/>';
						}
						
					}?>
					</td>
				</tr>
				<?php 
					if ($is_lot_group == 'Yes'){ 
						if ( ! empty($sub_lot)){
							if ( ! empty($sub_lot[$row['ynk_lotno']])){
								foreach ($sub_lot[$row['ynk_lotno']] as $key=>$value){
		
					?>
						<tr>
							<td></td>
							<td></td>
							<td><?php echo $value['lotno']; ?></td>
							<td><?php echo $value['vlotno']; ?></td>
							<td><?php echo $value['prod']; ?></td>
						</tr>
					<?php
											}
										}
								}
						}
					}
				
			
				}
				else
				{
			?>
					<tr>
						<td align='center' class='wrappable' style="width:739px;" colspan="13">No Records Found</td>
					</tr>
			<?php
				}
			
			
			?>
		</tbody>
	</table>
</page>