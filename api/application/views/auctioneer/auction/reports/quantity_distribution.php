
<style>
tr {
	page-break-inside: avoid;
}
thead {
	display: table-header-group;
}
tfoot {
	display: table-row-group;
}
tr {
	page-break-inside: avoid;
}
td.wrappable,table.data_table td.wrappable {
    white-space: normal;	
}            
table td,table th {
	font-size:10px;
	
	
}
</style>


<page backbottom='110px' backtop="0px" backleft="0px" backright="0px" style="font-size:10px;">
   <page_footer>
        <table style='width: 100%;'>
			<tr>
				<td align='left' style='float:left;width:200px;'>
				Page :  [[page_cu]]/[[page_nb]] <br/>
				<?php echo (new DateTime(null, new DateTimeZone($_SESSION['user_tz'])))->format('D dS \of M Y h:i:s A'); ?>
				
				</td>
				<td align='right' style='float:right;width:545px;'>
					
				</td>
			</tr>
		</table>
    </page_footer>
	<br>
	
	<table border='0.5' cellspacing='0' cellpadding='0' width='100%'>
		<tbody>
			<tr>
				<td style="width:560px;" >
								
				</td>
				<td align='center' style="width:180px;">
					<span style='font-size:20px;color:#2a5395;'><strong><?php echo PRODUCT ?></strong></span>
				</td>
			</tr>
			<tr>
				<td colspan='2' align='center' >
					<strong>BID SUMMARY REPORT FOR AUCTION :  <?php echo $common_auction_details['ynk_saleno']; ?> </strong>
				</td>
			</tr>
			<tr>
				<td colspan='2' align='center' >
					<strong><?php echo strtoupper($common_auction_details['vendor_name'])?></strong>
				</td>
			</tr>
			<tr>
				<td colspan='2' align='center' >
					<strong>YANKEE AUCTION DETAILS</strong>
				</td>
			</tr>
			<tr>
				<td colspan='2' align='left'>
					<strong>E-Auction Sale No :</strong><?php echo $common_auction_details['ynk_saleno']?>
				</td>
			</tr>
			<tr>
				<td colspan='2' align='left'>
					<strong>Auction Type : </strong><?php echo $common_auction_details['auctiontype']?>
				</td>
			</tr>
			<tr>
				<td colspan='2' align='left'>
					<strong>Message : </strong><?php echo $common_auction_details['auction_msg']?>
				</td>
			</tr>
			<tr>
				<td colspan='2' align='left'>
					<strong>Auction Start Date : </strong><?php echo $common_auction_details['sdt']?>
				</td>
			</tr>
			<tr>
				<td colspan='2' align='left'>
					<strong>Particulars : </strong><?php echo $common_auction_details['particulars']?>
				</td>
			</tr>
			<tr>
				<td colspan='2' align='left'>
					<strong>No. of Lots : </strong><?php echo $common_auction_details['no_of_lots']?>
				</td>
			</tr>
		</tbody>		
	</table>
	<br>
	
    <?php
    
        if ( ! empty($quantity_distribution))
        {
            foreach ($quantity_distribution as $k=>$v)
            {
    ?>
                   	<table border='0.5' cellspacing='0' cellpadding='0'>
                        <thead>
                            <tr>
                                <th style="width:650px;" align='center' class='wrappable' colspan="5"  > 
								<strong>Lot No :</strong> <?php echo $v['lotno'] ;?>  <br/> 
								<strong>Total Qty :</strong> <?php echo $v['total_qty'].' '.$v['total_unit'] ;?>  <br/> 

                              </th>		
                            </tr>
                        </thead>
                        <tbody>
                        <tr>
                            
                                <td align='center' class='wrappable' bgcolor="#e0e0e0"  style="font-weight:bold;width:50px;">BIDDER NAME</td>
                                <td align='center' class='wrappable' bgcolor="#e0e0e0"  style="font-weight:bold;width:50px;">BID DATE TIME</td>
                                <td align='center' class='wrappable' bgcolor="#e0e0e0" style="font-weight:bold;width:50px;">STATUS</td>
                                <td align='center' class='wrappable' bgcolor="#e0e0e0"  style="font-weight:bold;width:50px;">BID RATE</td>
                                <td align='center' class='wrappable' bgcolor="#e0e0e0"  style="font-weight:bold;width:50px;">ALOTTED QTY</td>

                                


                        </tr>
                        <?php
                            if ( ! empty($v['bid_details']))
                            {
                                foreach($v['bid_details'] as $key=>$value)
                                {
                            ?>
                                    <tr>
                                        <td align='center' class='wrappable'><?php echo $value['compname']?></td>
                                        <td align='center' class='wrappable' >
                                            <?php echo $value['bid_date'].' '.$value['bid_time']?>
                                        </td>
                                        <td align='center' class='wrappable' ><?php echo $value['status_in_lot']?></td>
                                        <td align='center' class='wrappable' ><?php echo $value['max_bid_amt']?></td>
                                        <td align='center' class='wrappable' ><?php echo $value['sub_alloted_qty'].' '.$v['bid_unit']?></td>

                                    </tr>
                            <?php
                                }
                            }

                        ?>
						<tr>
                            <td colspan="4" align='right'>AVAILABLE QTY </td> 
                            <td align='center'> <?php echo $v['available_qty'].' '.$v['total_unit']?> </td>
                        </tr>		
                        </tbody>
                    </table>
                    <br/>
    <?php
            }
        }
    ?>
</page>
