<!--CONTENT CONTAINER-->
<!--===================================================-->
<div id="content-container">
	<!--Page Title-->
	<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
	<div id="page-head">
	<div id="page-title">
		<h1 class="page-header text-overflow"><?php echo $pagetitle ?></h1>
	</div>
	<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
	<!--End page title-->


	<!--Breadcrumb-->
	<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
	<ol class="breadcrumb">
		<li><a href="<?php echo base_url('auction/index');?>">Home</a></li>
		<li class="active"><?php echo $pagetitle ?></li>
	</ol>
</div>
	<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
	<!--End breadcrumb-->
	<input type="hidden" id="t11" name="auctionId">
	<div id="page-content">	
	<div class="float_btn">
		<a class="btn btn-primary btn-icon btn-circle add-tooltip" data-placement="top" data-toggle="tooltip" data-original-title="Back" href='<?php echo base_url('auctioneer/auction/view_lots/'.$auctionid); ?>' ><i class="fa fa-arrow-left" aria-hidden="true"></i></a>
	</div>
		<div class="panel">
			<div class="panel-heading">
                <h3 class="panel-title"><?php echo $pagetitle ?>
                    <div class="pull-right">
                        <button class="btn btn-primary btn-labeled fa fa-plus" type="button" id="add_document"></i>Add</button>
                        <button class="btn btn-info btn-labeled fa fa-edit" type="button" id="edit_document"></i>Edit</button>
                        <button class="btn btn-success btn-labeled fa fa-download" type="button" id="download_document"></i>Download</button>

                    </div>
                </h3>
			</div>
			<div class="panel-body">
				<fieldset>
					<div class="table-responsive">
						<table class="table table-striped table-bordered table-hover no-footer dtr-inline">
							<thead>
								<tr>
									<th>#</th>
									<th>Document Name</th>
									<th>Uploaded By</th>
									<th>Uploaded Date Time</th>
                                </tr>
                                <tbody>
                                    <?php foreach ($document_details as $dd){ ?>
                                        <tr>
                                            <td><input type="radio" name="document_id" value="<?php echo $dd['auctioneer_documents_id']; ?>" /></td>
                                            <td><?php echo $dd['document_name']; ?></td>
                                            <td><?php echo$dd['c_name']; ?></td>
                                            <td><?php echo $dd['uploaded_datetime']; ?></td>
                                        </tr>
                                    <?php } ?>
                                </tbody>
							</thead>
						</table>
					</div>
                </fieldset>
			</div>
		</div>
	</div>
</div>

<!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Upload Common Documents</h4>
            </div>
            <form id="upload_common_document" enctype="multipart/form-data">
                <div class="modal-body" style="min-height:155px;">
                    <div class="form-group col-md-12">
                        You can upload upto 5 documents.
                        <input type="hidden" name="doc_id" id="doc_id" />
                        <div class="col-md-3">
                            <label>Document Name</label>
                        </div>
                        <div><input class="form-control" type="text" name="document_name" id="document_name" /></div>
                        <div class="col-md-3">
                            <label>File Name</label>
                        </div>
                        <div><input class="form-control" type="file" name="document" id="document" /></div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class='btn btn-primary'>Submit</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </form>
        </div>
    </div>
</div>

<!--===================================================-->
<!--END CONTENT CONTAINER-->
<script>
    $(document).ready(function(){
        $(".table").dataTable();
    })

    function clearModal()
    {
        $("#doc_id").val('');
        $("#document").val('');
        $("#document_name").val('');
    }

    $("#add_document").click(function(){
        clearModal();
        $("#myModal").modal('show')
    })

    $("#edit_document").click(function(){
        clearModal();
        var id = $('input[name="document_id"]:checked').val();
        if (id){
            $.ajax({
                url:"<?php echo base_url('auctioneer/auction/document_data_by_id/');?>",
                type: 'post',
                dataType:'json',
                data:{
                    id: id
                },
                success: function (response) {
                    $("#document_name").val(response.document_name);
                    $("#doc_id").val(response.auctioneer_documents_id);
                    $("#myModal").modal('show')
                },
                error: function(){
                    alert('Problem in fetching document data. Please try again.');
                }
            });
        } else {
            Display_msg('Select Document', 'error');
        }
    })

    $("#download_document").click(function(){
        var id = $('input[name="document_id"]:checked').val();
        if (id){
            $.ajax({
                url:"<?php echo base_url('auctioneer/auction/get_document_url/');?>",
                type: 'post',
                dataType:'json',
                data:{
                    id: id
                },
                success: function (response) {
                    if (response[0]){
                        window.open(response[0]);
                    } else {
                        Display_msg('File Not Found', 'error');
                    }
                },
                error: function(){
                    alert('Problem in fetching document data. Please try again.');
                }
            });
        } else {
            Display_msg('Select Document', 'error');
        }
    })




$(function(){
	var vRules = {
        document_name:{required:true},
        document:{required:true},

	};
	var vMessages = {
		document_name:{required:"<p class='text-danger'>Please Enter Document Name.</p>"},
	};

	$("#upload_common_document").validate({
		rules: vRules,
		messages: vMessages,
		submitHandler: function(form){
            $("#upload_common_document").ajaxSubmit({
                url:"<?php echo base_url('auctioneer/auction/add_edit_common_document/');?>",
                type: 'post',
                dataType:'json',
                success: function (response) {
                    Display_msg(response.message, response.status);
                    if(response.status=="success"){
                        setTimeout(function(){ location.reload(); }, 3000);
                    }
                }
            });
		}
	})
});


</script>