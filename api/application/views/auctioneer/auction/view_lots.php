
<!--CONTENT CONTAINER-->
<!--===================================================-->
<div id="content-container">

<div id="page-head">
	<!--Page Title-->
	<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
	<div id="page-head">
	
	<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
	<!--End page title-->


	<!--Breadcrumb-->
	<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
	<ol class="breadcrumb">
		<li><a href="<?php echo base_url('auction/index');?>"><i class="fa fa-home"></i></a></li>
		<li><a href="<?php echo base_url('auctioneer/auction/index');?>"> Auction</a></li>
		<li class="active">Lots</li>
	</ol>
	</div>
	<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
	<!--End breadcrumb-->
</div>
	<input type="hidden" id="t11" name="auctionId">
	<div id="page-content">
		
		<div class="panel">
			<div class="panel-heading">
				<h3 class="panel-title">Lots</h3>
			</div>
			
			<div class="panel-body">
				<div class="table-responsive">
					<table class="table table-striped table-bordered table-hover no-footer dtr-inline">
						<thead>
							<tr>
								<th>Sale No</th>
								<th>Start Time</th>
								<th>End Time</th>
								<th>Particulars</th>
							</tr>
							<tr>
								<td><?php echo $auction_details['ynk_saleno'] ?></td>
								<td><?php echo date('d-m-Y H:i:s',strtotime($auction_details['start_date']." ".$auction_details['stime'])) ?></td>
								<td><?php echo date('d-m-Y H:i:s',strtotime($auction_details['end_date']." ".$auction_details['etime'])) ?></td>
								<td><?php echo $auction_details['particulars'] ?></td>
							</tr>
						</thead>
					</table>
				</div>
				<div class="pad-btm form-inline">
					<div class="row">
							<?php if ($this->common_model->check_permission('Lot Add')): ?>
								<a class="btn btn-sm btn-success btn-labeled fa fa-plus" href="javascript:void(0);" title="Add Auction Lot" name="btn_add" id="btn_add" onclick="lot_actions('ADD')">Add</a>
							<?php endif; ?>

							<?php if ($this->common_model->check_permission('Lot Edit')): ?>
								<a class="btn btn-sm btn-primary btn-labeled fa fa-edit" href="javascript:void(0);" title="Edit Auction Lot" name="btn_edit" id="btn_edit" onclick="lot_actions('EDIT')">Edit</a>
							<?php endif; ?>

							<?php if ($this->common_model->check_permission('Lot time increase')): ?>
								<a class="btn btn-sm btn-primary btn-labeled fa fa-clock-o" href="javascript:void(0);" title="Update Lot Timing" name="btn_lot_timing" id="btn_lot_timing" onclick="lot_actions('LOT_TIME_UPDATE')">Update Lot Timing</a>
							<?php endif; ?>

							<?php if ($this->common_model->check_permission('Lot Add')): ?>
								<?php if (empty($lot_details)): ?>						
									<a class="btn btn-sm btn-primary btn-labeled fa fa-file-excel-o" href="javascript:void(0);" title="Upload Lot Excel" name="btn_lot_timing" id="btn_lot_timing" onclick="lot_actions('UPLOAD_EXCEL')">Upload Lot Excel</a>
								<?php endif; ?>
							<?php endif; ?>

							<?php if ($this->common_model->check_permission('Lot tax Edit')): ?>
								<a class="btn btn-sm btn-primary btn-labeled fa fa-credit-card" href="javascript:void(0);" title="Update Lot Tax Details" name="btn_lot_tax" id="btn_lot_tax" onclick="lot_actions('LOT_TAX_UPDATE')">Update Lot Tax Details</a>
							<?php endif; ?>

							<?php if ($this->common_model->check_permission('Upload lot image')): ?>
								<a class="btn btn-sm btn-primary btn-labeled fa fa-picture-o" href="javascript:void(0);" title="Upload Lot Images" name="btn_upload_lot_images" onclick="lot_actions('UPLOAD_LOT_IMAGES')">Upload Lot Images</a>
							<?php endif; ?>
					
							<a class="btn btn-sm btn-primary btn-labeled fa fa-cog" href="javascript:void(0);" title="View Lots" name="btn_lotSettings" id="btn_lotSettings">Lots Settings</a>
					
					
							<?php 
							if ($this->common_model->check_permission('Bid History View'))
							{ 
								?>
								<a class="btn btn-sm btn-primary btn-labeled fa fa-history" href="javascript:void(0);" title="View Bid History" name="btn_view_client_bid" onclick="lot_actions('VIEW_BID_HISTORY')">View Bid History</a>
								<?php
							}
							if ($this->common_model->check_permission('Client Bid Delete'))
							{ 
								?>
								<a class="btn btn-sm btn-danger btn-labeled fa fa-trash" href="javascript:void(0);" title="Delete Client Bid" name="btn_delete_client_bid" onclick="lot_actions('DELETE_CLIENT_BID')">Delete Client Bid</a>
								<?php
							}
							?>
								<?php if ($this->common_model->check_permission('Lot delete')): ?>
									<a class="btn btn-sm btn-danger btn-labeled fa fa-trash" href="javascript:void(0);" title="Delete Auction Lot" name="btn_delete" onclick="lot_actions('DELETE')">Delete Lot</a>
								<?php endif; ?>
					
					</div>
				</div>
				
				<div class="table-responsive">
					<table class="table table-striped table-bordered table-hover no-footer dtr-inline">
						<thead>
							<tr>
								<th data-bSortable="false">#</th>
								<th><?php echo $lsd['ltno']==""?'LOT NO':$lsd['ltno']; ?></th>
								<th><?php echo @$lsd['vltno']==""?'VENDOR LOT NO':$lsd['vltno']; ?></th>
								<th><?php echo @$lsd['product']==""?'DESCRIPTION':$lsd['product']; ?></th>
								<th><?php echo @$lsd['plant']==""?'PLANT':$lsd['plant']; ?> </th>
								<th><?php echo @$lsd['lsd']==""?'START DATE':$lsd['lsd']; ?></th>
								<th><?php echo @$lsd['led']==""?'END DATE':$lsd['led']; ?></th>
								<th><?php echo @$lsd['cmd_emd']==""?'CMD/EMD':$lsd['cmd_emd']; ?></th>
								<th><?php echo @$lsd['tb']==""?'TIME<br>BOUNDED':$lsd['tb']; ?> </th>
								<th><?php echo @$lsd['tq']==""?'TOTAL<BR>QTY':$lsd['tq']; ?></th>
								<th><?php echo @$lsd['bq']==""?'BID<BR>QTY':$lsd['bq']; ?></th>
								<th><?php echo @$lsd['sb']==""?'START<BR>PRICE':$lsd['sb']; ?></th>
								<th><?php echo @$lsd['db']==""?'INCR.':$lsd['db']; ?></th>								
								<th>PROXY START DATE</th>
								<th>IS BIDDED?</th>
							</tr>
						</thead>
						<tbody>
							<?php
                            if ( ! empty($lot_details))
                            {
								foreach ($lot_details as $single_lot)
								{ 
                            ?>
									<tr>
										<td>
											<input type='radio' id='select_lotno_<?php echo $single_lot['ynk_lotno']; ?>' name='select_lotno' value='<?php echo $single_lot['ynk_lotno']; ?>' />
										</td>
										<td><?php echo $single_lot['ynk_lotno']; ?></td>
										<td><?php echo $single_lot['vendorlotno']; ?></td>
										<td><?php echo nl2br($single_lot['product']); ?></td>
										<td><?php echo $single_lot['plant']; ?></td>											
										<td nowrap><?php echo date('d-m-Y',strtotime($single_lot['lot_open_date']))."<br>".date('H:i:s',strtotime($single_lot['stime'])); ?></td>
										<td nowrap><?php echo date('d-m-Y',strtotime($single_lot['lot_close_date']))."<br>".date('H:i:s',strtotime($single_lot['etime'])); ?></td>
										<td><?php echo $single_lot['cmd']; ?></td>
										<td><?php echo ($single_lot['time_bounded'] == 'Y') ? 'Yes' : 'No' ;?></td>
										<td><?php echo $single_lot['totalqty'].' '.$single_lot['totunit']; ?></td>
										<td><?php echo $single_lot['bidqty'].' '.$single_lot['bidunit']; ?></td>
										<td><?php echo $single_lot['startbid']; ?></td>
										<td><?php echo $single_lot['incrby']; ?></td>										
										<td nowrap><?php echo date('d-m-Y',strtotime($single_lot['proxystartdate']));?></td>
										<td><?php echo ($single_lot['bid_count'] > 0) ? '<span class="text-success">Yes</span>' : '<span class="text-danger">No</span>' ;?></td>
									</tr> <?php
								}
                            }
                            else
                            {
								?>
								<tr>
									<td colspan = '16' class="text-center">No Records Found</td>
								</tr>
								<?php
							}
							?>
						</tbody>
					</table>
				</div>
			</div>	
		</div>
	</div>	
</div>

<!-- Lot Add Edit Modal -->
<style>
.form-group {
    margin-bottom: 5px !important;
}
</style>
<!-- Modal to Add or Edit Lot Details -->
<div id="lot_add_edit_modal" class="modal fade" >
	<div class="modal-dialog" style="width: 80% !important;margin: 30px auto;">
		<div class="modal-content">
			<form name="lot_add_edit_form" id="lot_add_edit_form" class='form-horizontal' method="post" >
				<input type='hidden' id='auctionid' name='ynk_auctionid' value='<?php echo $auctionid; ?>'/>
				<input type='hidden' id='lot_action' name='lot_action' value=''/>
				
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title" id="modal_header_text">Lot Add Edit(SALE NO : <?php echo $auction_details['ynk_saleno'];?>)</h4>
				</div>
				<div class="modal-body">					
					
					<div class="form-group">
						<label class="col-sm-2 control-label" ><?php echo @$lsd['ltno']==""?'LOT NO':@$lsd['ltno']; ?> <span class="text-danger">*</span>:</label>
						<div class="col-sm-4">
							<input class="form-control" readonly type="text" name="ynk_lotno" id="ynk_lotno" size="20" maxlength='5' value='<?php echo $lot_details[sizeof($lot_details)-1]['ynk_lotno']+1; ?>'>
						</div>
						<label class="col-sm-2 control-label" ><?php echo @$lsd['vltno']==""?'VENDOR LOT NO':$lsd['vltno']; ?> <span class="text-danger">*</span>:</label>
						<div class="col-sm-4">
							<input class="form-control" type="text" name="vendorlotno" id="vendorlotno" maxlength="20" value="" />
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-2 control-label" ><?php echo @$lsd['product']==""?'PRODUCT DESCRIPTION':$lsd['product']; ?> <span class="text-danger">*</span>:</label>
						<div class="col-sm-4">
							<textarea class="form-control" rows="4" name="product" id="product" cols="50"></textarea>
						</div>
						
						<label class="col-sm-2 control-label" >BID INCREMENT TIME <span class="text-danger">*</span>:</label>
						<div class="col-md-1">
							<input class="form-control" placeholder='Hours' type="text" name="ihh" id="ihh" size="2" value='' maxlength="2">
						</div>
						<div class="col-md-1">
							<input class="form-control" placeholder='Minutes' type="text" name="imm" id="imm" size="2"  value='' maxlength="2">
						</div>
						<div class="col-md-1">
							<input class="form-control" placeholder='Seconds' type="text" name="iss" id="iss" size="2"  value='' maxlength="2">
						</div>
						<label class="col-sm-6 control-label" >Incerment of lot close time @ last munite (Hour:Min:Sec format Ex.:<font color="#FF0000">00:05:00)</font></label>
					
					</div>
					<div class="form-group">
						<label class="col-sm-2 control-label" ><?php echo @$lsd['plant']==""?'PLANT':$lsd['plant']; ?> <span class="text-danger">*</span>:</label>
						<div class="col-sm-4">
							<input class="form-control" type="text" name="plant" id="plant" maxlength="200" value=""/>
						</div>
						<label class="col-sm-2 control-label" ><?php echo @$lsd['tb']==""?'TIME BOUNDED':$lsd['tb']; ?> ? <span class="text-danger">*</span>:</label>
						<div class="col-sm-4">
							<select class="form-control" name="time_bounded" id="time_bounded">
								<option value='N'>No</option>
								<option value='Y'>Yes</option>
							</select>
						</div>
					</div>
					
					<div class="form-group">
						<label class="col-sm-2 control-label" ><?php echo @$lsd['lsd']==""?'LOT START DATE':$lsd['lsd']; ?> <span class="text-danger">*</span>:</label>
						<div class="col-sm-4">
							<input class="form-control" id="lot_open_date" value="" readonly="readonly" name="lot_open_date">
						</div>
						<label class="col-sm-2 control-label" ><?php echo @$lsd['led']==""?'LOT END DATE':$lsd['led']; ?> <span class="text-danger">*</span>:</label>
						<div class="col-sm-4">
							<input class="form-control" id="lot_close_date" value="" readonly="readonly"  name="lot_close_date">
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-2 control-label" >BID START TIME <span class="text-danger">*</span>:</label>
							<div class="col-md-3">
									<input class="form-control" name="stime" type="text" id="stime" type="time" value='<?php echo ! empty($stime) ? $stime : ""; ?>' />
							</div>
						<div class="col-sm-1">&nbsp;</div>
						
						<label class="col-sm-2 control-label" >BID END TIME  <span class="text-danger">*</span>:</label>
							<div class="col-md-3">
								<input class="form-control" name="etime" type="text" id="etime" type="time"  value='<?php echo ! empty($etime) ? $etime : ""; ?>' />
							</div>
						<div class="col-sm-1">&nbsp;</div>
					</div>
					
					<div class="form-group">
						<label class="col-sm-2 control-label" >&nbsp;</label>
						<div class="col-sm-4">
							<div class="text-danger">(Both 24 hrs time format ex: 23:59:59)</div>
						</div>
						<label class="col-sm-2 control-label" >CMD AMOUNT(<?php echo $auction_details['auction_currency'] ?>)<span class="text-danger">*</span>:</label>
						<div class="col-sm-4">
							<input class="form-control" type="text" name="cmd" id="cmd" size="20" value=''>
						</div>
					</div>											
					
					<div class="form-group">
						<label class="col-sm-2 control-label" ><?php echo @$lsd['tq']==""?'TOTAL QUANTITY ':$lsd['tq']; ?>  <span class="text-danger">*</span>:</label>
						<div class="col-sm-4">
							<input class="form-control" type="text" name="totalqty" id="totalqty" size="20" value='1'>
						</div>
						<label class="col-sm-2 control-label" >TOTAL UNIT :<span class="text-danger">*</span>:</label>
						<div class="col-sm-4">
							<input class="form-control" type="text" name="totunit" id="totunit" size="10" value='' maxlength="50" >
						</div>
						
					</div>
					<div class="form-group">
						<label class="col-sm-2 control-label" >BID QUANTITY<span class="text-danger">*</span>:</label>
						<div class="col-sm-4">
							<input class="form-control" type="text" name="bidqty" id="bidqty" size="20" value=''>
						</div>
						<label class="col-sm-2 control-label" >BID UNIT<span class="text-danger">*</span>:</label>
						<div class="col-sm-4">
							<input class="form-control" type="text" name="bidunit" id="bidunit" size="20" value=''>
						</div>
					</div>
					<div class="form-group part_bid">
						<label class="col-sm-2 control-label" >PART BID  <span class="text-danger">*</span>:</label>
						<div class="col-sm-4">
							<select class="form-control"  id="part_bid" name="part_bid" onchange="check_part_bid(this.value)">
								<option value="N">NO</option>
								<option value="Y">YES</option>
								
							</select>
						</div>
						<label class="col-sm-2 control-label min_bid" >MIN. BID QUANTITY :<span class="text-danger">*</span>:</label>
						<div class="col-sm-4 min_bid">
							<input class="form-control" type="text" name="min_part_bid" id="min_part_bid" size="20" value=''>
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-2 control-label" ><?php echo @$lsd['sb']==""?'START PRICE':$lsd['sb']; ?>(Rs.) <span class="text-danger">*</span>:</label>
						<div class="col-sm-4">
							<input class="form-control" type="text" name="startbid" id="startbid" size="20" value=''>
						</div>
						<label class="col-sm-2 control-label" ><?php echo @$lsd['db']==""?'INCREMENT / DECREMENT BY':$lsd['db']; ?> (<?php echo $auction_details['auction_currency'] ?>) <span class="text-danger">*</span>:</label>
						<div class="col-sm-4">
							<input class="form-control" type="text" name="incrby" id="incrby" size="10" value='' >
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-2 control-label" >ACCEPT PROXY BID :</label>
						<div class="col-sm-4">
							<input type="radio" onchange="proxy_date_enable()" value="Y" name="proxy" id='proxy_Y'>YES
							<input type="radio" onchange="proxy_date_enable()" value="N" name="proxy" id='proxy_N'>NO
						</div>
						<label class="col-sm-2 control-label" >PROXY BID START DATE :</label>
						<div class="col-sm-4">
							<input class="form-control datepickerclass" type="text" name="proxystartdate" id="proxystartdate" readonly value=''>
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-2 control-label" >LOT WITHDRAWN :</label>
						<div class="col-sm-2">
							<select class="form-control"  id="lot_withdradn" name="lot_withdradn">
								
								<option value="N">NO</option>
								<option value="Y">YES</option>
								
							</select>
						</div>
						<label class="col-sm-2 control-label" >REMARKS :</label>
						<div class="col-sm-6">
							<input class="form-control" type="text" name="remarks" id="remarks" value='' maxlength='500'>
						</div>
					</div>
					
				</div>
				<div class="modal-footer">
					<input class="btn btn-primary" type="submit" value='Submit' name="btn_lot_add_edit_submit" id='btn_lot_add_edit_submit' />
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				</div>
			</form>
		</div>
	</div>
</div>

<!-- Upload Excel -->
<div id="lotSettings" class="modal fade" >
	<div class="modal-dialog">
		<div class="modal-content">
			<form name="frmLotSettings" id="frmLotSettings" class='form-horizontal' method="post" >
				<input type='hidden' id='setting_auctionid' name='setting_auctionid' value='<?php echo $auctionid; ?>'/>
				
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title" id="modal_header_text">LOT SETTINGS</h4>
				</div>
				<div class="modal-body">		
					<p style='color:red'>leave blank for default settings</p>			
					<table class="table table-bordered">
								<tr>
								<th>DEFALUT HEADING</th>
								<th>CUSTOMIZED HEADING</th>
								<th>HIDE LIVE BID</th>
								</tr>
								<tr>
									<td>LT.NO.</td>
									<td><input type="text" class="form-control" name='ltno' value="<?=@$Setting_array['ltno'];?>" /></td>
									<td><input class="form-control" type="checkbox" id="h_lotno" name="h_lotno" value="Y" ></td>

								</tr>
								<tr>
									<td>VEN.L.NO.</td>
									<td><input type="text" class="form-control" name='vltno' placeholder='PARTNO/MFG NO' value="" /></td>
									<td><input class="form-control" type="checkbox" id="h_vltno" name="h_vltno" value="Y" ></td>
								</tr><tr>
									<td>PLANT</td>
									<td><input type="text" class="form-control" name='plant' placeholder='PLANT' value="" /></td>
									<td><input class="form-control" type="checkbox" id="h_plant" name="h_plant" value="Y" /></td>
								</tr>
								<tr>
									<td>PRODUCT</td>
									<td><input type="text" class="form-control" name='product' value="" /></td>
									<td><input class="form-control" type="checkbox" id="h_product" name="h_product" value="Y" /></td>
								</tr>
								<tr>
									<td>PRODUCT IMAGE</td>
									<td><input type="text" class="form-control" name='product_image' value="" /></td>
									<td><input class="form-control" type="checkbox" id="h_product_image" name="h_product_image" value="Y" /></td>
								</tr>
								<tr>
									<td>LOT START DATE</td>
									<td><input type="text" class="form-control" name='lsd' value="" /></td>
									<td><input class="form-control" type="checkbox" id="h_lsd" name="h_lsd" value="Y"></td>
								</tr>
								<tr>
									<td>LOT END DATE</td>
									<td><input type="text" class="form-control" name='led' value="" /></td>
									<td><input class="form-control" type="checkbox" id="h_led" name="h_led" value="Y"></td>
								</tr>
								<tr>
									<td>TIME BOUND</td>
									<td><input type="text" class="form-control" name='tb' value="" /></td>
									<td><input class="form-control" type="checkbox" id="h_tb" name="h_tb" value="Y"></td>
								</tr>
								<tr>
									<td>TOTAL QTY.</td>
									<td><input type="text" class="form-control" name='tq' value="" /></td>
									<td><input class="form-control" type="checkbox" id="h_tq" name="h_tq" value="Y"></td>
								</tr>
								<tr>
									<td>BID QTY</td>
									<td><input type="text" class="form-control" name='bq' value="" /></td>
									<td><input class="form-control" type="checkbox" id="h_bq" name="h_bq" value="Y"></td>
								</tr>
								<tr>
									<td>STARTING BID</td>
									<td><input type="text" class="form-control" name='sb' value="" /></td>
									<td><input class="form-control" type="checkbox" id="h_sb" name="h_sb" value="Y"></td>
								</tr>
								<tr>
									<td>DECREMENT BY</td>
									<td><input type="text" class="form-control" name='db' value="" /></td>
									<td><input class="form-control" type="checkbox" id="h_db" name="h_db" value="Y"></td>
								</tr>
								<tr>
									<td>PART BID ?</td>
									<td><input type="text" class="form-control" name='pb' value="" /></td>
									<td><input class="form-control" type="checkbox" id="h_pb" name="h_pb" value="Y"></td>
								</tr>
								<tr>
									<td>RATE/AMOUNT ?</td>
									<td><input type="text" class="form-control" name='amt' value="" /></td>
									<td></td>
								</tr>
								<tr>
									<td>WITHDRAWN ?</td>
									<td><input type="text" class="form-control" name='withdrawd' value="" /></td>
									<td></td>
								</tr>
							</table>
				</div>
				<div class="modal-footer">
					<input class="btn btn-primary" type="submit" value='Submit' name="btnSavelotSettings" id='btnSavelotSettings' />
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				</div>
			</form>
		</div>
	</div>
</div>
<!-- Upload Excel -->
<div id="lot_upload_excel_modal" class="modal fade" >
	<div class="modal-dialog">
		<div class="modal-content">
			<form name="lot_upload_excel_form" id="lot_upload_excel_form" class='form-horizontal' method="post" enctype="multipart/form-data" >
				<input type='hidden' id='auctionid' name='auctionid' value='<?php echo $auctionid; ?>'/>
				
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title" id="modal_header_text">Upload Lot Excel</h4>
				</div>
				<div class="modal-body">					
					<div class="form-group">
						<label class="col-sm-5 control-label" >YANKEE AUCTION SALE NO :</label>
						<div class="col-sm-6">
							<p class="text-muted" style="margin: 5px;"><?php echo $auction_details['ynk_saleno'];?></p>	
						</div>
					</div>
					<?php
					$this->load->library('Amazon_library/S3upload');
					$s3 = new S3upload();
					$lot_excel = $s3->getUrl('y_'.$auctionid.".xls", "lot_excel");
					if ( ! empty($lot_excel))
					{
						if (is_file_exist($lot_excel))
						{
                    ?>
							<div class="form-group">
								<label class="col-sm-5 control-label" >Uploaded File :</label>
								<div class="col-sm-6">
									<p class="text-muted" style="margin: 5px;">
										<a target="_blank" class='link' href="<?php echo base_url('auctioneer/auction/verify_excel/'.$auctionid); ?>">Verify Excel</a>
									</p>	
								</div>
							</div>
							<?php
						}
						else
						{
							?>
							<div class="form-group">
								<label class="col-sm-5 control-label" >Sample File :</label>
								<div class="col-sm-6">
									<p class="text-muted" style="margin: 5px;">
										<a target="_blank" class='link btn btn-success btn-labeled fa fa-download' href="<?php echo base_url('auctioneer_assets/lot_excel/auction.xls'); ?>">Excel Format</a>
									</p>
								</div>
							</div>
							<?php
						}
					}
					else
					{
						?>
						<div class="form-group">
							<label class="col-sm-5 control-label" >Sample File :</label>
							<div class="col-sm-6">
								<p class="text-muted" style="margin: 5px;">
									<a target="_blank" class='link btn btn-success btn-labeled fa fa-download' href="<?php echo base_url('auctioneer_assets/lot_excel/auction.xls'); ?>">Excel Format</a>
								</p>
							</div>
						</div>
						<?php
					}
					?>
					<div class="form-group">
						<label class="col-sm-5 control-label" >Upload Excel<span class="text-danger">*</span>:</label>
						<div class="col-sm-6">
							<input class="form-control" type="file" name="upload_file" id="upload_file"  value='' />
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<input class="btn btn-primary" type="submit" value='Submit' name="btn_lot_upload_excel_submit" id='btn_lot_upload_excel_submit' />
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				</div>
			</form>
		</div>
	</div>
</div>

<!--===================================================-->
<!--END CONTENT CONTAINER-->
<script>

var startDatee;
var endDatee;
$(document).ready(function() {

	$('#stime').mdtimepicker({
		format: 'hh:mm:ss',
		theme: 'blue'
	}); 
	$('#etime').mdtimepicker({
		format: 'hh:mm:ss',
		theme: 'blue'
	});
	
	

	startDatee = '<?php echo date($auction_details['start_date']); ?>';
	endDatee = '<?php echo date($auction_details['end_date']); ?>';
    $("#lot_open_date").datepicker({
        format: 'yyyy-mm-dd',
		startDate: startDatee,
		endDate: endDatee
    });
    $("#lot_open_date").change(function(){
        startDatee = '"'+$("#lot_open_date").val()+'"';

		$("#lot_close_date").val('');
		$("#lot_close_date").datepicker('destroy');
        $("#lot_close_date").datepicker({
            format: 'yyyy-mm-dd',
            startDate: startDatee,
			endDate: endDatee
        });
    });
});
function proxy_date_enable() {


if ($('input[name=proxy]:checked').val() == "Y") {
	$('#proxystartdate').prop( "disabled", false );

} else if ($('input[name=proxy]:checked').val() == "N") {
	$('#proxystartdate').prop( "disabled", true );
}
};
/*
* Function to perform an action for LOTs 
 */
function lot_actions(act){
	if (act == 'ADD')
	{
		$.ajax({
			url: "<?php echo base_url('auctioneer/auction/check_auction_permission'); ?>",
			type: "POST",
			dataType:"json",
			cache: false,
			clearForm: false,
			data:{
				"id": "<?php echo $auctionid; ?>",
				"permission": "Lot Add"
			},
			success:function(response){
				if (response.status == "success"){
					$('#lot_add_edit_modal').find('input:text,textarea').val('');
					$('#lot_action').val('Add');
					$.ajax({
						url : '<?php echo base_url('auctioneer/auction/available_lot_no/').$auctionid; ?>',
						cache : false,
						dataType : 'json',
						success : function(data) {
							$('#ynk_lotno').prop('readonly', true);
							$('#ynk_lotno').val(data.message);
						},
						error : function(data) {
							Display_msg(data.message);
						}
					});
					$('#lot_add_edit_modal').modal('show');
				} else {
					Display_msg(response.message,response.status);
				}
			}
		});
	}
	else if(act == 'LOT_TIME_UPDATE')
	{
		$.ajax({
			url: "<?php echo base_url('auctioneer/auction/check_auction_permission'); ?>",
			type: "POST",
			dataType:"json",
			cache: false,
			clearForm: false,
			data:{
				"id": "<?php echo $auctionid; ?>",
				"permission": "Lot time increase"
			},
			success:function(response){
				if (response.status == "success"){
					window.location.href = '<?php echo base_url("auctioneer/auction/update_lot_timings/".$auctionid); ?>'
				} else {
					Display_msg(response.message,response.status);
				}
			}
		});
	}
	else if (act == 'LOT_TAX_UPDATE')
	{
		$.ajax({
			url: "<?php echo base_url('auctioneer/auction/check_auction_permission'); ?>",
			type: "POST",
			dataType:"json",
			cache: false,
			clearForm: false,
			data:{
				"id": "<?php echo $auctionid; ?>",
				"permission": "Lot tax Edit"
			},
			success:function(response){
				if (response.status == "success"){
					window.location.href = '<?php echo base_url("auctioneer/auction/update_tax_details/".$auctionid."/");?>';
				} else {
					Display_msg(response.message,response.status);
				}
			}
		});
	}
	else if (act == 'EDIT')
	{

		var id = $("input[name=select_lotno]:checked").val();
		
		if( ! id)
		{
			Display_msg('Please Select Lot.','error');
			return false;
		}
		else
		{
			$.ajax({
				url: "<?php echo base_url('auctioneer/auction/check_auction_permission'); ?>",
				type: "POST",
				dataType:"json",
				cache: false,
				clearForm: false,
				data:{
					"id": "<?php echo $auctionid; ?>",
					"permission": "Lot Edit"
				},
				success:function(response){
					if (response.status == "success"){
						$('#lot_add_edit_form').trigger("reset");
						$('#lot_action').val('Edit');
						$.ajax({
							url:"<?php echo base_url('auctioneer/auction/get_single_lot_details');?>",
							type: 'post',
							dataType:'json',
							data:{'lotno':id,'auctionid':$('#auctionid').val()},
							cache: false,
							clearForm: true,
							success: function (response){
								if(response.status=="success"){
									$.each(response.data, function(key,value){
										if(key == 'proxy'){
											$('#'+key+'_'+value).prop("checked", true);
										} else {
											$('#'+key).val(value);
										}
										if (response.data.disable_input_option)
										{
											$("#part_bid").attr('disabled',false);
										}
									});
									$('#lot_add_edit_modal').modal('show');
								} else {
									Display_msg(response.message,response.status);
									return false;
								}
							}
						});
					} else {
						Display_msg(response.message,response.status);
					}
				}
			});
		}
	}
	else if (act == 'DELETE')
	{
		var id = $("input[name=select_lotno]:checked").val()
		if( ! id)
		{
			Display_msg('Please Select Lot.','error');
			return false;
		}
		else
		{
			$.ajax({
				url: "<?php echo base_url('auctioneer/auction/check_auction_permission'); ?>",
				type: "POST",
				dataType:"json",
				cache: false,
				clearForm: false,
				data:{
					"id": "<?php echo $auctionid; ?>",
					"permission": "Lot delete"
				},
				success:function(response){
					if (response.status == "success"){
						var ans = confirm('Are you sure you want to delete this lot?');
						if (ans){
							$.ajax({
								url:"<?php echo base_url('auctioneer/auction/delete_lot');?>",
								type: 'post',
								dataType:'json',
								data:{'lotno':id,'auctionid':$('#auctionid').val()},
								cache: false,
								clearForm: true,
								success: function (response){
									if (response.status=="success"){
										Display_msg(response.message,response.status);	
										setTimeout(function(){
											window.location.reload();
										}, 500);
									} else {	
										Display_msg(response.message,response.status);
										return false;
									}
								}
							});
						}
					} else {
						Display_msg(response.message,response.status);
					}
				}
			});
		}
	}
	else if (act == 'UPLOAD_EXCEL')
	{
		$.ajax({
			url: "<?php echo base_url('auctioneer/auction/check_auction_permission'); ?>",
			type: "POST",
			dataType:"json",
			cache: false,
			clearForm: false,
			data:{
				"id": "<?php echo $auctionid; ?>",
				"permission": "Lot Add"
			},
			success:function(response){
				if (response.status == "success"){
					$('#lot_upload_excel_modal').modal('show');
				} else {
					Display_msg(response.message,response.status);
				}
			}
		});
	}
	else if (act == 'UPLOAD_LOT_IMAGES')
	{
		var id = $("input[name=select_lotno]:checked").val()
		if ( ! id) {
			Display_msg('Please Select Lot.','error');
			return false;
		} else {
			$.ajax({
				url: "<?php echo base_url('auctioneer/auction/check_auction_permission'); ?>",
				type: "POST",
				dataType:"json",
				cache: false,
				clearForm: false,
				data:{
					"id": "<?php echo $auctionid; ?>",
					"permission": "Upload lot image"
				},
				success:function(response){
					if (response.status == "success"){
						window.location.href='<?php echo base_url("auctioneer/auction/upload_lot_image/".$auctionid."/"); ?>'+id;
					} else {
						Display_msg(response.message,response.status);
					}
				}
			});
		}
	}
	else if (act == 'VIEW_BID_HISTORY')
	{
		var id = $("input[name=select_lotno]:checked").val()
		if(id == '' || id == undefined)
		{
			Display_msg('Please Select Lot.','error');
			return false;
		}
		else
		{
			$.ajax({
				url: "<?php echo base_url('auctioneer/auction/check_auction_permission'); ?>",
				type: "POST",
				dataType:"json",
				cache: false,
				clearForm: false,
				data:{
					"id": "<?php echo $auctionid; ?>",
					"permission": "Bid History View"
				},
				success:function(response){
					if (response.status == "success"){
						window.location.href='<?php echo base_url("auctioneer/auction/view_bid_history/".$auctionid."/"); ?>'+id;
					} else {
						Display_msg(response.message,response.status);
					}
				}
			});
		}
	}
	else if (act == 'DELETE_CLIENT_BID')
	{
		var id = $("input[name=select_lotno]:checked").val()
		if(id == '' || id == undefined)
		{
			Display_msg('Please Select Lot.','error');
			return false;
		}
		else
		{
			$.ajax({
				url: "<?php echo base_url('auctioneer/auction/check_auction_permission'); ?>",
				type: "POST",
				dataType:"json",
				cache: false,
				clearForm: false,
				data:{
					"id": "<?php echo $auctionid; ?>",
					"permission": "Client Bid Delete"
				},
				success:function(response){
					if (response.status == "success"){
						window.location.href='<?php echo base_url("auctioneer/auction/delete_client_bid/".$auctionid."/"); ?>'+id;
					} else {
						Display_msg(response.message,response.status);
					}
				}
			});
		}
	}
	else
	{
		Display_msg('Please Select Lot.','error');
		return false;
	}
}

$(function() {
	$.validator.addMethod('compare_time', function (value, element, param) {
		return this.optional(element) || check_start_end_time_lot();
	}, "<p class='text-danger'>Enter Correct start time and end time</p>");
	function check_start_end_time_lot(){
		var lot_startTime = '"'+document.getElementById("stime").value+'"';
		var lot_endTime = '"'+document.getElementById("etime").value+'"';
		var lot_startDate = document.getElementById("lot_open_date").value;
		var lot_endDate = document.getElementById("lot_close_date").value;
		if (lot_startDate == lot_endDate) {
			if (lot_startTime >= lot_endTime)
			{
				return false;
			}
			else
			{
				return true;
			}
		} else {
			return true;
		}
	}
	
	var vRules = {
		lotno:{required:true},
		vendorlotno:{required:true},
		product:{required:true},
		product_details:{required:true},
		plant:{required:true},
		lot_open_date:{required:true},
		lot_close_date:{required:true},
		stime: { required:true },
		etime: {
			required: true,
			compare_time: "#stime" 
		},
		cmd:{required:true},		
		ihh:{required:true},
		imm:{required:true},
		iss:{required:true},
		totalqty:{required:true},
		startbid:{required:true},
		//reservedPrice : {required : true},
		bidqty:{required:true},
		incrby:{required:true}
	};

	var vMessages = {
		lotno:{required:"<p class='text-danger'>Please Enter The Lot No</p>"},
		vendorlotno:{required:"<p class='text-danger'>Please Enter The Vendor Lot No</p>"},
		product:{required:"<p class='text-danger'>Please Enter The Product</p>"},
		product_details:{required:"<p class='text-danger'>Please Enter The Product Details</p>"},
		plant:{required:"<p class='text-danger'>Please Enter The Plant</p>"},
		lot_open_date:{required:"<p class='text-danger'>Please Enter The Lot Start Date</p>"},
		lot_close_date:{required:"<p class='text-danger'>Please Enter The LOT End Date</p>"},
		bshh:{required:"<p class='text-danger'>Please Enter The Hour</p>"},
		bsmm:{required:"<p class='text-danger'>Please Enter The Minute</p>"},
		bsss:{required:"<p class='text-danger'>Please Enter The Second</p>"},
		behh:{required:"<p class='text-danger'>Please Enter The Hour</p>"},
		bemm:{required:"<p class='text-danger'>Please Enter The Minute</p>"},
		bess:{required:"<p class='text-danger'>Please Enter The Second</p>"},
		cmd:{required:"<p class='text-danger'>Please Enter The Cmd Amount</p>"},		
		ihh:{required:"<p class='text-danger'>Please Enter The Hour</p>"},
		imm:{required:"<p class='text-danger'>Please Enter The Minute</p>"},
		iss:{required:"<p class='text-danger'>Please Enter The Second</p>"},
		totalqty:{required:"<p class='text-danger'>Please Enter The Total Quantity</p>"},
		startbid:{required:"<p class='text-danger'>Please Enter The Starting Bid (Rs.) </p>"},
		bidqty:{required:"<p class='text-danger'>Please Enter The BID QUANTITY </p>"},
		incrby:{required:"<p class='text-danger'>Please Enter The  value</p>"}
		
	};
	$("#lot_add_edit_form").validate({
		rules: vRules,
		messages: vMessages,
		submitHandler: function(form){
			$(form).ajaxSubmit({
				url:"<?php echo base_url('auctioneer/auction/addedit_lots/');?>",
				type: 'post',
				dataType:'json',
				cache: false,
				clearForm: true,
				success: function (response){
					if (response.status=="success"){
						Display_msg(response.message,response.status);						
						$('#lot_add_edit_modal').modal('hide');
						setTimeout(function(){
							window.location.reload();
						}, 3000);
					} else {	
						Display_msg(response.message,response.status);
						return false;
					}
				}
			});
		}
	});
	
	
	/* Upload Lot Excel */
	var vRules2 = {
		upload_excel:{required:true}
	};
	var vMessages2 = {
		upload_excel:{required:"<p class='text-danger'>Please Select Excel File</p>"}
	};
	$("#lot_upload_excel_form").validate({
		rules: vRules2,
		messages: vMessages2,
		submitHandler: function(form){
			$(form).ajaxSubmit(	{
				url:"<?php echo base_url('auctioneer/auction/upload_excel/');?>", 
				type: 'post',
				dataType:'json',
				cache: false,
				clearForm: true,
				success: function (response){
					if(response.status=="success"){
						Display_msg(response.message,response.status);
						window.location.href='<?php echo  base_url('auctioneer/auction/verify_excel/'.$auctionid)?>';
					} else {	
						Display_msg(response.message,response.status);
						return false;
					}
				}
			});
		}
	})
});
$(document).on('click', "#btn_lotSettings", function(e) {	 
	   var $btn = $(this).button('loading');
	   $('#lotSettings').modal('show');
      	$.ajax({
      		url:"<?php echo base_url('auctioneer/auction/get_lot_settings');?>",
 				data:  {auction_id:$("#setting_auctionid").val()},
 				type: 'post',
				dataType:'json',
 				success: function(response)
 			    {
 	 			   
 					$('input[name="ltno"]').val(response.ltno);
 					 				
 					$('input[name="h_lotno"]').prop('checked', (response.h_lotno=="Y")?true:false);
 					
 					$('input[name="vltno"]').val(response.vltno);
 					$('input[name="h_vltno"]').prop('checked', (response.h_vltno=="Y")?true:false);
 					$('input[name="plant"]').val(response.plant);
 					$('input[name="h_plant"]').prop('checked', (response.h_plant=="Y")?true:false);
 					$('input[name="product"]').val(response.product);
 					$('input[name="h_product"]').prop('checked', (response.h_product=="Y")?true:false);
 					$('input[name="product_image"]').val(response.product_image);
 					$('input[name="h_product_image"]').prop('checked', (response.h_product_image=="Y")?true:false);
 					$('input[name="lsd"]').val(response.lsd);
 					$('input[name="h_lsd"]').prop('checked', (response.h_lsd=="Y")?true:false);
 					$('input[name="led"]').val(response.led);
 					$('input[name="h_led"]').prop('checked', (response.h_led=="Y")?true:false);
 					$('input[name="tb"]').val(response.tb);
 					$('input[name="h_tb"]').prop('checked', (response.h_tb=="Y")?true:false);
 					$('input[name="tq"]').val(response.tq);
 					$('input[name="h_tq"]').prop('checked', (response.h_tq=="Y")?true:false);
 					$('input[name="bq"]').val(response.bq);
 					$('input[name="h_bq"]').prop('checked', (response.h_bq=="Y")?true:false);
 					$('input[name="sb"]').val(response.sb);
 					$('input[name="h_sb"]').prop('checked', (response.h_sb=="Y")?true:false);
 					$('input[name="db"]').val(response.db);
 					$('input[name="h_db"]').prop('checked', (response.h_db=="Y")?true:false);
 					$('input[name="pb"]').val(response.pb);
 					$('input[name="h_pb"]').prop('checked', (response.h_pb=="Y")?true:false);
 					$('input[name="withdrawd"]').val(response.withdrawd);
 					$('input[name="amt"]').val(response.amt);
 					
					$btn.button('reset');
 					   
 			    },
 			  	error: function(data) 
 		    	{
 			    	$btn.button('reset')
 		    	} 	        
 		   });
	   
   
   });
$("#frmLotSettings").on('submit',(function(e) {
	
	e.preventDefault();
	
	var $btn = $("#btnSavelotSettings").button('loading');
	$.ajax({
		url: "<?php echo base_url('auctioneer/auction/save_lot_settings');?>",
		type: "POST",
		data:  new FormData(this),
		contentType: false,
		cache: false,
		processData:false,
		dataType:'json',
		success: function(response)
		{
			$("#loading").css('display','none');
			Display_msg(response.message,'success');
			 	$btn.button('reset');
			 	alert(data.message);
			 	
			   
			},
		error: function(data) 
		{
			$("#loading").css('display','none');
			alert(data.message);
			$btn.button('reset');
		} 	
	});
}));	
$(document).on('click', "#btnSavelotSettings", function(e) {	
		  
	e.preventDefault();
	   $( "#frmLotSettings" ).submit();
	   
   
   });
$("#container").addClass("mainnav-sm");
function check_part_bid(value)
{
	if (value == 'N')
	{
		$('.min_bid').hide();

	}
	else
	{
		$('.min_bid').show();
	}
}
</script>