
<!--CONTENT CONTAINER-->
<!--===================================================-->
<div id="content-container">
	<!--Page Title-->
	<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
	<div id="page-head">
	<div id="page-title">
		<h1 class="page-header text-overflow"><?php echo $pagetitle ?></h1>
	</div>
	<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
	<!--End page title-->


	<!--Breadcrumb-->
	<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
	<ol class="breadcrumb">
		<li><a href="<?php echo base_url('auction/index');?>">Home</a></li>
		<li><a href="<?php echo base_url('auctioneer/auction/index');?>">Yankee Auction</a></li>
		<li><a href="<?php echo base_url('auctioneer/auction/view_lots/'.$auctionid);?>">Yankee Auction Lots</a></li>
		<li class="active"><?php echo $pagetitle ?></li>
	</ol>
</div>
	<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
	<!--End breadcrumb-->
	<input type="hidden" id="t11" name="auctionId">
	<div id="page-content">
		<div class="float_btn">
			<a class="btn btn-primary btn-icon btn-circle add-tooltip" data-placement="top" data-toggle="tooltip" data-original-title="Back" href='<?php echo base_url('auctioneer/auction/view_lots/'.$auctionid); ?>' ><i class="fa fa-arrow-left" aria-hidden="true"></i></a>
		</div>	
		<div class="panel">
			<div class="panel-heading">
				<h3 class="panel-title"><?php echo $pagetitle ?></h3>
			</div>
			
			<div class="panel-body">
				<div class="table-responsive">
					<table class="table table-striped table-bordered table-hover no-footer dtr-inline">
						<thead>
							<tr>
								<th>Sale No</th>
								<th>Start Time</th>
								<th>End Time</th>
								<th>Particulars</th>
							</tr>
							<tr>
								<td><?php echo $auction_details['ynk_saleno'] ?></td>
								<td><?php echo date('d-m-Y H:i:s',strtotime($auction_details['start_date']." ".$auction_details['stime'])) ?></td>
								<td><?php echo date('d-m-Y H:i:s',strtotime($auction_details['end_date']." ".$auction_details['etime'])) ?></td>
								<td><?php echo $auction_details['particulars'] ?></td>
							</tr>
						</thead>
					</table>
				</div>
				
				<div class="table-responsive">
					<form id="form_lot_tax_update" name="form_lot_tax_update" method="post">
						<input type="hidden" id=auctionid" name="auctionid" value="<?php echo $auctionid; ?>" />
						<table class="table table-striped table-bordered table-hover no-footer dtr-inline">
							<thead>
								<tr>
									<!--<th width="20"><input name="checkbox_lot_all" type="checkbox" onchange="check_all_checkbox(this);" ></th>
									<th>Lot No</th>
									<th>Vendor Lot No</th>
									<th>VAT</th>
									<th>CST</th>
									<th>TCS</th>
									<th>GST</th>-->
									<th>SR No <input name="checkbox_lot_all" type="checkbox" onchange="check_all_checkbox(this);" ></th>
									<th>Tax Name</th>
									<th>Tax Rate</th>
									<th>Lot</th>
									<th><a title="Add Tax" href="javascript:void(0);" onclick="addTax();" class="btn btn-primary addBtn">+</a></th>


								</tr>
							</thead>
							<tbody id="tax_body">
								<?php
									$i = 1;
								if ( ! empty($view_tax))
								{
								
									foreach($view_tax as $key=>$value)
									{
								?>
										<tr>
											<td><?php echo $i?> <input class="chk_all chkbox" name="checkbox_lot_<?php echo $i?>" type="checkbox" onchange="enable_disable_lots('<?php echo $i?>',this);" ></td>
											<td><input type="text" disabled="disabled" class="form-control lot_<?php echo $i;?>" name="tax_name[<?php echo $i?>]" id="tax_name<?php echo $i?>" value="<?php echo $key?>"/></td>
											<td><input type="text" disabled="disabled" class="form-control lot_<?php echo $i;?>" name="tax_rate[<?php echo $i?>]" id="tax_rate<?php echo $i?>" value="<?php echo $value['tax_rate']?>"/></td>
											<td><input class="lot_<?php echo $i;?>" type="hidden" disabled="disabled" name="tax_id[<?php echo $i?>]" id="tax_id<?php echo $i?>" value="<?php echo $value['lot']?>">
											<input type="checkbox" class="lot_<?php echo $i;?>" disabled="disabled" name="all_lot[<?php echo $i?>]" id="all_lot<?php echo $i?>" value='Y' onchange="check_all_lot(<?php echo $i?>)"/>  All Lot
											<select class="form-control lots lot_<?php echo $i;?>" disabled="disabled" name="lot_no[<?php echo $i?>][]" id="lot_no<?php echo $i?>" multiple="multiple">
											<option value="">--select one--</option>
												<?php if ( ! empty($get_lots))
													{
														
														foreach($get_lots as $singlelot)
														{
															$HiddenProducts = explode(',' ,$value['lot']);
															if (in_array($singlelot['ynk_lotno'], $HiddenProducts))
															{
																$selected = 'selected';
															}
															else
															{
																$selected = '';
															}

												?>
															<option value="<?php echo $singlelot['ynk_lotno']?>" <?php echo $selected?>><?php echo $singlelot['product']?></option>
												<?php
														}
													}
												?>
											</select></td>
											<td><a title="Remove Tax" href="javascript:void(0);" onclick="remove_tax('<?php echo $key?>')" class="btn btn-danger">-</a></td>

										</tr>

								<?php
									$i++;
									}
								}
								
							/*	 if( ! empty($lot_details))
								{
									foreach($lot_details as $single_lot)
									{
										if(empty($view_tax[$single_lot['ynk_lotno']]))
										{
											$view_tax[$single_lot['ynk_lotno']]['VAT'] = '';
											$view_tax[$single_lot['ynk_lotno']]['CST'] = '';
											$view_tax[$single_lot['ynk_lotno']]['TCS'] = '';
											$view_tax[$single_lot['ynk_lotno']]['Other'] = '';
										}
										?>
										<tr>
											<td><input class="chk_all chkbox" name="checkbox_lot_<?php echo $single_lot['ynk_lotno']?>" type="checkbox" onchange="enable_disable_lots('<?php echo $single_lot['ynk_lotno']?>',this);" ></td>
											<td><?php echo $single_lot['ynk_lotno']?></td>
											<td><?php echo $single_lot['vendorlotno']?></td>
											<td>
												<input disabled="disabled" class="form-control lot_<?php echo $single_lot['ynk_lotno']?>" name="VAT[<?php echo $single_lot['ynk_lotno']?>]" required style="width:100px;" placeholder="VAT" value="<?php echo $view_tax[$single_lot['ynk_lotno']]['VAT'];?>" title='Enter VAT'  autocomplete="off"  />
											</td>
											<td>
												<input disabled="disabled" class="form-control lot_<?php echo $single_lot['ynk_lotno']?>" name="CST[<?php echo $single_lot['ynk_lotno']?>]" maxlength="8" required style="width:100px;" placeholder="CST" value="<?php echo $view_tax[$single_lot['ynk_lotno']]['CST'];?>" title='Enter CST' autocomplete="off"  />
											</td>
											<td>
												<input disabled="disabled" class="form-control lot_<?php echo $single_lot['ynk_lotno']?>" name="TCS[<?php echo $single_lot['ynk_lotno']?>]" required style="width:100px;" placeholder="TCS" value="<?php echo $view_tax[$single_lot['ynk_lotno']]['TCS'];?>" title='Enter TCS' autocomplete="off"  />
											</td>
											<td>
												<input disabled="disabled" class="form-control lot_<?php echo $single_lot['ynk_lotno']?>" name="Other[<?php echo $single_lot['ynk_lotno']?>]" maxlength="8" required style="width:100px;" placeholder="GST" value="<?php echo $view_tax[$single_lot['ynk_lotno']]['Other'];?>" title='Enter GST' autocomplete="off"   />
											</td>
										</tr><?php
									}
								} */
								?>

							</tbody>
							<tfoot>
								<?php 
								if ( ! empty($lot_details))
								{ ?>
									<tr>
										<td colspan = '9' class="text-center">
											<input type="submit" class="btn btn-primary" Value="Submit"/>
											<a class="btn btn-warning" href='<?php echo base_url('auctioneer/auction/view_lots/'.$auctionid) ;?>' >Back</a>
										</td>
									</tr>
									<?php
								}
								?>
							</tfoot>
						</table>
					</form>
				</div>
			</div>	
		</div>
	</div>	
</div>
<!--===================================================-->
<!--END CONTENT CONTAINER-->
<script>
/* Check All Lots */
function check_all_checkbox(obj){
	if($(obj).is(":checked"))
	{
		$('.chk_all').each(function()
		{
			$(this).prop('checked',true);
			$(this).change();
		});
	}
	else
	{
		$('.chk_all').each(function()
		{
			$(this).prop('checked',false);
			$(this).change();
		});
	}
	
}

/* Selected Lots Enable Disable */
function enable_disable_lots(lotno,obj)
{
	if($(obj).is(":checked"))
	{
		$(".lot_"+lotno).each(function()
		{
			$(this).removeAttr("disabled");
		});
	}
	else
	{
		$(".lot_"+lotno).each(function()
		{
			$(this).attr("disabled","disabled");
		});
	}
}
$('.lots').select2({
		placeholder: 'Lots',
		width: '100%',
		border: '1px solid #e4e5e7',
    });

$(document).ready(function(){


	

	//get_lot();
	addTax();
	/* Select Multiple Checkbox With Shift Key - Start */
	var $chkboxes = $('.chkbox');
    var lastChecked = null;
	
	$chkboxes.click(function(e) {
		if ( ! lastChecked) {
            lastChecked = this;
            return;
        }

        if (e.shiftKey) {
            var start = $chkboxes.index(this);
            var end = $chkboxes.index(lastChecked);

            $chkboxes.slice(Math.min(start,end), Math.max(start,end)+ 1).prop('checked', lastChecked.checked);
			$chkboxes.change();
        }
		
        lastChecked = this;
    });
	/* Select Multiple Checkbox With Shift Key - End */
	
	/* Form Submit */
	var vRules = {};
	var vMessages = {};
	
	$("#form_lot_tax_update").validate({
		rules: vRules,
		messages: vMessages,
		submitHandler: function(form){
			$(form).ajaxSubmit({
				url:"<?php echo base_url('auctioneer/auction/update_tax_details/'.$auctionid);?>",
				type: 'post',
				dataType:'json',
				cache: false,
				clearForm: false,
				success: function (response){
					if(response.status=="success") {
						Display_msg(response.message,response.status);
						setTimeout(function(){
							window.location.href = "<?php echo base_url('auctioneer/auction/update_tax_details/'.$auctionid) ?>";
						}, 3000);
					} else {	
						Display_msg(response.message,response.status);
						return false;
					}
				}
			});
		}
	});
});

var tax = <?php echo $i?>;

function addTax(){

	var html = 	'<tr>'+
					'<td class="tax_lable">'+tax+'</td><input type="hidden" name="tax_id['+tax+']" id="tax_id'+tax+'" value=""/>"/>'+
					'<td><input type="text"  class="form-control required" name="tax_name['+tax+']" id="tax_name'+tax+'" /></td>'+
					'<td><input type="text"  class="form-control required" name="tax_rate['+tax+']" id="tax_rate'+tax+'" /></td>'+
					'<td><input type="checkbox" name="all_lot['+tax+']" id="all_lot'+tax+'" value="Y" onchange="check_all_lot('+tax+')"/>  All Lot'+
					'<select class="form-control lots1" name="lot_no['+tax+'][]" id="lot_no'+tax+'" multiple="multiple"></select></td>'+
					'<td><a title="Remove Tax" href="javascript:void(0);" onclick="removeTax(this)" class="btn btn-danger">-</a></td>'+
				'</tr>';
	
	$("#tax_body").append(html);
	$('.lots1').select2({
		placeholder: 'Lots',
		width: '100%',
		border: '1px solid #e4e5e7',
    });
	$('.lots').select2({
		placeholder: 'Lots',
		width: '100%',
		border: '1px solid #e4e5e7',
    });
	tax++;
	manageLableSequence('tax_lable');
	get_lot();
	
}
function removeTax(obj)
{
	$(obj).parent().parent().remove();
	manageLableSequence('tax_lable');
}
function manageLableSequence(className){
	var labelCounter = <?php echo $i?>;
	$("."+className).each(function(){
		$(this).html(labelCounter);
		labelCounter++;
	});
}
function remove_tax(tax_name)
{
	if(confirm('Do You Want To Delete Tax Detail for All lot'))
	{
		$.ajax({
			url:'<?php echo base_url("auctioneer/auction/delete_tax")?>',
			data:{'tax_name':tax_name,auction_id:'<?php echo $auctionid?>'},
			dataType:'json',
			type:'POST',
			success:function(response)
			{
				Display_msg(response.message,response.status);
				setTimeout(function(){
							window.location.href = "<?php echo base_url('auctioneer/auction/update_tax_details/'.$auctionid) ?>";
				}, 3000);
				
			},
			error:function()
			{
				Display_msg(response.message,response.status);
				
			}
		});
	}
	else
	{
		return false;
	}

	
}


function get_lot()
{
	var auction_id = '<?php echo $auctionid?>';
	$.ajax({
		url:'<?php echo base_url('auctioneer/auction/get_lots')?>',
		data:{'auction_id':auction_id},
		dataType:'json',
		type:'POST',
		success:function(response)
		{
			var html='';
			$.each(response, function( index, value ) {
				html+='<option value="'+value['ynk_lotno']+'"';
				html+='>'+value['product']+'</option>';

			});
			$('.lots1').html(html);

		},
		error:function()
		{
			Display_msg(response.message,response.status);
		}

	});
}
function check_all_lot(id)
{
	
	if ($("#all_lot"+id).is(":checked"))
	{
		$('#lot_no'+id).next(".select2-container").hide();
	}
	else
	{
		
		$('#lot_no'+id).next(".select2-container").show();

	}
	
}

</script>