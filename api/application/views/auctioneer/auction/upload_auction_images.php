<style>
 
 .float_btn{
    position: fixed;
    top: 15%;
    right: 5%;
    z-index: 10;
}
.float_btn i {
	font-size: 20px;
}
#page-content{
    position: relative;
}
</style>
<!--CONTENT CONTAINER-->
<!--===================================================-->
<div id="content-container">
	<div id="page-head">

		<!--Page Title-->
		<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
		<div id="page-title">
			<h1 class="page-header text-overflow">Auction Images</h1>
		</div>
		<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
		<!--End page title-->


		<!--Breadcrumb-->
		<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
		<ol class="breadcrumb">
			<li><a href="<?php echo base_url('auction/index');?>">Home</a></li>
			<li class="active"><a href="<?php echo base_url('dashboard');?>">Dashboard</a></li>
			<li class="active">Auction Images</li>
		</ol>
		<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
		<!--End breadcrumb-->
	</div>	
	<div id="page-content">
		<div class="panel">
			<div class="panel-heading">
				<h3 class="panel-title">Auction Images</h3>
			</div>
			
			<div class="panel-body">
				<form class="form-horizontal"  method="POST" id="upload_frm">
					<input type='hidden' id="auction_id" name="auctionid" value='<?php echo $auctionid; ?>' />
					<input type='hidden' id="auction_type" name="auctiontype" value='<?php echo $auc_type; ?>' />
                    <div class="form-group">
                        <div class="col-sm-2">
                            <label class="label-control">Select File : </label>
                        </div>
                        <div class="col-sm-3">
                                <input id="imageFile" type="file" multiple="multiple" name="file[]" class="form-control">     
                        </div>
						<div class="col-sm-3">
							<button class="btn btn-success btn-labeled fa fa-check" type="submit" value='Submit' name="btn_upload_file_submit" id='btn_upload_file_submit'>Submit</button>
						 </div>
                    </div>
                </form>
               
                
			</div>	
		</div>



    <!-- Auction images panel-->
    <div class="panel">
			<div class="panel-heading">
				<h3 class="panel-title">Uploaded Images</h3>
			</div>
			
			<div class="panel-body">
            <?php  if ( ! empty($uploaded_img[$auctionid])):
            ?>
                 <div class="col-sm-12">
                <?php
                    $i=1;
                       
                        
                        foreach ($uploaded_img[$auctionid] as $id=>$singleimg):

                ?>
                         <div class="col-sm-3">
                                <div class="col-sm-2">
                                    <input type="checkbox" name="img" value="<?php echo $id?>">
                                </div>
                                <div class="col-sm-6">
                                   <?php if ($auc_type == 'f')
									{?>
                                    <a href="<?php echo AMAZON_BUCKET."/".s3BucketTestDir."images/auctionpic/forward/".$singleimg?>">Image <?php echo $i;?></a>
									<?php
									}
									else if($auc_type == 'r')
									{
									?>
									<a href="<?php echo AMAZON_BUCKET."/".s3BucketTestDir."images/auctionpic/reverse/".$singleimg?>">Image <?php echo $i;?></a>
									<?php
									}
									else if($auc_type =='t')
									{
									?>
									<a href="<?php echo AMAZON_BUCKET."/".s3BucketTestDir."images/auctionpic/tender/".$singleimg?>">Image <?php echo $i;?></a>
									<?php
									}
									else if($auc_type == 'd')
									{
									?>
									<a href="<?php echo AMAZON_BUCKET."/".s3BucketTestDir."images/auctionpic/dutch/".$singleimg?>">Image <?php echo $i;?></a>
									<?php
									}
									else if($auc_type == 'y')
									{
									?>
									<a href="<?php echo AMAZON_BUCKET."/".s3BucketTestDir."images/auctionpic/yankee/".$singleimg?>">Image <?php echo $i;?></a>
									<?php
									}
									else if ($auc_type == 's'){
									?>
									<a href="<?php echo AMAZON_BUCKET."/".s3BucketTestDir."images/auctionpic/salvage/".$singleimg?>">Image <?php echo $i;?></a>
									<?php
									}
									?>
                                </div>
                              
                        </div>
                        

                <?php
                        $i++;
                        endforeach;
                    

                ?>
                </div>
                <br/>
                <br/>
                <br/>
                <br/>
                <div class="col-sm-12">
                    <button class="btn btn-sm btn-danger" onclick="remove_img();">Remove Selected Image</button>
                </div>
            <?php 
             endif;
            ?>
            </div>
    </div>


	</div>	
</div>

<!--===================================================-->
<!--END CONTENT CONTAINER-->
<script type="text/javascript">
var imgwidth = 0;
var imgheight = 0;
function ResizeImage() {
    $("#resizeimg").hide();
    $("#btn_upload_file_submit").show();
     imgwidth = document.getElementById('imgwidth').value;
     imgheight = document.getElementById('imgheight').value;
    var filesToUpload = document.getElementById('imageFile').files
	
	for (var j=0 ;j<filesToUpload.length;j++)	
	{	
		var file = filesToUpload[j];
	
		
		var reader = new FileReader();
		
		reader.onload = function(e) {
            
			 var img = document.createElement("img");
			 var element = document.createElement("input");
             element.type = 'hidden';
            element.name = 'img[]';
           
			 var img = new Image();
 
			img.src = this.result;

            setTimeout(function(){
				var canvas = document.createElement("canvas");
				var MAX_WIDTH = imgwidth;
				var MAX_HEIGHT = imgheight;
				var width = img.width;
				var height = img.height;

				if (width > height) {
					if (width > MAX_WIDTH) {
						height *= MAX_WIDTH / width;
						width = MAX_WIDTH;
					}
				} else {
					if (height > MAX_HEIGHT) {
						width *= MAX_HEIGHT / height;
						height = MAX_HEIGHT;
					}
				}
				canvas.width = width;
				canvas.height = height;
				var ctx = canvas.getContext("2d");
				ctx.drawImage(img, 0, 0, width, height);
				
				var dataurl = canvas.toDataURL("image/jpeg");
				
				img.src = dataurl;
				element.value = dataurl;
				document.getElementById("demo").appendChild(img);
				document.getElementById("demo").appendChild(element);

			},1000);
        }
        // Load files into file reader
		
		reader.readAsDataURL(file);
	}
	
}
function remove_img()
{
    var img = '';
    if( $('input[name="img"]:checked').length > 0)
    {
        $("input:checkbox[name='img']:checked").each(function(){
            img+=$(this).val()+',';
        });
        if (img)
        {
            $.ajax({
                url:"<?php echo base_url('auctioneer/auction/delete_images/');?>",
                type: 'post',
                data:{'img':img},
                dataType:'json',
                success: function (response)
                {
                    if (response.status=="success"){
                        Display_msg(response.message,response.status);
                        window.location.reload();
                    } else {
                        Display_msg(response.message,response.status);
                        return false;
                    }
                }
            });
        }
    }
    else
    {
        alert('kindly select image');
    }
   
}
$(function(){
	var vRules = {
		imageFile:{required:true}
	};
	var vMessages = {
		imageFile:{required:"<p class='text-danger'>Please select file.</p>"}
	};
    $("#upload_frm").validate({
		rules: vRules,
		messages: vMessages,
		submitHandler: function(form){
			$(form).ajaxSubmit({
				url:"<?php echo base_url('auctioneer/auction/upload_auction_images/');?>",
				type: 'post',
				dataType:'json',
				cache: false,
				clearForm: true,
				success: function (response) {               
					if(response.status=="success")
					{
						Display_msg(response.message,response.status);

						setTimeout(function()
						{
							window.location.reload();
						}, 3000); 						
					}
					else
					{	
						Display_msg(response.message,response.status);
						return false;
					}
				}
			});
		}
	});
})

</script>