
<style>
p {
    margin: 0px;
}
.form-group {
    margin-right: 0px!important;
    margin-left: 0px!important;
}
.float_btn{
    position: fixed;
    top: 15%;
    right: 5%;
    z-index: 10;
}
.float_btn i {
	font-size: 20px;
}
#page-content{
    position: relative;
}
.select2-container{
	padding: 0 !important;
}
.no_set{
	min-height: auto;
	border: none;
}

/* If the bid logic is Rank */
.highlight {
	background-color: #e1f5fe;
	padding: 10px 0 5px 0;
	margin-bottom: 10px;
    border-radius: 8px;
}

/* Clearfix */
.clearfix::after {
  content: "";
  clear: both;
  display: table;
}
.panel {margin-bottom: 5px!important}

</style>

<!--CONTENT CONTAINER-->
<!--===================================================-->
<div id="content-container">
	<div id="page-head">

	<!--Page Title-->
	<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
	
	<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
	<!--End page title-->
	<!--Breadcrumb-->
	<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
	<ol class="breadcrumb">
		<li><a href="<?php echo base_url('auction/index');?>">Home</a></li>
		<li><a href="<?php echo base_url('auctioneer/auction/index');?>">All Auctions</a></li>
		<li><a class="active">Add Auction</a></li>
	</ol>
	<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
	<!--End breadcrumb-->
	</div>
	<!--Page content-->
	<!--===================================================-->
	<div id="page-content">
	
		<div class="row">
			<div class="col-sm-12">
				<!--Primary Panel-->
				<!--===================================================-->
				<div class="panel">
					<div class="panel-heading">
						<h3 class="panel-title" style="justify-content: center;;display: flex;">Company Name : <?php echo $company[0]['vendor_name']; ?></h3>
					</div>
					</div>
					<!--Horizontal Form-->
					<!--===================================================-->
					<form class="form-horizontal" method="post" name="form_ynk_addedit" id="form_ynk_addedit" >
						<input type='hidden' id='ynk_auctionid' name='ynk_auctionid' value='<?php echo $auctionid; ?>' />
						<div class="row">
							<div class="panel-body">
								<div class="col-sm-6">
									<div class="panel">
										<div class="panel-heading">
											<div class="panel-control">
												
											</div>
											<h3 class="panel-title">Basic Details</h3>
										</div>
										<div class="panel-body">
											<?php
												$disable = '';
												if ($disable_edit_input)
												{
													$disable = '';
												}
											
											?>
													<div class="form-group">
														<div class="col-sm-5">
															<label class="control-label">E-Auction Sale No</label>
														</div>
														<div class="col-sm-7">
															<input type='text' class='form-control' name='ynk_saleno' id='ynk_saleno' value='<?php echo $saleno; ?>' readonly>
														</div>
													</div>

													<div class="form-group">
														<div class="col-sm-5">
															<label class="control-label">Vendor Name <span class="text-danger"><i class="fa fa-asterisk" aria-hidden="true"></i></span></label>
														</div>
														<div class="col-sm-7">
															<select id="vendor_id" name="vendor_id">
																<option value=""></option>
																<?php foreach ($vendor_list as $k => $v){ ?>
																	<option value="<?php echo $k; ?>"><?php echo $v; ?></option>
																<?php } ?>
															</select>
															<label style='display:none;' id="vendor_id-error" class="error" for="vendor_id"><p class="text-danger">Please Enter The Company </p></label>
														</div>
													</div>

													<div class="form-group">
														<div class="col-sm-5">
															<label class="control-label">Plant Name</label>
														</div>
														<div class="col-sm-7">
															<select id="plant" name="plant[]">
																<option value=""></option>
															</select>
														</div>
													</div>

													<div class=" form-group">
														<div class="col-sm-5">
															<label class="control-label">Location / City<span class="text-danger"><i class="fa fa-asterisk" aria-hidden="true"></i></span></label>
														</div>
														<div class="col-sm-7">
															<select class="form-control select2" id="location" name="location">
															<?php if (empty($auctionid)){ ?>
																<option value="">--Select Location--</option>
															<?php } else { ?>
																<option value="<?php echo $location['id']; ?>"><?php echo $location['name']; ?></option>
															<?php }	?>
															</select>
														</div>
													</div>

													
											<div class="form-group">
												<div class="col-sm-5">
													<label class="control-label">Material Category <span class="text-danger"><i class="fa fa-asterisk" aria-hidden="true"></i></span></label>
												</div>
												<div class="col-sm-7">
													<select class="form-control select2" data-placeholder="Select an option" name="mat_id[]" id="mat_id"  multiple tabindex="-1" aria-hidden="true">
														<?php
															if($mat_category):
																foreach($mat_category as $key=>$mat):
														?>
																<optgroup label="<?php echo $key?>">
																</optgroup>
														<?php
																	if($mat):
																		foreach($mat as $k=>$v):
														?>
																		<option class="level_1" value="<?php echo $k?>"><?php echo $v?></option>
														<?php
																		endforeach;
																	endif;
																endforeach;
															endif;
														?>
													</select>
													<label style='display:none;' id="mat_id-error" class="error" for="mat_id"><p class="text-danger">Please Enter The Material</p></label>
												</div>
											</div>

											<div class=" form-group">
												<div class="col-sm-5">
													<label class="control-label">Particulars<span class="text-danger"><i class="fa fa-asterisk" aria-hidden="true"></i></span></label>
												</div>
												<div class="col-sm-7">
													<textarea class="form-control" type="text" name="particulars" id="particulars"  Placeholder="Particulars"><?php echo $particulars; ?></textarea>
												</div>
											</div>
											

										</div>
									</div>
									<div class="panel">
										<div class="panel-heading">
											<div class="panel-control">
												
											</div>
											<h3 class="panel-title">Auction Date & Time</h3>
										</div>
										<div class="panel-body">
											<div class="form-group">
												<div class="col-sm-3">
													<label class="control-label">
														Start Date<span class="text-danger"><i class="fa fa-asterisk" aria-hidden="true"></i></span>
													</label>

												</div>
												<div class="col-sm-3">
													<input class="form-control datepickerclass" name="start_date" id="start_date" readonly type="text" value='<?php echo $start_date; ?>'/>
												</div>
											
												<div class="col-sm-3">
													<label class="control-label">
														<a type='button' class="add-popover" data-toggle="popover" data-placement="top" data-original-title="Auction Start Time" data-content="24 hrs time format example : 23:59:59">
															<i class="fa fa-info-circle info_pop" aria-hidden="true"></i>
														</a>
														Start Time<span class="text-danger"><i class="fa fa-asterisk" aria-hidden="true"></i></span>
													</label>
												</div>
												<div class="col-sm-3">
    											
    													<input class="form-control time" placeholder="HH" type="text" name="stime" id="stime" value='<?php echo $stime; ?>' maxlength="8">
    												
												
												</div>
											</div>
									    	
											
											<div class="form-group">
												<div class="col-sm-3">
													<label class="control-label">End Date</label><span class="text-danger"><i class="fa fa-asterisk" aria-hidden="true"></i></span></label>
												</div>
												<div class="col-sm-3">
													<input class="form-control datepickerclass" name="end_date" id="end_date" readonly type="text" value='<?php echo $end_date; ?>' />
												</div>
												<div class="col-sm-3">
													<label class="control-label">
															<a type='button' class="add-popover" data-toggle="popover" data-placement="top" data-original-title="Auction Start Time" data-content="24 hrs time format example : 23:59:59">
																<i class="fa fa-info-circle info_pop" aria-hidden="true"></i>
															</a>
														End Time<span class="text-danger"><i class="fa fa-asterisk" aria-hidden="true"></i></span>
													</label>
												</div>
												<div class="col-sm-3">
													<input class="form-control time" placeholder="HH" type="text" name="etime" id="etime" value='<?php echo $etime; ?>' maxlength="8">
    												
												</div>
											</div>


												
											
									
										</div>
									</div>
								</div>
								<div class="col-sm-6">
									
							
									<div class="panel">
										<div class="panel-heading">
											<div class="panel-control">
												
											</div>
											<h3 class="panel-title">Auction Details</h3>
										</div>
										<div class="panel-body">

									
											<div class="form-group">
												<div class="col-sm-7">
													<label class="control-label">Auction Type<span class="text-danger"><i class="fa fa-asterisk" aria-hidden="true"></i></span></label>
												</div>
												<div class="col-sm-5">
													<select class="form-control"  id="auctiontype" name="auctiontype" <?php echo $disable?> onclick="get_auc_type(this.value)">
														<option value="">--Select Auction Type</option>
														<?php if ($this->common_model->check_permission('Forward Auction')){ ?><option value="Forward Auction" <?php echo $auctiontype=="Forward Auction"?' selected ':'';?>>Forward Auction</option><?php } ?>
														<?php if ($this->common_model->check_permission('Reverse Auction')){ ?><option value="Reverse Auction" <?php echo $auctiontype=="Reverse Auction"?' selected ':'';?>>Reverse Auction</option><?php } ?>
														<?php if ($this->common_model->check_permission('Dutch Auction')){ ?><option value="Dutch Auction" <?php echo $auctiontype=="Dutch Auction"?' selected ':'';?>>Dutch Auction</option><?php } ?>
														<?php if ($this->common_model->check_permission('Yankee Auction(Forward)')){ ?><option value="Yankee Auction(Forward)" <?php echo $auctiontype=="Yankee Auction(Forward)"?' selected ':'';?>>Yankee Auction (Forward)</option><?php } ?>
														<?php if ($this->common_model->check_permission('Yankee Auction(Reverse)')){ ?><option value="Yankee Auction(Reverse)" <?php echo $auctiontype=="Yankee Auction(Reverse)"?' selected ':'';?>>Yankee Auction (Reverse)</option><?php } ?>

													</select>
												</div>
											</div>
											<div style="display:none;" class="form-group">
												<div class="col-sm-7">
													<label class="control-label">Auction Paritcipation<span class="text-danger"><i class="fa fa-asterisk" aria-hidden="true"></i></span></label>
												</div>
												<div class="col-sm-5">
													<select class="form-control" name="auction_visibility" id="auction_visibility">
														<option value="1" >Allowed for all</option>
														<option value="2" >Allowed for registered vendors</option>
														<option value="3" >Allowed for participant only</option>
													</select>
													<div class="text-dark" id="all" style="display:none">(<font color="#FF0000">23:59:59</font>)</div>
													<div class="text-dark" id="register" style="display:none">(24 hrs time format ex: <font color="#FF0000">23:59:59</font>)</div>
													<div class="text-dark" id="participant" style="display:none">(24 hrs time format ex: <font color="#FF0000">23:59:59</font>)</div>
												</div>
											</div>
									
											<div class="form-group">
												<div class="col-sm-7">
													<label class="control-label">Referance No:<span class="text-danger"><i class="fa fa-asterisk" aria-hidden="true"></i></span></label>
												</div>
												<div class="col-sm-5">
													<input class="form-control" type="text" name="auction_msg" id="auction_msg" size="52" maxlength="500" value='<?php echo $auction_msg; ?>'>
												</div>
											</div>
										
											<div style='display:none;' class="form-group">
												<div class="col-sm-7">
													<label class="control-label">
														<a type='button' class="add-popover" data-toggle="popover" data-placement="top" data-original-title="Vendor Participation Control" data-content="Yes : A vendor can express an interest by checking the auction details and can be reviewed under Review Auction interested vendors	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; No :  A vendor can directly join the auction after seeing the auction details.">
															<i class="fa fa-info-circle info_pop" aria-hidden="true"></i>
														</a>	
														Review Vendor For Participation<span class="text-danger"><i class="fa fa-asterisk" aria-hidden="true"></i></span>
													</label>
												</div>
												<div class="col-sm-5">
													<select id='review_vendor_for_participation' name="review_vendor_for_participation" class="form-control" onchange="interest_notify(this.value)">
														<option value=''>Select one</option>
														<option value="1" <?php echo ($review_vendor_for_participation =='1')? 'selected':''?>>Yes</option>
														<option value="0" <?php echo ($review_vendor_for_participation =='0')? 'selected':''?>>No</option>
													</select>
												</div>
											</div>

											

											<div class=" form-group">
												<div class="col-sm-7">
													<label class="control-label">
														<a type='button' class="add-popover" data-toggle="popover" data-placement="top" data-original-title="CMD Amount" data-content="Enter Overall CMD Amount">
															<i class="fa fa-info-circle info_pop" aria-hidden="true"></i>
														</a>
														CMD Amount
													</label>
												</div>
												<div class="col-sm-5">
													<input placeholder="CMD Amount" type="text" class="form-control" name="cmd" id="cmd" value="<?php echo $cmd; ?>"/>
												</div>
											</div>
											<div style="display:none;" class=" form-group">
												<div class="col-sm-7">
													<label class="control-label">
															<a type='button' class="add-popover" data-toggle="popover" data-placement="top" data-original-title="Sub Lots" data-content="Get Sub group as a lot. Eg. Lot Desc Computer > Sub lots Monitor, CPU, Mouse, Keyboard">
																<i class="fa fa-info-circle info_pop" aria-hidden="true"></i>
															</a>
														Lot Sub Groups
													</label>
												</div>
												<div class="col-sm-5">
													<select class="form-control" name="is_lot_group" id="is_lot_group" <?php echo $disable?>>
														<option value="No" <?php echo (($is_lot_group == "Yes") ? NULL : " selected"); ?>>No</option>
														<option value="Yes" <?php if($is_lot_group == "Yes") echo " selected"; ?>>Yes</option>
													</select>
												</div>
											</div>
										</div>
										</div>
										
										<div class="panel">
										<div class="panel-heading">
											<div class="panel-control">
												
											</div>
											<h3 class="panel-title">Proxy & Currency</h3>
										</div>
										<div class="panel-body">
											

											<div class="form-group">
												<div class="col-sm-5">
													<label class="control-label">
														<a type='button' class="add-popover" data-toggle="popover" data-placement="top" data-original-title="Primary currency" data-content="Primary currency which you wants to allow in auction">
															<i class="fa fa-info-circle info_pop" aria-hidden="true"></i>
														</a>
														Primary Currency<span class="text-danger"><i class="fa fa-asterisk" aria-hidden="true"></i></span>
													</label>
												</div>
												<div class="col-sm-7">
													<select id='primary_currency' name="primary_currency" class="form-control">
														<option value=''></option>
														<?php foreach ($currency as $value){ if ( ! empty($primary_currency) && $value['id'] == $primary_currency) { $selected = 'selected'; } else { $selected = '';} ?>
															<option value="<?php echo $value['id']; ?>" <?php echo $selected; ?> ><?php echo $value['currency_name']; ?></option>
														<?php } ?>
													</select>
													<label style='display:none;' id="primary_currency-error" class="error" for="primary_currency"><p class="text-danger">Please Select primary currency</p></label>
												</div>
											</div>
										
											<div class="form-group">
												<div class="col-sm-12">
													<label class="control-label">
														<a type='button' class="add-popover" data-toggle="popover" data-placement="top" data-original-title="Multiple currency" data-content="Please check if you wants to allow multiple currency in the auction">
															<i class="fa fa-info-circle info_pop" aria-hidden="true"></i>
														</a>
														Allow multiple currency 
													</label>
													<input type="checkbox" id="multiple_currency" name="multiple_currency" />
												</div>
											</div>
										
											<div id="sec_currency" class="form-group">
												<div class="col-sm-5">
													<label class="control-label">Secondary Currency <span class="text-danger"><i class="fa fa-asterisk" aria-hidden="true"></i></span></label>
												</div>
												<div class="col-sm-7">
													<select id='secondary_currency' name="secondary_currency[]" class="form-control" multiple>
														<option value=''></option>
													</select>
												</div>
											</div>
										</div>
									</div>
									</div>
							
								
								
								<div class="col-sm-12">
									<div class="panel">
										<div class="panel-heading">
											<div class="panel-control">
												
											</div>
											<h3 class="panel-title">Settings</h3>
										</div>
										<div class="panel-body">
										<div class="col-sm-6">
											<div class=" form-group">
												<div class="col-sm-7">
													<label class="control-label">
														<a type='button' class="add-popover" data-toggle="popover" data-placement="top" data-original-title="Show H1 Bidder" data-content="Yes: H1 bid will be visible to buyer	">
															<i class="fa fa-info-circle info_pop" aria-hidden="true"></i>
														</a>
														Show <span class="auc_status">H1</span> Bid
													</label>
												</div>
												<div class="col-sm-5">
													<select class="form-control" size="1" name="show_l1_bid" id="show_l1_bid">
														<?php
															echo"<option value='Y'";if($show_l1_bid=="Y") echo "selected"; echo">Yes</option>";
															echo"<option value='N'";if($show_l1_bid=="N") echo "selected"; echo">No</option>";
														?>
													</select>
												</div>
											</div>
											<div class=" form-group">
												<div class="col-sm-7">
													<label class="control-label">
														<a type='button' class="add-popover" data-toggle="popover" data-placement="top" data-original-title="Show Bidder Name" data-content="Yes:Bidder Name will be visible to buyer	">
															<i class="fa fa-info-circle info_pop" aria-hidden="true"></i>
														</a>
														Show bidder Names
													</label>
												</div>
												<div class="col-sm-5">
													<select class="form-control" size="1" name="show_l1_bidder" id="show_l1_bidder">
														<?php
														echo"<option value='Y'";if($show_l1_bidder=="Y") echo "selected"; echo">Yes</option>";
														echo"<option value='N'";if($show_l1_bidder=="N") echo "selected"; echo">No</option>";
														?>
													</select>
												</div>
												
											</div>
											
											<?php
											  if ( ! empty($increase_lot_time_in))
												{
													list($bihh,$bimm,$biss) = explode(":",$increase_lot_time_in);
												}
												else
												{
													list($bihh,$bimm,$biss) = array('','','');
												} 
												?>
												<div class="form-group">
                    								<label class="col-sm-7">
														<a type='button' class="add-popover" data-toggle="popover" data-placement="top" data-original-title="One by one lot open" data-content="To open one Lot after the other">
															<i class="fa fa-info-circle info_pop" aria-hidden="true"></i>
														</a>
														One By One Lot Opening<span class="text-danger">*</span>:</label>
                    								<div class="col-sm-5">
                    									<select class="form-control" name="one_by_one_lot" id="one_by_one_lot" onchange="toggelNoOfLotsAtaTime(this.value);">
                    										<option value="N" <?php if(@$one_by_one_lot=="N") echo " selected"; ?>>No</option>
                    										<option value="Y" <?php if(@$one_by_one_lot=="Y") echo " selected"; ?>>Yes</option>
                    									</select>
                    								
                    								</div>
                    								
                    								<div id="OneByOne" class="">
                    								<label class="col-sm-7">No of Lot(s) At a time</label>
                        								<div class="col-sm-5">									
                        									<input class="form-control number" placeholder="No Of Lots Open at a Time" type="text" name="no_of_lot_for_one_by_one" id="no_of_lot_for_one_by_one" max-length="3" value='<?php echo @$no_of_lot_for_one_by_one; ?>'>
                        								</div>
                        								<label class="col-sm-7">Duration of Each Lot</label>
                        								<div class="col-sm-5">									
                        									<input class="form-control time" placeholder="No Of Lots Open at a Time" type="text" name="increase_lot_time_in" id="increase_lot_time_in" max-length="3" value='<?php echo @$increase_lot_time_in; ?>'>
                        								</div>
                    								</div>
												</div>

												
												<div class=" form-group">
													<div class="col-sm-7">
														<label class="control-label">
														<a type='button' class="add-popover" data-toggle="popover" data-placement="top" data-original-title="Time Max Extension" data-content="How many time(s) the lot close date and time will be increased if bidded at last munite, if 0 then unlimited">
															<i class="fa fa-info-circle info_pop" aria-hidden="true"></i>
														</a>	
														Limit max extension(0=Unlimited)
														</label>
													</div>
													<div class="col-sm-5">
														<input type="number" class="form-control" name="allowed_max_extension" id="allowed_max_extension" min="0" max="9999" value="<?php echo $allowed_max_extension; ?>">
													</div>
												</div>
											
												
												<div class=" form-group">
													<div class="col-sm-12">
														<label class="control-label">
															<a type='button' class="add-popover" data-toggle="popover" data-placement="top" data-original-title="IP Blocker" data-content="Select only if any company want to stop bidding from same ip from multiple bidders">
																<i class="fa fa-info-circle info_pop" aria-hidden="true"></i>
															</a>
															Stop Bidding from Same IP for Multiple Bidders
														</label>
														<input style='height:20px;' type="checkbox"name="chk_stop_bidding_same_ip" id="chk_stop_bidding_same_ip" value="1" onchange="change_stop_bidding_chk()" />
														<input type="hidden" name="stop_bidding_same_ip" id="stop_bidding_same_ip" value="0" />
													</div>

												</div>
										</div>
										<div class="col-sm-6">
											<div class=" form-group">
												<div class="col-sm-7">
													<label class="control-label">Accept less than <span class="auc_status">H1</span> bid</label>
												</div>
												<div class="col-sm-5">
													<select class="form-control" size="1" name="accept_less_l1_bid" id="accept_less_l1_bid">
														<?php
														echo"<option value='Y'";if($accept_less_l1_bid=="Y") echo "selected"; echo">Yes</option>";
														echo"<option value='N'";if($accept_less_l1_bid=="N") echo "selected"; echo">No</option>";
														?>
													</select>
												</div>
											</div>
											<div class=" form-group">
												<div class="col-sm-7">
													<label class="control-label">On Page Bid</label>
												</div>
												<div class="col-sm-5">
													<select class="form-control" size="1" name="on_page_bid" id="on_page_bid">
														<?php
														echo"<option value='N'";if($on_page_bid=="N") echo "selected"; echo">No</option>";
														echo"<option value='Y'";if($on_page_bid=="Y") echo "selected"; echo">Yes</option>";
														
														?>
													</select>
												</div>
											</div>

											<div class=" form-group">
												<div class="col-sm-7">
													<label class="control-label">Accept Same Bid if Quantity available</label>
												</div>
												<div class="col-sm-5">
													<select class="form-control" size="1" name="accept_same_bid" id="accept_same_bid">
														<?php
															echo"<option value='Y'";if($accept_same_bid=="Y") echo "selected"; echo">Yes</option>";
															echo"<option value='N'";if($accept_same_bid=="N") echo "selected"; echo">No</option>";
														?>
													</select>
												</div>
											</div>

											<div class=" form-group">
												<div class="col-sm-7">
													<label class="control-label">Maintain Decimal Point</label>
												</div>
												<div class="col-sm-5">
													<input class="form-control" type="text" name="decimal_point" value="<?=$decimal_point;?>">
												</div>
											</div>
										

										</div>
									
										</div>
										</div>
									</div>
								</div>
							
						</div>
						
						<div class="form-group">
								<label class="col-sm-2 control-label">&nbsp;</label>
								<div style="text-align: center" class="col-sm-12">
									<input class="btn btn-success" type="submit" value='Save' name="btn_submit" id='btn_submit' />
									<a class="btn btn-danger" href='<?php echo base_url('auctioneer/auction/index'); ?>'>Back</a>
								</div>
							</div>
					</form>
					<!--===================================================-->
					<!--End Horizontal Form-->
				
			</div>

		</div>
	</div>
	<!--===================================================-->
	<!--End page content-->
</div>
<!--===================================================-->
<!--END CONTENT CONTAINER-->
<script>

var currencyList = {};
<?php foreach ($currency as $value){ ?>
currencyList[<?php echo $value['id']; ?>] = '<?php echo $value['currency_name']; ?>';
<?php } ?>

$(document).ready(function() {
	$('#stime, #increase_lot_time_in, #etime').mdtimepicker({
		format: 'hh:mm:ss',
		theme: 'blue'
	}); 
	
	//fetch_city();
	$("#primary_currency").select2({
		placeholder: "Select primary currency"
	})
	$("#secondary_currency").select2({
		placeholder: "Select secodary currency",
	})
	$("#sec_currency").hide();
	rank_setting();
	get_mat();

	<?php if ( ! empty($notify_proxy_user_before) || ! empty($notify_proxy_user_after)){ ?>
		$("#allow_proxy_in_auction").trigger('click');
	<?php } ?>

    $('#vendor_id').select2({
		placeholder: "Select Company",
		width: "100%"
	});
	$('#location').select2({
	ajax: {
		url: '<?php echo base_url('auctioneer/auction/get_city_by_name');?>',
		type:'POST',
		dataType:'json',
		minimumInputLength: 3,
		data: function (params) {
		var query = {
			search: params.term,
			
		}
		return query;
		},
		processResults: function (data) {
			
			if (data)
			{
				return {
           		 results: data.items
       		 };     
			
			}
			else
			{
				return {
           		 results: ''
       		 };  
			}
			
		},

	

	}
});
	$('#plant').select2({
		placeholder: "Select Plant",
		width: "100%"
	});
	<?php if ( ! empty($multiple_currency)){ ?> $("#multiple_currency").trigger('click'); <?php } ?>

	$(".other_company").hide();
	interest_notify(<?php echo $notify_vendor_interest?>);

	$('#mat_id').select2({
		placeholder: "Select Material"
	});
	<?php if ( ! empty($company_id) && $company_id != $vendor_company_id){ ?>
	changeCountryData();
	<?php } ?>
});

$("#show_company_plant").change(function() {
	var id = document.getElementById('show_company_plant');
	if (id.checked === true) {
		$(".other_company").show();
	} else {
		$("#vendor_id").select2('val', '');
		$("#plant").select2('val', '');
		$(".other_company").hide();
	}
})

$("#vendor_id").change(function(){
	$("#plant").select2('val', '');
	var company_id = $("#vendor_id").val();
	$.ajax({
		url: "<?php echo base_url('auctioneer/auction/get_plant_details') ?>",
		type: "POST",
		data:{
			'company_id': company_id
		},
		dataType: "json",
		async: false,
		success: function(data)
		{
			var html = '';
			var i;
			for(i=0; i<data.length; i++)
			{
				html += '<option value="'+data[i].company_plant_id+'">'+data[i].plant_name+'</option>';
			}
			$('#plant').html(html);
		}
	});
});

$(document).ready(function(){
	<?php if ( ! empty($auctionid)){ ?>
		$("#vendor_id").val('<?php echo $company_id ?>').trigger('change');
		$("#plant").val('<?php echo $plant_id ?>').trigger('change');
	<?php } ?>
})

$(function()	{
	$("#get_auction_select").select2({
		placeholder: "Select Backdated Auction",
		allowClear: true
	});
	<?php
	if($stop_bidding_same_ip == "1"){
		?>
		$("#chk_stop_bidding_same_ip").prop('checked', true);
		$("#stop_bidding_same_ip").val("1");
		<?php
	}
	?>

	var vRules = {
		auctiontype:{required:true},
		auc_owner:{required:true},
		auction_msg:{required:true},
		'mat_id[]':{required:true},
		start_date:{required:true},
		end_date:{required:true},
		bshh:{required:true},
		bsmm:{required:true},
		bsss:{required:true},
		behh:{required:true},
		bemm:{required:true},
		bess:{required:true},
		vendor_id:{required:true},
		particulars:{required:true},
		location:{required:true},
		bid_logic:{required:true},
		primary_currency:{required:true},
	};
	var vMessages = {
		auctiontype:{required:"<p class='text-danger'>Please Enter The Auction Type</p>"},
		auc_owner:{required:"<p class='text-danger'>Please Select Auction Owner</p>"},
		auction_msg:{required:"<p class='text-danger'>Please Enter The Referance  Number.</p>"},
		'mat_id[]':{required:"<p class='text-danger'>Please Enter The Material</p>"},
		start_date:{required:"<p class='text-danger'>Please Enter The Auction Start Date</p>"},
		end_date:{required:"<p class='text-danger'>Please Enter Auction Open Up to</p>"},
		bshh:{required:"<p class='text-danger'>Please Enter hour </p>"},
		bsmm:{required:"<p class='text-danger'>Please Enter minute</p>"},
		bsss:{required:"<p class='text-danger'>Please Enter Second</p>"},
		behh:{required:"<p class='text-danger'>Please Enter hour</p>"},
		bemm:{required:"<p class='text-danger'>Please Enter minute</p>"},
		bess:{required:"<p class='text-danger'>Please Enter Second</p>"},
		vendor_id:{required:"<p class='text-danger'>Please Enter The Company </p>"},
		particulars:{required:"<p class='text-danger'>Please Enter The Particulars</p>"},
		location:{required:"<p class='text-danger'>Please Enter Auction Location</p>"},
		bid_logic:{required:"<p class='text-danger'>Please Enter The Logic</p>"},
		primary_currency:{required:"<p class='text-danger'>Please Select primary currency</p>"},
	};


	$("#form_ynk_addedit").validate({
		rules: vRules,
		messages: vMessages,
		submitHandler: function(form){
			$(form).ajaxSubmit({
				url:"<?php echo base_url('auctioneer/auction/update_data');?>",
				type: 'post',
				dataType:'json',
				cache: false,
				clearForm: false,
				success: function (response) {
					if(response.status=="success")
					{
						Display_msg(response.message,response.status);
						setTimeout(function()
						{
							window.location.href="<?php echo base_url('auctioneer/auction/index');?>";
						}, 500);
					}
					else
					{
						Display_msg(response.message,response.status);
						return false;
					}
				}
			});
		}
	});
	$("#one_by_one_lot").change();
});

/* To show and hide the fields based on the bidder logic */
function rank_setting() {
	if ($("#bid_logic").val() == "R") {
		$(".rank_section").show();
	} else {
		$(".rank_section").hide();
	} if ($("#bid_logic").val() == "O") {
		$(".one_by_one").show();
	} else {
		$(".one_by_one").hide();
	}
}
function get_mat()
{
	var mat  = '<?php echo $mat_id?>';
	var mat_arr = mat.split(',');
	$.each(mat_arr,function(index,obj)
	{
		$('select[name^="mat_id"] option[value="'+obj+'"]').attr("selected","selected");
	});
}

/* To show this field if One by One Lot is selected as Yes */
function toggelNoOfLotsAtaTime(val){
	if(val == "Y"){
		$("#OneByOne").show();
		$("#no_of_lot_for_one_by_one").parent().show();
		$("#no_of_lot_for_one_by_one").addClass("required");
		$("#all_lot_sync_flag").val("N");
	}else{
		$("#OneByOne").hide();
		$("#no_of_lot_for_one_by_one").parent().hide();
		$("#no_of_lot_for_one_by_one").val("");
		$("#no_of_lot_for_one_by_one").removeClass("required");
	}
}

/* To toggle One by One lot if All Lot Sync Flag is Changes */
function all_lot_sync_flag_change(val) {
	if(val == "Y"){
		$("#one_by_one_lot").val("N");
	}
	$("#one_by_one_lot").change();
}

function change_stop_bidding_chk() {
	var option = $('#chk_stop_bidding_same_ip').is(':checked');
	if(option){
		$("#stop_bidding_same_ip").val("1");
	}else{
		$("#stop_bidding_same_ip").val("0");
	}
}

function showAllowedMaxExtension() {
	var id = document.getElementById('limit_max_extension');
	if (id.checked === true) {
		$(".allowed_max_extension").show();
	} else {
		$(".allowed_max_extension").hide();
	}
}

function show_proxy_settings() {
	var id = document.getElementById('allow_proxy_in_auction');
	if (id.checked === true) {
		$(".notify_proxy_user_settings").show();
	} else {
		$(".notify_proxy_user_settings").hide();
		$("#notify_proxy_user_before").prop('selectedIndex',0);
		$("#notify_proxy_user_after").prop('selectedIndex',0);

	}
}

function interest_notify(val)
{
	if (val==1)
	{
		$('.notify_interest').show();
	}else if(val==0){
		$('.notify_interest').hide();
	}
}

$('#multiple_currency').click(function(){
	if ($(this).prop("checked") == true){
		var curId = $("#primary_currency").val();
		if (curId !== "")
		{
			var html = "<option></option>";
			jQuery.each( currencyList, function( i, val ) {
				if (i !== curId){
					html = html + '<option value="'+i+'" >'+val+'<option>';
				}
			});
			$("#secondary_currency").html(html);
			$("#sec_currency").show();
		}
		else
		{
			Display_msg('Please select primary curremcy','failed');
			$("#multiple_currency").prop("checked", false);
		}
	}
	else
	{
		$("#secondary_currency").val("").trigger('change');
		$("#sec_currency").hide();
	}
	<?php if ( ! empty($multiple_currency)){ ?> var selectedValue = new Array(); <?php
	foreach ($multiple_currency as $key => $value){ ?>
		selectedValue['<?php echo $key; ?>'] = '<?php echo $value['other_currency_id']; ?>';
	<?php } ?>
	$.each(selectedValue,function(index,obj){
		$('select[name^="secondary_currency"] option[value="'+obj+'"]').attr("selected","selected");
	});
	<?php } ?>
});

<?php if ( ! empty($company_id) && $company_id != $vendor_company_id){ ?>
function changeCountryData()
{
	$("#show_company_plant").trigger('click');
	var selectedValue = new Array();
	var selectedCompany = new Array();
	selectedCompany[0] = '<?php echo $company_id; ?>';
	<?php $plant_ids = explode(',', $plant_id);
	foreach ($plant_ids as $key => $value){ ?>
		selectedValue['<?php echo $key; ?>'] = '<?php echo $value; ?>';
	<?php } ?>
	$.each(selectedCompany,function(index,obj){
		$('select[name^="vendor_id"] option[value="'+obj+'"]').attr("selected","selected");
	});
	$("#vendor_id").trigger('change');
	$.each(selectedValue,function(index,obj){
		$('select[name^="plant[]"] option[value="'+obj+'"]').attr("selected","selected");
	});
}
<?php } ?>

$('body').on('click', function (e) {
    $('[data-toggle=popover]').each(function () {
        // hide any open popovers when the anywhere else in the body is clicked
        if (!$(this).is(e.target) && $(this).has(e.target).length === 0 && $('.popover').has(e.target).length === 0) {
            $(this).popover('hide');
        }
    });
});
function fetch_state(id,state='')
{
  
  var country_id = $("#country").val();
	var attr = $("#"+id).attr('attr');
	if(country_id){
		jQuery.ajax({
			url: '<?php echo base_url('user/get_state_by_country_id')?>',
			type:"post",
			async: false,
			data: {'country_id':country_id,state_id:state},
			dataType:'json',
			success: function (resObj){
				
				var html ='<option value="">Select State</option>';
				$.each(resObj,function(i,v)
				{
					if (state == v.id)
					{
						html+='<option value="'+v.id+'" selected>'+v.state+'</option>';
					}
					else
					{
						html+='<option value="'+v.id+'">'+v.state+'</option>';
					}
				
				});
				
				$("#state").html(html);
				
			},
			error: function(){
				$("#state").html('<option value="">Select State</option>');
				
			}
		});

	} else {
		$("#state").html('<option value="">Select State</option>');
		
	}
}

function fetch_city(id,city='',state='')
{
	
	var state_id = $("#state").val();
	if(state_id !== ""){
		  jQuery.ajax({
			url: '<?php echo base_url('user/get_cities_by_state_id');?>',
			type:"post",
			async: false,
			data: {'state_id':state_id,'city_id':city},
			dataType:'json',
			success: function (resObj) {

				var html ='<option value="">Select City</option>';
				$.each(resObj,function(i,v)
				{
					if (city == v.id)
					{
						html+='<option value="'+v.id+'" selected>'+v.name+'</option>';
					}
					else
					{
						html+='<option value="'+v.id+'">'+v.name+'</option>';
					}
				
				});
				
				$("#location").html(html);
				

			},
			error: function(){
				$("#location").html('<option value="">Select City</option>');
				
			}
		});
	} else {
		$("#location").html('<option value="">Select City</option>');
		
	}
}
function get_auc_type(auc_type)
{
	if (auc_type ==='Forward Auction' || auc_type ==='Yankee Auction(Forward)')
	{
		$('.auc_status').html('H1');
	}
	else
	{
		$('.auc_status').html('L1');

	}
}

</script>