<!--CONTENT CONTAINER-->
<!--===================================================-->
<div id="content-container">
    <!--Page Title-->
    <!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
    <div id="page-title">
        <h1 class="page-header text-overflow"><?php echo $pagetitle; ?></h1>
    </div>
    <!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
    <!--End page title-->

    <!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
    <!--End breadcrumb-->
	<div id="page-content">
		<div class="panel">
			<div class="panel-body">
                <form class="form-group" name="system_log_form" method="post" >
                    <div class="row filter">
                        <div class="col-sm-2">
                            <input class="form-control searchInput" placeholder="Sale No" type="text" name="sale_no" id="sale_no" value="" />
                        </div>
                        <div class="col-sm-2">
                            <input class="form-control searchInput" placeholder="Log Title" type="text" name="log_title" id="log_title" value="" />
                        </div>
                        <div class="col-sm-2">
                            <input class="form-control searchInput" placeholder="Updated By" type="text" name="updated_by" id="updated_by" value="" />
                        </div>
                        <div class="col-sm-2">
                        </div>
                        <a class="btn btn-danger btn-labeled fa fa-ban pull-right" onclick="clearSearchFilters();">Clear Search </a>
                            <a class="btn btn-success btn-labeled fa fa-refresh pull-right" onclick="refresh_datatable();">Refresh </a>
                    </div>

                    <div class="table-responsive">
                        <table style="word-wrap: break-word;table-layout: fixed;" callfunction="<?php echo base_url('auctioneer/dashboard/system_logs_ajax');?>" class="table table-striped table-bordered table-hover no-footer dtr-inline dynamicTable" noofrecords="10">
                            <thead>
                                <th width='5%' data-bSortable="false">Sr. No.</th>
                                <th>Auction Sale No</th>
                                <th>Log Title</th>
                                <th width='17%'>From Data</th>
                                <th width='17%'>To Data</th>
                                <th>Operation</th>
                                <th>Reference Log Id</th>
                                <th>IP Address</th>
                                <th>Browser</th>
                                <th>Browser Version</th>
                                <th>Platform</th>
                                <th width='10%'>User Browser Info</th>
                                <th>Updated By</th>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </form>
			</div>
		</div>
    </div>
</div>
<!--===================================================-->
<!--END CONTENT CONTAINER-->
<script>
    $(document).ready(function(){
        $(".table").DataTable();
    });
</script>