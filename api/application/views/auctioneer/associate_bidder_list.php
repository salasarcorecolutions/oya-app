<!--CONTENT CONTAINER-->
<!--===================================================-->
<div id="content-container">

<!--Page Title-->
<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
<div id="page-title">
	<h1 class="page-header text-overflow"><?php echo $pagetitle; ?></h1>
</div>
<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
<!--End page title-->

<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
<!--End breadcrumb-->
	<div id="page-content">
		<div class="panel">
			<div class="panel-heading">
				<h3 class="panel-title col-sm-5"><?php echo $pagetitle; ?></h3>
			</div>
			<div class="panel-body">
                <table id="associate_bidders_table" class="table table-striped table-bordered table-hover table-responsive">
                    <thead>
                        <th>Sr. No.</th>
                        <th>Company Name</th>
                        <th>Contact Person Name</th>
                        <th>Vendor Approval</th>
                        <th>Date Added</th>
                    </thead>
                    <tbody id="associate_bidder">
                    </tbody>
                </table>
			</div>
		</div>
	</div>
</div>
<!--===================================================-->
<!--END CONTENT CONTAINER-->
<style type='text/css'>
.table>tbody>tr>td,
.table>tbody>tr>th,
.table>tfoot>tr>td,
.table>tfoot>tr>th, 
.table>thead>tr>td,
.table>thead>tr>th
{
	padding: 4px !important;
}
fieldset
{
	border: 1px solid #ddd !important;
	margin: 0;
	padding: 10px;
	position: relative;
	border-radius:4px;
	background-color:#f5f5f5;
	padding-left:10px!important;
}

legend
{
	font-size:14px;
	font-weight:bold;
	margin-bottom: 0px;
	width: 35%; 
	border: 1px solid #ddd;
	border-radius: 4px;
	padding: 5px 5px 5px 10px; 
	background-color: #ffffff;
}
</style>

<script src="https://cdn.ckeditor.com/4.14.0/standard/ckeditor.js"></script>

<script type='text/javascript'>
var t;
$(document).ready(function(){
    t = $("#associate_bidders_table").DataTable();
    getAssociateData();
});


function getAssociateData()
{
    $.ajax({
        url:'<?php echo base_url('auctioneer/bidders/associate_bidder_data')?>',
        dataType: 'json',
        success:function(data)
        {
            if (data){
                var i, j, vendorApproval;
                for (i=0; i<data.length; i++){
                    j = i+1;
                    if (data[i]['vendor_approval'] == 1){
                        vendorApproval = '<a onclick="rejectBidder('+data[i]['bidder_id']+')" style="width:100%; display:flex; justify-content:center;"><i style="color:green; font-size:15px;" class="fa fa-check"></i></a>';
                    } else {
                        vendorApproval = '<a onclick="acceptBidder('+data[i]['bidder_id']+')" style="width:100%; display:flex; justify-content:center;"><i style="color:red; font-size:15px;" class="fa fa-times"></i></a>';
                    }
                    t.row.add( [
                        j,
                        data[i]['compname'],
                        data[i]['conperson'],
                        vendorApproval,
                        data[i]['date_added']
                    ] ).draw( false );
                }
            }
        }
    });
}

function rejectBidder(id)
{
    $.ajax({
        url:'<?php echo base_url('auctioneer/bidders/reject_associative_bidder')?>',
        data: {"bidder_id":id},
        type: "POST",
        dataType: 'json',
        success:function(response)
        {
            if (response.status == 'success'){
                t.rows().remove().draw();
                Display_msg(response.message,response.status);
                getAssociateData();
            } else {
                Display_msg(response.message,response.status);
            }
        }
    });
}

function acceptBidder(id)
{
    $.ajax({
        url:'<?php echo base_url('auctioneer/bidders/accept_associate_bidder')?>',
        data: {"bidder_id":id},
        type: "POST",
        dataType: 'json',
        success:function(response)
        {
            if (response.status == 'success'){
                t.rows().remove().draw();
                Display_msg(response.message,response.status);
                getAssociateData();
            } else {
                Display_msg(response.message,response.status);

            }
        }
    });
}



</script>
