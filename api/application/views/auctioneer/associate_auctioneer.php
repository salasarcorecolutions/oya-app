<!--===================================================-->
<div id="content-container">
	<!--Page Title-->
	<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
	<div id="page-title">
		<h1 class="page-header text-overflow"><?php echo $pagetitle; ?></h1>
	</div>
	<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
	<!--End page title-->

	<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
	<!--End breadcrumb-->
	<div id="page-content">
		<div class="panel">
			<div class="panel-body">
				<form name="frm" id="frm">
					<div class="panel panel-primary margin-bottom-40">
						<div class="panel-heading">
							<h3 class="panel-title"><i style='margin-right:10px;' class="fa fa-handshake-o"></i>Associate Auctioneer</h3>
						</div>
						<div class="panel-body">
							<div class="row">
                                <div class="col-sm-3">
                                    <div class="form-group">
                                        <label class="control-label">Auctioneer Code<span class="text-danger"><i class="fa fa-asterisk" aria-hidden="true"></i></span></label>
                                        
                                    </div>
								</div>
								<div class="col-sm-4">
									<div class="form-group">
									<input id="vendor_code" type="number" class="form-control" name="vendor_code" />
                                      
									</div>
								</div>
								<div class="col-sm-4">
									<div class="form-group">
									<button type="submit" class="btn btn-primary">Submit</button>
									</div>
								</div>
                            </div>
                        </div>
                    </div>
				</form>
			</div>
		</div>
    </div>
    <div id="page-content">
		<div class="panel">
			<div class="panel-body">
				<form name="new_frm" id="new_frm">
					<div class="panel panel-primary margin-bottom-40">
						<div class="panel-heading">
							<h3 class="panel-title"><i style='margin-right:10px;' class="fa fa-user-plus"></i>Send Request To Auctioneer</h3>
						</div>
						<div class="panel-body">
							<div class="row">
                                <table class="table table-striped table-bordered table-hover table-responsive">
                                    <thead>
                                        <tr>
                                            <th>Auctioneer Name</th>
                                            <th>Person Name</th>
                                            <th>City</th>
                                            <th>State</th>
                                            <th>Country</th>
                                            <th width='10%;'>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody id="auctioneer_details">
                                    </tbody>
                                </table>
                             </div>
                        </div>
                    </div>
				</form>
			</div>
		</div>
	</div>
</div>
<!--===================================================-->
<!--END CONTENT CONTAINER-->
<style type='text/css'>
.table>tbody>tr>td,
.table>tbody>tr>th,
.table>tfoot>tr>td,
.table>tfoot>tr>th,
.table>thead>tr>td,
.table>thead>tr>th
{
	padding: 4px !important;
}
fieldset
{
	border: 1px solid #ddd !important;
	margin: 0;
	padding: 10px;
	position: relative;
	border-radius:4px;
	background-color:#f5f5f5;
	padding-left:10px!important;
}

legend
{
	font-size:14px;
	font-weight:bold;
	margin-bottom: 0px;
	width: 35%;
	border: 1px solid #ddd;
	border-radius: 4px;
	padding: 5px 5px 5px 10px; 
	background-color: #ffffff;
}
</style>


<script type='text/javascript'>
	var vRules 	= 	{
		vendor_code:{required:true,maxlength:100},
	};
	var vMessage = {
		vendor_code:{required:"<b class='text-danger'>Please Enter Auctioneer Code</b>"},
    };

	$("#frm").validate({
		rules: vRules,
		messages: vMessage,
		submitHandler: function(form){
			$(form).ajaxSubmit({
				url: '<?php echo base_url('auctioneer/my_associate/fetch_auctioneer_data');?>',
				type:"post",
				dataType: 'json',
				success: function (resObj) {
                    if (resObj.status == 'success'){
                        var html = '';
                        html += '<tr>'
                        html += '<td>'+resObj.result.vendor_name+'</td>'
                        html += '<td>'+resObj.result.c_name+'</td>'
                        html += '<td>'+resObj.result.city_name+'</td>'
                        html += '<td>'+resObj.result.state_name+'</td>'
                        html += '<td>'+resObj.result.country_name+'</td>'
                        html += '<td><input type="hidden" name="vendor_id" value="'+resObj.result.vendor_id+'" />'
                        html += '<button type="submit" class="btn btn-primary">Send Request</button></td>'
                        html += '</tr>';
                        $("#auctioneer_details").html(html);
                    } else {
                        Display_msg(resObj.message,resObj.status);
                    }
				},
				error: function(e){
					alert('Problem in fetching auctioneer data. Please try again.');
				}
			});
		}
    });

    $("#new_frm").validate({
		submitHandler: function(form){
			$(form).ajaxSubmit({
				url: '<?php echo base_url('auctioneer/my_associate/send_associate_request');?>',
				type:"post",
				dataType: 'json',
				success: function (resObj) {
                    Display_msg(resObj.message,resObj.status);
				},
				error: function(e){
					alert('Problem in Sending request to auctioneer. Please try again.');
				}
			});
		}
	});


</script>
