<?php
$imgpath = "productpic";
?>
<!DOCTYPE HTML>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title><?php if(isset($pagetitle)){echo $pagetitle;}else{echo "Oya Auction";} ?></title>
	<!-----------------------------------DataTables css---------------------------------------------------------->
	<link rel="stylesheet" href="<?php echo base_url('auctioneer_assets/css/datatables/dataTables.bootstrap.css'); ?>" />
	<link rel="stylesheet" href="<?php echo base_url('auctioneer_assets/css/datatables/dataTables.responsive.css'); ?>" />
	<!--Bootstrap Stylesheet [ REQUIRED ]-->
	<link href="<?php echo base_url('auctioneer_assets/css/bootstrap.min.css'); ?>" rel="stylesheet">
	<!--Nifty Stylesheet [ REQUIRED ]-->
	<link href="<?php echo base_url('auctioneer_assets/css/nifty.min.css'); ?>" rel="stylesheet">
	<link href="<?php echo base_url('auctioneer_assets/css/dhtmlwindow.css'); ?>" rel="stylesheet">
	<link href="<?php echo base_url('auctioneer_assets/css/modal.css'); ?>" rel="stylesheet">
	<link href="<?php echo base_url('auctioneer_assets/css/custom.css'); ?>" rel="stylesheet">
	<!--Font Awesome [ OPTIONAL ]-->
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
	<!--Animate.css [ OPTIONAL ]-->
	<link href="<?php echo base_url('auctioneer_assets/plugins/animate-css/animate.min.css'); ?>" rel="stylesheet">
	<!--Morris.js [ OPTIONAL ]-->
	<link href="<?php echo base_url('auctioneer_assets/plugins/morris-js/morris.min.css'); ?>" rel="stylesheet">
	<!--rich text editor [ OPTIONAL ]-->
<link rel="stylesheet" href="<?php echo base_url();?>assets/plugins/rich-text-editor/richtext.min.css">
	

	<!--Switchery [ OPTIONAL ]-->
	<link href="<?php echo base_url('auctioneer_assets/plugins/switchery/switchery.min.css'); ?>" rel="stylesheet">

	<!--Bootstrap Select [ OPTIONAL ]-->
	<link href="<?php echo base_url('auctioneer_assets/plugins/bootstrap-select/bootstrap-select.min.css'); ?>" rel="stylesheet">

	<!--Bootstrap Tags Input [ OPTIONAL ]-->

	<!--Chosen [ OPTIONAL ]-->
	<link href="<?php echo base_url('auctioneer_assets/plugins/chosen/chosen.min.css'); ?>" rel="stylesheet">


	<!--Demo script [ DEMONSTRATION ]-->
	<link href="<?php echo base_url('auctioneer_assets/css/nifty.min.css'); ?>" rel="stylesheet">
	<!--Demo script [ DEMONSTRATION ]-->
	<!-- <link href="<?php echo base_url('auctioneer_assets/css/demo/nifty-demo.min.css'); ?>" rel="stylesheet"> -->

	

	<!--Page Load Progress Bar [ OPTIONAL ]-->
	<link href="<?php echo base_url('auctioneer_assets/plugins/pace/pace.min.css'); ?> " rel="stylesheet">
	<script src="<?php echo base_url('auctioneer_assets/plugins/pace/pace.min.js'); ?>"></script>
	
	<!--Bootstrap Timepicker [ OPTIONAL ]-->
	<link href="<?php echo base_url('auctioneer_assets/plugins/bootstrap-timepicker/bootstrap-timepicker.min.css'); ?>" rel="stylesheet">

	<!-- Bootstrap Datepicker [ OPTIONAL ] -->
	<!-- <link href="<?php echo base_url('auctioneer_assets/plugins/bootstrap-datepicker/bootstrap-datepicker.css'); ?>" rel="stylesheet"> -->
	<!-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/datepicker/0.6.5/datepicker.css" integrity="sha256-n3ci71vDbbK59GUg1tuo+c3KO7+pnBOzt7BDmOe87s4=" crossorigin="anonymous" /> -->
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/css/bootstrap-datepicker.min.css" integrity="sha256-siyOpF/pBWUPgIcQi17TLBkjvNgNQArcmwJB8YvkAgg=" crossorigin="anonymous" />
	<link href="<?php echo base_url('auctioneer_assets/css/mdtimepicker.css'); ?>" rel="stylesheet" />
	<!--link href="dataTables.tableTools.css" rel="stylesheet">-->
	<link href="<?php echo base_url('auctioneer_assets/css/style.css'); ?>" rel="stylesheet">
	
	<!-- added by aatish gupta start  22 sep 2016-->
	<link href="<?php echo base_url('auctioneer_assets/css/select2.css'); ?>" rel="stylesheet">
	<!-- added by aatish gupta end-->
	<!-- bootstrap multiselect -->
	<link href="<?php echo base_url('auctioneer_assets/css/bootstrap-multiselect.css'); ?>" rel="stylesheet">

			
	<!-- Favicons
	================================================== -->
	<link rel="shortcut icon" href="<?php echo base_url('auctioneer_assets/images/favicon.png'); ?>" type="image/x-icon">
	
	<!-- JS -->
	<script src="<?php echo base_url('auctioneer_assets/js/jquery-2.1.1.min.js'); ?>"></script>

	<!--BootstrapJS [ RECOMMENDED ]-->
	<script src="<?php echo base_url('auctioneer_assets/js/bootstrap.min.js'); ?>"></script>
	<!--
	<script src="js/jquery-ui.min.js"></script>-->
	
	<!-----------------------------------DataTables JS---------------------------------------------------------->
	
	<script src="<?php echo base_url('auctioneer_assets/js/dataTables/jquery.dataTables.min.js'); ?>"></script>
	<script src="<?php echo base_url('auctioneer_assets/js/dataTables/dataTables.bootstrap.min.js'); ?>"></script>
	<script src="<?php echo base_url('auctioneer_assets/js/dataTables/manual_datatable.js'); ?>"></script>
	<!--Fast Click [ OPTIONAL ]-->
	<script src="<?php echo base_url('auctioneer_assets/js/fastclick.min.js'); ?>"></script>
	<!--Modals [ SAMPLE ]-->
	<script src="<?php echo base_url('auctioneer_assets/js/ui-modals.js'); ?>"></script>
	<!--Bootbox Modals [ OPTIONAL ]-->
	<script src="<?php echo base_url('auctioneer_assets/js/bootbox.min.js'); ?>"></script>
	<script src="<?php echo base_url('auctioneer_assets/plugins/morris-js/raphael-js/raphael.min.js'); ?>"></script>
	<!--Sparkline [ OPTIONAL ]-->
	<script src="<?php echo base_url('auctioneer_assets/js/jquery.sparkline.min.js'); ?>"></script>
	<script src="<?php echo base_url();?>assets/plugins/rich-text-editor/jquery.richtext.min.js"></script>
	<!--Skycons [ OPTIONAL ]-->
	<script src="<?php echo base_url('auctioneer_assets/plugins/skycons/skycons.min.js'); ?>"></script>
	<!-- Validatation [ RECOMMENDED ]-->
	<script src="<?php echo base_url('auctioneer_assets/js/jquery.validate.js'); ?>"></script>
	<script src="<?php echo base_url('auctioneer_assets/js/jquery.form.js'); ?>"></script>
	<!--Switchery [ OPTIONAL ]-->
	<script src="<?php echo base_url('auctioneer_assets/plugins/switchery/switchery.min.js'); ?>"></script>
	<!--Bootstrap Select [ OPTIONAL ]-->
	<script src="<?php echo base_url('auctioneer_assets/plugins/bootstrap-select/bootstrap-select.min.js'); ?>"></script>
	<!--Chosen [ OPTIONAL ]-->
	<script src="<?php echo base_url('auctioneer_assets/plugins/chosen/chosen.jquery.min.js'); ?>"></script>
	<!--Bootstrap Tags Input [ OPTIONAL ]-->
	<script src="<?php echo base_url('auctioneer_assets/plugins/bootstrap-tagsinput/bootstrap-tagsinput.js'); ?>"></script>
	<!--Bootstrap Timepicker [ OPTIONAL ]-->
	<script src="<?php echo base_url('auctioneer_assets/plugins/bootstrap-timepicker/bootstrap-timepicker.min.js'); ?>"></script>
	<script src="<?php echo base_url('auctioneer_assets/js/bootstrap-multiselect.min.js'); ?>"></script>
	
	
<script language="javascript">
	function refreshParent(page) {
		location.href = "index.php?src="+page;
	}
</script>

<style>
	#mainnav-container{
		height: 105%;
	}
.form-control{
	height: auto!important;
}	
.float_btn{
    position: fixed;
    top: 15%;
    right: 5%;
    z-index: 10;
}
.float_btn i {
	font-size: 20px;
}
#page-content{
    position: relative;
}
.info_pop{
	font-size: 20px!important;
    color: #337ab7!important;
}
a.btn {
    margin: 1px;
}
.loading{
	position: fixed;
	top: 0%;
	opacity: 0.7;
	filter: alpha(opacity=70);
	width: 100%;
	height:100%;
	right: 0px;
	background-color: white;
	z-index: 50000;
}
.loading img{
	margin-top:10%;
}
.modal_section{
	padding: 2% 5%;
}
.br-5{
	border-radius:5px!important;
}
.text-center{
text-align: center;
}
.mdtp__wrapper {
    bottom: 175px!important;
}
#container .table th {
    font-size: 0.9em;
}
.footer_class{
	padding-top: 4%;
    padding-bottom: 2%;
    border-top: 1px solid #BDBDBD;
    margin-top: 3%;
}
.company_brand{
	position: absolute;
  top: 50%;
  left: 50%;
  transform: translate(-50%, -50%);
}
/* Chrome, Safari, Edge, Opera */
input::-webkit-outer-spin-button,
input::-webkit-inner-spin-button {
  -webkit-appearance: none;
  margin: 0;
}
.text-danger i{
	font-size:8px!important;
}
/* Firefox */
input[type=number] {
  -moz-appearance: textfield;
}
</style>
<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.10/css/select2.min.css" rel="stylesheet" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.10/js/select2.min.js"></script>
</head>
<body >
	<!-- Top Menu -->
	<div id="container" class="effect mainnav-lg">
	<!--NAVBAR-->
		<!--===================================================-->
		<header id="navbar">
			<div id="navbar-container" class="boxed">
				<!--Brand logo & name-->
				<!--================================-->
				<div class="navbar-header">
					<a href="<?php echo base_url('auctioneer/dashboard'); ?>" class="navbar-brand">
						<?php if ( ! empty($this->session->userdata('vendor_logo'))){ ?>
							<img style="padding:3%;" src="<?php echo $this->session->userdata('vendor_logo'); ?>" alt="Nifty Logo" class="brand-icon">
						<?php } else { ?>
							<img style="padding:3%;" src="<?php echo base_url('auctioneer_assets/images/oyafinalwhite.png'); ?>" alt="Nifty Logo" class="brand-icon">
						<?php } ?> 
						<div class="brand-title">
							<?php if (empty ($this->session->userdata('vendor_logo'))){ ?>
							<span class="brand-text">Oya Auction</span>
							<?php } else { ?>
								<span class="brand-text"><?php echo $this->session->userdata('vendor_name'); ?></span>
							<?php } ?>
						</div>
					</a>
				</div>
				<!--================================-->
				<!--End brand logo & name-->


				<!--Navbar Dropdown-->
				<!--================================-->
				
				<div style="position: relative;" class="navbar-content clearfix">


					<ul class="nav navbar-top-links company_brand">
						<?php if (empty ($this->session->userdata('vendor_logo'))){ ?>
							<li><a><h3>Oya Auction</h3></a></li>
						<?php } else { ?>
							<li><a><h3><?php echo $this->session->userdata('vendor_name'); ?></h3></a></li>
						<?php } ?>
					</ul>

					<ul class="nav navbar-top-links pull-left">

						<!--Navigation toogle button-->
						<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
						<li class="tgl-menu-btn">
							<a class="mainnav-toggle" href="#" >
								<i class="fa fa-navicon fa-lg"></i>
							</a>
						</li>
						<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
						<!--End Navigation toogle button-->
					</ul>
					<ul class="nav navbar-top-links pull-right">
						<!--User dropdown-->
						<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
						<li id="dropdown-user" class="dropdown">
							<a href="#" data-toggle="dropdown" class="dropdown-toggle text-right">
								<span class="pull-right">
									<img class="img-circle img-user media-object" alt="Profile Picture" src="<?php echo base_url('auctioneer_assets/images/av1.png'); ?>">
								</span>
								
								<div class="username hidden-xs"><?php echo $this->session->userdata('user_name'); ?></div>
							</a>
							<div class="dropdown-menu dropdown-menu-md dropdown-menu-right with-arrow panel-default">
						
								<div class="panel-body">
									<div style="width: 100%;display:flex;justify-content:center">
										<?php if ( ! empty($this->session->userdata('vendor_logo'))){ ?>
											<img class='img-md mar-btm' style="padding:3%;width:50%;height:auto;" src="<?php echo $this->session->userdata('vendor_logo'); ?>" alt="Nifty Logo" class="brand-icon">
										<?php } else { ?>
											<img class='img-md mar-btm' style="padding:3%;width:50%;height:auto;" src="<?php echo base_url('auctioneer_assets/images/oyafinalwhite.png'); ?>" alt="Nifty Logo" class="brand-icon">
										<?php } ?>
									</div>
									
									<?php if (empty ($this->session->userdata('vendor_logo'))){ ?>
									<center><h4 class="brand-text">Oya Auction</h4></center>
									<?php } else { ?>
									<center><h4 class="brand-text"><?php echo $this->session->userdata('vendor_name'); ?></h4></center>
									<?php } ?>
								</div>
								<div class="pad-all bord-btm">
									<p class="text-lg text-muted text-thin mar-btm">Profile Completion</p>
									<div class="progress progress-sm">
										<div class="progress-bar" style="width: 70%;">
											<span class="sr-only">70%</span>
										</div>
									</div>
								</div>
								<ul class="head-list">
									<li>
										<a href="#"  onclick="showTimeZoneModal()">
											<i class="fa fa-user fa-fw fa-lg"></i> Time Zone
										</a>
									</li>
								</ul>
								<ul class="head-list">
									<li>
										<a href="<?php echo base_url('user/logout');?>" class="btn btn-primary">
											<i class="fa fa-sign-out fa-fw"></i> Logout
										</a>
									</li>
								</ul>
							</div>
						</li>
						<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
						<!--End user dropdown-->
					</ul>
					<ul class="nav navbar-top-links pull-right">
						<li>
							<a class="hidden-xs" id='date-part'></a>
						</li>
					</ul>	
				</div>
				<!--================================-->
				<!--End Navbar Dropdown-->
			</div>
		</header>
		<!-- Modal -->
		<div class="modal fade" id="time_zone_modal" role="dialog">
			<div class="modal-dialog">
				<!-- Modal content-->
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal">&times;</button>
						<h4 class="modal-title">Update time zone</h4>
					</div>
					<div class="modal-body">
						<div>
							<select class="form-control" id="time_zone_list">
							</select>
						</div>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-primary" onclick="updateUserTimeZone()">Update</button>
						<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
					</div>
				</div>
			</div>
		</div>
		
		<!--===================================================-->
		<!--END NAVBAR-->
	<div class="boxed">
