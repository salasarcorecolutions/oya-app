<!--CONTENT CONTAINER-->
<!--===================================================-->
<div id="content-container">
	<!--Page Title-->
	<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
	<div id="page-head">
	<div id="page-title">
		<h1 class="page-header text-overflow"><?php echo $pagetitle; ?></h1>
	</div>
	<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
	<!--End page title-->

	<!--Breadcrumb-->
	<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
	<ol class="breadcrumb">
		<li><a href="<?php echo base_url('dashboard');?>">Home</a></li>
		<li class="active"><?php echo $pagetitle; ?></li>
	</ol>
	</div>
	<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
	<!--End breadcrumb-->
	<input type="hidden" id="t11" name="auctionId">
	<div id="page-content">	
		<div class="float_btn">
			<a class="btn btn-primary btn-icon btn-circle add-tooltip" data-placement="top" data-toggle="tooltip" data-original-title="Back" href='<?php echo base_url('dashboard'); ?>' ><i class="fa fa-arrow-left" aria-hidden="true"></i></a>
		</div>
		<div class="panel">
			<div class="panel-heading">
				<h3 class="panel-title"><?php echo $pagetitle; ?></h3>
			</div>
			
			<div class="panel-body">
				<form name="rev_auction_form" method="post" >
					<div class="row filter">
						<div class="col-sm-2">
							<input class="form-control searchInput" placeholder="Vendor Name" type="text" name="vendor_name" id="vendor_name" value="" />
						</div>
					</div>
					<div class="table-responsive">
						<table callfunction="<?php echo base_url('auctioneer/my_associate/fetch_associate_auctioneer');?>" class="table table-striped table-bordered table-hover no-footer dtr-inline dynamicTable">
							<thead>
								<tr>
									<th data-bSortable="false">Sr. No.</th>
									<th>Auctioneer Name</th>
									<th>Person Name</th>
									<th>City</th>
                                    <th>State</th>
									<th>Country</th>
									<th width='10%' data-bSortable="false">Action</th>
								</tr>
							</thead>
							<tbody>
							</tbody>
						</table>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>

<!--===================================================-->
<!--END CONTENT CONTAINER-->
<script>
    function setPermission(id)
    {
        $.ajax({
            url: "<?php echo base_url('auctioneer/my_associate/check_associate_vendor_relation'); ?>",
            type: "POST",
            dataType:"json",
            cache: false,
            clearForm: false,
            data: { "id" :id },
            success:function(response)
            {
                if (response.status == "success"){
                    window.open("<?php echo base_url('auctioneer/my_associate/set_auctioneer_permission/'); ?>"+response.id);
                } else {
                    Display_msg(response.message,response.status);
                }
            },
            error: function(){
                Display_msg('Problem in fetching permission. Please Try Again','failed');
            }
        });
    }

    function removeAuctioneer(id)
    {
        $.ajax({
            url: "<?php echo base_url('auctioneer/my_associate/remove_auctioneer_relation'); ?>",
            type: "POST",
            dataType:"json",
            cache: false,
            clearForm: false,
            data: { "id" :id },
            success:function(response)
            {
                if (response.status == "success"){
                    Display_msg(response.message,response.status);
                    refresh_datatable();
                } else {
                    Display_msg(response.message,response.status);
                }
            },
            error: function(){
                Display_msg('Problem in removing auctioneer. Please try again after sometime.','failed');
            }
        });
    }


</script>