<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/css/bootstrap-datepicker.min.css" integrity="sha256-siyOpF/pBWUPgIcQi17TLBkjvNgNQArcmwJB8YvkAgg=" crossorigin="anonymous" />
<style>
	.modal_section{
		max-height: 600px;
		overflow-x: scroll;
	}
	.control-label{
		color: #25476a;
		font-weight: bold;
	}
	.form-group {
    padding: 0 10px;
	}
	.no_pad{
		padding: 0px!important;
	}
</style>

<!--CONTENT CONTAINER-->
<!--===================================================-->
<div id="content-container">
	<!--Page Title-->
	<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
	<div id="page-title">
		<h1 class="page-header text-overflow">Employee Contact List</h1>
	</div>
	<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
	<!--End page title-->

	<!--Breadcrumb-->
	<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
	<ol class="breadcrumb">
		<li><a href="<?php echo base_url('dashboard');?>">Home</a></li>
		<li class="active">Employee List</li>
	</ol>
	<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
	<!--End breadcrumb-->
	<input type="hidden" id="t11" name="auctionId">
	<div id="page-content">
		<div class="panel">
			<div class="panel-heading">
				<h3 class="panel-title">

					<?php if ($this->common_model->check_permission('Sub User Add')){ ?>
						<button id="add_btn" class='btn btn-primary btn-labeled fa fa-plus pull-right' style="margin-top: 10px;" onclick='show_user_modal();'>Add Employee</button>
					<?php } ?>
				</h3>
			</div>
			<div class="panel-body">
				<div class="table-responsive">
					<table class="table table-striped table-bordered table-hover no-footer dtr-inline staticTable">
						<thead>
							<tr>
								<th>SR NO</th>
								<th>EMPLOYEE NAME</th>
								<th>CONTACT NO.</th>
								<th>E-MAIL</th>
								<th>EMPLOYEE TYPE</th>
								<th>STATUS</th>
								<th>ACTION</th>
							</tr>
						</thead>
						<tbody>
							<?php
							if ( ! empty($vendor_contact))
							{
								$count = 1;
								foreach($vendor_contact as $single_contact)
								{
									?>
									<tr>
										<td><?php echo $count++; ?></td>
										<td><?php echo $single_contact['c_name']; ?></td>
										<td><?php echo $single_contact['c_contacts']; ?></td>
										<td><?php echo $single_contact['c_email'].'<br>'.$single_contact['secondary_email'] ; ?></td>
										<td><?php echo ($single_contact['user_type'] == 'A') ? 'Admin' : 'User' ; ?></td>
										<td><?php echo ($single_contact['activated'] == 'Y') ? '<span style="color:green">Active</span>' : '<span style="color:red">Deactivate</span>'; ?></td>
										<td>
											<?php
											if ($single_contact['user_type'] != 'A')
											{
												if ($this->common_model->check_permission("Sub User Edit"))
												{
													?><a class=' btn btn-primary form-control btn-labeled fa fa-pencil-square-o btn-sm' href='javascript:void(0)' onclick='edit_user("<?php echo $single_contact['id']; ?>")'>Edit</a><?php
												}
											}

											?>
										</td>
									</tr>
									<?php
								}
							}
							?>
						</tbody>

					</table>
				</div>
			</div>
		</div>
	</div>
</div>
<!--===================================================-->
<!--END CONTENT CONTAINER-->

<div id="add_edit_user_modal" class="modal" role="dialog">
	<div style="z-index: 100000;" class="modal-dialog modal-lg animated zoomInDown">
		<div style='border-radius:5px;' class="modal-content">
			<div style="width:auto" class="container">
				<div class="modal_section">
					<form id="frm_add_alias" class="form-horizontal">
						<div class="row">
							<div class="col-12">
								<h3 style="text-align: center;">Enter following details:</h3>
							</div>
						</div>
						<div class="row" style="margin-top: 20px;">
							<div class="col-sm-6">
								<div class="form-group">
									<label class="control-label">Enter Name</label>
									<input type="hidden" id='id' name="vendor_contact_id">
									<input type="text" id="c_name" name="name" class="form-control br-5" placeholder="Enter your name.">
								</div>
							</div>
							<div class="col-sm-6">
								<div class="form-group">
									<label class="control-label">Enter Username</label>
									<input type="text" id="uname" name="username" placeholder="Enter username." class="form-control br-5">
								</div>
							</div>
							<div class="col-sm-6">
								<div class="form-group">
									<label class="control-label">Enter Password</label>
									<input type="password" id="password" placeholder="Enter Password" name="password" class="form-control br-5" >
								</div>
							</div>
							<div class="col-sm-6">
								<div class="form-group">
									<label class="control-label">Confirm Password</label>
									<input type="password" id="conf_password" name="conf_password" placeholder="Enter Confirm Password" class="form-control br-5">
								</div>
							</div>
							<div style="clear: both;" class="col-sm-6">
								<label class="control-label">Enter Phone No.</label>
								<div class="form-group">
									<div class="col-xl-9 col-lg-9 col-md-6 col-sm-8 col-xs-8 no_pad">
										<input type="number" id="phone_no_1" placeholder="Enter your Phone No." exactlength="10" name="phone_no" class="form-control br-5">
										<input id="mobile_otp_1" class="form-control br-5" style="display:none" type="number" min="0" name="mobile_otp_1" placeholder="Enter OTP" />
										<div id='mobile_msg_1'></div>
									</div>
								</div>
							</div>
							<div class="col-sm-6">
								<div class="form-group">
									<label class="control-label">Secondary Phone No.</label>
									<div class="col-xl-9 col-lg-9 col-md-6 col-sm-8 col-xs-8 no_pad">
										<input type="number" id="phone_no_2" name="phone_no_two" placeholder="Enter your Secondary Phone No." class="form-control br-5" >
										<input id="mobile_otp_2" class="form-control br-5" style="display:none" type="number" min="0" name="mobile_otp_2" placeholder="Enter OTP" />
										<div id='mobile_msg_2'></div>
									</div>
								</div>
							</div>
							<div style="clear: both;" class="col-sm-6">
								<label class="control-label">Enter Email</label>
								<div class="form-group">
									<div class="col-xl-9 col-lg-9 col-md-6 col-sm-8 col-xs-8 no_pad">
										<input id="email_1" type="email" name="email" placeholder="Enter your Email" class="form-control br-5">
										<input id="otp_1" type="number" min="0" name="otp_1" class="form-control br-5" placeholder="Enter OTP" />
										<div id='email_msg_1'></div>
									</div>
									<div class="col-xl-3 col-lg-3 col-md-2 col-sm-4 col-xs-4">
										<a class="btn btn-primary br-5" onclick="send_email_otp(1)">Verify</a>
									</div>
								</div>
							</div>
							<div class="col-sm-6">
								<label class="control-label">Enter Secondary Email</label>
								<div class="form-group">
									<div class="col-xl-9 col-lg-9 col-md-6 col-sm-8 col-xs-8 no_pad">
										<input type="email" id="email_2" placeholder="Enter your Secondary Email." name="email_two" class="form-control br-5" >
										<input id="otp_2" style="display:none" type="number" min="0" name="otp_2" placeholder="Enter OTP" class="form-control br-5" />
										<div id='email_msg_2'></div>
									</div>
									<div class="col-xl-3 col-lg-3 col-md-2 col-sm-4 col-xs-4">
										<a class="btn btn-primary br-5" onclick="send_email_otp(2)">Verify</a>
									</div>
								</div>
							</div>
							<div style="clear: both;" class="col-sm-6">
								<div class="form-group">
									<label class="control-label">Time Zone</label>
									<select class="form-control br-5" id="user_tz" name="user_tz" require>
										<option></option>
										<?php foreach ($time_zones as $value): ?>
											<option value="<?php echo $value['timezone']; ?>"><?php echo  '('.$value['gmt_offset'].') &nbsp' .$value['time_zone_description']. ' &nbsp ('.$value['country_name'].')'; ?></option>
										<?php endforeach; ?>
									</select>
								</div>
							</div>
							<div class="col-sm-6">
								<div class="form-group">
									<label class="control-label">Company/Plant</label>
									<select class="select2-multiple form-control br-5" id="company" name="company[]" multiple="multiple" require>
										<option></option>
										<?php foreach ($company as $value): ?>
											<option value="<?php echo $value['vendor_id'].",".$value['company_plant_id']; ?>"><?php echo 'Company Name: '.$value['vendor_name']. ' &nbsp Plant Name: '.$value['plant_name']?></option>
										<?php endforeach; ?>
									</select>
								</div>
							</div>
							<div class="col-sm-6">
								<div class="form-group">
									<label class="control-label">Active Till</label>
									<input class='form-control br-5' readonly type="text" name="active_till" id="active_till">
								</div>
							</div>
							<div class="col-sm-6">
								<div class="form-group">
									<label class="control-label">Employee Type</label>
									<select id="user_type" name="user_type">
										<option></option>
										<option value="1">User</option>
										<option value="0">Admin</option>
									</select>
								</div>
							</div>
						</div>
						<div class="footer_class">
							<center>
							<button type="submit" class="btn btn-success br-5" id='add_user' >Submit</button>
							<button type="button" class="btn btn-danger br-5" data-dismiss="modal">Close</button>
							</center>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/js/bootstrap-datepicker.min.js" integrity="sha256-bqVeqGdJ7h/lYPq6xrPv/YGzMEb6dNxlfiTUHSgRCp8=" crossorigin="anonymous"></script>
<script>

	function btn_tgl() {
		$('#add_btn').hide() ;
	}
	var vrules = {
		name:"required",
		phone_no:{
			required: true,
			number: true,
		},
		email: "required",
		username: "required",
		password: "required",
		conf_password:{
			required: "required",
			equalTo: "#password"
		},
		'company[]': "required"
	};
	var vmessages = {
		name: "<b class='text-danger'>Please Enter the Name</b>",
		phone_no:{
			required: "<b class='text-danger'>Please Enter Contact Number</b>",
			number: "<b class='text-danger'>Enter valid Number</b>",
			exactlength: "<b class='text-danger'>Enter valid Number</b>"
		},
		email: "<b class='text-danger'>Please enter valid E-mail ID</b>",
		username: "<b class='text-danger'>Please enter username</b>",
		password: "<b class='text-danger'>Please Enter the Password</b>",
		conf_password:{
			required: "<b class='text-danger'>Please Enter Confirm Password</b>",
			equalTo: "<b class='text-danger'>Password and Confirm password is not match</b>"
		},
		'company[]': "<b class='text-danger'>Please Select Company</b>"
	};

	$( document ).ready(function() {
		var activeTill = '<?php echo $active_till; ?>'
		$("#user_tz").select2({
			placeholder: "Select Time Zone",
			width: '100%'
		});
		$('#user_type').select2({
			placeholder: "Select User Type",
			width: '100%'
		});
        $('#company').select2({
			placeholder: "Select Company Plant",
			width: '100%'
		});
		$('#department_id').select2({
			placeholder: "Select Department",
			tags:true,
			width: '100%'
		});
		$('#role').select2({
			placeholder: "Select Role",
			width: '100%'
		});

		$('#active_till').datepicker({
			format: 'yyyy-mm-dd',
			endDate: activeTill
		});
	});

	$('#frm_add_alias').validate({
		rules: vrules,
		messages: vmessages,
		submitHandler: function(form){
			$(form).ajaxSubmit({
				url:"<?php echo base_url('auctioneer/sub_user/add_user');?>",
				type:"post",
				dataType: "json",
				success: function (response){
					if (response.status == "success"){
						Display_msg(response.message,response.status);
						setTimeout(function(){
							window.location.href="<?php echo base_url('dashboard');?>";
						}, 3000);
					} else {
						Display_msg(response.message,response.status);
					}
				},
				error: function(e){
					Display_msg(e);
				}
			});
		}
	});

	function show_user_modal()
	{
		clearForm();
		$('#company').val('').trigger('change');
		document.getElementById('frm_add_alias').reset();
		$("#add_edit_user_modal").modal('show');
	}

	function edit_user(id)
	{
		clearForm();
		$.ajax({
			url:"<?php echo base_url('auctioneer/sub_user/get_single_user_details');?>",
			type: 'post',
			dataType:'json',
			data:{'id':id},
			cache: false,
			success: function (response){
				var company = new Array();
				var i;
					
				if (response.status=="success"){
					$.each(response.data, function(key,value){
						if (key == 'password'){
							$("#password").val(value);
							$("#conf_password").val(value);
						} else if (key == 'c_contacts'){
							$("#phone_no_1").val(value);
						} else if (key == 'secondary_contacts'){
							$("#phone_no_2").val(value);
						} else if (key == 'c_email'){
							$("#email_1").val(value);
						} else if (key == 'secondary_email'){
							$("#email_2").val(value);
						} else if (key == 'uname'){
							$("#uname").val(value);
						} else if (key == 'active_date_to'){
							$("#active_till").val(value);
						} else if (key == 'id'){
							$("#id").val(value);
						} else if (key == 'c_name'){
							$("#c_name").val(value);
						} else if (key == 'user_type'){
							$("#user_type").val(value);
							$("#user_type").trigger('change');
						} else if (key == 'user_tz'){
							$("#user_tz").val(value);
							$("#user_tz").trigger('change');
						}
						else if (key == 'department_id'){
							$("#department_id").val(value);
							$("#department_id").trigger('change');
						}
						
					});
					for (i = 0; i<response.company.length; i++){
						company.push(response.company[i]['company_id']+','+response.company[i]['plant_id']);
					}
					$('#company').val(company).trigger('change');
					$('#add_edit_user_modal').modal('show');
				} else {
					Display_msg(response.message,response.status);
					return false;
				}
			},
			error: function(e){
				alert(e);
			}
		});
	}

	function send_email_otp(id){
		var email = $("#email_"+id).val();
		$.ajax({
			url: '<?php echo base_url("auctioneer/sub_user/send_email_otp");?>',
			type:"post",
			data: {'email': email},
			dataType:'json',
			success: function (response) {
				if (response.status == 'success'){
					$("#otp_"+id).show();
					$("#email_msg_"+id).html("<span class='text-success'>"+response.message+"</span>");
				} else {
					$("#email_msg_"+id).html("<span class='text-danger'>"+response.message+"</span>");
				}
			},
			error: function(){
				$("#email_msg_"+id).html("<span class='text-danger'>Problem in sending mail please try again</span>");
			}
		});
	}

	function clearForm()
	{
		$('#company').val('').trigger('change');
		$("#id").val('');
		$("#c_name").val('');
		$("#uname").val('');
		$("#password").val('');
		$("#conf_password").val('');
		$("#phone_no_1").val('');
		$("#mobile_otp_1 ").val('');
		$("#mobile_otp_1 ").hide();
		$("#mobile_msg_1").html('');
		$("#phone_no_2").val('');
		$("#mobile_otp_2 ").hide();
		$("#mobile_otp_2 ").val('');
		$("#mobile_msg_2").html('');
		$("#email_1").val('');
		$("#otp_1 ").val('');
		$("#otp_1 ").hide('');
		$("#email_msg_1 ").html('');
		$("#email_2").val('');
		$("#email_msg_2").html('');
		$("#active_till ").val('');
	}

	function btn_tgl() {
		$('#add_btn').hide() ;
	}
	
</script>