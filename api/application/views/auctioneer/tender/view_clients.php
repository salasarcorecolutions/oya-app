<!-- In order to let it work in CI, need to fix it to only get the viwes -->

<!--CONTENT CONTAINER-->
<!--===================================================-->
<div id="content-container">
	<!--Page Title-->
	<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
	<div id="page-head">
	<div id="page-title">
		<h1 class="page-header text-overflow">E-Tender Clients</h1>
	</div>
	<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
	<!--End page title-->


	<!--Breadcrumb-->
	<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
	<ol class="breadcrumb">
		<li><a href="<?php echo base_url('dashboard');?>">Home</a></li>
		<li class="active"><a href="<?php echo base_url('auctioneer/tender/index');?>">E-Tender</a></li>
		<li class="active">E-Tender Clients</li>
	</ol>
	</div>
	<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
	<!--End breadcrumb-->
	<input type="hidden" id="t11" name="auctionId">
	<div id="page-content">	
	<div class="float_btn">
		<a class="btn btn-primary btn-icon btn-circle add-tooltip"  data-placement="top" data-toggle="tooltip" data-original-title="Back" href='<?php echo base_url('auctioneer/tender/index'); ?>' ><i class="fa fa-arrow-left" aria-hidden="true"></i></a>
	</div>	
		<div class="panel">
			<div class="panel-heading">
				<h3 class="panel-title">E-Tender Clients</h3>
			</div>
			
			<div class="panel-body">
				<div class="pad-btm form-inline">
					<div class="row">
						<div class="col-sm-12 table-toolbar-right">	
							<?php
                            if ($this->common_model->check_permission("Tender Client Add")) {
                                ?><a class="btn btn-primary btn-labeled fa fa-plus" href="<?php echo base_url('auctioneer/tender/add_edit_clients/'.$tid); ?>" title="Add Client" name="btn_add_client" id="btn_add_client">Add Client</a><?php
                            }
                            ?>
						</div>
					</div>
				</div>
				<div class="table-responsive">
					<table class="table table-striped table-bordered table-hover no-footer dtr-inline ">
						<thead>
							<tr>
								<th style='width:3%;'>SR</th>
								<th style='width:20%;'>BIDDER</th>
								<th>CMD/EMD AMOUNT</th>
								<th>BANK NAME</th>
								<th>REF NO</th>
								<th>DD DATE</th>
								<th>TOTAL LIMIT	</th>
								<th>DATE UPDATED</th>
								<th style='width:30%;'>LOTS</th>
								<th>ACTION</th>
							</tr>
						</thead>
						<tbody>
						<?php
                        
                        if (! empty($client_list)) {
                            $sr = 1;
                            $edit_permission = false;
                            $delete_permission = false;
                            if ($this->common_model->check_permission("Tender Client Edit")) {
                                $edit_permission = true;
                            }
                            if ($this->common_model->check_permission("Tender Client Delete")) {
                                $delete_permission = true;
                            }
                            foreach ($client_list as $single_client) {
                                $action = '';
                                
                                if ($edit_permission) {
                                    $action = '<a title="Edit Client" href="'.base_url('auctioneer/tender/add_edit_clients/'.$tid."/".$single_client['client_id']).'">Edit</a><br><br>';
                                }
                                if ($delete_permission) {
                                    $action .= '<a title="Delete Client" href="javascript:void(0)" onclick="delete_client(\''.$single_client['client_id'].'\',\''.$tid.'\')">Delete</a><br>';
                                } ?>
								<tr>
									<td><?php echo $sr++; ?></td>
									<td><?php echo $single_client['compname'].' ['.$single_client['conperson'].']'; ?></td>
									<td>
										<?php
                                        if ($single_client['emd_cmd_type'] == 'C') {
                                            echo "CMD : ".$single_client['cmdamt'];
                                        } else {
                                            echo "EMD : ".$single_client['emdamt'];
                                        } ?>
									</td>
									<td>
										<?php
                                        if ($single_client['emd_cmd_type'] == 'C') {
                                            echo $single_client['cmdbank'];
                                        } else {
                                            echo $single_client['emdbank'];
                                        } ?>
									</td>
									<td>
										<?php
                                        if ($single_client['emd_cmd_type'] == 'C') {
                                            echo $single_client['cmdddno'];
                                        } else {
                                            echo $single_client['emdddno'];
                                        } ?>
									</td>
									<td>
										<?php
                                        if ($single_client['emd_cmd_type'] == 'C') {
                                            echo $single_client['cmddate'];
                                        } else {
                                            echo $single_client['emddate'];
                                        } ?>
									</td>
									<td><?php echo $single_client['totallimit']; ?></td>
									<td><?php echo $single_client['date_added']; ?></td>
									<td><?php echo $single_client['alloted_lots']; ?></td>
									<td><?php echo $action; ?></td>
								</tr>							
							<?php
                            }
                        } else {
                            ?>
							<tr>
								<td colspan='10' align='center'>No Records Found</td>
							</tr>
							<?php
                        }
                        ?>
						</tbody>
					</table>
				</div>
			</div>	
		</div>
	</div>	
</div>

<!--===================================================-->
<!--END CONTENT CONTAINER-->
<script>

function delete_client(id,tid)
{	
	if(id > 0)
	{
		var ans = confirm('Are you sure you want to edit this client?');
		if(ans)
		{		
			$.ajax(
			{
				url: "<?php echo base_url('auctioneer/tender/delete_auction_client'); ?>",
				type: "POST",
				dataType:"json",
				cache: false,
				clearForm: false,
				data:{"client_id":id,'tid':tid},
				success:function(response)
				{
					if(response.status == "success")
					{
						Display_msg(response.message,response.status);
						setTimeout(function()
						{
							window.location.reload();
						}, 3000);
					}
					else
					{
						Display_msg(response.message,response.status);
						return false;
					}
				}
			});
		}
	}
	else
	{
		Display_msg('Please Select Auction.','error');
		return false;
	}
}

</script>