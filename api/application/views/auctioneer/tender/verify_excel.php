<!-- In order to let it work in CI, need to fix it to only get the viwes -->
<style>
.table>tbody>tr>td, .table>tbody>tr>th, .table>tfoot>tr>td, .table>tfoot>tr>th, .table>thead>tr>td, .table>thead>tr>th {
    padding: 2px !important;
}
</style>
<!--CONTENT CONTAINER-->
<!--===================================================-->
<div id="content-container">
	<!--Page Title-->
	<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
	<div id="page-title">
		<h1 class="page-header text-overflow">E-Tender Lots</h1>
	</div>
	<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
	<!--End page title-->


	<!--Breadcrumb-->
	<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
	<ol class="breadcrumb">
		<li><a href="<?php echo base_url('dashboard');?>">Home</a></li>
		<li><a href="<?php echo base_url('auctioneer/tender/index');?>">E-Tender</a></li>
		<li class="active">E-Tender Lots</li>
	</ol>
	<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
	<!--End breadcrumb-->
	<input type="hidden" id="t11" name="auctionId">
	<div id="page-content">	
		<div class="panel">
			<div class="panel-heading">
				<h3 class="panel-title">E-Tender Lots</h3>
			</div>
			
			<div class="panel-body">
				<div class="table-responsive">
					<table class="table table-striped table-bordered table-hover no-footer dtr-inline">
						<thead>
							<tr>
								<th>Sale No</th>
								<th>Start Time</th>
								<th>End Time</th>
								<th>Particulars</th>
							</tr>
							<tr>
								<td><?php echo $auction_details['tender_id'] ?></td>
								<td><?php echo date('d-m-Y H:i:s', strtotime($auction_details['topen_dt'])) ?></td>
								<td><?php echo date('d-m-Y H:i:s', strtotime($auction_details['tclose_dt'])) ?></td>
								<td><?php echo $auction_details['particulars'] ?></td>
							</tr>
						</thead>
					</table>
				</div>
				
				<div class="table-responsive">
					<table class="table table-striped table-bordered table-hover no-footer dtr-inline">
						<thead>
							<tr>
							<?php
                                if (! empty($lot_headers)) {
                                    foreach ($lot_headers as $header_name) {
                                        ?>
										<th><?php echo $header_name; ?></th>
										<?php
                                    }
                                }
                            ?>
								
							</tr>
						</thead>
						<tbody>
							<?php							
                                $all_valid = true;
                                if ( ! empty($lot_details)) {
                                    /* This arrays are maintained as per the excel format */
                                    $required_field_array = array(0,1,2,3,4,5,6,7,9,10,15,16,17,18,19,26);
                                    $date_field_array = array(4,6);
                                    $yes_no_field_array = array(8,26);
                                    $val_greater_than_zero_array = array(10);
									$lotno = 1;
                                    foreach ($lot_details as $single_lot) {
                                        $col = 0;
                                        foreach ($single_lot as $key => $single_field) {
                                            /* Check Required Field */
                                            if (in_array($key, $required_field_array)) {
                                                if (trim($single_field) == '') {
                                                    $single_lot[$key] = '<span style="color:red;">'.trim($single_field).' This field is required</span>';
                                                    $all_valid = false;
                                                }
                                            }
                                            
                                            /* Check Date Field */
                                            if (in_array($key, $date_field_array)) {
                                                if ( ! is_date_valid($single_field)){
                                                    $single_lot[$key] = '<span style="color:red;">'.trim($single_field).'<br>Invalid Date</span>';
                                                    $all_valid = false;
                                                } else {
                                                    if ( ! isDateInRange(date('Y-m-d', strtotime($auction_details['topen_dt'])), date('Y-m-d', strtotime($auction_details['tclose_dt'])), $single_field)) {
                                                        $single_lot[$key] = '<span style="color:red;">'.trim($single_field).'<br>Invalid date range</span>';
                                                        $all_valid = false;
                                                    }
                                                }
                                            }

                                            /* Check Yes No Field */
                                            if (in_array($key, $yes_no_field_array)) {
                                                if ($single_field != 'Y' && $single_field != 'N') {
                                                    $single_lot[$key] = '<span style="color:red">'.trim($single_field).'<br>Invalid Value</span>';
                                                    $all_valid = false;
                                                }
                                            }

                                            /* Check Value > 0 */
                                            if (in_array($key, $val_greater_than_zero_array)) {
                                                if (trim($single_field) <= 0 or trim($single_field) == '') {
                                                    $single_lot[$key] = '<span style="color:red">'.trim($single_field).'<br>Invalid Value</span>';
                                                    $all_valid = false;
                                                }
                                            }
                                        } ?>
										<tr>
											<td align="center" nowrap><?php echo $lotno++; ?></td>	<!-- LOT NO -->
											<?php $col++;?>											
											<td align="center" nowrap><?php echo $single_lot[$col++]; ?></td>	<!-- VENDOR LOT NO -->
											<td><?php echo $single_lot[$col++]; ?></td>							<!-- MATERIAL CODE -->
											<td><?php echo $single_lot[$col++]; ?></td>							<!-- PLANT -->
											<td align="center" nowrap><?php echo $single_lot[$col++]; ?></td>	<!-- LOT OPEN DATE  -->
											<td align="center" nowrap><?php echo $single_lot[$col++]; ?></td>	<!-- LOT OPEN TIME -->
											<td align="center" nowrap><?php echo $single_lot[$col++]; ?></td>	<!-- LOT CLOSE DATE  -->
											<td align="center" nowrap><?php echo $single_lot[$col++]; ?></td>	<!-- LOT CLOSE TIME -->
											<td align="center" nowrap><?php echo $single_lot[$col++]; ?></td>	<!-- TIME BOUNDED -->
											<td align="center" nowrap><?php echo $single_lot[$col++]; ?></td>	<!-- DESCRIPTION -->
											<td align="center" nowrap><?php echo $single_lot[$col++]; ?></td>	<!-- MAX BID -->
											<td align="center" nowrap><?php echo $single_lot[$col++]; ?></td>	<!-- CMD -->
											<td align="center" nowrap><?php echo $single_lot[$col++]; ?></td>	<!-- START BID	 -->
											<td align="center" nowrap><?php echo $single_lot[$col++]; ?></td>	<!-- INCREMENT -->
											<td align="center" nowrap><?php echo $single_lot[$col++]; ?></td>	<!-- RP -->
											<td align="center" nowrap><?php echo $single_lot[$col++]; ?></td>	<!-- TOTAL QTY	 -->
											<td align="center" nowrap><?php echo $single_lot[$col++]; ?></td>	<!-- UNIT -->
											<td align="center" nowrap><?php echo $single_lot[$col++]; ?></td>	<!-- MIN BID QTY -->
											<td align="center" nowrap><?php echo $single_lot[$col++]; ?></td>	<!-- BID UNIT -->
											<td align="center" nowrap><?php echo $single_lot[$col++]; ?></td>	<!-- Currency  -->
											<td align="center" nowrap><?php echo $single_lot[$col++]; ?></td>	<!-- REMARS -->
											<td align="center" nowrap><?php echo $single_lot[$col++]; ?></td>	<!-- Excise Duty -->
											<td align="center" nowrap><?php echo $single_lot[$col++]; ?></td>	<!-- VAT -->
											<td align="center" nowrap><?php echo $single_lot[$col++]; ?></td>	<!-- CST -->
											<td align="center" nowrap><?php echo $single_lot[$col++]; ?></td>	<!-- TCS -->
											<td align="center" nowrap><?php echo $single_lot[$col++]; ?></td>	<!-- GST -->
											<td align="center" nowrap><?php echo $single_lot[$col++]; ?></td>	<!-- Is Compulsory -->
										<tr>
										<?php
                                    }
                                }
                            ?>
						</tbody>
						<?php
                        if ( ! empty($lot_details)) {
                            if ($all_valid) {
                                ?>
								<tr>
									<td colspan='<?php echo $col; ?>' align='center'>
										<form name='form_save_excel_lots' id='form_save_excel_lots' >
											<input type='hidden' id='tid' name='tid' value='<?php echo $tid; ?>' />
											<input type='submit' class='btn btn-primary' value='Save Lots' id='save_excel_export' name='save_excel_export' />
										</form>
									</td>
								</tr>
								<?php
                            }
                        }
                        ?>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>

<!--===================================================-->
<!--END CONTENT CONTAINER-->
<script>

$(function(){
	var vRules = {};
	var vMessages = {};
	$("#form_save_excel_lots").validate({
		rules: vRules,
		messages: vMessages,
		submitHandler: function(form){
			$(form).ajaxSubmit({
				url:"<?php echo base_url('auctioneer/tender/save_excel');?>",
				type: 'post',
				dataType:'json',
				cache: false,
				clearForm: true,
				success: function (response){
					if(response.status=="success"){
						Display_msg(response.message,response.status);
						setTimeout(function(){
							window.location.href="<?php echo base_url('auctioneer/tender/view_lots/'.$tid);?>";
						}, 3000);
					} else {
						Display_msg(response.message,response.status);
						return false;
					}
				}
			});
		}
	});

	$("#part_bid").change();
});
</script>
