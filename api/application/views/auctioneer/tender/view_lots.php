
<!--CONTENT CONTAINER-->
<!--===================================================-->
<div id="content-container">
	<!--Page Title-->
	<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
	<div id="page-head">
	<div id="page-title">
		<h1 class="page-header text-overflow">E-Tender Lots</h1>
	</div>
	<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
	<!--End page title-->


	<!--Breadcrumb-->
	<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
	<ol class="breadcrumb">
		<li><a href="<?php echo base_url('dashboard');?>">Home</a></li>
		<li><a href="<?php echo base_url('auctioneer/tender/index');?>">E-Tender</a></li>
		<li class="active">E-Tender Lots</li>
	</ol>
	</div>
	<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
	<!--End breadcrumb-->
	<input type="hidden" id="t11" name="auctionId">
	<div id="page-content">	
	<div class="float_btn">
		<a class="btn btn-primary btn-icon btn-circle add-tooltip" data-placement="top" data-toggle="tooltip" data-original-title="Back" href='<?php echo base_url('auctioneer/tender/index'); ?>' ><i class="fa fa-arrow-left" aria-hidden="true"></i></a>
	</div>
		<div class="panel">
			<div class="panel-heading">
				<h3 class="panel-title">E-Tender Lots</h3>
			</div>
			
			<div class="panel-body">
					<div class="table-responsive">
						<table class="table table-striped table-bordered table-hover no-footer dtr-inline">
							<thead>
								<tr>
									<th>Sale No</th>
									<th>Start Time</th>
									<th>End Time</th>
									<th>Particulars</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td><?php echo $auction_details['tender_id'] ?></td>
									<td><?php echo date('d-m-Y', strtotime($auction_details['topen_dt'])).' '.date('H:i:s',strtotime($auction_details['topen_time'])); ?></td>
									<td><?php echo date('d-m-Y', strtotime($auction_details['tclose_dt'])).' '.date('H:i:s',strtotime($auction_details['tclose_time'])); ?></td>
									<td><?php echo $auction_details['particulars'] ?></td>
								</tr>
							</tbody>
						</table>
					</div>
					<div class="pad-btm form-inline">
						<div class="row">
							<?php
                            if ($this->common_model->check_permission("Tender Lot Add")) {
								?>
								<a class="btn btn-success btn-labeled fa fa-plus" href="javascript:void(0);" title="Add Auction Lot" name="btn_add" id="btn_add" onclick="javascript:lot_actions('ADD')">Add</a>
								<?php
                            }
                            if ($this->common_model->check_permission("Tender Lot Edit")) {
								?>
								<a class="btn btn-primary btn-labeled fa fa-edit" href="javascript:void(0);" title="Edit Auction Lot" name="btn_edit" id="btn_edit" onclick="javascript:lot_actions('EDIT')">Edit</a>
								 <?php
                            }

                            if ($this->common_model->check_permission("Tender Update Lot Timings")) {
								?>
								<a class="btn btn-primary btn-labeled fa fa-clock-o" href="javascript:void(0);" title="Update Lot Timing" name="btn_lot_timing" id="btn_lot_timing" onclick="javascript:lot_actions('LOT_TIME_UPDATE')">Update Lot Timing</a>
								<?php
                            }
                            if ($this->common_model->check_permission("Tender Lot Add") && empty($lot_details)) {
								?>
									<a class="btn btn-primary btn-labeled fa fa-file-excel-o" href="javascript:void(0);" title="Upload Lot Excel" name="btn_upload_excel" id="btn_upload_excel" onclick="javascript:lot_actions('UPLOAD_EXCEL')">Upload Lot Excel</a>
								<?php
                            }
                            if ($this->common_model->check_permission("Tender Update Tax details")) {
								?>
									<a class="btn btn-primary btn-labeled fa fa-credit-card" href="javascript:void(0);" title="Update Lot Tax Details" name="btn_lot_details" id="btn_lot_details" onclick="javascript:lot_actions('LOT_TAX_UPDATE')">Update Lot Tax Details</a>
								<?php
                            }
                            if ($this->common_model->check_permission("Tender Lot Image Upload")) {
								?>
									<a class="btn btn-primary btn-labeled fa fa-picture-o" href="javascript:void(0);" title="Upload Lot Images" name="btn_upload_lot_images" onclick="javascript:lot_actions('UPLOAD_LOT_IMAGES')">Upload Lot Images</a>
								<?php
							}
							if ($this->common_model->check_permission("Tender Lot Delete")) {
								?>
									<a class="btn btn-danger btn-labeled fa fa-warning" href="javascript:void(0);" title="Delete Auction Lot" name="btn_delete" onclick="javascript:lot_actions('DELETE')">Delete</a>
								<?php
                            }
                            ?>
						</div>
					</div>
					<div class="table-responsive">
						<table class="table table-striped table-bordered table-hover no-footer dtr-inline">
							<thead>
								<tr>
									<th data-bSortable="false">#</th>
									<th>LOT NO</th>
									<th>MATERIAL CODE</th>
									<th>DESCRIPTION</th>
									<th>PLANT</th>
									<th>OPENING DATE</th>
									<th>CLOSING DATE</th>
									<th>TOTAL<BR>QTY</th>
									<th>BID<BR>QTY</th>
									<th>START<BR>PRICE</th>
									<th>INCR / DECR</th>
									<th>TIME<br>BOUNDED</th>
									<th>CMD</th>
								</tr>
							</thead>
							<tbody>
								<?php
	
								if ( ! empty($lot_details)){
									foreach ($lot_details as $single_lot){ ?>
										<tr>
											<td>
												<input type='radio' id='select_tender_lotno_<?php echo $single_lot['lotno']; ?>' name='select_tender_lotno' value='<?php echo $single_lot['lotno']; ?>' />
											</td>
											<td><?php echo $single_lot['lotno']; ?></td>
											<td><?php echo $single_lot['mat_code']; ?></td>
											<td><?php echo $single_lot['description']; ?></td>
											<td><?php echo $single_lot['plant']; ?></td>											
											<td nowrap><?php echo date('d-m-Y', strtotime($single_lot['lot_open_date']))."<br>".date('H:i:s', strtotime($single_lot['lot_open_time'])); ?></td>
											<td nowrap><?php echo date('d-m-Y', strtotime($single_lot['lot_closing_date']))."<br>".date('H:i:s', strtotime($single_lot['lot_closing_time'])); ?></td>
											<td><?php echo $single_lot['lot_qty'].' '.$single_lot['unit']; ?></td>
											<td><?php echo $single_lot['bid_qty'].' '.$single_lot['bid_unit']; ?></td>
											<td><?php echo $single_lot['start_bid']." ".$single_lot['currency']; ?></td>
											<td><?php echo $single_lot['minimum_incr_decr']." ".$single_lot['currency']; ?></td>
											<td><?php echo ($single_lot['time_bounded'] == 'Y') ? 'Yes' : 'No' ;?></td>
											<td><?php echo $single_lot['cmd']; ?></td>
										</tr> <?php
									}
								} else { ?>
									<tr>
										<td colspan = '16' class="text-center">No Records Found</td>
									</tr>
									<?php
								} ?>
							</tbody>
						</table>
					</div>
			</div>
				
			</div>	
		</div>
	</div>	
</div>

<!-- Lot Add Edit Modal -->
<style>
.form-group {
    margin-bottom: 5px !important;
}
</style>
<div id="lot_add_edit_modal" class="modal fade" >
	<div class="modal-dialog" style="width: 80% !important;margin: 30px auto;">
		<div class="modal-content">
			<form name="lot_add_edit_form" id="lot_add_edit_form" class='form-horizontal' method="post" >
				<input type='hidden' id='tid' name='tid' value='<?php echo $tid; ?>'/>
				<input type='hidden' id='tender_lot_action' name='tender_lot_action' value=''/>
				
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title" id="modal_header_text">Lot Add Edit</h4>
				</div>
				<div class="modal-body">
					<div class="form-group">
						<label class="col-sm-2 control-label" >E-TENDER SALE NO :</label>
						<div class="col-sm-4">
							<p class="text-muted" style="margin: 5px;"><?php echo $auction_details['tender_id'];?></p>	
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-2 control-label" >E-TENDER LOT NO :</label>
						<div class="col-sm-4">
							<input class="form-control" type="text" name="lotno" id="lotno" maxlength='5' readonly value=''>
						</div>
						<label class="col-sm-2 control-label" >MATERIAL CODE<span class="text-danger">*</span>:</label>
						<div class="col-sm-4">
							<input class="form-control" type="text" name="mat_code" id="mat_code" maxlength="50" value="" />
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-2 control-label" >LOT START DATE <span class="text-danger">*</span>:</label>
						<div class="col-sm-4">
							<input class="form-control" id="lot_open_date" value="" readonly="readonly" name="lot_open_date">
						</div>
						<label class="col-sm-2 control-label" >LOT START TIME <span class="text-danger">*</span>:</label>
						<div class="col-md-3">
							<input class="form-control" name="bid_start_time" type="text" id="bid_start_time" type="time" value='<?php echo ! empty($stime) ? $stime : ""; ?>' />
						</div>
						<div class="col-sm-1">&nbsp;</div>
					</div>
					<div class="form-group">
						<label class="col-sm-2 control-label" >LOT END DATE <span class="text-danger">*</span>:</label>
						<div class="col-sm-4">
							<input class="form-control" id="lot_closing_date" value="" readonly="readonly"  name="lot_closing_date">
						</div>
						<label class="col-sm-2 control-label" >LOT END TIME  <span class="text-danger">*</span>:</label>
						<div class="col-md-3">
							<input class="form-control" name="bid_end_time" type="text" id="bid_end_time" type="time"  value='<?php echo ! empty($etime) ? $etime : ""; ?>' />
						</div>
						<div class="col-sm-1">&nbsp;</div>
					</div>
					<div class="form-group">
						<label class="col-sm-2 control-label" >PRODUCT DESCRIPTION <span class="text-danger">*</span>:</label>
						<div class="col-sm-4">
							<textarea class="form-control" rows="4" name="description" id="description" cols="50"></textarea>
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-2 control-label" >PLANT <span class="text-danger">*</span>:</label>
						<div class="col-sm-4">
							<input class="form-control" type="text" name="plant" id="plant" maxlength="200" value=""/>
						</div>
						<label class="col-sm-2 control-label" >TIME BOUNDED ? <span class="text-danger">*</span>:</label>
						<div class="col-sm-4">
							<select class="form-control" name="time_bounded" id="time_bounded">
								<option value='N'>No</option>
								<option value='Y'>Yes</option>
							</select>
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-2 control-label" >MAX NO OF BID ACCEPTED<span class="text-danger">*</span>:</label>
						<div class="col-sm-4">
							<input class="form-control" type="text" name="max_no_of_bids" id="max_no_of_bids" size="20" value='1'>
						</div>
						<label class="col-sm-2 control-label" >CMD AMOUNT <span class="text-danger">*</span>:</label>
						<div class="col-sm-4">
							<input class="form-control" type="text" name="cmd" id="cmd" size="20" value=''>
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-2 control-label" >TOTAL QUANTITY <span class="text-danger">*</span>:</label>
						<div class="col-sm-4">
							<input class="form-control" type="text" name="lot_qty" id="lot_qty" size="20" value=''>
						</div>
						<label class="col-sm-2 control-label" >TOTAL UNIT <span class="text-danger">*</span>:</label>
						<div class="col-sm-4">
							<input class="form-control" type="text" name="unit" id="unit" size="10" value='' maxlength="50" >
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-2 control-label" >BID QUANTITY <span class="text-danger">*</span>:</label>
						<div class="col-sm-4">
							<input class="form-control" type="text" name="bid_qty" id="bid_qty" size="20" value=''>
						</div>
						<label class="col-sm-2 control-label" >BID UNIT <span class="text-danger">*</span>:</label>
						<div class="col-sm-4">
							<input class="form-control" type="text" name="bid_unit" id="bid_unit" size="10" value='' maxlength="50" >
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-2 control-label" >STARTING BID AMOUNT :</label>
						<div class="col-sm-4">
							<input class="form-control" type="text" name="start_bid" id="start_bid" size="20" value=''>
						</div>
						<label class="col-sm-2 control-label" >MIN INCR / DECR BY :</label>
						<div class="col-sm-4">
							<input class="form-control" type="text" name="minimum_incr_decr" id="minimum_incr_decr" size="10" value='' >
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-2 control-label" >CURRENCY <span class="text-danger">*</span>:</label>
						<div class="col-sm-4">
							<input class="form-control" type="text" name="currency" id="currency" value='' maxlength='3'>
						</div>
						<label class="col-sm-2 control-label" >REMARKS :</label>
						<div class="col-sm-4">
							<input class="form-control" type="text" name="remarks" id="remarks" value='' maxlength='500'>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<input class="btn btn-primary" type="submit" value='Submit' name="btn_lot_add_edit_submit" id='btn_lot_add_edit_submit' />
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				</div>
			</form>
		</div>
	</div>
</div>


<!-- Upload Excel -->
<div id="lot_upload_excel_modal" class="modal fade" >
	<div class="modal-dialog">
		<div class="modal-content">
			<form name="lot_upload_excel_form" id="lot_upload_excel_form" class='form-horizontal' method="post" enctype="multipart/form-data" >
				<input type='hidden' id='tid' name='tid' value='<?php echo $tid; ?>'/>
				
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title" id="modal_header_text">Upload Lot Excel</h4>
				</div>
				<div class="modal-body">					
					<div class="form-group">
						<label class="col-sm-5 control-label" >E-TENDER NO :</label>
						<div class="col-sm-6">
							<p class="text-muted" style="margin: 5px;"><?php echo $auction_details['tender_id'];?></p>	
						</div>
					</div>
					<?php
					
                    $s3 = new S3upload();
                    $lot_excel = $s3->getUrl('et_'.$tid.".xls", "lot_excel");
                    if (! empty($lot_excel)) {
                        if (is_file_exist($lot_excel)) {
                            ?>
							<div class="form-group">
								<label class="col-sm-5 control-label" >Uploaded File :</label>
								<div class="col-sm-6">
									<p class="text-muted" style="margin: 5px;">
										<a target="_blank" class='link' href="<?php echo base_url('auctioneer/tender/verify_excel/'.$tid); ?>">Verify Excel</a>
									</p>	
								</div>
							</div>
							<?php
                        } else {
                            ?>
							<div class="form-group">
								<label class="col-sm-5 control-label" >Sample File :</label>
								<div class="col-sm-6">
									<p class="text-muted" style="margin: 5px;">
										<a target="_blank" class='link btn btn-success btn-labeled fa fa-download' href="<?php echo base_url('auctioneer_assets/lot_excel/tender.xls'); ?>">Download Excel</a>
									</p>	
								</div>
							</div>
							<?php
                        }
                    } else {
                        ?>
						<div class="form-group">
							<label class="col-sm-5 control-label" >Sample File :</label>
							<div class="col-sm-6">
								<p class="text-muted" style="margin: 5px;">
									<a target="_blank" class='link btn btn-success btn-labeled fa fa-download' href="<?php echo base_url('auctioneer_assets/lot_excel/tender.xls'); ?>">Download Excel</a>
								</p>	
							</div>
						</div>
						<?php
                    }
                    ?>
					<div class="form-group">
						<label class="col-sm-5 control-label" >Upload Excel<span class="text-danger">*</span>:</label>
						<div class="col-sm-6">
							<input class="form-control" type="file" name="upload_file" id="upload_file"  value='' />
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<input class="btn btn-primary" type="submit" value='Submit' name="btn_lot_upload_excel_submit" id='btn_lot_upload_excel_submit' />
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				</div>
			</form>
		</div>
	</div>
</div>

<!--===================================================-->
<!--END CONTENT CONTAINER-->
<script>

var maxLotNo = '<?php echo ! empty($lot_details) ? $lot_details[sizeof($lot_details)-1]['lotno']+1 : 1; ?>';
var startDatee;
var endDatee;
$(document).ready(function() {

	$('#bid_start_time').mdtimepicker({
		format: 'hh:mm:ss',
		theme: 'blue'
	}); 
	$('#bid_end_time').mdtimepicker({
		format: 'hh:mm:ss',
		theme: 'blue'
	});
	

startDatee = '<?php echo date($auction_details['topen_dt']); ?>';
endDatee = '<?php echo date($auction_details['tclose_dt']); ?>';
    $("#lot_open_date").datepicker({
        format: 'yyyy-mm-dd',
		startDate: startDatee,
		endDate: endDatee
    });
    $("#lot_open_date").change(function(){
        startDatee = '"'+$("#lot_open_date").val()+'"';

		$("#lot_closing_date").val('');
		$("#lot_closing_date").datepicker('destroy');
        $("#lot_closing_date").datepicker({
            format: 'yyyy-mm-dd',
            startDate: startDatee,
			endDate: endDatee
        });
    });
});

function lot_actions(act){
	if(act == 'ADD')
	{
		clearAddLotModal();
		$("#lotno").val(maxLotNo);
		$('#tender_lot_action').val('Add');
		$('#lot_add_edit_modal').modal('show');
	}
	else if(act == 'EDIT')
	{
		var id = $("input[name=select_tender_lotno]:checked").val()
		if(id == '' || id == undefined)
		{
			Display_msg('Please Select Lot.','error');
			return false;
		}
		else
		{
			// Ajax to fetch data
			$('#lot_add_edit_form').trigger("reset");
			$('#tender_lot_action').val('Edit');
			$.ajax(
			{
				url:"<?php echo base_url('auctioneer/tender/get_single_lot_details');?>", 
				type: 'post',
				dataType:'json',
				data:{'lotno':id,'tid':$('#tid').val()},
				cache: false,
				clearForm: true,
				success: function (response)
				{
					if(response.status=="success")
					{
						$.each(response.data, function(key,value){
							console.log(key);
							if (key == 'proxy'){
								$('#'+key+'_'+value).prop("checked", true);
							} else if (key == 'lot_open_time'){
								$("#bid_start_time").val(value);
							} else if (key == 'lot_closing_time'){
								$("#bid_end_time").val(value);
							} else {
								$('#'+key).val(value);
							}
						}); 						
						$('#lot_add_edit_modal').modal('show');						
					}
					else
					{	
						Display_msg(response.message,response.status);
						return false;
					}
				}
			});
		}
	}
	else if(act == 'DELETE')
	{		
		var id = $("input[name=select_tender_lotno]:checked").val()
		if(id == '' || id == undefined)
		{
			Display_msg('Please Select Lot.','error');
			return false;
		}
		else
		{
			var ans = confirm('Are you sure you want to delete this lot?');
			if(ans)
			{
				$.ajax(
				{
					url:"<?php echo base_url('auctioneer/tender/delete_lot');?>", 
					type: 'post',
					dataType:'json',
					data:{'lotno':id,'tid':$('#tid').val()},
					cache: false,
					clearForm: true,
					success: function (response)
					{
						if(response.status=="success")
						{
							Display_msg(response.message,response.status);	
							setTimeout(function()
							{
								window.location.reload();
							}, 3000);
						}
						else
						{	
							Display_msg(response.message,response.status);
							return false;
						}
					}
				});
			}
			// Ajax to fetch data
			
		}
	}
	else if(act == 'UPLOAD_EXCEL')
	{
		$('#lot_upload_excel_modal').modal('show');
	}
	else if(act == 'LOT_TAX_UPDATE')
	{
		window.location.href = '<?php echo base_url("auctioneer/tender/update_tax_details/".$tid); ?>'
	}
	else if(act == 'LOT_TIME_UPDATE')
	{
		window.location.href = '<?php echo base_url("auctioneer/tender/update_lot_timings/".$tid); ?>'
	}
	else if (act == 'UPLOAD_LOT_IMAGES')
	{
		var id = $("input[name=select_tender_lotno]:checked").val()
		if(id == '' || id == undefined)
		{
			Display_msg('Please Select Lot.','error');
			return false;
		}
		else
		{
			window.location.href='<?php echo base_url("auctioneer/tender/upload_lot_image/".$tid."/"); ?>'+id;
		}
	}
	else
	{
		Display_msg('Please Select Lot.','error');
		return false;
	}
}

$(function(){
	$.validator.addMethod('compare_time', function (value, element, param) {
		return this.optional(element) || check_start_end_time_lot();
	}, "<p class='text-danger'>Enter Correct start time and end time</p>");
	function check_start_end_time_lot(){
		var lot_startTime = '"'+document.getElementById("bid_start_time").value+'"';
		var lot_endTime = '"'+document.getElementById("bid_end_time").value+'"';
		var lot_startDate = document.getElementById("lot_open_date").value;
		var lot_endDate = document.getElementById("lot_closing_date").value;
		if (lot_startDate == lot_endDate) {
			if (lot_startTime >= lot_endTime)
			{
				return false;
			}
			else
			{
				return true;
			}
		} else {
			return true;
		}
	}
	
	
	var vRules = {
		lotno:{required:true},
		mat_code:{required:true},
		description:{required:true},
		plant:{required:true},
		lot_open_date:{required:true},
		lot_closing_date:{required:true},
		bid_start_time: { required:true },
		bid_end_time: {
			required: true,
			compare_time: "#bid_start_time" 
		},
		cmd:{required:true},
		currency:{required:true},
		lot_qty:{required:true},
		unit:{required:true},
		bid_unit:{required:true},
		bid_qty:{required:true}
	};
	var vMessages = {
		lotno:{required:"<p class='text-danger'>Please enter tender lot no.</p>"},
		mat_code:{required:"<p class='text-danger'>Please enter material code.</p>"},
		description:{required:"<p class='text-danger'>Please enter product description.</p>"},
		plant:{required:"<p class='text-danger'>Please enter the plant name.</p>"},
		lot_open_date:{required:"<p class='text-danger'>Please etner lot opening date.</p>"},
		lot_closing_date:{required:"<p class='text-danger'>Please enter lot closing date.</p>"},
		bid_start_time: { required:"<p class='text-danger'>Please Enter Start Time </p>" },
		bid_end_time: { required:"<p class='text-danger'>Please Enter End Time</p>" },
		cmd:{required:"<p class='text-danger'>Please enter the cmd amount</p>"},
		currency:{required:"<p class='text-danger'>Please enter the currency</p>"},
		lot_qty:{required:"<p class='text-danger'>Please Enter The Total Quantity</p>"},
		unit:{required:"<p class='text-danger'>Please enter lot unit</p>"},
		bid_unit:{required:"<p class='text-danger'>Please enter lot unit</p>"},
		bid_qty:{required:"<p class='text-danger'>Please Enter The BID QUANTITY </p>"},
		
	};
	$("#lot_add_edit_form").validate(
	{
		rules: vRules,
		messages: vMessages,
		submitHandler: function(form)
		{
			$(form).ajaxSubmit(
			{
				url:"<?php echo base_url('auctioneer/tender/addedit_lots/');?>",
				type: 'post',
				dataType:'json',
				cache: false,
				clearForm: true,
				success: function (response)
				{
					if(response.status=="success")
					{
						Display_msg(response.message,response.status);
						
						$('#lot_add_edit_modal').modal('hide');
						setTimeout(function()
						{
							window.location.reload();
						}, 3000);
					}
					else
					{	
						Display_msg(response.message,response.status);
						return false;
					}
				}
			});
		}
	});
	
	
	/* Upload Lot Excel */
	var vRules2 = {
		upload_excel:{required:true}
	};
	var vMessages2 = {
		upload_excel:{required:"<p class='text-danger'>Please Select Excel File</p>"}
	};
	$("#lot_upload_excel_form").validate(
	{
		rules: vRules2,
		messages: vMessages2,
		submitHandler: function(form)
		{
			$(form).ajaxSubmit(
			{
				url:"<?php echo base_url('auctioneer/tender/upload_excel/');?>",
				type: 'post',
				dataType:'json',
				cache: false,
				clearForm: true,
				success: function (response)
				{
					if(response.status=="success")
					{
						Display_msg(response.message,response.status);
						
						$('#lot_upload_excel_modal').modal('hide');
						setTimeout(function()
						{
							window.location.href='<?php echo base_url("auctioneer/tender/verify_excel/".$tid)?>';
						}, 3000);
					}
					else
					{	
						Display_msg(response.message,response.status);
						return false;
					}
				}
			});
		}
	})
});

function clearAddLotModal()
{
	$("#lotno").val('');
	$("#mat_code").val('');
	$("#description").val('');
	$("#plant").val('');
	$("#lot_open_date").val('');
	$("#lot_closing_date").val('');
	$("#bid_start_time").val('');
	$("#bid_end_time").val('');
	$("#cmd").val('');
	$("#currency").val('');
	$("#lot_qty").val('');
	$("#unit").val('');
	$("#bid_unit").val('');
	$("#start_bid").val('');
	$("#bid_qty").val('');
	$("#minimum_incr_decr").val('');
	$("#max_no_of_bids").val('');
	$("#remarks").val('');
}


</script>
