<?php $this->load->view('auctioneer/auc_report_header')?>
<div class="panel panel-primary">
	<div class="panel-heading">
		<h3 class="panel-title">
		<strong>PRE BID REPORT FOR TENDER NO :  <?php echo $common_auction_details['saleno']; ?> </strong>
		</h3>
	</div>
	<div class="panel-body">
		
<page>
   <page_footer>
        <table style='width: 100%;'>
			<tr>
				<td align='left' style='float:left;width:200px;'>
			
				<?php echo (new DateTime(null, new DateTimeZone($_SESSION['user_tz'])))->format('D dS \of M Y h:i:s A'); ?>
				
				</td>
				<td align='right' style='float:right;width:545px;'>
				<span style='font-size:20px;color:#2a5395;'><strong><?php echo PRODUCT ?></strong></span>
				</td>
			</tr>
		</table>
    </page_footer>
	
	<div class="table-responsive">
		<table class="table table-striped table-bordered table-hover no-footer dtr-inline">
			<tbody>
			
			<tr>
				<td colspan='2' align='center' >
					<strong><?=strtoupper($common_auction_details['vendor_name'])?></strong>
				</td>
		
				<td colspan='2' align='center' >
					<strong>TENDER DETAILS</strong>
				</td>
			</tr>
			</tbody>		
			</table>
			</div>		
			<div class="table-responsive">
		<table class="table table-striped table-bordered table-hover no-footer dtr-inline">
			<thead>
				<tr>
					<th>Tender No</th>
					<th>Tender Commencement Date</th>
					<th>Tender Closing Date</th>
					<th>Particulars</th>
					
					<th>No. of Lots</th>
				</tr>
			</thead>	
		<tbody>
			
			<tr>
				<td align='left'>
					<?php echo $common_auction_details['saleno']?>
				</td>
		
				<td align='left'>
					<?php echo $common_auction_details['tcdate'].$common_auction_details['tctime']?>
				</td>
			
				<td align='left'>
					<?php echo $common_auction_details['edt']?>
				</td>
		
				<td align='left'>
					<?php echo $common_auction_details['particulars']?>
				</td>
			
				<td align='left'>
					<?php echo $common_auction_details['no_of_lots']?>
				</td>
			</tr>
		</tbody>		
	</table>
	</div>
	
	<div class="table-responsive">
		<table class="table table-striped table-bordered table-hover no-footer dtr-inline">
			<thead>
			<tr>
				<th align='center' class='wrappable' style='font-weight:bold;' >SR. NO.</th>
				<th align='center' class='wrappable' style='font-weight:bold;' >Material Code</th>
				<th align='center' class='wrappable' style='font-weight:bold;' >Description</th>
				<th align='center' class='wrappable' style='font-weight:bold;' >Plant</th>
				<th align='center' class='wrappable' style='font-weight:bold;' >Total Qty</th>
				<th align='center' class='wrappable' style='font-weight:bold;' >Bid Details</th>
			</tr>
		</thead>
		<tbody>	
			<?php
			
				if (!empty($pre_bid))
				{
					$I = 1;
					foreach($pre_bid['auction_details'] as $key=>$value)
					{
						
					?>
						
						<tr>
							<td align='center' class='wrappable' style='width:20px;'>
								<?php echo $I++;?>
							</td>
							<td align='center' class='wrappable' style='width:50px;'>
								<?php echo $value['mat_code'];?>
							</td>
							<td align='center' class='wrappable' style='width:153px;'>
								<?php echo $value['description'];?>
							</td>
							<td align='center' class='wrappable' style='width:60px;'>
								<?php echo $value['plant'];?>
							</td>
							<td align='center' class='wrappable' style='width:60px;'>
								<?php echo $value['lot_qty'].$value['lot_unit'];?>
							</td>
							<td align='center' style='width:362px;' class='wrappable' >
								<?php 
								
									if (isset($value['lot']) )
									{
								?>
								
									<table border='0.5' cellspacing='0' cellpadding='0'  >
											<thead>
												<tr>
													<th align='center' class='wrappable' style='font-weight:bold;width:140px;' bgcolor="#e0e0e0">Party Name</th>
													<th align='center' class='wrappable' style='font-weight:bold;width:60px;' bgcolor="#e0e0e0">Bid Amount</th>
													<th align='center' class='wrappable' style='font-weight:bold;width:60px;' bgcolor="#e0e0e0">Bid Qty</th>
													<th align='center' class='wrappable' style='font-weight:bold;width:80px;' bgcolor="#e0e0e0">IP</th>
												</tr>
											</thead>
											<tbody>
											<?php
													foreach($value['lot'] as $k=>$v)
													{
														if($v['amount'] > 0)
														{ 
													?>
														<tr>
															<td align='center' class='wrappable' style='width:140px;' ><?=$v['compname']?></td>
															<td align='center' class='wrappable' style='width:60px;' ><?=$v['amount']?></td>
															<td align='center' class='wrappable' style='width:60px;' ><?=$v['quantity']?></td>
															<td align='center' class='wrappable' style='width:80px;' ><?=$v['ip']?></td>
														</tr>
													
													<?php
														}
										
													}
												?>
												
												
											</tbody>
										</table>
									<?php			
									} else {
										?>No data is available <?php
									}
								?>
							</td>
						</tr>
					<?php	
					}
					
				}
			
			?>
		</tbody>
	</table>
			</div>
</page>
</div>
</div>	
	<footer id="footer">
		<div class="hide-fixed pull-right pad-rgt">&#0169; <?php echo date("Y");?> A One Salasar</div>
		<p class="pad-lft">&nbsp;</p>
	</footer>
</body>
</html>