<?php $this->load->view('auctioneer/auc_report_header')?>
<div class="panel panel-primary">
	<div class="panel-heading">
		<h3 class="panel-title">	<strong>BID SUMMARY FOR TENDER NO :  <?php echo $common_auction_details['saleno']; ?> </strong></h3>
	</div>
	<div class="panel-body">
		
<page>
   <page_footer>
        <table style='width: 100%;'>
			<tr>
				<td align='left' style='float:left;width:200px;'>
			
				<?php echo (new DateTime(null, new DateTimeZone($_SESSION['user_tz'])))->format('D dS \of M Y h:i:s A'); ?>
				
				</td>
				<td align='right' style='float:right;width:545px;'>
				<span style='font-size:20px;color:#2a5395;'><strong><?php echo PRODUCT ?></strong></span>
				</td>
			</tr>
		</table>
    </page_footer>
	
	<div class="table-responsive">
		<table class="table table-striped table-bordered table-hover no-footer dtr-inline">
			<tbody>
			
			<tr>
				<td colspan='2' align='center' >
					<strong><?=strtoupper($common_auction_details['vendor_name'])?></strong>
				</td>
		
				<td colspan='2' align='center' >
					<strong>TENDER DETAILS</strong>
				</td>
			</tr>
			</tbody>		
			</table>
			</div>		
			<div class="table-responsive">
		<table class="table table-striped table-bordered table-hover no-footer dtr-inline">
			<thead>
				<tr>
					<th>Tender No</th>
					<th>Tender Commencement Date</th>
					<th>Tender Closing Date</th>
					<th>Particulars</th>
					
					<th>No. of Lots</th>
				</tr>
			</thead>	
		<tbody>
			
			<tr>
				<td align='left'>
					<?php echo $common_auction_details['saleno']?>
				</td>
		
				<td align='left'>
					<?php echo $common_auction_details['tcdate'].$common_auction_details['tctime']?>
				</td>
			
				<td align='left'>
					<?php echo $common_auction_details['edt']?>
				</td>
		
				<td align='left'>
					<?php echo $common_auction_details['particulars']?>
				</td>
			
				<td align='left'>
					<?php echo $common_auction_details['no_of_lots']?>
				</td>
			</tr>
		</tbody>		
	</table>
	</div>
	
	<div class="table-responsive">
		<table class="table table-striped table-bordered table-hover no-footer dtr-inline">
			<thead>
			<tr>
				<th align='center'>SR. No</th>
				<th align='center'>Material Code</th>
				<th align='center'>Plant</th>
				<th align='center'>Total Qty</th>
				<th align='center'>Description</th>
				<th align='center'>H1 Bidder</th>
				<th align='center'>H1 Amount</th>
				<th align='center'>H2 Bidder</th>
				<th align='center'>H2 Amount</th>
				<th align='center'>H3 Bidder</th>
				<th align='center'>H3 Amount</th>				
			</tr>
		</thead>
		<tbody>	
			<?php 
				if ( ! empty($bid_summary))
				{
					$i = 1;
					foreach($bid_summary as $key=>$value)
					{
						?>
						<tr>
						<td align='center' class='wrappable' style="width:20px;">
							<?php echo $i++?>
						</td>
						<td align='center' class='wrappable' style="width:40px;">
							<?php echo $value['mat_code'];?>
						</td>
						<td align='center' class='wrappable' style="width:50px;">
							<?php echo $value['plant'];?>
						</td>
						<td align='center' class='wrappable' style="width:50px;">
							<?php echo $value['tq'];?>
						</td>
						<td align='center' class='wrappable' style="width:100px;">
							<?php echo $value['prod'];?>
						</td>
						<td align='center' class='wrappable' style="width:80px;">
							<?php
							if (isset($value['lot_details'][0]['cname']))
							{
								echo ! empty($value['lot_details'][0]['cname']) ? $value['lot_details'][0]['cname'] : '';
							}
							?>
						</td>
						<td align='center' class='wrappable' style="width:80px;">
							<?php 
							if (isset($value['lot_details'][0]['amt']))
							{
								echo ! empty($value['lot_details'][0]['amt']) ? $value['lot_details'][0]['amt'] : '';
							}
							?>
						</td>
						<td align='center' class='wrappable' style="width:80px;">
							<?php 
							if (isset($value['lot_details'][1]['cname']))
							{
								echo ! empty($value['lot_details'][1]['cname']) ? $value['lot_details'][1]['cname'] : '';
							}
							?>
						</td>
						<td align='center' class='wrappable' style="width:60px;">
							<?php 
							if (isset($value['lot_details'][1]['amt']))
							{
								echo ! empty($value['lot_details'][1]['amt']) ? $value['lot_details'][1]['amt'] : '';
							}
							?>
						</td>
						<td align='center' class='wrappable' style="width:60px;">
							<?php 
							if (isset($value['lot_details'][2]['cname']))
							{
								echo ! empty($value['lot_details'][2]['cname']) ? $value['lot_details'][1]['cname'] : '';
							}
							?>
						</td>
						<td align='center' class='wrappable' style="width:57px;" >
							<?php 
							if (isset($value['lot_details'][2]['amt']))
							{
								echo ! empty($value['lot_details'][2]['amt']) ? $value['lot_details'][1]['amt'] : '';
							}
							?>
						</td>
						</tr>

						<?php
						
					}	
				} else {
					?> <tr><td colspan="11" style="width: 735px;" align='center'>No data is available</td></tr> <?php
				}
			
			?>
		</tbody>
	</table>
	</div>
</page>
		
	</div>
</div>	
	<footer id="footer">
		<div class="hide-fixed pull-right pad-rgt">&#0169; <?php echo date("Y");?> A One Salasar</div>
		<p class="pad-lft">&nbsp;</p>
	</footer>
</body>
</html>