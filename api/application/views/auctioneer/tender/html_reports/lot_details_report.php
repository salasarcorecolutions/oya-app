<?php $this->load->view('auctioneer/auc_report_header')?>
<div class="panel panel-primary">
	<div class="panel-heading">
		<h3 class="panel-title">
		<strong>LOT DETAILS FOR TENDER NO :  <?php echo $common_auction_details['saleno']; ?> </strong>
		</h3>
	</div>
	<div class="panel-body">
		
<page>
   <page_footer>
        <table style='width: 100%;'>
			<tr>
				<td align='left' style='float:left;width:200px;'>
			
				<?php echo (new DateTime(null, new DateTimeZone($_SESSION['user_tz'])))->format('D dS \of M Y h:i:s A'); ?>
				
				</td>
				<td align='right' style='float:right;width:545px;'>
				<span style='font-size:20px;color:#2a5395;'><strong><?php echo PRODUCT ?></strong></span>
				</td>
			</tr>
		</table>
    </page_footer>
	
	<div class="table-responsive">
		<table class="table table-striped table-bordered table-hover no-footer dtr-inline">
			<tbody>
			
			<tr>
				<td colspan='2' align='center' >
					<strong><?=strtoupper($common_auction_details['vendor_name'])?></strong>
				</td>
		
				<td colspan='2' align='center' >
					<strong>TENDER DETAILS</strong>
				</td>
			</tr>
			</tbody>		
			</table>
			</div>		
			<div class="table-responsive">
		<table class="table table-striped table-bordered table-hover no-footer dtr-inline">
			<thead>
				<tr>
					<th>Tender No</th>
					<th>Tender Commencement Date</th>
					<th>Tender Closing Date</th>
					<th>Particulars</th>
					
					<th>No. of Lots</th>
				</tr>
			</thead>	
		<tbody>
			
			<tr>
				<td align='left'>
					<?php echo $common_auction_details['saleno']?>
				</td>
		
				<td align='left'>
					<?php echo $common_auction_details['tcdate'].$common_auction_details['tctime']?>
				</td>
			
				<td align='left'>
					<?php echo $common_auction_details['edt']?>
				</td>
		
				<td align='left'>
					<?php echo $common_auction_details['particulars']?>
				</td>
			
				<td align='left'>
					<?php echo $common_auction_details['no_of_lots']?>
				</td>
			</tr>
		</tbody>		
	</table>
	</div>
	
	<div class="table-responsive">
		<table class="table table-striped table-bordered table-hover no-footer dtr-inline">
			<thead>
			<tr>
				<th align='center' class='wrappable'   style='font-weight:bold;' >LOT NO.</th>
				<th align='center' class='wrappable'  style='font-weight:bold;' >MATERIAL CODE</th>
				<th align='center' class='wrappable'  style='font-weight:bold;' >OPENING DATETIME</th>
				<th align='center' class='wrappable'  style='font-weight:bold;' >CLOSING DATETIME</th>
				<th align='center' class='wrappable'  style='font-weight:bold;' >DESCRIPTION</th>
				<th align='center' class='wrappable'  style='font-weight:bold;' >RP</th>
				<th align='center' class='wrappable'  style='font-weight:bold;' >PLANT</th>
				<th align='center' class='wrappable'  style='font-weight:bold;' >MAX. NO. OF BID</th>
				<th align='center' class='wrappable'  style='font-weight:bold;' >TIME BOUNDED?</th>
				<th align='center' class='wrappable'  style='font-weight:bold;' >INCR / DECR</th>
				<th align='center' class='wrappable'  style='font-weight:bold;'>CMD</th>				
			</tr>
		</thead>
		<tbody>	
			<?php 
				if ( ! empty($lot_details))
				{
					$i = 1;
					foreach($lot_details as $key=>$value)
					{
			?>
						<tr>	
						<td class='wrappable' align='center' style='width:40px;'>
							<?php echo $value['lotno']; ?>
						</td>
						<td class='wrappable' align='center' style='width:40px;'>
							<?php echo $value['mat_code']; ?>
						</td>
						<td class='wrappable' align='center' style='width:80px;'>
							<?php echo $value['lot_open_date']; ?> Hrs.
						</td>
						<td class='wrappable' align='center' style='width:88px;'>
							<?php echo $value['lot_closing_date']; ?> Hrs.
						</td>
						<td class='wrappable' align='center' style='width:60px;' >
							<?php echo $value['description']; ?>
						</td>
						<td class='wrappable' align='center' style='width:60px;'>
							<?php echo $value['rp']; ?>
						</td>
						<td class='wrappable' align='center' style='width:60px;'>
							&nbsp;<?php echo $value['plant']; ?>
						</td>
						<td class='wrappable' align='center' style='width:60px;'>
							<?php if($value['max_no_of_bids']==0) echo "Unlimited"; else echo $value['max_no_of_bids']; ?>
						</td>
						<td class='wrappable' align='center' style='width:60px;'>
							<?php 
								if($value['time_bounded'] == 'Y')
								{
									echo 'Yes';
								}
								else
								{
									echo 'No';
								}
							?>
						</td>
						<td class='wrappable' align='center' style='width:60px;'>
							<?php echo $value['minimum_incr_decr']; ?>
						</td>
						<td class='wrappable' align='center' style='width:60px;'>
							<?php echo $value['cmd']; ?>
						</td>
						</tr>

			<?php
						
					}	
				} else {
					?> <tr><td colspan="11" style="width: 735px;" align='center'>No data is available</td></tr> <?php
				}
			
			?>
		</tbody>
	</table>
	</div>
</page>
</div>
</div>	
	<footer id="footer">
		<div class="hide-fixed pull-right pad-rgt">&#0169; <?php echo date("Y");?> A One Salasar</div>
		<p class="pad-lft">&nbsp;</p>
	</footer>
</body>
</html>