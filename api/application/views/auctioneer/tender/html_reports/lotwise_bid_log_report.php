<?php $this->load->view('auctioneer/auc_report_header')?>
<div class="panel panel-primary">
	<div class="panel-heading">
		<h3 class="panel-title">
		<strong>LOTWISE BID LOG FOR TENDER NO :  <?php echo $common_auction_details['saleno']; ?> </strong>
		</h3>
	</div>
	<div class="panel-body">
		
<page>
   <page_footer>
        <table style='width: 100%;'>
			<tr>
				<td align='left' style='float:left;width:200px;'>
			
				<?php echo (new DateTime(null, new DateTimeZone($_SESSION['user_tz'])))->format('D dS \of M Y h:i:s A'); ?>
				
				</td>
				<td align='right' style='float:right;width:545px;'>
				<span style='font-size:20px;color:#2a5395;'><strong><?php echo PRODUCT ?></strong></span>
				</td>
			</tr>
		</table>
    </page_footer>
	
	<div class="table-responsive">
		<table class="table table-striped table-bordered table-hover no-footer dtr-inline">
			<tbody>
			
			<tr>
				<td colspan='2' align='center' >
					<strong><?=strtoupper($common_auction_details['vendor_name'])?></strong>
				</td>
		
				<td colspan='2' align='center' >
					<strong>TENDER DETAILS</strong>
				</td>
			</tr>
			</tbody>		
			</table>
			</div>		
			<div class="table-responsive">
		<table class="table table-striped table-bordered table-hover no-footer dtr-inline">
			<thead>
				<tr>
					<th>Tender No</th>
					<th>Tender Commencement Date</th>
					<th>Tender Closing Date</th>
					<th>Particulars</th>
					
					<th>No. of Lots</th>
				</tr>
			</thead>	
		<tbody>
			
			<tr>
				<td align='left'>
					<?php echo $common_auction_details['saleno']?>
				</td>
		
				<td align='left'>
					<?php echo $common_auction_details['tcdate'].$common_auction_details['tctime']?>
				</td>
			
				<td align='left'>
					<?php echo $common_auction_details['edt']?>
				</td>
		
				<td align='left'>
					<?php echo $common_auction_details['particulars']?>
				</td>
			
				<td align='left'>
					<?php echo $common_auction_details['no_of_lots']?>
				</td>
			</tr>
		</tbody>		
	</table>
	</div>
	
	

	<?php

		if (!empty($lotwise_bid_log))
		{
			foreach ($lotwise_bid_log as $key=>$value)
			{

				?>
				<div class="table-responsive">
		<table class="table table-striped table-bordered table-hover no-footer dtr-inline">
			<thead>
					<tr>
						<th align='center' class='wrappable' colspan="6"  >
							<b>Lot No:</b>  <?php echo $value['lotno'] ;?>   <br> <b>Product:</b><?php echo $value['prod'];?>
						</th>
					</tr>
				</thead>
				<tbody>	
				<tr>
					<td align='center' class='wrappable' ><b>Start Time : </b> <br> <?php echo $value['st'];?> </td>
					<td align='center' class='wrappable' ><b>End Time : </b> <br> <?php echo $value['et'];?></td>
					<td align='center' class='wrappable'  ><b>Material:  </b> <br><?php echo $value['mat_code'];?></td>
					<td align='center' class='wrappable' style=''><strong>Bid Type :<br></strong><?php if($value['bid_type']=="I") echo "Incremental"; else echo "Decremental";?></td>
					<td align='center' class='wrappable' style=''><strong>Min Increment :<br></strong><?php echo $value['tq'];?></td>
					<td align='center' class='wrappable' style=''><strong>Lot Qty. :<br></strong><?php echo $value['iv'];?></td>
					
					
				</tr>
				<tr>
					<td align='center' class='wrappable' style='font-weight:bold;width:170px;'>BIDDER NAME</td>
					<td align='center' class='wrappable' style='font-weight:bold;width:115px;'>Date Time</td>
					<td align='center' class='wrappable' style='font-weight:bold;width:130px;'>Bid Amount</td>
					<td align='center' class='wrappable' style='font-weight:bold;width:130px;'>Bid Qty</td>
					<td align='center' class='wrappable' style='font-weight:bold;width:75px;'>Type</td>
					<td align='center' class='wrappable' style='font-weight:bold;width:85px;'>IP Address</td>
				</tr>
		
				<?php
				if (isset($value['lot_details']))
				{
					foreach ($value['lot_details']  as $lot_details)
					{
					?>
					<tr>
						<td align='center' class='wrappable' style='width:80px;'><?php echo $lot_details['cname'];?></td>
						<td align='center' class='wrappable' style='width:70px;'><?php echo $lot_details['dt'];?> <?php echo $lot_details['tm'];?></td>
						<td align='center' class='wrappable' style='width:70px;'><?php echo $lot_details['bid_amt'];?></td>
						<td align='center' class='wrappable' style='width:80px;'><?php echo $lot_details['quantity'];?></td>
						<td align='center' class='wrappable' style='width:60px;'><?php echo $lot_details['typ'];?></td>
						<td align='center' class='wrappable' style='width:80px;'><?php echo $lot_details['ip'];?></td>
					</tr>

						<?php
					}
					
				}
				else
				{
	?>
					<tr><td colspan='6' align='center' class='wrappable'>No Bid</td></tr>
	<?php
				}
	?>
				</tbody>
				</table></div>
	<?php
	
			}
		}
		
	?>
	
</page>
</div>
</div>	
	<footer id="footer">
		<div class="hide-fixed pull-right pad-rgt">&#0169; <?php echo date("Y");?> A One Salasar</div>
		<p class="pad-lft">&nbsp;</p>
	</footer>
</body>
</html>