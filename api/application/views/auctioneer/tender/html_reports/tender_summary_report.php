<?php $this->load->view('auctioneer/auc_report_header')?>
<div class="panel panel-primary">
	<div class="panel-heading">
		<h3 class="panel-title">
		<strong>AUCTION SUMMARY FOR TENDER NO :  <?php echo $common_auction_details['saleno']; ?> </strong>
		</h3>
	</div>
	<div class="panel-body">
		
	<page>
		<page_footer>
				<table style='width: 100%;'>
					<tr>
						<td align='left' style='float:left;width:200px;'>
					
						<?php echo (new DateTime(null, new DateTimeZone($_SESSION['user_tz'])))->format('D dS \of M Y h:i:s A'); ?>
						
						</td>
						<td align='right' style='float:right;width:545px;'>
						<span style='font-size:20px;color:#2a5395;'><strong><?php echo PRODUCT ?></strong></span>
						</td>
					</tr>
				</table>
			</page_footer>
			
			<div class="table-responsive">
				<table class="table table-striped table-bordered table-hover no-footer dtr-inline">
					<tbody>
						<tr>
							<td colspan='2' align='center' >
								<strong><?=strtoupper($common_auction_details['vendor_name'])?></strong>
							</td>
					
							<td colspan='2' align='center' >
								<strong>TENDER DETAILS</strong>
							</td>
						</tr>
					</tbody>		
				</table>
			</div>		
			<div class="table-responsive">
				<table class="table table-striped table-bordered table-hover no-footer dtr-inline">
					<thead>
						<tr>
							<th>Tender No</th>
							<th>Tender Commencement Date</th>
							<th>Tender Closing Date</th>
							<th>Particulars</th>
							<th>No. of Lots</th>
						</tr>
					</thead>	
					<tbody>
						
						<tr>
							<td align='left'>
								<?php echo $common_auction_details['saleno']?>
							</td>
					
							<td align='left'>
								<?php echo $common_auction_details['tcdate'].$common_auction_details['tctime']?>
							</td>
						
							<td align='left'>
								<?php echo $common_auction_details['edt']?>
							</td>
					
							<td align='left'>
								<?php echo $common_auction_details['particulars']?>
							</td>
						
							<td align='left'>
								<?php echo $common_auction_details['no_of_lots']?>
							</td>
						</tr>
					</tbody>		
				</table>
			</div>
			
			<div class="panel-heading">
				<h3 class="panel-title">
					<strong>AUCTION SUMMARY FOR SALE/AUCTION NO: <?php echo $common_auction_details['saleno']; ?></strong>
				</h3>
			</div>

			
						<?php

							if ( ! empty($tender_summary))
							{

								foreach($tender_summary as $key=>$value)
								{


						?>
						<div class="table-responsive">
						<table class="table table-striped table-bordered table-hover no-footer dtr-inline">
							<tbody>
								<tr>
									<td align="center" colspan="4" >
										<b><font size="2" >TENDER DETAILS</font></b>
									</td>
								</tr>
								<tr>
									<td style="width: 150px;"  ><b>Tender No</b></td>
									<td style="width: 210px;" ><?php echo $common_auction_details['saleno'];?></td>
									<td style="width: 150px;" >Auction Type</td>
									<td style="width: 208px;" ><?php echo $common_auction_details['tender_type'];?></td>
								</tr>
								<tr>
									<td  ><b>Tender Commencement Date</b></td>
									<td  ><?php echo $common_auction_details['tcdate'];?></td>
									<td  ><b>Tender Closing Date:</b></td>
									<td  ><?php echo $common_auction_details['edt'];?></td>
								</tr>
								<tr>
									<td  nowrap><b>Tender Opening Date</b></td>
									<td ><?php echo $common_auction_details['sdt'];?></td>
									<td ><b>Vendor</b></td>
									<td><?php echo $common_auction_details['vendor_name'];?></td>
								</tr>
								<tr>
									<td><b>Location</b></td>
									<td><?php echo $common_auction_details['location']; ?></td>
									<td><b>Sector</b></td>
									<td> <?php echo $common_auction_details['sector']; ?></td>
								</tr>
								<tr>
									<td align="center" colspan="4">
										<b><font size="2">HIGHLIGHTS OF THE AUCTION</font></b>
									</td>
								</tr>
								<tr>
									<td  ><b>Total No of Lots &nbsp;:&nbsp;</b></td>
									<td  ><?php echo $value['no_of_lots'];?> Lots</td>
									<td  ><b>No of Buyers Invited &nbsp;:&nbsp;</b></td>
									<td ><?php echo $value['no_of_invited'];?> Clients</td>
								</tr>
								<tr>
									<td ><b>Unbidded Lots &nbsp;:&nbsp;</b></td>
									<td ><?php echo $value['unbidded_lot_cnt'];?> Lots</td>
									<td ><b>No of Buyers Participated &nbsp;:&nbsp;</b> </td>
									<td ><?php echo $value['no_of_participated'];?> Clients</td>
								</tr>
								<tr>
									<td ><b>Total Spend &nbsp;:&nbsp;</b></td>
									<td ><?php echo $value['total_spend']?> </td>
									<td ><b>Auction Running Time &nbsp;:&nbsp;</b> </td>
									<td ><?php echo $value['auction_day']?> Clients</td>
								</tr>
								<tr>
									<td ><b>Highest Price &nbsp;:&nbsp;</b></td>
									<td ><?php echo $value['highest_price']?></td>
									<td ><b>Difference &nbsp;:&nbsp;</b> </td>
									<td ><?php echo $value['profit']?></td>
								</tr>
								<tr class="hd" >
									<td bgcolor="#FFF">&nbsp;</td>
									<td bgcolor="#FFF">&nbsp;</td>
									<td bgcolor="#FFF">&nbsp;</td>
									<td bgcolor="#FFF">&nbsp;</td>
								</tr>
							</tbody>
							</table>
						</div>
							
							<div class="table-responsive">
								<table class="table table-striped table-bordered table-hover no-footer dtr-inline">
								
											<thead>

												<tr>
													<th align="center">SR NO. &nbsp;&nbsp;</th>
													<th align="center">MATERIAL<br/> Code</th>
													<th align="center">PLANT &nbsp;&nbsp;</th>
													<th align="center" style="width:20px;">DESCRIPTION&nbsp;&nbsp;</th>
													<th align="center">TOTAL QTY </th>
													<th align="center">BID QTY </th>

													<th>Start Price &nbsp;&nbsp; </th>
													<?php
														if($value['bid_type'] == 'I')
														{
													?>
														<th align="center">H1 BID &nbsp;&nbsp;</th>
														<th align="center">H1 BIDDER &nbsp;&nbsp;</th>
													<?php
														}
														else
														{
													?>
														<th align="center">L1 BID &nbsp;&nbsp;</th>
														<th align="center">L1 BIDDER &nbsp;&nbsp;</th>
													
													<?php
															
														}

													?>

													<th align="center">NO OF &nbsp;<br/> BIDS</th>
													<th align="center">NO OF <br/>BIDDERS&nbsp;&nbsp;</th>
													<th align="center">LOT TOTAL <br/>AMOUNT</th>
												</tr>
											</thead>
											<tbody>
												<?php
													if( ! empty($value['vendorlotno']))
													{
														$i = 1;
														foreach($value['vendorlotno'] as $k=>$v)
														{
														?>
														<tr>
															<td style="width:40px;"  align="center" class='wrappable'><?php echo $i?></td>
															<td style="width:40px;"  align="center" class='wrappable'><?php echo $value['mat_code'][$k]?></td>
															<td style="width:70px;" align="center" class='wrappable'><?php echo $value['plant'][$k]?>&nbsp;&nbsp;</td>
															<td align="left" class='wrappable' style="width:81px;white-space:normal;word-wrap:break-word;"><p style="word-wrap: break-word"><?php echo $value['prod'][$k]?></p></td>
															
															<td style="width:50px;"  align="center" class='wrappable'><?php echo $value['tq'][$k]?>&nbsp;&nbsp;</td>
															<td style="width:50px;"  align="center" class='wrappable'><?php echo $value['bq'][$k]?>&nbsp;&nbsp;</td>
															<td style="width:50px;"  align="center" class='wrappable'><?php echo $value['sb'][$k]?>&nbsp;&nbsp;</td>
															<td style="width:50px;" " align="center" class='wrappable'><?php echo $value['amt'][$k]?></td>
															<td style="width:50px;"  align="center" class='wrappable'><?php echo $value['bidder_name'][$k]?>&nbsp;&nbsp;&nbsp;</td>
															
															<td style="width:50px;"  align="center" class='wrappable'><?php echo $value['no_of_bids'][$k]?>&nbsp;&nbsp;</td>
															<td style="width:50px;"  align="center" class='wrappable'><?php echo $value['no_of_bidders'][$k]?>&nbsp;&nbsp;</td>
															<td style="width:70px;"  align="center" class='wrappable'><?php echo $value['rs'][$k]?>&nbsp;&nbsp;</td>
														</tr>
														<?php
														$i++;
														}
													} else {
														?> <tr><td colspan="12" style="width: 735px;" align='center'>No data is available</td></tr> <?php
													}
												?>
											</tbody>
										</table>
							</div>
						<?php
								}
							}
						?>
		</page>
		</div>
</div>
</div>	

</body>
</html>