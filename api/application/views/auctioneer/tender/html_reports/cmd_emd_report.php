<?php $this->load->view('auctioneer/auc_report_header')?>
<div class="panel panel-primary">
	<div class="panel-heading">
		<h3 class="panel-title">
		<strong>CMD EMD REPORT FOR TENDER NO :  <?php echo $common_auction_details['saleno']; ?> </strong>
		</h3>
	</div>
	<div class="panel-body">
		
<page>
   <page_footer>
        <table style='width: 100%;'>
			<tr>
				<td align='left' style='float:left;width:200px;'>
			
				<?php echo (new DateTime(null, new DateTimeZone($_SESSION['user_tz'])))->format('D dS \of M Y h:i:s A'); ?>
				
				</td>
				<td align='right' style='float:right;width:545px;'>
				<span style='font-size:20px;color:#2a5395;'><strong><?php echo PRODUCT ?></strong></span>
				</td>
			</tr>
		</table>
    </page_footer>
	
	<div class="table-responsive">
		<table class="table table-striped table-bordered table-hover no-footer dtr-inline">
			<tbody>
			
			<tr>
				<td colspan='2' align='center' >
					<strong><?=strtoupper($common_auction_details['vendor_name'])?></strong>
				</td>
		
				<td colspan='2' align='center' >
					<strong>TENDER DETAILS</strong>
				</td>
			</tr>
			</tbody>		
			</table>
			</div>		
			<div class="table-responsive">
		<table class="table table-striped table-bordered table-hover no-footer dtr-inline">
			<thead>
				<tr>
					<th>Tender No</th>
					<th>Tender Commencement Date</th>
					<th>Tender Closing Date</th>
					<th>Particulars</th>
					
					<th>No. of Lots</th>
				</tr>
			</thead>	
		<tbody>
			
			<tr>
				<td align='left'>
					<?php echo $common_auction_details['saleno']?>
				</td>
		
				<td align='left'>
					<?php echo $common_auction_details['tcdate'].$common_auction_details['tctime']?>
				</td>
			
				<td align='left'>
					<?php echo $common_auction_details['edt']?>
				</td>
		
				<td align='left'>
					<?php echo $common_auction_details['particulars']?>
				</td>
			
				<td align='left'>
					<?php echo $common_auction_details['no_of_lots']?>
				</td>
			</tr>
		</tbody>		
	</table>
	</div>
	
	<div class="table-responsive">
		<table class="table table-striped table-bordered table-hover no-footer dtr-inline">
			<thead>
			<tr>
				<th align='center' class='wrappable' style='font-weight:bold;'>SR NO.</th>
				<th align='center' class='wrappable' style='font-weight:bold;'>EMD</th>
				<th align='center' class='wrappable' style='font-weight:bold;'>BIDDER NAME</th>
				<th align='center' class='wrappable' style='font-weight:bold;'>DD NO</th>
				<th align='center' class='wrappable' style='font-weight:bold;'>DD DATE</th>
				<th align='center' class='wrappable' style='font-weight:bold;'>BANK NAME</th>
			</tr>
		</thead>
		<tbody>
			<?php 
				if(!empty($cmd_emd))
				{
					$I = 1;
					foreach($cmd_emd as $key=>$value)
					{
			?>
			
						<tr>
							<td align='center' class='wrappable' style='width:50px;font-weight:bold;'><?=$I++?></td>
							<td align='center' class='wrappable' style='width:130px;'> 
								<?php 
									if($value['cmdamt']>0) 
									{
							
										echo $value['cmdamt'] ;
									}
									else
									{
										echo $value['emdamt'];
									}
								?> 
							</td>
							<td align='center' class='wrappable' style='width:226px;'><?php echo $value['compname']?></td>
							<td align='center' class='wrappable' style='width:100px;'>
								<?php
									if($value['cmdamt']>0) 
									{
							
										echo $value['cmdddno'] ;
									}
									else
									{
										echo $value['emdddno'];
									}
								?> 
							</td>
							<td align='center' class='wrappable' style='width:100px;'>
								<?php 
									if($value['cmdamt']>0) 
									{
							
										echo $value['cmddate'] ;
									}
									else
									{
										echo $value['emddate'];
									}
								?> 
							</td>
							<td align='center' class='wrappable' style='width:100px;'>
								<?php 
									if($value['cmdamt']>0) 
									{
							
										echo $value['cmdbank'] ;
									}
									else
									{
										echo $value['emdbank'];
									}
								?> 
							</td>
						</tr>
			
			
			<?php
						
						
					}
				}
			
			?>
		</tbody>
	</table>
	</div>
</page>
</div>
</div>	
	<footer id="footer">
		<div class="hide-fixed pull-right pad-rgt">&#0169; <?php echo date("Y");?> A One Salasar</div>
		<p class="pad-lft">&nbsp;</p>
	</footer>
</body>
</html>