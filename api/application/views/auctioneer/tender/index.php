<style>

	.btn-group{
		margin-left:19px;
		}
	.btn-group:first-child{
		margin-left:0px;
	}
   .fa-upload{
   margin-left:5px !important;
   }
   .auction-msg{
	   float:right !important;
   }
   .auction-live{
	   float:right !important;
   }
 @media (max-width:990px){
	 a.btn{
		 margin-top:10px;
		 width:250px;
	 }
	 .btn-group{
		 margin-left:0;
	 }
	 .fa-upload{
	   margin-left:0 !important;
	 }
	 .auction-msg{
	   float:left !important;
   }
   .auction-live{
	   float:left !important;
   }
 }
</style>
<!--CONTENT CONTAINER-->
<!--===================================================-->
<div id="content-container">
	<div id="page-head">

		<!--Page Title-->
		<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
		<div id="page-title">
			<h1 class="page-header text-overflow">E-Tender</h1>
		</div>
		<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
		<!--End page title-->


		<!--Breadcrumb-->
		<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
		<ol class="breadcrumb">
			<li><a href="<?php echo base_url('dashboard');?>">Home</a></li>
			<li class="active">E-Tender</li>
		</ol
		<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
		<!--End breadcrumb-->
	</div>	
		<div id="page-content">
			<div class="float_btn">
				<a class="btn btn-primary btn-icon btn-circle add-tooltip"  data-placement="top" data-toggle="tooltip" data-original-title="Back" href='<?php echo base_url('auctioneer/dashboard'); ?>' ><i class="fa fa-arrow-left" aria-hidden="true"></i></a>
			</div>	
	
			<div class="panel">
				<div class="panel-heading">
					<h3 class="panel-title">E-Tender</h3>
				</div>
				<div class="panel-body">
					<div class="pad-btm form-inline">
						<div class="row">
							<?php
								if ($this->common_model->check_permission('Tender Add'))
								{
									?><a class="btn btn-success btn-labeled fa fa-plus" href="javascript:void(0);" title="Add E-Tender" name="btn_add" id="btn_add" onclick="javascript:auction_actions('ADD')">Add</a><?php
								}
							?>
							<?php
								if($this->common_model->check_permission('Tender Edit'))
								{
									?><a class="btn btn-primary btn-labeled fa fa-edit" href="javascript:void(0);" title="Edit E-Tender" name="btn_edit" id="btn_edit" onclick="javascript:auction_actions('EDIT')">Edit</a><?php
								}
							?>

							<?php
								if($this->common_model->check_permission('Tender Lot View'))
								{
									?><a class="btn btn-primary btn-labeled fa fa-archive" href="javascript:void(0);" title="View Lots" name="btn_view_lots" id="btn_view_lots" onclick="javascript:auction_actions('LOTS')">View Lots</a><?php
								}
							?>

							<?php
								if($this->common_model->check_permission('Tender Client View'))
								{
									?><a class="btn btn-primary btn-labeled fa fa-user" href="javascript:void(0);" title="E-Tender Clients" name="btn_auction_client" onclick="javascript:auction_actions('TENDER_CLIENT')">Clients</a><?php
								}
							?>

							<?php
								if($this->common_model->check_permission('Tender Open'))
								{
									?><a class="btn btn-primary btn-labeled fa fa-folder-open" href="javascript:void(0);" title="Open Tender" name="btn_open_tender" onclick="javascript:auction_actions('OPEN_TENDER')">Open Tender</a><?php
								}
							?>

							<?php
								if($this->common_model->check_permission('Tender Supervisors View'))
								{
									?><a class="btn btn-primary btn-labeled fa fa-users" href="javascript:void(0);" title="Supervisors" name="btn_supervisors" onclick="javascript:auction_actions('SUPERVISORS')">Supervisors</a><?php
								}
							?>
							<?php
								if($this->common_model->check_permission('Tender Document View'))
								{
									?><a class="btn btn-primary btn-labeled fa fa-upload" href="javascript:void(0);" title="Documents" name="btn_docs" onclick="javascript:auction_actions('OTHER_DOCS')">Documents</a><?php
								}
							?>
							<?php
								if($this->common_model->check_permission('Tender Message View'))
								{
									?><a class="btn btn-primary btn-labeled fa fa-envelope" href="javascript:void(0);" title="E-Tender Message" name="btn_tender_msg" onclick="javascript:auction_actions('AUCTION_MESSAGE')">E-Tender Message</a><?php
								}
							?>

							<?php
								if ($this->common_model->check_permission('Upload Tender Images'))
								{
							?>
								<a class="btn btn-primary btn-labeled fa fa-picture-o" href="javascript:void(0);" title="Auction Image" name="btn_auction_img" onclick="auction_actions('AUCTION_IMAGES')">Auction Images</a>
							<?php
								}
							?>
								<a class="btn btn-success btn-labeled fa fa-refresh" onclick="refresh_datatable();">Refresh </a>
							<?php
								if($this->common_model->check_permission('Tender Delete'))
								{
									?><a class="btn btn-danger btn-labeled fa fa-trash" href="javascript:void(0);" title="Delete E-Tender" name="btn_delete" onclick="javascript:delete_tender()">Delete</a><?php
								}
							?>
						</div>
					</div>
					<form name="rev_tender_form" method="post" >
						<div class="row filter">
							<div class="col-sm-2">
								<input class="form-control searchInput" placeholder="Sale No" type="text" name="search_sale_no" id="search_sale_no" value="" />
							</div>
							<div class="col-sm-2">
								<input class="form-control datepickerclass searchInput" readonly placeholder="Start Date" type="text" name="search_start_date" id="search_start_date" value="" />
							</div>
							<div class="col-sm-2">
								<input class="form-control datepickerclass searchInput" readonly placeholder="End Date" type="text" name="search_end_date" id="search_end_date" value="" />
							</div>
							<div class="col-sm-2">
								<input class="form-control searchInput" placeholder="Location" type="text" name="search_tender_location" id="search_tender_location" value="" />
							</div>
							<div class="col-sm-2 ">
								<a class="btn btn-danger btn-labeled fa fa-ban" onclick="clearSearchFilters();">Clear Search </a>
							</div>

						</div>
						<div class="table-responsive">
							<table callfunction="<?php echo base_url('auctioneer/tender/fetch');?>" class="table table-striped table-bordered table-hover no-footer dtr-inline dynamicTable">
								<thead>
									<tr>
										<th data-bSortable="false">#</th>
										<th>E-Tender No</th>
										<th>COMMENCEMENT<br/>DATE</th>
										<th>OPENING<br/>DATE</th>
										<th>CLOSING<br/>DATE</th>
										<th>COMPANY NAME</th>
										<th>PLANT NAME</th>
										<th>PARTICULARS</th>
										<th>LOCATION</th>
										<th data-bSortable="false">PUBLISHED?</th>
									</tr>
								</thead>
								<tbody>
								</tbody>
							</table>
						</div>
					</form>
				</div>
			</div>
		</div>

	<div id="open_tender_modal" class="modal fade" role="dialog">
		<div class="modal-dialog">
			<div class="modal-content">
				<form name="open_tender_form" id="open_tender_form" method="post" class="form-horizontal">
					<input type='hidden' id='tid' name='tid' value=''/>
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal">&times;</button>
						<h4 class="modal-title" >Open Tender : <span id='tender_sale_no'></span></h4>
					</div>
					<div class="modal-body">
						<div class="form-group">
							<label class="col-sm-4 control-label">Tender Password<span class="text-danger">*</span>:</label>
							<div class="col-md-7">
								<input type='password' class="form-control" name="tender_password" id="tender_password" />
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-4 control-label">User Name <span class="text-danger">*</span>:</label>
							<div class="col-md-7">
								<input type='password' class="form-control" name="user_name" id="user_name" />
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-4 control-label">Password<span class="text-danger">*</span>:</label>
							<div class="col-md-7">
								<input type='password' class="form-control" name="user_password" id="user_password" />
							</div>
						</div>
					</div>
					<div class="modal-footer">
						<input class="btn btn-primary" type="submit" value='Submit' name="btn_upload_file_submit" id='btn_upload_file_submit' />
						<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>
<!--===================================================-->
<!--END CONTENT CONTAINER-->
<script>

function publish_status(id,status){
	var msg = '';
	if(status == 'N')
	{
		msg = 'Are you sure to unpublish this auction?';
	}
	else
	{
		msg = 'Are you sure to publish this auction?';
	}
	var ans = confirm(msg);
	if(ans)
	{
		$.ajax(
		{
			url: "<?php echo base_url('auctioneer/tender/update_publish_status'); ?>",
			type: "POST",
			dataType:"json",
			cache: false,
			clearForm: false,
			data:{"id":id,'status':status},
			success:function(response)
			{
				if(response.status == "success")
				{
					Display_msg(response.message,response.status);
					refresh_datatable();
				}
				else
				{
					Display_msg(response.message,response.status);
					return false;
				}
			}
		});
	}
}

function delete_tender()
{
	var id = $("#list_tid:checked").val()
	if(id == '' || id == undefined)
	{
		Display_msg('Please Select E-Tender.','error');
		return false;
	}
	else
	{
		if(id > 0)
		{
			var ans = confirm('Are you sure you want to delete this auction?');
			if(ans)
			{
				$.ajax(
				{
					url: "<?php echo base_url('auctioneer/tender/delete_tender'); ?>",
					type: "POST",
					dataType:"json",
					cache: false,
					clearForm: false,
					data:{"id":id},
					success:function(response)
					{
						if(response.status == "success")
						{
							Display_msg(response.message,response.status);
							refresh_datatable();
						}
						else
						{
							Display_msg(response.message,response.status);
							return false;
						}
					}
				});
			}
		}
		else
		{
			Display_msg('Please Select E-Tender.','error');
			return false;
		}
	}
}

function auction_actions(act){
	if(act == 'ADD')
	{
		window.location.href = '<?php echo base_url("auctioneer/tender/addedit") ?>';
	}
	else if(act == 'EDIT')
	{
		var id = $("#list_tid:checked").val();
		if(id == '' || id == undefined)
		{
			Display_msg('Please Select E-Tender.','error');
			return false;
		}
		else
		{
			window.location.href = '<?php echo base_url("auctioneer/tender/addedit/") ?>'+id;
		}
	}
	else if(act == 'OTHER_DOCS')
	{
		var id = $("#list_tid:checked").val();
		if(id == '' || id == undefined)
		{
			Display_msg('Please Select E-Tender.','error');
			return false;
		}
		else
		{
			window.location.href = '<?php echo base_url("auctioneer/tender/documents/") ?>'+id;
		}
	}
	else if(act == 'LOTS')
	{
		var id = $("#list_tid:checked").val();
		if(id == '' || id == undefined)
		{
			Display_msg('Please Select E-Tender.','error');
			return false;
		}
		else
		{
			window.location.href = '<?php echo base_url("auctioneer/tender/view_lots/") ?>'+id;
		}
	}
	else if(act == 'AUCTION_MESSAGE')
	{
		var id = $("#list_tid:checked").val();
		if(id == '' || id == undefined)
		{
			Display_msg('Please Select E-Tender.','error');
			return false;
		}
		else
		{
			window.location.href = '<?php echo base_url("auctioneer/tender/message/") ?>'+id;
		}
	}
	else if(act == 'SUPERVISORS')
	{
		var id = $("#list_tid:checked").val();
		if(id == '' || id == undefined)
		{
			Display_msg('Please Select E-Tender.','error');
			return false;
		}
		else
		{
			window.location.href = '<?php echo base_url("auctioneer/tender/supervisors/") ?>'+id;
		}
	}
	else if (act == 'AUCTION_IMAGES')
	{
		var id = $("#list_tid:checked").val()
		if( ! id)
		{
			Display_msg('Please Select Auction.','error');
			return false;
		}
		else
		{
			window.location.href = '<?php echo base_url("auctioneer/auction/auction_images/t/") ?>'+id;
		}
	}
	else if(act == 'OPEN_TENDER')
	{
		var id = $("#list_tid:checked").val();
		var sale_no = $("#list_tid:checked").attr('sale_no');
		if(id == '' || id == undefined)
		{
			Display_msg('Please Select E-Tender.','error');
			return false;
		}
		else
		{
			$('#tender_sale_no').html(sale_no);
			$('#tid').val(id);
			$('#user_name').val('');
			$('#user_password').val('');
			$('#open_tender_modal').modal('show');
		}
	}
	else if(act == 'TENDER_CLIENT')
	{
		var id = $("#list_tid:checked").val();
		if(id == '' || id == undefined)
		{
			Display_msg('Please Select E-Tender.','error');
			return false;
		}
		else
		{
			window.location.href = '<?php echo base_url("auctioneer/tender/view_clients/") ?>'+id;
		}
	}
	else
	{
		Display_msg('Please Select Action.','error');
		return false;
	}
}

$(function()
{
	var vRules = {
		tender_password:{required:true},
		user_name:{required:true},
		user_password:{required:true}
	};
	var vMessages = {
		tender_password:{required:"<p class='text-danger'>Please enter your tender password.</p>"},
		user_name:{required:"<p class='text-danger'>Please enter your login username.</p>"},
		user_password:{required:"<p class='text-danger'>Please enter your login password.</p>"}
	};

	$("#open_tender_form").validate({
		rules: vRules,
		messages: vMessages,
		submitHandler: function(form){
			$(form).ajaxSubmit({
				url:"<?php echo base_url('auctioneer/tender/open_tender/');?>",
				type: 'post',
				dataType:'json',
				cache: false,
				clearForm: true,
				success: function (response) {
					if(response.status=="success")
					{
						Display_msg(response.message,response.status);
						$('#open_tender_modal').modal('hide');
					}
					else
					{
						Display_msg(response.message,response.status);
						return false;
					}
				}
			});
		}
	});
});
</script>
