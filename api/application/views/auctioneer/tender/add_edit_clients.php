<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/css/bootstrap-datepicker.min.css" integrity="sha256-siyOpF/pBWUPgIcQi17TLBkjvNgNQArcmwJB8YvkAgg=" crossorigin="anonymous" />
<style>
.add_select2{
	width:100%;
}
</style>
<!--CONTENT CONTAINER-->
<!--===================================================-->
<div id="content-container">
	<!--Page Title-->
	<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
	<div id="page-head">
	<div id="page-title">
		<h1 class="page-header text-overflow">E-Tender Client Add Lot</h1>
	</div>
	<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
	<!--End page title-->


	<!--Breadcrumb-->
	<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
	<ol class="breadcrumb">
		<li><a href="<?php echo base_url('dashboard');?>">Home</a></li>
		<li class="active"><a href="<?php echo base_url('auctioneer/tender/index');?>">E-Tender</a></li>
		<li class="active"><a href="<?php echo base_url('auctioneer/tender/view_clients/'.$tid);?>">E-Tender Clients</a></li>
		<li class="active">E-Tender Client Add Lot</li>
	</ol>
</div>
	<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
	<!--End breadcrumb-->
	<div id="page-content">
	<div class="float_btn">
		<a class="btn btn-primary btn-icon btn-circle add-tooltip"  data-placement="top" data-toggle="tooltip" data-original-title="Back" href='<?php echo base_url('auctioneer/tender/view_clients/'.$tid); ?>' ><i class="fa fa-arrow-left" aria-hidden="true"></i></a>
	</div>	
		<div class="panel">
			<div class="panel-heading">
				<h3 class="panel-title">E-Tender Client Add Lot</h3>
			</div>

			<div class="panel-body">
				<fieldset>
					<legend>Auction Details</legend>
					<div class="table-responsive">
						<table class="table table-striped table-bordered table-hover no-footer dtr-inline">
							<thead>
								<tr>
									<th>Sale No</th>
									<th>Start Time</th>
									<th>End Time</th>
									<th>Particulars</th>
								</tr>
								<tr>
									<td><?php echo $auction_details['tender_id'] ?></td>
									<td><?php echo $auction_details['topen_dt']." ".$auction_details['tender_open_time']; ?></td>
									<td><?php echo $auction_details['tclose_dt']." ".$auction_details['tender_close_time'] ?></td>
									<td><?php echo $auction_details['particulars'] ?></td>
								</tr>
							</thead>
						</table>
					</div>
				</fieldset>
				<?php
				$amount = '0.00';
				$bank = '';
				$ref = '';
				$date = '';
				$type = '';
				if( ! empty($client_details))
				{
					if($client_details['emd_cmd_type'] == 'C')
					{
						$amount = $client_details['cmdamt'];
						$bank = $client_details['cmdbank'];
						$ref = $client_details['cmdddno'];
						$date = $client_details['cmddate'];
						$type = $client_details['emd_cmd_type'];
					}
					else
					{
						$amount = $client_details['emdamt'];
						$bank = $client_details['emdbank'];
						$ref = $client_details['emdddno'];
						$date = $client_details['emddate'];
						$type = $client_details['emd_cmd_type'];
					}
				}
				?>
				<form name="form_tender_client" id="form_tender_client" method="post" class="form-horizontal">
					<fieldset>
						<legend>Client Details</legend>
						<input type='hidden' id="tid" name="tid" value='<?php echo $tid; ?>' />
						<div class="form-group">
							<label class="col-sm-2 control-label">Client Name <span class="text-danger">*</span>:</label>
							<div class="col-sm-3">
								<select class=" add_select2" name="client_id" id="client_id">
									<option></option>
									<?php
									if( ! empty($all_clients))
									{
										foreach($all_clients as $single_client)
										{
											$selected = '';
											if($single_client['id'] == $client_details['client_id'])
											{
												$selected = 'selected="selected"';
											}
											?><option value='<?php echo $single_client['id'];?>' <?php echo $selected; ?>><?php echo $single_client['compname']." [".$single_client['conperson']."] [".$single_client['mob']."] [".$single_client['email']."]"?></option><?php
										}
									}
									?>
								</select>
							</div>
							<label class="col-sm-3 control-label">Deposit Type : </label>
							<div class="col-sm-3">
								<select id="emd_cmd_type" class="form-control"  name="emd_cmd_type">
									<option value='C' <?php echo ($type == 'C') ? 'selected' : '' ; ?>>CMD</option>
									<option value='E' <?php echo ($type == 'E') ? 'selected' : '' ; ?>>EMD</option>
								</select>
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-2 control-label">Amount <span class="text-danger">*</span>:</label>
							<div class="col-sm-3">
								<input class="form-control" type="text" name="amount" id="amount"  value="<?php echo $amount;?>" size="11" />
							</div>
							<label class="col-sm-3 control-label">Bank Name : </label>
							<div class="col-sm-3">
								<input class="form-control" type="text" name="bank_name" id="bank_name" value="<?php echo $bank; ?>" size="15" maxlength="100" />
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-2 control-label">Ref No</label>
							<div class="col-sm-3">
								<input class="form-control" type="text" name="ref_no" id="ref_no" value="<?php echo $ref; ?>" size="11" />
							</div>
							<label class="col-sm-3 control-label">Date : </label>
							<div class="col-sm-3">
								<input class="form-control" readonly type="text" name="transaction_date" id="transaction_date" value="<?php echo $date; ?>" />
							</div>
						</div>
					</fieldset>
					<?php
					//PrintArray($client_details);
					$alloted_lots_to_client = array();
					if( ! empty($client_details['alloted_lots']))
					{
						$alloted_lots_to_client = str_replace(' ','',$client_details['alloted_lots']);
						$alloted_lots_to_client = explode(',',$alloted_lots_to_client);
					}
					?>
					<fieldset>
						<legend>Lot Details</legend>
						<div class="table-responsive">
							<table class="table table-bordered table-striped">
								<thead>
									<tr>
										<th>
											<input type="checkbox" class="check_all" name="check_all" id="check_all" onchange="check_all_checkbox(this);">
										</th>
										<th>LOT NO</th>
										<th>VENDOR LOT NO</th>
										<th style='width:40%'>PRODUCT</th>
										<th>STARTING BID</th>
										<th>MIN INCR DESC</th>
										<th>CMD</th>
									</tr>
								</thead>
								<tbody>
								<?php
								$bidded_lot_array = array();
								if ( ! empty($bidded_lots))
								{
									$bidded_lot_array = explode(',',$bidded_lots);
								}

								if( ! empty($lot_details))
								{
									foreach ($lot_details as $single_lot)
									{
										$checked = '';
										if (in_array($single_lot['lotno'],$alloted_lots_to_client))
										{
											$checked = 'checked';
										}
										?>
										<tr>
											<td>
												<?php
												if ( ! in_array($single_lot['lotno'],$bidded_lot_array))
												{
													?>
													<input <?php echo $checked; ?> type="checkbox" class="checked_lot" name="client_lot[<?php echo $single_lot['lotno']?>]" id="client_lot<?php echo $single_lot['lotno']?>" value="<?php echo $single_lot['lotno']; ?>" onchange="enable_disable_lots('<?php echo $single_lot['lotno']?>',this);">
													<?php
												}
												?>
											</td>
											<td><?php echo $single_lot['lotno']; ?></td>
											<td><?php echo $single_lot['mat_code']; ?></td>
											<td><?php echo $single_lot['description']; ?></td>
											<td>
												<?php
												if(empty($single_lot['start_bid']))
												{
													?><input type="text" name="lot_start_bid[<?php echo $single_lot['lotno']?>]" lot_no="<?php echo $single_lot['lotno']?>" class="client_start_bid lot_<?php echo $single_lot['lotno']?>" value="0.00" style="width: 100px;" disabled /><?php
												}
												else
												{
													echo $single_lot['start_bid'];
													?><input type="hidden" name="lot_start_bid[<?php echo $single_lot['lotno']?>]" lot_no="<?php echo $single_lot['lotno']?>" class="client_start_bid lot_<?php echo $single_lot['lotno']?>" value="0.00" style="width: 100px;" disabled /><?php
												}
												?>
											</td>
											<td><?php echo $single_lot['minimum_incr_decr']; ?></td>
											<td><?php echo $single_lot['cmd']; ?></td>
										</tr>
										<?php
									}
								}
								?>
								</tbody>
							</table>
						</div>
					</fieldset>
					<div class="row ">
						<div class="col-sm-12" style="text-align: center;">
							<input type='submit' class="btn btn-info" placeholder="Submit" name="btn_submit" id="btn_submit" value='Submit'  />
							<a class="btn btn-warning" href='<?php echo base_url('auctioneer/tender/view_clients/'.$tid);?>' name="btn_back" id="btn_back" >Back</a>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>

<!--===================================================-->
<!--END CONTENT CONTAINER-->

<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/js/bootstrap-datepicker.min.js" integrity="sha256-bqVeqGdJ7h/lYPq6xrPv/YGzMEb6dNxlfiTUHSgRCp8=" crossorigin="anonymous"></script>
<script>

$("#transaction_date").datepicker({
	format: 'yyyy-mm-dd'
});

/* Check All Lots */
function check_all_checkbox(obj){
	if($(obj).is(":checked"))
	{
		$('.checked_lot').each(function()
		{
			$(this).prop('checked',true);
			$(this).change();
		});
	}
	else
	{
		$('.checked_lot').each(function()
		{
			$(this).prop('checked',false);
			$(this).change();
		});
	}

}

/* Selected Lots Enable Disable */
function enable_disable_lots(lotno,obj)
{
	if($(obj).is(":checked"))
	{
		$(".lot_"+lotno).each(function()
		{
			$(this).removeAttr("disabled");
		});
	}
	else
	{
		$(".lot_"+lotno).each(function()
		{
			$(this).attr("disabled","disabled");
		});
	}
}



function update_column_val(type)
{
	if(type == 'mul')
	{
		var text = $('#lot_mul_factor_all').val();
		$('.mul_factor').each(function(){
			var isDisabled = $(this).prop('disabled');
			if( ! isDisabled)
			{
				$(this).val(text);
			}

		});
	}
	else if(type == 'add')
	{
		var text = $('#lot_add_factor_all').val();
		$('.add_factor').each(function(){
			var isDisabled = $(this).prop('disabled');
			if( ! isDisabled)
			{
				$(this).val(text);
			}
		});
	}
}


$(function(){

	/* Select Multiple Checkbox With Shift Key - Start */
	var $chkboxes = $('.checked_lot');
    var lastChecked = null;

	$chkboxes.click(function(e) {
        if (!lastChecked) {
            lastChecked = this;
            return;
        }

        if (e.shiftKey) {
            var start = $chkboxes.index(this);
            var end = $chkboxes.index(lastChecked);

            $chkboxes.slice(Math.min(start,end), Math.max(start,end)+ 1).prop('checked', lastChecked.checked);
			$chkboxes.change();
        }

        lastChecked = this;
    });
	$chkboxes.change();

	/* Select Multiple Checkbox With Shift Key - End */


	var vRules = {
		client_id : {required:true},
		amount : {required:true,number:true}
	};
	var vMessages = {
		client_id : {required:"<p class='text-danger'>Please select client</p>"},
		amount : {required:"<p class='text-danger'>Please enter amount</p>"}
	};

	$("#form_tender_client").validate({
		rules: vRules,
		messages: vMessages,
		ignore: 'input[type=hidden], .select2-input, .select2-focusser',
		submitHandler: function(form){
			$(form).ajaxSubmit({
				url:"<?php echo base_url('auctioneer/tender/update_lots_to_client/');?>",
				type: 'post',
				dataType:'json',
				cache: false,
				success: function (response) {
					if(response.status=="success")
					{
						Display_msg(response.message,response.status);
						setTimeout(function()
						{
							window.location.href="<?php echo base_url('auctioneer/tender/view_clients/'.$tid);?>";
						}, 3000);
					}
					else
					{
						Display_msg(response.message,response.status);
						return false;
					}
				}
			});
		}
	})
});
</script>
