<!-- In order to let it work in CI, need to fix it to only get the viwes -->

<!--CONTENT CONTAINER-->
<!--===================================================-->
<div id="content-container">
	
	<!--Page Title-->
	<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
	<div id="page-head">
	<div id="page-title">
		<h1 class="page-header text-overflow">E-Tender Documents</h1>
	</div>
	<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
	<!--End page title-->


	<!--Breadcrumb-->
	<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
	<ol class="breadcrumb">
		<li><a href="<?php echo base_url('dashboard');?>">Home</a></li>
		<li><a href="<?php echo base_url('auctioneer/tender/index');?>">E-Tender</a></li>
		<li class="active">E-Tender Documents</li>
	</ol>
</div>
	<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
	<!--End breadcrumb-->
	<input type="hidden" id="t11" name="auctionId">
	<div id="page-content">
	<div class="float_btn">
		<a class="btn btn-primary btn-icon btn-circle add-tooltip"  data-placement="top" data-toggle="tooltip" data-original-title="Back" href='<?php echo base_url('auctioneer/tender/index'); ?>' ><i class="fa fa-arrow-left" aria-hidden="true"></i></a>
	</div>
		<div class="panel">
			<div class="panel-heading">
				<h3 class="panel-title">E-Tender Documents</h3>
			</div>

			<div class="panel-body">
				<form name="form_etender_doc" id="form_etender_doc" method="post" >
					<input type='hidden' id="tid" name="tid" value='<?php echo $tid; ?>' />
					<div class="row filter">
						<div class="col-sm-2">
							<input type='text' class="form-control " placeholder="Document Name" name="document_name" id="document_name"  />
						</div>
						<div class="col-sm-2">
							<input type='file' class="form-control " placeholder="Upload File" name="upload_file" id="upload_file"  />
						</div>
						<div class="col-sm-2">
							<input type='submit' class="btn btn-info" placeholder="Submit" name="btn_submit" id="btn_submit"  />
						</div>
					</div>
				</form>
				<br>
				<div class="table-responsive">
					<table class="table table-striped table-bordered table-hover no-footer dtr-inline ">
						<thead>
							<tr>
								<th data-bSortable="false">#</th>
								<th>Document Name</th>
								<th>Publish</th>
								<th>Date</th>
								<th>Action</th>
							</tr>
						</thead>
						<tbody>
						<?php
						if( ! empty($document_list))
						{
							$sr = 1;
							$delete_permission = false;
							if ($this->common_model->check_permission('Tender Document Delete'))
							{
								$delete_permission = true;
							}
							foreach($document_list as $single_doc)
							{
								$action = '';
								if($delete_permission)
								{
									$action = '<a href="javascript:void(0);" onclick="delete_document('.$single_doc['doc_id'].','.$tid.')">Delete</a>';
								}

								?>
								<tr>
									<td><?php echo $sr++; ?></td>
									<td><?php echo $single_doc['link']; ?></td>
									<td>
										<?php
										if($single_doc['published'] == 'Y')
										{ ?>
											<a href="javascript:void(0);" onclick="change_document_publish_status('<?php echo $single_doc['doc_id']; ?>','N');">
												<img src="<?php echo base_url('auctioneer_assets/images/publish.png');?>">
											</a> <?php
										}
										else
										{ ?>
											<a href="javascript:void(0);" onclick="change_document_publish_status('<?php echo $single_doc['doc_id']; ?>','Y');">
												<img src="<?php echo base_url('auctioneer_assets/images/publish_x.png');?>">
											</a> <?php
										}
										?>
									<td><?php echo $single_doc['date_uploaded']; ?></td>
									<td><?php echo $action;?></td>
								</tr>
							<?php
							}
						}
						?>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>

<!--===================================================-->
<!--END CONTENT CONTAINER-->
<script>

function delete_document(id,tid)
{
	if(id == '' || id == undefined)
	{
		Display_msg('Invalid document for deletion.','error');
		return false;
	}
	else
	{
		if(id > 0)
		{
			var ans = confirm('Are you sure you want to delete this document?');
			if(ans)
			{
				$.ajax(
				{
					url: "<?php echo base_url('auctioneer/tender/delete_document'); ?>",
					type: "POST",
					dataType:"json",
					cache: false,
					clearForm: false,
					data:{"doc_id":id,'tid':tid},
					success:function(response)
					{
						if(response.status == "success")
						{
							Display_msg(response.message,response.status);
							setTimeout(function()
							{
								window.location.reload();
							}, 3000);
						}
						else
						{
							Display_msg(response.message,response.status);
							return false;
						}
					}
				});
			}
		}
		else
		{
			Display_msg('Please Select Auction.','error');
			return false;
		}
	}
}

function change_document_publish_status(id,status){
	var msg = '';
	if(status == 'N')
	{
		msg = 'Are you sure to unpublish this document?';
	}
	else
	{
		msg = 'Are you sure to publish this document?';
	}
	var ans = confirm(msg);
	if(ans)
	{
		$.ajax(
		{
			url: "<?php echo base_url('auctioneer/tender/update_document_publish_status'); ?>",
			type: "POST",
			dataType:"json",
			cache: false,
			clearForm: false,
			data:{"id":id,'status':status},
			success:function(response)
			{
				if(response.status == "success")
				{
					Display_msg(response.message,response.status);
					setTimeout(function()
					{
						window.location.reload();
					}, 3000);
				}
				else
				{
					Display_msg(response.message,response.status);
					return false;
				}
			}
		});
	}
}

$(function(){
	var vRules = {
		document_name:{required:true},
		upload_file:{required:true}
	};
	var vMessages = {
		document_name:{required:"<p class='text-danger'>Please enter file name.</p>"},
		upload_file:{required:"<p class='text-danger'>Please select file.</p>"}
	};

	$("#form_etender_doc").validate({
		rules: vRules,
		messages: vMessages,
		submitHandler: function(form){
			$(form).ajaxSubmit({
				url:"<?php echo base_url('auctioneer/tender/upload_other_documents');?>",
				type: 'post',
				dataType:'json',
				cache: false,
				clearForm: true,
				success: function (response) {
					if(response.status=="success")
					{
						Display_msg(response.message,response.status);
						setTimeout(function()
						{
							window.location.reload();
						}, 3000);
					}
					else
					{
						Display_msg(response.message,response.status);
						return false;
					}
				}
			});
		}
	})
});
</script>
