<style>
textarea.form-control{
	max-width: 384px;
}	
p {
    margin: 0px;
}
.form-group {
    margin-right: 0px!important;
    margin-left: 0px!important;
}
.form-group {
    margin-bottom: 7px;
}
.select2-container{
	padding: 0 !important;
}

.mdtp__wrapper {
    bottom: 175px!important;
}
.float_btn{
    position: fixed;
    top: 15%;
    right: 5%;
    z-index: 10;
}
#page-content{
    position: relative;
}
.float_btn i {
	font-size: 20px;
}
</style>

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/datepicker/0.6.5/datepicker.css" integrity="sha256-n3ci71vDbbK59GUg1tuo+c3KO7+pnBOzt7BDmOe87s4=" crossorigin="anonymous" />
<link href="<?php echo base_url('assets/css/mdtimepicker.css'); ?>" rel="stylesheet" />

<!--CONTENT CONTAINER-->
<!--===================================================-->
<div id="content-container">
<div id="page-head">

	<!--Page Title-->
	<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
	<div id="page-title">
		<h1 class="page-header text-overflow"><?php echo $pagetitle; ?></h1>
	</div>
	<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
	<!--End page title-->
	<!--Breadcrumb-->
	<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
	<ol class="breadcrumb">
		<li><a href="<?php echo base_url('dashboard');?>">Home</a></li>
		<li><a href="<?php echo base_url('auctioneer/tender/index');?>">Tender Catalogue</a></li>
		<li><a class="active"><?php echo $pagetitle; ?></a></li>
	</ol>
	<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
	<!--End breadcrumb-->
</div>
	<!--Page content-->
	<!--===================================================-->
	<div id="page-content">
	
	<div class="float_btn">
		<a class="btn btn-primary btn-icon btn-circle add-tooltip"  data-placement="top" data-toggle="tooltip" data-original-title="Back" href='<?php echo base_url('auctioneer/tender/index'); ?>' ><i class="fa fa-arrow-left" aria-hidden="true"></i></a>
	</div>	
		<div class="row">
			<div class="col-sm-12">
				<!--Primary Panel-->
				<!--===================================================-->
				<div class="panel panel-primary">
					<div class="panel-heading">
						<h3 class="panel-title"><?php echo $pagetitle; ?></h3>
					</div>
					<!--Horizontal Form-->
					<!--===================================================-->
					<form class="form-horizontal" method="post" name="form_tender_addedit" id="form_tender_addedit" >
						<input type='hidden' id='tid' name='tid' value='<?php echo $tender_details['tid']?>' />
						<div class="row">
							<div class="panel-body">
								<div class="col-sm-6">
									<h4 class="text-main"><span style="font-size:16px; " class="label label-primary">Company Name</span> : <small><?php echo $company[0]['vendor_name']; ?></small></h4>
								</div>
								<div class="col-sm-6">
									<label class="control-label"><span style="font-size:14px; " class="label label-info	">Note</span>: <span>  For any information or help click on this <i style="margin-left: 10px;" class="fa fa-info-circle info_pop" aria-hidden="true"></i></span></label>
								</div>
							</div>
						</div>
						<div class="panel-body">
						

							<div class="col-sm-6">
									<div class="form-group">
										<div class="col-sm-5">
											<label class="control-label">E-Tender Sale No<span class="text-danger"><i class="fa fa-asterisk" aria-hidden="true"></i></span></label>
										</div>
										<div class="col-sm-7">
											<input type='text' class='form-control' name='tender_id' value='<?php echo $tender_details['tender_id'];?>' readonly>
										</div>
									</div>
							</div>
						
							<inuput type="hidden" name="auctiontype" id="auctiontype" value="5" />

							<div style='display:none' class="col-sm-6">
									<div class="form-group">
										<div class="col-sm-5">
											<label class="control-label">Auction Paritcipation<span class="text-danger"><i class="fa fa-asterisk" aria-hidden="true"></i></span></label>
										</div>
										<div class="col-sm-7">
											<select class="form-control" name="auction_visibility" id="auction_visibility">
												<option value="1" >Allowed for all</option>
												<option value="2" >Allowed for registered vendors</option>
												<option value="3" >Allowed for participant only</option>
											</select>
											<div class="text-dark" id="all" style="display:none">(<font color="#FF0000">23:59:59</font>)</div>
											<div class="text-dark" id="register" style="display:none">(24 hrs time format ex: <font color="#FF0000">23:59:59</font>)</div>
											<div class="text-dark" id="participant" style="display:none">(24 hrs time format ex: <font color="#FF0000">23:59:59</font>)</div>
										</div>
									</div>
							</div>

							<div class="col-sm-6">
									<div class="form-group">
										<div class="col-sm-5">
											<label class="control-label">Review Vendor For Participation<span class="text-danger"><i class="fa fa-asterisk" aria-hidden="true"></i></span>
												<a type='button' class="add-popover" data-toggle="popover" data-placement="top" data-original-title="Vendor Participation Control" data-content="Yes : {A vendor can express an interest by seeing the auction details and can be reviewed under Review Auction interested vendors	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; No :  A vendor can directly join the auction after seeing the auction details.">
													<i class="fa fa-info-circle info_pop" aria-hidden="true"></i>
												</a>
											</label>
										</div>
										<div class="col-sm-7">
											<select id='review_vendor_for_participation' name="review_vendor_for_participation" class="form-control" onchange="interest_notify(this.value)">
												<option value=''>--Select one--</option>
												<option value="1" <?php echo ($tender_details['review_vendor_for_participation'] ==1)? 'selected':''?>>Yes</option>
												<option value="0" <?php echo ($tender_details['review_vendor_for_participation'] ==0)? 'selected':''?>>No</option>
											</select>
										</div>
									</div>
							</div>

							<div class="col-sm-6 notify_interest" style="display:none;">
									<div class="form-group">
										<div class="col-sm-5">
											<label class="control-label">Get SMS/Email<span class="text-danger"><i class="fa fa-asterisk" aria-hidden="true"></i></span>
												<a type='button' class="add-popover" data-toggle="popover" data-placement="top" data-original-title="Vendor Interest" data-content="Notify when a vendor shows an interest to participate">
													<i class="fa fa-info-circle info_pop" aria-hidden="true"></i>
												</a>
											</label>
										</div>
										<div class="col-sm-7">
											<select id='notify_vendor_interest' name="notify_vendor_interest" class="form-control">
												<option value="">--Select one--</option>
												<option value="1" <?php echo ($tender_details['notify_vendor_interest'] =='1')? 'selected':''?>>Yes</option>
												<option value="0" <?php echo ($tender_details['notify_vendor_interest'] =='0')? 'selected':''?>>No</option>
											</select>
										</div>
									</div>
							</div>
							
							
							<div class="col-sm-6">
									<div class="form-group">
										<div class="col-sm-5">
											<label class="control-label">Material Category<span class="text-danger"><i class="fa fa-asterisk" aria-hidden="true"></i></span></label>
										</div>
										<div class="col-sm-7">
											<select class="form-control select2" data-placeholder="Select an option" name="mat_id[]" id="mat_id"  multiple tabindex="-1" aria-hidden="true">
											<?php 
												if($mat_category):
													foreach($mat_category as $key=>$mat):
											?>
													 <optgroup label="<?php echo $key?>">
													 </optgroup>
											<?php
														if($mat):
															foreach($mat as $k=>$v):
											?>
															<option class="level_1" value="<?php echo $k?>"><?php echo $v?></option>
											<?php
															endforeach;
														endif;
													endforeach;
												endif;
											?>
											</select>		
										</div>
									</div>
							</div>

					
							<div class="col-sm-6">
									<div class="form-group">
										<div class="col-sm-5">
											<label class="control-label">Tender Commencement Date<span class="text-danger"><i class="fa fa-asterisk" aria-hidden="true"></i></span></label>
										</div>
										<div class="col-sm-7">
											<input class="form-control" placeholder='YYYY-MM-DD' type="text" name="tender_commencement_date" id="tender_commencement_date" readonly value='<?php echo $tender_details['tcdt']?>'  />
										</div>
									</div>
							</div>

							<div class="col-sm-6">
									<div class="form-group">
										<div class="col-sm-5">
											<label class="control-label">Tender Commencement Time<span class="text-danger"><i class="fa fa-asterisk" aria-hidden="true"></i></span></label>
										</div>
										<div class="col-sm-7">
											<input class="form-control" placeholder="HH:MM:SS" type="text" name="commencement_time" id="commencement_time" value='<?php echo ! empty($tender_details['tctime']) ? $tender_details['tctime'] : "" ?>'  />
										</div>
									</div>
							</div>
					
							<div class="col-sm-6">
									<div class="form-group">
										<div class="col-sm-5">
											<label class="control-label">Tender Opening Date<span class="text-danger"><i class="fa fa-asterisk" aria-hidden="true"></i></span></label>
										</div>
										<div class="col-sm-7">
											<input class="form-control" placeholder='YYYY-MM-DD' type="text" name="tender_opening_date" id="tender_opening_date" readonly  value='<?php echo $tender_details['topen_dt'] ?>' />
										</div>
									</div>
							</div>

							<div class="col-sm-6">
									<div class="form-group">
										<div class="col-sm-5">
											<label class="control-label">Tender Opening Time<span class="text-danger"><i class="fa fa-asterisk" aria-hidden="true"></i></span></label>
										</div>
										<div class="col-sm-7">
											<input class="form-control" placeholder="HH:MM:SS" type="text" name="topen_time" id="start_time" value='<?php echo $tender_details['topen_time'] ?>' />
										</div>
									</div>
							</div>

							
							<div class="col-sm-6">
									<div class="form-group">
										<div class="col-sm-5">
											<label class="control-label">Tender Closing Date<span class="text-danger"><i class="fa fa-asterisk" aria-hidden="true"></i></span></label>
										</div>
										<div class="col-sm-7">
											<input class="form-control " placeholder='YYYY-MM-DD' type="text" name="tender_closing_date" id="tender_closing_date" readonly value="<?php echo $tender_details['tclose_dt']; ?>" />
										</div>
									</div>
							</div>

							<div class="col-sm-6">
									<div class="form-group">
										<div class="col-sm-5">
											<label class="control-label">Tender Closing Time<span class="text-danger"><i class="fa fa-asterisk" aria-hidden="true"></i></span></label>
										</div>
										<div class="col-sm-7">
											<input class="form-control" placeholder="HH:MM:SS" type="text" name="tclose_time" id="end_time" value="<?php echo $tender_details['tclose_time']; ?>" />
										</div>
									</div>
							</div>

							<div class="col-sm-6">
									<div class="form-group">
										<div class="col-sm-5">
											<label class="control-label">Last Date to Submit EMD<span class="text-danger"><i class="fa fa-asterisk" aria-hidden="true"></i></span></label>
										</div>
										<div class="col-sm-7">
											<input class="form-control" placeholder='YYYY-MM-DD' type="text" name="last_cmd_sub_dt" id="last_cmd_sub_dt" readonly value='<?php echo $tender_details['last_cmd_sub_dt']; ?>' />
										</div>
									</div>
							</div>

							<div class="col-sm-6">
									<div class="form-group">
										<div class="col-sm-5">
											<label class="control-label">CMD EMD Closing Time<span class="text-danger"><i class="fa fa-asterisk" aria-hidden="true"></i></span></label>
										</div>
										<div class="col-sm-7">
											<input class="form-control" placeholder="HH:MM:SS" type="text" name="emd_time" id="emd_time" value='<?php echo $tender_details['last_cmd_sub_time']; ?>' />
										</div>
									</div>
							</div>
							<?php if ($this->session->userdata('registration_type') == 1){ ?>
							<div class="col-sm-6">
									<div class="form-group">
										<div class="col-sm-12">
											<label class="control-label">Do You Want to Conduct the Auction for Linked Company <input type="checkbox" id="show_company_plant" style="margin: 25px 0px 0;" name="linked_company" value="1" /></label>
										
										</div>
									</div>
							</div>
							<?php } ?>

					
						
							
							<div class="other_company col-sm-6">
									<div class="form-group">
										<div class="col-sm-5">
											<label class="control-label">Company<span class="text-danger"><i class="fa fa-asterisk" aria-hidden="true"></i></span></label>
										</div>
										<div class="col-sm-7">
											<select class="form-control" size="1" name="vendor_id" id="vendor_id">
											</select>
										</div>
									</div>
							</div>
							
							<div class="other_company vendor_plant col-sm-6">
									<div class="form-group">
										<div class="col-sm-5">
											<label class="control-label">Plant<span class="text-danger"><i class="fa fa-asterisk" aria-hidden="true"></i></span></label>
										</div>
										<div class="col-sm-7">
											<select class="form-control" size="1" name="plant[]" id="plant" multiple="multiple">
												<?php
													if($vendor_company_id != $_SESSION['vendor_id'] && ! empty($plant_id)):
														$plant_ids = explode(",",$plant_id);
														$plant_names = explode(",",$plant_name);
														for($x = 0; $x < sizeof($plant_ids); $x++):
														{
															$selected = "selected";
															?><option value='<?php echo $plant_ids[$x]; ?>' <?php echo $selected; ?>><?php echo $plant_names[$x];?></option><?php
														}
														endfor;
													endif;
												?>
											</select>
										</div>
									</div>
							</div>

							<div class="col-sm-6">
									<div class="form-group">
										<div class="col-sm-5">
											<label class="control-label">Bid type<span class="text-danger"><i class="fa fa-asterisk" aria-hidden="true"></i></span></label>
										</div>
										<div class="col-sm-7">
											<select class="form-control" size="1" name="bid_type">
												<option value="I" <?php if($tender_details['bid_type']=="I") echo " selected";?>>Incremental</option>
												<option value="D" <?php if($tender_details['bid_type']=="D") echo " selected";?>>Decremental</option>
											</select>
										</div>
									</div>
							</div>
					
							
							<div class="col-sm-6">
									<div class="form-group">
										<div class="col-sm-5">
											<label class="control-label">Location / Plant<span class="text-danger"><i class="fa fa-asterisk" aria-hidden="true"></i></span></label>
										</div>
										<div class="col-sm-7">
											<input class="form-control" type="text" name="location" maxlength='200' value="<?php echo $tender_details['location'];?>">
										</div>
									</div>
							</div>

							<div class="col-sm-6">
									<div class="form-group">
										<div class="col-sm-5">
											<label class="control-label">Sector</label>
										</div>
										<div class="col-sm-7">
											<input class="form-control" type="text" name="sector" maxlength='200' value="<?php echo $tender_details['sector'];?>" />
										</div>
									</div>
							</div>

							<div class="col-sm-6">
									<div class="form-group">
										<div class="col-sm-5">
											<label class="control-label">Particulars<span class="text-danger"><i class="fa fa-asterisk" aria-hidden="true"></i></span></label>
										</div>
										<div class="col-sm-7">
											<textarea class="form-control" rows="3" name="particulars" cols="47"><?php echo $tender_details['particulars'];?></textarea>
										</div>
									</div>
							</div>
							<input type="hidden" name="show_amount" value="N" />
							<div class="col-sm-6">
									<div class="form-group">
										<div class="col-sm-5">
											<label class="control-label">Show H1/L1 Indication<span class="text-danger"><i class="fa fa-asterisk" aria-hidden="true"></i></span></label>
										</div>
										<div class="col-sm-7">
											<select class="form-control" size="1" name="show_h1_l1_indication">
												<option value="N" <?php if($tender_details['show_h1_l1_indication']=="N") echo " selected";?>>No</option>
												<option value="Y" <?php if($tender_details['show_h1_l1_indication']=="Y") echo " selected";?>>Yes</option>
											</select>
										</div>
									</div>
							</div>
							<input type="hidden" name="part_bid" value="N" />

							<div style='display:none;' class="col-sm-6">
									<div class="form-group">
										<div class="col-sm-5">
											<label class="control-label">Lot Sub Groups<span class="text-danger"><i class="fa fa-asterisk" aria-hidden="true"></i></span>
												<a type='button' class="add-popover" data-toggle="popover" data-placement="top" data-original-title="Sub Lots" data-content="Get Sub group as a lot. Eg. Lot Desc Computer > Sub lots Monitor, CPU, Mouse, Keyboard">
													<i class="fa fa-info-circle info_pop" aria-hidden="true"></i>
												</a>
											</label>
										</div>
										<div class="col-sm-7">
											<select class="form-control" name="is_lot_group" id="is_lot_group" onchange="is_lot_group_change();">
												<option value="No" <?php echo (isset($tender_details['is_lot_group']) && $tender_details['is_lot_group']=="No") ? 'selected' : '' ; ?> >NO</option>
												<option value="Yes" <?php echo (isset($tender_details['is_lot_group']) && $tender_details['is_lot_group']=="Yes") ? 'selected' : '' ; ?> >YES</option>
											</select>
										</div>
									</div>
							</div>

							
							<div class="col-sm-6 maintain_inc_in_sublot">
									<div class="form-group">
										<div class="col-sm-5">
											<label class="control-label">Lot Sub Groups Maintain Sub lot Incr/Decr<span class="text-danger"><i class="fa fa-asterisk" aria-hidden="true"></i></span>
												<a type='button' class="add-popover" data-toggle="popover" data-placement="top" data-original-title=" Sub Lot Incr/Decr" data-content="Maintain increment in all sublot or only in main lot. If Yes then system check incr/decr in all sub lots">
													<i class="fa fa-info-circle info_pop" aria-hidden="true"></i>
												</a>
											</label>
										</div>
										<div class="col-sm-7">
											<select class="form-control" name="maintain_inc_in_sublot" id="maintain_inc_in_sublot">
												<option value="No" <?php echo (isset($tender_details['maintain_inc_in_sublot']) && $tender_details['maintain_inc_in_sublot']=="No") ? 'selected' : '' ; ?> >NO</option>
												<option value="Yes" <?php echo (isset($tender_details['maintain_inc_in_sublot']) && $tender_details['maintain_inc_in_sublot']=="Yes") ? 'selected' : '' ; ?> >YES</option>
											</select>
										</div>
									</div>
							</div>
			
							<div class="col-sm-6">
									<div class="form-group">
										<div class="col-sm-5">
											<label class="control-label">CMD Amount
												<a type='button' class="add-popover" data-toggle="popover" data-placement="top" data-original-title="Overall CMD" data-content="Maintain increment in all sublot or only in main lot. If Yes then system check incr/decr in all sub lots">
													<i class="fa fa-info-circle info_pop" aria-hidden="true"></i>
												</a>
											</label>
										</div>
										<div class="col-sm-7">
											<input placeholder="CMD Amount" type="text" class="form-control" name="cmd" id="cmd" value="<?php echo (isset($tender_details['cmd'])) ? $tender_details['cmd'] : ''; ?>"/>
										</div>
									</div>
							</div>

							
							<div class="col-sm-6">
									<div class="form-group">
										<div class="col-sm-5">
											<label class="control-label">Tender Images<span class="text-danger"><i class="fa fa-asterisk" aria-hidden="true"></i></span>
										</div>
										<div class="col-sm-7">
											<?php 
											if (empty($tender_details['tid']))
											{
											?>
												<span class="pull-left btn btn-primary btn-file btn-labeled fa fa-folder-open">
													Browse.. <input name="file[]" multiple type="file">
												</span>
											<?php 
											}
											else
											{
											?>
												
												<a href="<?php echo base_url()?>auctioneer/auction/auction_images/t/<?php echo $tender_details['tid'];?>" target="_new">Edit Image</a>
											<?php
											}
											?>
										</div>
											
										</label>
									</div>
							</div>

							<div class="col-sm-6">
									<div class="form-group">
										<div class="col-sm-5">
											<label>Stop Bidding from Same IP for Multiple Bidders
												<a type='button' class="add-popover" data-toggle="popover" data-placement="top" data-original-title="IP Blocker" data-content=" Select only if any company want to stop bidding from same ip from multiple bidders">
													<i class="fa fa-info-circle info_pop" aria-hidden="true"></i>
												</a>
											</label>
										</div>
										<div class="col-sm-7">
											<input style='height:20px;' type="checkbox" class="form-control" name="chk_stop_bidding_same_ip" id="chk_stop_bidding_same_ip" value="1" onchange="change_stop_bidding_chk()" />
											<input type="hidden" name="stop_bidding_same_ip" id="stop_bidding_same_ip" value="0" />
										</div>
									</div>
							</div>
						</div>
						<div class="form-group">
								<label class="col-sm-2 control-label">&nbsp;</label>
								<div style="text-align: center" class="col-sm-12">
									<input class="btn btn-success" type="submit" value='Submit' name="btn_submit" id='btn_submit' />
									<a class="btn btn-danger" href='<?php echo base_url('auctioneer/tender/index'); ?>'>Back</a>
								</div>
							</div>
					</form>
					<!--===================================================-->
					<!--End Horizontal Form-->
				</div>
				<!--===================================================-->
				<!--End Primary Panel-->
			</div>

		</div>
	</div>
	<!--===================================================-->
	<!--End page content-->
</div>
<!--===================================================-->
<!--END CONTENT CONTAINER-->

<script src="https://cdnjs.cloudflare.com/ajax/libs/datepicker/0.6.5/datepicker.js" integrity="sha256-KgOC04qt96w+EFba7KuNt9sT+ahJua25I0I2rrkZHFo=" crossorigin="anonymous"></script>
<script src="<?php echo base_url('assets/js/mdtimepicker.js'); ?>"></script>

<script>

function is_lot_group_change(){
	var option = $('#is_lot_group').val();
	if(option === "Yes"){
		$(".maintain_inc_in_sublot").show();
	}else{
		$(".maintain_inc_in_sublot").hide();
	}
}

$(document).ready(function() {
	get_mat();
	$("#auction_conduct_type").select2({
		placeholder: 'Conduct Type'
	})
	is_lot_group_change();
	$('#vendor_id').select2({
		placeholder: "Select Company"
	});
	$('#plant').select2({
		placeholder: "Select Plant"
	});
	$('#mat_id').select2({
		placeholder: "Select Material"
	});
	interest_notify(<?php echo ! empty($tender_details['notify_vendor_interest']) ? $tender_details['notify_vendor_interest'] : 0; ?>);

	$(".other_company").hide();
	var registration_type = "<?php echo $registration_type ?>";
	if(registration_type == 1)
	{
		$.ajax({
			url: "<?php echo base_url('auctioneer/tender/get_company_details') ?>",
			type: "POST",
			dataType: "json",
			success: function(data){
				var html = '<option></option>';
				var i;
				for (i=0; i<data.length; i++){
					html += '<option value="'+data[i].company_id+'">'+data[i].vendor_name+'</option>';
				}
				$('#vendor_id').html(html);
			}
		});
	} else {
		$("#vendor_id").hide();
		var vendor_company_id = "<?php echo $vendor_company_id ?>";
		$.ajax({
				url: "<?php echo base_url('auctioneer/tender/get_plant_details') ?>",
				type: "POST",
				data:{
					'company_id': vendor_company_id
				},
				dataType: "json",
				success: function(data)
				{
					var html = '';
					var i;
					for(i=0; i<data.length; i++)
					{
						html += '<option value="'+data[i].company_plant_id+'">'+data[i].plant_name+'</option>';
					}
					$('#plant').html(html);
				}
		});
	}
	var company_id = <?php echo ! empty($company_id) ? $company_id : "000"; ?>;
	var plant_id = [<?php echo ! empty($plant_id) ? $plant_id : "000"; ?>];
	if(company_id === "000")
	{
		document.getElementById("show_company_plant").checked = true;
		$(".other_company").show();
		$("#vendor_id").val(company_id).select2();
	}
	var startComDate;
	<?php if ( ! empty($tender_details['tcdt'])){ ?>
		$("#tender_commencement_date").datepicker({
			format: 'yyyy-mm-dd',
			startDate: new Date("<?php echo $tender_details['tcdt'] ?>")
		});
		$("#tender_opening_date").datepicker({
			format: 'yyyy-mm-dd',
			startDate: new Date("<?php echo $tender_details['topen_dt'] ?>")
		});
		$("#tender_closing_date").datepicker({
			format: 'yyyy-mm-dd',
			startDate: new Date("<?php echo $tender_details['tclose_dt'] ?>")
		});
		$("#last_cmd_sub_dt").datepicker({
			format: 'yyyy-mm-dd',
			startDate: new Date("<?php echo $tender_details['last_cmd_sub_dt'] ?>"),
			endDate: new Date("<?php echo $tender_details['tclose_dt'] ?>")
		});
	<?php } else { ?>
		$("#tender_commencement_date").datepicker({
			format: 'yyyy-mm-dd',
			startDate: new Date("<?php echo $today_date ?>")
		});

	<?php } ?>
	
    $("#tender_commencement_date").change(function(){
        startComDate = '"'+$("#tender_commencement_date").val()+'"';
		$("#tender_opening_date").val('');
		$("#tender_opening_date").datepicker('destroy');
        $("#tender_opening_date").datepicker({
            format: 'yyyy-mm-dd',
            startDate: new Date(startComDate)
        });
    });
	$("#tender_opening_date").change(function() {
    	openDate = '"'+$("#tender_opening_date").val()+'"';
		$("#tender_closing_date").val('');
		$("#tender_closing_date").datepicker('destroy');
		$("#tender_closing_date").datepicker({
    		format: 'yyyy-mm-dd',
    		startDate: new Date(openDate)
        });
    });

    $("#tender_closing_date").change(function(){
		startComDate = '"'+$("#tender_commencement_date").val()+'"';
       	closeDate = '"'+$("#tender_closing_date").val()+'"';
		$("#last_cmd_sub_dt").val('');
		$("#last_cmd_sub_dt").datepicker('destroy');
		$("#last_cmd_sub_dt").datepicker({
    		format: 'yyyy-mm-dd',
			startDate : new Date(startComDate),
    		endDate: new Date(closeDate)
        });
    });

	$('#start_time').mdtimepicker({
		format: 'hh:mm:ss',
		theme: 'blue'
	}); //Initializes the time picker and uses the specified format (i.e. 23:30)
	$('#end_time').mdtimepicker({
		format: 'hh:mm:ss',
		theme: 'blue'
	}); //Initializes the time picker and uses the specified format (i.e. 23:30)
	$('#commencement_time').mdtimepicker({
		format: 'hh:mm:ss',
		theme: 'blue'
	}); //Initializes the time picker and uses the specified format (i.e. 23:30)
	$('#emd_time').mdtimepicker({
		format: 'hh:mm:ss',
		theme: 'blue'
	}) //Initializes the time picker and uses the specified format (i.e. 23:30)

})
function get_mat()
{
	var mat  = '<?php echo $tender_details['mat_id']; ?>';
	var mat_arr = mat.split(',');
	$.each(mat_arr,function(index,obj)
	{
		$('select[name^="mat_id"] option[value="'+obj+'"]').attr("selected","selected");
	});
} 
$("#show_company_plant").change(function() {
	var id = document.getElementById('show_company_plant');
	var registration_type = "<?php echo $registration_type; ?>";
	if (id.checked === true) {
		if(registration_type == 1)
		{
			$(".other_company").show();
		} else {
			$(".vendor_plant").show();
		}
	} else {
		if(registration_type == 1)
		{
			$('#vendor_id').val(null).trigger('change');
			$('#plant').val(null).trigger('change');
			$(".other_company").hide();
		} else {
			$('#plant').val(null).trigger('change');
			$(".vendor_plant").hide();
		}
	}
})

$("#vendor_id").change(function(){
	//$("#plant").select2().empty();
	$("#plant").select2('val', '');
	var company_id = $("#vendor_id").val();
	$.ajax({
		url: "<?php echo base_url('auctioneer/tender/get_plant_details') ?>",
		type: "POST",
		data:{
			'company_id': company_id
		},
		dataType: "json",
		success: function(data)
		{
			var html = '';
			var i;
			for(i=0; i<data.length; i++)
			{
				html += '<option value="'+data[i].company_plant_id+'">'+data[i].plant_name+'</option>';
			}
			$('#plant').html(html);
		}
	});
});


function change_stop_bidding_chk(){
	var option = $('#chk_stop_bidding_same_ip').is(':checked');
	if(option){
		$("#stop_bidding_same_ip").val("1");
	}else{
		$("#stop_bidding_same_ip").val("0");
	}
}

$(function(){
	<?php
	if($tender_details['stop_bidding_same_ip'] == "1"){
		?>
		$("#chk_stop_bidding_same_ip").prop('checked', true);
		$("#stop_bidding_same_ip").val("1");
		<?php
	}
	?>

	$.validator.addMethod('start_commencement_time', function (value, element, param) {
	return this.optional(element) || check_com_start_time();
	}, "<p class='text-danger'>Enter Correct Commencement Time and Opening time</p>");

	$.validator.addMethod('compare_time', function (value, element, param) {
	return this.optional(element) || check_start_end_time();
	}, "<p class='text-danger'>Enter Correct start time and end time</p>");

	$.validator.addMethod('end_emd_time', function (value, element, param) {
	return this.optional(element) || check_emd_end_time();
	}, "<p class='text-danger'>Enter Correct End Time and EMD time</p>");

	function check_com_start_time(){
		var commencementTime = '"'+$("#commencement_time").val()+'"';
		var startTime = '"'+$("#start_time").val()+'"';
		var startDate = $("#tender_opening_date").val();
		var commencementDate = $("#tender_commencement_date").val();
		if (startDate == commencementDate) {
			if (commencementTime >= startTime)
			{
				return false;
			}
			else
			{
				return true;
			}
		} else {
			return true;
		}
	}

	function check_start_end_time(){
		var startTime = '"'+document.getElementById("start_time").value+'"';
		var endTime = '"'+document.getElementById("end_time").value+'"';
		var startDate = document.getElementById("tender_opening_date").value;
		var endDate = document.getElementById("tender_closing_date").value;
		if (startDate == endDate) {
			if (startTime >= endTime)
			{
				return false;
			}
			else
			{
				return true;
			}
		} else {
			return true;
		}
	}

	function check_emd_end_time(){
		var emdTime = '"'+document.getElementById("emd_time").value+'"';
		var endTime = '"'+document.getElementById("end_time").value+'"';
		var emdDate = document.getElementById("last_cmd_sub_dt").value;
		var endDate = document.getElementById("tender_closing_date").value;
		if (emdDate == endDate) {
			if (emdTime >= endTime)
			{
				return false;
			}
			else
			{
				return true;
			}
		} else {
			return true;
		}
	}

	var vRules = {
		tender_id: { required:true },
		tender_commencement_date: { required:true },
		commencement_time: { required:true },
		start_time: {
			required:true,
			start_commencement_time: "#commencement_time"
		},
		end_time: {
			required: true,
			compare_time: "#start_time"
		},
		emd_time: {
			required:true,
			end_emd_time: "#end_time"
		},
		last_cmd_sub_dt: {required: true},
		tender_opening_date: { required:true },
		tender_closing_date: { required:true },
		vendor_id: { required:true },
		particulars: { required:true },
		location: { required:true },
		type:{required:true}
	};

	var vMessages = {
		tender_id: { required:"<p class='text-danger'>Please Enter The E-Tender Sale No</p>" },
		tender_commencement_date: { required:"<p class='text-danger'>Please Select Date</p>" },
		commencement_time: { required:"<p class='text-danger'>Please Enter Tender Commencement Time</p>" },
		start_time: { required:"<p class='text-danger'>Please Enter Tender Opening Time</p>" },
		end_time: { required:"<p class='text-danger'>Please Enter Tender Closing Time</p>" },
		emd_time: { required:"<p class='text-danger'>Please Enter CMD Emd Closing Time</p>" },
		last_cmd_sub_dt: { required:"<p class='text-danger'>Please Enter CMD Emd Closing Date</p>" },
		tender_opening_date: { required:"<p class='text-danger'>Please Select Date</p>" },
		tender_closing_date: { required:"<p class='text-danger'>Please Select Date</p>" },
		vendor_id: { required:"<p class='text-danger'>Please Enter The Company </p>" },
		particulars: { required:"<p class='text-danger'>Please Enter The Particulars</p>" },
		location: { required:"<p class='text-danger'>Please Enter Tender Location</p>" },
		type:{required:"<p class='text-danger'>Please select type of Tender you want to conduct</p>"}
	};

	$("#form_tender_addedit").validate({
		rules: vRules,
		messages: vMessages,
		submitHandler: function(form){
			$(form).ajaxSubmit({
				url:"<?php echo base_url('auctioneer/tender/update_data/');?>",
				type: 'post',
				dataType:'json',
				cache: false,
				clearForm: false,
				success: function (response) {
					if(response.status=="success")
					{
						Display_msg(response.message,response.status);
						setTimeout(function()
						{
							window.location.href="<?php echo base_url('auctioneer/tender/index');?>";
						}, 3000);
					}
					else
					{
						Display_msg(response.message,response.status);
						return false;
					}
				}
			});
		}
	})
});
function interest_notify(val)
{
	if (val==1){
		$('.notify_interest').show();
	} else if (val==0) {
		$('.notify_interest').hide();
	}
}
$('body').on('click', function (e) {
    $('[data-toggle=popover]').each(function () {
        // hide any open popovers when the anywhere else in the body is clicked
        if (!$(this).is(e.target) && $(this).has(e.target).length === 0 && $('.popover').has(e.target).length === 0) {
            $(this).popover('hide');
        }
    });
});
</script>
