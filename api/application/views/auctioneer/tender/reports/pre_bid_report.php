<style>
tr {
	page-break-inside: avoid;
}
thead {
	display: table-header-group;
}
tfoot {
	display: table-row-group;
}
tr {
	page-break-inside: avoid;
}
td.wrappable,table.data_table td.wrappable {
    white-space: normal;	
}            
table td,table th {
	font-size:10px;
	
	
}
</style>


<page backbottom='110px' backtop="0px" backleft="0px" backright="0px" style="font-size:10px;">
   <page_footer>
        <table style='width: 100%;'>
			<tr>
				<td align='left' style='float:left;width:200px;'>
				Page :  [[page_cu]]/[[page_nb]] <br/>
				<?php echo (new DateTime(null, new DateTimeZone($_SESSION['user_tz'])))->format('D dS \of M Y h:i:s A'); ?>
				
				</td>
				<td align='right' style='float:right;width:545px;'>
					
				</td>
			</tr>
		</table>
    </page_footer>
	<br>
	<table border='0.5' cellspacing='0' cellpadding='0' width='100%'>
		<tbody>
			<tr>
				<td style="width:550px;" >
								
				</td>
				<td align='center' style="width:180px;">
					<span style='font-size:20px;color:#2a5395;'><strong><?php echo PRODUCT ?></strong></span>
				</td>
			</tr>
			<tr>
				<td colspan='2' align='center' >
					<strong>PRE BID REPORT FOR TENDER NO :  <?php echo $common_auction_details['saleno']; ?> </strong>
				</td>
			</tr>
			<tr>
				<td colspan='2' align='center' >
					<strong><?=strtoupper($common_auction_details['vendor_name'])?></strong>
				</td>
			</tr>
			<tr>
				<td colspan='2' align='center' >
					<strong>TENDER DETAILS</strong>
				</td>
			</tr>
			<tr>
				<td colspan='2' align='left'>
					<strong>Tender No :</strong><?php echo $common_auction_details['saleno']?>
				</td>
			</tr>
			<tr>
				<td colspan='2' align='left'>
					<strong>Tender Commencement Date : </strong><?php echo $common_auction_details['tcdate'].$common_auction_details['tctime']?>
				</td>
			</tr>
			<tr>
				<td colspan='2' align='left'>
					<strong>Tender Closing Date : </strong><?php echo $common_auction_details['edt']?>
				</td>
			</tr>
			<tr>
				<td colspan='2' align='left'>
					<strong>Particulars : </strong><?php echo $common_auction_details['particulars']?>
				</td>
			</tr>
			<tr>
				<td colspan='2' align='left'>
					<strong>No. of Lots : </strong><?php echo $common_auction_details['no_of_lots']?>
				</td>
			</tr>
		</tbody>		
	</table>
	<br>
	<br>	
	<table border='0.5' cellspacing='0' cellpadding='0'>
		<thead>
			<tr>
				<th align='center' class='wrappable' style='font-weight:bold;' >SR.<br/>NO.</th>
				<th align='center' class='wrappable' style='font-weight:bold;' >Material<br/>Code</th>
				<th align='center' class='wrappable' style='font-weight:bold;' >Description</th>
				<th align='center' class='wrappable' style='font-weight:bold;' >Plant</th>
				<th align='center' class='wrappable' style='font-weight:bold;' >Total Qty</th>
				<th align='center' class='wrappable' style='font-weight:bold;' >Bid Details</th>
			</tr>
		</thead>
		<tbody>	
			<?php
			
				if (!empty($pre_bid))
				{
					$I = 1;
					foreach($pre_bid['auction_details'] as $key=>$value)
					{
						
					?>
						
						<tr>
							<td align='center' class='wrappable' style='width:20px;'>
								<?php echo $I++;?>
							</td>
							<td align='center' class='wrappable' style='width:50px;'>
								<?php echo $value['mat_code'];?>
							</td>
							<td align='center' class='wrappable' style='width:153px;'>
								<?php echo $value['description'];?>
							</td>
							<td align='center' class='wrappable' style='width:60px;'>
								<?php echo $value['plant'];?>
							</td>
							<td align='center' class='wrappable' style='width:60px;'>
								<?php echo $value['lot_qty'].$value['lot_unit'];?>
							</td>
							<td align='center' style='width:362px;' class='wrappable' >
								<?php 
								
									if (isset($value['lot']) )
									{
								?>
								
									<table border='0.5' cellspacing='0' cellpadding='0'  >
											<thead>
												<tr>
													<th align='center' class='wrappable' style='font-weight:bold;width:140px;' bgcolor="#e0e0e0">Party Name</th>
													<th align='center' class='wrappable' style='font-weight:bold;width:60px;' bgcolor="#e0e0e0">Bid Amount</th>
													<th align='center' class='wrappable' style='font-weight:bold;width:60px;' bgcolor="#e0e0e0">Bid Qty</th>
													<th align='center' class='wrappable' style='font-weight:bold;width:80px;' bgcolor="#e0e0e0">IP</th>
												</tr>
											</thead>
											<tbody>
											<?php
													foreach($value['lot'] as $k=>$v)
													{
														if($v['amount'] > 0)
														{ 
													?>
														<tr>
															<td align='center' class='wrappable' style='width:140px;' ><?=$v['compname']?></td>
															<td align='center' class='wrappable' style='width:60px;' ><?=$v['amount']?></td>
															<td align='center' class='wrappable' style='width:60px;' ><?=$v['quantity']?></td>
															<td align='center' class='wrappable' style='width:80px;' ><?=$v['ip']?></td>
														</tr>
													
													<?php
														}
										
													}
												?>
												
												
											</tbody>
										</table>
									<?php			
									} else {
										?>No data is available <?php
									}
								?>
							</td>
						</tr>
					<?php	
					}
					
				}
			
			?>
		</tbody>
	</table>
	
</page>