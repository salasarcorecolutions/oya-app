<style>
tr {
	page-break-inside: avoid;
}
thead {
	display: table-header-group;
}
tfoot {
	display: table-row-group;
}
tr {
	page-break-inside: avoid;
}
td.wrappable,table.data_table td.wrappable {
    white-space: normal;	
}            
table td,table th {
	font-size:10px;
	
	
}
</style>


<page backbottom='110px' backtop="0px" backleft="0px" backright="0px" style="font-size:10px;">
   <page_footer>
        <table style='width: 100%;'>
			<tr>
				<td align='left' style='float:left;width:200px;'>
				Page :  [[page_cu]]/[[page_nb]] <br/>
				<?php echo (new DateTime(null, new DateTimeZone($_SESSION['user_tz'])))->format('D dS \of M Y h:i:s A'); ?>
				
				</td>
				<td align='right' style='float:right;width:545px;'>
					
				</td>
			</tr>
		</table>
    </page_footer>
	<br>
	<table border='0.5' cellspacing='0' cellpadding='0' width='100%'>
		<tbody>
			<tr>
				<td style="width:550px;" >
								
				</td>
				<td align='center' style="width:180px;">
					<span style='font-size:20px;color:#2a5395;'><strong><?php echo PRODUCT ?></strong></span>
				</td>
			</tr>
			<tr>
				<td colspan='2' align='center' >
					<strong>LOT DETAILS FOR TENDER NO :  <?php echo $common_auction_details['saleno']; ?> </strong>
				</td>
			</tr>
			<tr>
				<td colspan='2' align='center' >
					<strong><?=strtoupper($common_auction_details['vendor_name'])?></strong>
				</td>
			</tr>
			<tr>
				<td colspan='2' align='center' >
					<strong>TENDER DETAILS</strong>
				</td>
			</tr>
			<tr>
				<td colspan='2' align='left'>
					<strong>Tender No :</strong><?php echo $common_auction_details['saleno']?>
				</td>
			</tr>
			<tr>
				<td colspan='2' align='left'>
					<strong>Tender Commencement Date : </strong><?php echo $common_auction_details['tcdate'].$common_auction_details['tctime']?>
				</td>
			</tr>
			<tr>
				<td colspan='2' align='left'>
					<strong>Tender Closing Date : </strong><?php echo $common_auction_details['edt']?>
				</td>
			</tr>
			<tr>
				<td colspan='2' align='left'>
					<strong>Particulars : </strong><?php echo $common_auction_details['particulars']?>
				</td>
			</tr>
			<tr>
				<td colspan='2' align='left'>
					<strong>No. of Lots : </strong><?php echo $common_auction_details['no_of_lots']?>
				</td>
			</tr>
		</tbody>		
	</table>
	<br>
	<br>	
	<table border='0.5' cellspacing='0' cellpadding='0'>
		<thead>
			<tr>
				<th align='center' class='wrappable'  bgcolor="#e0e0e0" style='font-weight:bold;' >LOT NO.</th>
				<th align='center' class='wrappable' bgcolor="#e0e0e0" style='font-weight:bold;' >MAT.<br>CODE</th>
				<th align='center' class='wrappable' bgcolor="#e0e0e0" style='font-weight:bold;' >OPEN DT.</th>
				<th align='center' class='wrappable' bgcolor="#e0e0e0" style='font-weight:bold;' >CLOSING DT.</th>
				<th align='center' class='wrappable' bgcolor="#e0e0e0" style='font-weight:bold;' >DESCRIPTION</th>
				<th align='center' class='wrappable' bgcolor="#e0e0e0" style='font-weight:bold;' >RP</th>
				<th align='center' class='wrappable' bgcolor="#e0e0e0" style='font-weight:bold;' >PLANT</th>
				<th align='center' class='wrappable' bgcolor="#e0e0e0" style='font-weight:bold;' >MAX. NO. <br/>OF BID</th>
				<th align='center' class='wrappable' bgcolor="#e0e0e0" style='font-weight:bold;' >TIME <br/>BOUNDED?</th>
				<th align='center' class='wrappable' bgcolor="#e0e0e0" style='font-weight:bold;' >INCR/DECR</th>
				<th align='center' class='wrappable' bgcolor="#e0e0e0" style='font-weight:bold;'>CMD</th>				
			</tr>
		</thead>
		<tbody>	
			<?php 
				if ( ! empty($lot_details))
				{
					$i = 1;
					foreach($lot_details as $key=>$value)
					{
			?>
						<tr>	
						<td class='wrappable' align='center' style='width:40px;'>
							<?php echo $value['lotno']; ?>
						</td>
						<td class='wrappable' align='center' style='width:40px;'>
							<?php echo $value['mat_code']; ?>
						</td>
						<td class='wrappable' align='center' style='width:80px;'>
							<?php echo $value['lot_open_date']; ?> Hrs.
						</td>
						<td class='wrappable' align='center' style='width:88px;'>
							<?php echo $value['lot_closing_date']; ?> Hrs.
						</td>
						<td class='wrappable' align='center' style='width:60px;' >
							<?php echo $value['description']; ?>
						</td>
						<td class='wrappable' align='center' style='width:60px;'>
							<?php echo $value['rp']; ?>
						</td>
						<td class='wrappable' align='center' style='width:60px;'>
							&nbsp;<?php echo $value['plant']; ?>
						</td>
						<td class='wrappable' align='center' style='width:60px;'>
							<?php if($value['max_no_of_bids']==0) echo "Unlimited"; else echo $value['max_no_of_bids']; ?>
						</td>
						<td class='wrappable' align='center' style='width:60px;'>
							<?php 
								if($value['time_bounded'] == 'Y')
								{
									echo 'Yes';
								}
								else
								{
									echo 'No';
								}
							?>
						</td>
						<td class='wrappable' align='center' style='width:60px;'>
							<?php echo $value['minimum_incr_decr']; ?>
						</td>
						<td class='wrappable' align='center' style='width:60px;'>
							<?php echo $value['cmd']; ?>
						</td>
						</tr>

			<?php
						
					}	
				} else {
					?> <tr><td colspan="11" style="width: 735px;" align='center'>No data is available</td></tr> <?php
				}
			
			?>
		</tbody>
	</table>
</page>