<style>
	 a.btn{

	 text-align:left;
	 margin-top:5px;
	 width: 100%;
 }
.media-left{
	padding: 0px 10px;
}
.middle input {
	position: absolute;
	    left: 11px;
	    top: 4px;

}
.media-left i{
	font-size: 26px;
}
.middle{
	position: relative;
	border: 3px solid #25476a;
    border-radius: 7px;
}
.text-15px{
	font-size: 15px;
}
</style>
<!--CONTENT CONTAINER-->
<!--===================================================-->
<div id="content-container">
	<!--Page Title-->
	<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
	<div id="page-head">
		<div id="page-title">
			<h1 class="page-header text-overflow">Tender Reports</h1>
		</div>
	<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
	<!--End page title-->

	<!--Breadcrumb-->
	<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
		<ol class="breadcrumb">
			<li><a href="admin.php">Home</a></li>
			<li><a class="active">Tender Reports</a></li>
		</ol>
	</div>
	<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
	<!--End breadcrumb-->

	<!--Page content-->
	<!--===================================================-->
	<div id="page-content">
		<div class="float_btn">
			<a class="btn btn-primary btn-icon btn-circle add-tooltip" data-placement="top" data-toggle="tooltip" data-original-title="Back" href='<?php echo base_url('dashboard'); ?>' ><i class="fa fa-arrow-left" aria-hidden="true"></i></a>
		</div>
		<div class="row">
			<div class="col-sm-12">
				<!--Primary Panel-->
				<!--===================================================-->
				<div class="panel panel-primary">
					<div class="panel-heading">
						<h3 class="panel-title">TENDER REPORTS</h3>
					</div>
					<!--Horizontal Form-->
					<!--===================================================-->

					<div class="panel-body" >
						<form method="post" name="frm" id="frm"   target="_blank" >
							<div class="col-lg-12">
								<div class="col-sm-3">
									<div class="form-group">
										<label class="control-label">Auction Id :</label>
										<select class="form-control" size="1" name="cbotender_id" id="cbotender_id" onChange="javascript:ajax_get_auction_lot_client()" >
										<option value="" >--select--</option>
										<?php
											if ( ! empty($get_all_tender))
											{
												foreach($get_all_tender as $single_auc)
												{
										?>
												<option value="<?php echo $single_auc['tid']?>"><?php echo $single_auc['tender_id'] .'  [' .$single_auc['start_date'].' ]  [ '.$single_auc['vendor_name'].' ]  [ '.$single_auc['particulars'].' ] '?></option>

										<?php
												}
											}

										?>
										</select>

									</div>
								</div>
								<div class="col-sm-3">
									<div class="form-group">
										<label class="control-label">Lot No :</label>
										<select class="form-control" size="1" name="lotno" id="lotno" >
										<option value="" >--select--</option>

										</select>
									</div>
								</div>
									<div class="col-sm-2">
										<div class="form-group">
											<a id='ckbCheckAll' class="btn btn-info btn-labeled fa fa-check-circle-o" >Select All</a>
										</div>
									</div>


									<div class="col-sm-2">
										<div class="form-group">
											<a class="btn btn-success btn-labeled fa fa-file-excel-o" href="javascript:void(0);" value="Export In PDF" onclick="export_in_excel()">Export Excel</a>
										</div>
									</div>

									<div class="col-sm-2">
										<div class="form-group">
											<a class="btn btn-danger btn-labeled fa fa-file-pdf-o" href="javascript:void(0);" value="Export In PDF" onclick="export_in_pdf()">Export PDF</a>
										</div>
									</div>

								<input type="hidden" name="tid" id="tid" value="">
								<input type="hidden" name="clientid" id="clientid" value="">
							</div>
							<div id="checkboxlist">
								<div class="col-lg-12 normal_auction">
									<div class="row">
									<div class="form-group">
										<?php
										if ($this->common_model->check_permission("Tender Pre Bid Report") || TRUE){
										echo "
											<div class='col-lg-3'>
												<label for='pre_bid'>
													<div class='panel media middle pad-all'>
														<input type='checkbox' class='checked_lot' id='pre_bid' name='pre_bid' value='Pre Bid Result'>
														<div class='media-left'>
															<span class='icon-wrap icon-wrap-sm icon-circle bg-primary'>
															<i class='fa fa-file-text-o' aria-hidden='true'></i>
															</span>
														</div>
														<div id='pre_bid_result_html' class='media-body'>
															<p class='text-15px mar-no text-semibold text-dark'>Pre Bid Result</p>
															<p class='text-primary mar-no'>Report</p>
														</div>
													</div>
												</label>
											</div>";
										}
										?>
										<?php
										if ($this->common_model->check_permission("Tender EMD Report") || TRUE){
										echo "
											<div class='col-lg-3'>
												<label for='cmd_emd'>
													<div class='panel media middle pad-all'>
														<input type='checkbox' class='checked_lot' name='cmd_emd' id='cmd_emd' value='EMD REPORT'>
														<div class='media-left'>
															<span class='icon-wrap icon-wrap-sm icon-circle bg-primary'>
															<i class='fa fa-file-text-o' aria-hidden='true'></i>
															</span>
														</div>
														<div id='emd_html' class='media-body'>
															<p class='text-15px mar-no text-semibold text-dark'>EMD</p>
															<p class='text-primary mar-no'>Report</p>
														</div>
													</div>
												</label>
											</div>";
										}
										?>
										<?php
										if ($this->common_model->check_permission("Tender Terms Acceptance Report") || TRUE) {
										echo "
											<div class='col-lg-3'>
												<label for='terms_acceptance_report'>
													<div class='panel media middle pad-all'>
													<input type='checkbox' class='checked_lot' id='terms_acceptance_report' name='terms_acceptance_report' value='Terms Acceptance Report'>
														<div class='media-left'>
															<span class='icon-wrap icon-wrap-sm icon-circle bg-primary'>
															<i class='fa fa-file-text-o' aria-hidden='true'></i>
															</span>
														</div>
														<div id='terms_acceptance_html' class='media-body'>
															<p class='text-15px mar-no text-semibold text-dark'>Terms </p>
															<p class='text-primary mar-no'>Acceptance Report</p>
														</div>
													</div>
												</label>
											</div>";
										}
										?>
										<?php
										if ($this->common_model->check_permission("Tender Client Wise Lot Report") || TRUE){
										echo "
											<div class='col-lg-3'>
												<label for='report_client_wise_lot_updated'>
													<div class='panel media middle pad-all'>
													<input type='checkbox' class='checked_lot' id='report_client_wise_lot_updated' name='report_client_wise_lot_updated' value='CLIENT WISE LOT UPDATED'>
														<div class='media-left'>
															<span class='icon-wrap icon-wrap-sm icon-circle bg-primary'>
															<i class='fa fa-file-text-o' aria-hidden='true'></i>
															</span>
														</div>
														<div id='client_wise_html' class='media-body'>
															<p class='text-15px mar-no text-semibold text-dark'>Client Wise </p>
															<p class='text-primary mar-no'>Lot Updated Report</p>
														</div>
													</div>
												</label>
											</div>";
										}
										?>
										<?php
										if ($this->common_model->check_permission("Tender Client Ips Report") || TRUE){
										echo "
											<div class='col-lg-3'>
												<label for='client_n_ip'>
													<div class='panel media middle pad-all'>
													<input type='checkbox' class='checked_lot' id='client_n_ip' name='client_n_ip' value='Clients & their Ips'>
														<div class='media-left'>
															<span class='icon-wrap icon-wrap-sm icon-circle bg-primary'>
															<i class='fa fa-file-text-o' aria-hidden='true'></i>
															</span>
														</div>
														<div id='client_n_ips_html' class='media-body'>
															<p class='text-15px mar-no text-semibold text-dark'>Client & Their IP</p>
															<p class='text-primary mar-no'>Report</p>
														</div>
													</div>
												</label>
											</div>";
										}
										?>
										<?php 
										if ($this->common_model->check_permission("Tender Multiple Client Same Ip report") || TRUE){
										echo "
											<div class='col-lg-3'>
												<label for='multiple_client_in_same_ip'>
													<div class='panel media middle pad-all'>
													<input type='checkbox' class='checked_lot' id='multiple_client_in_same_ip' name='multiple_client_in_same_ip' value='Client & Their Ips'>
														<div class='media-left'>
															<span class='icon-wrap icon-wrap-sm icon-circle bg-primary'>
															<i class='fa fa-file-text-o' aria-hidden='true'></i>
															</span>
														</div>
														<div id='multiple_client_in_same_html' class='media-body'>
															<p class='text-15px mar-no text-semibold text-dark'>Multiple Client </p>
															<p class='text-primary mar-no'>Same IP Report</p>
														</div>
													</div>
												</label>
											</div>";
										}
										?>
										<?php
										if ($this->common_model->check_permission("Tender New Client Report") || TRUE){
										echo "
											<div class='col-lg-3'>
												<label for='new_client'>
													<div class='panel media middle pad-all'>
													<input type='checkbox' class='checked_lot' id='new_client' name='new_client' value='New Client'>
														<div class='media-left'>
															<span class='icon-wrap icon-wrap-sm icon-circle bg-primary'>
															<i class='fa fa-file-text-o' aria-hidden='true'></i>
															</span>
														</div>
														<div id='new_client_html' class='media-body'>
															<p class='text-15px mar-no text-semibold text-dark'>New Client</p>
															<p class='text-primary mar-no'>Report</p>
														</div>
													</div>
												</label>
											</div>";
										}
										?>
									</div>

									<div class="form-group">

										<?php
										if ($this->common_model->check_permission("Tender Lot Wise Bid Log Report") || TRUE){
										echo "
											<div class='col-lg-3'>
												<label for='lotwise_bid_log'>
													<div class='panel media middle pad-all'>
													<input type='checkbox' class='checked_lot' id='lotwise_bid_log' name='lotwise_bid_log' value='LOTWISE BID LOG'>
														<div class='media-left'>
															<span class='icon-wrap icon-wrap-sm icon-circle bg-primary'>
															<i class='fa fa-file-text-o' aria-hidden='true'></i>
															</span>
														</div>
														<div id='lotwise_bid_log_html' class='media-body'>
															<p class='text-15px mar-no text-semibold text-dark'>Lotwise Bid Log</p>
															<p class='text-primary mar-no'>Report</p>
														</div>
													</div>
												</label>
											</div>";
										}

										?>
									

										<?php
										if ($this->common_model->check_permission("Tender Bid Summary Report") || TRUE){
										echo "
											<div class='col-lg-3'>
												<label for='bid_summary_new'>
													<div class='panel media middle pad-all'>
													<input type='checkbox' class='checked_lot' id='bid_summary_new' name='bid_summary_new' value='Bid Summary Report (New)'>
														<div class='media-left'>
															<span class='icon-wrap icon-wrap-sm icon-circle bg-primary'>
															<i class='fa fa-file-text-o' aria-hidden='true'></i>
															</span>
														</div>
														<div id='bid_summary_html' class='media-body'>
															<p class='text-15px mar-no text-semibold text-dark'>Bid Summary</p>
															<p class='text-primary mar-no'>Report</p>
														</div>
													</div>
												</label>
											</div>";
										}
										?>


										<?php
										if ($this->common_model->check_permission("Tender Lot Details Report") || TRUE){
										echo "
											<div class='col-lg-3'>
												<label for='lot_details'>
													<div class='panel media middle pad-all'>
													<input type='checkbox' class='checked_lot' id='lot_details' name='lot_details' value='LOT DETAILS'>
														<div class='media-left'>
															<span class='icon-wrap icon-wrap-sm icon-circle bg-primary'>
															<i class='fa fa-file-text-o' aria-hidden='true'></i>
															</span>
														</div>
														<div id='lot_details_html' class='media-body'>
															<p class='text-15px mar-no text-semibold text-dark'>Lot Details</p>
															<p class='text-primary mar-no'>Report</p>
														</div>
													</div>
												</label>
											</div>";
										}
										?>

										<?php
										if ($this->common_model->check_permission("Tender Summary Report") || TRUE){
										echo "
											<div class='col-lg-3'>
												<label for='tender_summary'>
													<div class='panel media middle pad-all'>
													<input type='checkbox' class='checked_lot' id='tender_summary' name='tender_summary' value='Tender Summary'>
														<div class='media-left'>
															<span class='icon-wrap icon-wrap-sm icon-circle bg-primary'>
															<i class='fa fa-file-text-o' aria-hidden='true'></i>
															</span>
														</div>
														<div id='tender_summary_html' class='media-body'>
															<p class='text-15px mar-no text-semibold text-dark'>Tender Summary</p>
															<p class='text-primary mar-no'>Report</p>
														</div>
													</div>
												</label>
											</div>";
										}
										?>
									</div>
								</div>
							</div>
							<input type="hidden" name="save_file" id="save_file" value="0">
						</form>
					</div>

					<!--===================================================-->
					<!--End Horizontal Form-->
				</div>
				<!--===================================================-->
				<!--End Primary Panel-->
			</div>
		</div>
	</div>
	<!--===================================================-->
	<!--End page content-->

</div>
</div>
<!--===================================================-->
<!--END CONTENT CONTAINER-->

<script type="text/javascript">
function ajax_get_auction_lot_client()
{
	if (document.getElementById("cbotender_id").selectedIndex==0)
	{
		$("#lotno").html("<option value='' selected>--select--</option>");
	}
	else
	{
		var auction_id = document.getElementById("cbotender_id").value;
		if(auction_id != "")
		{
			$.ajax
			({
				url:"<?php echo base_url('auctioneer/tender_reports/get_auction_lots_n_client');?>",
				type:"POST",
				dataType:"json",
				data:{"auction_id":auction_id,"auc_type":'ET'},
				success:function(response)
				{
					if (response.tender.lots !== 0){
						$("#lotno").html('');
						$("#lotno").html('<option value="">--Select One--</option>');
						response.tender.lots.forEach(function(element) {
							$("#lotno").append('<option value="'+element.lotno+'">'+element.lotno+'['+element.mat_code+']</option>');
						});
					}
				}
			});
		}
	}
}
function export_in_pdf()
{
	var theForm = $("#frm");
	if(document.getElementById("cbotender_id").selectedIndex==0)
	{
		alert("Please select Auction");
		document.getElementById("cbotender_id").focus();
		return false;
	}
	var chkArray = [];

	/* look for all checkboes that have a parent id called 'checkboxlist' attached to it and check if it was checked */
	$("#checkboxlist input:checked").each(function() {
		chkArray.push($(this).val());
	});

	/* check if there is selected checkboxes, by default the length is 1 as it contains one single comma */
	if(chkArray.length > 0)
	{
		if (chkArray.length == 1){
			$('#save_file').val('0');

		}
		else
		{
			$('#save_file').val('1');
		}
		$(theForm).attr('action', '<?php echo base_url('auctioneer/tender_reports/get_tender_reports_in_pdf')?>');
		$(theForm).submit();


	}
	else
	{
		alert("Please at least check one of the checkbox");
	}
}

function export_in_excel()
{
	var theForm = $("#frm");
	if(document.getElementById("cbotender_id").selectedIndex==0)
	{
		alert("Please select Auction");
		document.getElementById("cbotender_id").focus();
		return false;
	}


	var chkArray = [];

	/* look for all checkboes that have a parent id called 'checkboxlist' attached to it and check if it was checked */
	$("#checkboxlist input:checked").each(function() {
		chkArray.push($(this).val());
	});

	/* we join the array separated by the comma */
	var selected;
	selected = chkArray.join(',') ;

	/* check if there is selected checkboxes, by default the length is 1 as it contains one single comma */
	if(selected.length > 0){
		$(theForm).attr('action', '<?php echo base_url('auctioneer/tender_reports/get_reports_in_excel')?>');
		$(theForm).submit();
	}else{
		alert("Please at least check one of the checkbox");
	}

}
var clicked = false;
$("#ckbCheckAll").on("click", function() {
  $(".checked_lot").prop("checked", !clicked);
  clicked = !clicked;
  this.innerHTML = clicked ? 'Deselect All' : 'Select All';

  if($('#ckbCheckAll').hasClass('btn-info')){
		$('#ckbCheckAll').removeClass('btn-info')
		$("#ckbCheckAll").addClass('btn-primary');
	}else{
		$('#ckbCheckAll').removeClass('btn-primary')
		$("#ckbCheckAll").addClass('btn-info');
	}
});

$("#pre_bid_result_html").click(function(){
	var lotNo = $("#lotno").val();
	var auction_id = document.getElementById("cbotender_id").value;
	if(auction_id != "")
	{
		$.ajax({
			url:"<?php echo base_url('auctioneer/tender_reports/check_current_auction');?>",
			type:"POST",
			dataType:"json",
			data:{"auction_id":auction_id},
			success:function(response)
			{
				if (response.status == 'success'){
					window.open("<?php echo base_url().'auctioneer/tender_reports/pre_bid_result_html/'; ?>"+auction_id+"/"+lotNo);
				} else {
					alert(response.message);
				}
			},
			error: function(){
				alert("Problem in fetching report please try again.");
			}
		});
	}
	else
	{
		alert('Select auction');
	}
})

$("#emd_html").click(function(){
	var lotNo = $("#lotno").val();
	var auction_id = document.getElementById("cbotender_id").value;
	if(auction_id != "")
	{
		$.ajax({
			url:"<?php echo base_url('auctioneer/tender_reports/check_current_auction');?>",
			type:"POST",
			dataType:"json",
			data:{"auction_id":auction_id},
			success:function(response)
			{
				if (response.status == 'success'){
					window.open("<?php echo base_url().'auctioneer/tender_reports/emd_html/'; ?>"+auction_id+"/"+lotNo);
				} else {
					alert(response.message);
				}
			},
			error: function(){
				alert("Problem in fetching report please try again.");
			}
		});
	}
	else
	{
		alert('Select auction');
	}
})

$("#terms_acceptance_html").click(function(){
	var lotNo = $("#lotno").val();
	var auction_id = document.getElementById("cbotender_id").value;
	if(auction_id != "")
	{
		$.ajax({
			url:"<?php echo base_url('auctioneer/tender_reports/check_current_auction');?>",
			type:"POST",
			dataType:"json",
			data:{"auction_id":auction_id},
			success:function(response)
			{
				if (response.status == 'success'){
					window.open("<?php echo base_url().'auctioneer/tender_reports/terms_acceptance_html/'; ?>"+auction_id+"/"+lotNo);
				} else {
					alert(response.message);
				}
			},
			error: function(){
				alert("Problem in fetching report please try again.");
			}
		});
	}
	else
	{
		alert('Select auction');
	}
})

$("#client_wise_html").click(function(){
	var lotNo = $("#lotno").val();
	var auction_id = document.getElementById("cbotender_id").value;
	if(auction_id != "")
	{
		$.ajax({
			url:"<?php echo base_url('auctioneer/tender_reports/check_current_auction');?>",
			type:"POST",
			dataType:"json",
			data:{"auction_id":auction_id},
			success:function(response)
			{
				if (response.status == 'success'){
					window.open("<?php echo base_url().'auctioneer/tender_reports/client_wise_html/'; ?>"+auction_id+"/"+lotNo);
				} else {
					alert(response.message);
				}
			},
			error: function(){
				alert("Problem in fetching report please try again.");
			}
		});
	}
	else
	{
		alert('Select auction');
	}
})

$("#client_n_ips_html").click(function(){
	var lotNo = $("#lotno").val();
	var auction_id = document.getElementById("cbotender_id").value;
	if(auction_id != "")
	{
		$.ajax({
			url:"<?php echo base_url('auctioneer/tender_reports/check_current_auction');?>",
			type:"POST",
			dataType:"json",
			data:{"auction_id":auction_id},
			success:function(response)
			{
				if (response.status == 'success'){
					window.open("<?php echo base_url().'auctioneer/tender_reports/client_n_ips_html/'; ?>"+auction_id+"/"+lotNo);
				} else {
					alert(response.message);
				}
			},
			error: function(){
				alert("Problem in fetching report please try again.");
			}
		});
	}
	else
	{
		alert('Select auction');
	}
})

$("#multiple_client_in_same_html").click(function(){
	var lotNo = $("#lotno").val();
	var auction_id = document.getElementById("cbotender_id").value;
	if(auction_id != "")
	{
		$.ajax({
			url:"<?php echo base_url('auctioneer/tender_reports/check_current_auction');?>",
			type:"POST",
			dataType:"json",
			data:{"auction_id":auction_id},
			success:function(response)
			{
				if (response.status == 'success'){
					window.open("<?php echo base_url().'auctioneer/tender_reports/multiple_client_in_same_html/'; ?>"+auction_id+"/"+lotNo);
				} else {
					alert(response.message);
				}
			},
			error: function(){
				alert("Problem in fetching report please try again.");
			}
		});
	}
	else
	{
		alert('Select auction');
	}
})

$("#new_client_html").click(function(){
	var lotNo = $("#lotno").val();
	var auction_id = document.getElementById("cbotender_id").value;
	if(auction_id != "")
	{
		$.ajax({
			url:"<?php echo base_url('auctioneer/tender_reports/check_current_auction');?>",
			type:"POST",
			dataType:"json",
			data:{"auction_id":auction_id},
			success:function(response)
			{
				if (response.status == 'success'){
					window.open("<?php echo base_url().'auctioneer/tender_reports/new_client_html/'; ?>"+auction_id+"/"+lotNo);
				} else {
					alert(response.message);
				}
			},
			error: function(){
				alert("Problem in fetching report please try again.");
			}
		});
	}
	else
	{
		alert('Select auction');
	}
})

$("#lotwise_bid_log_html").click(function(){
	var lotNo = $("#lotno").val();
	var auction_id = document.getElementById("cbotender_id").value;
	if(auction_id != "")
	{
		$.ajax({
			url:"<?php echo base_url('auctioneer/tender_reports/check_current_auction');?>",
			type:"POST",
			dataType:"json",
			data:{"auction_id":auction_id},
			success:function(response)
			{
				if (response.status == 'success'){
					window.open("<?php echo base_url().'auctioneer/tender_reports/lotwise_bid_log_html/'; ?>"+auction_id+"/"+lotNo);
				} else {
					alert(response.message);
				}
			},
			error: function(){
				alert("Problem in fetching report please try again.");
			}
		});
	}
	else
	{
		alert('Select auction');
	}
})

$("#bid_summary_html").click(function(){
	var lotNo = $("#lotno").val();
	var auction_id = document.getElementById("cbotender_id").value;
	if(auction_id != "")
	{
		$.ajax({
			url:"<?php echo base_url('auctioneer/tender_reports/check_current_auction');?>",
			type:"POST",
			dataType:"json",
			data:{"auction_id":auction_id},
			success:function(response)
			{
				if (response.status == 'success'){
					window.open("<?php echo base_url().'auctioneer/tender_reports/bid_summary_html/'; ?>"+auction_id+"/"+lotNo);
				} else {
					alert(response.message);
				}
			},
			error: function(){
				alert("Problem in fetching report please try again.");
			}
		});
	}
	else
	{
		alert('Select auction');
	}
})

$("#lot_details_html").click(function(){
	var lotNo = $("#lotno").val();
	var auction_id = document.getElementById("cbotender_id").value;
	if(auction_id != "")
	{
		$.ajax({
			url:"<?php echo base_url('auctioneer/tender_reports/check_current_auction');?>",
			type:"POST",
			dataType:"json",
			data:{"auction_id":auction_id},
			success:function(response)
			{
				if (response.status == 'success'){
					window.open("<?php echo base_url().'auctioneer/tender_reports/lot_details_html/'; ?>"+auction_id+"/"+lotNo);
				} else {
					alert(response.message);
				}
			},
			error: function(){
				alert("Problem in fetching report please try again.");
			}
		});
	}
	else
	{
		alert('Select auction');
	}
})

$("#tender_summary_html").click(function(){
	var lotNo = $("#lotno").val();
	var auction_id = document.getElementById("cbotender_id").value;
	if(auction_id != "")
	{
		$.ajax({
			url:"<?php echo base_url('auctioneer/tender_reports/check_current_auction');?>",
			type:"POST",
			dataType:"json",
			data:{"auction_id":auction_id},
			success:function(response)
			{
				if (response.status == 'success'){
					window.open("<?php echo base_url().'auctioneer/tender_reports/tender_summary_html/'; ?>"+auction_id+"/"+lotNo);
				} else {
					alert(response.message);
				}
			},
			error: function(){
				alert("Problem in fetching report please try again.");
			}
		});
	}
	else
	{
		alert('Select auction');
	}
})


</script>


<style type="text/css">
#livesearch
  {
  margin:0px;
  width:200px;
  text-align: left;
  padding:5px;
  z-index:10;
  position:absolute;
  background:#fff;

  }
  #livesearch a{padding:2px;margin:1px;width:100%}
  #livesearch a:hover{background:#00FF00}
#txt1
  {
  margin:0px;
  }
</style>
