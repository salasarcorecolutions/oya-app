<style>
tr {
	page-break-inside: avoid;
}
thead {
	display: table-header-group;
}
tfoot {
	display: table-row-group;
}
tr {
	page-break-inside: avoid;
}
td.wrappable,table.data_table td.wrappable {
    white-space: normal;	
}            
table td,table th {
	font-size:10px;
	
	
}
</style>


<page backbottom='110px' backtop="0px" backleft="0px" backright="0px" style="font-size:10px;">
   <page_footer>
        <table style='width: 100%;'>
			<tr>
				<td align='left' style='float:left;width:200px;'>
				Page :  [[page_cu]]/[[page_nb]] <br/>
				<?php echo (new DateTime(null, new DateTimeZone($_SESSION['user_tz'])))->format('D dS \of M Y h:i:s A'); ?>
				
				</td>
				<td align='right' style='float:right;width:545px;'>
					
				</td>
			</tr>
		</table>
    </page_footer>
	<br>
	<table border='0.5' cellspacing='0' cellpadding='0' width='100%'>
		<tbody>
			<tr>
				<td style="width:550px;" >
								
				</td>
				<td align='center' style="width:180px;">
					<span style='font-size:20px;color:#2a5395;'><strong><?php echo PRODUCT ?></strong></span>
				</td>
			</tr>
			<tr>
				<td colspan='2' align='center' >
					<strong>AUCTION SUMMARY FOR TENDER NO :  <?php echo $common_auction_details['saleno']; ?> </strong>
				</td>
			</tr>
			<tr>
				<td colspan='2' align='center' >
					<strong><?=strtoupper($common_auction_details['vendor_name'])?></strong>
				</td>
			</tr>
			<tr>
				<td colspan='2' align='center' >
					<strong>TENDER DETAILS</strong>
				</td>
			</tr>
			<tr>
				<td colspan='2' align='left'>
					<strong>Tender No :</strong><?php echo $common_auction_details['saleno']?>
				</td>
			</tr>
			<tr>
				<td colspan='2' align='left'>
					<strong>Tender Commencement Date : </strong><?php echo $common_auction_details['tcdate'].$common_auction_details['tctime']?>
				</td>
			</tr>
			<tr>
				<td colspan='2' align='left'>
					<strong>Tender Closing Date : </strong><?php echo $common_auction_details['edt']?>
				</td>
			</tr>
			<tr>
				<td colspan='2' align='left'>
					<strong>Particulars : </strong><?php echo $common_auction_details['particulars']?>
				</td>
			</tr>
			<tr>
				<td colspan='2' align='left'>
					<strong>No. of Lots : </strong><?php echo $common_auction_details['no_of_lots']?>
				</td>
			</tr>
			<tr>
			<td colspan="2" align="center" style="width:250px; text-align:center;">
				<b><font size="3">AUCTION SUMMARY FOR SALE/AUCTION NO: <?php echo $common_auction_details['saleno']; ?></font></b>
			</td>
		</tr>
		</tbody>		
	</table>
	<br>

	<?php

					if ( ! empty($tender_summary))
					{

						foreach($tender_summary as $key=>$value)
						{


				?>
					<table border="0.5" cellpadding="0"  cellspacing="0" width="100%" id="table1" class="adminlist">
						<tr>
							<td align="center" colspan="4" >
								<b><font size="2" >TENDER DETAILS</font></b>
							</td>
						</tr>
						<tr>
							<td style="width: 150px;"  ><b>Tender No</b></td>
							<td style="width: 210px;" ><?php echo $common_auction_details['saleno'];?></td>
							<td style="width: 150px;" >Auction Type</td>
							<td style="width: 208px;" ><?php echo $common_auction_details['tender_type'];?></td>
						</tr>
						<tr>
							<td  ><b>Tender Commencement Date</b></td>
							<td  ><?php echo $common_auction_details['tcdate'];?></td>
							<td  ><b>Tender Closing Date:</b></td>
							<td  ><?php echo $common_auction_details['edt'];?></td>
						</tr>
						<tr>
							<td  bgcolor="#FFF"  nowrap><b>Tender Opening Date</b></td>
							<td bgcolor="#FFF"  ><?php echo $common_auction_details['sdt'];?></td>
							<td  bgcolor="#FFF" ><b>Vendor</b></td>
							<td bgcolor="#FFF" ><?php echo $common_auction_details['vendor_name'];?></td>
						</tr>
						<tr>
							<td  ><b>Location</b></td>
							<td  ><?php echo $common_auction_details['location']; ?></td>
							<td  ><b>Sector</b></td>
							<td   > <?php echo $common_auction_details['sector']; ?></td>
						</tr>
						<tr>
							<td align="center" colspan="4">
								<b><font size="2">HIGHLIGHTS OF THE AUCTION</font></b>
							</td>
						</tr>
						<tr>
							<td  ><b>Total No of Lots &nbsp;:&nbsp;</b></td>
							<td  ><?php echo $value['no_of_lots'];?> Lots</td>
							<td  ><b>No of Buyers Invited &nbsp;:&nbsp;</b></td>
							<td ><?php echo $value['no_of_invited'];?> Clients</td>
						</tr>
						<tr>
							<td ><b>Unbidded Lots &nbsp;:&nbsp;</b></td>
							<td ><?php echo $value['unbidded_lot_cnt'];?> Lots</td>
							<td ><b>No of Buyers Participated &nbsp;:&nbsp;</b> </td>
							<td ><?php echo $value['no_of_participated'];?> Clients</td>
						</tr>
						<tr>
							<td ><b>Total Spend &nbsp;:&nbsp;</b></td>
							<td ><?php echo $value['total_spend']?> </td>
							<td ><b>Auction Running Time &nbsp;:&nbsp;</b> </td>
							<td ><?php echo $value['auction_day']?> Clients</td>
						</tr>
						<tr>
							<td ><b>Highest Price &nbsp;:&nbsp;</b></td>
							<td ><?php echo $value['highest_price']?></td>
							<td ><b>Difference &nbsp;:&nbsp;</b> </td>
							<td ><?php echo $value['profit']?></td>
						</tr>
						<tr class="hd" >
							<td bgcolor="#FFF">&nbsp;</td>
							<td bgcolor="#FFF">&nbsp;</td>
							<td bgcolor="#FFF">&nbsp;</td>
							<td bgcolor="#FFF">&nbsp;</td>
						</tr>


					</table>
					<br><br>
					<table border="0.5" cellspacing="0" width="100%" >
									<thead>

										<tr>
											<th align="center">SR NO. &nbsp;&nbsp;</th>
											<th align="center">MATERIAL<br/> Code</th>
											<th align="center">PLANT &nbsp;&nbsp;</th>
											<th align="center" style="width:20px;">DESCRIPTION&nbsp;&nbsp;</th>
											<th align="center">TOTAL QTY </th>
											<th align="center">BID QTY </th>

											<th>Start Price &nbsp;&nbsp; </th>
											<?php
												if($value['bid_type'] == 'I')
												{
											?>
												<th align="center">H1 BID &nbsp;&nbsp;</th>
												<th align="center">H1 BIDDER &nbsp;&nbsp;</th>
											<?php
												}
												else
												{
											?>
												<th align="center">L1 BID &nbsp;&nbsp;</th>
												<th align="center">L1 BIDDER &nbsp;&nbsp;</th>
											
											<?php
													
												}

											?>

											<th align="center">NO OF &nbsp;<br/> BIDS</th>
											<th align="center">NO OF <br/>BIDDERS&nbsp;&nbsp;</th>
											<th align="center">LOT TOTAL <br/>AMOUNT</th>
										</tr>
									</thead>
									<tbody>
										<?php
											if( ! empty($value['vendorlotno']))
											{
												$i = 1;
												foreach($value['vendorlotno'] as $k=>$v)
												{
												?>
												<tr>
													<td style="width:40px;"  align="center" class='wrappable'><?php echo $i?></td>
													<td style="width:40px;"  align="center" class='wrappable'><?php echo $value['mat_code'][$k]?></td>
													<td style="width:70px;" align="center" class='wrappable'><?php echo $value['plant'][$k]?>&nbsp;&nbsp;</td>
													<td align="left" class='wrappable' style="width:81px;white-space:normal;word-wrap:break-word;"><p style="word-wrap: break-word"><?php echo $value['prod'][$k]?></p></td>
													
													<td style="width:50px;"  align="center" class='wrappable'><?php echo $value['tq'][$k]?>&nbsp;&nbsp;</td>
													<td style="width:50px;"  align="center" class='wrappable'><?php echo $value['bq'][$k]?>&nbsp;&nbsp;</td>
													<td style="width:50px;"  align="center" class='wrappable'><?php echo $value['sb'][$k]?>&nbsp;&nbsp;</td>
													<td style="width:50px;" " align="center" class='wrappable'><?php echo $value['amt'][$k]?></td>
													<td style="width:50px;"  align="center" class='wrappable'><?php echo $value['bidder_name'][$k]?>&nbsp;&nbsp;&nbsp;</td>
													
													<td style="width:50px;"  align="center" class='wrappable'><?php echo $value['no_of_bids'][$k]?>&nbsp;&nbsp;</td>
													<td style="width:50px;"  align="center" class='wrappable'><?php echo $value['no_of_bidders'][$k]?>&nbsp;&nbsp;</td>
													<td style="width:70px;"  align="center" class='wrappable'><?php echo $value['rs'][$k]?>&nbsp;&nbsp;</td>
												</tr>
												<?php
												$i++;
												}
											} else {
												?> <tr><td colspan="12" style="width: 735px;" align='center'>No data is available</td></tr> <?php
											}
										?>
									</tbody>
								</table>
				<?php
						}
					}
				?>
</page>