<style>
tr {
	page-break-inside: avoid;
}
thead {
	display: table-header-group;
}
tfoot {
	display: table-row-group;
}
tr {
	page-break-inside: avoid;
}
td.wrappable,table.data_table td.wrappable {
    white-space: normal;	
}            
table td,table th {
	font-size:10px;
	
	
}
</style>


<page backbottom='110px' backtop="0px" backleft="0px" backright="0px" style="font-size:10px;">
   <page_footer>
        <table style='width: 100%;'>
			<tr>
				<td align='left' style='float:left;width:200px;'>
				Page :  [[page_cu]]/[[page_nb]] <br/>
				<?php echo (new DateTime(null, new DateTimeZone($_SESSION['user_tz'])))->format('D dS \of M Y h:i:s A'); ?>
				
				</td>
				<td align='right' style='float:right;width:545px;'>
					
				</td>
			</tr>
		</table>
    </page_footer>
	<br>
	<table border='0.5' cellspacing='0' cellpadding='0' width='100%'>
		<tbody>
			<tr>
				<td style="width:550px;" >
								
				</td>
				<td align='center' style="width:180px;">
					<span style='font-size:20px;color:#2a5395;'><strong><?php echo PRODUCT ?></strong></span>
				</td>
			</tr>
			<tr>
				<td colspan='2' align='center' >
					<strong>MULTIPLE CLIENTS SAME IP FOR TENDER NO :  <?php echo $common_auction_details['saleno']; ?> </strong>
				</td>
			</tr>
			<tr>
				<td colspan='2' align='center' >
					<strong><?=strtoupper($common_auction_details['vendor_name'])?></strong>
				</td>
			</tr>
			<tr>
				<td colspan='2' align='center' >
					<strong>TENDER DETAILS</strong>
				</td>
			</tr>
			<tr>
				<td colspan='2' align='left'>
					<strong>Tender No :</strong><?php echo $common_auction_details['saleno']?>
				</td>
			</tr>
			<tr>
				<td colspan='2' align='left'>
					<strong>Tender Commencement Date : </strong><?php echo $common_auction_details['tcdate'].$common_auction_details['tctime']?>
				</td>
			</tr>
			<tr>
				<td colspan='2' align='left'>
					<strong>Tender Closing Date : </strong><?php echo $common_auction_details['edt']?>
				</td>
			</tr>
			<tr>
				<td colspan='2' align='left'>
					<strong>Particulars : </strong><?php echo $common_auction_details['particulars']?>
				</td>
			</tr>
			<tr>
				<td colspan='2' align='left'>
					<strong>No. of Lots : </strong><?php echo $common_auction_details['no_of_lots']?>
				</td>
			</tr>
		</tbody>		
	</table>
	<br>
	<br>	
	<table border='0.5' cellspacing='0' cellpadding='0'>
		<thead>
			<tr>
				<th align='center' bgcolor="#e0e0e0">SR. NO</th>
				<th align='center' bgcolor="#e0e0e0">BIDDER NAME</th>
				<th align='center' bgcolor="#e0e0e0">ADDRESS</th>
				<th align='center' bgcolor="#e0e0e0">EMAIL</th>
				<th align='center' bgcolor="#e0e0e0">IP ADDRESS</th>
				<th align='center' bgcolor="#e0e0e0">IS BIDDED?</th>
			</tr>
		</thead>
		<tbody>	
			<?php
			
				if ( ! empty($multiple_client_n_ip))
				{
					$I = 1;
					foreach($multiple_client_n_ip as $key=>$singleClient)
					{
						if ($key!='show_no_records')
						{
						
					?>
						<tr>
							<td align='center' class='wrappable' style='width:45px;'><?php echo $I++; ?></td>
							<td align='center' class='wrappable' style='width:150px;'><?php echo $singleClient['compname']; ?></td>
							<td align='center' class='wrappable' style='width:193px;'><?php echo $singleClient['adddress']; ?></td>
							<td align='center' class='wrappable' style='width:163px;'><?php echo $singleClient['email']; ?></td>
							<td align='center' class='wrappable' style='width:90px;'><?php echo $singleClient['ip']; ?></td>
							<td align='center' class='wrappable' style='width:65px;'><?php echo $singleClient['is_bidded']; ?></td>
						</tr>
					<?php
						}
					}
					?>
					
				<?php
				}
				if($multiple_client_n_ip['show_no_records'])
				{
				?>
					<tr class='hd'>
						<td  align='center' class='wrappable' style='width:735px;' colspan="6">No Records Found</td>
					</tr>

				<?php
				}

			?>
		</tbody>
	</table>
</page>