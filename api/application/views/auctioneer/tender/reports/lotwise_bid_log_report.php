<style>
tr {
	page-break-inside: avoid;
}
thead {
	display: table-header-group;
}
tfoot {
	display: table-row-group;
}
tr {
	page-break-inside: avoid;
}
td.wrappable,table.data_table td.wrappable {
    white-space: normal;
}            
table td,table th {
	font-size:10px;
	
	
}
</style>


<page backbottom='110px' backtop="0px" backleft="0px" backright="0px" style="font-size:10px;">
   <page_footer>
        <table style='width: 100%;'>
			<tr>
				<td align='left' style='float:left;width:200px;'>
				Page :  [[page_cu]]/[[page_nb]] <br/>
				<?php echo (new DateTime(null, new DateTimeZone($_SESSION['user_tz'])))->format('D dS \of M Y h:i:s A'); ?>
				
				</td>
				<td align='right' style='float:right;width:545px;'>
					
				</td>
			</tr>
		</table>
    </page_footer>
	<br>
	<table border='0.5' cellspacing='0' cellpadding='0' width='100%'>
		<tbody>
			<tr>
				<td style="width:550px;" >
								
				</td>
				<td align='center' style="width:180px;">
					<span style='font-size:20px;color:#2a5395;'><strong><?php echo PRODUCT ?></strong></span>
				</td>
			</tr>
			<tr>
				<td colspan='2' align='center' >
					<strong>LOTWISE BID LOG FOR TENDER NO :  <?php echo $common_auction_details['saleno']; ?> </strong>
				</td>
			</tr>
			<tr>
				<td colspan='2' align='center' >
					<strong><?=strtoupper($common_auction_details['vendor_name'])?></strong>
				</td>
			</tr>
			<tr>
				<td colspan='2' align='center' >
					<strong>TENDER DETAILS</strong>
				</td>
			</tr>
			<tr>
				<td colspan='2' align='left'>
					<strong>Tender No :</strong><?php echo $common_auction_details['saleno']?>
				</td>
			</tr>
			<tr>
				<td colspan='2' align='left'>
					<strong>Tender Commencement Date : </strong><?php echo $common_auction_details['tcdate'].$common_auction_details['tctime']?>
				</td>
			</tr>
			<tr>
				<td colspan='2' align='left'>
					<strong>Tender Closing Date : </strong><?php echo $common_auction_details['edt']?>
				</td>
			</tr>
			<tr>
				<td colspan='2' align='left'>
					<strong>Particulars : </strong><?php echo $common_auction_details['particulars']?>
				</td>
			</tr>
			<tr>
				<td colspan='2' align='left'>
					<strong>No. of Lots : </strong><?php echo $common_auction_details['no_of_lots']?>
				</td>
			</tr>
		</tbody>		
	</table>
	<br>
	<br>	
	<?php

		if (!empty($lotwise_bid_log))
		{
			foreach ($lotwise_bid_log as $key=>$value)
			{

				?>
				<table border='0.5' cellspacing='0' cellpadding='0' width='100%'>
				<thead>		
					<tr>
						<th align='center' class='wrappable' colspan="6"  >
							<b>Lot No:</b>  <?php echo $value['lotno'] ;?>   <br> <b>Product:</b><?php echo $value['prod'];?>
						</th>
					</tr>
				</thead>
				<tbody>	
				<tr>
					<td align='center' class='wrappable' ><b>Start Time : </b> <br> <?php echo $value['st'];?> </td>
					<td align='center' class='wrappable' ><b>End Time : </b> <br> <?php echo $value['et'];?></td>
					<td align='center' class='wrappable'  ><b>Material:  </b> <?php echo $value['mat_code'];?></td>
					<td align='center' class='wrappable' style=''><strong>Bid Type :<br></strong><?php if($value['bid_type']=="I") echo "Incremental"; else echo "Decremental";?></td>
					<td align='center' class='wrappable' style=''><strong>Min Increment :<br></strong><?php echo $value['tq'];?></td>
					<td align='center' class='wrappable' style=''><strong>Lot Qty. :<br></strong><?php echo $value['iv'];?></td>
					
					
				</tr>
				<tr>
					<td align='center' class='wrappable' style='font-weight:bold;width:170px;' bgcolor="#e0e0e0">BIDDER NAME</td>
					<td align='center' class='wrappable' style='font-weight:bold;width:115px;' bgcolor="#e0e0e0">Date Time</td>
					<td align='center' class='wrappable' style='font-weight:bold;width:130px;' bgcolor="#e0e0e0">Bid Amount</td>
					<td align='center' class='wrappable' style='font-weight:bold;width:130px;' bgcolor="#e0e0e0">Bid Qty</td>
					<td align='center' class='wrappable' style='font-weight:bold;width:75px;' bgcolor="#e0e0e0">Type</td>
					<td align='center' class='wrappable' style='font-weight:bold;width:85px;' bgcolor="#e0e0e0">IP Address</td>
				</tr>
		
				<?php
				if (isset($value['lot_details']))
				{
					foreach ($value['lot_details']  as $lot_details)
					{
					?>
					<tr>
						<td align='center' class='wrappable' style='width:80px;'><?php echo $lot_details['cname'];?></td>
						<td align='center' class='wrappable' style='width:70px;'><?php echo $lot_details['dt'];?> <?php echo $lot_details['tm'];?></td>
						<td align='center' class='wrappable' style='width:70px;'><?php echo $lot_details['bid_amt'];?></td>
						<td align='center' class='wrappable' style='width:80px;'><?php echo $lot_details['quantity'];?></td>
						<td align='center' class='wrappable' style='width:60px;'><?php echo $lot_details['typ'];?></td>
						<td align='center' class='wrappable' style='width:80px;'><?php echo $lot_details['ip'];?></td>
					</tr>

						<?php
					}
					
				}
				else
				{
	?>
					<tr><td colspan='6' align='center' class='wrappable'>No Bid</td></tr>
	<?php
				}
	?>
				</tbody>
				</table><br><br><br>
	<?php
	
			}
		}
		
	?>
	
</page>