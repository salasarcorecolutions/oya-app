<style>
tr {
	page-break-inside: avoid;
}
thead {
	display: table-header-group;
}
tfoot {
	display: table-row-group;
}
tr {
	page-break-inside: avoid;
}
td.wrappable,table.data_table td.wrappable {
    white-space: normal;	
}            
table td,table th {
	font-size:10px;
	
	
}
</style>


<page backbottom='110px' backtop="0px" backleft="0px" backright="0px" style="font-size:10px;">
   <page_footer>
        <table style='width: 100%;'>
			<tr>
				<td align='left' style='float:left;width:200px;'>
				Page :  [[page_cu]]/[[page_nb]] <br/>
				<?php echo (new DateTime(null, new DateTimeZone($_SESSION['user_tz'])))->format('D dS \of M Y h:i:s A'); ?>
				
				</td>
				<td align='right' style='float:right;width:545px;'>
					
				</td>
			</tr>
		</table>
    </page_footer>
	<br>
	<table border='0.5' cellspacing='0' cellpadding='0' width='100%'>
		<tbody>
			<tr>
				<td style="width:550px;" >
								
				</td>
				<td align='center' style="width:180px;">
					<span style='font-size:20px;color:#2a5395;'><strong><?php echo PRODUCT ?></strong></span>
				</td>
			</tr>
			<tr>
				<td colspan='2' align='center' >
					<strong>TERM ACCEPTANCE FOR TENDER NO :  <?php echo $common_auction_details['saleno']; ?> </strong>
				</td>
			</tr>
			<tr>
				<td colspan='2' align='center' >
					<strong><?=strtoupper($common_auction_details['vendor_name'])?></strong>
				</td>
			</tr>
			<tr>
				<td colspan='2' align='center' >
					<strong>TENDER DETAILS</strong>
				</td>
			</tr>
			<tr>
				<td colspan='2' align='left'>
					<strong>Tender No :</strong><?php echo $common_auction_details['saleno']?>
				</td>
			</tr>
			<tr>
				<td colspan='2' align='left'>
					<strong>Tender Commencement Date : </strong><?php echo $common_auction_details['tcdate'].$common_auction_details['tctime']?>
				</td>
			</tr>
			<tr>
				<td colspan='2' align='left'>
					<strong>Tender Closing Date : </strong><?php echo $common_auction_details['edt']?>
				</td>
			</tr>
			<tr>
				<td colspan='2' align='left'>
					<strong>Particulars : </strong><?php echo $common_auction_details['particulars']?>
				</td>
			</tr>
			<tr>
				<td colspan='2' align='left'>
					<strong>No. of Lots : </strong><?php echo $common_auction_details['no_of_lots']?>
				</td>
			</tr>
		</tbody>		
	</table>
	<br>
	<br>	
	<table border='0.5' cellspacing='0' cellpadding='0'>
		<thead>
			<tr>
				<th align='center' class='wrappable' style='font-weight:bold;' bgcolor="#e0e0e0">SR.<br/>No.</th>
				<th align='center' class='wrappable' style='font-weight:bold;' bgcolor="#e0e0e0">COMPANY NAME</th>
				<th align='center' class='wrappable' style='font-weight:bold;' bgcolor="#e0e0e0">BIDDER NAME</th>
				<th align='center' class='wrappable' style='font-weight:bold;' bgcolor="#e0e0e0">IP ADDRESS</th>
				<th align='center' class='wrappable' style='font-weight:bold;' bgcolor="#e0e0e0">ACCEPTANCE DATE AND TIME</th>
			</tr>
		</thead>
		<tbody>
			<?php
			
				if (!empty($terms_acceptance))
				{
					$I = 1;
					foreach($terms_acceptance as $key=>$value)
					{
						
					?>
						<tr>
							<td align='center' class='wrappable' style='width:20px;'><?=$I++?></td>
							<td align='center' class='wrappable' style='width:181px;'><?=$value['cname']?></td>
							<td align='center' class='wrappable' style='width:181px;'><?=$value['cper']?></td>
							<td align='center' class='wrappable' style='width:150px;'><?=$value['ipaddress']?></td>
							<td align='center' class='wrappable' style='width:180px;'><?=date("Y-m-d H:i:s",strtotime($value['adt']))?> Hrs.</td>
						</tr>
						
					<?php	
					}
					
				} else {
					?><tr><td align='center' colspan="5" style="width: 735px;">No data is available</td></tr><?php
				}
			
			?>
		</tbody>
	</table>
	
</page>