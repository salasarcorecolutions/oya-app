<!-- In order to let it work in CI, need to fix it to only get the viwes -->

<!--CONTENT CONTAINER-->
<!--===================================================-->
<div id="content-container">
	<!--Page Title-->
	<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
	<div id="page-head">
	<div id="page-title">
		<h1 class="page-header text-overflow">E-Tender Supervisors</h1>
	</div>
	<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
	<!--End page title-->


	<!--Breadcrumb-->
	<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
	<ol class="breadcrumb">
		<li><a href="<?php echo base_url('dashboard');?>">Home</a></li>
		<li class="active"><a href="<?php echo base_url('auctioneer/tender/index');?>">E-Tender</a></li>
		<li class="active">E-Tender Supervisors</li>
	</ol>
</div>
	<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
	<!--End breadcrumb-->
	<input type="hidden" id="t11" name="auctionId">
	<div id="page-content">
	<div class="float_btn">
		<a class="btn btn-primary btn-icon btn-circle add-tooltip"  data-placement="top" data-toggle="tooltip" data-original-title="Back" href='<?php echo base_url('auctioneer/tender/index'); ?>' ><i class="fa fa-arrow-left" aria-hidden="true"></i></a>
	</div>	
		<div class="panel">
			<div class="panel-heading">
				<h3 class="panel-title">E-Tender Supervisors</h3>
			</div>

			<div class="panel-body">
				<div class="table-responsive">
					<table class="table table-striped table-bordered table-hover no-footer dtr-inline">
						<thead>
							<tr>
								<th>Sale No</th>
								<th>Start Time</th>
								<th>End Time</th>
								<th>Particulars</th>
							</tr>
							<tr>
								<td><?php echo $auction_details['tender_id'] ?></td>
								<td><?php echo date('d-m-Y H:i:s',strtotime($auction_details['topen_dt'])) ?></td>
								<td><?php echo date('d-m-Y H:i:s',strtotime($auction_details['tclose_dt'])) ?></td>
								<td><?php echo $auction_details['particulars'] ?></td>
							</tr>
						</thead>
					</table>
				</div>
				<br/>
				<form name="form_suppervisior" id="form_suppervisior" method="post" >
					<input type='hidden' id="tid" name="tid" value='<?php echo $tid; ?>' />
					<div class="row filter">
						<div class="col-sm-4">
							<select class="form-control " name="sup_id" id="sup_id">
								<option value='' >Select Supervisor</option>
								<?php
								if( ! empty($supervisor_list))
								{
									foreach($supervisor_list as $single_supervisor)
									{
										?><option value='<?php echo $single_supervisor['id'];?>'><?php echo $single_supervisor['c_name'];?></option><?php
									}
								}
								?>
							</select>
						</div>
						<div class="col-sm-2">
							<input type='submit' class="btn btn-info" placeholder="Submit" name="btn_submit" id="btn_submit" value='Add Supervisors'  />
						</div>
					</div>
				</form>
				<br>
				<div class="table-responsive">
					<table class="table table-striped table-bordered table-hover no-footer dtr-inline ">
						<thead>
							<tr>
								<th>Sr</th>
								<th>Supervisor</th>
								<th>Email</th>
								<th>Contact Number</th>
								<th>Date Added</th>
								<th>Date Updated</th>
								<th>Open Request</th>
								<th>Action</th>
							</tr>
						</thead>
						<tbody>
						<?php
						if( ! empty($added_supervisors_list))
						{
							$sr = 1;
							$delete_permission = false;
							if($this->common_model->check_permission("Supervisors Remove"))
							{
								$delete_permission = true;
							}
							foreach($added_supervisors_list as $single_supervisor)
							{
								$action = '';
								if($delete_permission && $single_supervisor['open_request'] == 'N')
								{
									$action = '<a href="javascript:void(0);" onclick="delete_supervisor('.$single_supervisor['id'].','.$tid.')">Delete</a>';
								}

								?>
								<tr>
									<td><?php echo $sr++; ?></td>
									<td><?php echo $single_supervisor['c_name']; ?></td>
									<td><?php echo $single_supervisor['c_email']; ?></td>
									<td><?php echo $single_supervisor['c_contacts']; ?></td>
									<td><?php echo $single_supervisor['date_added']; ?></td>
									<td><?php echo $single_supervisor['date_updated']; ?></td>
									<td><?php echo $single_supervisor['open_request']; ?></td>
									<td><?php echo $action;?></td>
								</tr>
							<?php
							}
						}
						?>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>

<!--===================================================-->
<!--END CONTENT CONTAINER-->
<script>

function delete_supervisor(id,tid)
{
	if(id == '' || id == undefined)
	{
		Display_msg('Invalid document for deletion.','error');
		return false;
	}
	else
	{
		if(id > 0)
		{
			var ans = confirm('Are you sure you want to delete this supervisor?');
			if(ans)
			{
				$.ajax(
				{
					url: "<?php echo base_url('auctioneer/tender/delete_supervisor'); ?>",
					type: "POST",
					dataType:"json",
					cache: false,
					clearForm: false,
					data:{"sup_id":id,'tid':tid},
					success:function(response)
					{
						if(response.status == "success")
						{
							Display_msg(response.message,response.status);
							setTimeout(function()
							{
								window.location.reload();
							}, 3000);
						}
						else
						{
							Display_msg(response.message,response.status);
							return false;
						}
					}
				});
			}
		}
		else
		{
			Display_msg('Please Select Auction.','error');
			return false;
		}
	}
}

$(function(){
	var vRules = {
		sup_id:{required:true}
	};
	var vMessages = {
		sup_id:{required:"<p class='text-danger'>Please select supervisor</p>"}
	};

	$("#form_suppervisior").validate({
		rules: vRules,
		messages: vMessages,
		submitHandler: function(form){
			$(form).ajaxSubmit({
				url:"<?php echo base_url('auctioneer/tender/add_edit_supervisor');?>",
				type: 'post',
				dataType:'json',
				cache: false,
				success: function (response) {
					if(response.status=="success")
					{
						Display_msg(response.message,response.status);
						setTimeout(function()
						{
							window.location.reload();
						}, 3000);
					}
					else
					{
						Display_msg(response.message,response.status);
						return false;
					}
				}
			});
		}
	})
});
</script>
