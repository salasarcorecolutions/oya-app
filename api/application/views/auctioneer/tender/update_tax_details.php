<!-- In order to let it work in CI, need to fix it to only get the viwes -->

<!--CONTENT CONTAINER-->
<!--===================================================-->
<div id="content-container">
	<!--Page Title-->
	<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
	<div id="page-head">
	<div id="page-title">
		<h1 class="page-header text-overflow">E-Tender Lots Tax Details</h1>
	</div>
	<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
	<!--End page title-->


	<!--Breadcrumb-->
	<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
	<ol class="breadcrumb">
		<li><a href="<?php echo base_url('dashboard');?>">Home</a></li>
		<li><a href="<?php echo base_url('auctioneer/tender/index');?>">E-Tender</a></li>
		<li><a href="<?php echo base_url('auctioneer/tender/view_lots/'.$tid);?>">E-Tender Lots</a></li>
		<li class="active">E-Tender Lots Tax Details</li>
	</ol>
	</div>
	<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
	<!--End breadcrumb-->
	<input type="hidden" id="t11" name="auctionId">
	<div id="page-content">
		<div class="float_btn">
			<a class="btn btn-primary btn-icon btn-circle add-tooltip"  data-placement="top" data-toggle="tooltip" data-original-title="Back" href='<?php echo base_url('auctioneer/tender/view_lots/'.$tid); ?>' ><i class="fa fa-arrow-left" aria-hidden="true"></i></a>
		</div>	

		<div class="panel">
			<div class="panel-heading">
				<h3 class="panel-title">E-Tender Lots Tax Details</h3>
			</div>

			<div class="panel-body">
				<div class="table-responsive">
					<table class="table table-striped table-bordered table-hover no-footer dtr-inline">
						<thead>
							<tr>
								<th>Sale No</th>
								<th>Start Time</th>
								<th>End Time</th>
								<th>Particulars</th>
							</tr>
							<tr>
								<td><?php echo $auction_details['tender_id'] ?></td>
								<td><?php echo date('d-m-Y H:i:s',strtotime($auction_details['topen_dt'])) ?></td>
								<td><?php echo date('d-m-Y H:i:s',strtotime($auction_details['tclose_dt'])) ?></td>
								<td><?php echo $auction_details['particulars'] ?></td>
							</tr>
						</thead>
					</table>
				</div>

				<div class="table-responsive">
					<form id="form_lot_update_timing" name="form_lot_update_timing" method="post">
						<input type="hidden" id="tid" name="tid" value="<?php echo $tid; ?>" />
						<table class="table table-striped table-bordered table-hover no-footer dtr-inline">
							<thead>
								<tr>
									<th width="20"><input name="checkbox_lot_all" type="checkbox" onchange="check_all_checkbox(this);" ></th>
									<th>Lot No</th>
									<th>Material Code</th>
									<th>ED</th>
									<th>VAT</th>
									<th>CST</th>
									<th>TCS</th>
									<th>GST</th>
								</tr>
							</thead>
							<tbody>
								<?php
								if( ! empty($lot_details))
								{
									foreach($lot_details as $single_lot)
									{
										if(empty($view_tax[$single_lot['lotno']]))
										{
											$view_tax[$single_lot['lotno']]['ED'] = '';
											$view_tax[$single_lot['lotno']]['VAT'] = '';
											$view_tax[$single_lot['lotno']]['CST'] = '';
											$view_tax[$single_lot['lotno']]['TCS'] = '';
											$view_tax[$single_lot['lotno']]['Other'] = '';
										}
										?>
										<tr>
											<td><input class="chk_all chkbox" name="checkbox_lot_<?php echo $single_lot['lotno']?>" type="checkbox" onchange="enable_disable_lots('<?php echo $single_lot['lotno']?>',this);" ></td>
											<td><?php echo $single_lot['lotno']?></td>
											<td><?php echo $single_lot['mat_code']?></td>
											<td>
												<input disabled="disabled" class="form-control lot_<?php echo $single_lot['lotno']?>" name="ED[<?php echo $single_lot['lotno']?>]" required style="width:100px;" value="<?php echo $view_tax[$single_lot['lotno']]['ED'];?>" placeholder="ED" title='Enter Excise Duty'  autocomplete="off"  />
											</td>
											<td>
												<input disabled="disabled" class="form-control lot_<?php echo $single_lot['lotno']?>" name="VAT[<?php echo $single_lot['lotno']?>]" required style="width:100px;" placeholder="VAT" value="<?php echo $view_tax[$single_lot['lotno']]['VAT'];?>" title='Enter VAT'  autocomplete="off"  />
											</td>
											<td>
												<input disabled="disabled" class="form-control lot_<?php echo $single_lot['lotno']?>" name="CST[<?php echo $single_lot['lotno']?>]" maxlength="8" required style="width:100px;" placeholder="CST" value="<?php echo $view_tax[$single_lot['lotno']]['CST'];?>" title='Enter CST' autocomplete="off"  />
											</td>
											<td>
												<input disabled="disabled" class="form-control lot_<?php echo $single_lot['lotno']?>" name="TCS[<?php echo $single_lot['lotno']?>]" required style="width:100px;" placeholder="TCS" value="<?php echo $view_tax[$single_lot['lotno']]['TCS'];?>" title='Enter TCS' autocomplete="off"  />
											</td>
											<td>
												<input disabled="disabled" class="form-control lot_<?php echo $single_lot['lotno']?>" name="Other[<?php echo $single_lot['lotno']?>]" maxlength="8" required style="width:100px;" placeholder="GST" value="<?php echo $view_tax[$single_lot['lotno']]['Other'];?>" title='Enter GST' autocomplete="off"   />
											</td>
										</tr><?php
									}
								}
								?>
							</tbody>
							<tfoot>
								<?php
								if( ! empty($lot_details))
								{ ?>
									<tr>
										<td colspan = '9' class="text-center">
											<input type="submit" class="btn btn-primary" Value="Submit"/>
											<a class="btn btn-warning" href='<?php echo base_url('auctioneer/tender/view_lots/'.$tid) ;?>' >Back</a>
										</td>
									</tr>
									<?php
								}
								?>
							</tfoot>
						</table>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
<!--===================================================-->
<!--END CONTENT CONTAINER-->
<script>
/* Check All Lots */
function check_all_checkbox(obj){
	if($(obj).is(":checked"))
	{
		$('.chk_all').each(function()
		{
			$(this).prop('checked',true);
			$(this).change();
		});
	}
	else
	{
		$('.chk_all').each(function()
		{
			$(this).prop('checked',false);
			$(this).change();
		});
	}

}

/* Selected Lots Enable Disable */
function enable_disable_lots(lotno,obj)
{
	if($(obj).is(":checked"))
	{
		$(".lot_"+lotno).each(function()
		{
			$(this).removeAttr("disabled");
		});
	}
	else
	{
		$(".lot_"+lotno).each(function()
		{
			$(this).attr("disabled","disabled");
		});
	}
}

$(function()
{
	/* Select Multiple Checkbox With Shift Key - Start */
	var $chkboxes = $('.chkbox');
    var lastChecked = null;

	$chkboxes.click(function(e) {
		console.log('in');
        if (!lastChecked) {
            lastChecked = this;
            return;
        }

        if (e.shiftKey) {
            var start = $chkboxes.index(this);
            var end = $chkboxes.index(lastChecked);

            $chkboxes.slice(Math.min(start,end), Math.max(start,end)+ 1).prop('checked', lastChecked.checked);
			$chkboxes.change();
        }

        lastChecked = this;
    });
	/* Select Multiple Checkbox With Shift Key - End */

	/* Form Submit */
	var vRules = {};
	var vMessages = {};

	$("#form_lot_update_timing").validate(
	{
		rules: vRules,
		messages: vMessages,
		submitHandler: function(form)
		{
			$(form).ajaxSubmit(
			{
				url:"<?php echo base_url('auctioneer/tender/update_tax_details/'.$tid);?>",
				type: 'post',
				dataType:'json',
				cache: false,
				clearForm: false,
				success: function (response)
				{
					if(response.status=="success")
					{
						Display_msg(response.message,response.status);

						setTimeout(function()
						{
							window.location.href = "<?php echo base_url('auctioneer/tender/view_lots/'.$tid) ?>";
						}, 3000);
					}
					else
					{
						Display_msg(response.message,response.status);
						return false;
					}
				}
			});
		}
	});
});

</script>
