<style>
textarea.form-control{
	max-width: 384px;
}	
	p {
    margin: 0px;
}

.form-group {
    margin-bottom: 7px;
}
.mdtp__wrapper {
		bottom:175px!important;
	}
.select2-container{
	padding: 0 !important;
}
.form-horizontal .control-label{
	text-align:left !important;
}
/* If the bid logic is Rank */
.highlight {
	background-color: #f3f3f3;
	padding: 10px;
	margin-bottom: 10px;
    border-radius: 8px;
}
</style>
<link href="<?php echo base_url('assets/css/mdtimepicker.css'); ?>" rel="stylesheet" />
<link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/css/bootstrap-datetimepicker.css" rel="stylesheet" />
<link href=”//cdn.rawgit.com/Eonasdan/bootstrap-datetimepicker/e8bddc60e73c1ec2475f827be36e1957af72e2ea/build/css/bootstrap-datetimepicker.css” rel=”stylesheet”>



<!--CONTENT CONTAINER-->
<!--===================================================-->
<div id="content-container">
<div id="page-head">
	<!--Page Title-->
	<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
	<div id="page-title">
		<h1 class="page-header text-overflow"><?php echo $pagetitle; ?></h1>
	</div>
	<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
	<!--End page title-->
	<!--Breadcrumb-->
	<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
	<ol class="breadcrumb">
		<li><a href="<?php echo base_url('dashboard');?>"><i class="fa fa-home"></i></a></li>
		<li><a href="<?php echo base_url('/auctioneer/salvage/index');?>">All Salvage Auction</a></li>
		<li><a class="active"><?php echo $pagetitle; ?></a></li>
	</ol>
	<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
	<!--End breadcrumb-->
</div>
	<!--Page content-->
	<!--===================================================-->
	<div id="page-content">
		<div class="float_btn">
			<a class="btn btn-primary btn-icon btn-circle add-tooltip" data-placement="top" data-toggle="tooltip" data-original-title="Back" href='<?php echo base_url('/auctioneer/salvage/index'); ?>' ><i class="fa fa-arrow-left" aria-hidden="true"></i></a>
		</div>
		<div class="row">
			<div class="col-sm-12">
				<!--Primary Panel-->
				<!--===================================================-->
				<div class="panel panel-primary">
					<div class="panel-heading">
						<h3 class="panel-title"><?php echo $pagetitle; ?></h3>
					</div>
					<!--Horizontal Form-->
					<!--===================================================-->
					<form class="form-horizontal" method="post" name="salvage_addedit" id="salvage_addedit">
						<input type='hidden' id='product_id' name='product_id' value='<?php echo ! empty($product_id) ? $product_id : ''; ?>' />
						<div class="row">
							<div class="panel-body">
								<div class="col-sm-6">
									<h4 class="text-main"><span style="font-size:16px; " class="label label-primary">Company Name</span> : <small><?php echo $company[0]['vendor_name']; ?></small></h4>
								</div>
								
							</div>
						</div>
						<div class="panel-body">
							<div class="col-sm-6">
							
									<div class="form-group">
										<div class="col-sm-4">
											<label class="control-label">E-Auction Sale No<span class="text-danger"><i class="fa fa-asterisk" aria-hidden="true"></i></span></label>
										</div>
										<div class="col-sm-8">
											<input type='text' class='form-control' name='sale_no' value='<?php echo $sale_no?>' readonly>
										</div>
									</div>
							</div>

							<div class="col-sm-6">
									<div class="form-group">
										<div class="col-sm-4">
											<label class="control-label">Price Currency<span class="text-danger"><i class="fa fa-asterisk" aria-hidden="true"></i></span></label>
										</div>
										<div class="col-sm-8">
											<select name="price_currency" id="price_currency" class="form-control select2 required" title="Choose Auction Currency">
											<option ></option>
											<?php
											if ( ! empty($all_currency))
											{
												foreach ($all_currency as $single_currency)
												{
													$selected = '';
													if ($single_currency['currency_name'] == $price_currency)
													{
														$selected = "selected";
													}
													?><option value='<?php echo $single_currency['currency_name'];?>' <?php echo $selected; ?>><?php echo $single_currency['currency_name'];?></option><?php
												}
											}
											?>
										</select>
										</div>
									</div>
							</div>

							<div class="col-sm-6">
									<div class="form-group">
										<div class="col-sm-4">
											<label class="control-label">Product Name<span class="text-danger"><i class="fa fa-asterisk" aria-hidden="true"></i></span></label>
										</div>
										<div class="col-sm-8">
											<input class="form-control" type="text" name="product_name" id="product_name" size="52" maxlength="500" value='<?php echo ! empty($product_name) ? $product_name : ""; ?>'>
										</div>
									</div>
							</div>
						
							<div class="col-sm-6">
									<div class="form-group">
										<div class="col-sm-4">
											<label class="control-label">Description<span class="text-danger"><i class="fa fa-asterisk" aria-hidden="true"></i></span></label>
										</div>
										<div class="col-sm-8">
											<textarea rows='1' class="form-control ckeditor" name="short_description" id="short_description" required><?php echo ! empty($short_description) ? $short_description : "" ?></textarea>
										</div>
									</div>
							</div>

							<div class="col-sm-6">
									<div class="form-group">
										<div class="col-sm-4">
											<label class="control-label">Product Category<span class="text-danger"><i class="fa fa-asterisk" aria-hidden="true"></i></span></label>
										</div>
										<div class="col-sm-8">
											<select id="parent_category_id" name="parent_category_id">
												<option></option>
												<?php foreach ($product_category as $value){ ?>
													<option value="<?php echo $value['category_id']; ?>"><?php echo $value['category_name']; ?></option>
												<?php } ?>
											</select>
										</div>
									</div>
							</div>
						
							<div class="col-sm-6">
									<div class="form-group">
										<div class="col-sm-4">
											<label class="control-label">Sub Product Category</label>
										</div>
										<div class="col-sm-8">
											<select id="level_2_category_id" name="level_2_category_id">
												<option></option>
											</select>
										</div>
									</div>
							</div>
							<div class="col-sm-6">
								<div class="form-group">
									<div class="col-sm-4">
										<label class="control-label">Vendor Name <span class="text-danger"><i class="fa fa-asterisk" aria-hidden="true"></i></span></label>
									</div>
									<div class="col-sm-8">
										<select id="vendor_id" name="vendor_id">
											<option value=""></option>
											<?php foreach ($vendor_list as $k => $v){ ?>
												<option value="<?php echo $k; ?>"><?php echo $v; ?></option>
											<?php } ?>
										</select>
										<label style='display:none;' id="vendor_id-error" class="error" for="vendor_id"><p class="text-danger">Please Enter The Company </p></label>
									</div>
								</div>
							</div>
							<div class="col-sm-6">
								<div class="form-group">
									<div class="col-sm-4">
										<label class="control-label">Plant Name</label>
									</div>
									<div class="col-sm-8">
										<select id="plant" name="plant[]">
											<option value=""></option>
										</select>
									</div>
								</div>
							</div>
							<?php if ($this->session->userdata('registration_type') == 0){ ?>
							<!-- 	<div class="col-sm-6">
									<div class="form-group">
										<div class="col-sm-4">
											<label class="control-label">Do You Want to Conduct the Auction for Linked Company<span class="text-danger"><i class="fa fa-asterisk" aria-hidden="true"></i></span></label>
										</div>
										<div class="col-sm-8">
											<input type="checkbox" id="show_company_plant" style="margin: 25px 0px 0;" name="linked_company" value="1" />
										</div>
									</div>
								</div> -->

							<?php } ?>

							<!-- <div class="col-sm-6 other_company">
									<div class="form-group">
										<div class="col-sm-4">
											<label class="control-label">Company<span class="text-danger"><i class="fa fa-asterisk" aria-hidden="true"></i></span></label>
										</div>
										<div class="col-sm-8">
											<select class="form-control" size="1" name="company_id" id="company_id"></select>
										</div>
									</div>
							</div> -->

							<!-- <div class="col-sm-6 vendor_plant other_company">
									<div class="form-group">
										<div class="col-sm-4">
											<label class="control-label">Plant<span class="text-danger"><i class="fa fa-asterisk" aria-hidden="true"></i></span></label>
										</div>
										<div class="col-sm-8">
											<select class="form-control" size="1" name="plant" id="plant">
												<?php
													if ($company_id != $_SESSION['vendor_id']):
														$plant_ids = explode(",",$plant_id);
														$plant_names = explode(",",$plant_name);
														for ($x = 0; $x<sizeof($plant_ids); $x++):
														{
															$selected = "selected";
															?><option value='<?php echo $plant_ids[$x]; ?>' <?php echo $selected; ?>><?php echo $plant_names[$x];?></option><?php
														}
														endfor;
													endif;
												?>
											</select>
										</div>
									</div>
							</div> -->

							<div class="col-sm-6">
									<div class="form-group">
										<div class="col-sm-4">
											<label class="control-label">Auction Start Date<span class="text-danger"><i class="fa fa-asterisk" aria-hidden="true"></i></span></label>
										</div>
										<div class="col-sm-8">
											<input class="form-control" name="start_date" id="start_date" readonly type="text" value='<?php echo ! empty($start_date) ? servertz_to_usertz($start_date,$this->session->userdata('user_tz'),'Y-m-d') : ""; ?>'/>
										</div>
									</div>
							</div>

							<div class="col-sm-6">
									<div class="form-group">
										<div class="col-sm-4">
											<label class="control-label">Auction Open Up to<span class="text-danger"><i class="fa fa-asterisk" aria-hidden="true"></i></span></label>
										</div>
										<div class="col-sm-8">
											<input class="form-control" name="end_date" id="end_date" readonly type="text" value='<?php echo ! empty($end_date) ? servertz_to_usertz($end_date,$this->session->userdata('user_tz'),'Y-m-d') : ""; ?>' />
										</div>
									</div>
							</div>

							<div class="col-sm-6">
									<div class="form-group">
										<div class="col-sm-4">
											<label class="control-label">Auction Start Time<span class="text-danger"><i class="fa fa-asterisk" aria-hidden="true"></i></span></label>
										</div>
										<div class="col-sm-8">
											<input class="form-control" name="start_time" type="text" id="start_time" type="time" value='<?php echo ! empty($start_time) ? servertz_to_usertz($start_time,$this->session->userdata('user_tz'),'H:i:s') : ""; ?>' />
										</div>
									</div>
							</div>

							<div class="col-sm-6">
									<div class="form-group">
										<div class="col-sm-4">
											<label class="control-label">Auction End Time<span class="text-danger"><i class="fa fa-asterisk" aria-hidden="true"></i></span></label>
										</div>
										<div class="col-sm-8">
											<input class="form-control" name="end_time" type="text" id="end_time" type="time" value='<?php echo ! empty($end_time) ? servertz_to_usertz($end_time,$this->session->userdata('user_tz'),'H:i:s') : ""; ?>' />
										</div>
									</div>
							</div>

							<div class="col-sm-6">
									<div class="form-group">
										<div class="col-sm-4">
											<label class="control-label">Country<span class="text-danger"><i class="fa fa-asterisk" aria-hidden="true"></i></span></label>
										</div>
										<div class="col-sm-8">
											<select id="country_id" name="country_id">
												<option></option>
												<?php foreach ($country as $value){ ?>
													<option value="<?php echo $value['id']; ?>"><?php echo $value['name']; ?></option>
												<?php } ?>
											</select>
										</div>
									</div>
							</div>
						

							<div class="col-sm-6">
									<div class="form-group">
										<div class="col-sm-4">
											<label class="control-label">State<span class="text-danger"><i class="fa fa-asterisk" aria-hidden="true"></i></span></label>
										</div>
										<div class="col-sm-8">
											<select id="state_id" name="state_id">
												<option></option>
											</select>
										</div>
									</div>
							</div>
						
							<div id="s_other" class="col-sm-6">
									<div class="form-group">
										<div class="col-sm-4">
											<label class="control-label">Other State<span class="text-danger"><i class="fa fa-asterisk" aria-hidden="true"></i></span></label>
										</div>
										<div class="col-sm-8">
											<input class="form-control" name="state_other" type="text" id="state_other" type="text"  />
										</div>
									</div>
							</div>
							
							
						
							
							<div id="s_other" class="col-sm-6">
									<div class="form-group">
										<div class="col-sm-4">
											<label class="control-label"for="city_id"> City<span class="text-danger"><i class="fa fa-asterisk" aria-hidden="true"></i></span></label>
										</div>
										<div class="col-sm-8">
											<select id="city_id" name="city_id">
												<option></option>
											</select>
										</div>
									</div>
							</div>

							<div id="c_other" class="col-sm-6">
									<div class="form-group">
										<div class="col-sm-4">
											<label class="control-label" for="city_id">Other City<span class="text-danger"><i class="fa fa-asterisk" aria-hidden="true"></i></span></label>
										</div>
										<div class="col-sm-8">
											<input class="form-control" name="city_other" type="text" id="city_other" type="text"  />
										</div>
									</div>
							</div>
							

							<div class="col-sm-6">
									<div class="form-group">
										<div class="col-sm-4">
											<label class="control-label" for="manufacturer_id">Manufacturer<span class="text-danger"><i class="fa fa-asterisk" aria-hidden="true"></i></span></label>
										</div>
										<div class="col-sm-8">
											<select id="manufacturer_id" name="manufacturer_id">
												<option></option>
												<?php foreach ($manufacturer as $value){ ?>
													<option value="<?php echo $value['manufacture_id']; ?>"><?php echo $value['manufacturer_name']; ?></option>
												<?php } ?>
												<option value="other">Other</option>
											</select>
										</div>
									</div>
							</div>
						
							<div id="manu_other" class="col-sm-6">
									<div class="form-group">
										<div class="col-sm-4">
											<label class="control-label" for="manufacturer_other">Other Manufacturer<span class="text-danger"><i class="fa fa-asterisk" aria-hidden="true"></i></span></label>
										</div>
										<div class="col-sm-8">
											<input class="form-control" name="manufacturer_other" type="text" id="manufacturer_other" type="text"  />
										</div>
									</div>
							</div>

							
							<div class="col-sm-6">
									<div class="form-group">
										<div class="col-sm-4">
											<label class="control-label" for="manufacturer_year">Manufacturer Year<span class="text-danger"><i class="fa fa-asterisk" aria-hidden="true"></i></span></label>
										</div>
										<div class="col-sm-8">
											<input class="form-control" name="manufacturer_year" type="text" id="manufacturer_year" type="text" value="<?php echo ! empty($manufacturer_year) ? $manufacturer_year : ""; ?>" />
										</div>
									</div>
							</div>

							<div class="col-sm-6">
									<div class="form-group">
										<div class="col-sm-4">
											<label class="control-label" for="dimension" >Model No.<span class="text-danger"></span></label>
										</div>
										<div class="col-sm-8">
											<input class="form-control" name="model_no" type="text" id="model_no" type="text" value="<?php echo ! empty($model_no) ? $model_no : ""; ?>"  />
										</div>
									</div>
							</div>

							<div class="col-sm-6">
									<div class="form-group">
										<div class="col-sm-4">
											<label class="control-label" for="dimension" >Dimension</label>
										</div>
										<div class="col-sm-8">
											<input class="form-control" name="dimension" type="text" id="dimension" type="text" value="<?php echo ! empty($dimension) ? $dimension : ""; ?>" />
										</div>
									</div>
							</div>

							<div class="col-sm-6">
									<div class="form-group">
										<div class="col-sm-4">
											<label class="control-label" for="serial_no">Serial No.</label>
										</div>
										<div class="col-sm-8">
											<input class="form-control" name="serial_no" type="text" id="serial_no" type="text" value="<?php echo ! empty($serial_no) ? $serial_no : ""; ?>" />
										</div>
									</div>
							</div>

						

							<div class="col-sm-6">
									<div class="form-group">
										<div class="col-sm-4">
											<label class="control-label">Price<span class="text-danger"><i class="fa fa-asterisk" aria-hidden="true"></i></span></label>
										</div>
										<div class="col-sm-8">
											<input class="form-control" name="price" type="text" id="price" type="text" value="<?php echo ! empty($price) ? $price : ""; ?>" />
										</div>
									</div>
							</div>

							<div class="col-sm-6">
									<div class="form-group">
										<div class="col-sm-4">
											<label class="control-label" >Buying Logic<span class="text-danger"><i class="fa fa-asterisk" aria-hidden="true"></i></span></label>
										</div>
										<div class="col-sm-8">
											<select class="form-control" name="buying_format" id="buying_format">
												<option value="">--Select Buying logic--</option>
												<option <?php if ( ! empty($buying_format) && $buying_format == 'Live Auction'){ echo 'selected'; } ?> value="Live Auction">Live Auction</option>
												<option <?php if ( ! empty($buying_format) && $buying_format == 'Make Offer'){ echo 'selected'; } ?> value="Make Offer">Make Offer</option>
												<option <?php if ( ! empty($buying_format) && $buying_format == 'Online Auction'){ echo 'selected'; } ?> value="Online Auction">Online Auction</option>
												<option <?php if ( ! empty($buying_format) && $buying_format == 'Buy Now'){ echo 'selected'; } ?> value="Buy Now">Buy Now</option>
												<option <?php if ( ! empty($buying_format) && $buying_format == 'Sailed Bid'){ echo 'selected'; } ?> value="Sailed Bid">Sailed Bid</option>
											</select>
										</div>
									</div>
							</div>
							<div class="col-sm-6">
									<div class="form-group">
										<div class="col-sm-4">
											<label class="control-label" >Reserved Price<span class="text-danger"><i class="fa fa-asterisk" aria-hidden="true"></i></span></label>
										</div>
										<div class="col-sm-8">
											<input class="form-control" name="reserve_price" type="text" id="reserve_price" type="text" value="<?php echo ! empty($reserve_price) ? $reserve_price : ""; ?>" />

										</div>
									</div>
							</div>
							<div class="col-sm-6">
									<div class="form-group">
										<div class="col-sm-4">
											<label class="control-label" >Result Date<span class="text-danger"><i class="fa fa-asterisk" aria-hidden="true"></i></span></label>
										</div>
										<div class="col-sm-4">
										<input class="form-control datepickerclass" name="result_date" type="text" id="result_date" type="text" value="<?php echo ! empty($result_date) ? servertz_to_usertz($result_date,$this->session->userdata('user_tz'),'Y-m-d') : ""; ?>" readonly/>
										</div>
										
										
									</div>
							</div>
						</div>
							<div class="form-group">
								<label class="col-sm-2 control-label">&nbsp;</label>
								<div style="text-align: center" class="col-sm-12">
									<input class="btn btn-success" type="submit" value='Submit' name="btn_submit" id='btn_submit' />
									<a class="btn btn-danger" href='<?php echo base_url('auctioneer/salvage/index'); ?>'>Back</a>
								</div>
							</div>
					</form>
					<!--===================================================-->
					<!--End Horizontal Form-->
				</div>
				<!--===================================================-->
				<!--End Primary Panel-->
			</div>

		</div>	
	</div>
	<!--===================================================-->
	<!--End page content-->
</div>
<!--===================================================-->
<!--END CONTENT CONTAINER-->



<script src="https://cdnjs.cloudflare.com/ajax/libs/datepicker/0.6.5/datepicker.js" integrity="sha256-KgOC04qt96w+EFba7KuNt9sT+ahJua25I0I2rrkZHFo=" crossorigin="anonymous"></script>
<script src="<?php echo base_url('assets/js/mdtimepicker.js'); ?>"></script>
<script src="https://cdn.ckeditor.com/4.14.0/standard/ckeditor.js"></script>


<script>

$(document).ready(function() {

	$("#manu_other").hide();
	$("#c_other").hide();
	$("#s_other").hide();

	$("#auction_conduct_type").select2({
		placeholder: 'Auction Conduct'
	});


	$('#mat_id').select2({
		placeholder: "Select Material"
	});
	
	$('#country_id').select2({
		placeholder: "Select Country Name",
		width: "100%"
	});
	$('#state_id').select2({
		placeholder: "Select State Name",
		width: "100%"
	});
	$('#city_id').select2({
		placeholder: "Select City Name",
		width: "100%"
	});

	$('#manufacturer_id').select2({
		placeholder: "Select Manufacturer Name",
		width: "100%"
	});

	$('#level_2_category_id').select2({
		placeholder: "Select Sub Category",
		width: "100%"
	});
	$('#vendor_id').select2({
		placeholder: "Select Vendor",
		width: "100%"
	});
	$('#plant').select2({
		placeholder: "Select Plant",
		width: "100%"
	});

	$('#parent_category_id').select2({
		placeholder: "Select Category",
		width: "100%"
	});

	$('#price_currency').select2({
		placeholder: "Select price currency",
		width: "100%"
	});

	$('#start_time').mdtimepicker({
		format: 'hh:mm:ss',
		theme: 'blue'
	}); //Initializes the time picker and uses the specified format (i.e. 23:30)
	$('#end_time').mdtimepicker({
		format: 'hh:mm:ss',
		theme: 'blue'
	}); //Initializes the time picker and uses the specified format (i.e. 23:30)
	<?php  if(! empty($company_id))
	{
	?>
			$("#vendor_id").val(<?php echo $company_id?>).select2({
				
				width: "100%"
			});
			$("#vendor_id").trigger("change");
	<?php
	}?>
	
	<?php if ( ! empty($start_date) && ! empty($end_date)){ ?>
		$("#start_date").datepicker({
			format: 'yyyy-mm-dd',
			startDate: new Date("<?php echo $start_date; ?>")
		});
		$("#end_date").datepicker({
            format: 'yyyy-mm-dd',
            startDate: new Date("<?php echo $start_date; ?>")
        });
	<?php } else { ?>
	$("#start_date").datepicker({
        format: 'yyyy-mm-dd',
        startDate: new Date("<?php echo $today_date; ?>")
    });
	<?php } ?>
    $("#start_date").change(function(){
        startDatee = '"'+$("#start_date").val()+'"';
		$("#end_date").val('');
		$("#end_date").datepicker('destroy');
        $("#end_date").datepicker({
            format: 'yyyy-mm-dd',
            startDate: new Date(startDatee)
        });
    });
	<?php if ( ! empty($product_id)){ ?>
		changeProductValues()
	<?php } ?>
});

$("#show_company_plant").change(function() {
	var id = document.getElementById('show_company_plant');
	var registration_type = "<?php echo $registration_type; ?>";
	if (id.checked === true) {
		if (registration_type == 0){
			$(".other_company").show();
		} else {
			$(".vendor_plant").show();
		}
	} else {
		if(registration_type == 0)
		{
			$('#company_id').val(null).trigger('change');
			$('#plant').val(null).trigger('change');
			$(".other_company").hide();
		} else {
			$('#plant').val(null).trigger('change');
			$(".vendor_plant").hide();
		}
	}
})
$("#vendor_id").change(function(){
	$("#plant").select2('val', '');
	var company_id = $("#vendor_id").val();
	$.ajax({
		url: "<?php echo base_url('auctioneer/salvage/get_plant_details') ?>",
		type: "POST",
		data:{
			'company_id': company_id
		},
		dataType: "json",
		async: false,
		success: function(data)
		{
			var html = '';
			var i;
			for(i=0; i<data.length; i++)
			{
				html += '<option value="'+data[i].company_plant_id+'">'+data[i].plant_name+'</option>';
			}
			$('#plant').html(html);
		}
	});
});
/* $("#company_id").change(function(){
	$("#plant").select2('val', '');
	var company_id = $("#company_id").val();
	var plantId = "<?php  if ( ! empty($company_id) && $this->session->userdata('vendor_id') == $company_id) { echo ""; } else { 	echo ! empty($plant_id) ? $plant_id : ""; }  ?>";
	$.ajax({
		url: "<?php echo base_url('auctioneer/salvage/get_plant_details') ?>",
		type: "POST",
		data:{
			'company_id': company_id
		},
		dataType: "json",
		async: false,
		success: function(data)
		{
			var html = '';
			var i;
			for(i=0; i<data.length; i++)
			{
				html += '<option value="'+data[i].company_plant_id+'">'+data[i].plant_name+'</option>';
			}
			$('#plant').html(html);
			if (plantId){
				$('#plant').val(plantId).trigger('change');
			}
		}
	});
}); */

$(function(){
	$("#get_auction_select").select2({
		placeholder: "Select Backdated Auction",
		allowClear: true
	});	
	$("#auction_allowed_currency").select2({
		placeholder: "Select Allowed Currency",
		allowClear: true
	});

	$.validator.addMethod('compare_time', function (value, element, param) {
		return this.optional(element) || check_start_end_time();
	}, "<p class='text-danger'>Enter Correct start time and end time</p>");

	function check_start_end_time(){
		var startTime = '"'+document.getElementById("start_time").value+'"';
		var endTime = '"'+document.getElementById("end_time").value+'"';
		var startDate = document.getElementById("start_date").value;
		var endDate = document.getElementById("end_date").value;
		if (startDate == endDate) {
			if (startTime >= endTime){
				return false;
			} else {
				return true;
			}
		} else {
			return true;
		}
	}

	$(".other_company").hide();
	

	var vRules = {
		sale_no: { required:true},
		product_name: { required:true},
		short_description: { 
			required: function() 
				{
					var msg = CKEDITOR.instances['short_description'].getData().replace(/<[^>]*>/gi, '').length;
					if (msg ==0) {
						$("#error").show();
						$("#error").html('<b>Please Enter Description</b>');
						return false;
					}else
					{
						$("#error").hide();
						return false;
					}
				},

				minlength:10
		},
		parent_category_id: { required:true },
		start_date: { required:true },
		end_date: { required:true},
		start_time: { required:true },
		end_time: {
			required: true,
			compare_time: "#start_time"
		},
		country_id: { required:true},
		state_id: { required:true},
		city_id: { required:true},
		manufacturer_id: { required:true},
		price_currency: { required:true},
		manufacturer_year: { required:true },
		price: { required:true },
		buying_format:{required:true},
		reserve_price: {required:true},
		result_date: {required:true},
		vendor_id:{required:true},
		plant: {required:true},
	};

	var vMessages = {
		sale_no: { required:"<p class='text-danger'>Please Enter The E-Auction Sale No</p>" },
		product_name: { required:"<p class='text-danger'>Please Enter Product Name</p>" },
		short_description: { required:"<p class='text-danger'>Please Enter Description</p>" },
		parent_category_id:{ required:"<p class='text-danger'>Please Select Product Category</p>" },
		start_date: { required:"<p class='text-danger'>Please Enter The Auction Start Date</p>" },
		end_date: { required:"<p class='text-danger'>Please Enter Auction Open Up to</p>" },
		start_time: { required:"<p class='text-danger'>Please Enter Start Time </p>" },
		end_time: { required:"<p class='text-danger'>Please Enter End Time</p>" },
		country_id: { required:"<p class='text-danger'>Please Select Country </p>" },
		state_id:{ required:"<p class='text-danger'>Please Select State</p>" },
		city_id: { required:"<p class='text-danger'>Please Select City</p>" },
		manufacturer_id: { required:"<p class='text-danger'>Please Select Manufacturer</p>" },
		price_currency: { required:"<p class='text-danger'>Please Select Price Currency</p>" },
		manufacturer_year: { required:"<p class='text-danger'>Please Enter Manufacturer Year</p>" },
		price: { required:"<p class='text-danger'>Please enter price</p>" },
		buying_format:{required:"<p class='text-danger'>Please select buying format</p>"},
		reserve_price:{required:"<p class='text-danger'>Please Enter Reserve Price</p>"},
		result_date:{required:"<p class='text-danger'>Please Enter Result Date</p>"},
		vendor_id:{required:"<p class='text-danger'>Please Select Company</p>"},
		plant: {required:"<p class='text-danger'>Please Select Plant</p>"},


	};

	$("#salvage_addedit").validate({
		rules: vRules,
		messages: vMessages,
		submitHandler: function(form){
			for (instance in CKEDITOR.instances) {
                CKEDITOR.instances[instance].updateElement();
            }
			$(form).ajaxSubmit({
				url:"<?php echo base_url('auctioneer/salvage/update_data/'); ?>",
				type: 'post',
				dataType:'json',
				cache: false,
				clearForm: false,
				success: function (response) {
					if (response.status=="success"){
						Display_msg(response.message,response.status);
						setTimeout(function(){
							window.location.href="<?php echo base_url('auctioneer/salvage/index');?>";
						}, 3000);							
					} else {	
						Display_msg(response.message,response.status);
					}
				}
			});
		}
	});

});

$("#parent_category_id").change(function(){
	$.ajax({
		url: "<?php echo base_url('auctioneer/salvage/get_sub_parent_category') ?>",
		type: "POST",
		data:{'id':$("#parent_category_id").val()},
		dataType:"json",
		async: false,
		success:function(response){
			var html = "<option></option>";
			if(response.length > 0){
				for (var i=0; i<response.length; i++){
					html = html + '<option value="'+response[i]['category_id']+'">'+response[i]['category_name']+'</option>';
				}
			}
			$("#level_2_category_id").html(html);
		}
	});
});

$("#country_id").change(function(){
	$.ajax({
		url: "<?php echo base_url('auctioneer/salvage/get_state') ?>",
		type: "POST",
		data:{'id':$("#country_id").val()},
		dataType:"json",
		async: false,
		success:function(response){
			$("#state_id").empty();
			var html = "<option></option>";
			if(response.length > 0){
				for (var i=0; i<response.length; i++){
					html = html + '<option value="'+response[i]['id']+'">'+response[i]['state']+'</option>';
				}
			}
			html = html + "<option value='other'>Other</option>";
			$("#state_id").html(html);
		}
	});
});

$("#state_id").change(function(){
	if ($("#state_id").val() !== "other")
	{
		$("#s_other").hide();
		$.ajax({
			url: "<?php echo base_url('auctioneer/salvage/get_city') ?>",
			type: "POST",
			data:{'id':$("#state_id").val()},
			dataType:"json",
			async: false,
			success:function(response){
				$("#city_id").empty();
				var html = "<option></option>";
				if(response.length > 0){
					for (var i=0; i<response.length; i++){
						html = html + '<option value="'+response[i]['id']+'">'+response[i]['name']+'</option>';
					}
				}
				html = html + "<option value='other'>Other</option>";
				$("#city_id").html(html);
			}
		});
	}
	else
	{
		$("#s_other").show();
		$("#city_id").empty();
		var html = "<option></option>";
		html = html + "<option value='other'>Other</option>";
		$("#city_id").html(html);
	}
})

$("#city_id").change(function(){
	if ($("#city_id").val() !== "other"){
		$("#c_other").hide();
	} else {
		$("#c_other").show();
	}
})

$("#manufacturer_id").change(function(){
	if ($("#manufacturer_id").val() !== "other"){
		$("#manu_other").hide();
	} else {
		$("#manu_other").show();
	}
});

<?php if ( ! empty($product_id)){ ?>
function changeProductValues()
{
	var ProductCategory = "<?php echo ! empty($parent_category_id) ? $parent_category_id : ""; ?>";
	var subProductCategory = "<?php echo ! empty($level_2_category_id) ? $level_2_category_id : ""; ?>";
	var countryId = "<?php echo ! empty($country_id) ? $country_id : ""; ?>";
	var stateId = "<?php echo ! empty($state_id) ? $state_id : "other"; ?>";
	var stateOther = "<?php echo ! empty($state_other) ? $state_other : ""; ?>";
	var cityId = "<?php echo ! empty($city_id) ? $city_id : "other"; ?>";
	var cityOther = "<?php echo ! empty($city_other) ? $city_other : ""; ?>";
	var manufacturerId = "<?php echo ! empty($manufacturer_id) ? $manufacturer_id : "other"; ?>";
	var manufacturerOther = "<?php echo ! empty($manufacturer_other) ? $manufacturer_other : ""; ?>";
	if (ProductCategory !== ""){
		$("#parent_category_id").val(ProductCategory).trigger('change');
	}
	if (subProductCategory !== ""){
		$("#level_2_category_id").val(subProductCategory).trigger('change');
	}
	if (countryId !== ""){
		$("#country_id").val(countryId).trigger('change');
	}
	$("#state_id").val(stateId).trigger('change');
	if (stateId == 'other'){
		$("#state_other").val(stateOther);
	}
	$("#city_id").val(cityId).trigger('change');
	if (cityId == 'other'){
		$("#city_other").val(cityOther);
	}
	$("#manufacturer_id").val(manufacturerId).trigger('change');
	if (manufacturerId == 'other'){
		$("#manufacturer_other").val(manufacturerOther);
	}
}

<?php } ?>
$('body').on('click', function (e) {
    $('[data-toggle=popover]').each(function () {
        // hide any open popovers when the anywhere else in the body is clicked
        if (!$(this).is(e.target) && $(this).has(e.target).length === 0 && $('.popover').has(e.target).length === 0) {
            $(this).popover('hide');
        }
    });
});

</script>