
<!--CONTENT CONTAINER-->
<!--===================================================-->
<div id="content-container">
	<!--Page Title-->
	<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
	<div id="page-head">
	<div id="page-title">
		<h1 class="page-header text-overflow">
			Document Upload
		</h1>
	</div>
	<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
	<!--End page title-->

	<!--Breadcrumb-->
	<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
	<ol class="breadcrumb">
		<li><a href="<?php echo base_url('dashboard');?>">Home</a></li>
		<li class="active"><a href="<?php echo base_url('auctioneer/salvage/index');?>">Salvage Auction</a></li>
		<li class="active">Salvage Auction Documents</li>
	</ol>
</div>
	<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
	<!--End breadcrumb-->
	<div id="page-content">	
		<div class="float_btn">
			<a class="btn btn-primary btn-icon btn-circle add-tooltip" data-placement="top" data-toggle="tooltip" data-original-title="Back" href='<?php echo base_url('auctioneer/salvage/index'); ?>' ><i class="fa fa-arrow-left" aria-hidden="true"></i></a>
		</div>
		<div class="panel">
			<div class="panel-heading">
				<h3 class="panel-title">Salvage Auction Documents</h3>
			</div>

			<div class="panel-body">
				<form name="salvage_auction_other_doc" id="salvage_auction_other_doc" method="post" >
					<input type='hidden' id="product_id" name="product_id" value='<?php echo $product_id; ?>' />
					<div class="row filter">
						<div class="col-sm-2">
							<input type='text' class="form-control " placeholder="Document Name" name="doc_name" id="doc_name"  />
						</div>
						<div class="col-sm-2">
							<select class="form-control" name="document_type">
								<option value="">--Document Type--</option>
								<option value="Terms and Condition">Terms and Condition</option>
								<option value="Product Details">Product Details</option>
								<option value="Other">Other</option>
							</select>
						</div>
						<div class="col-sm-2">
							<input type='file' class="form-control " placeholder="Upload File" name="upload_file" id="upload_file"  />
						</div>
						<div class="col-sm-2">
							<input type='submit' class="btn btn-info" placeholder="Submit" name="btn_submit" id="btn_submit"  />
						</div>
					</div>
				</form>
				<br>
				<div class="table-responsive">
					<table class="table table-striped table-bordered table-hover no-footer dtr-inline ">
						<thead>
							<tr>
								<th data-bSortable="false">#</th>
								<th>Document Name</th>
								<th>Document Type</th>
								<th>Added Date</th>
								<th>Action</th>
							</tr>
						</thead>
						<tbody>
						<?php
						if ( ! empty($document_list))
						{
							$sr = 1;
							$delete_permission = false;
							// if (has_page_permission("forward_auction","doc_file_delete"))
							// {
								$delete_permission = true;
							// }
							foreach ($document_list as $single_doc)
							{
								$action = '';
								if($delete_permission)
								{
									$action = '<a href="javascript:void(0);" onclick="delete_document('.$single_doc['document_id'].','.$product_id.')">Delete</a>';
								}
								
                        ?>
								<tr>
									<td><?php echo $sr++; ?></td>
									<td><?php echo $single_doc['link']?></td>
									<td><?php echo $single_doc['document_type']?></td>
									<td><?php echo $single_doc['date_updated']; ?></td>
									<td><?php echo $action;?></td>
								</tr>							
							<?php
							}
						}
						?>
						</tbody>
					</table>
				</div>
			</div>	
		</div>
	</div>	
</div>

<!--===================================================-->
<!--END CONTENT CONTAINER-->
<script>

function delete_document(id,product_id)
{
	if( ! id)
	{
		Display_msg('Invalid document for deletion.','error');
		return false;
	}
	else
	{
		if(id > 0)
		{
			var ans = confirm('Are you sure you want to delete this document?');
			if(ans)
			{
				$.ajax(
				{
					url: "<?php echo base_url('auctioneer/salvage/delete_document'); ?>",
					type: "POST",
					dataType:"json",
					cache: false,
					data:{"doc_id":id,'product_id':product_id},
					success:function(response)
					{
						if(response.status == "success")
						{
							Display_msg(response.message,response.status);
							setTimeout(function()
							{
								window.location.reload();
							}, 3000);
						}
						else
						{
							Display_msg(response.message,response.status);
							return false;
						}
					}
				});
			}
		}
		else
		{
			Display_msg('Please Select Auction.','error');
			return false;
		}
	}
}


$(function(){
	var vRules = {
		doc_name:{required:true},
		upload_file:{required:true},
		document_type:{required:true}
	};
	var vMessages = {
		doc_name:{required:"<p class='text-danger'>Please select file.</p>"},
		upload_file:{required:"<p class='text-danger'>Please select file.</p>"},
		document_type:{required:"<p class='text-danger'>Please select type of document.</p>"}
	};

	$("#salvage_auction_other_doc").validate({
		rules: vRules,
		messages: vMessages,
		submitHandler: function(form){
			$(form).ajaxSubmit({
				url:"<?php echo base_url('auctioneer/salvage/upload_document/');?>",
				type: 'post',
				dataType:'json',
				cache: false,
				clearForm: true,
				success: function (response) {               
					if(response.status=="success")
					{
						Display_msg(response.message,response.status);
						setTimeout(function(){
							window.location.reload();
						}, 3000);
					}
					else
					{	
						Display_msg(response.message,response.status);
						return false;
					}
				}
			});
		}
	})
});


</script>