<style>
tr {
	page-break-inside: avoid;
}
thead {
	display: table-header-group;
}
tfoot {
	display: table-row-group;
}
tr {
	page-break-inside: avoid;
}
td.wrappable,table.data_table td.wrappable {
    white-space: normal;	
}            
table td,table th {
	font-size:10px;
	
	
}
</style>


<page backbottom='110px' backtop="0px" backleft="0px" backright="0px" style="font-size:10px;">
   <page_footer>
        <table style='width: 100%;'>
			<tr>
				<td align='left' style='float:left;width:200px;'>
				Page :  [[page_cu]]/[[page_nb]] <br/>
				<?php echo (new DateTime(null, new DateTimeZone($_SESSION['user_tz'])))->format('D dS \of M Y h:i:s A'); ?>
				
				</td>
				<td align='right' style='float:right;width:545px;'>
					
				</td>
			</tr>
		</table>
    </page_footer>
	<br>
	<table border='0.5' cellspacing='0' cellpadding='0' width='100%'>
		<tbody>
			<tr>
				<td style="width:550px;" >
								
				</td>
				<td align='center' style="width:180px;">
					<span style='font-size:20px;color:#2a5395;'><strong><?php echo PRODUCT ?></strong></span>
				</td>
			</tr>
			<tr>
				<td colspan='2' align='center' >
					<strong>BID LOG FOR AUCTION :  <?php echo $common_auction_details[0]['sale_no']; ?> </strong>
				</td>
			</tr>
			<tr>
				<td colspan='2' align='center' >
					<strong><?=strtoupper($common_auction_details[0]['vendor_name'])?></strong>
				</td>
			</tr>
			<tr>
				<td colspan='2' align='center' >
					<strong>SALVAGE AUCTION DETAILS</strong>
				</td>
			</tr>
			<tr>
				<td colspan='2' align='left'>
					<strong>Sale No :</strong><?php echo $common_auction_details[0]['product_id']?>
				</td>
			</tr>
			<tr>
				<td colspan='2' align='left'>
					<strong>Auction Type : </strong> Salvage Auction
				</td>
			</tr>
			<tr>
				<td colspan='2' align='left'>
					<strong>Auction Start Date : </strong><?php echo $common_auction_details[0]['start_date']?>
				</td>
			</tr>
			<tr>
				<td colspan='2' align='left'>
					<strong>Particulars : </strong><?php echo strip_tags($common_auction_details[0]['short_description'])?>
				</td>
			</tr>
		</tbody>		
	</table>
	<br>
	<table border='0.5' cellspacing='0' cellpadding='0' width='100%'>
			<tbody>	
				<tr>
					<td  align='center' class='wrappable' style="font-weight:bold;width:100px;" bgcolor="#e0e0e0"  > BIDDER NAME</td>
					<td align='center' class='wrappable' style="font-weight:bold;width:100px;" bgcolor="#e0e0e0" > DATE</td>
					<td align='center' class='wrappable' style="font-weight:bold;width:80px;" bgcolor="#e0e0e0" > TIME</td>
					<td align='center' class='wrappable' style="font-weight:bold;width:80px;" bgcolor="#e0e0e0" > AMOUNT</td>
					<td align='center' class='wrappable' style="font-weight:bold;width:250px;" bgcolor="#e0e0e0" > LOG</td>
					<td align='center' class='wrappable' style="font-weight:bold;width:80px;" bgcolor="#e0e0e0" > BIDDER IP</td>
				</tr>
				<?php
		
					if ( ! empty($get_lotwise_bid_log_report))
					{
						foreach ($get_lotwise_bid_log_report as $key=>$value)
						{
				?>
							<tr>
								<td style="text-align:center;"><?php echo $value['cname']?></td>
								<td style="text-align:center;"><?php echo date('Y-m-d',strtotime($value['bid_datetime']))?></td>
								<td style="text-align:center;"><?php echo date('H:i:s',strtotime($value['bid_datetime']))?></td>
								<td style="text-align:center;"><?php echo $value['bid_amt']?></td>
								<td style="text-align:center;"><?php echo $value['log_note']?></td>
								<td style="text-align:center;"><?php echo $value['bidder_ip']?></td>
							</tr>
				<?php
						}
					}
					else
					{
				?>
						<tr><td colspan="5">No Bid</td></tr>
				<?php
					}
				?>
			</tbody>


	</table>
	
				
</page>
