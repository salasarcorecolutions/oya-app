<style>
	 a.btn{
	 
	 text-align:left;
	 margin-top:5px;
	 width: 100%;
 }
.media-left{
	padding: 0px 10px;
}	
.middle input {
	position: absolute;
	    left: 11px;
	    top: 4px;

}
.media-left i{
	font-size: 26px;
}
.middle{
	position: relative;
	border: 3px solid #25476a;
    border-radius: 7px;
}
.text-15px{
	font-size: 15px;
}
</style>
<!--CONTENT CONTAINER-->
<!--===================================================-->
<div id="content-container">
	<!--Page Title-->
	<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
	<div id="page-head">
	<div id="page-title">
		<h1 class="page-header text-overflow">Salvage Auction Reports</h1>
	</div>
	<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
	<!--End page title-->


	<!--Breadcrumb-->
	<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
	<ol class="breadcrumb">
		<li><a href="admin.php">Home</a></li>
		<li><a class="active">Salvage Auction Reports</a></li>
	</ol>
</div>
	<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
	<!--End breadcrumb-->

	<!--Page content-->
	<!--===================================================-->
	<div id="page-content">
		<div class="float_btn">
			<a class="btn btn-primary btn-icon btn-circle add-tooltip" data-placement="top" data-toggle="tooltip" data-original-title="Back" href='<?php echo base_url('auctioneer/dashboard'); ?>' ><i class="fa fa-arrow-left" aria-hidden="true"></i></a>
		</div>
		<div class="row">
			<div class="col-sm-12">
		
			
				<!--Primary Panel-->
				<!--===================================================-->
				<div class="panel panel-primary">
					<div class="panel-heading">
						<h3 class="panel-title">Salvage Auction Reports</h3>
					</div>
					<!--Horizontal Form-->
					<!--===================================================-->
					
					<div class="panel-body" >
						<form method="post" name="frm" id="frm"   target="_blank" >
							<div class="col-lg-12">
								
									<div class="col-sm-3">
										<div class="form-group">
											<label class="control-label">Auction Id :</label>
											<select class="form-control" size="1" name="cboauctionid" id="cboauctionid" >
											<option value="" >--select--</option>
											<?php 
												if ( ! empty($get_all_salvage_auction))
												{
													foreach($get_all_salvage_auction as $single_auc)
													{
											?>
													<option value="<?php echo $single_auc['product_id']?>"><?php echo $single_auc['product_name'] .'  [' .date('Y-m-d' ,strtotime($single_auc['start_date'])).' ]  [ '.$single_auc['vendor_name'].' ] '.'['.$single_auc['sale_no'].']'?></option>
											
											<?php
													}
												}
											
											?>
											</select>
											
										</div>
									</div>
									<div class="col-sm-2">
										<div class="form-group">
											<a id='ckbCheckAll' class="btn btn-info btn-labeled fa fa-check-circle-o" >Select All</a>
										</div>
									</div>		
									<div class="col-sm-2">
										<div class="form-group">
											<a class="btn btn-success btn-labeled fa fa-file-excel-o" href="javascript:void(0);" value="Export In PDF" onclick="export_in_excel()">Export Excel</a>
										</div>
									</div>
									<div class="col-sm-2">
										<div class="form-group">
											<a class="btn btn-danger btn-labeled fa fa-file-pdf-o" href="javascript:void(0);" value="Export In PDF" onclick="export_in_pdf()">Export PDF</a>
										</div>
									</div>
								

								<input type='hidden' name="auctionid" id="auctionid" value="">
								<input type='hidden' name="clientid" id="clientid" value="">
							</div>
							<div id="checkboxlist">
								<div class="col-lg-12 normal_auction">
									<div class="row">
										<div class="form-group">
											
											<?php if ($this->common_model->check_permission('Salvage Paticipated Bidders Report')){ echo "
											<div class='col-lg-3' >
												<label for='client_wise_lot'>
													<div class='panel media middle pad-all'>
													<input type='checkbox' class='checked_lot'  id='client_wise_lot' name='client_wise_lot' value='client_wise_lot'>
															<div class='media-left'>
																<span class='icon-wrap icon-wrap-sm icon-circle bg-primary'>
																<i class='fa fa-file-text-o' aria-hidden='true'></i>
																</span>
															</div>
															<div class='media-body'>
																<p class='text-15px mar-no text-semibold text-dark'>Participated </p>
																<p class='text-primary mar-no'>Bidder Report</p>
															</div>
													</div>
													</label>
												</div>";
											}
											?>

											<?php if ($this->common_model->check_permission('Salvage Bid Log Report')){ echo "
												<div class='col-lg-3' >
													<label for='lotwise_bid_log'>
														<div class='panel media middle pad-all'>
														<input type='checkbox' class='checked_lot'  id='lotwise_bid_log' name='lotwise_bid_log' value='lotwise_bid_log'>
																<div class='media-left'>
																	<span class='icon-wrap icon-wrap-sm icon-circle bg-primary'>
																	<i class='fa fa-file-text-o' aria-hidden='true'></i>
																	</span>
																</div>
																<div class='media-body'>
																	<p class='text-15px mar-no text-semibold text-dark'>Bid Log</p>
																	<p class='text-primary mar-no'>Report</p>
																</div>
														</div>
													</label>
												</div>";
											}
											?>

								
											<?php if ($this->common_model->check_permission('Salvage H1 and Total Bids Report')){ echo "
												<div class='col-lg-3' >
													<label for='h1_n_total_bids'>
														<div class='panel media middle pad-all'>
														<input type='checkbox' class='checked_lot' id='h1_n_total_bids' name='h1_n_total_bids' value='h1_n_total_bids'>
																<div class='media-left'>
																	<span class='icon-wrap icon-wrap-sm icon-circle bg-primary'>
																	<i class='fa fa-file-text-o' aria-hidden='true'></i>
																	</span>
																</div>
																<div class='media-body'>
																	<p class='text-15px mar-no text-semibold text-dark'>H1 & Total Bids</p>
																	<p class='text-primary mar-no'>Report</p>
																</div>
														</div>
													</label>
												</div>";
											}
											?>


											<?php if ($this->common_model->check_permission('Salvage Bid Summary Report')){
											echo "
												<div class='col-lg-3' >
													<label for='bid_summary'>
														<div class='panel media middle pad-all'>
														<input type='checkbox' class='checked_lot' id='bid_summary' name='bid_summary' value='bid_summary'>
																<div class='media-left'>
																	<span class='icon-wrap icon-wrap-sm icon-circle bg-primary'>
																	<i class='fa fa-file-text-o' aria-hidden='true'></i>
																	</span>
																</div>
																<div class='media-body'>
																	<p class='text-15px mar-no text-semibold text-dark'>Bid Summary</p>
																	<p class='text-primary mar-no'>Report</p>
																</div>
														</div>
													</label>
												</div>";
											}
											?>

											<?php if ($this->common_model->check_permission('Salvage Product Details Report')){
											echo "
												<div class='col-lg-3' >
													<label for='product_details'>
														<div class='panel media middle pad-all'>
														<input type='checkbox' class='checked_lot'  id='product_details'name='product_details' value='bid_summary'>
																<div class='media-left'>
																	<span class='icon-wrap icon-wrap-sm icon-circle bg-primary'>
																	<i class='fa fa-file-text-o' aria-hidden='true'></i>
																	</span>
																</div>
																<div class='media-body'>
																	<p class='text-15px mar-no text-semibold text-dark'>Product Details</p>
																	<p class='text-primary mar-no'>Report</p>
																</div>
														</div>
													</label>
												</div>";
											}
											?>

											<?php if ($this->common_model->check_permission('Salvage Product On Auction Report')){
											echo "
												<div class='col-lg-3' >
													<label for='product_on_auction'>
														<div class='panel media middle pad-all'>
														<input type='checkbox' class='checked_lot' id='product_on_auction' name='product_on_auction' value='bid_summary'>
																<div class='media-left'>
																	<span class='icon-wrap icon-wrap-sm icon-circle bg-primary'>
																	<i class='fa fa-file-text-o' aria-hidden='true'></i>
																	</span>
																</div>
																<div class='media-body'>
																	<p class='text-15px mar-no text-semibold text-dark'>Product On </p>
																	<p class='text-primary mar-no'>Auction Report</p>
																</div>
														</div>
													</label>
												</div>";
											}
											?>
										</div>
									</div>
								</div>
							
								
							</div>
							<input type="hidden" name="save_file" id="save_file" value="0"/>
						</form>	
					</div>
						
					<!--===================================================-->
					<!--End Horizontal Form-->
				</div>
				<!--===================================================-->
				<!--End Primary Panel-->
		
			
		
			</div>
		
		</div>	
	</div>
	<!--===================================================-->
	<!--End page content-->


</div>
<!--===================================================-->
<!--END CONTENT CONTAINER-->	

<script type="text/javascript">

function export_in_pdf(){
	var theForm = $("#frm");
	if (document.getElementById("cboauctionid").selectedIndex==0){
		alert("Please select Auction");
		document.getElementById("cboauctionid").focus();
		return false;
	}
	var chkArray = [];
	var selected;
	/* look for all checkboes that have a parent id called 'checkboxlist' attached to it and check if it was checked */
	$(".checked_lot:checkbox:checked").each(function() {
		chkArray.push($(this).val());
		/* we join the array separated by the comma */
	});
		
	/* check if there is selected checkboxes, by default the length is 1 as it contains one single comma */
	if(chkArray.length > 0)
	{
		if (chkArray.length == 1){
			$('#save_file').val('0');
		} else {
			$('#save_file').val('1');
		}
		$(theForm).attr('action', '<?php echo base_url('auctioneer/salvage_auction_reports/get_salvage_reports_in_pdf')?>');
		$(theForm).submit();
	} else {
		alert("Please at least check one of the checkbox");
	}

}

function export_in_excel()
{
	var theForm = $("#frm");
	if(document.getElementById("cboauctionid").selectedIndex==0)
	{
		alert("Please select Auction");
		document.getElementById("cboauctionid").focus();
		return false;
	}
	var chkArray = [];

	/* look for all checkboes that have a parent id called 'checkboxlist' attached to it and check if it was checked */
	$("#checkboxlist input:checked").each(function() {
		chkArray.push($(this).val());
	});
	
	/* we join the array separated by the comma */
	var selected;
	selected = chkArray.join(',');
	
	/* check if there is selected checkboxes, by default the length is 1 as it contains one single comma */
	if(selected.length > 0){
		$(theForm).attr('action', '<?php echo base_url('auctioneer/salvage_auction_reports/get_salvage_reports_in_excel')?>');
		$(theForm).submit();
	}
	else
	{
		alert("Please at least check one of the checkbox");	
	}
	
}
var clicked = false;
$("#ckbCheckAll").on("click", function() {
  $(".checked_lot").prop("checked", !clicked);
  clicked = !clicked;
  this.innerHTML = clicked ? 'Deselect All' : 'Select All';

  if($('#ckbCheckAll').hasClass('btn-info')){
		$('#ckbCheckAll').removeClass('btn-info')
		$("#ckbCheckAll").addClass('btn-primary');
	}else{
		$('#ckbCheckAll').removeClass('btn-primary')
		$("#ckbCheckAll").addClass('btn-info');
	}
});

</script>


<style type="text/css">
#livesearch
  {
  margin:0px;
  width:200px;
  text-align: left;
  padding:5px;
  z-index:10;
  position:absolute;
  background:#fff;
  
  }
  #livesearch a{padding:2px;margin:1px;width:100%}
  #livesearch a:hover{background:#00FF00}
#txt1
  {
  margin:0px;
  }
</style>
