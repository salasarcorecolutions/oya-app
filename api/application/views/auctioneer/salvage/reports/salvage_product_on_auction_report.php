<style>
tr {
	page-break-inside: avoid;
}
thead {
	display: table-header-group;
}
tfoot {
	display: table-row-group;
}
tr {
	page-break-inside: avoid;
}
td.wrappable,table.data_table td.wrappable {
    white-space: normal;	
}            
table td,table th {
	font-size:10px;
	
	
}
</style>


<page backbottom='110px' backtop="0px" backleft="0px" backright="0px" style="font-size:10px;">
   <page_footer>
        <table style='width: 100%;'>
			<tr>
				<td align='left' style='float:left;width:200px;'>
				Page :  [[page_cu]]/[[page_nb]] <br/>
				<?php echo (new DateTime(null, new DateTimeZone($_SESSION['user_tz'])))->format('D dS \of M Y h:i:s A'); ?>
				
				</td>
				<td align='right' style='float:right;width:545px;'>
					
				</td>
			</tr>
		</table>
    </page_footer>
	<br>
	<table border='0.5' cellspacing='0' cellpadding='0' width='100%'>
		<tbody>
			<tr>
				<td style="width:550px;" >
								
				</td>
				<td align='center' style="width:180px;">
					<span style='font-size:20px;color:#2a5395;'><strong><?php echo PRODUCT ?></strong></span>
				</td>
			</tr>
			<tr>
				<td colspan='2' align='center' >
					<strong>PRODUCT ON AUCTION :  <?php echo $common_auction_details[0]['sale_no']; ?> </strong>
				</td>
			</tr>
			<tr>
				<td colspan='2' align='center' >
					<strong><?=strtoupper($common_auction_details[0]['vendor_name'])?></strong>
				</td>
			</tr>
			<tr>
				<td colspan='2' align='center' >
					<strong>SALVAGE AUCTION DETAILS</strong>
				</td>
			</tr>
			<tr>
				<td colspan='2' align='left'>
					<strong>Sale No :</strong><?php echo $common_auction_details[0]['product_id']?>
				</td>
			</tr>
			<tr>
				<td colspan='2' align='left'>
					<strong>Auction Type : </strong> Salvage Auction
				</td>
			</tr>
			<tr>
				<td colspan='2' align='left'>
					<strong>Auction Start Date : </strong><?php echo $common_auction_details[0]['start_date']?>
				</td>
			</tr>
			<tr>
				<td colspan='2' align='left'>
					<strong>Particulars : </strong><?php echo $common_auction_details[0]['short_description']?>
				</td>
			</tr>
		</tbody>		
	</table>
	<br>
	<?php if ( ! empty($get_product_list)) 
		{
	?>
	<table border='0.5' cellspacing='0' cellpadding='0' width='100%'>
		<tbody>
				<tr>
					<td align='center' class='wrappable' style="font-weight:bold;width:100px;">PRODUCT NAME</td>
					<td align='center' class='wrappable' style="font-weight:bold;width:100px;">SALE START DATETIME</td>
					<td align='center' class='wrappable' style="font-weight:bold;width:100px;">SALE END DATETIME</td>
					<td align='center' class='wrappable' style="font-weight:bold;width:100px;">RESULT DATETIME</td>
					<td></td>
				</tr>
				<tr>
					<td align='center' class='wrappable' style="font-weight:bold;width:100px;"><?php echo $get_product_list['product_name']?></td>
					<td align='center' class='wrappable' style="font-weight:bold;width:100px;"><?php echo $get_product_list['start_date'].' '.$get_product_list['start_time']?></td>
					<td align='center' class='wrappable' style="font-weight:bold;width:100px;"><?php echo $get_product_list['end_date'].' '.$get_product_list['end_time']?></td>
					<td align='center' class='wrappable' style="font-weight:bold;width:100px;"><?php echo $get_product_list['result_date']?></td>
					<td></td>
				</tr>
				<tr>
					<td colspan="5" style="height:10px">
					</td>
				</tr>
				<tr>
					<td align='center' class='wrappable' style="font-weight:bold;width:100px;">MODEL NO</td>
					<td align='center' class='wrappable' style="font-weight:bold;width:200px;">MANUFACTURER YEAR</td>
					<td align='center' class='wrappable' style="font-weight:bold;width:100px;">DIMESION</td>
					<td align='center' class='wrappable' style="font-weight:bold;width:100px;">SERIAL NO</td>
					<td align='center' class='wrappable' style="font-weight:bold;width:100px;">PRICE</td>
				</tr>
				<tr>
					<td align='center' class='wrappable' style="font-weight:bold;width:100px;"><?php echo $get_product_list['model_no']?></td>
					<td align='center' class='wrappable' style="font-weight:bold;width:200px;"><?php echo $get_product_list['manufacturer_year']?></td>
					<td align='center' class='wrappable' style="font-weight:bold;width:100px;"><?php echo $get_product_list['dimension']?></td>
					<td align='center' class='wrappable' style="font-weight:bold;width:100px;"><?php echo $get_product_list['serial_no']?></td>
					<td align='center' class='wrappable' style="font-weight:bold;width:100px;"><?php echo $get_product_list['price'].$get_product_list['price_currency']?></td>
				
				</tr> 
		</tbody>
	</table>
	<?php }?>
				
</page>
