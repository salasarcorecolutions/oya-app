
<!--CONTENT CONTAINER-->
<!--===================================================-->
<div id="content-container">
	<!--Page Title-->
	<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
	<div id="page-head">
	<div id="page-title">
		<h1 class="page-header text-overflow">Salvage Auction Images</h1>
	</div>
	<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
	<!--End page title-->


	<!--Breadcrumb-->
	<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
	<ol class="breadcrumb">
		<li><a href="<?php echo base_url('auction/index');?>">Home</a></li>
		<li class="active"><a href="<?php echo base_url('dashboard');?>">Dashboard</a></li>
		<li class="active">Salvage Auction Images</li>
	</ol>
</div>
	<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
	<!--End breadcrumb-->
	<div id="page-content">
		<div class="float_btn">
			<a class="btn btn-primary btn-icon btn-circle add-tooltip" data-placement="top" data-toggle="tooltip" data-original-title="Back" href='<?php echo base_url('auctioneer/salvage/index'); ?>' ><i class="fa fa-arrow-left" aria-hidden="true"></i></a>
		</div>
		<div class="panel">
			<div class="panel-heading">
				<h3 class="panel-title">Auction Images</h3>
			</div>
			
			<div class="panel-body">
				<form class="form-horizontal"  method="POST" id="upload_frm">
					<input type='hidden' id="product_id" name="product_id" value='<?php echo $product_id; ?>' />
					<div class="form-group">
                        <div class="col-sm-2">
                            <label class="label-control">Select File : </label>
                        </div>
                        <div class="col-sm-3">
                            <input id="imageFile" type="file" multiple="multiple" name="file[]" class="form-control">
                        </div>
						<div class="col-sm-3">
							<input class="btn btn-primary" type="submit" value='Submit' name="btn_upload_file_submit" id='btn_upload_file_submit'  />
						 </div>
                    </div>
                </form>
               
                
			</div>	
		</div>
        <!-- Auction images panel-->
        <div class="panel">
			<div class="panel-heading">
				<h3 class="panel-title">Uploaded Images</h3>
			</div>
			
			<div class="panel-body">
            <?php  if ( ! empty($uploaded_img[$product_id])):
            ?>
                 <div class="col-sm-12">
                <?php
                    $i=1;
					foreach ($uploaded_img[$product_id] as $id=>$singleimg):

                ?>
                    <div class="col-sm-3">
                        <div class="col-sm-2">
                            <input type="checkbox" name="img" value="<?php echo $id?>">
                        </div>
                        <div class="col-sm-6">
                            <a href="<?php echo AMAZON_BUCKET."/".s3BucketTestDir."images/auctionpic/salvage/".$singleimg['image_name']?>">Image <?php echo $i;?></a>
                        </div>
                    </div>
                <?php
                    $i++;
                    endforeach;
                ?>
                </div>
                <br/>
                <br/>
                <br/>
                <br/>
                <div class="col-sm-12">
                    <button class="btn btn-sm btn-danger" onclick="remove_img();">Remove Selected Image</button>
                </div>
            <?php 
             endif;
            ?>
            </div>
        </div>
	</div>
</div>

<!--===================================================-->
<!--END CONTENT CONTAINER-->
<script type="text/javascript">

function remove_img()
{
    var img = '';
	if( $('input[name="img"]:checked').length > 0)
	{
		$("input:checkbox[name='img']:checked").each(function(){
			img+=$(this).val()+',';
		});
       
        if (img)
        {
            $.ajax({
				url:"<?php echo base_url('auctioneer/salvage/delete_images/');?>",
				type: 'post',
				data:{'img':img},
				dataType:'json',
				success: function (response)
				{ 
					if (response.status=="success")
					{
						Display_msg(response.message,response.status);
						window.location.reload();                        
					}
					else
					{	
						Display_msg(response.message,response.status);
						return false;
					}

				}
            });
    	}
   }
   else
   {
       alert('kindly select image');
   }
   
}
$(function(){
	var vRules = {
		'file[]':{required:true}
	};
	var vMessages = {
		'file[]':{required:"<p class='text-danger'>Please select file.</p>"}
	};
	$("#upload_frm").validate({
		rules: vRules,
		messages: vMessages,
		submitHandler: function(form){
			$(form).ajaxSubmit({
				url:"<?php echo base_url('auctioneer/salvage/upload_auction_images/');?>",
				type: 'post',
				dataType:'json',
				cache: false,
				clearForm: true,
				success: function (response) {               
					if(response.status=="success")
					{
						Display_msg(response.message,response.status);
						
						setTimeout(function()
						{
							window.location.reload();
						}, 3000); 						
					}
					else
					{	
						Display_msg(response.message,response.status);
						return false;
					}
				}, 
				error :function()
				{
					Display_msg('Request Failed, Please Try again after some time');
				}
			});
		}
	});
})

</script>