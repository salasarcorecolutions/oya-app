	<!--===================================================-->
	<!-- END OF CONTAINER -->

	<!-- FOOTER -->
	<!--===================================================-->

	<footer id="footer">

		<!-- Visible when footer positions are static -->
		<!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->
		<div class="hide-fixed pull-right pad-rgt">&#0169; <?php echo date("Y");?> Oya Auction</div>
		<!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->
		<!-- Remove the class name "show-fixed" and "hide-fixed" to make the content always appears. -->
		<!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->
		<p class="pad-lft">&nbsp;</p>
	</footer>
	<!--===================================================-->
	<!-- END FOOTER -->

	<!-- SCROLL TOP BUTTON -->
	<!--===================================================-->
	<button id="scroll-top" class="btn"><i class="fa fa-chevron-up"></i></button>
	</div>

	<!-- SETTINGS - DEMO PURPOSE ONLY -->
	<!--===================================================-->

	<!--===================================================-->
	<!-- END SETTINGS -->
	<!--===================================================-->
	<!--JAVASCRIPT-->
	<!--=================================================-->
	<!-- moment.js for live date time -->
	<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.18.1/moment.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/moment-timezone/0.5.28/moment-timezone-with-data.min.js"></script>
	<!--Nifty Admin [ RECOMMENDED ]-->
	<script src="<?php echo base_url('auctioneer_assets/js/nifty.min.js'); ?>"></script>
	<!--DataTables [ OPTIONAL ] -->
	<script src="<?php echo base_url('auctioneer_assets/js/mdtimepicker.js'); ?>"></script>
	<script src="<?php echo base_url('auctioneer_assets/js/dhtmlwindow.js'); ?>"></script>
	<script src="<?php echo base_url('auctioneer_assets/js/nifty-demo.min.js'); ?>"></script>
	<script src="<?php echo base_url('auctioneer_assets/js/modal.js'); ?>"></script>
	<script type=”text/javascript” src=”//maxcdn.bootstrapcdn.com/bootstrap/3.3.1/js/bootstrap.min.js”></script>


	<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/js/bootstrap-datepicker.min.js" integrity="sha256-bqVeqGdJ7h/lYPq6xrPv/YGzMEb6dNxlfiTUHSgRCp8=" crossorigin="anonymous"></script>


	<script>

		$(document).ready(function(){
			$.ajax({
				url :'<?php echo base_url('auctioneer/dashboard/get_date')?>',
				type:'POST',
				dataType:'json',
				success:function(response)
				{
					if (response.active_till < response.date){
						destroySession();
					}
				}
			});
		})

		function destroySession()
		{
			$.ajax({
				url :'<?php echo base_url('auction/logout')?>',
				type:'POST',
				dataType:'json',
				success:function(response)
				{
					window.location('<?php echo base_url('login'); ?>');
				}
			});
		}

		function Display_msg(msg,msg_type)
		{
			if(msg_type=="success"){
				$.niftyNoty({
					type: 'success',
					icon : 'fa fa-check',
					message : msg,
					container : 'floating',
					timer : 10000
				});
			}else{
				$.niftyNoty({
					type: 'danger',
					icon : 'fa fa-minus',
					message : msg,
					container : 'floating',
					timer : 10000
				});
			}
		}
		
	</script>
	<?php if(isset($_SESSION['msg'])): ?>	
		<script>
			Display_msg("<?php echo $_SESSION['msg']; ?>","<?php echo $_SESSION['msg_type']; ?>");
		</script>		
		<?php unset($_SESSION['msg']); ?>
	<?php endif; ?>

		<script>
			 if($('table').hasClass('dataTables')){ 
				oTable = $('.dataTables').dataTable({
				dom: 'T<"clear">lfrtip',
				tableTools: {
					"sSwfPath": "swf/copy_csv_xls_pdf.swf"
				},
				responsive: true,
				sPaginationType: "full_numbers",
				iDisplayLength: 10,
				aoColumnDefs: [
				  { bSortable: false, aTargets: [ '_all' ] }
				],
				fnInitComplete: function (oSettings, json) {
				},
				fnDrawCallback: function (oSettings) {
					if (typeof datatablecomplete == 'function') {
						datatablecomplete("dataTables");
					}
					$(".DTTT_container > a").each(function () {
						$(this).addClass("btn btn-white");
					});
				}
			}); 
		
        }
		</script>
		<script>

		$('.datepickerclass').datepicker({
			format: 'yyyy-mm-dd',
			
		}).on('change', function(){		
		$(this).keyup();
		});


		</script>
		<script>
             $('.datetimepickerclass').timepicker({
                minuteStep: 1,
                appendWidgetTo: 'body',
                showSeconds: true,
                showMeridian: false,
                defaultTime: false,
				showInputs: true,
            });
        </script>

<script>
    // Left Menu Set Position
	var site_url = document.URL;	   
    var menuflag = true;
    $("#mainnav-menu").find("a").each(function () {
        if ($(this).attr('href') == site_url) {			
            $(this).parent().parent().parent().addClass("active");
            $(this).parent().parent().addClass("in");
            $(this).parent().addClass("active-link");
            menuflag = false;
        }
        if($(this).attr('href'))
        {
            $(this).click(function(){
                setCookie("page", $(this).attr('href'), 1);
            });
        }
    });
    if(menuflag){
        $("#mainnav-menu").find("a").each(function () {
            var page = getCookie("page");
            if (page == $(this).attr('href')) {
                $(this).parent().parent().parent().addClass("active");
                $(this).parent().parent().addClass("in");
                $(this).parent().addClass("active-link");
            }
        });
    }			
    function setCookie(cname, cvalue, exdays) {
        var d = new Date();
        d.setTime(d.getTime() + (exdays*24*60*60*1000));
        var expires = "expires="+d.toUTCString();
        document.cookie = cname + "=" + cvalue + "; " + expires;
    }
    function getCookie(cname) {
        var name = cname + "=";
        var ca = document.cookie.split(';');
        for(var i=0; i<ca.length; i++) {
            var c = ca[i];
            while (c.charAt(0)==' ') c = c.substring(1);
            if (c.indexOf(name) == 0) return c.substring(name.length,c.length);
        }
        return "";
    }
    $(function(){
        $(".loading").hide();
        // Show Loader
        $(document).ajaxSend(function(event, jqXHR, settings) {
            $("#loading").css('display','block');
        });
        $(document).ajaxStart(function(event, jqXHR, settings) {
            $("#loading").css('display','block');
        });

        // Hide Loader
        $(document).ajaxComplete(function(event, jqXHR, settings) {
            $("#loading").css('display','none');
        });
        $(document).ajaxError(function(event, jqXHR, settings) {
            $("#loading").css('display','none');
        });
        $(document).ajaxStop(function(event, jqXHR, settings) {
            $("#loading").css('display','none');
        });
        $(document).ajaxSuccess(function(event, jqXHR, settings) {
            $("#loading").css('display','none');
        });
    });

    $(document).ready(function()
    {
		var interval = setInterval(function() {
        var momentNow = moment().tz('<?php echo ! empty($_SESSION['user_tz']) ? trim($_SESSION['user_tz']) : 'UTC';?>');
        $('#date-part').html(momentNow.format('dddd') .substring(0,3).toUpperCase()+ ' '+ momentNow.format('DD MMMM YYYY')+' '+momentNow.format('hh:mm:ss A'));

    }, 1000);

        $(".add_select2").select2({
            placeholder:'Select Value'
        });
		$("#time_zone_list").select2({
			placeholder: "Select Time Zone",
			width: "100%"
		})
    });

	function showTimeZoneModal()
	{
		$("#time_zone_list").select2('data', null);
		$.ajax({
			url :'<?php echo base_url('auctioneer/sub_user/get_time_zones')?>',
			type:'POST',
			async: false,
			dataType:'json',
			success:function(response){
				var i;
				var options = '<option></option>';
				for (i = 0; i < response['result'].length; i++){
					options = options + "<option value='"+response['result'][i]['timezone']+"'>"+"("+response['result'][i]['gmt_offset']+")"+"	"+response['result'][i]['time_zone_description']+", "+response['result'][i]['country_name']+"</option>"
				}
				$("#time_zone_list").html(options);
				$('#time_zone_list').val(response.user_tz).trigger("change");
				$("#time_zone_modal").modal('show');
			},
			error: function(){
				alert("Problem in getting timezone please try again");
			}
		});
	}

	function updateUserTimeZone()
	{
		$.ajax({
			url :'<?php echo base_url('auctioneer/sub_user/update_time_zone')?>',
			type:'POST',
			data:{
				'user_tz': $("#time_zone_list").val()
			},
			dataType:'json',
			success:function(response){
				Display_msg(response.message, response.status);
			},
			complete: function(){
				$("#time_zone_modal").modal('hide');
			},
			error: function(){
				alert("Problem in changing timezone please try again");
			}
		});
	}

</script>

<div id="loading" class="loading" align="center" style="display:none;">
    <img src="<?php echo base_url('auctioneer_assets/images/loading1.gif'); ?>" />
</div>
</body>
</html>


