

<!--CONTENT CONTAINER-->
<!--===================================================-->
<div id="content-container">

<!--Page Title-->
<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
<div id="page-title">
	<h1 class="page-header text-overflow"><?php echo $pagetitle; ?></h1>
</div>
<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
<!--End page title-->

<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
<!--End breadcrumb-->
	<div id="page-content">
	<div class="panel panel-default panel-left">
	<div class="panel-body">
										<!--	<div class="row">
												<div class="col-sm-7">
					
													<!--Sender Information-->
													<!--<div class="media">
														<span class="media-left">
															<img src="" class="img-circle img-sm" alt="Profile Picture">
														</span>
														<div class="media-body">
															<div class="text-bold">Lisa D. Smith</div>
															<small class="text-muted">lisa.aqua@themeon.net</small>
														</div>
													</div>
												</div>
												<hr class="hr-sm visible-xs">
												<div class="col-sm-5 clearfix">-->
					
													<!--Details Information-->
													<!--<div class="pull-right text-right">
														<p class="mar-no"><small class="text-muted">Monday 23, Jan 2014</small></p>
														<a href="#">
															<strong>Holiday.zip</strong>
															<i class="fa fa-paperclip fa-lg fa-fw"></i>
														</a>
													</div>
												</div>
											</div>-->
											<!--<div class="row pad-ver">
												<div class="col-xs-7">-->
					
													<!--Mail toolbar-->
													
												<!--</div>
												<div class="col-xs-5 clearfix">
													<div class="pull-right">-->
					
														<!--Reply & forward buttons-->
														<!--<div class="btn-group btn-group">
															<a class="btn btn-default" href="#">
															<i class="fa fa-reply"></i>
															</a>
															<a class="btn btn-default" href="#">
															<i class="fa fa-share"></i>
															</a>
														</div>
													</div>
												</div>
											</div>-->
					
											<!--Message-->
											<!--===================================================-->
											<div class="col-md-12 col-lg-12">
					
					<!--Chat widget-->
					<!--===================================================-->
					<div class="panel">
						<!--Heading-->
						<div class="panel-heading">
							
							<h3 class="panel-title"><?php echo $msg_details['compname']?></h3>
						</div>
			
						<!--Widget body-->
						<div id="demo-chat-body" class="collapse in">
							<div class="nano" style="height:380px">
								<div class="nano-content pad-all">
									<ul class="list-unstyled media-block msg_list">
										
									</ul>
								</div>
							</div>
			
							
						</div>
					</div>
					<!--===================================================-->
					<!--Chat widget-->
				</div>
											<!--===================================================-->
											<!--End Message-->
					
											<!-- Attach Files-->
											<!--===================================================-->
											<div class="pad-ver">
												
												
											</div>
											<!--===================================================-->
											<!-- End Attach Files-->
					
											<div class="col-lg-12">
												<form name="compose_msg" id="compose_msg" class="form-group">
												  <textarea class="message_content form-control ckeditor" name="msg1" id="msg1"></textarea>
												  <span id="error"></span>
												 <input type="hidden" name="bidder_id" value="<?php echo $msg_details['bidder_id']?>"/>
												<input type="hidden" name="msg_id" value="<?php echo $msg_details['msg_id']?>"/>
												
												<span class=" btn btn-success btn-file">
													Browse... <input type="file" name="upload_file[]" id="upload_file" multiple/>
												</span>
												</form>
												<button  class="btn btn-primary pull-right" onclick="submit_msg()">
													<span class="fa fa-paper-plane"></span>
													Send Message
												</button>
												
                                              

											</div>
											<!--Quick reply : Summernote Placeholder -->
											
										</div>
	</div>
	</div>
</div>
<!--===================================================-->
<!--END CONTENT CONTAINER-->
<style type='text/css'>

</style>

<!-- Le javascript
================================================== --> 
<!-- Placed at the end of the document so the pages load faster --> 

<script src="https://cdn.ckeditor.com/4.14.0/standard/ckeditor.js"></script>


<script type='text/javascript'>

$(document).ready(function ()
{
	get_msg(<?php echo $msg_id?>);
    
});
function submit_msg()
{
	
	var msg = CKEDITOR.instances['msg1'].getData().replace(/<[^>]*>/gi, '').length;
    if (msg ==0) {
		$("#error").show();
		$("#error").html('<b>Please Enter Message</b>');
        return false;
	}else
	{
		$("#error").hide();
		$("#msg1").val(CKEDITOR.instances['msg1'].getData());
		var form = $("#compose_msg");
		form.validate({
			rules: {
				ignore:[],
				msg1:{ 
					required: function() 
                        {
                         CKEDITOR.instances.msg1.updateElement();
                        },

                     minlength:10
				}
				
			},
			messages: {
				
				msg1:"<b class='text-danger'>Please Enter Message</b>",
				
			}
		});


		if (form.valid()){
			$('#compose_msg').ajaxSubmit({
				url:"<?php echo base_url('auctioneer/messages/compose_thread');?>",
				type: 'post',
				dataType:'json',
				cache: false,
				clearForm: false,
				enctype: 'multipart/form-data',
				success: function (response)
				{
					if (response.status=="success")
					{
						$("#msg1").val('');
						CKEDITOR.instances['msg1'].setData('');
						get_msg('<?php echo $msg_id?>');
						
					}
					else
					{
						alert(response.msg);
					
					}
				}, 
				error: function()
				{
					alert('Please Try again later');
				}
			});
		
		}
	}
	
}
function get_msg(msg_id)
{
	
	$.ajax({
		url:"<?php echo base_url('auctioneer/messages/get_msg');?>",
		type: 'post',
		data:{'msg_id':msg_id},
		cache:false,
		clearForm:false,
		dataType: 'json',
		success: function (response)
		{
			var html ='';
			if (response)
			{
				var user_id = '<?php echo $this->session->userdata('vendor_id');?>';
				var html ='';

				$.each(response, function(i,v)
				{
					if(v['sender_id']!=user_id)
					{
						html+= '<li class="mar-btm">'+
									'<div class="media-left">'+
										'<img src="" class="img-circle img-sm" alt="Profile Picture">'+
									'</div>'+
									'<div class="media-body pad-hor">'+
										'<div class="speech">'+
											'<a href="#" class="media-heading">'+v['bidder_name']+'</a>'+
											    '<p>'+v['message_text']+'</p>'+
													'<p class="speech-time">'+
													'<i class="fa fa-clock-o fa-fw"></i>'+v['date_time']+
												'</p>'+
										'</div>'+
									'</div>';
									if (v['attachment'])
									{
										var attach = v['attachment'].split(",");
										var i = 1;
										$.each(attach, function(k,value)
										{
											var file = '<?php echo AMAZON_BUCKET.'/'.s3BucketTestDir?>message_attachment/'+value;
											html+='<div class="media-left"><h4><span class="attach" >'+'<i class="fa fa-paperclip fa-fw"></i><a href="'+file+'" target="_blank">File'+i+'</a></span></h4></div><br/>';
											i++;
										});
									}
								html+='</li>';
								
					}
					else
					{
						html+='<li class="mar-btm">'+
								'<div class="media-right">'+
									'<img src="" class="img-circle img-sm" alt="Profile Picture">'+
								'</div>'+
								'<div class="media-body pad-hor speech-right">'+
									'<div class="speech">'+
										'<a href="#" class="media-heading">'+v['bidder_name']+'</a>'+
										    '<p>'+v['message_text']+'</p>'+
												'<p class="speech-time">'+
													'<i class="fa fa-clock-o fa-fw"></i>'+v['date_time']+
												'</p>'+
									'</div>'+
								'</div>';
								if (v['attachment'])
								{
									var attach = v['attachment'].split(",");
									var i = 1;
									$.each(attach, function(k,value)
									{
										var file = '<?php echo AMAZON_BUCKET.'/'.s3BucketTestDir?>message_attachment/'+value;
										html+='<div class="speech-right"><h4><span class="attach" style="margin-right:210px;">'+'<i class="fa fa-paperclip fa-fw"></i><a href="'+file+'" target="_blank">File'+i+'</a></span></h4></div>';
										i++;
									});
								}
								
							html+='</li>';
							
						
					}
					
					
				});
				
			}
			else
			{
				html+='No Record Found';
			}
			$('.msg_list').html(html);	

		}
	});
}

</script>
