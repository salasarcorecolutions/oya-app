<?php
defined('BASEPATH') or exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<!-- Fav and touch icons -->
<link rel="apple-touch-icon-precomposed" sizes="144x144"
	href="<?php echo base_url(ciHtmlTheme.'/assets/ico/apple-touch-icon-144-precomposed.png')?>">
<link rel="apple-touch-icon-precomposed" sizes="114x114"
	href="<?php echo base_url(ciHtmlTheme.'/assets/ico/apple-touch-icon-114-precomposed.png')?>">
<link rel="apple-touch-icon-precomposed" sizes="72x72"
	href="<?php echo base_url(ciHtmlTheme.'/assets/ico/apple-touch-icon-72-precomposed.png')?>">
<link rel="apple-touch-icon-precomposed"
	href="<?php echo base_url(ciHtmlTheme.'/ico/apple-touch-icon-57-precomposed.png')?>">
<link rel="shortcut icon"
	href="<?php echo base_url(ciHtmlTheme.'/assets/ico/favicon.png')?>">
<title>Select an Account type : Oya Auction</title>
<!-- Bootstrap core CSS -->
<link
	href="<?php echo base_url(ciHtmlTheme.'/assets/bootstrap/css/bootstrap.min.css')?>"
	rel="stylesheet">
<!-- Custom styles for this template -->
<link href="<?php echo base_url(ciHtmlTheme.'/assets/css/style.css')?>"
	rel="stylesheet">
<link href="<?php echo base_url(ciHtmlTheme.'/assets/css/custom.css')?>"
	rel="stylesheet">
<!-- styles needed for carousel slider -->
<link
	href="<?php echo base_url(ciHtmlTheme.'/assets/css/owl.carousel.css')?>"
	rel="stylesheet">
<link
	href="<?php echo base_url(ciHtmlTheme.'/assets/css/owl.theme.css')?>"
	rel="stylesheet">

<script>
    paceOptions = {
      elements: true
    };
</script>
<script
	src="<?php echo base_url(ciHtmlTheme.'/assets/js/pace.min.js')?>"></script>
</head>
<style>

/**************************
********* Pricing Table CSS *
**************************/

.pricing_heading {
  margin-top: 120px
}

.pricing_heading h2, 
.pricing_heading p {
  color:#4e4e4e;
}

.pricing-area h1 {
  font-weight: 300;
  margin-top: 0;
  margin-bottom: 0px;
}

.pricing-area span {
  font-weight: 700;
  font-size: 18px;
}

.pricing-area h3, 
.pricing-area span  {
  color: #fff;
}

.plan {
  position: relative;
  margin-bottom: 70px;
}

.pricing-area ul {
  background:#FAFAFA;
  padding: 0;
  margin: 0;
}

.pricing-area ul li {
  list-style: none;
  padding: 15px 0;
  font-size: 16px;
  font-weight: 400;
}

.pricing-area ul li:nth-child(even){
  background:#f5f5f5
}

.pricing-area .price-three img, 
.pricing-area .price-six img {
  position: absolute;
  right: 15px;
  top: 0;
}

.pricing-area .heading-one, 
.pricing-area .heading-two, 
.pricing-area .heading-three {
  padding:17px 0;
  border-radius:2px 2px 0 0;
} 

.pricing-area .heading-one, 
.pricing-area .heading-two, 
.pricing-area .heading-three, 
.pricing-area .heading-four, 
.pricing-area .heading-five, 
.pricing-area .heading-six, 
.pricing-area .heading-seven {
  margin-left: 0px
}


.pricing-area .heading-one, 
.pricing-area .price-one .plan-action .btn-primary {
  background: #e24f43
}

.pricing-area .heading-two, 
.pricing-area .price-two .plan-action .btn-primary {
  background:#27AE60;
}

.pricing-area .heading-three,
.pricing-area .price-three .plan-action .btn-primary {
  background: #f39c12;
}

.pricing-area .heading-four, 
.pricing-area .price-four .plan-action .btn-primary {
  background: #5c5c5c
}

.pricing-area .heading-five, 
.pricing-area .price-five .plan-action .btn-primary {
  background: #5c5c5c
}

.pricing-area .heading-six, 
.pricing-area .price-six .plan-action .btn-primary {
  background: #e24f43
;
}

.pricing-area .heading-seven, 
.pricing-area .price-seven .plan-action .btn-primary {
  background: #5c5c5c
}

.pricing-area .bg{
 background: #fafafa 
}

.pricing-area .heading-one h3:before {
  border-color: #bf4539 transparent transparent;
}

.pricing-area .heading-two h3:before {
  border-color: #1f8c4d transparent transparent;
}

.pricing-area .heading-three h3:before {
  border-color: #d4880f transparent transparent;
}

.pricing-area .heading-four h3:before {
  border-color: #424242 transparent transparent;
}

.pricing-area .heading-five h3:before {
  border-color: #424242 transparent transparent;
}

.pricing-area .heading-six h3:before {
  border-color: #bf4539 transparent transparent;
}

.pricing-area .heading-seven h3:before {
  border-color: #424242 transparent transparent;
}

.pricing-area h3:before {
  border-color: #D4880F transparent transparent;
  border-radius: 5px 0 0 0px;
  border-style: solid;
  border-width: 12px;
  content: "";
  height: 0;
  left: 4px;
  position: absolute;
  top: 78px;
  width: 0;
  z-index: -999;
}

.small-pricing h3:after {
  height:94px;
}

.small-pricing h3:before {
  top: 74px;
}

.plan-action {
  height: 40px;
}

.pricing-area .plan-action .btn-primary {
  position: relative;
  padding:5px 20px;
  color: #fff
;
  margin-top: 5px;
}

.pricing-area .plan-action .btn-primary:before {
  border-color: #E24F43 transparent transparent;
  border-radius: 5px 0 0 0;
  border-style: solid;
  border-width: 12px;
  bottom: -12px;
  content: "";
  height: 0;
  left: -9px;
  position: absolute;
  width: 0;
  z-index: -1;
}

.pricing-area .plan-action .btn-primary:after {
  border-color: #E24F43 transparent transparent;
  border-radius: 5px 0 0 0;
  border-style: solid;
  border-width: 12px;
  bottom: -12px;
  content: "";
  height: 0;
  position: absolute;
  right: -9px;
  width: 0;
  z-index: -1;
}

.pricing-area .price-two .plan-action .btn-primary:before, 
.pricing-area .price-two .plan-action .btn-primary:after {
  border-color: #1f8c4d transparent transparent;
}

.pricing-area .price-three .plan-action .btn-primary:before, 
.pricing-area .price-three .plan-action .btn-primary:after {
  border-color: #d4880f transparent transparent;
}

.pricing-area .price-four .plan-action .btn-primary:before, 
.pricing-area .price-four .plan-action .btn-primary:after, 
.pricing-area .price-five .plan-action .btn-primary:before, 
.pricing-area .price-five .plan-action .btn-primary:after, 
.pricing-area .price-seven .plan-action .btn-primary:before, 
.pricing-area .price-seven .plan-action .btn-primary:after  {
  border-color: #424242 transparent transparent;
  right: -9px;
}

.pricing-area .price-six .plan-action .btn-primary:before, 
.pricing-area .price-six .plan-action .btn-primary:after {
  border-color: #bf4539 transparent transparent;
  right: -9px;
}

</style>
<body>

	<div id="wrapper">
		<!-- /.header -->
 <?php $this->load->view(ciHtmlTheme.'/includes/header');?>
  
  <div class="main-container">
			<div class="container">
				<div class="pricing-area text-center">
                <div class="row">
                    <div class="col-sm-6 plan price-one wow fadeInDown animated" style="visibility: visible; animation-name: fadeInDown;">
                        <ul>
                            <li class="heading-one">
                                <h1>Buyer</h1>
                                <span>FREE/Month*</span>
                                <p>For Buyer to participate in auctions worldwide </p>
                            </li>
                           <li>Forward Auctions</li>
                           <li>Reverse Auctions</li>
                           <li>Dutch Auctions</li>
                           <li>e-Tendering Auctions</li>
                           <li>24/7 Support</li>
                            <li class="plan-action">
                                <a href="<?php echo base_url('user/buyer_reg')?>" class="btn btn-primary">Sign Up As Buyer</a>
                            </li>
                        </ul>
                    </div>

                    <div class="col-sm-6 plan price-two wow fadeInDown animated" style="visibility: visible; animation-name: fadeInDown;">
                        <ul>
                            <li class="heading-two">
                                <h1>Auctioneer/Vendor</h1>
                                <span>FREE/Month</span>
                                <p>Looking for a buyer, Sale your products</p>
                            </li>
                            <li>Forward Auctions</li>
                           <li>Reverse Auctions</li>
                           <li>Dutch Auctions</li>
                           <li>e-Tendering Auctions</li>
                           <li>24/7 Support</li>
                            <li class="plan-action">
                                <a href="<?php echo base_url('user/auctioneer_reg')?>" class="btn btn-primary">Sign Up Auctioneer</a>
                            </li>
                        </ul>
                    </div>

                   

                    
                </div>
            </div>
				


			</div>
		</div>
		<!-- /.main-container -->


		<!-- /.claa me  -->
	<?php $this->load->view(ciHtmlTheme.'/includes/call_back');?>
  <!-- /.footer --> 
  <?php $this->load->view(ciHtmlTheme.'/includes/footer');?>
 
</div>
	<!-- /.wrapper -->

	<!-- Le javascript
================================================== -->
	<!-- Placed at the end of the document so the pages load faster -->
	<script
		src="<?php echo base_url(ciHtmlTheme.'/assets/js/jquery.min.js')?>"> </script>
	<script
		src="<?php echo base_url(ciHtmlTheme.'/assets/bootstrap/js/bootstrap.min.js')?>"></script>
	<!-- include carousel slider plugin  -->
	<script
		src="<?php echo base_url(ciHtmlTheme.'/assets/js/owl.carousel.min.js')?>"></script>

	<!-- include equal height plugin  -->
	<script
		src="<?php echo base_url(ciHtmlTheme.'/assets/js/jquery.matchHeight-min.js')?>"></script>
	<!-- include jquery list shorting plugin plugin  -->
	<script
		src="<?php echo base_url(ciHtmlTheme.'/assets/js/hideMaxListItem.js')?>"></script>
	<!-- include jquery.fs plugin for custom scroller and selecter  -->
	<script
		src="<?php echo base_url(ciHtmlTheme.'/assets/plugins/jquery.fs.scroller/jquery.fs.scroller.js')?>"></script>
	<script
		src="<?php echo base_url(ciHtmlTheme.'/assets/plugins/jquery.fs.selecter/jquery.fs.selecter.js')?>"></script>
	<!-- include custom script for site  -->
	<script
		src="<?php echo base_url(ciHtmlTheme.'/assets/js/jquery.validate.min.js')?>"></script>
	<script src="<?php echo base_url(ciHtmlTheme.'/assets/js/script.js')?>"></script>
	<script>    
$("#btnLogin").click(function(e){
	 e.preventDefault();
	var form=$("#frmLogin");
	form.validate({
	    ignore:[],
		rules: {
			username: {required : true},
			password: {required : true},
			login_type: {required : true},
			
			},
		messages: {	
			username:"Please Enter User name"
			},
			
		});

	if(form.valid())
	 	$( "#frmLogin" ).submit();
	

	
});
$('#frmLogin').on('submit',(function(e) {
	var $btn = $('#btnLogin').button('loading');
	 e.preventDefault();
		alert('');

	 
		$.ajax({
			url: '<?= base_url();?>user/check_login',
			type:"POST",
			data: new FormData(this),
			contentType: false,
			cache: false,
			processData:false,
			dataType: 'json',
			success: function (resObj) {
				if (resObj.status) 
				{
					
						window.location = resObj.redirect_url;
					
					
				}
				else{
					$("#msg").html('<div class="alert alert-warning alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button> <strong>Warning! Invalid Username or Password<i class="fa fa-warning"></i></div>');
					 $btn.button('reset')
				}	
			},
			error: function(){
				$btn.button('reset')
			}
			});
	}));		
 </script>
</body>
</html>
