<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<!-- Fav and touch icons -->
<link rel="apple-touch-icon-precomposed" sizes="144x144" href="<?php echo base_url(ciHtmlTheme.'/assets/ico/apple-touch-icon-144.png')?>">
<link rel="apple-touch-icon-precomposed" sizes="114x114" href="<?php echo base_url(ciHtmlTheme.'/assets/ico/apple-touch-icon-114.png')?>">
<link rel="apple-touch-icon-precomposed" sizes="72x72" href="<?php echo base_url(ciHtmlTheme.'/assets/ico/apple-touch-icon-72.png')?>">
<link rel="apple-touch-icon-precomposed" sizes="57x57" href="<?php echo base_url(ciHtmlTheme.'/assets/ico/apple-touch-icon-57.png')?>">
<link rel="shortcut icon" href="<?php echo base_url(ciHtmlTheme.'/assets/ico/favicon.png')?>">
<title>Product Details</title>
<!-- Bootstrap core CSS -->
<link href="<?php echo base_url(ciHtmlTheme.'/assets/bootstrap/css/bootstrap.min.css')?>" rel="stylesheet">
<!-- Custom styles for this template -->
<link href="<?php echo base_url(ciHtmlTheme.'/assets/css/style.css')?>" rel="stylesheet">
<link href="<?php echo base_url(ciHtmlTheme.'/assets/css/custom.css')?>" rel="stylesheet">
<!-- styles needed for carousel slider -->
<link href="<?php echo base_url(ciHtmlTheme.'/assets/css/owl.carousel.css')?>" rel="stylesheet">
<link href="<?php echo base_url(ciHtmlTheme.'/assets/css/owl.theme.css')?>" rel="stylesheet">
<link href="<?php echo base_url(ciHtmlTheme.'/assets/plugins/bxslider/jquery.bxslider.css')?>" rel="stylesheet">
<link href="<?php echo base_url(ciHtmlTheme.'/assets/css/mdtimepicker.css'); ?>" rel="stylesheet" />



<script>
    paceOptions = {
      elements: true
    };
</script>
</head>
<body>

<div id="wrapper">
<!-- /.header -->
 <?php $this->load->view(ciHtmlTheme.'/includes/header');?>
  <!-- /.header -->
  
  <!-- main container -->
  
  
  <div class="main-container">
    <div class="container">
      <ol class="breadcrumb pull-left">
        <li><a href="<?php echo base_url()?>"><i class="icon-home fa"></i>Home</a></li>
        <li><a href="<?php echo base_url('products')?>">All Products</a></li>
        <li><a href="<?php echo base_url('products/index/'.$product['category_name'])?>"><?php echo $product['category_name']?></a></li>
        <li class="active"><?php echo $product['product_name']?></li>
      </ol>
      <div class="pull-right backtolist"><a href="<?php echo base_url('products')?>"> <i class="fa fa-angle-double-left"></i> Back to Results</a></div>
    </div>
    <div class="container">
      <div class="row">
        <div class="col-sm-9 page-content col-thin-right">
          <div class="inner inner-box ads-details-wrapper">
            <h2> <?php echo $product['product_name']?>  </h2>
            <span class="info-row"> <span class="date"><i class=" icon-clock"> </i> <?php echo $product['start_date'].' '.$product['start_time']?> </span> <span> <b>To</b> </span><span class="date"><i class=" icon-clock"> </i> <?php echo $product['end_date'].' '.$product['end_time']?> </span> - <span class="category"><?php echo $product['category_name']?> </span>- <span class="item-location"><i class="fa fa-map-marker"></i> <?php echo $product['country_name']?> </span> </span>
              <div class="ads-image">
                <h1 class="pricetag"> <?php echo $product['price'].' '.$product['price_currency']?></h1>
                <ul class="bxslider">
                  <?php 
                  if ( ! empty($product_images))
                  {
                      foreach($product_images as $k=>$img)
                      {
                  ?>
                          <li><img src="<?php echo AMAZON_BUCKET?>/<?php echo s3BucketTestDir?>images/auctionpic/salvage/<?php echo $img['image_name']; ?>" alt="<?php echo $product['product_name']?>" /></li>
                  <?php
                      }  
                  }
                  ?>
                </ul>
                <div id="bx-pager">
                <?php 
                  if ( ! empty($product_images))
                  {
                      foreach($product_images as $k=>$img)
                      {
                  ?>
                        <a class="thumb-item-link" data-slide-index="0" href=""><img src="<?php echo AMAZON_BUCKET?>/<?php echo s3BucketTestDir?>images/auctionpic/salvage/<?php echo $img['image_name']; ?>" alt="<?php echo $product['product_name']?>" /></a>
                  <?php
                      }  
                  }
                  ?>
                  
              </div>
          </div>
            <!--ads-image-->
            
            <div class="Ads-Details">
              <h5 class="list-title"><strong>Products Detsils</strong></h5>
              <div class="row">
                <div class="ads-details-info col-md-8">
                  <p><?php echo $product['short_description']?></p>
                  <h4>Technical Condition</h4>
                  <ul class="list-circle">
                  
                    
                   
                    <li><?php echo 'Open Date Time : '.$product['start_date']." ".$product['start_time']." TO ".$product['end_date']." ".$product['end_time']; ?></li>
                    <?php 
                      if ( ! empty($product['dimension']))
                      {
                      ?>
                          <li><?php echo 'Dimension : '.$product['dimension']; ?></li>
                      <?php
                      }
                    ?>
                     <?php 
                      if ( ! empty($product['model_no']))
                      {
                      ?>
                          <li><?php echo 'Model No : '.$product['model_no']; ?></li>
                      <?php
                      }
                    ?>
                     <?php 
                      if ( ! empty($product['serial_no']))
                      {
                      ?>
                          <li><?php echo 'Serial No : '.$product['serial_no']; ?></li>
                      <?php
                      }
                    ?>
                    <li><?php echo ! empty($product['manufacturer_name']) ? 'Manufacturer Name : '.ucwords($product['manufacturer_name']) : 'Manufacturer Name :'.ucwords($product['manufacturer_other']); ?> </li>
                    <li><?php echo ! empty($product['manufacturer_year']) ? 'Manufacture Year : '.$product['manufacturer_year']:''; ?> </li>
                  </ul>
                  <?php 
                    if ( ! empty($doc))
                    {
                    ?>
                     <h4>Product Documents</h4>
                     <ul class="list-circle">
                    <?php
                        foreach($doc as $k=>$single_doc)
                        {
                    ?>
                           
                          <li><?php echo $single_doc['doc_name']?> : <a href="<?php echo $single_doc['doc_path']?>">Click To View</a></li>
                            
                    <?php
                        }
                    ?>
                      </ul>
                    <?php
                    }
                  ?>
                  <?php 
                    if ( ! empty($custom_sections))
                    {
                        foreach($custom_sections as $k=>$section)
                        {
                    ?>
                            <h4><?php echo $section['section_name']?></h4>
                            <ul>
                                <li><?php echo $section['section_description']?></li>
                            </ul>
                    <?php
                        }
                    }
                  ?>
                  
                  
                </div>
                <div class="col-md-4">
                  <aside class="panel panel-body panel-details">
                    <ul>
                      <li>
                        <p class=" no-margin "><strong>Price : </strong> <?php echo $product['price'].''.$product['price_currency']?></p>
                      </li>
                      <li>
                        <p class="no-margin"><strong>Type : </strong> <?php echo $product['category_name']?></p>
                      </li>
                      <li>
                      <?php 
                        $state_name = ! empty($product['state_name']) ? 'State Name : '.$product['state_name'] : 'State Name : '.$product['state_other']; 
                        $city_name = ! empty($product['city_name']) ? 'City Name : '.$product['city_name'] : 'State Name : '.$product['city_other']; 
                        ?>
                        <p class="no-margin"><strong>Location : </strong> <?php echo $product['country_name'].$state_name.", ".$city_name;?></p>
                      </li>
                      <li>
                        <p class=" no-margin "><strong>Buying Type : </strong> <?php echo $product['buying_format'];?></p>
                      </li>
                      <li>
                        <p class="no-margin"><strong>Product Name : </strong> <?php echo $product['product_name']?></p>
                      </li>
                    </ul>
                  </aside>
                  <div class="ads-action">
                    <ul class="list-border">

                      <?php if (empty($fav_product))
                      {
                      ?>
                         <li><a  href="#" class="fav" onclick="add_favourite(<?php echo $product['product_id']?>);"> <i class=" fa fa-heart"></i> Add In Favourite </a> </li>

                      <?php
                      }
                      else
                      {
                      ?>
                          <li><a  href="#" class="fav" onclick="add_favourite(<?php echo $product['product_id']?>);"> <i class=" fa fa-heart"></i> Added In Favourite </a> </li>

                      <?php
                      }
                      ?>
                    </ul>
                  </div>
                </div>
              </div>
              <div class="content-footer text-left"> <a class="btn  btn-default" onclick="msg_popup()" ><i class=" icon-mail-2"></i> Send a message </a>  </div>
            </div>
        </div>
          <!--/.ads-details-wrapper--> 
          
      </div>
        <!--/.page-content-->
        
      <div class="col-sm-3  page-sidebar-right">
        <aside>
          <div class="panel sidebar-panel panel-contact-seller">
            <div class="panel-heading" style="color:black"><i class=" fa fa-heart"></i> Favourite Product</div>
            <div class="panel-content user-info">
              <div class="panel-body text-center">
                <div class="seller-info">
                  <h3 class="no-margin"></h3>
                  <p>Location: <strong><?php echo $product['country_name']?></strong></p>
                </div>

                <div class="user-ads-action"> 
                    <?php
                      if ( empty($fav_product))
                      {
                    ?>
                      <a href="#" onclick="add_favourite(<?php echo $product['product_id']?>);" class="btn btn-info btn-block fav"><i class=" fa fa-heart"></i> Add In Favourite </a> 
                    <?php

                      }
                      else
                      {
                      ?>
                        <a href="#"  class="btn   btn-default btn-block fav"><i class=" fa fa-heart"></i> Added In Favourite</a> 
                      <?php
                      }
                    ?>
                </div>
              </div>
            </div>
          </div> 
          <?php 
          if ($product['buying_format'] == 'Live Auction' && ! empty($this->session->userdata('bidder_id')))
          {
          ?>

          <div class="panel sidebar-panel">
            <div class="panel-heading" style="color:black">Quote Price</div>
              <div class="panel-content">
                <div class="panel-body text-left">
             
                  <?php if ($product['buying_format'] == 'Live Auction' &&  ( $product_permission['show_bidder_name'] =='y' || $product_permission['show_bidder_rate']=='y' || $product_permission['show_rank']=='y')) :

                      if ( ! empty($bidder_details) &&  is_array($allow_bidder)):
                        
                      ?>

                              <h3>Bidder Details</h3>
                              <table class="table table-responsive table-bordered">
                              <th>
                                  Bidder Name
                              </th>
                              <th>
                                  Bidder Rate
                              </th>
                              <th>
                                  Bidder Rank
                              </th>
                              <tbody>
                                  <?php 
                                      $i =0;
                                          foreach($bidder_details as $key=>$value):
                                            $i++;
                                            $flag =false;
                                            $my_bid = false;
                                            if($this->session->userdata('bidder_id') == $value['bidder_id'])
                                            {
                                                $flag = true;
                                               
                                            }
                                            else
                                            {
                                                if ($product_permission['show_other_bidder'] === 'n')
                                                {
                                                  $flag =false;
                                                  $my_bid = false;
                                                }
                                                else
                                                {
                                                  $flag =true;
                                                  $my_bid = true;
                                                }
                                            }
                                          if ($flag)
                                          {
                                  ?>
                                          <tr>

                                              <?php if ($product_permission['show_bidder_name'] =='y' || $my_bid ===true)
                                              {
                                              ?>
                                                      <td><?php echo $value['compname']?></td>
                                              <?php 
                                              }
                                              else
                                              {
                                              ?>
                                                      <td><?php echo '*******';?></td>

                                              <?php
                                              }
                                              ?>
                                              <?php if ($product_permission['show_bidder_rate'] =='y' || $my_bid ===true)
                                              {
                                              ?>
                                                      <td><?php echo $value['amount']?></td>
                                              <?php 
                                              }
                                              else
                                              {
                                              ?>
                                                      <td><?php echo '*******';?></td>

                                              <?php
                                              }
                                              ?>
                                              <?php if ($product_permission['show_rank'] =='y' || $my_bid ===true)
                                              {
                                              ?>
                                                      <td><?php echo 'H'.$i;?></td>
                                              <?php 
                                              }
                                              else
                                              {
                                              ?>
                                                      <td><?php echo '*******';?></td>

                                              <?php
                                              }
                                              ?>

                                          </tr>
                                          
                                  <?php 
                                          }
                                          else
                                          {
                                              if ( empty ($bidder_details))
                                              {
                                                
                                              
                                          ?>
                                            <tr><td colspan="3" align='center'> No Bid</td></tr>
                                          <?php
                                              }
                                          }
                                         
                                          endforeach;

                                      ?>
                              </tbody>
                              </table>
                      <?php 
                      endif;
                      endif; ?>
                      <?php 
                if ( ! empty($current_auction))
                {
                   if ($product['is_public'] =='Y' )
                    {
                  ?>
                      <div style="margin-top:2%" class="col-sm-12">
                        <button onclick="placeMyBid()" style="padding: 1%" type="button" class="btn btn-success form-control">Place a Quote</button>
                      </div>
                  <?php 
                    }
                    else
                    {
                        

                    ?>
                        

                    <?php
                  
                        if ( ! empty($allow_bidder) && is_array($allow_bidder))
                        {
                          foreach ($allow_bidder as $bid)
                          {
                           
                            if ($bid['bidder_id'] == $this->session->userdata('bidder_id') && $bid['is_accepted'] == 'N')
                            {
                            ?>
                              <div style="margin-top:2%" class="col-lg-6">
                                <button  style="padding: 1%;width:167px" type="button" onclick="send_participation_request('<?php echo $product_id?>');" class="btn blue_btn form-control">Request For Participation</button>
                              </div>
                            <?php
                              echo '<div class="col-sm-12"><span class="text-danger">Your Request has been pending from vendor</span></div>';
                            }
                            else if ($bid['bidder_id'] == $this->session->userdata('bidder_id') && $bid['is_accepted'] == 'Y')
                            {
                            ?>
                              <div style="margin-top:2%" class="col-sm-12">
                                <button onclick="placeMyBid()" style="padding: 1%" type="button" class="btn btn-success form-control">Place a Quote</button>
                              </div>
                            <?php
                            }
                            else
                            {
                            ?>
                              <div style="margin-top:2%" class="col-lg-6">
                                <button  style="padding: 1%;width:167px" type="button" onclick="send_participation_request('<?php echo $product_id?>');" class="btn blue_btn form-control">Request For Participation</button>
                              </div>
                            <?php
                            }
                           
                          }
                        }
                        else
                        {
                          ?>
                              <div style="margin-top:2%" class="col-lg-6">
                                <button  style="padding: 1%;width:167px" type="button" onclick="send_participation_request('<?php echo $product_id?>');" class="btn blue_btn form-control">Request For Participation</button>
                              </div>
                          <?php
                        }
                        
                    }
                }
              ?>
              </div>
              </div>
          </div>
          <?php 
          }
          else
          {
            ?>
            <div class="panel sidebar-panel">
            <div class="panel-heading" style="color:black">Quote Price</div>
              <div class="panel-content">
              <div class="panel-body text-left">
                    <div style="margin-top:2%" class="col-sm-12">
                      <button onclick="placeMyBid()" style="padding: 1%" type="button" class="btn btn-success form-control">Place a Quote</button>
                    </div>
              </div>
              </div>
          </div>
          <?php
          }
          ?>
          <!--/.categories-list--> 
        </aside>
      </div>
        <!--/.page-side-bar--> 
    </div>
  </div>
  <!-- /.main-container -->
  
  
  
	<?php $this->load->view(ciHtmlTheme.'/includes/call_back');?>
  <!-- /.footer --> 
  <?php $this->load->view(ciHtmlTheme.'/includes/footer');?>
 
</div>
</body>
</html>
<!-- /.wrapper --> 
<!-- Modal -->
<div class="modal fade biddingModal" id="biddingModal" role="dialog">
		<div style="display:flex; align-items:center;">
			<div  class="modal-dialog">
					<!-- Modal content-->
					<form name="bid_now_frm" id="bid_now_frm">
            <div class="modal-content">
                <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                  <h4 class="modal-title">Place Your Bid</h4>
                </div>
                <div class="modal-body">
                  <div class="row">
                      <div class="col-md-6">
                          <label>Bid Amount</label>
                          <input class='form-control' type="number" name="my_bid" placeholder="Enter Amount" /> 
                      </div>
                      <?php if (! empty($last_bid)):?>
                      <div class="col-md-6">
                        <label>Last Bid</label>
                        <input id="last_bid" class='form-control' type="number" readonly  value="<?php echo $last_bid;?>" /> 
                      </div>
                  </div>  
                  <?php endif; ?>
                </div>
                <input type="hidden" name="product_id" id="product_id" value="<?php echo $product_id;?>"/>
                <input type="hidden" name="end_date" id="end_date" value="<?php echo $product['end_date'];?>"/>

                <div class="modal-footer">
                  <?php if ( ! empty($this->session->userdata('bidder_id'))):?>
                      <button type="submit" class="btn btn-success" id="btnSave" onclick="submit_bid()" >Save</button>
                    <?php endif; ?>
                  <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>

                </div>
            </div>
					</form>
        </div>
    </div>
  </div>
</div>
      <!-- The Modal -->
      
<!-- Modal contactAdvertiser -->

<div class="modal" id="contactAdvertiser"   role="dialog">

  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title"><i class=" icon-mail-2"></i> Contact advertiser </h4>
      </div>
      <form role="form" id="send_message" name="send_message" method="POST">
      <div class="modal-body">
        
          <div class="form-group">
            <label for="recipient-name" class="control-label">Name:</label>
            <input class="form-control required" id="recipient-name" name="name" placeholder="Your name" data-placement="top" data-trigger="manual" data-content="Must be at least 3 characters long, and must only contain letters." type="text">
          </div>
          <div class="form-group">
            <label for="sender-email" class="control-label">E-mail:</label>
            <input id="sender-email" name="email" type="text" data-content="Must be a valid e-mail address (user@gmail.com)" data-trigger="manual" data-placement="top" placeholder="email@you.com" class="form-control email">
          </div>
          <div class="form-group">
            <label for="recipient-Phone-Number"  class="control-label">Phone Number:</label>
            <input type="text"  maxlength="60" class="form-control" id="recipient-Phone-Number" name="phone">
          </div>
          <div class="form-group">
            <label for="message-text" class="control-label">Message <span class="text-count">(300) </span>:</label>
            <textarea class="form-control" id="message-text" name="message"  placeholder="Your message here.." data-placement="top" data-trigger="manual"></textarea>
          </div>
          <div class="form-group">
            <p class="help-block pull-left text-danger hide" id="form-error">&nbsp; The form is not valid. </p>
          </div>
          <input type="hidden" name="product_id" id="product_id" value="<?php echo $product_id?>"/>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
        <button type="submit" class="btn btn-success pull-right" onclick="send_msg()">Send message!</button>
      </div>
      </form>
    </div>
  </div>
</div>

<!-- /.modal -->
<!-- Le javascript
================================================== --> 
<!-- Placed at the end of the document so the pages load faster --> 


<!-- bxSlider Javascript file --> 
<!-- Placed at the end of the document so the pages load faster --> 
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.10.1/jquery.min.js"> </script>
<script src="<?php echo base_url(ciHtmlTheme.'/assets/bootstrap/js/bootstrap.min.js')?>"></script>

<!-- include carousel slider plugin  --> 
<script src="<?php echo base_url(ciHtmlTheme.'/assets/js/owl.carousel.min.js')?>"></script> 

<!-- include equal height plugin  --> 
<script src="<?php echo base_url(ciHtmlTheme.'/assets/js/jquery.matchHeight-min.js')?>"></script> 

<!-- include jquery list shorting plugin plugin  --> 
<script src="<?php echo base_url(ciHtmlTheme.'/assets/js/hideMaxListItem.js')?>"></script> 

<!-- bxSlider Javascript file --> 
<script src="<?php echo base_url(ciHtmlTheme.'/assets/plugins/jquery.fs.scroller/jquery.fs.scroller.js')?>"></script>
<script src="<?php echo base_url(ciHtmlTheme.'/assets/plugins/jquery.fs.selecter/jquery.fs.selecter.js')?>"></script>

<script src="<?php echo base_url(ciHtmlTheme.'/assets/plugins/bxslider/jquery.bxslider.min.js')?>"></script> 
<script src="<?php echo base_url(ciHtmlTheme.'/assets/js/script.js')?>"></script>

<script src="<?php echo base_url(ciHtmlTheme.'/assets/js/jquery.validate.min.js')?>"></script>
<script src="<?php echo base_url(ciHtmlTheme.'/assets/js/jquery.form.min.js')?>"></script>

<script src="<?php echo base_url(ciHtmlTheme.'/assets/js/mdtimepicker.js'); ?>"></script>


<script>
$('.bxslider').bxSlider({
  pagerCustom: '#bx-pager'
});
function placeMyBid()
{
    var bidder_id = '<?php echo $this->session->userdata('bidder_id')?>';
    if (bidder_id)
    {
        $("#biddingModal").modal('show');
    }
    else
    {
        alert('Please Login');
    }
}
function add_favourite(product_id)
{
    var bidder_id = '<?php echo $this->session->userdata('bidder_id')?>';
    if (bidder_id)
    {
       $.ajax({
            url:'<?php echo base_url()?>products/add_products_in_favourite',
            type:'post',
            dataType:'json',
            data:{'bidder_id':bidder_id,'product_id':product_id,type:'P'},
            success:function(resObj)
            {
              if (resObj.status ='success')
              {
                $(".fav").html('<i class=" fa fa-heart"></i> Added In Favourite');
                
              }
              else
              {
                alert(resObj.msg);
              }
                
            }
        }); 
    }
    else
    {
        alert('Please Login');
    }
}
function send_participation_request(product_id)
{
    var bidder_id = '<?php echo $this->session->userdata('bidder_id')?>';
    if (bidder_id)
    {
        $.ajax({
            url:'<?php echo base_url()?>products/send_participation_request',
            type:'post',
            dataType:'json',
            data:{product_id:product_id},
            success:function(resObj)
            {
            
                alert(resObj.message);
            },
            error:function()
            {
                alert('Invalid Request');
            }
        });
    }
    else
    {
     alert('Please Login');
    }
}
function submit_bid()
{
        var vRules 	= 	{
                    my_bid:{required:true},
            };
        var vMessages = {
                    my_bid:{required:"Please Enter Bid"},
                        };
        $("#bid_now_frm").validate({
            rules: vRules,
            messages: vMessages,
            submitHandler: function(form){
                $(form).ajaxSubmit({
                    url: '<?php echo base_url();?>products/submit_bid',
                    type:"post",
                    dataType: 'json',
                    success: function (resObj) {
                        alert(resObj.message);
                        window.location.reload();
                    },
                    error:function()
                    {
                        alert('Invalid Request');
                    }
                });
            }
        });
        
}

function send_msg()
{
  var vRules 	= 	{
                    name:{required:true},
                    email: {required : true},
                    phone: {required : true},
                    message:{required : true}
            };
        var vMessages = {
                    name:{required:"Please Enter Name"},
                    email:{required:"Please Enter Email"},
                    phone:{required:"Please Enter Phone"},
                    message:{required:"Please Enter Message"},

                        };
        $("#send_message").validate({
            rules: vRules,
            messages: vMessages,
            submitHandler: function(form){
                $(form).ajaxSubmit({
                    url: '<?php echo base_url();?>products/send_message',
                    type:"post",
                    dataType: 'json',
                    success: function (resObj) {
                       alert(resObj.message);
                       $("#contactAdvertiser").modal('hide');
                    },
                    error:function()
                    {
                        alert('Invalid Request');
                    }
                });
            }
        });
           
}
function msg_popup()
{
 
  $('#contactAdvertiser').modal({show : true});


 
}

</script> 
<!-- include jquery autocomplete plugin  -->

