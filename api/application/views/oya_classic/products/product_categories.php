<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<!-- Fav and touch icons -->
<link rel="apple-touch-icon-precomposed" sizes="144x144" href="<?php echo base_url(ciHtmlTheme.'/assets/ico/apple-touch-icon-144.png')?>">
<link rel="apple-touch-icon-precomposed" sizes="114x114" href="<?php echo base_url(ciHtmlTheme.'/assets/ico/apple-touch-icon-114.png')?>">
<link rel="apple-touch-icon-precomposed" sizes="72x72" href="<?php echo base_url(ciHtmlTheme.'/assets/ico/apple-touch-icon-72.png')?>">
<link rel="apple-touch-icon-precomposed" sizes="57x57" href="<?php echo base_url(ciHtmlTheme.'/assets/ico/apple-touch-icon-57.png')?>">
<link rel="shortcut icon" href="<?php echo base_url(ciHtmlTheme.'/assets/ico/favicon.png')?>">
<title>BOOTCLASIFIED - Responsive Classified Theme</title>
<!-- Bootstrap core CSS -->
<link href="<?php echo base_url(ciHtmlTheme.'/assets/bootstrap/css/bootstrap.min.css')?>" rel="stylesheet">
<!-- Custom styles for this template -->
<link href="<?php echo base_url(ciHtmlTheme.'/assets/css/style.css')?>" rel="stylesheet">
<link href="<?php echo base_url(ciHtmlTheme.'/assets/css/custom.css')?>" rel="stylesheet">
<!-- styles needed for carousel slider -->
<link href="<?php echo base_url(ciHtmlTheme.'/assets/css/owl.carousel.css')?>" rel="stylesheet">
<link href="<?php echo base_url(ciHtmlTheme.'/assets/css/owl.theme.css')?>" rel="stylesheet">
<link href="<?php echo base_url(ciHtmlTheme.'/assets/css/niceCountryInput.css')?>" rel="stylesheet">
<link href="<?php echo base_url(ciHtmlTheme.'/assets/css/mdtimepicker.css'); ?>" rel="stylesheet" />



<script>
    paceOptions = {
      elements: true
    };

</script>
<script src="<?php echo base_url(ciHtmlTheme.'/assets/js/pace.min.js')?>"></script>

 </head>
<body>

<div id="wrapper">
<!-- /.header -->
 <?php $this->load->view(ciHtmlTheme.'/includes/header');?>
  <!-- /.header -->
  
  <!-- main container -->
  
  
  <div class="main-container">
    <div class="container">
      <div class="row">
        <div class="col-sm-3 page-sidebar">
          <aside>
            <div class="inner-box">
              <div class="categories-list  list-filter">
                <h5 class="list-title"><strong><a href="#">All Categories</a></strong></h5>
                <ul class=" list-unstyled">
                <?php
                    if ( ! empty($product_categories))
                    {
                        foreach($product_categories as $key=>$cat)
                        {
                ?>
                             <li><a href="sub-category-sub-location.html"><span class="title"><?php echo ucfirst($cat['category_name'])?></span><span class="count">&nbsp;8626</span></a> </li>
                <?php
                        }
                    }
                ?>
                </ul>
              </div>
              <!--/.categories-list-->
              
              <div class="locations-list  list-filter">
                <h5 class="list-title"><strong><a href="#">Location</a></strong></h5>
                <ul class="browse-list list-unstyled long-list">
                  <?php 
                        if ( ! empty($product_locations))
                        {
                            foreach($product_locations as $key=>$loc)
                            {
                    ?>

                                <li> <a href="sub-category-sub-location.html"> <?php echo $loc['city_name']?> </a></li>
                    <?php
                            }
                        }
                  ?>
                  
                </ul>
              </div>
              <!--/.locations-list-->
              
              <div class="locations-list  list-filter">
                <h5 class="list-title"><strong><a href="#">Price range</a></strong></h5>
                <form role="form" class="form-inline ">
                  <div class="form-group col-sm-4 no-padding">
                    <input type="text" placeholder="$ 2000 " id="minPrice" class="form-control">
                  </div>
                  <div class="form-group col-sm-1 no-padding text-center"> - </div>
                  <div class="form-group col-sm-4 no-padding">
                    <input type="text" placeholder="$ 3000 " id="maxPrice" class="form-control">
                  </div>
                  <div class="form-group col-sm-3 no-padding">
                    <button class="btn btn-default pull-right" type="submit">GO</button>
                  </div>
                </form>
                <div style="clear:both"></div>
              </div>
              <!--/.list-filter-->
              <div class="locations-list  list-filter">
                <h5 class="list-title"><strong><a href="#">Seller</a></strong></h5>
                <ul class="browse-list list-unstyled long-list">
                 <?php 
                    if ( ! empty($product_vendors))
                    {
                        foreach($product_vendors as $key=>$vendor)
                        {
                    ?>
                         <li> <a href="sub-category-sub-location.html"><?php echo $vendor['vendor_name']?> <span class="count">28,705</span></a></li>    
                    <?php
                        }
                    }
                 ?>
                </ul>
              </div>
              <!--/.list-filter-->
              <div class="locations-list  list-filter">
                <h5 class="list-title"><strong><a href="#">Condition</a></strong></h5>
                <ul class="browse-list list-unstyled long-list">
                  <li> <a href="sub-category-sub-location.html">Live Auction <span class="count">228,705</span></a></li>
                  <li> <a href="sub-category-sub-location.html">Make Offer <span class="count">28,705</span></a></li>
                  <li> <a href="sub-category-sub-location.html">Online Auction </a></li>
                  <li> <a href="sub-category-sub-location.html">Buy Now </a></li>
                  <li> <a href="sub-category-sub-location.html">Sailed Bid </a></li>


                </ul>
              </div>
              <!--/.list-filter-->
              <div style="clear:both"></div>
            </div>
            
            <!--/.categories-list--> 
          </aside>
        </div>
        <!--/.page-side-bar-->
        <div class="col-sm-9 page-content col-thin-left">
          <div class="category-list">
            <div class="tab-box "> 
              
              <!-- Nav tabs -->
              <ul class="nav nav-tabs add-tabs" id="ajaxTabs" role="tablist">
                <li class="active"><a href="#allAds" data-url="ajax/1.html" role="tab" data-toggle="tab">All Ads <span class="badge"><?php echo $products_count; ?></span></a></li>
              </ul>
              <div class="tab-filter">
                <select class="selectpicker" data-style="btn-select" id="range" data-width="auto" onchange="get_range(this.value);">
                  <option>Sort by</option>
                  <option value="asc">Price: Low to High</option>
                  <option value="desc">Price: High to Low</option>
                </select>
              </div>
            </div>
            <!--/.tab-box-->
            
            <div class="listing-filter">
              <div class="pull-left col-xs-6">
                <div class="breadcrumb-list"> <a href="#" class="current"> <span>All ads</span></a> in India <a href="#selectRegion" id="dropdownMenu1"  data-toggle="modal"> <span class="caret"></span> </a> </div>
              </div>
              <div class="pull-right col-xs-6 text-right listing-view-action"> <span class="list-view active"><i class="  icon-th"></i></span> <span class="compact-view"><i class=" icon-th-list  "></i></span> <span class="grid-view "><i class=" icon-th-large "></i></span> </div>
              <div style="clear:both"></div>
            </div>
            <!--/.listing-filter-->
            
            <div class="adds-wrapper">
              <div class="tab-content scrollit">
                <div class="tab-pane active" id="allAds" >
                    <table class="table table-responsive table-bordered table-hover tableWrapper">
                        <thead> 
                            <th>Sale No </th>
                            <th>Start Date </th>
                            <th>End Date </th>
                            <th>Product Name </th>
                            <th>Product Category </th>
                            <th>Manufacturer </th>
                            <th>Buying Format </th>

                        </thead>
                        <tbody id="results">
                            
                            <span id="loading">Loading Please wait...</span>

                        </tbody>
                    </table>
                </div>
                <div class="tab-pane" id="businessAds"></div>
                <div class="tab-pane" id="personalAds"></div>
              </div>
            </div>
            <!--/.adds-wrapper-->
            
            <div class="tab-box  save-search-bar text-center"> <a href=""> <i class=" icon-star-empty"></i> Save Search </a> </div>
          </div>
         <!--  <div class="pagination-bar text-center">
            <ul class="pagination">
              <li  class="active"><a href="#">1</a></li>
              <li><a href="#">2</a></li>
              <li><a href="#">3</a></li>
              <li><a href="#">4</a></li>
              <li><a href="#">5</a></li>
              <li><a href="#"> ...</a></li>
              <li><a class="pagination-btn" href="#">Next &raquo;</a></li>
            </ul>
          </div> -->
          <!--/.pagination-bar -->
          
          <!-- <div class="post-promo text-center">
            <h2> Do you get anything for sell ? </h2>
            <h5>Sell your products online FOR FREE. It's easier than you think !</h5>
            <a href="post-ads.html" class="btn btn-lg btn-border btn-post btn-danger">Post a Free Ad </a></div> -->
          <!--/.post-promo--> 
          
        </div>
        <!--/.page-content--> 
        
      </div>
    </div>
  </div>
  <!-- /.main-container -->
  
  
  
	<?php $this->load->view(ciHtmlTheme.'/includes/call_back');?>
  <!-- /.footer --> 
  <?php $this->load->view(ciHtmlTheme.'/includes/footer');?>
 
</div>
<!-- /.wrapper --> 
<!-- Modal Change City -->

<div class="modal fade" id="selectRegion" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title" id="exampleModalLabel"><i class=" icon-map"></i> Select your region </h4>
      </div>
      <div class="modal-body">
        <div class="row">
          <div class="col-sm-12">
        
            <p>Popular cities in <strong id="country_name">India</strong>
            </p>

          
            <div class="niceCountryInputSelector" id='flag_name' style="width: 300px;" data-selectedcountry="IN" data-showspecial="false" data-showflags="true" data-i18nall="All selected"
            data-i18nnofilter="No selection" data-i18nfilter="Filter" data-onchangecallback="onChangeCallback" />          

            <hr class="hr-thin">
          </div>
          
        </div>
      </div>
    </div>
  </div>
</div>

<!-- /.modal --> 

<!-- Le javascript
================================================== --> 
<!-- Placed at the end of the document so the pages load faster --> 
<script src="<?php echo base_url(ciHtmlTheme.'/assets/js/jquery.min.js')?>"> </script>
<script src="<?php echo base_url(ciHtmlTheme.'/assets/bootstrap/js/bootstrap.min.js')?>"></script> 
<!-- include carousel slider plugin  --> 
<script src="<?php echo base_url(ciHtmlTheme.'/assets/js/owl.carousel.min.js')?>"></script> 
<!-- include equal height plugin  --> 
<script src="<?php echo base_url(ciHtmlTheme.'/assets/js/jquery.matchHeight-min.js')?>"></script> 
<!-- include jquery list shorting plugin plugin  --> 
<script src="<?php echo base_url(ciHtmlTheme.'/assets/js/hideMaxListItem.js')?>"></script> 
<!-- include jquery.fs plugin for custom scroller and selecter  --> 
<script src="<?php echo base_url(ciHtmlTheme.'/assets/plugins/jquery.fs.scroller/jquery.fs.scroller.js')?>"></script>
<script src="<?php echo base_url(ciHtmlTheme.'/assets/plugins/jquery.fs.selecter/jquery.fs.selecter.js')?>"></script>
<!-- include custom script for site  --> 
<script src="<?php echo base_url(ciHtmlTheme.'/assets/js/script.js')?>"></script>
<!-- include jquery autocomplete plugin  -->
<script type="text/javascript" src="<?php echo base_url(ciHtmlTheme.'/assets/plugins/autocomplete/jquery.mockjax.js')?>"></script>
<script type="text/javascript" src="<?php echo base_url(ciHtmlTheme.'/assets/plugins/autocomplete/jquery.autocomplete.js')?>"></script>
<script type="text/javascript" src="<?php echo base_url(ciHtmlTheme.'/assets/plugins/autocomplete/usastates.js')?>"></script>
<script src="<?php echo base_url(ciHtmlTheme.'/assets/js/niceCountryInput.js')?>"></script>
<script src="<?php echo base_url(ciHtmlTheme.'/assets/js/mdtimepicker.js'); ?>"></script>


<script>
    var limit = 8;
    var start = 0;
    var action = 'inactive';
    var country ='';
    var range = $("#range").val();
  function onChangeCallback(ctr){
      console.log("The country was changed: " + ctr);
      $("#results").empty();
       limit = 8;
       start = 0;
      load_table_data(limit, start,ctr,range);
      $("#country_name").html();
     
  }

  $(document).ready(function () {
      $(".niceCountryInputSelector").each(function(i,e){
          new NiceCountryInput(e).init();
      });
         
        limit = 8;
        start = 0;
        var action = 'inactive';
        var country = '';
        var range = $("#range").val();
        load_table_data(limit, start,country,range);
        
       
   
  });
  $('.tab-content').scroll(function() {
    if($(this).scrollTop() + $(this).innerHeight() > $(this)[0].scrollHeight && action == 'inactive')
    {
       
        action = 'active';
        start = start + limit;
        setTimeout(function(){
        load_table_data(limit, start,country,range);
        }, 1000);
    }
});
  function get_range(range)
  {
    limit = 8;
    start = 0;
    var country = '';
    var action = 'inactive';
    $("#results").empty();
    load_table_data(limit, start,country,range);
  }
function load_table_data(limit, start,country,sort='')
{
    
    $.ajax({
                url: "<?php echo base_url('home/product_listing')?>",
                type: "post",
                dataType: "json",
                data: {
                'start': start,
                'limit':limit,
                'country_code':country,
                'sort':sort
                },
                success: function(data) 
                {
                    
                    $.each(data, function(key, value) 
                    {
                        if(value.manufacturer_name)
                        {
                            var manufacturer_name = value.manufacturer_name;
                        }
                        else
                        {
                            var manufacturer_name ='';
                        }
                        $("#results").append("<tr>"+
                                    "<td>"+value.sale_no+"</td>"+
                                    "<td>"+value.start_date+"</td>"+
                                    "<td>"+value.end_date+"</td>"+
                                    "<td>"+value.product_name+"</td>"+
                                    "<td>"+value.category_name+"</td>"+
                                    "<td>"+manufacturer_name+"</td>"+
                                    "<td>"+value.buying_format+"</td>"+
                                    "</tr>");
                        if(data == '')
                        {
                            $('#loading').html("<button type='button' class='btn btn-info'>No Data Found</button>");
                            action = 'active';
                        }
                        else
                        {
                            $('#loading').html("<button type='button' class='btn btn-warning'>Please Wait....</button>");
                            action = "inactive";
                        }
                    });
                    

                }
            });
            

}
</script>
</body>
</html>
