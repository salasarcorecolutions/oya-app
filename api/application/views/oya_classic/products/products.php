<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<!-- Fav and touch icons -->
<link rel="apple-touch-icon-precomposed" sizes="144x144" href="<?php echo base_url(ciHtmlTheme.'/assets/ico/apple-touch-icon-144.png')?>">
<link rel="apple-touch-icon-precomposed" sizes="114x114" href="<?php echo base_url(ciHtmlTheme.'/assets/ico/apple-touch-icon-114.png')?>">
<link rel="apple-touch-icon-precomposed" sizes="72x72" href="<?php echo base_url(ciHtmlTheme.'/assets/ico/apple-touch-icon-72.png')?>">
<link rel="apple-touch-icon-precomposed" sizes="57x57" href="<?php echo base_url(ciHtmlTheme.'/assets/ico/apple-touch-icon-57.png')?>">
<link rel="shortcut icon" href="<?php echo base_url(ciHtmlTheme.'/assets/ico/favicon.png')?>">
<title>Products Listing</title>
<!-- Bootstrap core CSS -->
<link href="<?php echo base_url(ciHtmlTheme.'/assets/bootstrap/css/bootstrap.min.css')?>" rel="stylesheet">
<!-- Custom styles for this template -->
<link href="<?php echo base_url(ciHtmlTheme.'/assets/css/style.css')?>" rel="stylesheet">
<link href="<?php echo base_url(ciHtmlTheme.'/assets/css/custom.css')?>" rel="stylesheet">
<!-- styles needed for carousel slider -->
<link href="<?php echo base_url(ciHtmlTheme.'/assets/css/owl.carousel.css')?>" rel="stylesheet">
<link href="<?php echo base_url(ciHtmlTheme.'/assets/css/owl.theme.css')?>" rel="stylesheet">
<link href="<?php echo base_url(ciHtmlTheme.'/assets/css/niceCountryInput.css')?>" rel="stylesheet">
<link href="<?php echo base_url(ciHtmlTheme.'/assets/css/mdtimepicker.css'); ?>" rel="stylesheet" />





<script>
    paceOptions = {
      elements: true
    };
</script>
<script src="<?php echo base_url(ciHtmlTheme.'/assets/js/pace.min.js')?>"></script>
<style>
  
 </style>
 </head>
<body>

<div id="wrapper">
<!-- /.header -->
 <?php $this->load->view(ciHtmlTheme.'/includes/header');?>
  <!-- /.header -->
  
  <div class="intro view_prod_banner">
    <div class="dtable hw100">
      <div class="dtable-cell hw100">
        <div class="container text-center">
       <h1 class="intro-title animated fadeInDown"> Find Products  </h1>
          <p class="sub animateme fittext3 animated fadeIn"> Welcome to world of Products </p>
      
          <!--<div class="row search-row animated fadeInUp">
              <div class="col-lg-4 col-sm-4 search-col relative locationicon">
               <i class="icon-location-2 icon-append"></i>
                <input type="text" name="country" id="autocomplete-ajax"  class="form-control locinput input-rel searchtag-input has-icon" placeholder="City/Zipcode..." value="">

              </div>
              <div class="col-lg-4 col-sm-4 search-col relative"> <i class="icon-docs icon-append"></i>
                <input type="text" name="ads"  class="form-control has-icon" placeholder="I'm looking for a ..." value="">
              </div>
              <div class="col-lg-4 col-sm-4 search-col">
                <button class="btn btn-primary btn-search btn-block"><i class="icon-search"></i><strong>Find</strong></button>
              </div>
            </div>-->
          
        </div>
      </div>
    </div>
  </div>
  <!-- /.intro -->
  
  <div class="main-container">
    <div class="container-fluid">
      <div class="row">
        <div id='sider' class="col-sm-2 page-sidebar">
         <aside>
            <div class="inner-box">
              <div class="categories-list  list-filter">
                <h5 class="list-title"><strong><a href="#">All Categories</a></strong></h5>
                <ul class=" list-unstyled">
                <?php
                    if ( ! empty($product_cat))
                    {
                      $cat_arr = array();
                        foreach($product_cat as $key=>$category)
                        {
                          if ( ! empty($cat))
                          {
                            $cat_arr = explode('_',$cat);
                            
                          }
                ?>
                             <li>
                             <input type="checkbox" class="cat" name="cat[]" value="<?php echo $category['category_name']?>" onclick="change_category(<?php echo $category['category_id']?>)" <?php echo (in_array($category['category_name'], $cat_arr))?'checked':'';?>/>
                              <span class="title"> <?php echo ucfirst($category['category_name'])?></span>&nbsp;<span class="badge badge-success pull-right"> 
                              <?php echo $category['cnt']?>
                              </span>
                            </li>
                <?php
                        }
                    }
                ?>
                </ul>
              </div>
              <!--/.categories-list-->
              
              <div class="locations-list  list-filter">
                <h5 class="list-title"><strong><a href="#">Location</a></strong></h5>
                <ul class="browse-list list-unstyled long-list">
                  <?php 
                        if ( ! empty($product_locations))
                        {
                            foreach($product_locations as $key=>$ctr)
                            {
                              $location = array();
                              if ( ! empty($ctr['city_name']))
                              {
                                if ( ! empty($loc))
                                {
                                  $location = explode('_',$loc);
                                }
                    ?>

                                <li> <input type="checkbox" name="ctr[]" class="ctr" value="<?php echo $ctr['countries_iso_code_2']?>" onclick="change_loc('<?php echo $ctr['countries_iso_code_2']?>',this);" <?php echo (in_array($ctr['countries_iso_code_2'], $location))?'checked':'';?>/>
                                <span class="title"> <?php echo ucfirst($ctr['city_name'])?></span>&nbsp; <span class="badge badge-success pull-right"><?php echo $ctr['cnt']?></span></li>
                    <?php
                              }
                            }
                        }
                  ?>
                  
                </ul>
              </div>
              <!--/.locations-list-->
              
              <div class="locations-list  list-filter">
                <h5 class="list-title"><strong><a href="#">Price range</a></strong></h5>
                <form role="form" class="form-inline ">
                  <div class="form-group col-sm-4 no-padding">
                    <input type="text" placeholder="$ 2000 " id="minPrice" class="form-control" value="<?php echo $sp?>"> 
                  </div>
                  <div class="form-group col-sm-1 no-padding text-center"> - </div>
                  <div class="form-group col-sm-4 no-padding">
                    <input type="text" placeholder="$ 3000 " id="maxPrice" class="form-control" value="<?php echo $ep?>">
                  </div>
                  <div class="form-group col-sm-3 no-padding">
                    <button class="btn btn-default pull-right" type="button" onclick="submit_url();">GO</button>
                  </div>
                </form>
                <div style="clear:both"></div>
              </div>
              <!--/.list-filter-->
              <div class="locations-list  list-filter">
                <h5 class="list-title"><strong><a href="#">Manufacturer</a></strong></h5>
                <ul class="browse-list list-unstyled long-list">
                 <?php 
                    if ( ! empty($product_manufacturer))
                    {
                      $mf_arr = array();
                        foreach($product_manufacturer as $key=>$vendor)
                        {
                          if ( ! empty($mf))
                          {
                            $mf_arr = explode('_',$mf);
                          }
                            if ($vendor['cnt'] > 0)
                            {
                    ?>
                                 <li><input type="checkbox" class="manufacture" name="manufacturer[]" value="<?php echo $vendor['manufacturer_name']?>" onclick="change_manufacturer('<?php echo $vendor['manufacturer_name']?>');" <?php echo (in_array($vendor['manufacturer_name'], $mf_arr))?'checked':'';?>/> 
                                <?php echo $vendor['manufacturer_name']?> 
                                      &nbsp;<span class="badge badge-success pull-right"><?php echo $vendor['cnt']?></span></li>    
                      <?php
                          }
                        }
                    }
                 ?>
                </ul>
              </div>

              <div class="locations-list  list-filter">
                <h5 class="list-title"><strong><a href="#">Seller</a></strong></h5>
                <ul class="browse-list list-unstyled long-list">
                 <?php 
                    $ven = array();
                    if ( ! empty($product_seller))
                    {
                        foreach($product_seller as $key=>$cnt)
                        {
                          if ( ! empty($seller))
                          {
                            $ven = explode('_',$seller);
                            
                          }
                    ?>
                         <li><input type="checkbox" class="seller" name="seller[]" value="<?php echo $key ?>" onclick="change_seller('<?php echo $key?>');" <?php echo (in_array($key, $ven))?'checked':'';?>/> 
                         <?php echo $key?> &nbsp;<span class="badge badge-success pull-right"><?php echo $cnt['count']?></span></a></li>    
                    <?php
                        }
                    }
                 ?>
                </ul>
              </div>
              <!--/.list-filter-->
              <div class="locations-list  list-filter">
                <h5 class="list-title"><strong><a href="#">Buying Format</a></strong></h5>
                <ul class="browse-list list-unstyled long-list">
                <?php 
                    $arr= array();
                    if ( ! empty($buying_format))
                    {
                        foreach($buying_format as $key=>$format)
                        {
                          if ( ! empty($format['buying_format']))
                          {
                            if ( ! empty($buy))
                            {
                              $arr = explode('_',$buy);
                            }
                           
                    ?>
                         <li><input type="checkbox" name="buy[]" class="buying_format" value="<?php echo $format['buying_format']?>" onclick="change_buying_format('<?php echo $format['buying_format']?>',this);"  <?php echo (in_array($format['buying_format'], $arr))?'checked':'';?>/> <?php echo $format['buying_format']?>&nbsp; <span class="badge badge-success pull-right"><?php echo $format['cnt']?></span></li>    
                    <?php
                          }
                        }
                    }
                 ?>
                </ul>
              </div>
              <!--/.list-filter-->
              <div style="clear:both"></div>
            </div>
            
            <!--/.categories-list--> 
          </aside>
        </div>
        <!--/.page-sidebar-->
        
        <div class="col-sm-10 page-content">
         
          
          <div class="category-list">
            <div class="tab-box "> 
              
              <!-- Nav tabs -->
              <ul class="nav nav-tabs add-tabs" role="tablist">
                <li class="active"><a href="#allAds" role="tab" data-toggle="tab">All Products <span class="badge"><?php echo $product_details_cnt?></span></a></li>
              </ul>
              <div class="tab-filter">
                <select class="selectpicker" data-style="btn-select" data-width="auto" id="range" onchange="load_more('first');">
                  <option value=''>Sort by</option>
                  <option value="asc">Price: Low to High</option>
                  <option value="desc">Price: High to Low</option>
                </select>
              </div>
            </div>
            <!--/.tab-box-->
            
            <div class="listing-filter">
              <div class="pull-left col-xs-6">
               <div class="breadcrumb-list"> <a href="#" class="current"> <span>All Products</span></a> in Location <a href="#selectRegion" id="dropdownMenu1"  data-toggle="modal"> <span class="caret"></span> </a> </div>
              </div>
                <div class="pull-right col-xs-6 text-right grid-view-action"> 
                    <!-- <span class="grid-view active"><i class=" icon-th-large "></i></span>  -->
                    <!-- <span class="list-view "><i class="  icon-th"></i></span>
                    <span class="compact-view"><i class=" icon-th-list  "></i></span>  -->
                </div>
              <div style="clear:both"></div>
            </div>

            <div class="adds-wrapper " id="parent">

            </div>


            <!--/.listing-filter-->
            
             <!--/.adds-wrapper-->
            <input type="hidden" id="row" value="0">
            <input type="hidden" id="all" value="<?php echo $product_details_cnt?>">
            <input type="hidden" id="category_id" value="<?php echo $this->input->get('cat')?>"/>
            <input type="hidden" id="country" value="<?php echo $this->input->get('ctr')?>">
            <input type="hidden" id="manufacturer" value="<?php echo $this->input->get('mf')?>">
            <input type="hidden" id="seller" value="<?php echo $this->input->get('seller')?>">
            <input type="hidden" id="buying_format" value="<?php echo $this->input->get('buy')?>"/>
            <div class="col-sm-6">
               <button type="button" class="btn col-sm-3 pull-right load-more" style="background:#004DDA;color:white;margin-top:10px;" onclick="load_more();">  Load More.. </button>
            </div>
          </div>
         
       
  
        </div>
        <!--/.page-content--> 
      </div>
      <!--/.row--> 
    </div> 
  </div>
  <!-- /.main-container -->
  
  <!-- /.counts -->
	<?php $this->load->view(ciHtmlTheme.'/includes/counts');?>
  <!-- /.claa me  -->
	<?php $this->load->view(ciHtmlTheme.'/includes/call_back');?>
  <!-- /.footer --> 
  <?php $this->load->view(ciHtmlTheme.'/includes/footer');?>
 
</div>
<!-- /.wrapper --> 
<div class="modal fade" id="selectRegion" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title" id="exampleModalLabel"><i class=" icon-map"></i> Select your region </h4>
      </div>
      <div class="modal-body">
        <div class="row">
          <div class="col-sm-12">
        
            <p>Popular cities in <strong id="country_name">India</strong>
            </p>

            
            <div class="niceCountryInputSelector" id='flag_name' style="width: 300px;" data-selectedcountry="IN" data-showspecial="false" data-showflags="true" data-i18nall="All selected"
            data-i18nnofilter="No selection" data-i18nfilter="Filter" data-onchangecallback="onChangeCallback" />          

            <hr class="hr-thin">
          </div>
          
        </div>
      </div>
    </div>
  </div>
</div>
<!-- Le javascript
================================================== --> 
<!-- Placed at the end of the document so the pages load faster --> 
<script src="<?php echo base_url(ciHtmlTheme.'/assets/js/jquery.min.js')?>"> </script>
<script src="<?php echo base_url(ciHtmlTheme.'/assets/bootstrap/js/bootstrap.min.js')?>"></script> 
<!-- include carousel slider plugin  --> 
<script src="<?php echo base_url(ciHtmlTheme.'/assets/js/owl.carousel.min.js')?>"></script> 
<!-- include equal height plugin  --> 
<script src="<?php echo base_url(ciHtmlTheme.'/assets/js/jquery.matchHeight-min.js')?>"></script> 
<!-- include jquery list shorting plugin plugin  --> 
<script src="<?php echo base_url(ciHtmlTheme.'/assets/js/hideMaxListItem.js')?>"></script> 
<!-- include jquery.fs plugin for custom scroller and selecter  --> 
<script src="<?php echo base_url(ciHtmlTheme.'/assets/plugins/jquery.fs.scroller/jquery.fs.scroller.js')?>"></script>
<script src="<?php echo base_url(ciHtmlTheme.'/assets/plugins/jquery.fs.selecter/jquery.fs.selecter.js')?>"></script>
<!-- include custom script for site  --> 
<script src="<?php echo base_url(ciHtmlTheme.'/assets/js/script.js')?>"></script>
<!-- include jquery autocomplete plugin  -->
<script src="<?php echo base_url(ciHtmlTheme.'/assets/js/jquery.validate.min.js')?>"></script>
<script src="<?php echo base_url(ciHtmlTheme.'/assets/js/jquery.form.min.js')?>"></script>

<script src="<?php echo base_url(ciHtmlTheme.'/assets/js/niceCountryInput.js')?>"></script>
<script src="<?php echo base_url(ciHtmlTheme.'/assets/js/mdtimepicker.js'); ?>"></script>

<script>
$(document).ready(function () {
  $(".niceCountryInputSelector").each(function(i,e){
      new NiceCountryInput(e).init();
      load_more('first');
     
  });
});
function change_loc(country_code)
{
  var term="";
  $('.ctr').each(function() {if($(this).is(':checked')) {
    term =term;
    if (term)
    {
      term+="_";
    }
    term+=$(this).val();
  }});
  $("#country").val(term);
  submit_url();
}
function change_manufacturer(manufacturer_id)
{
  var term="";
	$('.manufacture').each(function() {if($(this).is(':checked')) {
    term =term;
    if (term)
    {
      term+="_";
    }
    term+=$(this).val();
  }});
  $('#manufacturer').val(term);
  submit_url();
  
}
function change_seller(vendor_id)
{
  var term="";
	$('.seller').each(function() {if($(this).is(':checked')) {
    term =term;
    if (term)
    {
      term+="_";
    }
    term+=$(this).val();
  }});
  $('#seller').val(term);
  submit_url();
}
function change_buying_format(buying_format,ele)
{
     var term="";
		 $('.buying_format').each(function() {if($(this).is(':checked')) {
      term =term;
      if (term)
      {
        term+="_";
      }
      term+=$(this).val();
    }});
     $('#buying_format').val(term);
     submit_url();
    
}

function submit_url()
{
  var url = '<?php echo base_url('products')?>?p=1';
  if($("#buying_format").val()!="") url=url+"&buy="+$("#buying_format").val();
  if($("#country").val()!="") url=url+"&ctr="+$("#country").val();
  if($("#seller").val()!="") url=url+"&seller="+$("#seller").val();
  if($("#manufacturer").val()!="") url=url+"&mf="+$("#manufacturer").val();
  if($("#category_id").val()!="") url=url+"&cat="+$("#category_id").val();
  if($("#minPrice").val()!="") url=url+"&start_price="+$("#minPrice").val();
  if($("#maxPrice").val()!="") url=url+"&end_price="+$("#maxPrice").val();
  window.history.pushState({path:url},'',url);
  load_more('first');

	
}

function onChangeCallback(ctr){
    $("#country").val(ctr);
    load_more('first');
    
}
function change_category(category_id)
{

  var term="";
  $('.cat').each(function() {if($(this).is(':checked')) {term =term+"_"+ $(this).val();}});
  $('#category_id').val(term);
  submit_url();
}

   // Load more data
   
   var i=0;
function load_more(record='')
{

  
  
    var country = $("#country").val();
    var range = $("#range").val();
    var start_price= $("#minPrice").val();
    var end_price = $("#maxPrice").val();
    var manufacturer_id = $("#manufacturer").val();
    var seller = $("#seller").val();
    var buying_format =  $("#buying_format").val();
    var category_id = $("#category_id").val();
    if (record)
    {
      $("#row").val(0);
    }
    var row = Number($('#row').val());
    var allcount = Number($('#all').val());
    var rowperpage = 8;

    if(row <= allcount){
      
        $.ajax({
            url: '<?php echo base_url('products/product_details')?>',
            type: 'get',
            data: {'cat':$("#category_id").val(),'mat':'<?php echo $sub_category?>','start':row,'limit':rowperpage,'country':country,
                  'order':range,'start_price':start_price,'end_price':end_price,'buying_format':buying_format,'manufacturer':manufacturer_id,'seller':seller},
            dataType:'json',
            beforeSend:function(){
                $(".load-more").text("Loading...");
            },
            success: function(response){

                // Setting little delay while displaying new content
                setTimeout(function() {
                    // appending posts after last post with class="post"
                    if (response)
                    {
                      var html='';
                      var c =1;
                        $.each(response,function(i,v)
                        {
                          i++;
                          allcount = response.count;
                          if ( ! Number.isNaN(i))
                          {
                            
                            var img = v.images.length;
                            html+='<div class="item-list make-grid" id='+v.product_id+'>'+
                                    '<ul class="product-labels">'+
                                        '<li>Product : '+v.sale_no+' </li>'+
                                    '</ul>'+
                                    '<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 no-padding">'+

                                        '<div id="myCarousel'+c+'" class="carousel slide" data-ride="carousel">';
                      
                          
                                  if (img > 0)
                                  {
                                      var i = 0;
                                      var j = 0;
                                      var count = img;
                                      var last_element = count -1; 
                                      var class_name='';
                                      var path_class ='';
                                      html+='<span class="photo-count"><i class="fa fa-camera"></i>'+count+'</span>'+
                                                '<ol class="carousel-indicators">';

                                        $.each(v.images,function(k,img)
                                        {
                                      
                                          if (i == last_element)
                                          {
                                            class_name = 'active';
                                          }
                                          else
                                          {
                                            class_name == '';
                                          }
                            
                                          html+='<li data-target="#myCarousel'+c+'" data-slide-to="'+i+'" class="'+class_name+'"></li>';
                            
                                          i++;
                                      });
                        
                                  html+='</ol>'+
                                    '<div class="carousel-inner">';
                                    $.each(v.images,function(index,img_path)
                                    {
                                      
                                      
                                        if (j == last_element)
                                        {
                                            path_class = "active";
                                        }
                                        else
                                        {
                                            path_class= "";
                                        }
                              
                                    html+='<div class="slider_img item '+path_class+'">'+
                                        '<img src="<?php echo AMAZON_BUCKET?>/<?php echo s3BucketTestDir?>images/auctionpic/salvage/'+img_path['image_name']+'">'+
                                    '</div>';
                              
                                    j++;
                                    });
                          
                                  html+='</div>'+
                                  '<a class="left carousel-control" href="#myCarousel'+c+'" data-slide="prev">'+
                                      '<span class="glyphicon glyphicon-chevron-left"></span><span class="sr-only">Previous</span>'+
                                  '</a>'+
                                  '<a class="right carousel-control" href="#myCarousel'+c+'" data-slide="next">'+
                                      '<span class="glyphicon glyphicon-chevron-right"></span><span class="sr-only">Next</span>'+
                                  '</a>';
                          }
                          else
                          {
                        
                            html+='<span class="photo-count"><i class="fa fa-camera"></i> 2 </span>'+ 
                    
                              '<ol class="carousel-indicators">'+
                                '<li data-target="#myCarousel'+c+'" data-slide-to="0" class=""></li>'+
                                '<li data-target="#myCarousel'+c+'" data-slide-to="1" class=""></li>'+
                                '<li data-target="#myCarousel'+c+'" data-slide-to="2" class="active"></li>'+
                            '</ol>'+
                            '<div class="carousel-inner">'+
                                '<div class="slider_img item">'+
                                    '<img src="https://img1.exportersindia.com/product_images/bc-full/dir_133/3984152/gun-metal-scrap-2339918.jpeg">'+
                                '</div>'+
                                '<div class="slider_img item">'+
                                    '<img src="https://img1.exportersindia.com/product_images/bc-full/dir_133/3984152/gun-metal-scrap-2339918.jpeg">'+
                                '</div>'+
                                '<div class="slider_img item active">'+
                                    '<img src="https://img1.exportersindia.com/product_images/bc-full/dir_133/3984152/gun-metal-scrap-2339918.jpeg">'+
                                '</div>'+
                            '</div>'+
                            '<a class="left carousel-control" href="#myCarousel'+c+'" data-slide="prev">'+
                                '<span class="glyphicon glyphicon-chevron-left"></span><span class="sr-only">Previous</span>'+
                            '</a>'+
                            '<a class="right carousel-control" href="#myCarousel'+c+'" data-slide="next">'+
                                '<span class="glyphicon glyphicon-chevron-right"></span><span class="sr-only">Next</span>'+
                            '</a>';
                      
                          }
                          
                        html+='</div>'+
                    '</div>'+

                    

                    '<div class="row mt-10">'+
                        '<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 ">'+
                            '<h5 class="add-details"> <a href="#"> '+v['product_name']+' </a> </h5>'+
                        '</div>'+
                    '</div>'+

                    '<div class="row mt-10">'+
                            '<div class="col-lg-2 col-md-2 col-sm-2 col-xs-2">'+
                                    '<span class="add-details"> <a href="#"> FOR: </a> </span>'+
                            '</div>'+
                            '<div class="col-lg-10 col-md-10 col-sm-10 col-xs-10 paddingL5" style="overflow:hidden">'+
                                '<span class="text-upper pointer" title="vendor_name" style="white-space:nowrap">'+v['vendor_name']+'</span>'+
                            '</div>'+
                    '</div>'+

                    '<div class="row mt-10">'+
                        '<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">'+
                            '<span class="start_time_icon"><i class="fa fa-hourglass-start" aria-hidden="true"></i></span> <span class="time_label">'+v['start_date']+' '+v['start_time']+'</span>'+
                        '</div>'+
                        '<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">'+
                            '<span class="end_time_icon"><i class="fa fa-hourglass-end" aria-hidden="true"></i></span><span class="time_label">'+v['end_date']+' '+v['end_time']+'</span>'+
                        '</div>'+
                    '</div>'+

                    '<div class="row mt-10">'+
                        '<div class="col-lg-8 col-xs-8 ">'+
                            '<a href="<?php echo base_url('products/view_product_details/')?>'+v['url']+'"><button type="button" class="btn btn-primary">View Details</button></a>'+
                        '</div>';
                        if(v['live_auction'])
                        {
                          html+='<div class="col-lg-4 col-xs-4">'+
                            '<button type="button" class="live-btn"><i class="fa fa-certificate"></i> <span>Live</span></button>'+
                        '</div>';
                        }
                        
                    html+='</div>'+
                '</div></div>';
                      }
                      c++;
                        });
                      
                      
                    }
 
                    if (row > 0)
                    {
                      
                      $(".item-list:last").after(html).show().fadeIn("slow");
                      
                    
                    }else{
                     
                      $(".adds-wrapper").html(html).show().fadeIn("slow");
                    } 
                   

                    var rowno = row + rowperpage;
                   
                    $("#row").val(rowno);
                    

                    // checking row value is greater than allcount or not
                   
                    if(rowno >= allcount){

                        // hide 
                        $('.load-more').hide();
                    }else{
                        $('.load-more').show();
                        $(".load-more").text("Load more..");
                    }
                }, 2000);

            }
        });
    }else{
        $('.load-more').text("Loading...");

        // Setting little delay while removing contents
        setTimeout(function() {

            // When row is greater than allcount then remove all class='post' element after 3 element
            $('.item-list:nth-child(3)').nextAll('.item-list').remove();

            // Reset the value of row
            $("#row").val(0);

            // Change the text and background
            $('.load-more').text("Load more..");
            $('.load-more').css("background","#15a9ce");
            
        }, 2000);


    }

}
</script>
</body>
</html>
