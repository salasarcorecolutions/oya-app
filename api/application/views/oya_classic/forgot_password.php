<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<!-- Fav and touch icons -->
<link rel="apple-touch-icon-precomposed" sizes="144x144" href="<?php echo base_url(ciHtmlTheme.'/assets/ico/apple-touch-icon-144-precomposed.png')?>">
<link rel="apple-touch-icon-precomposed" sizes="114x114" href="<?php echo base_url(ciHtmlTheme.'/assets/ico/apple-touch-icon-114-precomposed.png')?>">
<link rel="apple-touch-icon-precomposed" sizes="72x72" href="<?php echo base_url(ciHtmlTheme.'/assets/ico/apple-touch-icon-72-precomposed.png')?>">
<link rel="apple-touch-icon-precomposed" href="<?php echo base_url(ciHtmlTheme.'/ico/apple-touch-icon-57-precomposed.png')?>">
<link rel="shortcut icon" href="<?php echo base_url(ciHtmlTheme.'/assets/ico/favicon.png')?>">
<title>Forgot Password : Oya Auction</title>
<!-- Bootstrap core CSS -->
<link href="<?php echo base_url(ciHtmlTheme.'/assets/bootstrap/css/bootstrap.min.css')?>" rel="stylesheet">
<!-- Custom styles for this template -->
<link href="<?php echo base_url(ciHtmlTheme.'/assets/css/style.css')?>" rel="stylesheet">
<link href="<?php echo base_url(ciHtmlTheme.'/assets/css/custom.css')?>" rel="stylesheet">
<!-- styles needed for carousel slider -->
<link href="<?php echo base_url(ciHtmlTheme.'/assets/css/owl.carousel.css')?>" rel="stylesheet">
<link href="<?php echo base_url(ciHtmlTheme.'/assets/css/owl.theme.css')?>" rel="stylesheet">

<script>
    paceOptions = {
      elements: true
    };
</script>
<script src="<?php echo base_url(ciHtmlTheme.'/assets/js/pace.min.js')?>"></script>
 </head>
<body>

<div id="wrapper">
<!-- /.header -->
 <?php $this->load->view(ciHtmlTheme.'/includes/header');?>
  
  <div class="main-container">
    <div class="container">
      <div class="row">
        <div class="col-sm-5 login-box">
          <div class="panel panel-default">
              <div class="col-lg-12  box-title margin-bottom-5">
                    <div class="inner"><h2><i class="icon-user-add color-aqua"></i> FORGOT PASSWORD</h2>


                    </div>
                </div>
            <form role="form" name="frmResetPassword" id="frmResetPassword">   
            <div class="panel-body">
         
             
                <div class="form-group">
                  <label for="sender-email" class="control-label">Email:</label>
                  <div class="input-icon"> 
                  	<i class="icon-user fa"></i>
                    <input type="text"  placeholder="Email" class="form-control name" name="email" id="email" data-placement="bottom" title="Email">
                    
                  </div>
                   <div class="error_section">
                       <label id="username-error" style="display:none;" class="error" for="username">Please Enter Email </label>
                   </div>
                </div>
                
                <!-- <div class="form-group">
                  <label for="sender-email" class="control-label">Username:</label>
                  <div class="input-icon"> 
                  	<i class="icon-user fa"></i>
                    <input type="text"  placeholder="Username" class="form-control name" name="username" id="username" data-placement="bottom" title="Username ">
                    
                  </div>
                   <div class="error_section">
                       <label id="username-error" style="display:none;" class="error" for="username">Please Enter User name </label>
                   </div>
                </div> -->
                
                <div class="form-group"> <strong>I Am :</strong>
                 <label class="radio-inline" for="radios-0"><input name="login_type"  value="A"  type="radio" data-placement="bottom" title="Your Registration Type">Vendor/Auctioneer</label>
                 <label class="radio-inline" for="radios-1"><input name="login_type"  value="B"  type="radio">Buyer/Supplier</label>
                </div>
                <div class="error_section">
                <label style='display:none;' id="login_type-error" class="error" for="login_type">This field is required.</label>
                </div>
                <div class="form-group">
                  <a  href="#" class="btn btn-primary  btn-block" id="btnSendPasswordLink">Email Password Reset Link</a>
                  <div id="msg"></div>
                </div>
              
            </div>
            <div class="panel-footer">
    			<div class="login-box-btm text-center">
                <p> Don't have an account? <br>
                  <a href="<?php echo base_url('user/account_type');?>"><strong>Sign Up !</strong> </a> </p>
              	</div>
              
              <div style=" clear:both"></div>
            </div>
            </form>
          </div>
          
        </div>
      </div>
    </div>
  </div>
  <!-- /.main-container -->
  
 
  <!-- /.claa me  -->
	<?php $this->load->view(ciHtmlTheme.'/includes/call_back');?>
  <!-- /.footer --> 
  <?php $this->load->view(ciHtmlTheme.'/includes/footer');?>
 
</div>
<!-- /.wrapper --> 

<!-- Le javascript
================================================== --> 
<!-- Placed at the end of the document so the pages load faster --> 
<script src="<?php echo base_url(ciHtmlTheme.'/assets/js/jquery.min.js')?>"> </script>
<script src="<?php echo base_url(ciHtmlTheme.'/assets/bootstrap/js/bootstrap.min.js')?>"></script> 
<!-- include carousel slider plugin  --> 
<script src="<?php echo base_url(ciHtmlTheme.'/assets/js/owl.carousel.min.js')?>"></script> 

<!-- include equal height plugin  --> 
<script src="<?php echo base_url(ciHtmlTheme.'/assets/js/jquery.matchHeight-min.js')?>"></script> 
<!-- include jquery list shorting plugin plugin  --> 
<script src="<?php echo base_url(ciHtmlTheme.'/assets/js/hideMaxListItem.js')?>"></script> 
<!-- include jquery.fs plugin for custom scroller and selecter  --> 
<script src="<?php echo base_url(ciHtmlTheme.'/assets/plugins/jquery.fs.scroller/jquery.fs.scroller.js')?>"></script>
<script src="<?php echo base_url(ciHtmlTheme.'/assets/plugins/jquery.fs.selecter/jquery.fs.selecter.js')?>"></script>
<!-- include custom script for site  --> 
<script src="<?php echo base_url(ciHtmlTheme.'/assets/js/jquery.validate.min.js')?>"></script>
<script src="<?php echo base_url(ciHtmlTheme.'/assets/js/script.js')?>"></script>
<script>    
$("#btnSendPasswordLink").click(function(e){
	 e.preventDefault();
	var form=$("#frmResetPassword");
	form.validate({
	    ignore:[],
		rules: {
			username: {required : true},
		    login_type: {required : true},
			
			},
		messages: {	
			username:"This is required."
			},
			
		});

	if(form.valid())
	 	$( "#frmResetPassword" ).submit();
	

	
});
$('#frmResetPassword').on('submit',(function(e) {
	var $btn = $('#btnSendPasswordLink').button('loading');
	 e.preventDefault();
		$.ajax({
			url: '<?= base_url();?>user/send_credential',
			type:"POST",
			data: new FormData(this),
			contentType: false,
			cache: false,
			processData:false,
			dataType: 'json',
			success: function (resObj) {
				if (resObj.status) 
				{
					
					$("#msg").html('<div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button> <strong>Success! '+resObj.msg+'<i class="fa fa-warning"></i></div>');
					
					
				}
				else{
					$("#msg").html('<div class="alert alert-warning alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button> <strong>Warning! '+resObj.msg+'<i class="fa fa-warning"></i></div>');
					 $btn.button('reset')
				}	
			},
			error: function(){
				$btn.button('reset')
			}
			});
	}));		
 </script>
</body>
</html>
