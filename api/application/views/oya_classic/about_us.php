<?php
defined('BASEPATH') or exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<!-- Fav and touch icons -->
<link rel="apple-touch-icon-precomposed" sizes="144x144"
	href="<?php echo base_url(ciHtmlTheme.'/assets/ico/apple-touch-icon-144-precomposed.png')?>">
<link rel="apple-touch-icon-precomposed" sizes="114x114"
	href="<?php echo base_url(ciHtmlTheme.'/assets/ico/apple-touch-icon-114-precomposed.png')?>">
<link rel="apple-touch-icon-precomposed" sizes="72x72"
	href="<?php echo base_url(ciHtmlTheme.'/assets/ico/apple-touch-icon-72-precomposed.png')?>">
<link rel="apple-touch-icon-precomposed" href="<?php echo base_url(ciHtmlTheme.'/ico/apple-touch-icon-57-precomposed.png')?>">
<link rel="shortcut icon" 	href="<?php echo base_url(ciHtmlTheme.'/assets/ico/favicon.png')?>">
<title>Buyer Registration : Oya Auction</title>
<!-- Bootstrap core CSS -->
<link href="<?php echo base_url(ciHtmlTheme.'/assets/bootstrap/css/bootstrap.min.css')?>"	rel="stylesheet">
<link href="<?php echo base_url('assets/plugins/chosen/chosen.min.css')?>" rel="stylesheet">
<!-- Custom styles for this template -->

<link href="<?php echo base_url(ciHtmlTheme.'/assets/css/style.css')?>"	rel="stylesheet">
<link href="<?php echo base_url(ciHtmlTheme.'/assets/css/custom.css')?>" rel="stylesheet">

<!-- styles needed for carousel slider -->


<script>
    paceOptions = {
      elements: true
    };
</script>

<script src="<?php echo base_url(ciHtmlTheme.'/assets/js/pace.min.js')?>"></script>
</head>
<body>

	<div id="wrapper">
		<!-- /.header -->
 <?php $this->load->view(ciHtmlTheme.'/includes/header');?>

 <div class="intro-inner">
    <div class="about-intro">
      <div class="dtable hw100">
        <div class="dtable-cell hw100">
          
        </div>
      </div>
    </div>
    <!--/.about-intro --> 
    
  </div>
  <!-- /.intro-inner -->
  
  <div style='background-color:white;' class="main-container inner-page">
    <div class="container">
      <div class="section-content">
        <div class="row ">
          <h1 class="text-center title-1"> WELCOME TO INDIA’S BIGGEST ONLINE AUCTION OYA AUCTION</h1>
          <hr class="center-block small text-hr">
          <div class="col-sm-6">
            <div class="text-content has-lead-para text-left">
              <p class="lead"> At our open trading platform, the clients can easily determine the value of goods sold. Our platform is dynamic, user-friendly, transparent, and gives you 100% promised results. Our broad database and extensive network in the industry integrate enterprises and improve the relationships between the users. Our service includes </p>
              <ul style="list-style-type: square;">
                  <li>Creating and organizing online scrap management, procurement, and logistics solutions.</li>
                  <li>Organizing and conducting Forward, Reverse auctions and E-Tenders.</li>
                  <li> Getting detailed reports.</li>
                  <li> Manage who can participate in your auction.</li>
              </ul>
            </div>
          </div>
          <div class="col-sm-6"> <img src="<?php echo base_url(ciHtmlTheme.'/images/about_us/oya.jpg')?>" alt="imfo" class="img-responsive"> </div>
        </div>
      </div>
    </div>
  </div>
  <!-- /.main-container -->
  <div class="parallaxbox about-parallax-bottom">
    <div class="container">
        <h1 style='color:#1b2c92;' class="text-center title-1"> WHAT DO WE DO </h1> 
      <div class="row text-center featuredbox">

        <div class="col-sm-3 xs-gap">
          <div class="inner">
            <div class="icon-box-wrap"> <i class="fa fa-gavel"></i></div>
            <h3 class="title-4">FORWARD AUCTION</h3>
            <p>Preferred solution for Scrap Management or selling any material.</p>
          </div>
        </div>
        <div class="col-sm-3 xs-gap">
          <div class="inner">
            <div class="icon-box-wrap"> <i class="fa fa-gavel"></i></div>
            <h3 class="title-4">REVERSE AUCTION</h3>
            <p>Best for E-procurement and Logistics solutions.</p>
          </div>
        </div>
        <div class="col-sm-3 xs-gap">
          <div class="inner">
            <div class="icon-box-wrap"> <i class="fa fa-gavel"></i></div>
            <h3 class="title-4">DUTCH AUCTION </h3>
            <p>Highly competitive let the bidders give their best prices.</p>
          </div>
        </div>

        <div class="col-sm-3 xs-gap">
          <div class="inner">
            <div class="icon-box-wrap"> <i class="fa fa-gavel"></i></div>
            <h3 class="title-4">E-TENDER </h3>
            <p>A good solution to get the best price from the bidders.</p>
          </div>
        </div>
      </div>
    </div>
  </div>

 <?php $this->load->view(ciHtmlTheme.'/includes/footer');?>
 
 </div>
     <!-- /.wrapper -->
 
     <!-- Le javascript
 ================================================== -->
     <!-- Placed at the end of the document so the pages load faster -->
     <script	src="<?php echo base_url(ciHtmlTheme.'/assets/js/jquery.min.js')?>"> </script>
     <script	src="<?php echo base_url(ciHtmlTheme.'/assets/bootstrap/js/bootstrap.min.js')?>"></script>
     <!-- include carousel slider plugin  -->
     <script src="<?php echo base_url('assets/plugins/chosen/chosen.jquery.min.js')?>"></script>
     <!-- include equal height plugin  -->
     <script	src="<?php echo base_url(ciHtmlTheme.'/assets/js/jquery.matchHeight-min.js')?>"></script>
     <!-- include jquery list shorting plugin plugin  -->
     <script	src="<?php echo base_url(ciHtmlTheme.'/assets/js/hideMaxListItem.js')?>"></script>
     <!-- include jquery.fs plugin for custom scroller and selecter  -->
     <script	src="<?php echo base_url(ciHtmlTheme.'/assets/plugins/jquery.fs.scroller/jquery.fs.scroller.js')?>"></script>
     <script	src="<?php echo base_url(ciHtmlTheme.'/assets/plugins/jquery.fs.selecter/jquery.fs.selecter.js')?>"></script>
     <!-- include custom script for site  -->
     <script	src="<?php echo base_url(ciHtmlTheme.'/assets/js/jquery.validate.min.js')?>"></script>
     <script src="<?php echo base_url(ciHtmlTheme.'/assets/js/script.js')?>"></script>
     </body>
</html>
