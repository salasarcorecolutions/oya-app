<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<!-- Fav and touch icons -->
<link rel="apple-touch-icon-precomposed" sizes="144x144" href="<?php echo base_url(ciHtmlTheme.'/assets/ico/apple-touch-icon-144.png')?>">
<link rel="apple-touch-icon-precomposed" sizes="114x114" href="<?php echo base_url(ciHtmlTheme.'/assets/ico/apple-touch-icon-114.png')?>">
<link rel="apple-touch-icon-precomposed" sizes="72x72" href="<?php echo base_url(ciHtmlTheme.'/assets/ico/apple-touch-icon-72.png')?>">
<link rel="apple-touch-icon-precomposed" sizes="57x57" href="<?php echo base_url(ciHtmlTheme.'/assets/ico/apple-touch-icon-57.png')?>">
<link rel="shortcut icon" href="<?php echo base_url(ciHtmlTheme.'/assets/ico/favicon.png')?>">
<title>Auction Details</title>
<!-- Bootstrap core CSS -->
<link href="<?php echo base_url(ciHtmlTheme.'/assets/bootstrap/css/bootstrap.min.css')?>" rel="stylesheet">
<!-- Custom styles for this template -->
<link href="<?php echo base_url(ciHtmlTheme.'/assets/css/style.css')?>" rel="stylesheet">
<link href="<?php echo base_url(ciHtmlTheme.'/assets/css/custom.css')?>" rel="stylesheet">
<!-- styles needed for carousel slider -->
<link href="<?php echo base_url(ciHtmlTheme.'/assets/css/owl.carousel.css')?>" rel="stylesheet">
<link href="<?php echo base_url(ciHtmlTheme.'/assets/css/owl.theme.css')?>" rel="stylesheet">
<link href="<?php echo base_url(ciHtmlTheme.'/assets/plugins/bxslider/jquery.bxslider.css')?>" rel="stylesheet">
<link href="<?php echo base_url(ciHtmlTheme.'/assets/css/mdtimepicker.css'); ?>" rel="stylesheet" />


<script>
    paceOptions = {
      elements: true
    };
</script>
</head>
<body>

<div id="wrapper">
<!-- /.header -->
 <?php $this->load->view(ciHtmlTheme.'/includes/header');?>
  <!-- /.header -->
  
  <!-- main container -->
  
  
  <div class="main-container">
    <div class="container">
      <ol class="breadcrumb pull-left">
        <li><a href="#"><i class="icon-home fa"></i>Home</a></li>
        <li><a href="<?php echo base_url('buyer/auctions'); ?>">All Auction</a></li>
        <li class="active"><?php echo $auction_details['ynk_saleno']?></li>
      </ol>
      <div class="pull-right backtolist"><a href="<?php echo base_url('auction')?>"> <i class="fa fa-angle-double-left"></i> Back to Results</a></div>
    </div>
    <div class="container">
      <div class="row">
        <div class="col-sm-9 page-content col-thin-right">
          <div class="inner inner-box ads-details-wrapper">
            <h2> <?php echo $auction_details['ynk_saleno']?>  </h2>
            <span class="info-row"> <span class="date"><i class=" icon-clock"> </i> <?php echo $auction_details['start_date'].' '.$auction_details['stime']?> </span> <span> <b>To</b> </span><span class="date"><i class=" icon-clock"> </i> <?php echo $auction_details['end_date'].' '.$auction_details['etime']?> </span> - <span class="category"><?php echo $auction_details['ynk_saleno']?> </span>- <span class="item-location"><i class="fa fa-map-marker"></i> <?php echo $auction_details['name']?> </span> </span>
            <div class="ads-image">
              
              <ul class="bxslider">
                <?php 
                if ( ! empty($auction_images[0]['img']))
                {
                    foreach($auction_images as $k=>$img)
                    {
                ?>
                        <li><img src="<?php echo AMAZON_BUCKET ?>/<?php echo s3BucketTestDir ?>images/auctionpic/yankee/<?php echo $img['img']; ?>" alt="<?php echo $auction_details['ynk_saleno']?>" /></li>
                <?php
                    }
                }
                else
                {
                  ?>
                        <li><img src="../../images/auction.gif" alt="<?php echo $auction_details['ynk_saleno']?>" /></li>
                <?php

                }
                ?>
              </ul>
              <div id="bx-pager">
              <?php 
                if ( ! empty($auction_images[0]['img']))
                {
                    foreach($auction_images as $k=>$img)
                    {
                ?>
                       <a class="thumb-item-link" data-slide-index="0" href=""><img src="<?php echo AMAZON_BUCKET?>/<?php echo s3BucketTestDir ?>images/auctionpic/yankee/<?php echo $img['img']; ?>" alt="<?php echo $auction_details['ynk_saleno']?>" /></a>
                <?php
                    }  
                }
                ?>
                
             </div>
            </div>
            <!--ads-image-->
            
            <div class="Ads-Details">
              <h5 class="list-title"><strong>Auction Details</strong></h5>
              <div class="row">
                <div class="ads-details-info col-md-8">
                  <p><?php echo $auction_details['particulars'];?></p>
                  <ul class="list-circle">
                    <li>Auction Start Date Time : <?php echo $auction_details['start_date']. ' '.$auction_details['stime']?></li>
                    <li>Auction End Date Time : <?php echo $auction_details['end_date']. ' '.$auction_details['etime']?></li>
                    <?php 
                      if ( ! empty($auction_materials))
                      {
                        
                    ?>
                        <li>Materials : <?php echo $auction_materials?></li>
                    <?php
                      }
                    ?>
                    
                   
                  </ul>
                  
                  <?php
                    if ( ! empty($auction_documents['docs']))
                    {
                  ?>
                      <h4>Auction Documents</h4>
                      <ul class="list-circle">
                  <?php
                      foreach($auction_documents['docs'] as $k=>$v)
                      {
                    ?>
                        <li><?php echo $v['doc_name']?> : <a href="<?php echo $v['doc_path']?>">Click To view</a></li>
                    <?php
                      }
                    ?>
                      </ul>
                    <?php
                    }
                  ?>
           
                </div>
                <div class="col-md-4">
                  <aside class="panel panel-body panel-details">
                    <ul>
                      <li>
                        <p class=" no-margin "><strong>CMD/EMD: </strong> <?php echo $auction_details['cmd']?></p>
                      </li>
                      <li>
                        <p class="no-margin"><strong>Type: </strong><?php echo $auction_details['auctiontype']?></p>
                      </li>
                      <li>
                        <p class="no-margin"><strong>Location: </strong><?php echo $auction_details['name']?> </p>
                      </li>
                      <li>
                        <p class=" no-margin "><strong>Vendor: </strong> <?php echo $auction_details['vendor_name']?></p>
                      </li>
                     
                    </ul>
                  </aside>
                  <div class="ads-action">
                    <ul class="list-border">

                      <?php if (empty($fav_product))
                      {
                      ?>
                         <li><a  href="#" class="fav" onclick="add_favourite();"> <i class=" fa fa-heart"></i> Add In Favourite </a> </li>

                      <?php
                      }
                      else
                      {
                      ?>
                          <li><a  href="#" class="fav" onclick="add_favourite();"> <i class=" fa fa-heart"></i> Added In Favourite </a> </li>

                      <?php
                      }
                      ?>
                      <li><a  href="#" > <i class=" fa fa-share-alt"></i> Share ad</a> </li>
                    </ul>
                  </div>
                </div>
                <div class="col-md-12">
                  <h4>Auction Lot Details</h4>
                  <div class="table_scroll">
                    <table class="table table-responsive table-hovered table-bordered" style="max-height: 300px;overflow-y: scroll;">
                        <thead>
                            <tr>
                              
                              <td> <input type="checkbox" id="checkAll" class="all"> Lot No </td>
                              <td> Lot Description</td>
                              <td> Lot CMD</td>
                              <td> Total Qty</td>
                              <td> Currency</td>
                              <td> #</td>
                        
                            </tr>
                        </thead>
                        <tbody>
                                <?php 
                                  if ( ! empty($auction_lot_details))
                                  {
                                  
                                      foreach($auction_lot_details as $k=>$v)
                                      {
                                  ?>

                                      <tr>
                                        
                                        <td>
                                        <?php 
                                          if ( ! empty($lots))
                                          {

                                            if ( ! in_array($v['ynk_lotno'], $lots['lot_id']))
                                            {?>
                                              <input type="checkbox" name="lot[]" class="lot_check" value="<?php echo $v['ynk_lotno']?>"/>
                                            <?php 
                                            }
                                          
                                            
                                          }
                                          else
                                          {
                                          ?>
                                              <input type="checkbox" name="lot[]" class="lot_check" value="<?php echo $v['ynk_lotno']?>"/>
                                          <?php
                                          }
                                          ?> <?php echo $v['ynk_lotno']?>
                                        </td>
                                        <td><?php echo $v['product']?></td>
                                        <td><?php echo $v['cmd']?></td>
                                        <td><?php echo $v['totalqty'].' '.$v['totunit']?></td>
                                        <td><?php echo $v['currency']?></td>
                                        <td><a onclick="lot_details(<?php echo $v['ynk_lotno']?>);"><label class='label-control'>Preview</label></a><br/>
                                          <?php
                                            if ( ! empty($lots))
                                            {
                                              if (  in_array($v['ynk_lotno'], $lots['lot_id']))
                                              {
                                                if ($lots['allow_bidder'][$v['ynk_lotno']] == 0)
                                                {
                                              ?>
                                                <span class="text-danger"><b>Your Request Under Review</b></span>
                                              <?php
                                                }
                                              else
                                                {
                                            ?>
                                                <blink><span class="text-danger"><b>Approved</b></span></blink>
                                            <?php
                                                }
                                              }	
                                            }
                                            ?>
                                        
                                        </td>





                                      </tr>
                              <?php
                                  
                                  }
                              }
                            ?>
                        </tbody>
                    </table>
                  </div>
              </div>
            </div>
            <div class="content-footer text-left">  <a class="btn  btn-info" onclick="express_interest(<?php echo $auction_id?>,<?php echo $auction_details['auction_type'];?>,<?php echo  ( ! empty($this->session->userdata('bidder_id'))) ?$this->session->userdata('bidder_id'):'0'?>,<?php echo $auction_details['vendor_id'];?>,<?php echo $added_via?>)" ><i class=" "></i> Express Interest </a> </div>

          </div>
          <!--/.ads-details-wrapper--> 
          
        </div>
        </div>
        <!--/.page-content-->
        
        <div class="col-sm-3  page-sidebar-right">
          <aside>
            <div class="panel sidebar-panel panel-contact-seller">
              <div class="panel-heading" style="color:black">Contact Vendor</div>
              <div class="panel-content user-info">
                <div class="panel-body text-center">
                <div class="seller-info">
                    <h3 class="no-margin"><?php echo $auction_details['vendor_name']?></h3>
                    <p>Location: <strong><?php echo $auction_details['name']?></strong></p>
                  </div>

                  <div class="user-ads-action"> 
                      <a class="btn  btn-default btn-block" data-toggle="modal" href="#contactAdvertiser"><i class=" icon-mail-2"></i> Send a message </a>
                      <?php
                        if ( empty($fav_product))
                        {
                      ?>
                       <a href="#"  class="btn btn-info btn-block fav" onclick="add_favourite(<?php echo $auction_details['ynk_auctionid']?>);"><i class=" fa fa-heart"></i> Add In Favourite </a> 
                      <?php

                        }
                        else
                        {
                        ?>
                          <a href="#"  class="btn   btn-default btn-block fav" ><i class=" fa fa-heart"></i> Added In Favourite</a> 
                        <?php
                        }
                      ?>
                  </div>
                </div>
              </div>
            </div> 
           
           
            <!--/.categories-list--> 
          </aside>
        </div>
        <!--/.page-side-bar--> 
     
    </div>
  </div>
  <!-- /.main-container -->
  
  
  
	<?php $this->load->view(ciHtmlTheme.'/includes/call_back');?>
  <!-- /.footer --> 
  <?php $this->load->view(ciHtmlTheme.'/includes/footer');?>
 
</div>
	
<!-- /.wrapper --> 
<!-- Modal -->
<div class="modal fade" id="lotModel" role="dialog">
			<div style="display:flex; align-items:center;">
        <div  class="modal-dialog">
            <!-- Modal content-->
          
            <div class="modal-content" style="width:766px;">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Lot Details</h4>
              </div>
              <div class="modal-body">
                <div class="row" id="lot">
                  
                </div>  
              </div>
              <div class="modal-footer">
              </div>
            </div>
          </div>
        </div>
		</div>
  </div>
      <!-- The Modal -->
      
<!-- Modal contactAdvertiser -->

<div class="modal fade" id="contactAdvertiser" tabindex="-1" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title"><i class=" icon-mail-2"></i> Contact advertiser </h4>
      </div>
      <form role="form" id="send_message" name="send_message" method="POST">
      <div class="modal-body">
        
          <div class="form-group">
            <label for="recipient-name" class="control-label">Name:</label>
            <input class="form-control required" id="recipient-name" name="name" placeholder="Your name" data-placement="top" data-trigger="manual" data-content="Must be at least 3 characters long, and must only contain letters." type="text">
          </div>
          <div class="form-group">
            <label for="sender-email" class="control-label">E-mail:</label>
            <input id="sender-email" name="email" type="text" data-content="Must be a valid e-mail address (user@gmail.com)" data-trigger="manual" data-placement="top" placeholder="email@you.com" class="form-control email">
          </div>
          <div class="form-group">
            <label for="recipient-Phone-Number"  class="control-label">Phone Number:</label>
            <input type="text"  maxlength="60" class="form-control" id="recipient-Phone-Number" name="phone">
          </div>
          <div class="form-group">
            <label for="message-text" class="control-label">Message <span class="text-count">(300) </span>:</label>
            <textarea class="form-control" id="message-text" name="message"  placeholder="Your message here.." data-placement="top" data-trigger="manual"></textarea>
          </div>
          <div class="form-group">
            <p class="help-block pull-left text-danger hide" id="form-error">&nbsp; The form is not valid. </p>
          </div>
          <input type="hidden" name="product_id" id="product_id" value="<?php echo $auction_details['ynk_auctionid']?>"/>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
        <button type="submit" class="btn btn-success pull-right" onclick="send_msg()">Send message!</button>
      </div>
      </form>
    </div>
  </div>
</div>

<!-- /.modal -->
<!-- Le javascript
================================================== --> 
<!-- Placed at the end of the document so the pages load faster --> 


<!-- bxSlider Javascript file --> 
<!-- Placed at the end of the document so the pages load faster --> 
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.10.1/jquery.min.js"> </script>
<script src="<?php echo base_url(ciHtmlTheme.'/assets/bootstrap/js/bootstrap.min.js')?>"></script>

<!-- include carousel slider plugin  --> 
<script src="<?php echo base_url(ciHtmlTheme.'/assets/js/owl.carousel.min.js')?>"></script> 

<!-- include equal height plugin  --> 
<script src="<?php echo base_url(ciHtmlTheme.'/assets/js/jquery.matchHeight-min.js')?>"></script> 

<!-- include jquery list shorting plugin plugin  --> 
<script src="<?php echo base_url(ciHtmlTheme.'/assets/js/hideMaxListItem.js')?>"></script> 

<!-- bxSlider Javascript file --> 
<script src="<?php echo base_url(ciHtmlTheme.'/assets/plugins/bxslider/jquery.bxslider.min.js')?>"></script> 
<script src="<?php echo base_url(ciHtmlTheme.'/assets/js/jquery.validate.min.js')?>"></script>
<script src="<?php echo base_url(ciHtmlTheme.'/assets/js/jquery.form.min.js')?>"></script>
<script src="<?php echo base_url(ciHtmlTheme.'/assets/js/mdtimepicker.js'); ?>"></script>


<script>
$('.bxslider').bxSlider({
  pagerCustom: '#bx-pager'
});
$("#checkAll").click(function(){
    $('input:checkbox').not(this).prop('checked', this.checked);
});

function add_favourite(product_id)
{
    var bidder_id = '<?php echo $this->session->userdata('bidder_id')?>';
    if (bidder_id)
    {
       $.ajax({
            url:'<?php echo base_url()?>auction/add_products_in_favourite',
            type:'post',
            dataType:'json',
            data:{'bidder_id':bidder_id,'product_id':product_id,type:'A'},
            success:function(resObj)
            {
              if (resObj.status ='success')
              {
                $(".fav").html('<i class=" fa fa-heart"></i> Added In Favourite');
                
              }
              else
              {
                alert(resObj.msg);
              }
                
            }
        }); 
    }
    else
    {
        alert('Please Login');
    }
}

function send_msg()
{
  var vRules 	= 	{
                    name:{required:true},
                    email: {required : true},
                    phone: {required : true},
                    message:{required : true}
            };
        var vMessages = {
                    name:{required:"Please Enter Name"},
                    email:{required:"Please Enter Email"},
                    phone:{required:"Please Enter Phone"},
                    message:{required:"Please Enter Message"},

                        };
        $("#send_message").validate({
            rules: vRules,
            messages: vMessages,
            submitHandler: function(form){
                $(form).ajaxSubmit({
                    url: '<?php echo base_url();?>auction/send_message',
                    type:"post",
                    dataType: 'json',
                    success: function (resObj) {
                       alert(resObj.message);
                       $("#contactAdvertiser").modal('hide');
                    },
                    error:function()
                    {
                        alert('Invalid Request');
                    }
                });
            }
        });
           
}
function lot_details(lot_id)
{

   $.ajax({
      url:'<?php echo base_url('auction/auction_lot_details')?>',
      type:'POST',
      data:{'auctionid':<?php echo $auction_id?>,'lot_id':lot_id},
      dataType:'json',
      success:function(response)
      {
          var html ='';
          html+='<div class="container">'+
                  '<div class="row">'+
                    '<div class="col-sm-9 page-content col-thin-right">'+
                      '<div class="inner inner-box ads-details-wrapper">'+
                        '<h2> '+response.product+' <small class="label label-default adlistingtype"></small> </h2>'+
                        '<span class="info-row"> <span class="date"><i class=" icon-clock"> </i> '+response.lot_open_date+' To '+response.lot_close_date+'</span> - <span class="item-location"><i class="fa fa-map-marker"></i> '+response.plant+' </span> </span>';
                        if (response.auc_img.length>0)
                        {
                          html+='<div class="ads-image">'+
                            '<ul class="bxslider">';
                          $.each(response.auc_img, function(i,v)
                          {
                              html+='<li><img src="<?php echo AMAZON_BUCKET?>/<?php echo s3BucketTestDir?>images/lotpic/yankee/'+v.img_name+'" alt="img" /></li>';
                           
                          });
                          html+= '</ul>'+
                          '</div>';
                        }
                        
                        html+='<div class="Ads-Details">'+
                          '<h5 class="list-title"><strong>Lot Details</strong></h5>'+
                          '<div class="row">'+
                            '<div class="ads-details-info col-md-8">'+
                              '<p>'+response.product_details+'. </p>'+
                              '<h4>Description</h4>'+
                              '<ul class="list-circle">'+
                                '<li> Lot Open Date Time :'+response.lot_open_date+'</li>'+
                                '<li> Lot Close Date Time :'+response.lot_close_date+'</li>'+
                                '<li> Total Qty :'+response.totalqty+' '+response.totunit+'</li>'+
                                '<li> Bid Qty :'+response.bidqty+ ' '+response.bidunit+'</li>'+
                                '<li> Lot Currency :'+response.currency+'</li>';
                                if (response.part_bid ==='Y')
                                {
                                  html+='<li> Part Bid : Yes </li>'+
                                  '<li> Part Bid Qty :'+response.min_part_bid+'</li>';

                                }
                              html+='</ul>'+
                            '</div>'+
                            '<div class="col-md-4">'+
                              '<aside class="panel panel-body panel-details">'+
                                '<ul>'+
                                  '<li>'+
                                    '<p class=" no-margin "><strong>CMD/EMD:</strong> '+response.cmd+'</p>'+
                                  '</li>'+
                                  '<li>'+
                                    '<p class="no-margin"><strong>Type:</strong> <?php echo $auction_details['auctiontype']?></p>'+
                                  '</li>'+
                                  '<li>'+
                                    '<p class="no-margin"><strong>Location:</strong> '+response.plant+' </p>'+
                                  '</li>'+
                                '</ul>'+
                              '</aside>'+
                            '</div>'+
                          '</div>'+
                        '</div>'+
                      '</div>'+
                    
                    '</div>'+
                  
                 '</div>'+
                '</div>'+
              '</div>';
             
              $("#lot").html(html);
             
              $("#lotModel").modal('show');
              
             
              
              

      }
   });
}
var bx;
$('#lotModel').on('shown.bs.modal', function () {
  if(bx === undefined){
    bx= $('.bxslider').bxSlider();
  } else {
    bx.reloadSlider(); 
  }
});

function express_interest(auction_id,auc_type,bidder_id='',vendor_id,added_via)
{
  if(bidder_id)
  {
    var lots = '';
    var checkedNum = $('input[name="lot[]"]:checked').length;
    if (!checkedNum) {
      alert('Please Select lot');
    }
    else
    {
      var vals = "";
      $.each($("input[name='lot[]']:checked"), function(){  
        vals += $(this).val()+',';  
      });
      if (vals)
      {
        $.ajax({
            url: '<?php echo base_url('auction/express_interest')?>',
            type: 'POST',
            data:{bidder_id :bidder_id,auction_id:auction_id,auction_type:auc_type,vendor_id:vendor_id,lot_id:vals,added_via:added_via},
            dataType:'json',
            success: function(response){
            
              alert(response.message);
              window.location.reload();
            
            },
            error :function(){
              alert('Invalid Request');
            }
        });
      }
    }
    
  }
  else
  {
    alert('Please Login As Bidder');
  }
}
</script> 
<!-- include jquery autocomplete plugin  -->

</body>
</html>
