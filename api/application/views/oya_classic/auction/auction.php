<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<!-- Fav and touch icons -->
<link rel="apple-touch-icon-precomposed" sizes="144x144" href="<?php echo base_url(ciHtmlTheme.'/assets/ico/apple-touch-icon-144.png')?>">
<link rel="apple-touch-icon-precomposed" sizes="114x114" href="<?php echo base_url(ciHtmlTheme.'/assets/ico/apple-touch-icon-114.png')?>">
<link rel="apple-touch-icon-precomposed" sizes="72x72" href="<?php echo base_url(ciHtmlTheme.'/assets/ico/apple-touch-icon-72.png')?>">
<link rel="apple-touch-icon-precomposed" sizes="57x57" href="<?php echo base_url(ciHtmlTheme.'/assets/ico/apple-touch-icon-57.png')?>">
<link rel="shortcut icon" href="<?php echo base_url(ciHtmlTheme.'/assets/ico/favicon.png')?>">
<title>Auctions Listing</title>
<!-- Bootstrap core CSS -->
<link href="<?php echo base_url(ciHtmlTheme.'/assets/bootstrap/css/bootstrap.min.css')?>" rel="stylesheet">
<!-- Custom styles for this template -->
<link href="<?php echo base_url(ciHtmlTheme.'/assets/css/style.css')?>" rel="stylesheet">
<link href="<?php echo base_url(ciHtmlTheme.'/assets/css/custom.css')?>" rel="stylesheet">
<!-- styles needed for carousel slider -->
<link href="<?php echo base_url(ciHtmlTheme.'/assets/css/owl.carousel.css')?>" rel="stylesheet">
<link href="<?php echo base_url(ciHtmlTheme.'/assets/css/owl.theme.css')?>" rel="stylesheet">
<link href="<?php echo base_url(ciHtmlTheme.'/assets/css/niceCountryInput.css')?>" rel="stylesheet">
<link href="<?php echo base_url(ciHtmlTheme.'/assets/css/mdtimepicker.css'); ?>" rel="stylesheet" />



<script>
    paceOptions = {
      elements: true
    };
</script>
<script src="<?php echo base_url(ciHtmlTheme.'/assets/js/pace.min.js')?>"></script>
<style>
  
 </style>
 </head>
<body>

<div id="wrapper">
<!-- /.header -->
 <?php $this->load->view(ciHtmlTheme.'/includes/header');?>
  <!-- /.header -->
  
  <div class="intro">
    <div class="dtable hw100">
      <div class="dtable-cell hw100">
        <div class="container text-center">
       <h1 class="intro-title animated fadeInDown"> Find Auctions  </h1>
          <p class="sub animateme fittext3 animated fadeIn"> Welcome to world of Auctions </p>
      
            <div class="row search-row animated fadeInUp">
              <div class="col-lg-4 col-sm-4 search-col relative looking_for"> <i class="icon-docs icon-append"></i>
                <select style='' class="form-control selecter" name="category" id="search-category" >
                    <option value="" style="background-color:#E9E9E9;font-weight:bold;"><a href="<?php echo base_url('auction')?>"> Auction </a></option>
                    <option value="" style="background-color:#E9E9E9;font-weight:bold;"><a href="<?php echo base_url('products')?>"> Products </a></option>
                </select>
              </div>
              <div class="col-lg-4 col-sm-4 search-col relative locationicon">
               <i class=""></i>
                <input type="text" name="country" id="multiple_search"  class="form-control locinput input-rel searchtag-input has-icon" placeholder="Search..." value="">

              </div>
              <div class="col-lg-4 col-sm-4 search-col">
                <button class="btn btn-primary btn-search btn-block" onclick="load_more('first')"><i class="icon-search"></i><strong>Find</strong></button>
              </div>
            </div>
        </div>
      </div>
    </div>
  </div>
  <!-- /.intro -->
  
  <div class="main-container">
    <div class="container-fluid">
      <div class="row">
        <div id='sider' class="col-sm-2 page-sidebar">
         <aside>
            <div class="inner-box">
              <div class="categories-list  list-filter">
                <h5 class="list-title"><strong><a href="#">All Categories</a></strong></h5>
                <ul class=" list-unstyled">
                <?php
               
                    if ( ! empty($auction_category))
                    {
                        foreach($auction_category as $key=>$cat)
                        {
                          $cnt = 0;
                            if ( ! empty($cat))
                            {
                              $category = array();
                              foreach($cat as $k=>$v)
                              {
                                  $total = $cnt+$v['mat_total'];

                                  $cnt = $total;
                                  $cat_id = $v['cat_id'];
                                  if ( ! empty($categories))
                                  {
                                    $category = explode('_',$categories);
                                  }
                              }
                            }
                ?>
                             <li><input type="checkbox" class="cat" name="cat[]" value="<?php echo $key?>" onclick="change_category(<?php echo $cat_id?>)"  <?php echo (in_array($key, $category))?'checked':'';?>/> 
                             <span class="title"> <?php echo ucfirst($key)?> </span>&nbsp;<span class="badge badge-success pull-right"><?php echo $total?></span> </li>
                <?php
                        }
                    }
                ?>
                </ul>
              </div>
              <!--/.categories-list-->
              
              <div class="locations-list  list-filter">
                <h5 class="list-title"><strong><a href="#">Location</a></strong></h5>
                <ul class="browse-list list-unstyled long-list">
                  <?php 
                  
                        if ( ! empty($auction_location))
                        {
                          $location = array();
                            foreach($auction_location as $key=>$loc)
                            {
                              if ( ! empty($country))
                              {
                                $location = explode('_',$country);
                              } 
                             
                    ?>

                                <li><input type="checkbox" name="loc[]" class ='ctr' value="<?php echo $loc['country']?>" onclick="change_location('<?php echo $loc['country']?>')" <?php echo (in_array($loc['country'], $location))?'checked':'';?>/>  
                                <span class="title"><?php echo ucfirst($key)?></span>&nbsp;<span class="badge badge-success pull-right"><?php echo $loc['count']?></span></li>
                    <?php
                              
                            }
                        }
                  ?>
                  
                </ul>
              </div>
              <!--/.locations-list-->
              <div class="locations-list  list-filter">
                <h5 class="list-title"><strong><a href="#">Auction Type</a></strong></h5>
                <ul class="browse-list list-unstyled long-list">
                <?php
                  $type = array();
                  if ( ! empty($auc_type))
                  {
                    
                    $type = explode('_',$auc_type);
                  }
                ?>
                <li> <input type="checkbox" name="auc_type[]" class='auc' value="Forward Auction" onclick="change_auc_type('Forward Auction');" <?php echo (in_array('Forward Auction',$type))?'checked':'';?>/> <span class="title"><?php echo ! empty($auction_type['Forward Auction'])?'Forward Auction':'Forward Auction'?></span>&nbsp;<span class="badge badge-success pull-right"><?php echo ! empty($auction_type['Forward Auction'])?$auction_type['Forward Auction']:'0'?></span></li>
                <li> <input type="checkbox" name="auc_type[]" class='auc' value="Reverse Auction" onclick="change_auc_type('Reverse Auction');" <?php echo (in_array('Reverse Auction',$type))?'checked':'';?>/>  <span class="title"><?php echo ! empty($auction_type['Reverse Auction'])?'Reverse Auction':'Reverse Auction'?></span>&nbsp;<span class="badge badge-success pull-right"><?php echo ! empty($auction_type['Reverse Auction'])?$auction_type['Reverse Auction']:'0'?></span></li>
                <li> <input type="checkbox" name="auc_type[]" class='auc' value="Yankee Auction(Forward)" onclick="change_auc_type('Yankee Auction(Forward)');" <?php echo (in_array('Yankee Auction(Forward)',$type))?'checked':'';?>>  <span class="title"><?php echo ! empty($auction_type['Yankee Auction(Forward)'])?'Yankee Auction(Forward)':'Yankee Auction(Forward)'?></span>&nbsp;<span class="badge badge-success pull-right"><?php echo ! empty($auction_type['Yankee Auction(Forward)'])?$auction_type['Yankee Auction(Forward)']:'0'?></span></li>
                <li> <input type="checkbox" name="auc_type[]" class='auc' value="Yankee Auction(Reverse)" onclick="change_auc_type('Yankee Auction(Reverse)');" <?php echo (in_array('Yankee Auction(Reverse)',$type))?'checked':'';?>>  <span class="title"><?php echo ! empty($auction_type['Yankee Auction(Reverse)'])?'Yankee Auction(Reverse)':'Yankee Auction(Reverse)'?></span>&nbsp;<span class="badge badge-success pull-right"><?php echo ! empty($auction_type['Yankee Auction(Reverse)'])?$auction_type['Yankee Auction(Reverse)']:'0'?></span></li>
                <li> <input type="checkbox" name="auc_type[]" class='auc' value="Tender" onclick="change_auc_type('Tender');" <?php echo (in_array('Tender',$type))?'checked':'';?>>  <span class="title"><?php echo ! empty($auction_type['Tender'])?'Tender':'Tender'?></span>&nbsp;<span class="badge badge-success pull-right"><?php echo ! empty($auction_type['Tender'])?$auction_type['Tender']:'0'?></a></span></li>
                
                  <?php 
                  
                        if ( ! empty($auction_type))
                        {
                            foreach($auction_type as $key=>$auc)
                            {
                             
                    ?>

                    <?php
                              
                            }
                        }
                  ?>
                  
                </ul>
              </div>
             
              <!--/.list-filter-->
              <div class="locations-list  list-filter">
                <h5 class="list-title"><strong><a href="#">Material Type</a></strong></h5>
                <ul class="browse-list list-unstyled long-list">
                 <?php 
                    if ( ! empty($auction_material))
                    {
                        foreach($auction_material as $key=>$mat)
                        {
                          $material = array();
                          if ( ! empty($mat_id))
                          {
                            $material = explode('_',$mat_id);
                          }
                    ?>
                         <li> <input type="checkbox" name="mat[]" class="mat" onclick="change_mat(<?php echo $mat['mat_id']?>)" value="<?php echo $key?>" <?php echo (in_array($key,$material))?'checked':'';?>/> <?php echo $key?> &nbsp;<span class="badge badge-success pull-right"><?php echo $mat['count']?></span></li>    
                    <?php
                        }
                    }
                 ?>
                </ul>
              </div>

              <div class="locations-list  list-filter">
                <h5 class="list-title"><strong><a href="#">Vendor</a></strong></h5>
                <ul class="browse-list list-unstyled long-list">
                 <?php 
                    if ( ! empty($auction_vendor))
                    {
                      $vendor_arr = array();
                      if ( ! empty($vendor_id))
                      {
                        $vendor_arr = explode('_', $vendor_id);

                      }
                        foreach($auction_vendor as $key=>$vendor)
                        {
                         
                    ?>
                         <li> <input type="checkbox" name="vendor[]" class="ven" value="<?php echo $vendor['vendor_name']?>" onclick="change_vendor(<?php echo $vendor['vendor_id']?>)" <?php echo (in_array($vendor['vendor_name'],$vendor_arr))?'checked':'';?>/>  <?php echo $vendor['vendor_name']?> &nbsp;<span class="badge badge-success pull-right"><?php echo $vendor['cnt']?></span></li>    
                    <?php
                        }
                    }
                 ?>
                </ul>
              </div>
              <!--/.list-filter-->
             
              <!--/.list-filter-->
              <div style="clear:both"></div>
            </div>
            
            <!--/.categories-list--> 
          </aside>
        </div>
        <!--/.page-sidebar-->
        
        <div class="col-sm-10 page-content">
         
          
          <div class="category-list">
            <div class="tab-box "> 
              
              <!-- Nav tabs -->
              <ul class="nav nav-tabs add-tabs" role="tablist">
                <li class="active"><a href="#allAds" role="tab" data-toggle="tab">All Auction <span class="badge"><?php echo $auction_count;?></span></a></li>
              </ul>
              <div class="tab-filter">
                <select class="selectpicker" data-style="btn-select" data-width="auto" id="range" onchange="load_more('first');">
                  <option value=''>Sort by</option>
                  <option value="asc">CMD: Low to High</option>
                  <option value="desc">CMD: High to Low</option>
                </select>
              </div>
            </div>
            <!--/.tab-box-->
            
            <div class="listing-filter">
              <div class="pull-left col-xs-6">
               <div class="breadcrumb-list"> <a href="#" class="current"> <span>All Auction</span></a> in Location <a href="#selectRegion" id="dropdownMenu1"  data-toggle="modal"> <span class="caret"></span> </a> </div>
              </div>
                <div class="pull-right col-xs-6 text-right grid-view-action"> 
                    <!-- <span class="grid-view active"><i class=" icon-th-large "></i></span>  -->
                    <!-- <span class="list-view "><i class="  icon-th"></i></span>
                    <span class="compact-view"><i class=" icon-th-list  "></i></span>  -->
                </div>
              <div style="clear:both"></div>
            </div>

            <div class="adds-wrapper " id="parent">

            </div>


            <!--/.listing-filter-->
            
             <!--/.adds-wrapper-->
            <input type="hidden" id="row" value="0">
            <input type="hidden" id="all" value="<?php echo $auction_count?>">
            <input type="hidden" id="category_id" value="<?php echo $categories?>">
            <input type="hidden" id="country" value="<?php echo $country?>">
            <input type="hidden" id="auc_type" value="<?php echo $auc_type?>">
            <input type="hidden" id="mat_id" value="<?php echo $mat_id?>">
            <input type="hidden" id="vendor_id" value="<?php echo $vendor_id?>">
            <input type="hidden" id="saleno" value="<?php echo $saleno?>">

            

            <div class="col-sm-6">
               <button type="button" class="btn col-sm-3 pull-right load-more" style="background:#004DDA;color:white;margin-top:10px;" onclick="load_more();">  Load More.. </button>
            </div>
          </div>
         
       
  
        </div>
        <!--/.page-content--> 
      </div>
      <!--/.row--> 
    </div> 
  </div>
  <!-- /.main-container -->
  
  <!-- /.counts -->
	<?php $this->load->view(ciHtmlTheme.'/includes/counts');?>
  <!-- /.claa me  -->
	<?php $this->load->view(ciHtmlTheme.'/includes/call_back');?>
  <!-- /.footer --> 
  <?php $this->load->view(ciHtmlTheme.'/includes/footer');?>
 
</div>
<!-- /.wrapper --> 
<div class="modal fade" id="selectRegion" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title" id="exampleModalLabel"><i class=" icon-map"></i> Select your region </h4>
      </div>
      <div class="modal-body">
        <div class="row">
          <div class="col-sm-12">
        
            <p>Popular cities in <strong id="country_name">India</strong>
            </p>

            
            <div class="niceCountryInputSelector" id='flag_name' style="width: 300px;" data-selectedcountry="IN" data-showspecial="false" data-showflags="true" data-i18nall="All selected"
            data-i18nnofilter="No selection" data-i18nfilter="Filter" data-onchangecallback="onChangeCallback" />          

            <hr class="hr-thin">
          </div>
          
        </div>
      </div>
    </div>
  </div>
</div>
<!-- Le javascript
================================================== --> 
<!-- Placed at the end of the document so the pages load faster --> 
<script src="<?php echo base_url(ciHtmlTheme.'/assets/js/jquery.min.js')?>"> </script>
<script src="<?php echo base_url(ciHtmlTheme.'/assets/bootstrap/js/bootstrap.min.js')?>"></script> 
<!-- include carousel slider plugin  --> 
<script src="<?php echo base_url(ciHtmlTheme.'/assets/js/owl.carousel.min.js')?>"></script> 
<!-- include equal height plugin  --> 
<script src="<?php echo base_url(ciHtmlTheme.'/assets/js/jquery.matchHeight-min.js')?>"></script> 
<!-- include jquery list shorting plugin plugin  --> 
<script src="<?php echo base_url(ciHtmlTheme.'/assets/js/hideMaxListItem.js')?>"></script> 
<!-- include jquery.fs plugin for custom scroller and selecter  --> 
<script src="<?php echo base_url(ciHtmlTheme.'/assets/plugins/jquery.fs.scroller/jquery.fs.scroller.js')?>"></script>
<script src="<?php echo base_url(ciHtmlTheme.'/assets/plugins/jquery.fs.selecter/jquery.fs.selecter.js')?>"></script>
<!-- include custom script for site  --> 
<script src="<?php echo base_url(ciHtmlTheme.'/assets/js/script.js')?>"></script>
<!-- include jquery autocomplete plugin  -->
<script src="<?php echo base_url(ciHtmlTheme.'/assets/js/jquery.validate.min.js')?>"></script>
<script src="<?php echo base_url(ciHtmlTheme.'/assets/js/jquery.form.min.js')?>"></script>

<script src="<?php echo base_url(ciHtmlTheme.'/assets/js/niceCountryInput.js')?>"></script>
<script src="<?php echo base_url(ciHtmlTheme.'/assets/js/mdtimepicker.js'); ?>"></script>

<script>
$(document).ready(function () {
  $(".niceCountryInputSelector").each(function(i,e){
      new NiceCountryInput(e).init();
      load_more('first');
  });
});

function change_loc(country_code)
{
  clear_search_filter();
  $("#country").val(country_code);
  load_more('first');
  
}
function change_mat(mat_id)
{
  var term="";
	$('.mat').each(function() {if($(this).is(':checked')) {
    term =term;
    if (term)
    {
      term+="_";
    }
    term+=$(this).val();
  }});
  $('#mat_id').val(term);
  submit_url();
}
function change_vendor(vendor_id)
{
  
  var term="";
  $('.ven').each(function() {if($(this).is(':checked')) {
    term =term;
    if (term)
    {
      term+="_";
    }
    term+=$(this).val();
  }});
  $("#vendor_id").val(term);
  submit_url();
}
function change_auc_type(auc_type)
{
  var term="";
  $('.auc').each(function() {if($(this).is(':checked')) {
    term =term;
    if (term)
    {
      term+="_";
    }
    term+=$(this).val();
  }});
  $("#auc_type").val(term);
  submit_url();
}
function onChangeCallback(ctr){
    clear_search_filter();
    $("#country").val(ctr);
    load_more('first');
    
}
function change_category(category_id)
{
  var term="";
  $('.cat').each(function() {if($(this).is(':checked')) {
    term =term;
    if (term)
    {
      term+="_";
    }
    term+=$(this).val();
  }});
  $("#category_id").val(term);
  submit_url();
}
function change_location(country_code)
{
  var term="";
  $('.ctr').each(function() {if($(this).is(':checked')) {
    term =term;
    if (term)
    {
      term+="_";
    }
    term+=$(this).val();
  }});
  
  $("#country").val(term);
  submit_url();
  
}
function submit_url()
{
  var url = '<?php echo base_url('auction')?>?a=1';
  if($("#category_id").val()!="") url=url+"&cat="+$("#category_id").val();
  if($("#country").val()!="") url=url+"&ctr="+$("#country").val();
  if($("#auc_type").val()!="") url=url+"&auc_type="+$("#auc_type").val();
  if($("#mat_id").val()!="") url=url+"&mat="+$("#mat_id").val();
  if($("#vendor_id").val()!="") url=url+"&vendor="+$("#vendor_id").val();
  window.history.pushState({path:url},'',url);
  load_more('first');

	
}
   // Load more data
function clear_search_filter()
{
   $("#country").val('');
   $("#range").val('');
   $("#vendor_id").val('');
   $("#mat_cat_id").val('');
   $("#multiple_search").val('');
   $("#category_id").val('');
   $("#auc_type").val('');
   $("#mat_id").val('');
}
   var i=0;
function load_more(record='')
{
    $(".load-more").show();
    var country = $("#country").val();
    var range = $("#range").val();
    var vendor_id = $("#vendor_id").val();
    var mat_cat_id = $("#mat_cat_id").val();
    var multiple_search = $("#multiple_search").val();
    var category_id = $("#category_id").val();
    var auc_type = $("#auc_type").val();
    var mat_id = $("#mat_id").val();
    var saleno = $("#saleno").val();

    if (record)
    {
      $("#row").val(0);
      $('.adds-wrapper').html('');
    }
    var row = Number($('#row').val());
    var allcount = Number($('#all').val());
    var rowperpage = 8;
   
    if(row <= allcount){
      
        $.ajax({
            url: '<?php echo base_url('auction/auction_listing')?>',
            type: 'get',
            dataType:'json',
            data:{'start':row,'limit':rowperpage,'range':range,'country':country,'mat_cat_id':mat_cat_id,'multiple_search':multiple_search,'category_id':category_id,
            'auc_type':auc_type,'mat_id':mat_id,'vendor_id':vendor_id,'saleno':saleno},
            beforeSend:function(){
                $(".load-more").text("Loading...");
            },
            success: function(response){

                // Setting little delay while displaying new content
                setTimeout(function() {
                    // appending posts after last post with class="post"
                    if (response)
                    {
                     
                      var html='';
                        var c =0;
                        $.each(response['data'],function(i,v)
                        {
                          i++;
                          allcount = response.count;
                          if ( ! Number.isNaN(i))
                          {
                           
                              html+='<div class="item-list make-grid" id='+v.saleno+'>'+
                                      '<ul class="product-labels">';
                                        if(v.auction_type == 4){
                                            var title = 'Auction Saleno';
                                        }
                                        else{
                                          var title = 'Tender Saleno';
                                        }
                                        html+='<li>'+title+' : '+v.saleno+' </li>'+
                                      '</ul>'+
                                      '<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 no-padding">'+

                                          '<div id="myCarousel'+c+'" class="carousel slide" data-ride="carousel">';
                        
                            
                                    if (v.auc_img)
                                    {
                                        var images = v.auc_img.split(',');
                                        var i = 0;
                                        var j = 0;
                                        var count = images.length;
                                        var last_element = count -1; 
                                        var class_name='';
                                        var path_class ='';
                                        var path;
                                        html+='<span class="photo-count"><i class="fa fa-camera"></i>'+count+'</span>'+
                                                  '<ol class="carousel-indicators">';

                                          $.each(images,function(k,img)
                                          {
                                        
                                            if (i == last_element)
                                            {
                                              class_name = 'active';
                                            }
                                            else
                                            {
                                              class_name == '';
                                            }
                              
                                            html+='<li data-target="#myCarousel'+c+'" data-slide-to="'+i+'" class="'+class_name+'"></li>';
                              
                                            i++;
                                        });
                                        html+='</ol>'+
                                        '<div class="carousel-inner">';
                                        $.each(images,function(index,img_path)
                                        {
                                          
                                          
                                            if (j == last_element)
                                            {
                                                path_class = "active";
                                            }
                                            else
                                            {
                                                path_class= "";
                                            }
                                            if (v.auction_type == 4)
                                            {
                                                 path = '<?php echo AMAZON_BUCKET?>/<?php echo s3BucketTestDir?>images/auctionpic/yankee/'+img_path;
                                            }
                                            else
                                            {
                                               path = '<?php echo AMAZON_BUCKET?>/<?php echo s3BucketTestDir?>images/auctionpic/tender/'+img_path;
                                            }
                                  
                                        html+='<div class="slider_img item '+path_class+'">'+
                                            '<img src="'+path+'">'+
                                        '</div>';
                                  
                                        j++;
                                        });
                              
                                      html+='</div>'+
                                      '<a class="left carousel-control" href="#myCarousel'+c+'" data-slide="prev">'+
                                          '<span class="glyphicon glyphicon-chevron-left"></span><span class="sr-only">Previous</span>'+
                                      '</a>'+
                                      '<a class="right carousel-control" href="#myCarousel'+c+'" data-slide="next">'+
                                          '<span class="glyphicon glyphicon-chevron-right"></span><span class="sr-only">Next</span>'+
                                      '</a>';
                        
                                        
                                    }
                                    else
                                    {
                                  
                                      html+='<span class="photo-count"><i class="fa fa-camera"></i> 2 </span>'+ 
                              
                                        '<ol class="carousel-indicators">'+
                                          '<li data-target="#myCarousel'+c+'" data-slide-to="0" class=""></li>'+
                                          '<li data-target="#myCarousel'+c+'" data-slide-to="1" class=""></li>'+
                                          '<li data-target="#myCarousel'+c+'" data-slide-to="2" class="active"></li>'+
                                      '</ol>'+
                                      '<div class="carousel-inner">'+
                                          '<div class="slider_img item">'+
                                              '<img src="https://img1.exportersindia.com/product_images/bc-full/dir_133/3984152/gun-metal-scrap-2339918.jpeg">'+
                                          '</div>'+
                                          '<div class="slider_img item">'+
                                              '<img src="https://img1.exportersindia.com/product_images/bc-full/dir_133/3984152/gun-metal-scrap-2339918.jpeg">'+
                                          '</div>'+
                                          '<div class="slider_img item active">'+
                                              '<img src="https://img1.exportersindia.com/product_images/bc-full/dir_133/3984152/gun-metal-scrap-2339918.jpeg">'+
                                          '</div>'+
                                      '</div>'+
                                      '<a class="left carousel-control" href="#myCarousel'+c+'" data-slide="prev">'+
                                          '<span class="glyphicon glyphicon-chevron-left"></span><span class="sr-only">Previous</span>'+
                                      '</a>'+
                                      '<a class="right carousel-control" href="#myCarousel'+c+'" data-slide="next">'+
                                          '<span class="glyphicon glyphicon-chevron-right"></span><span class="sr-only">Next</span>'+
                                      '</a>';
                                
                                    }
                          
                        html+='</div>'+
                    '</div>'+

                    

                    '<div class="row mt-10">'+
                        '<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 ">'+
                            '<h5 class="add-details"> <a href="#"> </a> </h5>'+
                        '</div>'+
                    '</div>'+

                    '<div class="row mt-10">'+
                            '<div class="col-lg-2 col-md-2 col-sm-2 col-xs-2">'+
                                    '<span class="add-details"> <a href="#"> FOR: '+v.vendor_name+'</a> </span>'+
                            '</div>'+
                            '<div class="col-lg-10 col-md-10 col-sm-10 col-xs-10 paddingL5" style="overflow:hidden">'+
                                '<span class="text-upper pointer" title="vendor_name" style="white-space:nowrap"></span>'+
                            '</div>'+
                    '</div>'+

                    '<div class="row mt-10">'+
                        '<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">'+
                            '<span class="start_time_icon"><i class="fa fa-hourglass-start" aria-hidden="true"></i></span> <span class="time_label">'+v.start_date+' '+v.stime+'</span>'+
                        '</div>'+
                        '<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">'+
                            '<span class="end_time_icon"><i class="fa fa-hourglass-end" aria-hidden="true"></i></span><span class="time_label">'+v.end_date+' '+v.etime+'</span>'+
                        '</div>'+
                    '</div>'+

                    '<div class="row mt-10">'+
                        '<div class="col-lg-8 col-xs-8 ">';
                            if(v.auction_type == 4){
                              html+='<a href="<?php echo base_url('auction/view_details/')?>'+v.encrypt_saleno+'"><button type="button" class="btn btn-primary">View Details</button></a>';
                            }else{
                              html+='<a href="<?php echo base_url('tender/view_details/')?>'+v.encrypt_saleno+'"><button type="button" class="btn btn-primary">View Details</button></a>';
                            }
                        html+='</div>';
                        if(v['status'] == 1)
                        {
                          html+='<div class="col-lg-4 col-xs-4">'+
                            '<button type="button" class="live-btn"><i class="fa fa-certificate"></i> <span>Live</span></button>'+
                        '</div>';
                        }
                        
                    html+='</div>'+
                '</div></div>';
                      }
                      c++;
                        });
                      
                      
                    }
 
                    if (row > 0)
                    {
                      
                      $(".item-list:last").after(html).show().fadeIn("slow");
                      
                    
                    }else{
                     
                      $(".adds-wrapper").html(html).show().fadeIn("slow");
                    } 
                   

                    var rowno = row + rowperpage;
                  
                    $("#row").val(rowno);
                    
                   
                    // checking row value is greater than allcount or not
                   
                    if(rowno >= allcount){

                        // hide 
                        $('.load-more').hide();
                    }else{
                        $('.load-more').show();
                        $(".load-more").text("Load more..");
                    }
                }, 2000);

            }
        });
    }else{
        $('.load-more').text("Loading...");

        // Setting little delay while removing contents
        setTimeout(function() {

            // When row is greater than allcount then remove all class='post' element after 3 element
            $('.item-list:nth-child(3)').nextAll('.item-list').remove();

            // Reset the value of row
            $("#row").val(0);

            // Change the text and background
            $('.load-more').text("Load more..");
            $('.load-more').css("background","#15a9ce");
            
        }, 2000);


    }

}
</script>
</body>
</html>
