<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<!-- Fav and touch icons -->
<link rel="apple-touch-icon-precomposed" sizes="144x144" href="<?php echo base_url(ciHtmlTheme.'/assets/ico/apple-touch-icon-144-precomposed.png')?>">
<link rel="apple-touch-icon-precomposed" sizes="114x114" href="<?php echo base_url(ciHtmlTheme.'/assets/ico/apple-touch-icon-114-precomposed.png')?>">
<link rel="apple-touch-icon-precomposed" sizes="72x72" href="<?php echo base_url(ciHtmlTheme.'/assets/ico/apple-touch-icon-72-precomposed.png')?>">
<link rel="apple-touch-icon-precomposed" href="<?php echo base_url(ciHtmlTheme.'/ico/apple-touch-icon-57-precomposed.png')?>">
<link rel="shortcut icon" href="<?php echo base_url(ciHtmlTheme.'/assets/ico/favicon.png')?>">
<title>Login : Oya Auction</title>
<!-- Bootstrap core CSS -->
<link href="<?php echo base_url(ciHtmlTheme.'/assets/bootstrap/css/bootstrap.min.css')?>" rel="stylesheet">
<!-- Custom styles for this template -->
<link href="<?php echo base_url(ciHtmlTheme.'/assets/css/style.css')?>" rel="stylesheet">
<link href="<?php echo base_url(ciHtmlTheme.'/assets/css/custom.css')?>" rel="stylesheet">
<!-- styles needed for carousel slider -->
<link href="<?php echo base_url(ciHtmlTheme.'/assets/css/owl.carousel.css')?>" rel="stylesheet">
<link href="<?php echo base_url(ciHtmlTheme.'/assets/css/owl.theme.css')?>" rel="stylesheet">

<script>
    paceOptions = {
      elements: true
    };
</script>
<script src="<?php echo base_url(ciHtmlTheme.'/assets/js/pace.min.js')?>"></script>
 </head>
<body>

<div id="wrapper">
<!-- /.header -->
 <?php $this->load->view(ciHtmlTheme.'/includes/header');?>
  
  <div class="main-container">
    <div class="container">
      <div class="row">
        <div class="col-sm-5 login-box">
          <div class="panel panel-default">
              <div class="col-lg-12  box-title margin-bottom-5">
                    <div class="inner"><h2><i class="icon-user-add color-aqua"></i> ASSOCIATE WITH VENDOR</h2>


                    </div>
                </div>
            <form role="form" name="frmAssociate" id="frmAssociate">   
            <div class="panel-body">
         
            
				<div class="form-group">
                  <label for="sender-email" class="control-label">Vendor/Auctioneer:</label>
                  
                    <?php echo $result['vendor_name']?>
                    
                  
                  
                </div>
				<div class="form-group">
                  <label for="sender-email" class="control-label">Address:</label>
                  
                 <?php echo $result['country_name']. '- '.$result['state_name'].'- '.$result['city_name'].'- '.$result['pin']?>
                    
                  
                  
                </div>
                <div class="form-group">
                  <label for="sender-email" class="control-label">Username:</label>
                  <div class="input-icon"> 
                  	<i class="icon-user fa"></i>
                    <input type="text"  placeholder="Username" class="form-control name" name="username" id="username" data-placement="bottom" title="Username / Email">
                    
                  </div>
                   <div class="error_section">
                   <label style='display:none;' id="username-error" class="error" for="username">Please Enter User name</label>
                  </div>
                </div>
                <div class="form-group">
                  <label for="user-pass"  class="control-label">Password:</label>
                  <div class="input-icon"> 
                  	<i class="icon-lock fa"></i>
                    <input type="password"  class="form-control pass" placeholder="Password"  id="password" name="password" data-placement="bottom" title="Your Password">
                  </div>
                  <div class="error_section">
                     <label style='display:none;' id="password-error" class="error" for="password">Please Enter Password</label>
                  </div>
                </div>
               
                <div class="error_section">
                <label style='display:none;' id="login_type-error" class="error" for="login_type">This field is required.</label>
                </div>
                <div class="form-group">
                  <a  href="#" class="btn btn-primary  btn-block" id="btnLogin">Login and Associate</a>
                  <div id="msg"></div>
                </div>
              
            </div>
            <div class="panel-footer">

             

              <p class="text-center pull-right"> <a href="<?php echo base_url('user/forgot_password');?>"> Lost your password? </a> </p>
              <div style=" clear:both"></div>
            </div>
            <input type="hidden"  class="form-control name" name="invitation_email" id="invitation_email" value="<?php echo $email?>">
            <input type="hidden"  class="form-control name" name="invitation_vendor_id" id="invitation_vendor_id" value="<?php echo $vendor_id?>">
            </form>
          </div>
          <div class="login-box-btm text-center">
            <p> Don't have an account? <br>
              <a href="<?php echo base_url('user/account_type');?>"><strong>Sign Up !</strong> </a> </p>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- /.main-container -->
  
 
  <!-- /.claa me  -->
	<?php $this->load->view(ciHtmlTheme.'/includes/call_back');?>
  <!-- /.footer --> 
  <?php $this->load->view(ciHtmlTheme.'/includes/footer');?>
 
</div>
<!-- /.wrapper --> 

<!-- Le javascript
================================================== --> 
<!-- Placed at the end of the document so the pages load faster --> 
<script src="<?php echo base_url(ciHtmlTheme.'/assets/js/jquery.min.js')?>"> </script>
<script src="<?php echo base_url(ciHtmlTheme.'/assets/bootstrap/js/bootstrap.min.js')?>"></script> 
<!-- include carousel slider plugin  --> 
<script src="<?php echo base_url(ciHtmlTheme.'/assets/js/owl.carousel.min.js')?>"></script> 

<!-- include equal height plugin  --> 
<script src="<?php echo base_url(ciHtmlTheme.'/assets/js/jquery.matchHeight-min.js')?>"></script> 
<!-- include jquery list shorting plugin plugin  --> 
<script src="<?php echo base_url(ciHtmlTheme.'/assets/js/hideMaxListItem.js')?>"></script> 
<!-- include jquery.fs plugin for custom scroller and selecter  --> 
<script src="<?php echo base_url(ciHtmlTheme.'/assets/plugins/jquery.fs.scroller/jquery.fs.scroller.js')?>"></script>
<script src="<?php echo base_url(ciHtmlTheme.'/assets/plugins/jquery.fs.selecter/jquery.fs.selecter.js')?>"></script>
<!-- include custom script for site  --> 
<script src="<?php echo base_url(ciHtmlTheme.'/assets/js/jquery.validate.min.js')?>"></script>
<script src="<?php echo base_url(ciHtmlTheme.'/assets/js/script.js')?>"></script>
<script>    
$("#btnLogin").click(function(e){
	 e.preventDefault();
	var form=$("#frmAssociate");
	form.validate({
	    ignore:[],
		rules: {
			username: {required : true},
			password: {required : true},
		
			
			},
		messages: {	
			username:"Please Enter User name"
			},
			
		});

	if(form.valid())
	{
		
	 	$( "#frmAssociate" ).submit();
	}

	
});
$('#frmAssociate').on('submit',(function(e) {
	var $btn = $('#btnLogin').button('loading');
	 e.preventDefault();
		$.ajax({
			url: '<?= base_url();?>user/update_associate_details',
			type:"POST",
			data: new FormData(this),
			contentType: false,
			cache: false,
			processData:false,
			dataType: 'json',
			success: function (resObj) {
				
				if (resObj.status) 
				{
					alert("You have Associated with the Auctioneer");
						window.location = resObj.redirect_url;
					
					
				}
				else{
					$("#msg").html('<div class="alert alert-warning alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button> <strong>Warning! '+resObj.message+'<i class="fa fa-warning"></i></div>');
					 $btn.button('reset')
				}	
			},
			error: function(){
				$btn.button('reset')
			}
			});
	}));		
 </script>
</body>
</html>
