<?php
defined('BASEPATH') or exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<!-- Fav and touch icons -->
<link rel="apple-touch-icon-precomposed" sizes="144x144"
	href="<?php echo base_url(ciHtmlTheme.'/assets/ico/apple-touch-icon-144-precomposed.png')?>">
<link rel="apple-touch-icon-precomposed" sizes="114x114"
	href="<?php echo base_url(ciHtmlTheme.'/assets/ico/apple-touch-icon-114-precomposed.png')?>">
<link rel="apple-touch-icon-precomposed" sizes="72x72"
	href="<?php echo base_url(ciHtmlTheme.'/assets/ico/apple-touch-icon-72-precomposed.png')?>">
<link rel="apple-touch-icon-precomposed"
	href="<?php echo base_url(ciHtmlTheme.'/ico/apple-touch-icon-57-precomposed.png')?>">
<link rel="shortcut icon"
	href="<?php echo base_url(ciHtmlTheme.'/assets/ico/favicon.png')?>">
<title>Auctioneer Registration : Oya Auction</title>
<!-- Bootstrap core CSS -->
<link
	href="<?php echo base_url(ciHtmlTheme.'/assets/bootstrap/css/bootstrap.min.css')?>"
	rel="stylesheet">
<link
	href="<?php echo base_url('assets/plugins/chosen/chosen.min.css')?>"
	rel="stylesheet">
<!-- Custom styles for this template -->

<link href="<?php echo base_url(ciHtmlTheme.'/assets/css/style.css')?>"
	rel="stylesheet">
<link href="<?php echo base_url(ciHtmlTheme.'/assets/css/custom.css')?>"
	rel="stylesheet">

<!-- styles needed for carousel slider -->
<link
	href="<?php echo base_url(ciHtmlTheme.'/assets/css/owl.carousel.css')?>"
	rel="stylesheet">
<link
	href="<?php echo base_url(ciHtmlTheme.'/assets/css/owl.theme.css')?>"
	rel="stylesheet">

<script>
    paceOptions = {
      elements: true
    };
</script>

<script
	src="<?php echo base_url(ciHtmlTheme.'/assets/js/pace.min.js')?>"></script>

</head>
<body>

	<div id="wrapper">
		<!-- /.header -->
 <?php $this->load->view(ciHtmlTheme.'/includes/header');?>
  
  <div class="main-container">
			<div class="container">
				<div class="row">
					<div class="col-md-8 page-content">
						<div class="inner-box category-content">
							<h2 class="title-2">
								<i class="icon-user-add"></i> Terms & Conditions
							</h2>
							<div class="row">
								<div class="col-sm-12">
									<div class="modal-body" style="width:100%;height:500px"> 
										<iframe  id="iframe" src="<?php echo base_url('assets/terms/register_terms.pdf#toolbar=0');?>" style="height:100%;width:100%;"></iframe>
									</div>
									<div class="modal-footer">
										<button class="btn btn-primary" type="button" onclick="submit_terms();" name="accept" id='accept' >Accept</button>
										<button type="button" class="btn btn-danger" onclick="close_terms()"; name="not_accepted">Close</button>
									</div>
									<input type="hidden" id="email" value="<?php echo $_SESSION['vendor_contacts']['c_email']; ?>"> 
								</div>
							</div>
						</div>
					</div>
					<!-- /.page-content -->

					<div class="col-md-4 reg-sidebar">
						<div class="reg-sidebar-inner text-center">
							<div class="promo-text-box">
								<i class=" icon-picture fa fa-4x icon-color-1"></i>
								<h3>
									<strong>Create Auction</strong>
								</h3>
								<p>Create your private and public auction, Set wide range of auction types and accept part quantity </p>
							</div>
							<div class="promo-text-box">
								<i class=" icon-pencil-circled fa fa-4x icon-color-2"></i>
								<h3>
									<strong>Invite Buyers</strong>
								</h3>
								<p>Invite buyers to participate your auctions. Work with predefined buyers, Restrict buyers for Private auctions</p>
							</div>
							<div class="promo-text-box">
								<i class="  icon-heart-2 fa fa-4x icon-color-3"></i>
								<h3>
									<strong>Branch Management</strong>
								</h3>
								<p>Create Your branches, Auction branch wise and set different permission to your users  </p>
							</div>
						</div>
					</div>
				</div>
				<!-- /.row -->
			</div>
		</div>
		<!-- /.main-container -->


		<!-- /.claa me  -->
	<?php $this->load->view(ciHtmlTheme.'/includes/call_back');?>
  <!-- /.footer --> 
  <?php $this->load->view(ciHtmlTheme.'/includes/footer');?>
 
</div>
	<!-- /.wrapper -->

	<!-- Le javascript
================================================== -->
	<!-- Placed at the end of the document so the pages load faster -->
	<script
		src="<?php echo base_url(ciHtmlTheme.'/assets/js/jquery.min.js')?>"> </script>
	<script
		src="<?php echo base_url(ciHtmlTheme.'/assets/bootstrap/js/bootstrap.min.js')?>"></script>
	<!-- include carousel slider plugin  -->
	<script
		src="<?php echo base_url(ciHtmlTheme.'/assets/js/owl.carousel.min.js')?>"></script>
	<script
		src="<?php echo base_url('assets/plugins/chosen/chosen.jquery.min.js')?>"></script>
	<!-- include equal height plugin  -->
	<script
		src="<?php echo base_url(ciHtmlTheme.'/assets/js/jquery.matchHeight-min.js')?>"></script>
	<!-- include jquery list shorting plugin plugin  -->
	<script
		src="<?php echo base_url(ciHtmlTheme.'/assets/js/hideMaxListItem.js')?>"></script>
	<!-- include jquery.fs plugin for custom scroller and selecter  -->
	<script
		src="<?php echo base_url(ciHtmlTheme.'/assets/plugins/jquery.fs.scroller/jquery.fs.scroller.js')?>"></script>
	<script
		src="<?php echo base_url(ciHtmlTheme.'/assets/plugins/jquery.fs.selecter/jquery.fs.selecter.js')?>"></script>
	<!-- include custom script for site  -->

	<script>
		// accept terms and save temp data
		function submit_terms(){
			var email = $("#email").val();
			$.ajax({
				url: "<?php echo base_url();?>user/save_temp_data",
				type: "POST",
				data:  {'email':email},
				contentType: false,
				cache: false,
				processData:false,
				dataType:'json',
				success: function(data)
				{
					if(data.success)
					{
						alert("Terms and conditions accepted successfully.");
						location.href="<?php echo base_url('user/verify_vemail')?>";
					}
					else
					{
						alert(data.msg);
						$btn.button('reset');
					}
						
					},
				error: function() 
				{
					alert(data.msg);
					$btn.button('reset');
				} 	
			}); 
		}

		// on click of close button
		function close_terms(){
			var txt;
				var r = confirm("Are you sure you want to close!");
				if (r == true) {
					alert('You are not registred!');
					window.load('user/account_type');
				} else {
				}
		}
	</script> 
</body>
</html>
