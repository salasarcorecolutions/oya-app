
<div class="footer" id="footer">
    <div class="container">
      <ul class=" pull-left navbar-link footer-nav">
        <li><a href="<?php echo  base_url();?>"> Home </a> 
		<a href="<?php echo  base_url('about_us');?>"> About us </a> 
		<a href="<?php echo  base_url('terms_and_conditions');?>"> Terms and Conditions </a> 
		<a href="<?php echo  base_url('privacy_policy');?>"> Privacy Policy </a> 
		<a href="#" data-target="#contact_us_modal" data-toggle="modal"> Contact us </a> 
		<a href="<?php echo  base_url('faq');?>"> FAQ </a>
      </ul>
      <ul class=" pull-right navbar-link footer-nav">
        <li> &copy; 2020 Oya Auction, All Right Reserved </li>
      </ul>
    </div>
    
  </div>

<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" class="modal fade in" id="contact_us_modal">
      <div class="modal-dialog">
      <form name="contact_us_form" id="contact_us_form">
        <div class="modal-content">
          <div class="modal-header">
            <button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
            <h4 id="myModalLabel" class="modal-title-site text-center">Contact Us</h4>
          </div>
         
          <div class="modal-body" style="padding:15px 60px">
            <div class="row">
              <label class="col-md-3 control-label" for="textinput-name">Name</label>
              <div class="col-md-9">
                  <div class="form-group">
                  <input id="textinput-name" name="name" placeholder=" Name" class="form-control input-md"  type="text">
                </div>
              </div>
              <label class="col-md-3 control-label" for="textinput-name">Company Name</label>
              <div class="col-md-9">
                  <div class="form-group">
                  <input placeholder="Company Name" class="form-control input-md" type="text" name="company_name">
                </div>
              </div>
              <label class="col-md-3 control-label" for="textinput-name">Email</label>
              <div class="col-md-9">
                  <div class="form-group">
                  <input placeholder="Company Name" class="form-control input-md" type="email" name="email">
                </div>
              </div>
              <!-- Text input-->
              <label class="col-md-3 control-label" for="seller-Number">Phone Number</label>
              <div class="col-md-9">
                <div class="form-group">
                <input id="seller-Number" name="phone_no" placeholder="Phone Number" class="form-control input-md"  type="number">
              </div>
              </div>
              <!-- Select Basic -->
              <label class="col-md-3 control-label" for="seller-Location">Auction Type</label>
              <div class="col-md-9">
                  <div class="form-group">
                  <select id="auc_type" name="auc_type" class="form-control">
                    <option value="Forward Auction">Forward Auction</option>
                    <option value="Reverse Auction">Reverse Auction</option>
                    <option value="Yankee Auction">Yankee Auction</option>
                    <option value="Tender">Tender</option>
                    <option value="Product Auction">Product Auction</option>
                  </select>
                </div>
              </div>
              <label class="col-md-3 control-label" for="textinput-name">Message </label>
              <div class="col-md-9">
                  <div class="form-group">
                  <textarea placeholder="Message here" class="form-control input-md" type="text" name="msg"></textarea>
                </div>
              </div>
              <label class="col-md-3 control-label" >Reply Over</label>
              <div class="col-md-9">
                <div class="form-group">
                <label class="radio-inline" for="radios-0">
                    <input name="reply_over" class="contact_reply_over"  value="Mail" checked="checked" type="radio">
                    Mail </label>
                  <label class="radio-inline" for="radios-1">
                    <input name="reply_over" class="contact_reply_over"  value="Call" type="radio">
                    Call </label>
                </div>
              </div>
              <div class="call" style="display:none">
                <label class="col-md-3 control-label call" for="textinput-name">Call Time</label>
                <div class="col-md-4 call">
                  <div class="input-group"> <span class="input-group-addon">From</span>
                    <input id="prependedtext" name="call_time_from" id="call_back_totime" class="form-control" placeholder="placeholder" type="time">
                  </div>
                </div>
                <div class="col-md-4 call">
                  <div class="input-group"> <span class="input-group-addon">To</span>
                    <input id="prependedtext" name="call_time_to" id="call_back_endtime" class="form-control" placeholder="placeholder" type="time">
                  </div>
                </div>
              </div>
            </div>
          </div>
         
          <div class="modal-footer">

            <button class="btn btn-primary" type="submit" id="contact_us_btn">Save changes</button>
            <button data-dismiss="modal" class="btn btn-danger" type="button">Close</button>
          </div>
          </form>
      </div>
    
    <!-- /.modal-content --> 
    </div>
  <!-- /.modal-dialog --> 
</div>

<script>
var contact_us_url = '<?php echo base_url('home/contact_us')?>';
</script>
