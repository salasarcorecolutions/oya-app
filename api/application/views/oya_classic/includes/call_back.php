 <div class="page-bottom-info">
      <div class="page-bottom-info-inner">
      <div class="container">
		<div class="col-md-3 wow bounceInRight animated">
			<!-- Contact Us -->
			<div class="headline"><h4>Contact Us</h4></div> 
			<address class="color-white" style="color:#fff">
							A One Salasar Pvt. Ltd.<br>
							104, Vasudeo Chambers, Mulund Link Road,<br>
							Bhandup (W), Mumbai - 400078.<br>
							<span><i class="icon-phone-1"></i> <a href="callto://022-25660141">022-25660141</a> </span><br>
							<span><i class="icon-mobile"></i> <a href="callto://09320445534">09320445534</a> </span><br>
							<span><i class="icon-mobile"></i> <a href="callto://09320589231">09320589231</a> </span><br>
							<i class="icon-mail"></i> <a href="mailto:scs@salasarauction.com" class="">scs@salasarauction.com</a>
						</address>
			<!-- End Contact Us -->
			
		</div>
	<div class="col-md-6">
	
      	<div class="page-bottom-info-content text-center margin-bottom-20">
        	<h5>If you have any questions, comments or concerns, please call the Classified Advertising department at (+91) 22-25660141</h5>
            <a class="btn  btn-lg btn-primary-dark" href="tel:+9122-25660141">
            <i class="icon-mobile"></i> <span class="hide-xs color50">Call Now:</span> (+91) 022-25660141  </a>
        </div>
        <div class="text-center margin-bottom-20 col-md-12 clearfix" style="text-center">
       
 		<div style="width: 290px; margin:0px auto" class="input-group input-group-sm">
                  <input type="text" placeholder="eMail" class="form-control pull-right" name="sEmail" id="sEmail">

                  <div class="input-group-btn">
                    <button class="btn btn-default" type="submit" id="LoadRecordsButton"><i class="icon-mail"></i> Subscrive</button>
                  </div>
                </div>

		</div>
		<!-- Social Links -->
		
			<div class="social-icon text-center">
				<ul>
					<li><a class="twitter" href=""><i class="fa fa-twitter"></i></a></li>
					<li><a class="facebook" href=""><i class="fa fa-facebook"></i></a></li>
					<li><a class="youtube" href=""><i class="fa fa-youtube"></i></a></li>
					<li><a class="dribble" href=""><i class="fa fa-globe"></i></a></li>
					<li><a class="rss" href=""><i class="fa fa-rss"></i></a></li>
				</ul>
			</div>
			
			<!-- End Social Links -->
				
	</div>
	<div class="col-md-3 wow bounceInLeft animated">
	 <div class="headline"><h4>Quick Links</h4></div> 
	 <div class="footer-service">
			<ul class="list-unstyled">
				<li><i class="fa fa-link"></i> <a title="About Us" href="#">FORWARD AUCTION</a></li>
				<li><i class="fa fa-link"></i> <a title="Contact Us" href="#">REVERSE AUCTION</a></li>
				<li><i class="fa fa-link"></i> <a title="Terms And Contitions" href="#">eTENDER</a></li>
				<li><i class="fa fa-link"></i> <a title="Shipping Policy" href="#">DUTCH AUCTION</a></li>
				<li><i class="fa fa-link"></i> <a title="Privacy Policy" href="<?php echo base_url()?>">SALVAGE AUCTION</a></li>
				<li><i class="fa fa-link"></i> <a title="FAQ" href="<?php echo base_url('faq')?>">FAQ</a></li>
			</ul>
	</div>
	

	
	
	</div>

</div>
      
      </div>
  </div>