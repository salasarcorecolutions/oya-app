<div class="header">
    
     
      <!-- /.container-fluid --> 
   
    
    <nav class="navbar navbar-main yamm">
    <div class="container-fluid">
        <div class="navbar-header" style='padding-top:5px;'>
          
          <a style='width: 100%;'href="<?php echo base_url();?>">
            
              <img style="height: auto;width: 50px;" src="<?php echo base_url('/images/logo.png');?>"  alt="Image Alternative text" title="Image Title" /><span class="comp_name" >OYA AUCTION</span>
           
          
          </a>
          <button class="navbar-toggle collapsed" type="button" data-toggle="collapse" data-target="#main-nav-collapse" area_expanded="false"><span class="sr-only">Main Menu</span><span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span>
          </button>
        </div>
        <div class="collapse navbar-collapse" id="main-nav-collapse">
          
            <ul class="nav navbar-nav nav_sec">
            <li class="dropdown"><a style='position: relative;' href="#"><i class="fa fa-reorder icon_set"></i>&nbsp; Auction By Category<i class="drop-caret" data-toggle="dropdown"></i></a>
                
                <ul class="dropdown-menu dropdown-menu-category">
            
                        <?php
                    if ( ! empty($auction_categories))
                    {
                        foreach ($auction_categories as $items)
			    		{
			    			
			    			
			    			if(sizeof($items['sub_menu'])>0)
			    			{
			    				?>
                                <li><a href="<?php echo base_url('auction')?>?p=1&cat=<?php echo $items['category_name'];?>"><i class="fa fa-caret-square-o-right dropdown-menu-category-icon"></i><?php echo $items['category_name'] ?></a>
                            
                                <div class="dropdown-menu-category-section">
                                    <div class="dropdown-menu-category-section-inner">
                                        <div class="dropdown-menu-category-section-content">
                                           
                                                    <h5 class="dropdown-menu-category-title"><?php echo $items['category_name'] ?></h5>
                                                    <ul class="dropdown-menu-category-list">
                                                        <?php
                                                            foreach ($items['sub_menu'] as $subItems)
                                                            {?>
                                                                <li><i class="fa fa-chevron-circle-right"></i><a href="<?php echo base_url('auction')?>?p=1&mat=<?php echo $subItems['mat_name'];?>"><?php echo $subItems["mat_name"]?></a></li>
                                                            <?php }?>
                                                    </ul>
                                              
                                        </div>
                                    </div>
                                </div>    
                            
                            
                                </li>

                                <?php
			    			}
			    				
			    		}
                    }
                    	
			       ?>




                                

                    
                    </ul>
                </li>
                <li class="dropdown"><a style='position: relative;' href="#"><i class="fa fa-reorder icon_set"></i>&nbsp; Product By Category<i class="drop-caret" data-toggle="dropdown"></i></a>
                
                <ul class="dropdown-menu dropdown-menu-category">
            
                        <?php
                    if ( ! empty($product_categories))
                    {
                        foreach ($product_categories as $items)
			    		{
			    			
			    			
			    			if(sizeof($items['sub_menu'])>0)
			    			{
			    				?>
                                <li><a href="<?php echo base_url('auction')?>?a=1&cat=<?php echo $items['category_name'];?>"><i class="fa fa-caret-square-o-right dropdown-menu-category-icon"></i><?php echo $items['category_name'] ?></a>
                            
                                <div class="dropdown-menu-category-section">
                                    <div class="dropdown-menu-category-section-inner">
                                        <div class="dropdown-menu-category-section-content">
                                           
                                                    <h5 class="dropdown-menu-category-title"><?php echo $items['category_name'] ?></h5>
                                                    <ul class="dropdown-menu-category-list">
                                                        <?php
                                                            foreach ($items['sub_menu'] as $subItems)
                                                            {?>
                                                                <li><i class="fa fa-chevron-circle-right"></i><a href="<?php echo base_url('products')?>?p=1&mat=<?php echo $subItems['category_name'];?>"><?php echo $subItems["category_name"]?></a></li>
                                                            <?php }?>
                                                    </ul>
                                              
                                        </div>
                                    </div>
                                </div>    
                            
                            
                                </li>

                                <?php
			    			}
			    				
			    		}
                    }
                   
			    		
			       ?>




                                

                    
                    </ul>
                </li>
                
                <li><a href="<?php echo base_url('auction');?>?a=1&auc_type=Forward Auction"> FORWARD AUCTION</a></li>
                <li><a href="<?php echo base_url('auction');?>?a=1&auc_type=Reverse Auction">REVERSE AUCTION</a></li>
                <!--<li><a href="<?php echo base_url('auction');?>?a=1&auc_type=Dutch Auction">DUTCH AUCTION</a></li>-->
                <li><a href="<?php echo base_url('auction');?>?a=1&auc_type=Tender">E-TENDER</a></li>
                
               <?php 
			if($this->session->userdata('bidder_id')=="" && $this->session->userdata('vendor_id')=="")
			{
				echo '<li><a href="'.base_url("user/account_type").'"> REGISTER</a></li>';
				echo '<li><a href="'.base_url("user/login").'">LOGIN</a></li>';
			}
			
				if($this->session->userdata('bidder_id')!="") {
                   ?>
                       <li style='position: relative;' id="auc_cat_sec" class="dropdown"><a id="auc_cat" href="#"><i class="fa fa-user icon_set"></i>&nbsp;<?php echo $this->session->userdata('compname');?><i class="drop-caret" data-toggle="dropdown"></i></a>
                    <ul class="dropdown-menu dropdown-menu-category profile_dropdown">
                        
                    <li ><a href="<?php echo base_url('buyer/dashboard')?>"><i class="icon-home dropdown-menu-category-icon"></i> Dashboard </a></li>
					<li><a href="<?php echo base_url('buyer/auctions/favourite_auctions')?>"><i class="icon-heart dropdown-menu-category-icon"></i> Favourite Auctions</a></li>
					<li><a href="<?php echo base_url('buyer/auctions/pending_approval')?>"><i class="icon-heart dropdown-menu-category-icon"></i>  Pending Participations </a></li>
					<li><a href="<?php echo base_url('buyer/my_associations')?>"><i class="icon-star-circled dropdown-menu-category-icon"></i> My Associates </a></li>
					<li><a href="<?php echo base_url('buyer/my_messages')?>"><i class="icon-folder-close dropdown-menu-category-icon"></i> My Messages </a></li>
					<li><a href="<?php echo base_url('buyer/auctions/current_auctions')?>"><i class="icon-hourglass dropdown-menu-category-icon"></i> Live Auctions </a></li>
					<li><a href="<?php echo base_url('buyer/products/my_favourite_products')?>"><i class=" icon-money dropdown-menu-category-icon"></i> Favourite Products </a></li>
					<li><a href="<?php echo base_url('buyer/profile');?>"><i class=" icon-money dropdown-menu-category-icon"></i> My Profile </a></li>
					<li><a href="<?php echo base_url('user/logout');?>"><i class=" icon-money dropdown-menu-category-icon"></i> Log Out </a></li>
                        
                    </ul>
                </li>
                <?php }
                if($this->session->userdata('vendor_id')!="") {
				// buyer menus
			?>
				<li style='postion:relative;' class="dropdown "> <a style='width:150px;' href="#" class="dropdown-toggle" data-toggle="dropdown"> &nbsp;&nbsp;<?php echo $this->session->userdata('user_name')?><i class="icon-user fa icon_1"></i> <i class=" icon-down-open-big fa icon_2"></i></a>
				  <ul class="dropdown-menu dropdown-menu-category home_header">
					<li class="active"><a href="<?php echo base_url('auctioneer/dashboard')?>"><i class="icon-home dropdown-menu-category-icon"></i> Dashboard </a></li>
					<li><a href="<?php echo base_url('auctioneer/bidders/associate_bidder_list')?>"><i class="icon-star-circled dropdown-menu-category-icon"></i> My Associates </a></li>
					<li><a href="<?php echo base_url('auctioneer/messages')?>"><i class="icon-folder-close dropdown-menu-category-icon"></i> My Messages </a></li>
					<li><a href="<?php echo base_url('auctioneer/auction/index')?>"><i class="icon-hourglass dropdown-menu-category-icon"></i> Live Auctions </a></li>
					<!--<li><a href="<?php echo base_url('auctioneer/profile');?>"><i class=" icon-money dropdown-menu-category-icon"></i> My Profile </a></li>-->
					<li><a href="<?php echo base_url('user/logout');?>"><i class=" icon-money dropdown-menu-category-icon"></i> Log Out </a></li>
				  </ul>
				</li>
			<?php } ?>
                  </ul>
            
            
        </div>
    </div>
</nav>
  </div>