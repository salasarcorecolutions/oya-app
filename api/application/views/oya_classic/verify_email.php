<?php
defined('BASEPATH') or exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<!-- Fav and touch icons -->
<link rel="apple-touch-icon-precomposed" sizes="144x144"
	href="<?php echo base_url(ciHtmlTheme.'/assets/ico/apple-touch-icon-144-precomposed.png')?>">
<link rel="apple-touch-icon-precomposed" sizes="114x114"
	href="<?php echo base_url(ciHtmlTheme.'/assets/ico/apple-touch-icon-114-precomposed.png')?>">
<link rel="apple-touch-icon-precomposed" sizes="72x72"
	href="<?php echo base_url(ciHtmlTheme.'/assets/ico/apple-touch-icon-72-precomposed.png')?>">
<link rel="apple-touch-icon-precomposed" href="<?php echo base_url(ciHtmlTheme.'/ico/apple-touch-icon-57-precomposed.png')?>">
<link rel="shortcut icon" 	href="<?php echo base_url(ciHtmlTheme.'/assets/ico/favicon.png')?>">
<title>Buyer Registration : Oya Auction</title>
<!-- Bootstrap core CSS -->
<link href="<?php echo base_url(ciHtmlTheme.'/assets/bootstrap/css/bootstrap.min.css')?>"	rel="stylesheet">
<link href="<?php echo base_url('assets/plugins/chosen/chosen.min.css')?>" rel="stylesheet">
<!-- Custom styles for this template -->

<link href="<?php echo base_url(ciHtmlTheme.'/assets/css/style.css')?>"	rel="stylesheet">
<link href="<?php echo base_url(ciHtmlTheme.'/assets/css/custom.css')?>" rel="stylesheet">

<!-- styles needed for carousel slider -->
<link
	href="<?php echo base_url(ciHtmlTheme.'/assets/css/owl.carousel.css')?>" rel="stylesheet">
<link
	href="<?php echo base_url(ciHtmlTheme.'/assets/css/owl.theme.css')?>" rel="stylesheet">

<script>
    paceOptions = {
      elements: true
    };
</script>

<script
	src="<?php echo base_url(ciHtmlTheme.'/assets/js/pace.min.js')?>"></script>
	
</head>
<body>

	<div id="wrapper">
		<!-- /.header -->
 <?php $this->load->view(ciHtmlTheme.'/includes/header');?>
  
  <div class="main-container">
			<div class="container">
				<div class="row">
					<div class="col-md-8 page-content">
						<div class="inner-box category-content">
							<h2 class="title-2">
								<i class="icon-user-add"></i> Verify Your Account
							</h2>
							<div class="row">
								<div class="col-sm-12">
									 


                                    <div class="row padding15 paddingB_only light-grey-bg blue-text textAlignC font18">
                                        <form style="padding-top: 4%;" id="vendor-form" class="form-horizontal" >
                                        
                                            <div class="col-sm-12">
                                                <div class="col-sm-2 field-label-up ico-attach-left -size_lg">
                                                    <label class="label-control">Email :</label>
                                                </div>
                                                <div class="col-sm-4 form-group">	
                                                    <input type="text"  class="form-control"  id="uname" readonly value="<?php echo $_SESSION['primary_mail']?>" />
                                                    <input type="hidden" name="action" id="action" value="u" />
                                                </div>	
                                                <div class="col-sm-3">
                                                <?php if ($_SESSION['primary_email_verify'] == 0)
                                                {?>
                                                
                                                    <button type="button" id="email_send" class="btn btn-success btn-border" href="javascript:void(0);" onclick="send_email_otp();add_resend_email_counter(1)">
                                                            <span class="button-text font18" >Send OTP</span>
                                                    </button>
                                                
                                                <?php }?>
                                                </div>
                                                <div class="col-sm-3 email_verify">
                                                    <?php if ($_SESSION['primary_email_verify'] == 0){?>
                                                        <span class='text-danger'><i class="fa fa-times non_verify"></i> Not Verified</span>
                                                    <?php }else{?>
                                                        <span class='text-success'><i class="fa fa-check verify"></i> Verified</span>
                                                    <?php } ?>
                                                </div>
                                            </div>
                                            <span id="error_email_msg"  style="color:red;display:none;"></span>
                                            <span id="success_email_msg"  style="color:green;display:none;"></span>
                                            <div class="col-sm-12 user_email_otp" style="display:none;" >
                                                <div class="col-sm-2 field-label-up ico-attach-left -size_lg">
                                                    <label class="label-control">OTP :</label>
                                                </div>
                                                <div class="col-sm-4 form-group">	
                                                    <input value="" class="form-control" type="text" placeholder="Enter OTP " name="otp" id="otp" />
                                                </div>
                                                <div class="col-sm-2">
                                                    <button type="button" class="btn btn-success" href="javascript:void(0);" onclick="verify_otp()">
                                                        Verify
                                                    </button>		
                                                </div>				
                                                <div class="col-sm-4">
                                                    <button type="button" id="resend_email_btn" class="btn btn-warning" href="javascript:void(0);" onclick="re_send_email_otp(1);" >
                                                        <span class="font18" id="email_counter_1" >Resend OTP </span>
                                                    </button>
                                                    <span class="col-sm-12 text-primary" id="resend_otp_email_link_1">Resend OTP<span id="reset_email_counter_1">(30)</span></span>
                                                </div> 
                                            </div>
                                        </form> 
                                    </div> 

								</div>
							</div>
						</div>
					</div>
					<!-- /.page-content -->

					<div class="col-md-4 reg-sidebar">
						<div class="reg-sidebar-inner text-center">
							<div class="promo-text-box">
								<i class=" icon-picture fa fa-4x icon-color-1"></i>
								<h3>
									<strong>Get Notification</strong>
								</h3>
								<p>Update your field of interest and get notifications of auctions and products around you. </p>
							</div>
							<div class="promo-text-box">
								<i class=" icon-pencil-circled fa fa-4x icon-color-2"></i>
								<h3>
									<strong>Participate Auctions</strong>
								</h3>
								<p>Let Auctioneer invite you to participate auctions or bid on a product. Update your field of interests</p>
							</div>
							<div class="promo-text-box">
								<i class="  icon-heart-2 fa fa-4x icon-color-3"></i>
								<h3>
									<strong>Favorite Auctions and Products.</strong>
								</h3>
								<p>Add Favourite Auctions and Products for your future referance</p>
							</div>
						</div>
					</div>
				</div>
				<!-- /.row -->
			</div>
		</div>
		<!-- /.main-container -->


		<!-- /.claa me  -->
	<?php $this->load->view(ciHtmlTheme.'/includes/call_back');?>
  <!-- /.footer --> 
  <?php $this->load->view(ciHtmlTheme.'/includes/footer');?>
 
</div>
	<!-- /.wrapper -->

	<!-- Le javascript
================================================== -->
	<!-- Placed at the end of the document so the pages load faster -->
	<script	src="<?php echo base_url(ciHtmlTheme.'/assets/js/jquery.min.js')?>"> </script>
	<script	src="<?php echo base_url(ciHtmlTheme.'/assets/bootstrap/js/bootstrap.min.js')?>"></script>
	<!-- include carousel slider plugin  -->
	<script	src="<?php echo base_url(ciHtmlTheme.'/assets/js/owl.carousel.min.js')?>"></script>
	<script src="<?php echo base_url('assets/plugins/chosen/chosen.jquery.min.js')?>"></script>
	<!-- include equal height plugin  -->
	<script	src="<?php echo base_url(ciHtmlTheme.'/assets/js/jquery.matchHeight-min.js')?>"></script>
	<!-- include jquery list shorting plugin plugin  -->
	<script	src="<?php echo base_url(ciHtmlTheme.'/assets/js/hideMaxListItem.js')?>"></script>
	<!-- include jquery.fs plugin for custom scroller and selecter  -->
	<script	src="<?php echo base_url(ciHtmlTheme.'/assets/plugins/jquery.fs.scroller/jquery.fs.scroller.js')?>"></script>
	<script	src="<?php echo base_url(ciHtmlTheme.'/assets/plugins/jquery.fs.selecter/jquery.fs.selecter.js')?>"></script>
	<!-- include custom script for site  -->
	<script	src="<?php echo base_url(ciHtmlTheme.'/assets/js/jquery.validate.min.js')?>"></script>
	<script src="<?php echo base_url(ciHtmlTheme.'/assets/js/script.js')?>"></script>
	 
    <script>
    
     	 
        var resend_email_otp = 0;
		var resend_mob_otp = 0;
		
		function send_email_otp(cnt='')
		{
			var emaill = '<?php echo $_SESSION['primary_mail']?>';
			var id = '<?php echo $_SESSION['temp_id']?>';
			var type = '<?php echo $_SESSION['type']?>';
			$.ajax({
				url: '<?php echo base_url();?>user/send_email_otp',
				data: {'email':emaill,'id':id},
				type:"post",
				dataType: 'json',
				success: function (resObj) {
					if (resObj.status == "success") 
					{
							$(".user_email_otp").show();
							$("#email_send").hide();
							$("#send_otp").hide();
							$("#verify").show();
							$("#success_msg").show();
							$("#success_msg").html(resObj.msg);
					}else{
						$("#error_msg").show();
						$("#error_msg").html(resObj.msg);
					}	
				},
				error: function(){
					alert('Please Try Again After Some Time .... ');
				}
			});
				
		}
		 
		function re_send_email_otp(id)
		{
			var otp_cnt = resend_email_otp++;
			if (otp_cnt <=2){
				send_email_otp(otp_cnt);
				add_resend_email_counter(id);
			} else {
				$("#error_email_msg").show();
				$("#error_email_msg").html('Send OTP Failed,Please Try Again After Some Time');
				add_resend_email_counter(id,'failed');
				$("#resend_email_btn").hide();
			}
		
		}
		 
		function verify_otp()
		{
			var email = '<?php echo $_SESSION['primary_mail']?>';
			var id = '<?php echo $_SESSION['temp_id']?>';
			var type = '<?php echo $_SESSION['type']?>';
			var otp = $("#otp").val();
			if (otp)
			{
				if (type == 'bidder')
				{
					var url_link = '<?php echo base_url('user/verify_bidder_email_otp')?>';
				}
				else
				{
					var url_link = '<?php echo base_url('user/verify_e_otp')?>';
				}
				$.ajax({
					url: url_link,
					data: {'otp':otp,'id':id,'email':email,'type':type},
					type:"post",
					dataType: 'json',
					success: function (resObj) {
						if (resObj.status == "success") 
						{
							window.location.href='<?php echo base_url('user/login')?>';

						}else{
							$("#error_email_msg").show();
							$("#error_email_msg").html(resObj.msg);
						}	
					},
					error: function(){
						alert('Invalid Request');
					}
				});
			}
			else
			{
				alert('Please Enter OTP');
			}
				
		}
		
		 
		

		var counterObj = new Object();
		function add_resend_email_counter(id,status='')
		{
			if (status)
			{
				counterObj['count_vendor_email_'+id] = 300; //To set the counter variable for the given id.
			}else{
				counterObj['count_vendor_email_'+id]  = 30;
			}
            resend_email_counter(id);
        		
		}
		 
		function resend_email_counter(id)
		{
			document.getElementById('reset_email_counter_'+id).innerHTML = '&nbsp('+counterObj['count_vendor_email_'+id]+')';
            counterObj['count_vendor_email_'+id]--;
            if (counterObj['count_vendor_email_'+id] < 0) {
                counterObj['count_vendor_email_'+id] = 0;
                document.getElementById("reset_email_counter_"+id).style.cursor = "pointer";
				document.getElementById("resend_otp_email_link_"+id).style.display = "none";
				$('#resend_email_btn').show();
            } else {
                setTimeout(function(){
                resend_email_counter(id);
            }, 1000);
            document.getElementById("reset_email_counter_"+id).style.cursor = "default";
            document.getElementById("email_counter_"+id).style.display = "inline-block";
			document.getElementById("resend_otp_email_link_"+id).style.display = "block";
			$('#resend_email_btn').hide();
			
			}
		}
		 
     
    
    </script> 
</body>
</html>
