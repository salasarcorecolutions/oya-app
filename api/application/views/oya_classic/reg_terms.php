<html lang="en">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<!-- Fav and touch icons -->
<link rel="apple-touch-icon-precomposed" sizes="144x144"
	href="<?php echo base_url()?>oya_classic/assets/ico/apple-touch-icon-144-precomposed.png">
<link rel="apple-touch-icon-precomposed" sizes="114x114"
	href="<?php echo base_url()?>oya_classic/assets/ico/apple-touch-icon-114-precomposed.png">
<link rel="apple-touch-icon-precomposed" sizes="72x72"
	href="<?php echo base_url()?>oya_classic/assets/ico/apple-touch-icon-72-precomposed.png">
<link rel="apple-touch-icon-precomposed" href="<?php echo base_url()?>oya_classic/ico/apple-touch-icon-57-precomposed.png">
<link rel="shortcut icon" 	href="<?php echo base_url()?>oya_classic/assets/ico/favicon.png">
<title>Buyer Registration : Oya Auction</title>
<!-- Bootstrap core CSS -->
<link href="<?php echo base_url()?>oya_classic/assets/bootstrap/css/bootstrap.min.css"	rel="stylesheet">
<link href="<?php echo base_url()?>assets/plugins/chosen/chosen.min.css" rel="stylesheet">
<!-- Custom styles for this template -->

<link href="<?php echo base_url()?>oya_classic/assets/css/style.css"	rel="stylesheet">
<link href="<?php echo base_url()?>oya_classic/assets/css/custom.css" rel="stylesheet">
</head>
<body>
<div class="section-content">
        <div class="row ">
        <h1 class="text-center title-1"> <strong>TERMS AND CONDITIONS</strong> </h1>
      	<hr class="center-block small text-hr">
        </div>
        <h3>OYA.AUCTION - Legal Notice</h3>
        
        <p>Thank you for shopping at Oya.Auction Com.  Our mission is to provide a 
          world class shopping experience for quality home furnishings through a
          commitment to excellence and integrity in value and service.
        </p>
         
        <h3>Use Of the Site</h3>

        <p>This site, or any portion of this site, may not be reproduced, 
          duplicated, copied, sold, resold or otherwise exploited for any
          commercial purpose that is not expressly permitted and its 
          affiliates reserve the right to refuse service, terminate accounts
          and/or cancel orders in its discretion including, without limitation,
          if Oya.Auction believes that customer conduct violates applicable law 
          or is harmful to the interests of Oya.Auction and its affiliates.
        </p>

        <h3>Copyright</h3>

        <p>All content and any compilations thereof included on this site, 
          including, but not limited to, text, graphics, logos, icons, images, 
          audio clips and software, is the property of Oya.auction, or its 
          content suppliers and is protected international copyright laws.
          All software used on this site is the property of Oya.Auction or
          its software suppliers and protected by copyright laws. The content 
          and software on this site may be used as a shopping and auction resource 
          only. Any other use, including the reproduction, modification, distribution,
          transmission, republication, display or performance of the content on
          this site is strictly prohibited.
        </p>

        <h3>Trademarks</h3>
        
        <p>Oya.a  uction and Oya.Auction Logo is trademarks of Oya.Auction in India and
          other countries. Oya.Auction graphics, logos and service names are trademarks
          of Oya.Auction, Oya.Auction's trademarks may not be used in connection with
          any product or service that is not Oya.Auction in any manner that is likely
          to cause confusion among customers or in any manner that disparages or
          discredits Oya.Auction.
        </p>
        
      <h3>Disclaimer</h3>

      <p>This site is provided by Oya.Auction on an "as is" basis. Oya.Auction makes 
        no representations or warranties of any kind, express or implied, as to the
        operation of the site or the information, content materials or products included
        on this site. To the full extent permissible by applicable law, Oya.Auction disclaims 
        all warranties express or implied, including, but not limited to, implied warranties
        of merchantability and fitness for a particular purpose. In no event shall Oya.Auction be
        liable for any damages of any kind arising from the use of this site, including, but not 
        limited to, direct, indirect, incidental, punitive and consequential damages.
      </p>
      
      <h3>Applicable Law</h3>

      <p>This site is created and controlled by Oya.Auction in the State of maharastra, India.
         As such, the laws of the State of Maharastra will govern these disclaimers, terms and
        conditions without giving effect to any principles of conflicts of laws. We reserve 
        the right to make changes to our site and these disclaimers, terms and conditions at any time.
      </p>

      <h3>Limitation of Liability</h3>

      <p>Oya.auction reserves the right to cancel any customer order that is found to have a pricing 
        error due to a web site programming bug or typographical error on it web sites or print catalogs.
        Oya.Auction is monitoring the content of others associates in this site. The unlawful content
        will be removed without any notice to the associates.
      </p>  

        </div>
        </body>
</html>