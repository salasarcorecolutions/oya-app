<?php
defined('BASEPATH') or exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<!-- Fav and touch icons -->
<link rel="apple-touch-icon-precomposed" sizes="144x144"
	href="<?php echo base_url(ciHtmlTheme.'/assets/ico/apple-touch-icon-144-precomposed.png')?>">
<link rel="apple-touch-icon-precomposed" sizes="114x114"
	href="<?php echo base_url(ciHtmlTheme.'/assets/ico/apple-touch-icon-114-precomposed.png')?>">
<link rel="apple-touch-icon-precomposed" sizes="72x72"
	href="<?php echo base_url(ciHtmlTheme.'/assets/ico/apple-touch-icon-72-precomposed.png')?>">
<link rel="apple-touch-icon-precomposed" href="<?php echo base_url(ciHtmlTheme.'/ico/apple-touch-icon-57-precomposed.png')?>">
<link rel="shortcut icon" 	href="<?php echo base_url(ciHtmlTheme.'/assets/ico/favicon.png')?>">
<title>Buyer Registration : Oya Auction</title>
<!-- Bootstrap core CSS -->
<link href="<?php echo base_url(ciHtmlTheme.'/assets/bootstrap/css/bootstrap.min.css')?>"	rel="stylesheet">
<link href="<?php echo base_url('assets/plugins/chosen/chosen.min.css')?>" rel="stylesheet">
<!-- Custom styles for this template -->

<link href="<?php echo base_url(ciHtmlTheme.'/assets/css/style.css')?>"	rel="stylesheet">
<link href="<?php echo base_url(ciHtmlTheme.'/assets/css/custom.css')?>" rel="stylesheet">

<script>
    paceOptions = {
      elements: true
    };
</script>

<script src="<?php echo base_url(ciHtmlTheme.'/assets/js/pace.min.js')?>"></script>
</head>
<body>

	<div id="wrapper">
		<!-- /.header -->
 <?php $this->load->view(ciHtmlTheme.'/includes/header');?>
   
 <div class="intro-inner">
    <div class="contact-intro">
      <div class="w100 map">
      <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d15074.949818206229!2d72.9397213!3d19.1629664!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x4e6da2c63d988eb0!2sA%20One%20Salasar%20Pvt.%20Ltd.!5e0!3m2!1sen!2sin!4v1596801515134!5m2!1sen!2sin" width="1585" height="450" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
      </div>
    </div>
    <!--/.contact-intro || map end--> 
    
  </div>
  <!-- /.intro-inner -->
  
 <div class="main-container">
    <div class="container">
      <div class="row clearfix">
          <div class="col-md-4">
              <div class="contact_info">
                 <h5 class="list-title gray"><strong>Contact us</strong></h5>
                   
                     <div class="contact-info ">
                          <div class="address">
                            <p class="p1">Vasudev Chamber, 104,</p>
                            <p class="p1">Mulund Goregaon Link Rd, Bhandup</p>
                            <p class="p1">W, Mumbai, Maharashtra 400078 </p>
                            <p>Email: scs@salasarauction.com</p>
                            <p>Contact: +912225660141
                            <p>&nbsp;</p>
                            <div>
                            
                              
                              </div>
                          </div>
                        </div>
                    
              <div class="social-list">
                    <a target="_blank" href="https://twitter.com/AoneSalasar"><i  class="fa fa-twitter fa-lg "></i></a>
                    <a target="_blank" href="https://www.facebook.com/AOneSalasarAuction/"><i  class="fa fa-facebook fa-lg "></i></a>
                    <a target="_blank" href="https://www.linkedin.com/company/aonesalasarauction"><i  class="fa fa-linkedin fa-lg "></i></a>
                    </div>
              </div>
          </div>
          <div class="col-md-8">
                <div class="contact-form">
                                            <h5 class="list-title gray"><strong>Contact us</strong></h5>

             
                     <form class="form-horizontal" method="post">
                    <fieldset>
						<div class="row">
                            <div class="col-sm-6">
                            <div class="form-group">
                                <div class="col-md-12">
                                    <input id="name" name="name" type="text" placeholder="Full Name" class="form-control">
                                </div>
                            </div>
                            </div>
                            
                             <div class="col-sm-6">
                            <div class="form-group">
                                <div class="col-md-12">
                                    <input id="lastname" name="number" type="number" placeholder="Mobile No." class="form-control">
                                </div>
                            </div>
                            </div>
                            
                             <div class="col-sm-6">
                            <div class="form-group">
                                <div class="col-md-12">
                                <input id="email" name="email" type="text" placeholder="Email Address" class="form-control">
                                </div>
                            </div>
                            </div>
                            
                             <div class="col-sm-6">
                            <div class="form-group">
                            <div class="col-md-12">
                                <input id="companyname" name="name" type="text" placeholder="Company Name" class="form-control">
                            </div>
                        </div>
                            </div>
                            
                            
                            
                            <div class="col-lg-12">
                            <div class="form-group">
                            <div class="col-md-12">
                                <textarea class="form-control" id="message" name="message" placeholder="Enter your massage for us here. We will get back to you within 2 business days." rows="7"></textarea>
                            </div>
                        </div>
                            
                            
                            <div class="form-group">
                            <div class="col-md-12 ">
                                <button type="submit" class="btn btn-primary btn-lg">Submit</button>
                            </div>
                        </div>
                        
                        
                            </div>
                        
                        </div>

                        
                    </fieldset>
                </form>
                    </div>
          </div>
      </div>
    </div>
  </div>

<?php $this->load->view(ciHtmlTheme.'/includes/footer');?>
 
 </div>
     <!-- /.wrapper -->
 
     <!-- Le javascript
 ================================================== -->
     <!-- Placed at the end of the document so the pages load faster -->
     <script	src="<?php echo base_url(ciHtmlTheme.'/assets/js/jquery.min.js')?>"> </script>
     <script	src="<?php echo base_url(ciHtmlTheme.'/assets/bootstrap/js/bootstrap.min.js')?>"></script>
     <!-- include carousel slider plugin  -->
     <script src="<?php echo base_url('assets/plugins/chosen/chosen.jquery.min.js')?>"></script>
     <!-- include equal height plugin  -->
     <script	src="<?php echo base_url(ciHtmlTheme.'/assets/js/jquery.matchHeight-min.js')?>"></script>
     <!-- include jquery list shorting plugin plugin  -->
     <script	src="<?php echo base_url(ciHtmlTheme.'/assets/js/hideMaxListItem.js')?>"></script>
     <!-- include jquery.fs plugin for custom scroller and selecter  -->
     <script	src="<?php echo base_url(ciHtmlTheme.'/assets/plugins/jquery.fs.scroller/jquery.fs.scroller.js')?>"></script>
     <script	src="<?php echo base_url(ciHtmlTheme.'/assets/plugins/jquery.fs.selecter/jquery.fs.selecter.js')?>"></script>
     <!-- include custom script for site  -->
     <script	src="<?php echo base_url(ciHtmlTheme.'/assets/js/jquery.validate.min.js')?>"></script>
     <script src="<?php echo base_url(ciHtmlTheme.'/assets/js/script.js')?>"></script>
     </body>
</html>
