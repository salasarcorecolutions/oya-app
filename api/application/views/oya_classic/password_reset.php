<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<!-- Fav and touch icons -->
<link rel="apple-touch-icon-precomposed" sizes="144x144" href="<?php echo base_url(ciHtmlTheme.'/assets/ico/apple-touch-icon-144-precomposed.png')?>">
<link rel="apple-touch-icon-precomposed" sizes="114x114" href="<?php echo base_url(ciHtmlTheme.'/assets/ico/apple-touch-icon-114-precomposed.png')?>">
<link rel="apple-touch-icon-precomposed" sizes="72x72" href="<?php echo base_url(ciHtmlTheme.'/assets/ico/apple-touch-icon-72-precomposed.png')?>">
<link rel="apple-touch-icon-precomposed" href="<?php echo base_url(ciHtmlTheme.'/ico/apple-touch-icon-57-precomposed.png')?>">
<link rel="shortcut icon" href="<?php echo base_url(ciHtmlTheme.'/assets/ico/favicon.png')?>">
<title>Reset Password : Oya Auction</title>
<!-- Bootstrap core CSS -->
<link href="<?php echo base_url(ciHtmlTheme.'/assets/bootstrap/css/bootstrap.min.css')?>" rel="stylesheet">
<!-- Custom styles for this template -->
<link href="<?php echo base_url(ciHtmlTheme.'/assets/css/style.css')?>" rel="stylesheet">
<link href="<?php echo base_url(ciHtmlTheme.'/assets/css/custom.css')?>" rel="stylesheet">
<!-- styles needed for carousel slider -->
<link href="<?php echo base_url(ciHtmlTheme.'/assets/css/owl.carousel.css')?>" rel="stylesheet">
<link href="<?php echo base_url(ciHtmlTheme.'/assets/css/owl.theme.css')?>" rel="stylesheet">

<script>
    paceOptions = {
      elements: true
    };
</script>
<script src="<?php echo base_url(ciHtmlTheme.'/assets/js/pace.min.js')?>"></script>
 </head>
<body>

<div id="wrapper">
<!-- /.header -->
 <?php $this->load->view(ciHtmlTheme.'/includes/header');?>
  
  <div class="main-container">
    <div class="container">
      <div class="row">
        <div class="col-sm-5 login-box">
          <div class="panel panel-default">
              <div class="col-lg-12  box-title margin-bottom-5">
                    <div class="inner"><h2><i class="icon-user-add color-aqua"></i> RESET PASSWORD</h2>


                    </div>
                </div>
            <form role="form" name="frmResetPassword" id="frmResetPassword">   
            <div class="panel-body">
         
             
                <div class="form-group">
                  <label for="sender-email" class="control-label">Enter New Password:</label>
                  <div class="input-icon"> 
                  	<i class="icon-user fa"></i>
                    <input type="text"  placeholder="New Password" class="form-control name" name="pass" id="pass" data-placement="bottom" title="Password">
                    
                  </div>
                   <div class="error_section">
                       <label id="username-error" style="display:none;" class="error" for="username">Please Enter New Password </label>
                   </div>
                </div>

                <div class="form-group">
                  <label for="sender-email" class="control-label">Enter Confirm Password:</label>
                  <div class="input-icon"> 
                  	<i class="icon-user fa"></i>
                    <input type="text"  placeholder="Confirm Password" class="form-control name" name="con_pass" id="con_pass" data-placement="bottom" title="Confirm Password ">
                    
                  </div>
                   <div class="error_section">
                       <label id="username-error" style="display:none;" class="error" for="username">Please Enter Confirm Password </label>
                   </div>
                </div>
                <input type="hidden" name="link" id="link" value="<?php echo $link?>"/>
               
                <div class="error_section">
                <label style='display:none;' id="login_type-error" class="error" for="login_type">This field is required.</label>
                </div>
                <div class="form-group">
                  <a  href="#" class="btn btn-primary  btn-block" id="btnSendPasswordLink">Update Password</a>
                  <div id="msg"></div>
                </div>
              
            </div>
            <div class="panel-footer">
    			<div class="login-box-btm text-center">
                <p> Don't have an account? <br>
                  <a href="<?php echo base_url('user/account_type');?>"><strong>Sign Up !</strong> </a> </p>
              	</div>
              
              <div style=" clear:both"></div>
            </div>
            </form>
          </div>
          
        </div>
      </div>
    </div>
  </div>
  <!-- /.main-container -->
  
 
  <!-- /.claa me  -->
	<?php $this->load->view(ciHtmlTheme.'/includes/call_back');?>
  <!-- /.footer --> 
  <?php $this->load->view(ciHtmlTheme.'/includes/footer');?>
 
</div>
<!-- /.wrapper --> 

<!-- Le javascript
================================================== --> 
<!-- Placed at the end of the document so the pages load faster --> 
<script src="<?php echo base_url(ciHtmlTheme.'/assets/js/jquery.min.js')?>"> </script>
<script src="<?php echo base_url(ciHtmlTheme.'/assets/bootstrap/js/bootstrap.min.js')?>"></script> 
<!-- include carousel slider plugin  --> 
<script src="<?php echo base_url(ciHtmlTheme.'/assets/js/owl.carousel.min.js')?>"></script> 

<!-- include equal height plugin  --> 
<script src="<?php echo base_url(ciHtmlTheme.'/assets/js/jquery.matchHeight-min.js')?>"></script> 
<!-- include jquery list shorting plugin plugin  --> 
<script src="<?php echo base_url(ciHtmlTheme.'/assets/js/hideMaxListItem.js')?>"></script> 
<!-- include jquery.fs plugin for custom scroller and selecter  --> 
<script src="<?php echo base_url(ciHtmlTheme.'/assets/plugins/jquery.fs.scroller/jquery.fs.scroller.js')?>"></script>
<script src="<?php echo base_url(ciHtmlTheme.'/assets/plugins/jquery.fs.selecter/jquery.fs.selecter.js')?>"></script>
<!-- include custom script for site  --> 
<script src="<?php echo base_url(ciHtmlTheme.'/assets/js/jquery.validate.min.js')?>"></script>
<script src="<?php echo base_url(ciHtmlTheme.'/assets/js/script.js')?>"></script>
<script>    
$("#btnSendPasswordLink").click(function(e){
	 e.preventDefault();
	var form=$("#frmResetPassword");
	form.validate({
	    ignore:[],
		rules: {
			pass: {required : true},
		    con_pass: {required : true},
			
			},
		messages: {	
            pass:"This is required.",
            con_pass:"This is required."
			},
			
		});

	if(form.valid())
	 	$( "#frmResetPassword" ).submit();
	

	
});
$('#frmResetPassword').on('submit',(function(e) {
	var $btn = $('#btnSendPasswordLink').button('loading');
	 e.preventDefault();
		$.ajax({
			url: '<?= base_url();?>user/update_password',
			type:"POST",
			data: new FormData(this),
			contentType: false,
			cache: false,
			processData:false,
			dataType: 'json',
			success: function (resObj) {
				if (resObj.status) 
				{
					
					$("#msg").html('<div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button> <strong>Success! '+resObj.msg+'<i class="fa fa-warning"></i></div>');
					
					
				}
				else{
					$("#msg").html('<div class="alert alert-warning alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button> <strong>Warning! '+resObj.msg+'<i class="fa fa-warning"></i></div>');
					 $btn.button('reset')
				}	
			},
			error: function(){
				$btn.button('reset')
			}
			});
	}));		
 </script>
</body>
</html>
