<?php
defined('BASEPATH') or exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<!-- Fav and touch icons -->
<link rel="apple-touch-icon-precomposed" sizes="144x144"
	href="<?php echo base_url(ciHtmlTheme.'/assets/ico/apple-touch-icon-144-precomposed.png')?>">
<link rel="apple-touch-icon-precomposed" sizes="114x114"
	href="<?php echo base_url(ciHtmlTheme.'/assets/ico/apple-touch-icon-114-precomposed.png')?>">
<link rel="apple-touch-icon-precomposed" sizes="72x72"
	href="<?php echo base_url(ciHtmlTheme.'/assets/ico/apple-touch-icon-72-precomposed.png')?>">
<link rel="apple-touch-icon-precomposed" href="<?php echo base_url(ciHtmlTheme.'/ico/apple-touch-icon-57-precomposed.png')?>">
<link rel="shortcut icon" 	href="<?php echo base_url(ciHtmlTheme.'/assets/ico/favicon.png')?>">
<title>Buyer Registration : Oya Auction</title>
<!-- Bootstrap core CSS -->
<link href="<?php echo base_url(ciHtmlTheme.'/assets/bootstrap/css/bootstrap.min.css')?>"	rel="stylesheet">
<link href="<?php echo base_url('assets/plugins/chosen/chosen.min.css')?>" rel="stylesheet">
<!-- Custom styles for this template -->

<link href="<?php echo base_url(ciHtmlTheme.'/assets/css/style.css')?>"	rel="stylesheet">
<link href="<?php echo base_url(ciHtmlTheme.'/assets/css/custom.css')?>" rel="stylesheet">

<script>
    paceOptions = {
      elements: true
    };
</script>

<script src="<?php echo base_url(ciHtmlTheme.'/assets/js/pace.min.js')?>"></script>
</head>
<body>

	<div id="wrapper">
		<!-- /.header -->
 <?php $this->load->view(ciHtmlTheme.'/includes/header');?>
<div style='display:none;'> class="intro-inner secure">
    <div class="about-intro">
      <div class="dtable hw100">
        <div class="dtable-cell hw100">
         
        </div>
      </div>
    </div>
    <!--/.about-intro --> 
    
  </div>
  <!-- /.intro-inner -->
  
  <div style='background-color:white;' class="main-container inner-page">
    <div class="container">
      <div class="section-content">
        <div class="row ">
        <h1 class="text-center title-1"> <strong>PRIVACY POLICY</strong> </h1>
      	<hr class="center-block small text-hr">
        </div>
        <h3>What if I communicate with Oya.Auction electronically?</h3>
        
        <p>When you send an email to Oya.Auction or visit the web site, you are communicating with us electronically.
            When you send an email to Oya.Auction , you consent to receive communications from us electronically in
            response. Please note that this does not constitute opting in to our email or direct mail lists. Oya.Auction 
            may communicate with you by email, by updating or posting notices on the web site. You agree that 
            all agreements, notices, disclosure and other communications that Oya.Auction provides to you electronically
            satisfy any legal requirement that such communication be in writing. All communications with 
            Oya.Auction become the property of Oya.Auction .Oya.Auction is committed to respecting the privacy
            rights of our customers and all visitors to our web site. We take this matter very seriously, and
            have instituted many policies and procedures to insure that none of your privacy rights as stated herein are
            ever violated. The following outlines Oya.Auction 's privacy policy and details the measures we have taken 
            to safeguard and protect your privacy, while providing you with a unique and rewarding online shopping
            experience. By visiting Oya.Auction , you are agreeing to the policies detailed below.
        </p>
         
        <h3>What information does Oya.Auction collect?</h3>

        <p>In order to serve you better, Oya.Auction collects and stores your personal information when you:<br> 
            (i) create an account <br> 
            (ii) sign up to be notified of special deals like discounts and exclusive offers; or <br> 
            (iii) enter a contest or promotion on our site. An account is created when you do one of following things:<br> 
            make a purchase on Oya.Auction , create a wish list on Oya.Auction , or register for an account on Oya.Auction ,
            or enter certain Oya.Auction sweepstakes. When creating an account, we ask you for your first and last name,
            your e-mail address, a password, and perhaps some other similar information. If you are making a purchase, 
            we will additionally ask you for your billing address, shipping address, and credit card information.<br> <br>
            Our system also may gather information about the areas you visit on our site as well as the IP address you use
            to connect to our site. We may use this data internally only for two reasons:<br> 
            1) to personalize your experience on Oya.Auction .com and the marketing materials you receive from Oya.Auction or its partners and <br> 
            2) to prevent fraud on your account. We would not share any of this data on an individual basis (we may share it in an
            aggregate form without personal identifiers) with any outside company other than perhaps consultants or 
            service providers of Oya.Auction who were assisting Oya.Auction in serving you better. We do not keep track
            of where you go on the World Wide Web.
        </p>

        <h3>What does Oya.Auction do with this personal information?</h3>

        <p>Your information is used to process your orders and personalize your shopping experience on Oya.Auction , for 
            internal purposes and to comply with any legal requirements. By storing your personal information on a secure
            server, Oya.Auction is able to shorten your checkout process and allow you to check your order status online.
            At Oya.Auction , we also review what kinds of products appeal most to our customers as a group. This statistical
            information helps us improve our offering in the same way that other companies change their catalog based on 
            what sells best. We may also use information about the kinds of products you buy from Oya.Auction to make other 
            marketing offers to you. Of course, you always have the option to opt-out from receiving such offers
            (for instructions on how to opt-out, please see below).
        </p>

        <h3>Does Oya.Auction share any information it collects with outside parties?</h3>
        
        <p>We may use your purchase information about the products you buy from Oya.Auction to make other marketing offers
        to you. Of course, you always have the option to opt-out from receiving such offers (for instructions on how to opt-out, 
        please see below). We may share this information with consultants or service providers to help Oya.Auction serve you better. 
        Of course, to the extent that we would ever share any of that information with a consultant or service provider, we would 
        only do so if that party agreed to protect your private information by adopting a privacy policy at least as stringent as our own.
        </p>
        
      <h3>How does Oya.Auction safeguard my personal information?</h3>

      <p>Oya.Auction has implemented a variety of security measures to maintain the safety of your personal information. 
          Your personal information is contained behind secured networks and is only accessible by a limited number of 
          employees who have special access rights. When you place orders or access your personal information, we offer 
          the use of a secure server. All sensitive/credit information supplied by you is transmitted via Secure Socket 
          Layer (SSL) technology and securely stored in Oya.Auction 's database.
      </p>
      
      <h3>Does Oya.Auction provide a way to update or remove personal information?</h3>

      <p>Oya.Auction provides a my account feature on our website which allows you to modify and/or remove most of your
        personal information. This page can be accessed by clicking the my account link at the top of every page on 
        the site. Rest assured that all of your information within my account is secure and accessible only by you or
        limited Oya.Auction personnel.
      </p>

      <h3>How does Oya.Auction use my e-mail address?</h3>

      <p>Your e-mail address is used to create and identify your account on Oya.Auction . Your e-mail address is 
        also used by Oya.Auction to correspond with you about any orders that you may place or to notify you with
        the results of any contests you may have entered. We may also use your e-mail address to notify you about 
        important functionality changes to the Web site, new Oya.Auction services and special offers and promotions
        we think you'll find valuable. If you would rather not receive this information, or would like to opt-out
        of receiving e-mail of this kind, simply click on the "Unsubscribe" link that appears at the bottom of
        any email and follow the simple instructions to have your email address removed from the list of customers
        who receive notices of new functionality, services, special offers and promotions.
      </p>
      
      <h3>Does Oya.Auction use "cookies"?</h3>

      <p>A cookie is a small piece of information sent by a web site that is saved on your hard disk by your computer's
        browser. The cookie holds information a site may need to personalize your experience and to gather web site
        statistical data, such as which pages are visited, the internet provider's domain name and country that our
        visitors come and the addresses of the sites visited immediately before coming to and immediately after
        leaving Oya.Auction.com. However, none of this information is associated with you as an individual. It 
        is measured anonymously and only in the aggregate. The information in the cookies lets us trace your
        "click stream" activity (i.e., the paths taken by visitors to our site as they move from page to page). 
        Cookies do not capture your individual email address or any private information about you.
        Oya.Auction may use third-party advertising companies to serve ads on our web site as well as third party companies
        to provide tracking and analysis of information and data collected from visitors to our web site. 
        These companies may employ cookies and action tags (also known as single pixel gifs or web beacons) to 
        measure advertising effectiveness. Any information that these third parties collect via cookies and action
         tags is completely anonymous and Oya.Auction reserves the right to provide that information and data to these third parties.
      </p>  

      <h3>Your consent</h3>

      <p>You acknowledge that this privacy policy is part of the terms of service and you agree that using this site signifies your
        assent to Oya.Auction 's privacy policy. Oya.Auction reserves the right to change the terms of service, including this
        privacy policy, at any time, without advanced notice. If we decide to change our privacy policy, we will post those changes 
        on this page so that you are always aware of what information we collect, how we use it, and under what circumstances we disclose it.
      </p>  

        </div>
    </div>
  </div>
  <!-- /.main-container -->

  <?php $this->load->view(ciHtmlTheme.'/includes/footer');?>
 
 </div>
     <!-- /.wrapper -->
 
     <!-- Le javascript
 ================================================== -->
     <!-- Placed at the end of the document so the pages load faster -->
     <script src="<?php echo base_url(ciHtmlTheme.'/assets/js/jquery.min.js')?>"> </script>
     <script src="<?php echo base_url(ciHtmlTheme.'/assets/bootstrap/js/bootstrap.min.js')?>"></script>
     <!-- include carousel slider plugin  -->
     <script src="<?php echo base_url(ciHtmlTheme.'/assets/js/owl.carousel.min.js')?>"></script>
     <script src="<?php echo base_url('assets/plugins/chosen/chosen.jquery.min.js')?>"></script>
     <!-- include equal height plugin  -->
     <script src="<?php echo base_url(ciHtmlTheme.'/assets/js/jquery.matchHeight-min.js')?>"></script>
     <!-- include jquery list shorting plugin plugin  -->
     <script src="<?php echo base_url(ciHtmlTheme.'/assets/js/hideMaxListItem.js')?>"></script>
     <!-- include jquery.fs plugin for custom scroller and selecter  -->
     <script src="<?php echo base_url(ciHtmlTheme.'/assets/plugins/jquery.fs.scroller/jquery.fs.scroller.js')?>"></script>
     <script src="<?php echo base_url(ciHtmlTheme.'/assets/plugins/jquery.fs.selecter/jquery.fs.selecter.js')?>"></script>
     <!-- include custom script for site  -->
     <script src="<?php echo base_url(ciHtmlTheme.'/assets/js/jquery.validate.min.js')?>"></script>
     <script src="<?php echo base_url(ciHtmlTheme.'/assets/js/script.js')?>"></script>
     </body>
</html>
