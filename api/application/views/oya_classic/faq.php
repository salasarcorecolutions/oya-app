<?php
defined('BASEPATH') or exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <!-- Fav and touch icons -->
  <link rel="apple-touch-icon-precomposed" sizes="144x144" href="<?php echo base_url(ciHtmlTheme . '/assets/ico/apple-touch-icon-144-precomposed.png') ?>">
  <link rel="apple-touch-icon-precomposed" sizes="114x114" href="<?php echo base_url(ciHtmlTheme . '/assets/ico/apple-touch-icon-114-precomposed.png') ?>">
  <link rel="apple-touch-icon-precomposed" sizes="72x72" href="<?php echo base_url(ciHtmlTheme . '/assets/ico/apple-touch-icon-72-precomposed.png') ?>">
  <link rel="apple-touch-icon-precomposed" href="<?php echo base_url(ciHtmlTheme . '/ico/apple-touch-icon-57-precomposed.png') ?>">
  <link rel="shortcut icon" href="<?php echo base_url(ciHtmlTheme . '/assets/ico/favicon.png') ?>">
  <title>Buyer Registration : Oya Auction</title>
  <!-- Bootstrap core CSS -->
  <link href="<?php echo base_url(ciHtmlTheme . '/assets/bootstrap/css/bootstrap.min.css') ?>" rel="stylesheet">
  <link href="<?php echo base_url('assets/plugins/chosen/chosen.min.css') ?>" rel="stylesheet">
  <!-- Custom styles for this template -->

  <link href="<?php echo base_url(ciHtmlTheme . '/assets/css/style.css') ?>" rel="stylesheet">
  <link href="<?php echo base_url(ciHtmlTheme . '/assets/css/custom.css') ?>" rel="stylesheet">

  <!-- styles needed for carousel slider -->
  <link href="<?php echo base_url(ciHtmlTheme . '/assets/css/owl.carousel.css') ?>" rel="stylesheet">
  <link href="<?php echo base_url(ciHtmlTheme . '/assets/css/owl.theme.css') ?>" rel="stylesheet">

  <script>
    paceOptions = {
      elements: true
    };
  </script>

  <script src="<?php echo base_url(ciHtmlTheme . '/assets/js/pace.min.js') ?>"></script>
</head>

<body>

  <div id="wrapper">
    <!-- /.header -->
    <?php $this->load->view(ciHtmlTheme . '/includes/header'); ?>

    <div style='display:none;' class="faq intro-inner">
      <div class="about-intro">
        <div class="dtable hw100">
          <div class="dtable-cell hw100">

          </div>
        </div>
      </div>
    </div>
    <!-- /.intro-inner -->


    <div style='background-color:white;' class="main-container inner-page">
      <div class="container">
        <div class="section-content">
          <div class="row ">
            <h1 class="text-center title-1"> <strong>FREQUENTLY ASKED QUESTIONS</strong> </h1>
            <hr class="center-block small text-hr">
          </div>
          <div class="faq-content">
            <div class="row">
              <div class="col-lg-3">
                <h3 class="text-center title-1"> <strong>FAQ Group</strong> </h3>
                <ul class="list-group">
                  <?php if (!empty($faq_group)) :

                    $i = 1;
                  ?>
                    <?php foreach ($faq_group as $k => $v) {
                    ?>
                      <a class="showSingle" target="<?php echo $i ?>" onclick="get_list(<?php echo $i ?>,<?php echo $v['id'] ?>)">
                        <li class="list-group-item"><?php echo $v['group_name'] ?></li>
                      </a>
                    <?php
                      $i++;
                    }
                    ?>
                  <?php endif; ?>
                </ul>
              </div>
              <div class="col-lg-9">
                <div class="row search-row animated fadeInUp">
                  <div class="col-lg-8 relative ">
                    <input type="text" id="search" value="<?php echo $search; ?>" placeholder="Search..." class="form-control locinput input-rel searchtag-input ">

                  </div>

                  <div class="col-lg-4 ">
                    <button class="btn btn-primary btn-search btn-block" onclick="get_search()"><i class="fa fa-search"></i><strong> Search</strong></button>
                  </div>
                </div>
                <div class="list">

                </div>
                <!--  <div id="div1" aria-multiselectable="true" role="tablist" class="targetDiv target_active panel-group faq-panel">
                
        
                <div class="panel">
                  <div id="headingThree" role="tab" class="panel-heading">
                    <h4 class="panel-title">
                      <a aria-controls="collapseThree" aria-expanded="false" href="#collapseThree" data-parent="#div1" data-toggle="collapse" class="collapsed">
                        If I post an ad, will I also get more spam e-mails? 
                      </a>
                    </h4>
                  </div>
                  <div aria-labelledby="headingThree" role="tabpanel" class="panel-collapse collapse" id="collapseThree">
                    <div class="panel-body">
                        Pellentesque in mauris placerat, porttitor lorem id, ornare nisl. Pellentesque rhoncus convallis felis, in egestas libero. Donec et nibh dapibus, sodales nisi quis, fringilla augue. Donec dui quam, egestas in varius ut, tincidunt quis ipsum. Nulla nec odio eu nisi imperdiet dictum.
                    </div>
                  </div>
                </div>
              </div>
              <div id="div2" aria-multiselectable="true" role="tablist" class="targetDiv panel-group faq-panel">

                <div class="panel">
                  <div id="heading_04" role="tab" class="panel-heading">
                    <h4 class="panel-title">
                      <a aria-controls="collapse_04" aria-expanded="false" href="#collapse_04" data-parent="#div2" data-toggle="collapse" class="collapsed">
                        How long will my ad remain on the website?  
                      </a>
                    </h4>
                  </div>
                  <div aria-labelledby="heading_04" role="tabpanel" class="panel-collapse collapse" id="collapse_04">
                    <div class="panel-body">
                        Pellentesque in mauris placerat, porttitor lorem id, ornare nisl. Pellentesque rhoncus convallis felis, in egestas libero. Donec et nibh dapibus, sodales nisi quis, fringilla augue. Donec dui quam, egestas in varius ut, tincidunt quis ipsum. Nulla nec odio eu nisi imperdiet dictum.
                    </div>
                  </div>
                </div>
          
                <div class="panel">
                  <div id="heading_05" role="tab" class="panel-heading">
                    <h4 class="panel-title">
                      <a aria-controls="collapse_05" aria-expanded="false" href="#collapse_05" data-parent="#div2" data-toggle="collapse" class="collapsed">
                        I sold my item. How do I delete my ad? 
                      </a>
                    </h4>
                  </div>
                  <div aria-labelledby="heading_05" role="tabpanel" class="panel-collapse collapse" id="collapse_05">
                    <div class="panel-body">
                    Pellentesque in mauris placerat, porttitor lorem id, ornare nisl. Pellentesque rhoncus convallis felis, in egestas libero. Donec et nibh dapibus, sodales nisi quis, fringilla augue. Donec dui quam, egestas in varius ut, tincidunt quis ipsum. Nulla nec odio eu nisi imperdiet dictum.
                    </div>
                  </div>
                </div>
              </div>
              <div id="div3" aria-multiselectable="true" role="tablist" class="targetDiv panel-group faq-panel">
                <div class="panel">
                  <div id="heading_06" role="tab" class="panel-heading">
                    <h4 class="panel-title">
                      <a aria-controls="collapse_06" aria-expanded="false" href="#collapse_06" data-parent="#div3" data-toggle="collapse" class="collapsed">
                        What is a wish list?  
                      </a>
                    </h4>
                  </div>
                  <div aria-labelledby="heading_06" role="tabpanel" class="panel-collapse collapse" id="collapse_06">
                    <div class="panel-body">
                         Pellentesque in mauris placerat, porttitor lorem id, ornare nisl. Pellentesque rhoncus convallis felis, in egestas libero. Donec et nibh dapibus, sodales nisi quis, fringilla augue. Donec dui quam, egestas in varius ut, tincidunt quis ipsum. Nulla nec odio eu nisi imperdiet dictum.
                    </div>
                  </div>
                </div>

                <div class="panel">
                  <div id="heading_04" role="tab" class="panel-heading">
                    <h4 class="panel-title">
                      <a aria-controls="collapse_07" aria-expanded="false" href="#collapse_07" data-parent="#div3" data-toggle="collapse" class="collapsed">
                        How long will my ad remain on the website?  
                      </a>
                    </h4>
                  </div>
                  <div aria-labelledby="heading_04" role="tabpanel" class="panel-collapse collapse" id="collapse_07">
                    <div class="panel-body">
                        Pellentesque in mauris placerat, porttitor lorem id, ornare nisl. Pellentesque rhoncus convallis felis, in egestas libero. Donec et nibh dapibus, sodales nisi quis, fringilla augue. Donec dui quam, egestas in varius ut, tincidunt quis ipsum. Nulla nec odio eu nisi imperdiet dictum.
                    </div>
                  </div>
                </div>
              </div> -->

              </div>
            </div>
          </div>

        </div>
      </div>
    </div>
    <!-- /.main-container -->
    <div class="parallaxbox about-parallax-bottom">
      <div class="container">
        <h1 style='color:#1b2c92;' class="text-center title-1"> WHAT DO WE DO </h1>
        <div class="row text-center featuredbox">

          <div class="col-sm-3 xs-gap">
            <div class="inner">
              <div class="icon-box-wrap"> <i class="fa fa-gavel"></i></div>
              <h3 class="title-4">FORWARD AUCTION</h3>
              <p>Preferred solution for Scrap Management or selling any material.</p>
            </div>
          </div>
          <div class="col-sm-3 xs-gap">
            <div class="inner">
              <div class="icon-box-wrap"> <i class="fa fa-gavel"></i></div>
              <h3 class="title-4">REVERSE AUCTION</h3>
              <p>Best for E-procurement and Logistics solutions.</p>
            </div>
          </div>
          <div class="col-sm-3 xs-gap">
            <div class="inner">
              <div class="icon-box-wrap"> <i class="fa fa-gavel"></i></div>
              <h3 class="title-4">DUTCH AUCTION </h3>
              <p>Highly competitive let the bidders give their best prices.</p>
            </div>
          </div>

          <div class="col-sm-3 xs-gap">
            <div class="inner">
              <div class="icon-box-wrap"> <i class="fa fa-gavel"></i></div>
              <h3 class="title-4">E-TENDER </h3>
              <p>A good solution to get the best price from the bidders.</p>
            </div>
          </div>
        </div>
      </div>
    </div>

    <?php $this->load->view(ciHtmlTheme . '/includes/footer'); ?>

  </div>
  <!-- /.wrapper -->

  <!-- Le javascript
 ================================================== -->
  <!-- Placed at the end of the document so the pages load faster -->
  <script src="<?php echo base_url(ciHtmlTheme . '/assets/js/jquery.min.js') ?>"> </script>
  <script src="<?php echo base_url(ciHtmlTheme . '/assets/bootstrap/js/bootstrap.min.js') ?>"></script>
  <!-- include carousel slider plugin  -->
  <script src="<?php echo base_url(ciHtmlTheme . '/assets/js/owl.carousel.min.js') ?>"></script>
  <script src="<?php echo base_url('assets/plugins/chosen/chosen.jquery.min.js') ?>"></script>
  <!-- include equal height plugin  -->
  <script src="<?php echo base_url(ciHtmlTheme . '/assets/js/jquery.matchHeight-min.js') ?>"></script>
  <!-- include jquery list shorting plugin plugin  -->
  <script src="<?php echo base_url(ciHtmlTheme . '/assets/js/hideMaxListItem.js') ?>"></script>
  <!-- include jquery.fs plugin for custom scroller and selecter  -->
  <script src="<?php echo base_url(ciHtmlTheme . '/assets/plugins/jquery.fs.scroller/jquery.fs.scroller.js') ?>"></script>
  <script src="<?php echo base_url(ciHtmlTheme . '/assets/plugins/jquery.fs.selecter/jquery.fs.selecter.js') ?>"></script>
  <!-- include custom script for site  -->
  <script src="<?php echo base_url(ciHtmlTheme . '/assets/js/jquery.validate.min.js') ?>"></script>
  <script src="<?php echo base_url(ciHtmlTheme . '/assets/js/script.js') ?>"></script>
  <script>
    jQuery(function() {

      get_list(1, <?php echo $faq_id ?>);
    });

    function get_search() {
      var url = '<?php echo base_url('faq') ?>?f=1';
      if ($("#search").val() != "") url = url + "&search=" + $("#search").val();
      window.history.pushState({
        path: url
      }, '', url);
      window.location.reload();
    }

    function get_list(i, grp_id) {
      var search = '<?php echo $search ?>';
      $.ajax({
        url: '<?php echo base_url('faq/get_list') ?>',
        dataType: 'json',
        type: 'POST',
        data: {
          'faq_group': grp_id,
          'search': '<?php echo $search ?>'
        },
        success: function(response) {
          if (response) {
            var html = '';
            var i = 1;
            $.each(response, function(key, v) {
              html += ' <div id="div' + i + '" aria-multiselectable="true" role="tablist" class="targetDiv target_active panel-group faq-panel">';
              html += '<div class="panel">' +
                '<div id="heading' + i + '" role="tab" class="panel-heading">' +
                '<h4 class="panel-title">' +
                '<a aria-controls="collapse' + i + '" aria-expanded="false" href="#collapse' + i + '" data-parent="#div' + i + '" data-toggle="collapse" class="collapsed">' + v['quest'] +
                '</a>' +
                '</h4>' +
                '</div>' +
                '<div aria-labelledby="heading' + i + '" role="tabpanel" class="panel-collapse collapse" id="collapse' + i + '">' +
                '<div class="panel-body">' + v['ans'] +

                '</div>' +
                '</div>' +
                '</div>' +
                '</div>';
              i++;
            });
          }
          console.log(html);
          $('.list').html(html);
          //  $('.targetDiv').slideUp();
          jQuery('#div' + i).slideToggle("slow", function() {});
        }
      });
    }
  </script>
</body>

</html>