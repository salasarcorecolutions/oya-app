<?php
defined('BASEPATH') or exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<!-- Fav and touch icons -->
<link rel="apple-touch-icon-precomposed" sizes="144x144"
	href="<?php echo base_url(ciHtmlTheme.'/assets/ico/apple-touch-icon-144-precomposed.png')?>">
<link rel="apple-touch-icon-precomposed" sizes="114x114"
	href="<?php echo base_url(ciHtmlTheme.'/assets/ico/apple-touch-icon-114-precomposed.png')?>">
<link rel="apple-touch-icon-precomposed" sizes="72x72"
	href="<?php echo base_url(ciHtmlTheme.'/assets/ico/apple-touch-icon-72-precomposed.png')?>">
<link rel="apple-touch-icon-precomposed"
	href="<?php echo base_url(ciHtmlTheme.'/ico/apple-touch-icon-57-precomposed.png')?>">
<link rel="shortcut icon"
	href="<?php echo base_url(ciHtmlTheme.'/assets/ico/favicon.png')?>">
<title>Auctioneer Registration : Oya Auction</title>
<!-- Bootstrap core CSS -->
<link
	href="<?php echo base_url(ciHtmlTheme.'/assets/bootstrap/css/bootstrap.min.css')?>"
	rel="stylesheet">
<link
	href="<?php echo base_url('assets/plugins/chosen/chosen.min.css')?>"
	rel="stylesheet">
<!-- Custom styles for this template -->

<link href="<?php echo base_url(ciHtmlTheme.'/assets/css/style.css')?>"
	rel="stylesheet">
<link href="<?php echo base_url(ciHtmlTheme.'/assets/css/custom.css')?>"
	rel="stylesheet">

<!-- styles needed for carousel slider -->
<link
	href="<?php echo base_url(ciHtmlTheme.'/assets/css/owl.carousel.css')?>"
	rel="stylesheet">
<link
	href="<?php echo base_url(ciHtmlTheme.'/assets/css/owl.theme.css')?>"
	rel="stylesheet">
	<link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/css/select2.min.css" rel="stylesheet" />

<script>
    paceOptions = {
      elements: true
    };
</script>

<script
	src="<?php echo base_url(ciHtmlTheme.'/assets/js/pace.min.js')?>"></script>

</head>
<body>

	<div id="wrapper">
		<!-- /.header -->
 <?php $this->load->view(ciHtmlTheme.'/includes/header');?>
  
  <div class="main-container">
			<div class="container">
				<div class="row">
					<div class="col-md-8 page-content">
						<div class="inner-box category-content">
							<h2 class="title-2">
								<i class="icon-user-add"></i> Company Details
							</h2>
							<div class="row">
								<div class="col-sm-12">
									<form class="form-horizontal" id="vendor_reg_form" method="post">
										<fieldset>


											<!-- Text input-->
											<div class="form-group required">
												<label class="col-md-4 control-label">Company Name <sup>*</sup></label>
												<div class="col-md-6">
													<input type="text" class="form-control"
														id="v_compname"
														placeholder="Company Name" name="v_compname"
														onblur="check_company_v(this.value)">
														<span class="text text-danger col-lg-12 sec_error" id="errc"></span>
												</div>
											</div>

											<!-- Text input-->
											<div class="form-group required">
												<label class="col-md-4 control-label">Company Address<sup>*</sup></label>
												<div class="col-md-6">
													<input type="text" class="form-control" id="comp_address1"
														placeholder="Address Line" name="v_txtAddressLine1"> <input
														type="text" class="form-control" id="comp_address2"
														placeholder="Address Line 2 (optional)"
														name="v_txtAddressLine2">

												</div>
											</div>

											<div class="form-group required">
												<label class="col-md-4 control-label">Select country<sup>*</sup></label>
												<div class="col-md-6">
													<select class="form-control" name="cmp_country" 
														id="cmp_country" onchange="fetch_timezone(this.id);fetch_state(this.id)"
														attr="cmp">
														<option value="">Select Country</option>
														<?php

														if (! empty($countries)) {
															foreach ($countries as $country) {
																?>
																			<option value="<?php echo $country['id'];?>"><?php echo $country['name'];?></option>
																					<?php
															}
														}
														?>
														</select>
												</div>
											</div>

											<div class="form-group required">
												<label class="col-md-4 control-label">Select State<sup>*</sup></label>
												<div class="col-md-6">
													<select class="form-control companyState" name="cmp_state"
														id="cmp_state" attr='cmp' onchange="fetch_city(this.id)">
														<option></option>
													</select>
												</div>
											</div>
											<div class="form-group required" id="StateOther" style="display: none;">
												<label class="col-md-4 control-label">State Other: <sup>*</sup></label>
												<div class="col-md-6">
													<input type="text" class="form-control"
														placeholder="Other State Name" name="b_state_other"
														id="b_state_other" />
												</div>
											</div>
											<div id="c_city_cmp" class="form-group required">
												<label class="col-md-4 control-label">Select City<sup>*</sup></label>
												<div class="col-md-6">
													<select class="form-control" name="cmp_city" id="cmp_city">
														<option></option>
													</select>
												</div>
											</div>
											<div class="form-group required" id="CityOther"  style="display: none;">
												<label class="col-md-4 control-label">City Other: <sup>*</sup></label>
												<div class="col-md-6">
													<input type="text" class="form-control"
														placeholder="Other City Name" name="b_city_other"
														id="b_city_other" />
												</div>
											</div>
											<div class="form-group required">
												<label class="col-md-4 control-label">Pincode <sup>*</sup></label>
												<div class="col-md-6">
													<input type="text" class="form-control" id="comp_pin"
														placeholder="Pin Code" maxlength="6" name="v_pin" />
												</div>
											</div>

											<div class="form-group required">
												<label class="col-md-4 control-label">Timezone<sup>*</sup></label>
												<div class="col-md-6">
													<select class="user_tz form-control br-5" id="user_tz_v"
														name="user_tz_v" require>
														<option></option>
													
														</select>
												</div>
											</div>
											
											<div id="add_branch_div">
												<h2 class="title-2">
													<i class="icon-user-add"></i> Branch Details
												</h2>

												<div class="form-group">
													<label class="col-md-4 control-label"></label>
													<div class="col-md-8">
														<div class="col-md-8">
															<div class="termbox mb10">
																<label class="checkbox-inline" for="sameascompany_1"> <input
																	type="checkbox" id="sameascompany_1"
																	onchange="sameAsCompany(1);" /> Same as company
																</label>
															</div>
														</div>

													</div>
												</div>
												<!-- Text input-->
												<div class="form-group required">
													<label class="col-md-4 control-label">Branch/Plant Name <sup>*</sup></label>
													<div class="col-md-6">
														<input type="text" class="form-control" id="branch_name_1"
															placeholder="Branch/Plant Name" name="v_branch_name[]">
													</div>
												</div>

												<!-- Text input-->
												<div class="form-group required">
													<label class="col-md-4 control-label">Branch Address <sup>*</sup></label>
													<div class="col-md-6">
														<input type="text" class="form-control"
															placeholder="Branch/Plant Address Line" id="add_line_1_1"
															name="v_branch_address1[]"> <input type="text"
															class="form-control"
															placeholder="Branch/Plant Address Line 2(optional)"
															id="add_line_2_1" name="v_branch_address2[]">
													</div>
												</div>

												<div class="form-group required">
													<label class="col-md-4 control-label">Select country<sup>*</sup></label>
													<div class="col-md-6">
														<select class="form-control" name="v_branch_country[]"
															id="vbranch_country_1" attr="v_branch_1" onchange="fetch_state(this.id)">
															<option value="">Select Country</option>
															<?php
															
															if (! empty($countries)) 
															{
																foreach ($countries as $country) 
																{
																	?>
																<option value="<?php echo $country['id'];?>"><?php echo $country['name'];?></option>
																<?php 
																} 
															} ?>
													</select>
													</div>
												</div>

												<div class="form-group required">
													<label class="col-md-4 control-label">Select State<sup>*</sup></label>
													<div class="col-md-6">
														<select class="form-control" name="v_branch_state[]"
															id="v_branch_1_state" onchange="fetch_city(this.id)"
															attr="v_branch_1">
															<option></option>
														</select>
													</div>
												</div>
												<div class="form-group required" id="v_StateOther"  style="display: none;">
													<label class="col-md-4 control-label">State Other: <sup>*</sup></label>
													<div class="col-md-6">
														<input type="text" class="form-control"
															placeholder="Other State Name" name="v_state_other"
															id="v_state_other" />
													</div>
												</div>
												<div id="b_city_cmp" class="form-group required">
													<label class="col-md-4 control-label">Select City<sup>*</sup></label>
													<div class="col-md-6">
														<select class="form-control" name="v_branch_city[]"
															id="v_branch_1_city">
															<option></option>
														</select>
													</div>
												</div>
												<div class="form-group required" id="v_CityOther"  style="display: none;">
													<label class="col-md-4 control-label">City Other: <sup>*</sup></label>
													<div class="col-md-6">
														<input type="text" class="form-control"
															placeholder="Other City Name" name="v_city_other"
															id="v_city_other" />
													</div>
												</div>

												<div class="form-group required">
													<label class="col-md-4 control-label">Pincode <sup>*</sup></label>
													<div class="col-md-6">
														<input type="text" class="form-control"
															placeholder="Pin Code" maxlength="6" id="pin_code_1"
															name="v_branch_pin[]" />
													</div>
												</div>

												
											</div>

											

											<h2 class="title-2">
												<i class="icon-user-add"></i> Personal Details
											</h2>
											<!-- Text input-->
											<div class="form-group required">
												<label class="col-md-4 control-label">Name <sup>*</sup></label>
												<div class="col-md-6">
													<input type="text" class="form-control"
														placeholder="First Name" name="v_first_name"> <input
														type="text" class="form-control" placeholder="Last Name"
														name="v_last_name">
														
												</div>
											</div>

											<!-- Text input-->
											<div class="form-group required">
												<label class="col-md-4 control-label">Username <sup>*</sup></label>
												<div class="col-md-6">
													<input type="text" class="form-control"
														placeholder="Enter Username" id="v_uid" name="v_uid"
														onblur="check_user_v(this.value)">
														<span class="text text-danger col-lg-12 sec_error" id="erru"></span>
												</div>
											</div>

											<div class="form-group required">
												<label class="col-md-4 control-label">Password<sup>*</sup></label>
												<div class="col-md-6">
													<input type="password" class="form-control" id="pass"
														placeholder="Enter Password" name="v_pass">

												</div>
											</div>

											<div class="form-group required">
												<label class="col-md-4 control-label">Confirm Password<sup>*</sup></label>
												<div class="col-md-6">
													<input type="password" class="form-control"
														id="confirmpass" placeholder="Confirm Password"
														name="v_conpass">
												</div>
											</div>

											<div class="form-group required">
												<label class="col-md-4 control-label">Email Id<sup>*</sup></label>
												<div class="col-md-6">
													<input type="text" class="form-control"
														placeholder="Enter Email" class="email" name="v_email"
														id="v_email_id_1" onblur="check_email_v(this.value)" />
														<span class="text text-danger col-lg-12 sec_error" id="erre"></span>
												</div>
											</div>



											<div class="form-group required">
												<label class="col-md-4 control-label">Mobile No <sup>*</sup></label>
												<div class="col-md-6">
													<div class="col-md-4">
														<select class="form-control">
															<option value="">+91</option>
															<option></option>
														</select>
													</div>
													<div class="col-md-8">
														<input type="text" class="form-control"
															placeholder="Mobile Number" id="v_mob_id_1" name="v_mob"
															>
													</div>

												</div>
											</div>
											

											<h2 class="title-2">
												<i class="icon-user-add"></i>Interested In
											</h2>
											<!-- Text input-->
											<div class="form-group required">

												<div class="col-md-6">
													<div class="mb10">
														<label class="checkbox-inline" for="SM"> <input id="SM"
															type="checkbox" name="interest[]"
															value='scrap_management' class="checkBoxClass" /> Scrap
															Management
														</label>
													</div>
												</div>

												<div class="col-md-6">
													<div class="mb10">
														<label class="checkbox-inline" for="LG"> <input id="LG"
															type="checkbox" class="checkBoxClass" name="interest[]"
															value="logistics" /> Logistics
														</label>
													</div>
												</div>

												<div class="col-md-6">
													<div class="mb10">
														<label class="checkbox-inline" for="ET"> <input id="ET"
															type="checkbox" class="checkBoxClass" name="interest[]"
															value="e_tender" /> E-Tender
														</label>
													</div>
												</div>

												<div class="col-md-6">
													<div class="mb10">
														<label class="checkbox-inline" for="PC"> <input id="PC"
															type="checkbox" class="checkBoxClass" name="interest[]"
															value="Procurement" /> Procurement
														</label>
													</div>
												</div>

												<div class="col-md-6">
													<div class="mb10">
														<label class="checkbox-inline" for="checkboxes-1"> <input
															id="FA" type="checkbox" class="checkBoxClass"
															name="interest[]" value="forward_auction" /> Forward
															Auction
														</label>
													</div>
												</div>

												<div class="col-md-6">
													<div class="mb10">
														<label class="checkbox-inline" for="checkboxes-1"> <input
															id="RA" type="checkbox" class="checkBoxClass"
															name="interest[]" value="reverese_auction" /> Reverse
															Auction
														</label>
													</div>
												</div>

												<div class="col-md-6">
													<div class="mb10">
														<label class="checkbox-inline" for="checkboxes-1"> <input
															id="DA" type="checkbox" class="checkBoxClass"
															name="interest[]" value="dutch_auction" /> Dutch Auction
														</label>
													</div>
												</div>


											</div>
											<h2 class="title-2">
												<i class="icon-user-add"></i>Registration Type
											</h2>
											<div class="form-group required">

												<div class="col-md-6">
													<div class="mb10">
														<label class="checkbox-inline" for="Vendor"> 
														<input id="registration_type"
															type="radio" name="registration_type"
															value='V' class="checkBoxClass" /> 
															Vendor
														</label>
														<label class="checkbox-inline" for="Auctioneer"> 
														<input id="registration_type"
															type="radio" name="registration_type"
															value='A' class="checkBoxClass" /> 
															Auctioneer
														</label>
														<div class="col-md-12">
															<label style="display:none;" id="registration_type-error" class="error" for="registration_type">Please Select Registration Type.</label>
														</div>
													</div>
												</div>

											</div>
											<div class="form-group">
												<label class="col-md-3 control-label"></label>
												<div class="col-md-4">

													<a id="vendor_reg_submit" class="btn btn-primary" >Register</a>
												</div>
											</div>
										</fieldset>
									</form>
								</div>
							</div>
						</div>
					</div>
					<!-- /.page-content -->

					<div class="col-md-4 reg-sidebar">
						<div class="reg-sidebar-inner text-center">
							<div class="promo-text-box">
								<i class=" icon-picture fa fa-4x icon-color-1"></i>
								<h3>
									<strong>Create Auction</strong>
								</h3>
								<p>Create your private and public auction, Set wide range of auction types and accept part quantity </p>
							</div>
							<div class="promo-text-box">
								<i class=" icon-pencil-circled fa fa-4x icon-color-2"></i>
								<h3>
									<strong>Invite Buyers</strong>
								</h3>
								<p>Invite buyers to participate your auctions. Work with predefined buyers, Restrict buyers for Private auctions</p>
							</div>
							<div class="promo-text-box">
								<i class="  icon-heart-2 fa fa-4x icon-color-3"></i>
								<h3>
									<strong>Branch Management</strong>
								</h3>
								<p>Create Your branches, Auction branch wise and set different permission to your users  </p>
							</div>
						</div>
					</div>
				</div>
				<!-- /.row -->
			</div>
		</div>
		<!-- /.main-container -->


		<!-- /.claa me  -->
	<?php $this->load->view(ciHtmlTheme.'/includes/call_back');?>
  <!-- /.footer --> 
  <?php $this->load->view(ciHtmlTheme.'/includes/footer');?>
 
</div>
	<!-- /.wrapper -->

	<!-- Le javascript
================================================== -->
	<!-- Placed at the end of the document so the pages load faster -->
	<script
		src="<?php echo base_url(ciHtmlTheme.'/assets/js/jquery.min.js')?>"> </script>
	<script
		src="<?php echo base_url(ciHtmlTheme.'/assets/bootstrap/js/bootstrap.min.js')?>"></script>
	<!-- include carousel slider plugin  -->
	<script
		src="<?php echo base_url(ciHtmlTheme.'/assets/js/owl.carousel.min.js')?>"></script>
	<script
		src="<?php echo base_url('assets/plugins/chosen/chosen.jquery.min.js')?>"></script>
	<!-- include equal height plugin  -->
	<script
		src="<?php echo base_url(ciHtmlTheme.'/assets/js/jquery.matchHeight-min.js')?>"></script>
	<!-- include jquery list shorting plugin plugin  -->
	<script
		src="<?php echo base_url(ciHtmlTheme.'/assets/js/hideMaxListItem.js')?>"></script>
	<!-- include jquery.fs plugin for custom scroller and selecter  -->
	<script
		src="<?php echo base_url(ciHtmlTheme.'/assets/plugins/jquery.fs.scroller/jquery.fs.scroller.js')?>"></script>
	<script
		src="<?php echo base_url(ciHtmlTheme.'/assets/plugins/jquery.fs.selecter/jquery.fs.selecter.js')?>"></script>
	<!-- include custom script for site  -->
	<script
		src="<?php echo base_url(ciHtmlTheme.'/assets/js/jquery.validate.min.js')?>"></script>
	<script src="<?php echo base_url(ciHtmlTheme.'/assets/js/script.js')?>"></script>
	<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/js/select2.min.js"></script>

	<script>
		$(document).ready(function(){
			$("#vbranch_country_1").select2({
				placeholder: 'Select Country',
				width: '100%'
			})
			$("#cmp_country").select2({
				placeholder: 'Select Country',
				width: '100%'
			})
			$("#cmp_state").select2({
				placeholder: 'Select State',
				width: '100%'
			})
			$("#cmp_city").select2({
				placeholder: 'Select City',
				width: '100%'
			})
			$("#v_branch_1_state").select2({
				placeholder: 'Select State',
				width: '100%'
			})
			$("#v_branch_1_city").select2({
				placeholder: 'Select City',
				width: '100%'
			})
		})

	$("#vendor_reg_submit").click(function(e){
		e.preventDefault();
		var form=$("#vendor_reg_form");
		form.validate({
			ignore:[],
			rules: {
				v_compname: {required : true,minlength:3},
				v_txtAddressLine1: {required : true,minlength:4},
				cmp_country: {required : true},
				cmp_state: {required : true},
				v_pin:{required:true,maxlength:6,digits:true},
				user_tz_v: {required : true},
				'v_branch_name[]': {required : true,minlength:4},
				'v_branch_address1[]':{required : true,minlength:4},
				'v_branch_country[]':{required : true},
				v_branch_state:{required : true},
				v_branch_pin:{required : true},
				v_first_name :{required : true,minlength:4},
				v_mob: {required : true,maxlength:10,minlength:10,number:true},
				v_email: {required : true,email:true},
				v_uid: {required : true,minlength:6},
				v_pass: {required : true,minlength:6},
				v_conpass: {equalTo: "#pass"}, 
				registration_type:{required : true},
			},
			messages: {	
				v_compname:"Please Enter Company Name",
				v_txtAddressLine1:"Please Enter Address",
				cmp_country:"Please Select Country",
				cmp_state:"Please Select State",
				cmp_city:"Please Select City",
				v_pin:{required:"Please Enter Pin Code "},
				user_tz_v: "Please Select TimeZone",
				'v_branch_name[]': "Please Enter Branch Name",
				'v_branch_address1[]':"Please Enter Address",
				'v_branch_country[]':"Please Select Country",
				v_branch_state:"Please Select State",
				v_branch_city:"Please Select City",
				v_branch_pin:"Please Enter Pin",
				v_first_name :"Please Enter Name",
				v_mob: {required:"Please Enter Mobile No. ",minlength:"Please Enter Valid Mobile No. ",maxlength:"Please Enter Valid Mobile No. "},
				v_email:{required:"Please Enter Email ",email:"Please Enter Proper Email"},
				v_uid: {required:"Please Enter Username ",minlength:"Please Enter Atleast 8 characters Username"},
				v_pass: {required:"Please Enter Password ",minlength:"Please Enter Atleast 6 characters Password"},
				v_conpass:{required:"Please Enter Confirm Password ",equalTo: "Password does not match.",},
				registration_type:{required:"Please Select Registration Type."},

				},
				
			});

		if(form.valid())
			$( "#vendor_reg_form" ).submit();
	});

	$("#vendor_reg_form").on('submit',(function(e) {
		e.preventDefault();
		var $btn = $("#vendor_reg_submit").button('loading');
		$.ajax({
			url: "<?php echo base_url();?>user/save_temp_vendor",
			type: "POST",
			data:  new FormData(this),
			contentType: false,
			cache: false,
			processData:false,
			dataType:'json',
			success: function(data)
			{
				if(data.success)
				{ 
					alert("Vendor Registered Successfully.")
					location.href="<?php echo base_url('user/verify_vterms')?>"; 
				}
				else
				{
					alert(data.msg);
					$btn.button('reset');
				}
					
			},
			error: function() 
			{
				alert(data.message);
				$btn.button('reset');
			} 	
		});
	}));

  
function check_company_v(comp_name)
{
	$.ajax({
		url:'<?php echo base_url('user/check_company_v')?>',
		type:'POST',
		dataType:'json',
		data:{'comp_name':comp_name},
		success:function(data)
		{
			if (data.status == 'success')
			{
				$("#errc").text(data.message);
			}
			else
			{
				$("#errc").text('');
			}
		},
		error:function()
		{
			alert('Invalid Request');
		}
	});
}

function check_user_v(v_uid){
	$.ajax({
		url:'<?php echo base_url('user/check_user_v')?>',
		type:'POST',
		dataType:'json',
		data:{'v_uid':v_uid},
		success:function(data)
		{
			if (data.status == 'success')
			{
				
				$("#erru").text(data.msg);
			}
			else
			{
				$("#erru").text('');
			}
		},
		error:function()
		{
			alert('Invalid Request');
		}
	});
}

function check_email_v(email)
{
	$.ajax({
		url:'<?php echo base_url('user/check_vendor_email')?>',
		type:'POST',
		dataType:'json',
		data:{'email':email},
		success:function(data)
		{
			if (data.status =='success')
			{
				$("#erre").text(data.message);
			}
			else
			{
				$("#erre").text('');
			}
		},
		error:function()
		{
			alert('Invalid Request');
		}
	});
}



function fetch_timezone(id) {
	
	var country_name = $("#"+id).val();
	 if(country_name){
		 jQuery.ajax({
			url: '<?php echo base_url('user/get_time_zones')?>',
			type:"post",
			async: false,
			dataType:"json",
			data: {'country_name':country_name},
			success: function (resObj){
				if(resObj)
				{
					var i = 0;
					var html ='';
					$.each(resObj, function( index, value ) {
							html+='<option value="'+value.timezone+'"> ('+value.gmt_offset+') &nbsp '+value.time_zone_description+' , &nbsp'+ value.country_name+'</option>';
						i++;
					});
					if (i==1){
						if (id=='cmp_country')
						{
							$("#user_tz_v").html(html);
						}
						else
						{
							$("#user_tz").html(html);
						}
					}else{
						if (id=='cmp_country')
						{
							$("#user_tz_v").append('<option value="">--Select Timezone--</option>');
						}
						else
						{
							$("#user_tz").append('<option value="">--Select Timezone--</option>');
						}
						
					}
					
				}
			},
			error: function(){
				alert('Problem in fetching time zone. Please try again');
			}
		});

	} else {
		
	} 
}

function fetch_state(id,state='')
{
	var country_id = $("#"+id).val();
	var attr = $("#"+id).attr('attr');
	if(country_id){
		jQuery.ajax({
			url: '<?php echo base_url('user/get_state_by_country_id')?>',
			type:"post",
			async: false,
			data: {'country_id':country_id,state_name:state},
			dataType:'json',
			success: function (resObj){
				var html ='<option value="">Select State</option>';
				$.each(resObj,function(i,v)
				{
					html+='<option value="'+v.id+'">'+v.state+'</option>';
				});
				html+='<option value="other">--Other--</option>';
				$("#"+attr+"_state").html(html);
			},
			error: function(){
				alert('Probem in fetching state. Please try again.');
			}
		});

	} else {
		$("#"+attr+"_state").html('<option value="other">Select State</option>');

	}
}

 function fetch_city(id,city='',state='')
{
	if (state){
		var state_id = state;
	} else {
		var state_id = $("#"+id).val();
	}
	var attr = $("#"+id).attr('attr');
	console.log(attr);
	if (state_id !== "" && state_id !== "other"){
		if (attr == 'cmp'){
			$("#StateOther").hide();
			$("#CityOther").hide();
			$("#c_city_cmp").show();
		} else {
			$("#v_CityOther").hide();
			$("#v_StateOther").hide();
			$("#b_city_cmp").show();
		}
		jQuery.ajax({
			url: '<?php echo base_url('user/get_cities_by_state_id');?>',
			type:"post",
			async: false,
			data: {'state_id':state_id,'city_name':city},
			dataType:'json',
			success: function (resObj) {
				var html ='<option value=""></option>';
				$.each(resObj,function(i,v){
					html+='<option value="'+v.id+'">'+v.name+'</option>';
				});
				$("#"+attr+"_city").html(html);
			},
			error: function(){
				alert('Problem in fetching city. Please try again.');
			}
		});
	} else {
		if (attr == 'cmp'){
			$("#StateOther").show();
			$("#CityOther").show();
			$("#c_city_cmp").hide();
		} else {
			$("#v_CityOther").show();
			$("#v_StateOther").show();
			$("#b_city_cmp").hide();
		}
		$("#"+attr+"_city").html('<option value=""></option>');
	}
}


function sameAsCompany(id)
{
	var companyCountry = $("#cmp_country").val();
    var companyState = $("#cmp_state").val();
    var companyCity = $("#cmp_city").val();
	if ($('#sameascompany_'+id).is(':checked')){
		$("#branch_name_"+id).val($("#v_compname").val());
		$("#add_line_1_"+id).val($("#comp_address1").val());
		$("#add_line_2_"+id).val($("#comp_address2").val());
		$("#vbranch_country_"+id).val($("#cmp_country").val()).trigger('change');
		fetch_state('vbranch_country_'+id,companyState);
		fetch_city("v_branch_"+id+"_state",companyCity,companyState);
		$("#v_branch_"+id+"_state").val($("#cmp_state").val()).trigger('change');
		$("#v_branch_"+id+"_city").val($("#cmp_city").val()).trigger('change');
		$("#pin_code_"+id).val($("#comp_pin").val());
		$("#v_state_other").val($("#b_state_other").val());
		$("#v_city_other").val($("#b_city_other").val());
	} else {
		$("#branch_name_"+id).val("");
		$("#add_line_1_"+id).val("");
		$("#add_line_2_"+id).val("");
		$("#vbranch_country_"+id).val("").trigger('change');
		$("#v_branch_"+id+"_state").val("").trigger('change');
		$("#v_branch_"+id+"_city").val("").trigger('change');
		$("#pin_code_"+id).val("");
		$("#v_state_other").val("");
		$("#v_city_other").val("");
 	}
}
 </script>
</body>
</html>
