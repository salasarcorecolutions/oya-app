<?php
defined('BASEPATH') or exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<!-- Fav and touch icons -->
<link rel="apple-touch-icon-precomposed" sizes="144x144"
	href="<?php echo base_url(ciHtmlTheme.'/assets/ico/apple-touch-icon-144-precomposed.png')?>">
<link rel="apple-touch-icon-precomposed" sizes="114x114"
	href="<?php echo base_url(ciHtmlTheme.'/assets/ico/apple-touch-icon-114-precomposed.png')?>">
<link rel="apple-touch-icon-precomposed" sizes="72x72"
	href="<?php echo base_url(ciHtmlTheme.'/assets/ico/apple-touch-icon-72-precomposed.png')?>">
<link rel="apple-touch-icon-precomposed" href="<?php echo base_url(ciHtmlTheme.'/ico/apple-touch-icon-57-precomposed.png')?>">
<link rel="shortcut icon" 	href="<?php echo base_url(ciHtmlTheme.'/assets/ico/favicon.png')?>">
<title>Buyer Registration : Oya Auction</title>
<!-- Bootstrap core CSS -->
<link href="<?php echo base_url(ciHtmlTheme.'/assets/bootstrap/css/bootstrap.min.css')?>"	rel="stylesheet">
<link href="<?php echo base_url('assets/plugins/chosen/chosen.min.css')?>" rel="stylesheet">
<!-- Custom styles for this template -->

<link href="<?php echo base_url(ciHtmlTheme.'/assets/css/style.css')?>"	rel="stylesheet">
<link href="<?php echo base_url(ciHtmlTheme.'/assets/css/custom.css')?>" rel="stylesheet">

<!-- styles needed for carousel slider -->
<link href="<?php echo base_url(ciHtmlTheme.'/assets/css/owl.carousel.css')?>" rel="stylesheet">
<link href="<?php echo base_url(ciHtmlTheme.'/assets/css/owl.theme.css')?>" rel="stylesheet">

<script>
    paceOptions = {
      elements: true
    };
</script>

<script	src="<?php echo base_url(ciHtmlTheme.'/assets/js/pace.min.js')?>"></script>
	
</head>
<body>

	<div id="wrapper">
		<!-- /.header -->
 <?php $this->load->view(ciHtmlTheme.'/includes/header');?>
  
  <div class="main-container">
			<div class="container">
				<div class="row">
					<div class="col-md-8 page-content">
						<div class="inner-box category-content">
							<h2 class="title-2">
								<i class="icon-user-add"></i> Create your BUYER account, Its free
							</h2>
							<div class="row">
								<div class="col-sm-12">
									<form class="form-horizontal" id="frmRegister" method="post">
										<fieldset>


											<!-- Text input-->
											<div class="form-group required">
												<label class="col-md-4 control-label">Company Name : <sup>*</sup></label>
												<div class="col-md-6">
													<input id="b_compname" class="form-control"	name="b_compname" type="text" placeholder="Company Name" onblur="check_buyer_company(this.value)" /> 
														<span class="text text-danger col-log-6" id="err1"></span>
												</div>
											</div>
											<div class="form-group required">
												<label class="col-md-4 control-label">Company Address : <sup>*</sup></label>
												<div class="col-md-6">
													<input type="text" class="form-control"
														placeholder="Address Line" name="b_txtAddressLine1"
														id="b_txtAddressLine1" />
												</div>
											</div>

											<!-- Text input-->
											<div class="form-group required">
												<label class="col-md-4 control-label">Country : <sup>*</sup></label>
												<div class="col-md-6">
													<select class="form-control" id="b_country" name="b_country"
														onchange="fetch_timezone(this.id);fetch_state(this.id);"
														attr="bidder">
														<option value="">Select Country</option>
														<?php

															if (! empty($countries)) {
															foreach ($countries as $country) {
																?>
																		<option value="<?php echo $country['id'];?>"><?php echo $country['name'];?></option>
																				<?php
															
																}
															}
														?>
													</select>
												</div>
											</div>

											
											<div class="form-group required">
												<label class="col-md-4 control-label">State : <sup>*</sup></label>
												<div class="col-md-6">
													<select class="add_select2 form-control" id="b_state" name="b_state" onchange="fetch_city(this.id)" attr= "bidder" >
														<option value="">Select State</option>
													</select>
												
												</div>
											</div>
											<div class="form-group hide required" id="StateOther">
												<label class="col-md-4 control-label">State Other: <sup>*</sup></label>
												<div class="col-md-6">
												<input type="text" class="form-control" placeholder="Other State Name" name="b_state_other" id="b_state_other" />
													
												
												</div>
											</div>
											
											<div class="form-group required">
												<label class="col-md-4 control-label">City : <sup>*</sup></label>
												<div class="col-md-6">
													<select class="add_select2 form-control" name="b_city" id="b_city" >
																<option value="">Select City</option>
																<option></option>
															</select>
												
												</div>
											</div>
											<div class="form-group hide required" id="CityOther">
												<label class="col-md-4 control-label">City Other: <sup>*</sup></label>
												<div class="col-md-6">
												<input type="text" class="form-control" placeholder="Other City Name" name="b_city_other" id="b_city_other" />
													
												
												</div>
											</div>
											<div class="form-group required">
												<label class="col-md-4 control-label">Time Zone : <sup>*</sup></label>
												<div class="col-md-6">
													<select class="user_tz form-control br-5" id="user_tz" name="user_tz" require>
														<option value=""> --Select Your Timezone--</option>
														<?php  foreach ($time_zones as $value): ?>
															<option value="<?php echo $value['timezone']; ?>"><?php echo  '('.$value['gmt_offset'].') &nbsp' .$value['time_zone_description'].', '.$value['country_name'];  ?></option>
														<?php endforeach; ?>
													</select> 
												</div>
											</div>
											<div class="form-group required">
												<label class="col-md-4 control-label">Pin : <sup>*</sup></label>
												<div class="col-md-6">
													<input type="text" class="form-control"
														placeholder="Pin Code" name="b_pin"
														id="b_pin" />
												</div>
											</div>
											<div class="form-group required">
												<label for="inputEmail3" class="col-md-4 control-label">Your Name<sup>*</sup>
												</label>
												<div class="col-md-6">
													<input type="text" class="form-control" placeholder="Your Name" id="b_conperson" name="b_conperson" />
												</div>
											</div>
											<div class="form-group required">
												<label for="inputPassword3" class="col-md-4 control-label">Mobile No. :	<sup>*</sup></label>
												<div class="col-md-6">
													<input type="text" class="form-control" placeholder="Mobile Number" name="b_mob" id="b_mob" maxlength="10" />
												</div>
											</div>
											<div class="form-group required">
												<label for="inputPassword3" class="col-md-4 control-label">Email :	<sup>*</sup></label>
												<div class="col-md-6">
													<input type="text" class="form-control" placeholder="Email Id" name="b_email" id="b_email" onblur="check_email(this.value)" />
													<span class="text text-danger col-log-6" id="errEmail"></span>
												</div>
											</div>
											<div class="form-group required">
												<label for="inputPassword3" class="col-md-4 control-label">User Id :<sup>*</sup></label>
												<div class="col-md-6">
													<input type="text" class="form-control" placeholder="User Id" id="b_uid" name="b_uid" maxlength="50" onblur="check_user(this.value)" />
													<span class="text text-danger col-log-6" id="err3"></span>
												</div>
											</div>
											<div class="form-group required">
												<label for="inputPassword3" class="col-md-4 control-label">Password :<sup>*</sup></label>
												<div class="col-md-6">
													<input type="password" class="form-control" placeholder="Enter Password" name="b_pass" id="b_pass" maxlength="50" />
												</div>
											</div>
											<div class="form-group required">
												<label for="inputPassword3" class="col-md-4 control-label">Confirm Password :<sup>*</sup></label>
												<div class="col-md-6">
													<input type="password" class="form-control" placeholder="Retype Password" id="b_conpass" name="b_conpass"  maxlength="50" />
												</div>
											</div>
											
											<div class="form-group required">
												<label for="inputPassword3" class="col-md-4 control-label"></label>
												<div class="col-md-6">
													<button class="btn btn-primary" id="btnRegister" >Register</button>
												</div>
											</div>
											
										</fieldset>
									</form>
								</div>
							</div>
						</div>
					</div>
					<!-- /.page-content -->

					<div class="col-md-4 reg-sidebar">
						<div class="reg-sidebar-inner text-center">
							<div class="promo-text-box">
								<i class=" icon-picture fa fa-4x icon-color-1"></i>
								<h3>
									<strong>Get Notification</strong>
								</h3>
								<p>Update your field of interest and get notifications of auctions and products around you. </p>
							</div>
							<div class="promo-text-box">
								<i class=" icon-pencil-circled fa fa-4x icon-color-2"></i>
								<h3>
									<strong>Participate Auctions</strong>
								</h3>
								<p>Let Auctioneer invite you to participate auctions or bid on a product. Update your field of interests</p>
							</div>
							<div class="promo-text-box">
								<i class="  icon-heart-2 fa fa-4x icon-color-3"></i>
								<h3>
									<strong>Favorite Auctions and Products.</strong>
								</h3>
								<p>Add Favourite Auctions and Products for your future referance</p>
							</div>
						</div>
					</div>
				</div>
				<!-- /.row -->
			</div>
		</div>
		<!-- /.main-container -->

		<!-- Terms and conditions -->
		<div id="terms_n_condition" class="modal fade" role="dialog">
			<div class="modal-dialog">
				<div class="modal-content" >
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal">&times;</button>
						<h4 class="modal-title" id="modal_header_text">Terms & Condition</h4>
					</div>
					<div class="modal-body" style="width:800px;height:500px"> 
						<iframe  id="iframe" src="<?php echo base_url('assets/terms/register_terms.pdf#toolbar=0');?>" style="height:100%;width:75%;"></iframe>
					</div>
					<div class="modal-footer">
						<input style="color: white;" class="btn btn-primary" type="button" value="Accept" onclick="submit_terms();" name="accept" id='accept' />
						<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
					</div> 
				</div>
				<input type="hidden" value="1" name="reg"> 
			</div>
		</div>


		<!-- /.claa me  -->
	<?php $this->load->view(ciHtmlTheme.'/includes/call_back');?>
  <!-- /.footer --> 
  <?php $this->load->view(ciHtmlTheme.'/includes/footer');?>
 
</div>
	<!-- /.wrapper -->

	<!-- Le javascript
================================================== -->
	<!-- Placed at the end of the document so the pages load faster -->
	<script	src="<?php echo base_url(ciHtmlTheme.'/assets/js/jquery.min.js')?>"> </script>
	<script	src="<?php echo base_url(ciHtmlTheme.'/assets/bootstrap/js/bootstrap.min.js')?>"></script>
	<!-- include carousel slider plugin  -->
	<script	src="<?php echo base_url(ciHtmlTheme.'/assets/js/owl.carousel.min.js')?>"></script>
	<script src="<?php echo base_url('assets/plugins/chosen/chosen.jquery.min.js')?>"></script>
	<!-- include equal height plugin  -->
	<script	src="<?php echo base_url(ciHtmlTheme.'/assets/js/jquery.matchHeight-min.js')?>"></script>
	<!-- include jquery list shorting plugin plugin  -->
	<script	src="<?php echo base_url(ciHtmlTheme.'/assets/js/hideMaxListItem.js')?>"></script>
	<!-- include jquery.fs plugin for custom scroller and selecter  -->
	<script	src="<?php echo base_url(ciHtmlTheme.'/assets/plugins/jquery.fs.scroller/jquery.fs.scroller.js')?>"></script>
	<script	src="<?php echo base_url(ciHtmlTheme.'/assets/plugins/jquery.fs.selecter/jquery.fs.selecter.js')?>"></script>
	<!-- include custom script for site  -->
	<script	src="<?php echo base_url(ciHtmlTheme.'/assets/js/jquery.validate.min.js')?>"></script>
	<script src="<?php echo base_url(ciHtmlTheme.'/assets/js/script.js')?>"></script>
	<script>    
	

function submit_terms(){ 
	var form=$("#frmRegister");
	 
	$.ajax({
		url: "<?php echo base_url();?>user/save_temp_buyer",
		type: "POST",
		data: $('#frmRegister').serialize(),
		dataType:'json',
		success: function(data)
		{ 
			if(data.success)
			{
				//alert("<?php echo base_url()?>user/verify_email");
				alert('Thank you for your Interest\n Please verify your email to unlock your account');
				location.href="<?php echo base_url('user/verify_email')?>";
			}
			else
			{
				alert(data.msg);
			 	$btn.button('reset');
			}
			 	
		},
		error: function() 
		{
			alert(data.message);
			$btn.button('reset');
		}
		  	
	});
};

$("#btnRegister").click(function(e){
	 e.preventDefault();
	var form=$("#frmRegister");
	form.validate({
	    ignore:[],
		rules: {
			
			b_compname: {required : true,minlength:3},
			b_txtAddressLine1: {required : true,minlength:4},
			b_country: {required : true},
			b_state: {required : true},
			b_city: {required : true},
			b_pin: {required : true,minlength:6},
			user_tz: {required : true},
			b_conperson: {required : true},
			b_mob: {required : true,maxlength:10,minlength:10,number:true},
			b_email: {required : true,email:true},
			b_uid: {required : true,minlength:6},
			b_pass: {required : true,minlength:6},
			b_conpass: {equalTo: "#b_pass"},
			
			},
		messages: {	
			b_compname:"Please Enter Company Name",
			b_txtAddressLine1:"Please Enter Address",
			b_txtAddressLine1:{required:"Please Enter Address "},
			b_conperson:{required:"Please Enter Contact Person "},
			b_city:{required:"Please Enter City/Town "},
			b_pin:{required:"Please Enter Pin Code "},
			b_state:{required:"Please Enter State "},
			b_country:{required:"Please Enter Country "},
			b_tel:{digits:"Please Enter Valid Number "},
			b_mob:{required:"Please Enter Mobile No. ",minlength:"Please Enter Valid Mobile No. ",maxlength:"Please Enter Valid Mobile No. "},
			b_email:{required:"Please Enter Email ",email:"Please Enter Proper Email"},
			b_uid:{required:"Please Enter Username ",minlength:"Please Enter Atleast 8 characters Username"},
			b_pass:{required:"Please Enter Password ",minlength:"Please Enter Atleast 6 characters Password"},
			b_conpass:{required:"Please Enter Confirm Password ",equalTo: "Password does not match.",},
			user_tz:{required:"Please Select Timezone"}
			
			},
			
		});

	if(form.valid())
	 	$( "#frmRegister" ).submit();
	

	
});
$("#frmRegister").on('submit',(function(e) {
	
	e.preventDefault();
	
	var $btn = $("#btnRegister").button('loading');
	$.ajax({
		url: "<?php echo base_url();?>user/check_temp_buyer",
		type: "POST",
		data:  new FormData(this),
		contentType: false,
		cache: false,
		processData:false,
		dataType:'json',
		success: function(data)
		{
			if(data.success)
			{
				$("#terms_n_condition").modal({backdrop: 'static', keyboard: false}); 
			}
			else
			{
				alert(data.msg);
			 	$btn.button('reset');
			}
			 	
			},
		error: function() 
		{
			alert(data.message);
			$btn.button('reset');
		} 	
	});
}));


function check_buyer_company(comp_name)
{
	$.ajax({
		url:'<?php echo base_url('user/check_buyer_company')?>',
		type:'POST',
		dataType:'json',
		data:{'comp_name':comp_name},
		success:function(data)
		{
			if (data.status=='success')
			{
				$("#err1").text(data.message);
			}
			else
			{
				$("#err1").text('');
			}
		},
		error:function()
		{
			alert('Invalid Request');
		}
	});
}



function fetch_timezone(id) {
	
	var country_id = $("#"+id).val(); 
	 if(country_id){
		 jQuery.ajax({
			url: '<?php echo base_url('user/get_time_zones')?>',
			type:"post",
			async: false,
			dataType:"json",
			data: {'country_name':country_id},
			success: function (resObj){
				if(resObj)
				{
					var i = 0;
					var html ='';
					$.each(resObj, function( index, value ) {
							html+='<option value="'+value.timezone+'"> ('+value.gmt_offset+') &nbsp '+value.time_zone_description+' , &nbsp'+ value.country_name+'</option>';
						i++;
					}); 
					if (i==1){
						$("#user_tz").html(html);
						
					}else{
						
							$("#user_tz").append('<option value="">--Select Timezone--</option>');
						
						
					}
					
				}
			},
			error: function(){
				
			}
		});

	} else {
		
	} 
}
function check_user(uid)
{
	var uid = $("#b_uid").val();
	$.ajax({
		url:'<?php echo base_url('user/check_buyer_user')?>',
		type:'POST',
		dataType:'json',
		data:{'uid':uid},
		success:function(data)
		{
			if (data.status=='success')
			{
				
				$("#err3").text(data.message);
			}
			else
			{
				$("#err3").text('');
			}
		},
		error:function()
		{
			alert('Invalid Request');
		}
	})
}
function check_email(email)
{
	var comp_name = $("#b_compname").val();
	$.ajax({
		url:'<?php echo base_url('user/check_buyer_email')?>',
		type:'POST',
		dataType:'json',
		data:{'comp_name':comp_name,'email':email},
		success:function(data)
		{
			
			if (data.status)
			{
				
				$("#errEmail").text(data.message);
			}
			else
			{
				$("#errEmail").text('');
			}
		},
		error:function()
		{
			alert('Invalid Request');
		}
	});
}
 function fetch_state(id,state='')
{
	var country_id = $("#"+id).val();
	var attr = $("#"+id).attr('attr');
	if(country_id){
		 jQuery.ajax({
			url: '<?php echo base_url('user/get_state_by_country_id')?>',
			type:"post",
			async: false,
			data: {'country_id':country_id,state_name:state},
			dataType:'json',
			success: function (resObj){
				
				var html ='<option value="">Select State</option>';
				$.each(resObj,function(i,v)
				{
					html+='<option value="'+v.id+'">'+v.state+'</option>';
				});
				 
				$("#b_state").html(html);
				$("#b_state").append($('<option></option>').val('0').html('--OTHER--'));
			},
			error: function(){
				$("#b_state").html('<option value="">Select State</option>');
				
			}
		});

	} else {
		$("#b_state").html('<option value="">Select State</option>');
		
	}
}

 function fetch_city(id,city='',state='')
{
	if (state){
		var state_id = state;
	} else {
		var state_id = $("#"+id).val();
	}
	var attr = $("#"+id).attr('attr');
	if(state_id !== ""){
		 jQuery.ajax({
			url: '<?php echo base_url('user/get_cities_by_state_id');?>',
			type:"post",
			async: false,
			data: {'state_id':state_id,'city_name':city},
			dataType:'json',
			success: function (resObj) {

				var html ='<option value="">Select City</option>';
				$.each(resObj,function(i,v)
				{
					html+='<option value="'+v.id+'">'+v.name+'</option>';
				});
				
				$("#b_city").html(html);
				$("#b_city").append($('<option></option>').val('0').html('--OTHER--'));
			
			},
			error: function(){
				$("#b_city").html('<option value="">Select City</option>');
				
			}
		});
	} else {
		$("#b_city").html('<option value="">Select City</option>');
		
	}
}

	
$("#b_city").on("change",function(){

	if($("#b_city").val()=="0")
	{
		$("#CityOther").addClass("show");
		$("#CityOther").removeClass("hide");
		
	}
	else
	{
		$("#CityOther").addClass("hide");
		$("#CityOther").removeClass("show");
		}
	});
$("#b_state").on("change",function(){

	if($("#b_state").val()=="0")
	{
		$("#StateOther").addClass("show");
		$("#StateOther").removeClass("hide");
		
	}
	else
	{
		$("#StateOther").addClass("hide");
		$("#StateOther").removeClass("show");
		}
	});


 </script>
</body>
</html>
