<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<!-- Fav and touch icons -->
<link rel="apple-touch-icon-precomposed" sizes="144x144" href="<?php echo base_url(ciHtmlTheme.'/assets/ico/apple-touch-icon-144.png')?>">
<link rel="apple-touch-icon-precomposed" sizes="114x114" href="<?php echo base_url(ciHtmlTheme.'/assets/ico/apple-touch-icon-114.png')?>">
<link rel="apple-touch-icon-precomposed" sizes="72x72" href="<?php echo base_url(ciHtmlTheme.'/assets/ico/apple-touch-icon-72.png')?>">
<link rel="apple-touch-icon-precomposed" sizes="57x57" href="<?php echo base_url(ciHtmlTheme.'/assets/ico/apple-touch-icon-57.png')?>">
<link rel="shortcut icon" href="<?php echo base_url(ciHtmlTheme.'/assets/ico/favicon.png')?>">
<title>Oya Auction | World of Auction | Used Heavy Equipment for Sale | Heavy Equipment Auctions</title>
<meta name="keywords" content="Online auction, Online bidding, Auctioneers, Live auctions, Used Heavy Equipment for Sale, Heavy Equipment Auctions, Auctions around World, Fordard Auctions, Reverse Auctions, eTendering, Yankee Auction">		
<!-- Google / Search Engine Tags -->
<meta itemprop="name" content="Secured Online Auctions|Best Online Bidding Site|Live Online Auctions">
<meta itemprop="description" content="Oya.Auction is the best online auction site around World, providing trust worthy solutions to auctioneers and bidders all across verious countries since years.">

<!-- Bootstrap core CSS -->
<link href="<?php echo base_url(ciHtmlTheme.'/assets/bootstrap/css/bootstrap.min.css')?>" rel="stylesheet">
<!-- Custom styles for this template -->
<link href="<?php echo base_url(ciHtmlTheme.'/assets/css/style.css')?>" rel="stylesheet">
<link href="<?php echo base_url(ciHtmlTheme.'/assets/css/custom.css')?>" rel="stylesheet">
<!-- styles needed for carousel slider -->
<link href="<?php echo base_url(ciHtmlTheme.'/assets/css/owl.carousel.css')?>" rel="stylesheet">
<link href="<?php echo base_url(ciHtmlTheme.'/assets/css/owl.theme.css')?>" rel="stylesheet">
<link href="<?php echo base_url(ciHtmlTheme.'/assets/css/mdtimepicker.css'); ?>" rel="stylesheet" />

<script>
    paceOptions = {
      elements: true
    };
</script>
<script src="<?php echo base_url(ciHtmlTheme.'/assets/js/pace.min.js')?>"></script>

 </head>
<body>

<div id="wrapper">
<!-- /.header -->
 <?php $this->load->view(ciHtmlTheme.'/includes/header');?>
  <!-- /.header -->
  
  <div class="intro">
    <div class="dtable hw100">
      <div class="dtable-cell hw100">
        <div class="container text-center">
       <h1 class="intro-title animated fadeInDown"> Find Auctions  </h1>
          <p class="sub animateme fittext3 animated fadeIn"> Welcome to world of Auctions </p>
      
          <div class="row search-row animated fadeInUp">
              <div class="col-lg-4 col-sm-4 search-col relative looking_for"> <i class="icon-docs icon-append"></i>
                <select style='' class="form-control selecter" name="category" id="search-category" >
                    <option selected="selected" value=""><i class="icon-search"></i>I'm looking for a ...</option>
                    <option value="A" style="background-color:#E9E9E9;font-weight:bold;"> Auction </a></option>
                    <option value="P" style="background-color:#E9E9E9;font-weight:bold;">Products </a></option>
                </select>
              </div>
              <div class="col-lg-4 col-sm-4 search-col relative locationicon">
               <i class="icon-location-2 icon-append"></i>
                <input type="text" name="country" id="autocomplete-ajax"  class="form-control locinput input-rel searchtag-input has-icon" placeholder="City/Zipcode..." value="">

              </div>
              <div class="col-lg-4 col-sm-4 search-col">
                <button class="btn btn-primary btn-search btn-block" onclick="search();"><i class="icon-search"></i><strong>Find</strong></button>
              </div>
            </div>
          
        </div>
      </div>
    </div>
  </div>
  <!-- /.intro -->
  
  <div class="main-container">
    <div class="container">

        <div class="col-lg-12 content-box ">
                <div class="row row-featured row-featured-category">
                    <div class="col-lg-12  box-title no-border">
                       <div class="inner"><h2><span>Browse by product</span>
                           Category     <a href="<?php echo base_url('products')?>" class="sell-your-item"> View more <i class="  icon-th-list"></i> </a></h2>
                       </div>
                    </div>
                    <?php
                    if ( ! empty($product_categories))
                    {
                        foreach ($product_categories as $items)
                        {
                            $img = '';
                            if(sizeof($items['sub_menu'])>0)
                            {
                                $img_src = AMAZON_BUCKET.'/'.s3BucketTestDir.'images/prod_cat_image/'.$items['category_image'];
                            ?>

                                <div class="col-lg-2 col-md-3 col-sm-3 col-xs-4 f-category">
                                 <a href="<?php echo base_url('products?p=1&cat='.$items['category_name'])?>"><img src="<?php echo $img_src?>" class="img-responsive" alt="img"> <h6><?php echo $items['category_name'] ?>  </h6> </a>
                               </div>
                            <?php
                            }  
                        }
                    }
                      
                    ?>

                </div>



            </div>

        <div style="clear: both"></div>

        <div class="col-lg-12 content-box ">
                <div class="row row-featured row-featured-category">
                    <div class="col-lg-12  box-title no-border">
                       <div class="inner"><h2><span>Browse by auction</span>
                           Category     <a href="<?php echo base_url('auction')?>" class="sell-your-item"> View more <i class="  icon-th-list"></i> </a></h2>
                       </div>
                    </div>
                    <?php
                       
                      foreach ($auction_categories as $items)
                      {
                        
                       
                        if(sizeof($items['sub_menu'])>0)
                        {
                            $img_src = AMAZON_BUCKET.'/'.s3BucketTestDir.'images/auction_cat_image/'.$items['category_image'];
                          ?>

                        <div class="col-lg-2 col-md-3 col-sm-3 col-xs-4 f-category">
                            <a href="<?php echo base_url('auction?a=1&cat='.$items['category_name'])?>">
                            <img src="<?php echo $img_src?>" class="img-responsive" alt="img"> <h6><?php echo $items['category_name'] ?>  </h6> </a>
                        </div>
                    <?php
                        }
			    			     
                      }
                    ?>


                </div>



            </div>

        <div style="clear: both"></div>

        <div class="col-lg-12 content-box ">
            <div class="row row-featured">
                <div class="col-lg-12  box-title ">
                    <div class="inner"><h2><span> </span>
                        Latest Products <a href="<?php echo base_url('products')?>" class="sell-your-item"> View more <i class="  icon-th-list"></i> </a></h2>


                    </div>
                </div>

                <div style="clear: both"></div>

                <div class=" relative  content featured-list-row clearfix">

                    <nav class="slider-nav has-white-bg nav-narrow-svg">
                        <a class="prev">
                            <span class="nav-icon-wrap"></span>

                        </a>
                        <a class="next">
                            <span class="nav-icon-wrap"></span>
                        </a>
                    </nav>

                    <div class="no-margin featured-list-slider ">
                        <?php 
                        if ( ! empty($latest_products))
                        {
                            foreach($latest_products as $key=>$value)
                            {
                                if ( ! empty($value['image_name']))
                                {
                                    $url = AMAZON_BUCKET.'/'.s3BucketTestDir.'images/auctionpic/salvage/'.$value['image_name'];
                                }
                                else
                                {
                                    $url = "https://www.iconbunny.com/icons/media/catalog/product/1/0/1087.12-shopping-bag-icon-iconbunny.jpg";
                                }
                                
                        ?>
                                <div class="item ">
                                <a href="<?php echo base_url('products/view_product_details/'.$value['url'])?>">
                                    <span class="item-carousel-thumb">
                                        <img class="img-responsive" src="<?php echo $url?>" alt="<?php echo $value['product_name']?>" style="height:75px;" >
                                    </span>
                                    <span class="item-name"> <?php echo $value['product_name']?> </span>
                                    <span class="price"> <?php echo $value['price'].$value['price_currency']?> </span>
                                </a>
                                </div>
                        <?php
                            } 
                        }
                        ?>
                        
                     </div>

                </div>
            </div>
        </div>
        <div class="col-lg-12 content-box ">   
            <div class="row row-featured">
                <div class="col-lg-12  box-title ">
                    <div class="inner"><h2><span> </span>
                        Latest Auctions <a href="<?php echo base_url('auction')?>" class="sell-your-item"> View more <i class="  icon-th-list"></i> </a></h2>


                    </div>
                </div>

                <div style="clear: both"></div>

                <div class=" relative  content featured-list-row clearfix ">

                    <nav class="slider-nav has-white-bg nav-narrow-svg">
                        <a class="prev">
                            <span class="nav-icon-wrap"></span>

                        </a>
                        <a class="next">
                            <span class="nav-icon-wrap"></span>
                        </a>
                    </nav>

                    <div class="no-margin featured-list-slider owl ">
                        <?php 
                        if ( ! empty($latest_auctions))
                        {
                            foreach($latest_auctions as $key=>$value)
                            {
                                if ( ! empty($value['image']))
                                {
                                    if ($value['auction_type'] == 4)
                                    {
                                        $url = AMAZON_BUCKET.'/'.s3BucketTestDir.'images/auctionpic/yankee/'.$value['image'];

                                    }
                                    else
                                    {
                                        $url = AMAZON_BUCKET.'/'.s3BucketTestDir.'images/auctionpic/tender/'.$value['image'];

                                    }
                                }
                                else
                                {
                                    $url = "https://www.iconbunny.com/icons/media/catalog/product/1/0/1087.12-shopping-bag-icon-iconbunny.jpg";
                                }
                                
                        ?>
                                <div class="item  ">
                                <a href="<?php echo base_url('auction/view_details/'.md5($value['saleno']))?>">
                                    <span class="item-carousel-thumb">
                                        <img class="img-responsive" src="<?php echo $url?>" alt="<?php echo $value['saleno']?>"  style="height:75px" >
                                    </span>
                                    <span class="item-name"> <?php echo $value['vendor_name']?> </span>
                                    <span class=""> <?php echo $value['saleno']?> </span>
                                </a>
                                </div>
                        <?php
                            } 
                        }
                        ?>
                        
                     </div>

                </div>
            </div>
        </div>

        


        <div class="col-lg-12 content-box ">
            <div class="row row-featured">

                <div style="clear: both"></div>

                <div class=" relative  content  clearfix">


                    <div class="">
                        <div class="tab-lite">

                            <!-- Nav tabs -->
                            <ul class="nav nav-tabs " role="tablist">
                                <li role="presentation" class="active"><a href="#tab1" aria-controls="tab1" role="tab" data-toggle="tab"><i class="icon-location-2"></i> Top Locations</a></li>
                                <li ><a href="<?php echo base_url('auction');?>"><i class="icon-search"></i>  Top Search</a></li>
                                <li role="presentation"><a href="#tab3" aria-controls="tab3" role="tab" data-toggle="tab"><i class="icon-th-list"></i> Top Makes</a></li>
                            </ul>

                            <!-- Tab panes -->
                            <div class="tab-content">
                                <div role="tabpanel" class="tab-pane active" id="tab1">

                                    <div class="col-lg-12 tab-inner">

                                        <div class="row"> 
                                            <?php foreach($top_locations as $top_loc) { ?>
                                                <ul class="cat-list col-sm-3  col-xs-6 col-xxs-12">
                                                    <?php foreach($top_loc as $loc) { ?>
                                                        <li> <a href="category.html"><?php echo $loc['name']; ?></a></li>
                                                    <?php } ?>
                                                </ul>
                                            <?php } ?> 
                                        </div> 
                                    </div>


                                </div>
                                <div role="tabpanel" class="tab-pane" id="tab2">

                                    <div class="col-lg-12 tab-inner">

                                        <div class="row">
                                        
                                            <?php foreach(array_chunk($products_listing, 8, true) as $prdt_list) { ?>
                                                <ul class="cat-list cat-list-border col-sm-3  col-xs-6 col-xxs-12">
                                                    <?php foreach($prdt_list as $prdt) { ?>
                                                        <li> <a href="category.html"><?php echo $prdt['product_name']; ?></a></li>
                                                    <?php } ?>
                                                </ul>
                                            <?php } ?>  
                                        </div>

                                    </div>




                                </div>
                                <div role="tabpanel" class="tab-pane" id="tab3">

                                    <div class="col-lg-12 tab-inner">

                                        <div class="row">
 
                                            <?php foreach($top_makes as $top_manuf) { ?>
                                                <ul class="cat-list col-sm-3  col-xs-6 col-xxs-12">
                                                    <?php foreach($top_manuf as $manuf) { ?>
                                                        <li> <a href="category.html"><?php echo $manuf['manufacturer_name']; ?></a></li>
                                                    <?php } ?>
                                                </ul>
                                            <?php } ?> 

                                        </div>

                                    </div>


                                </div>
                            </div>

                        </div>

                    </div>


                </div>

            </div>
        </div>


        <div class="row">





       <!-- <div class="col-sm-9 page-content col-thin-right">
          <div class="inner-box category-content">
            <h2 class="title-2">Find Products World Wide </h2>
            <div class="row">
              
            <?php
			    		foreach ($product_categories as $items)
			    		{
			    			
			    			
			    			if(sizeof($items['sub_menu'])>0)
			    			{
			    				?>
                                <div class="col-md-4 col-sm-4 ">
                                    <div class="cat-list">
                                    <h3 class="cat-title"><a href="category.html"><i class="fa fa-folder-open ln-shadow"></i> <?php echo $items['category_name'] ?> <span class="count"><?php echo sizeof($items['sub_menu']) ?></span> </a>
                                    
                                    <span data-target=".cat-id-1" data-toggle="collapse" class="btn-cat-collapsed collapsed">   <span class=" icon-down-open-big"></span> </span>
                                    </h3>
                                    <ul class="cat-collapse collapse in cat-id-1">
                                    <?php
                                        foreach ($items['sub_menu'] as $subItems)
                                        {?>
                                            <li> <a href="category.html"><?=$subItems["category_name"]?></a> </li>
                                        <?php }?>
                                    </ul>
                                    </div>
                                </div>
              <?php
			    			}
			    			else
                                {

                                    echo'No record found';
                                }	
			    		}
			       ?>
           
            </div>
          </div>

        
        </div>-->
        
      </div>
    </div>
  </div>
  <!-- /.main-container -->
  
  <!-- /.counts -->
	<?php $this->load->view(ciHtmlTheme.'/includes/counts');?>
  <!-- /.claa me  -->
	<?php $this->load->view(ciHtmlTheme.'/includes/call_back');?>
  <!-- /.footer --> 
  <?php $this->load->view(ciHtmlTheme.'/includes/footer');?>
 
</div>
<!-- /.wrapper --> 

<!-- Le javascript
================================================== --> 
<!-- Placed at the end of the document so the pages load faster --> 
<script src="<?php echo base_url(ciHtmlTheme.'/assets/js/jquery.min.js')?>"> </script>
<script src="<?php echo base_url(ciHtmlTheme.'/assets/bootstrap/js/bootstrap.min.js')?>"></script> 
<!-- include carousel slider plugin  --> 
<script defer  src="<?php echo base_url(ciHtmlTheme.'/assets/js/owl.carousel.min.js')?>"></script> 
<!-- include equal height plugin  --> 
<script src="<?php echo base_url(ciHtmlTheme.'/assets/js/jquery.matchHeight-min.js')?>"></script> 
<!-- include jquery list shorting plugin plugin  --> 
<script src="<?php echo base_url(ciHtmlTheme.'/assets/js/hideMaxListItem.js')?>"></script> 
<!-- include jquery.fs plugin for custom scroller and selecter  --> 
<script src="<?php echo base_url(ciHtmlTheme.'/assets/plugins/jquery.fs.scroller/jquery.fs.scroller.js')?>"></script>
<script src="<?php echo base_url(ciHtmlTheme.'/assets/plugins/jquery.fs.selecter/jquery.fs.selecter.js')?>"></script>
<script src="<?php echo base_url(ciHtmlTheme.'/assets/plugins/jquery.fs.selecter/jquery.fs.selecter.js')?>"></script>
<script src="<?php echo base_url(ciHtmlTheme.'/assets/js/jquery.validate.min.js')?>"></script>

<!-- include custom script for site  --> 
<script src="<?php echo base_url(ciHtmlTheme.'/assets/js/script.js')?>"></script>
<!-- include jquery autocomplete plugin  -->
<script type="text/javascript" src="<?php echo base_url(ciHtmlTheme.'/assets/plugins/autocomplete/jquery.mockjax.js')?>"></script>
<script type="text/javascript" src="<?php echo base_url(ciHtmlTheme.'/assets/plugins/autocomplete/jquery.autocomplete.js')?>"></script>
<script type="text/javascript" src="<?php echo base_url(ciHtmlTheme.'/assets/plugins/autocomplete/usastates.js')?>"></script>
<script type="text/javascript" src="<?php echo base_url(ciHtmlTheme.'/assets/plugins/autocomplete/autocomplete-demo.js')?>"></script>
<script src="<?php echo base_url(ciHtmlTheme.'/assets/js/mdtimepicker.js'); ?>"></script>

</body>
<script>
function search()
{
    var search_type = $("#search-category").val();
    var city = $("#autocomplete-ajax").val();
    if (search_type!='' && city!='')
    {
        window.location.href="<?php echo base_url('home/index/')?>"+search_type+'/'+city;
    }
    
}
$(document).ready(function () {
        (function ($) {
            $('.featured-list-slider').owlCarousel();
        })(jQuery);

      
    });
</script>
</html>
