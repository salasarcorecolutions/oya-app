<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<!-- Fav and touch icons -->
<link rel="apple-touch-icon-precomposed" sizes="144x144" href="<?php echo base_url(ciHtmlTheme.'/assets/ico/apple-touch-icon-144.png')?>">
<link rel="apple-touch-icon-precomposed" sizes="114x114" href="<?php echo base_url(ciHtmlTheme.'/assets/ico/apple-touch-icon-114.png')?>">
<link rel="apple-touch-icon-precomposed" sizes="72x72" href="<?php echo base_url(ciHtmlTheme.'/assets/ico/apple-touch-icon-72.png')?>">
<link rel="apple-touch-icon-precomposed" sizes="57x57" href="<?php echo base_url(ciHtmlTheme.'/assets/ico/apple-touch-icon-57.png')?>">
<link rel="shortcut icon" href="<?php echo base_url(ciHtmlTheme.'/assets/ico/favicon.png')?>">
<title>UPCOMMING AUCTIONS</title>
<!-- Bootstrap core CSS -->
<link href="<?php echo base_url(ciHtmlTheme.'/assets/bootstrap/css/bootstrap.min.css')?>" rel="stylesheet">
<!-- Custom styles for this template -->
<link href="<?php echo base_url(ciHtmlTheme.'/assets/css/style.css')?>" rel="stylesheet">
<link href="<?php echo base_url(ciHtmlTheme.'/assets/css/custom.css')?>" rel="stylesheet">
<link href="<?php echo base_url(ciHtmlTheme.'/assets/css/buyer.css')?>" rel="stylesheet">


<style>
.badge_style{
	background-color: #0a59a4!important;
	border-radius:5px!important;
	font-size: 20px!important;
}
.border_blue{
	border: 2px solid #c1e1ff;
    min-height: 36px!important;

}
</style>
<script src="<?php echo base_url(ciHtmlTheme.'/assets/js/pace.min.js')?>"></script>

 </head>
<body>

<div id="wrapper">
<!-- /.header -->
 <?php $this->load->view(ciHtmlTheme.'/buyer/buyer_header');?>
  <!-- /.header -->
  <div class="main-container">
	<div class="container-fluid">
      	<div class="row">
			<div class="col-sm-3 page-sidebar">
			<?php $this->load->view(ciHtmlTheme.'/buyer/left_navigation');?>
			</div>

			<div class="col-sm-6 page-content">
				<div id="succ_msg" style="display:none;" class="alert alert-success alert-dismissable">
					<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
					<strong><h5></h5></strong>
				</div>
				<div id="err_msg" style="display:none;" class="alert alert-danger alert-dismissable">
					<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
					<strong><h5></h5></strong>
				</div>

			<div class="inner-box category-content">
				<h2 class="title-2"> <i class="icon-user-add"></i> Associate With Vendor</h2>
					<!-- Text input-->
						<form id="vendor-form" class="form-horizontal" >
							<div class="form-group required">
								<label style='text-align: left;' class="col-md-4 control-label">Vendor Name  </label>
								<div class="col-md-6">
									<input type="text" class="form-control" value='<?php echo $result['vendor_name']?>' disabled>
								</div>
							</div>

							<div class="form-group required">
								<label style='text-align: left;' class="col-md-4 control-label">Vendor Location  </label>
								<div class="col-md-6">
									<input type="text" class="form-control" value='<?php echo $result['country']. '- '.$result['state'].'- '.$result['city'].'- '.$result['pin']?>' disabled>
								</div>
							</div>

							<div style='margin:30px; 0px' class="form-group">
								<div class="col-sm-4">
								</div>	
								<div style='text-align:center;' class="col-sm-4">
									<a type="button" class="btn btn-primary " href="javascript:void(0);" onclick="associate_vendor();">
											Aossicate
									</a>		
								</div>
							</div>
						</form>
				</div>
			</div>
			<!-- section to add adds in future versions-->
			<div class="col-sm-3 reg-sidebar">
			<div class="reg-sidebar-inner text-center">
				<div class="promo-text-box"> <i class=" icon-picture fa fa-4x icon-color-1"></i>
				<h3><strong>Post a Free Classified</strong></h3>
				<p> Post your free online classified ads with us. Lorem ipsum dolor sit amet, consectetur adipiscing elit. </p>
				</div>
				<div class="promo-text-box"> <i class=" icon-pencil-circled fa fa-4x icon-color-2"></i>
				<h3><strong>Create and Manage Items</strong></h3>
				<p> Nam sit amet dui vel orci venenatis ullamcorper eget in lacus.
					Praesent tristique elit pharetra magna efficitur laoreet.</p>
				</div>
				<div class="promo-text-box"> <i class="  icon-heart-2 fa fa-4x icon-color-3"></i>
				<h3><strong>Create your Favorite  ads list.</strong></h3>
				<p> PostNullam quis orci ut ipsum mollis malesuada varius eget metus.
					Nulla aliquet dui sed quam iaculis, ut finibus massa tincidunt.</p>
				</div>
			</div>
			</div>
		</div>
		<!-- /.footer -->
        <?php $this->load->view(ciHtmlTheme.'/includes/footer');?>

</div>
<!-- /.wrapper -->
</div>
<!-- Le javascript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="<?php echo base_url(ciHtmlTheme.'/assets/js/jquery.min.js')?>"> </script>
<script src="<?php echo base_url(ciHtmlTheme.'/assets/bootstrap/js/bootstrap.min.js')?>"></script>


<!-- include equal height plugin  --> 
<script src="<?php echo base_url(ciHtmlTheme.'/assets/js/jquery.matchHeight-min.js')?>"></script>
<!-- include jquery list shorting plugin plugin  -->
<script src="<?php echo base_url(ciHtmlTheme.'/assets/js/hideMaxListItem.js')?>"></script>
<!-- include jquery.fs plugin for custom scroller and selecter  -->
<script src="<?php echo base_url(ciHtmlTheme.'/assets/plugins/jquery.fs.scroller/jquery.fs.scroller.js')?>"></script>
<script src="<?php echo base_url(ciHtmlTheme.'/assets/plugins/jquery.fs.selecter/jquery.fs.selecter.js')?>"></script>
<!-- include custom script for site  -->
<script src="<?php echo base_url(ciHtmlTheme.'/assets/js/script.js')?>"></script>
		<div class="modal fade" id="log_in" tabindex="-1" role="dialog" aria-labelledby="loginModel" aria-hidden="true">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
						<h4 class="modal-title text-info">Login Modal</h4>
					</div>
					<div class="modal-body">	
						<form name="login_auth" id="login_auth" class="login-form b-radius5">
						<div class="input-group margin-bottom-20">
					        <span class="input-group-addon"><i class="fa fa-user"></i></span>
							<input type="text" id="username" name="username" placeholder="Username" class="form-control height40"/>
						</div>                    
						<div class="input-group margin-bottom-20">
							<span class="input-group-addon"><i class="fa fa-lock"></i></span>
							<input type="password" id="password" name="password" placeholder="Password" class="form-control height40"/>
						</div>
						<div class="margin-bottom-20">
					         <span style="color: red; float:left" id="error"></span>
						</div>
						<br>
						<div class="row">
							<div class="col-md-3">
								<h4 class="pull-left">Login as:</h4>
							</div>
							<div class="col-md-6">
								<select id="login_type" class="form-control">
									<option value="B">Buyer/Supplier</option>
								</select>
							</div>
							<div class="col-md-3">
								<button class="b-radius2 bg-transparent border-pacific width100 height34 hover-pecific text-pacific pull-right" id="login">Login</button>
							</div>
						</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	
		<script>
			function associate_vendor()
			{
				var email = '<?php echo $email?>';
				var vendor_id = '<?php echo $vendor_id?>';
				if (confirm('Do You Want To Associate With Vendor'))
				{
					$.ajax({
					url: '<?php echo base_url();?>user/update_associate_details',
					data: {'email':email,'vendor_id':vendor_id},
					type:"post",
					dataType: 'json',
					success: function (resObj) {
						if (resObj.status == "success") 
						{
							
							$("#succ_msg").show();
							$("#err_msg").hide();
							$("#succ_msg").html(resObj.message);

						}else if(resObj.status == 'login'){
							$("#log_in").modal('show');
						}	
						else
						{
							$("#succ_msg").hide();
							$("#err_msg").show();
							$("#err_msg").html(resObj.message);
						}
					},
					error: function(){
						alert('Please Try Again After Some Time .... ');
					}
					});	
				}
				else
				{
					return FALSE;
				}
				
			}
			$("#login").click(function(e)
			{
				e.preventDefault();
				var form = $("#login_auth");
				form.validate({
					rules: {
						username: "required",
						password: "required",
					},
					messages: {
						username: "<b class='text-danger'>Please Enter the Username</b>",
						password: "<b class='text-danger'>Please Enter the Password</b>",
					}
				});
				if (form.valid()){
					var login_type = $("#login_type").val();
					if (login_type == "B") {
						bidder_login();
					}
				}
			});
			function bidder_login()
			{
				$.ajax({
					url: '<?php echo base_url("buyer_sign_in/login"); ?>',
					data: {
						"username":$("#username").val(),
						"password":$("#password").val(),
					},
					type:"POST",
					dataType: 'json',
					success: function ( response ) {
						if (response.success){
							$("#log_in").modal('hide');
							associate_vendor();
						}
						else if (response.status == "pending"){
							window.location = "<?php echo base_url('buyer_sign_in/verify_user'); ?>";

						}				
						else {
							$("#error").html(response.msg);
						}
					},
					error: function(){
						alert('Problem in login please try again');
					}
				});
			}	
		</script>
	</body>
</html>