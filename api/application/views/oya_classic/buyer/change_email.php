<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<!-- Fav and touch icons -->
<link rel="apple-touch-icon-precomposed" sizes="144x144" href="<?php echo base_url(ciHtmlTheme.'/assets/ico/apple-touch-icon-144.png')?>">
<link rel="apple-touch-icon-precomposed" sizes="114x114" href="<?php echo base_url(ciHtmlTheme.'/assets/ico/apple-touch-icon-114.png')?>">
<link rel="apple-touch-icon-precomposed" sizes="72x72" href="<?php echo base_url(ciHtmlTheme.'/assets/ico/apple-touch-icon-72.png')?>">
<link rel="apple-touch-icon-precomposed" sizes="57x57" href="<?php echo base_url(ciHtmlTheme.'/assets/ico/apple-touch-icon-57.png')?>">
<link rel="shortcut icon" href="<?php echo base_url(ciHtmlTheme.'/assets/ico/favicon.png')?>">
<title>CHANGE EMAIL </title>
<!-- Bootstrap core CSS -->
<link href="<?php echo base_url(ciHtmlTheme.'/assets/bootstrap/css/bootstrap.min.css')?>" rel="stylesheet">
<!-- Custom styles for this template -->
<link href="<?php echo base_url(ciHtmlTheme.'/assets/css/style.css')?>" rel="stylesheet">
<link href="<?php echo base_url(ciHtmlTheme.'/assets/css/custom.css')?>" rel="stylesheet">
<!-- styles needed for carousel slider -->


<script>
    paceOptions = {
      elements: true
    };
</script>
<script src="<?php echo base_url(ciHtmlTheme.'/assets/js/pace.min.js')?>"></script>

 </head>
<body>

<div id="wrapper">
<!-- /.header -->
<?php $this->load->view(ciHtmlTheme.'/buyer/buyer_header');?>
  <!-- /.header -->
  <div class="main-container">
   <div class="container-fluid">
      <div class="row">
        <div class="col-sm-3 page-sidebar">
         <?php $this->load->view(ciHtmlTheme.'/buyer/left_navigation');?>
        </div>
        <!--/.page-sidebar-->
        
        <div class="col-sm-9 page-content">
          <div class="inner-box">
          <div class="row">
            <div class="col-md-5 col-xs-4 col-xxs-12">
              <h3 class="no-padding text-center-480 useradmin"><a href="<?php echo base_url('buyer/profile')?>">
              <img class="userImg" src="<?php echo base_url('images/user-icon.png')?>" alt="<?php echo $this->session->userdata('conperson');?>" align="left"> <?php echo $this->session->userdata('conperson');?> </a> 
              	<br /><span class="page-sub-header-sub small"><?php echo $this->session->userdata('compname');?></span>
              </h3>
            	
            </div>
            <div class="col-md-7 col-xs-8 col-xxs-12">
              <div class="header-data text-center-xs"> 
                
                <!-- Traffic data -->
                <div class="hdata">
                  <div class="mcol-left"> 
                    <!-- Icon with red background --> 
                    <i class="fa fa-eye ln-shadow"></i> </div>
                  <div class="mcol-right"> 
                    <!-- Number of visitors -->
                    <p><a href="#">7000</a> <em>visits</em></p>
                  </div>
                  <div class="clearfix"></div>
                </div>
                
                <!-- revenue data -->
                <div class="hdata">
                  <div class="mcol-left"> 
                    <!-- Icon with green background --> 
                    <i class="icon-th-thumb ln-shadow"></i> </div>
                  <div class="mcol-right"> 
                    <!-- Number of visitors -->
                    <p><a href="#">12</a><em>Ads</em></p>
                  </div>
                  <div class="clearfix"></div>
                </div>
                
                <!-- revenue data -->
                <div class="hdata">
                  <div class="mcol-left"> 
                    <!-- Icon with blue background --> 
                    <i class="fa fa-user ln-shadow"></i> </div>
                  <div class="mcol-right"> 
                    <!-- Number of visitors -->
                    <p><a href="#">18</a> <em>Favorites </em></p>
                  </div>
                  <div class="clearfix"></div>
                </div>
              </div>
            </div>
          </div>
          </div>
          
          <div class="inner-box">
            <div class="welcome-msg">
              <h3 class="page-sub-header2 clearfix no-padding">Hello <?php echo $this->session->userdata('conperson');?></h3>
              <span class="page-sub-header-sub small">You last logged in at: 01-01-2014 12:40 AM [UK time (GMT + 6:00hrs)]</span> 
            </div>
            <div id="accordion" class="panel-group">
                <div class="panel panel-default" id="email_model">
                    <div class="panel-heading">
                    <h4 class="panel-title"> Change Email </h4>
                    </div>
                    <div class="panel-collapse">
                        <div class="panel-body">
                            <form class="form-horizontal" id="formEdit" method="post"> 
                                <div class="form-group">
                                    <label  class="col-sm-3 control-label">Email</label>
                                    <div class="col-sm-9">
                                        <input type="email" class="form-control" id="c_mail" name="email" placeholder="john.deo@example.com" value="<?php echo $_SESSION['email'];?>">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label  class="col-sm-3 control-label">New Email</label>
                                    <div class="col-sm-9">
                                        <input type="email" class="form-control" placeholder="john.deo@example.com" id="newmail" name="newmail" onblur="check_email(this.value)" required>
                                        <span class="text text-danger col-log-6" id="errEmail"></span>
                                    </div>
                                </div> 
                                <input type="hidden" id="cmpname" value="<?php echo $_SESSION['compname'];?>">
                                <div class="form-group " id="update_btn">
                                    <div class="col-sm-offset-3 col-sm-9">
                                        <button id="btnUpdate" class="btn btn-primary ">Update</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div> 

                <div class="panel panel-default" id="verify_model">
                    <div class="panel-heading">
                        <h4 class="panel-title"> <a href="#collapseB2"  data-toggle="collapse">Verify Email </a> </h4>
                    </div>
                    <div class="panel-collapse collapse in" id="collapseB2">
                        <div class="panel-body">
                            <form class="form-horizontal" id="formEdit" method="post"> 
                                <div class="form-group">
                                    <label  class="col-sm-3 control-label">Email</label>
                                    <div class="col-sm-9">
                                        <input type="email" class="form-control" id="v_mail">
                                    </div>
                                </div>  
                                <div class="form-group ">
                                    <button type="button" id="email_send" class="type-button field-border theme-blue radius  paddingLR_only" href="javascript:void(0);" onclick="send_email_otp();add_resend_email_counter(1)">
                                            <span class="button-text font18" >Send OTP</span>
                                    </button>
                                </div>
                                <span id="error_email_msg"  style="color:red;display:none;"></span>
                                <span id="success_email_msg"  style="color:green;display:none;"></span>
                                <div class="col-sm-12 user_email_otp" style="display:none;" >
                                  <div class="col-sm-2 field-label-up ico-attach-left -size_lg">
                                      <label class="label-control">OTP :</label>
                                  </div>
                                  <div class="col-sm-4 form-group">	
                                      <input value="" class="form-control" type="text" placeholder="Enter OTP " name="otp" id="otp" />
                                  </div>
                                  <div class="col-sm-2">
                                      <button type="button" class="type-button field-border theme-blue radius  paddingLR_only" href="javascript:void(0);" onclick="verify_otp()">
                                              <span class="button-text font18">Verify</span>
                                      </button>		
                                  </div>				
                                  <div class="col-sm-4">
                                      <button type="button" id="resend_email_btn" class="type-button field-border theme-blue radius  paddingLR_only col-sm-12" href="javascript:void(0);" onclick="re_send_email_otp(1);" >
                                          <span class="button-text font18" id="email_counter_1" >Resend OTP </span>
                                      </button>
                                      <span class="col-sm-12" id="resend_otp_email_link_1">Resend OTP<span id="reset_email_counter_1">(30)</span></span>
                                  </div> 
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <!--/.row-box End--> 
            
          </div>
        </div>
        <!--/.page-content--> 
      </div>
      <!--/.row--> 
    </div>
    <!--/.container--> 
  </div>
  <!-- /.main-container -->
  
  
  <!-- /.footer --> 
  <?php $this->load->view(ciHtmlTheme.'/includes/footer');?>
 
</div>
<!-- /.wrapper --> 

<!-- Le javascript
================================================== --> 
<!-- Placed at the end of the document so the pages load faster --> 
<script src="<?php echo base_url(ciHtmlTheme.'/assets/js/jquery.min.js')?>"> </script>
<script src="<?php echo base_url(ciHtmlTheme.'/assets/bootstrap/js/bootstrap.min.js')?>"></script> 
<!-- include carousel slider plugin  --> 

<!-- include equal height plugin  --> 
<script src="<?php echo base_url(ciHtmlTheme.'/assets/js/jquery.matchHeight-min.js')?>"></script> 
<!-- include jquery list shorting plugin plugin  --> 
<script src="<?php echo base_url(ciHtmlTheme.'/assets/js/hideMaxListItem.js')?>"></script> 
<!-- include jquery.fs plugin for custom scroller and selecter  --> 
<script src="<?php echo base_url(ciHtmlTheme.'/assets/plugins/jquery.fs.scroller/jquery.fs.scroller.js')?>"></script>
<script src="<?php echo base_url(ciHtmlTheme.'/assets/plugins/jquery.fs.selecter/jquery.fs.selecter.js')?>"></script>
<!-- include custom script for site  --> 
<script src="<?php echo base_url(ciHtmlTheme.'/assets/js/script.js')?>"></script>
<!-- include jquery autocomplete plugin  -->

<script	src="<?php echo base_url(ciHtmlTheme.'/assets/js/jquery.validate.min.js')?>"></script>
<script src="<?php echo base_url(ciHtmlTheme.'/assets/js/script.js')?>"></script>

<script>
    $('#c_mail').attr('disabled', true); 
    $('#verify_model').hide(); 
   
  // check fields nd show verify model 
    $("#btnUpdate").click(function(e){
        e.preventDefault();
        var form=$("#formEdit");
        form.validate({
            ignore:[],
            rules: { 
            newmail: {required : true},
            mail: {required : true }
            },
        messages: {	
            newmail:"Please Enter Email",   
            mail:"Please Enter New Email",  
            },
            
        });

        if(form.valid())
        $( "#formEdit" ).submit();  
    });

  $("#formEdit").on('submit',(function(e) {
	
    e.preventDefault(); 
 
    var newval = $("#newmail").val();
    var o_email = $("#c_mail").val();
    if(o_email == newval){
      $("#errEmail").text("No Change in mail!");
    }
    else {
      $('#verify_model').show();
      $('#email_model').hide();
      $("#v_mail").attr("value",newval);
      $('#v_mail').attr('disabled', true); 
    }
    
  }));

  function send_email_otp(cnt='')
  {
    var email = '<?php echo $_SESSION['email']?>';
    var id = '<?php echo $_SESSION['bidder_id']?>'; 
    $.ajax({
        url: '<?php echo base_url();?>buyer/profile/send_email_otp',
        data: {'email':email,'id':id},
        type:"post",
        dataType: 'json',
        success: function (resObj) {
          if (resObj.status == "success") 
          {
              $(".user_email_otp").show();
              $("#email_send").hide();
              $("#send_otp").hide(); 
          }else{
            alert(resObj.msg);                   
          }	
        },
        error: function(){
          alert('Please Try Again After Some Time .... ');
        }
    }); 
  }

  function re_send_email_otp(id)
  {
    var otp_cnt = resend_email_otp++;
    if (otp_cnt <=2){
      send_email_otp(otp_cnt);
      add_resend_email_counter(id);
    } else {
      $("#error_email_msg").show();
      $("#error_email_msg").html('Send OTP Failed,Please Try Again After Some Time');
      add_resend_email_counter(id,'failed');
      $("#resend_email_btn").hide();
    }
  
  }
		 
  function verify_otp()
  {
    var email = $("#v_mail").val();;
    var id = '<?php echo $_SESSION['bidder_id']?>'; 
    var otp = $("#otp").val();
    if (otp)
    { 
      var url_link = '<?php echo base_url('buyer/profile/verify_email_otp')?>';
        
      $.ajax({
        url: url_link,
        data: {'otp':otp,'id':id,'email':email},
        type:"post",
        dataType: 'json',
        success: function (resObj) {
          if (resObj.status == "success") 
          {
            alert('updated!');
            setInterval('location.reload()', 800); 

          }else{
            $("#error_email_msg").show();
            $("#error_email_msg").html(resObj.msg);
          }	
        },
        error: function(){
          alert('Invalid Request');
        }
      });
    }
    else
    {
      alert('Please Enter OTP');
    } 
  }
		 

  var counterObj = new Object();
  function add_resend_email_counter(id,status='')
  {
    if (status)
    {
      counterObj['count_vendor_email_'+id] = 300; //To set the counter variable for the given id.
    }else{
      counterObj['count_vendor_email_'+id]  = 30;
    }
    resend_email_counter(id); 
  }
		 
  function resend_email_counter(id)
  {
    document.getElementById('reset_email_counter_'+id).innerHTML = '&nbsp('+counterObj['count_vendor_email_'+id]+')';
    counterObj['count_vendor_email_'+id]--;
    if (counterObj['count_vendor_email_'+id] < 0){
      counterObj['count_vendor_email_'+id] = 0;
      document.getElementById("reset_email_counter_"+id).style.cursor = "pointer";
      document.getElementById("resend_otp_email_link_"+id).style.display = "none";
      $('#resend_email_btn').show();
    } else {
      setTimeout(function(){
          resend_email_counter(id);
      }, 1000);
      document.getElementById("reset_email_counter_"+id).style.cursor = "default";
      document.getElementById("email_counter_"+id).style.display = "inline-block";
      document.getElementById("resend_otp_email_link_"+id).style.display = "block";
      $('#resend_email_btn').hide(); 
    }
  }
		 
		 

</script>
</body>
</html>
