<?php
defined('BASEPATH') or exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<!-- Fav and touch icons -->
<link rel="apple-touch-icon-precomposed" sizes="144x144"
	href="<?php echo base_url(ciHtmlTheme.'/assets/ico/apple-touch-icon-144.png')?>">
<link rel="apple-touch-icon-precomposed" sizes="114x114"
	href="<?php echo base_url(ciHtmlTheme.'/assets/ico/apple-touch-icon-114.png')?>">
<link rel="apple-touch-icon-precomposed" sizes="72x72"
	href="<?php echo base_url(ciHtmlTheme.'/assets/ico/apple-touch-icon-72.png')?>">
<link rel="apple-touch-icon-precomposed" sizes="57x57"
	href="<?php echo base_url(ciHtmlTheme.'/assets/ico/apple-touch-icon-57.png')?>">
<link rel="shortcut icon"
	href="<?php echo base_url(ciHtmlTheme.'/assets/ico/favicon.png')?>">
<title>PROXY DETAILS - <?php echo @$auction['saleno']?></title>
<!-- Bootstrap core CSS -->
<link
	href="<?php echo base_url(ciHtmlTheme.'/assets/bootstrap/css/bootstrap.min.css')?>"
	rel="stylesheet">
<!-- Custom styles for this template -->
<link href="<?php echo base_url(ciHtmlTheme.'/assets/css/style.css')?>"
	rel="stylesheet">
<link href="<?php echo base_url(ciHtmlTheme.'/assets/css/custom.css')?>"
	rel="stylesheet">


<script>
    paceOptions = {
      elements: true
    };
</script>
<script
	src="<?php echo base_url(ciHtmlTheme.'/assets/js/pace.min.js')?>"></script>
</head>
<body>

	<div id="wrapper">
		<!-- /.header -->
 <?php $this->load->view(ciHtmlTheme.'/buyer/buyer_header');?>
  <!-- /.header -->
		<div class="main-container">
			<div class="container-fluid">
				<div class="row">
					<div class="col-sm-3 page-sidebar">
         <?php $this->load->view(ciHtmlTheme.'/buyer/left_navigation');?>
        </div>
					<!--/.page-sidebar-->

					<div class="col-sm-9 page-content">
						<div class="inner-box">
							<h2 class="title-2">
								<i class="icon-heart-1"></i> DETAILS OF SALE NO: <?php echo $auction['saleno']?></h2>
							<div class="row">
								<div class="col-md-8">
									<div class="box">
										<div class="box-header">
											<h5 class="box-title">DETAILS</h5>

											
										</div>
										<!-- /.box-header -->
										<div class="box-body no-padding">
										<table class="table table-responsive">
										<tr><td class="text-right">Sale No :</td><td><?php echo $auction['saleno']; ?></td></tr>
										<tr><td class="text-right">Vendor :</td><td><?php echo $auction['auccomp']; ?></td></tr>
										<tr><td class="text-right">Auction Type :</td><td><?php echo $auction['auctype']; ?></td></tr>
										<tr><td class="text-right">Start Date :</td><td><?php echo $auction['start_date']; ?></td></tr>
										<tr><td class="text-right">End Date :</td><td><?php echo $auction['end_date']; ?></td></tr>
										<tr><td class="text-right">Particulars :</td><td><?php echo $auction['aucpartcular']; ?></td></tr>
										<tr><td class="text-right">Currency :</td><td><?php echo $auction['auction_currency']; ?></td></tr>
										<tr><td class="text-right">Total Lots :</td><td><?php echo $auction['nooflots']; ?></td></tr>
										
										</table>
										
										
										
										</div>
									</div>
								</div>
								<div class="col-md-4">
									<div class="box">
										<div class="box-header">
											<h5 class="box-title">DOCUMENTS</h5></div><!-- /.box-header -->
											<div class="box-body no-padding">
											<table class="table">
												<thead>
													<tr>

														<th>Document Name</th>
														<th>View</th>
													</tr>
												</thead>
												<tbody>
												<?php foreach($auction['docs'] as $doc_items) { ?>
													<tr>

													<td><?php echo $doc_items['doc_name']?></td>
													<td><a href="<?php echo $doc_items['doc_path']?>" target="_new"><i class="fa fa-link"></i> View</a></td>
													</tr>
													<?php }?>
												</tbody>
											</table>

											
										</div>
										<!-- /.box-body -->
									</div>
								</div>
							</div>
							<div class="row">
							<div class="col-md-12">
									<div class="box">
										<div class="box-header">
											<h5 class="box-title">LOTS
											<span class="pull-right"><a class="btn btn-success btn-xs" href="<?php echo base_url('buyer/live_auctions/live/'.$auction['auction_id']);?>"><i class="icon-hammer"></i> Live Auction</a></span>
										</h5>
										</div>
										<!-- /.box-header -->
										<div class="box-body no-padding">
											<table class="table table-bordered">
												<tbody>
													<tr>
																											
														<th><?php echo @$lot_settings['ltno']==""?'LOT NO':@$lot_settings['ltno']; ?></th>
														<th><?php echo @$lot_settings['plant']==""?'PLANT':@$lot_settings['plant']; ?></th>
														<th><?php echo @$lot_settings['vltno']==""?'VENDOR LOT':@$lot_settings['vltno']; ?> </th>
														<th><?php echo @$lot_settings['product']==""?'PRODUCT':@$lot_settings['product']; ?> </th>
														<th><?php echo @$lot_settings['lsd']==""?'BID START':@$lot_settings['lsd']; ?> </th>
														<th><?php echo @$lot_settings['lsd']==""?'BID END':@$lot_settings['lsd']; ?> </th>
														<th><?php echo @$lot_settings['tq']==""?'QUANTITY':@$lot_settings['tq']; ?> </th>
														<th><?php echo @$lot_settings['bq']==""?'BID QUANTITY':@$lot_settings['bq']; ?> </th>
														<th>START PRICE </th>
														<th>PROXY </th>
														<th>P. Qty </th>
														<th></th>
													

													</tr>
													<tr>
												<?php foreach($lots as $lot_items){?>
													<tr>

														<td><?php echo $lot_items['ynk_lotno'];?></td>
														<td><?php echo $lot_items['plant'];?></td>
														<td><?php echo $lot_items['vendorlotno'];?></td>
														<td><a href="<?php echo base_url('buyer/auctions/lot_details/'.$auction['auction_id'].'/'.MD5($lot_items['ynk_lotno']))?>"><?php echo $lot_items['product'];?></a></td>
														<td><?php echo servertz_to_usertz($lot_items['lot_open_date'],$this->session->userdata('user_tz'));?></td>
														<td><?php echo servertz_to_usertz($lot_items['lot_close_date'],$this->session->userdata('user_tz'));?></td>
														<td><?php echo $lot_items['totalqty'];?> <?php echo $lot_items['totunit'];?></td>
														<td><?php echo $lot_items['bidqty'];?> <?php echo $lot_items['totunit'];?></td>
														<td><?php echo $lot_items['startbid'];?> <?php echo $auction['auction_currency']; ?> </td>
														<td><?php echo $lot_items['proxy_amt'];?> <?php echo $auction['auction_currency']; ?> </td>
														<td><?php echo $lot_items['proxy_qty'];?> <?php echo $lot_items['totunit']; ?> </td>
														<td>
													
														<?php 
														
														if($lot_items['proxy']=="Y")
														{
														    echo '<button class="proxy btn btn-success btn-sm margin-bottom-5" data-auction-id="'.$auction['auction_id'].'" data-lot-id="'.md5($lot_items['ynk_lotno']).'" data-total-qty="'.$lot_items['totalqty'].'" data-part_bid="'.$lot_items['part_bid'].'" data-unit="'.$lot_items['totunit'].'">Add Proxy</button>';
														    
														    if($lot_items['is_paused']=="N")
														      echo '<a class="PauseProxy btn btn-success btn-sm" id="'.md5($lot_items['ynk_lotno']).'"  data-auction-id="'.$auction['auction_id'].'" data-lot-id="'.md5($lot_items['ynk_lotno']).'" data-proxy_stat="'.$lot_items['is_paused'].'" >Pause Proxy</a>';
														    else
														      echo '<a class="PauseProxy btn btn-danger btn-sm" id="'.md5($lot_items['ynk_lotno']).'" data-auction-id="'.$auction['auction_id'].'" data-lot-id="'.md5($lot_items['ynk_lotno']).'" data-proxy_stat="'.$lot_items['is_paused'].'" >Resume Proxy</a>';
                                                          }
                                                         else 
                                                             echo 'No Proxy';
                                                         
														?> </td>
													</tr>
													<?php }?>

												</tbody>
											</table>
										</div>
										<!-- /.box-body -->
									</div>
								</div>
								</div>
						</div>
						<!--/.page-content-->
					</div>
					<!--/.row-->
				</div>
				<!--/.container-->
			</div>
			<!-- /.main-container -->


			<!-- /.footer --> 
  <?php $this->load->view(ciHtmlTheme.'/includes/footer');?>
 
</div>

		<!-- /.wrapper -->
	</div>
	<div class="modal fade" id="ModalProxy" tabindex="-1" role="dialog" aria-labelledby="myModalProxy" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">

      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="myModalLabel">Add Proxy</h4>
      </div>
      <div class="modal-body">
        <div class="row">
              <div class="col-sm-12">
                <form class="form-horizontal" id="frmProxy" name="frmProxy">
                  <fieldset>
                    
                     <input type="hidden" name="proxy_auction_id" id="proxy_auction_id">
                     <input type="hidden" name="proxy_lot_id" id="proxy_lot_id">
                     <span id="msg"></span>
                    <!-- Text input-->
                    <div class="form-group required">
                      <label class="col-md-4 control-label" >Quantity (<span id="ltUnit"></span>) <sup>*</sup></label>
                      <div class="col-md-6">
                        <input  name="proxy_qty" id="proxy_qty"  placeholder="Quantity" class="form-control input-md" required="true" type="text">
                      </div>
                    </div>
                    
                    <!-- Text input-->
                    <div class="form-group required">
                      <label class="col-md-4 control-label" >Your Proxy Amount (<?php echo $currency['prefered_currency_name']; ?>)<sup>*</sup></label>
                      <div class="col-md-6">
                        <input  name="proxy_amount" id="proxy_amount" name="textinput" placeholder="Proxy Amount" class="form-control input-md" type="text">
                      </div>
                    </div>
                    <div class="form-group required">
                      <label class="col-md-4 control-label" >Auction Currency (<?php echo $currency['primary_currency_name']; ?>)<sup>*</sup></label>
                      <div class="col-md-6">
                       <label class="control-label" > <span id="idConversion"></span> (Conversion Rate:<?php echo $currency['currency_conversion_rate']; ?>)</label>
                      </div>
                    </div>
                 </fieldset>
               </form>
              </div>
             </div>
                   
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary" id="btnProxy">Save Proxy</button>
      </div>
    </div>
  </div>
</div>
	<!-- Le javascript
================================================== -->
	<!-- Placed at the end of the document so the pages load faster -->
	<script
		src="<?php echo base_url(ciHtmlTheme.'/assets/js/jquery.min.js')?>"> </script>
	<script
		src="<?php echo base_url(ciHtmlTheme.'/assets/bootstrap/js/bootstrap.min.js')?>"></script>


	<!-- include equal height plugin  -->
	<script
		src="<?php echo base_url(ciHtmlTheme.'/assets/js/jquery.matchHeight-min.js')?>"></script>
	<!-- include jquery list shorting plugin plugin  -->
	<script
		src="<?php echo base_url(ciHtmlTheme.'/assets/js/hideMaxListItem.js')?>"></script>
	<!-- include jquery.fs plugin for custom scroller and selecter  -->
	<script
		src="<?php echo base_url(ciHtmlTheme.'/assets/plugins/jquery.fs.scroller/jquery.fs.scroller.js')?>"></script>
	<script
		src="<?php echo base_url(ciHtmlTheme.'/assets/plugins/jquery.fs.selecter/jquery.fs.selecter.js')?>"></script>
	<!-- include custom script for site  -->
	<script src="<?php echo base_url(ciHtmlTheme.'/assets/js/jquery.validate.min.js')?>"></script>
	<!-- include jquery autocomplete plugin  -->
	<script type="text/javascript">
	$(document).ready(function(){ 
		var c_rate='<?php echo $currency['currency_conversion_rate']; ?>';
	
	$('.proxy').click(function(e){	
		e.preventDefault(); 
		
		$("#proxy_auction_id").val($(this).attr('data-auction-id'));
		$("#proxy_lot_id").val($(this).attr('data-lot-id'));
		$("#proxy_qty").val('');
		$("#proxy_amount").val('');
		$("#ltUnit").html($(this).attr('data-unit'));
		$("#msg").html('');
		$("#idConversion").html('');
		var part_bid=$(this).attr('data-part_bid');
		if(part_bid=="N") 
			{
			$("#proxy_qty").val($(this).attr('data-total-qty'));
			}
		$('#ModalProxy').modal('show');
	});

	$("#proxy_amount").keyup(function(){
		
		  total=parseFloat(c_rate)* parseFloat($(this).val())
		  $("#idConversion").html(total.toFixed(2)); 
		});

	 $("#btnProxy").click(function(e){
		 
		   e.preventDefault();
		   var form=$("#frmProxy");
			 			  
		   form.validate({
			   
			    ignore:[],
				rules: {
				
					proxy_qty: {required : true, number:true},
					proxy_amount: {required : true, number:true},
					
					},
				messages: {	
						
					}
				});
		
	  
	   if(form.valid())
		   	 	$( "#frmProxy" ).submit();
		 	
		   
			
		});
	 $("#frmProxy").on('submit',(function(e) {
			
			e.preventDefault();
			
			var $btn = $("#btnProxy").button('loading');
			$.ajax({
				url: "<?php echo base_url();?>buyer/proxy/save_proxy/",
				type: "POST",
				data:  new FormData(this),
				contentType: false,
				cache: false,
				processData:false,
				dataType:'json',
				success: function(data)
				{
					if(data.status)
					$("#msg").html('<p class="color-green">'+data.msg+'</p>');
					else
					$("#msg").html('<p class="color-red">'+data.msg+'</p>');
					
					$btn.button('reset');
					 
					   
					},
				error: function() 
				{
					alert("Problem in saving Proxy, Try after some time");
					$btn.button('reset');
				} 	
			});
		}));
	 $(".PauseProxy").on('click',(function(e) {
			
			e.preventDefault();
			//id=$(this).id();
			//alert(id);
			var btn = $(this).button('loading');
			$.ajax({
				url: "<?php echo base_url();?>buyer/proxy/pause_resume_proxy",
				type: "POST",
				data:  {auction_id:$(this).attr('data-auction-id'),lot_id:$(this).attr('data-lot-id'),proxy_stat:$(this).attr('data-proxy_stat')},
				
				cache: false,
				
				dataType:'json',
				success: function(data)
				{
					if(data.status)
					{
						btn.button('reset');
						if(data.current_status=="Y")
						{
							//$('.'+btn+'').addClass('btn-danger');
						//	$('.'+id+'').removeClass('btn-success');
						//	$(this).html('Resume Proxy');
						}
						else
						{
							//$('.'+id+'').addClass('btn-success');
							//$('.'+id+'').removeClass('btn-danger');
							//$('.'+btn+'').html('Pause Proxy');
						}
					}
					alert(data.message);
					location.reload();
				
					 
					   
					},
				error: function() 
				{
					alert("Problem in saving Proxy, Try after some time");
					$btn.button('reset');
				} 	
			});
		}));
	
	
	});//ready document
	</script>
</body>
</html>
