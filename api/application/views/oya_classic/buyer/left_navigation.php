
<div class="card hovercard">
			<div class="cardheader"></div>
			<div class="avatar">
				<?php if ( ! empty($this->session->userdata('buyer_logo'))){ ?>
					<img  src="<?php echo $this->session->userdata('buyer_logo'); ?>" alt="Nifty Logo" class="brand-icon">
				<?php } else { ?>
					<img  src="<?php echo base_url('images/user-icon.png'); ?>" alt="Nifty Logo" class="brand-icon">
				<?php } ?> 
			</div>
			<div class="info">
				<div class="title">
					<a href="<?php echo base_url('buyer/profile')?>"><?php echo $this->session->userdata('conperson');?></a>
				</div>
				
			</div>
			<div class="bottom">
				<div class="progress-group">
                    <span class="progress-text">Profile Completion</span>
                    <span class="progress-number"><b><?php echo $this->session->userdata('profile');?></b>/100</span>

                    <div class="progress sm">
                      <div class="progress-bar progress-bar-aqua" style="width: <?php echo $this->session->userdata('profile');?>%"></div>
                    </div>
                  </div>
			</div>
		</div>
<aside>
	<div class="inner-box">
		
		<div class="user-panel-sidebar">
			
			<div class="collapse-box">
				<h5 class="collapse-title no-border">
					Auctions <a href="#MyAuction" data-toggle="collapse" class="pull-right"><i
						class="fa fa-angle-down"></i></a>
				</h5>
				<div class="panel-collapse collapse in" id="MyAuction">
					<ul class="acc-list">

						<li><a href="<?php echo base_url('buyer/auctions/current_auctions');?>"><i class="icon-docs"></i>Today's Auctions</a></li>
						<li><a href="<?php echo base_url('buyer/auctions/upcoming_auctions');?>"><i class="icon-heart"></i>	Upcomming Auctions  </a></li>
						<li><a href="<?php echo base_url('buyer/auctions/all_auctions');?>"><i class="icon-docs"></i> All Auctions</a></li>
						<li><a href="<?php echo base_url('buyer/auction_reports');?>"><i class="icon-star-circled"></i> Reports </a></li>
						<li><a href="<?php echo base_url('buyer/auctions/favourite_auctions');?>"><i class="icon-hourglass"></i> Favourit Auctions </a></li>
					</ul>
				</div>

				<hr />
				<h5 class="collapse-title no-border">
					E - Tenders <a href="#MyTender" data-toggle="collapse" class="pull-right"><i
						class="fa fa-angle-down"></i></a>
				</h5>
				<div class="panel-collapse collapse in" id="MyTender">
					<ul class="acc-list">

						<li><a href="<?php echo base_url('buyer/etenders/current_tenders');?>"><i class="icon-docs"></i>Today's E - Tenders</a></li>
						<li><a href="<?php echo base_url('buyer/etenders/upcoming_tenders');?>"><i class="icon-heart"></i>	Upcomming E - Tenders  </a></li>
						<li><a href="<?php echo base_url('buyer/etenders/all_tenders');?>"><i class="icon-docs"></i> All E - Tenders</a></li>
						<li><a href="<?php echo base_url('buyer/tender_reports');?>"><i class="icon-star-circled"></i> Reports </a></li>

					</ul>
				</div>

				<hr />
				<h5 class="collapse-title no-border">
					Products <a href="#MyProducts" data-toggle="collapse" class="pull-right"><i
						class="fa fa-angle-down"></i></a>
				</h5>
				<div class="panel-collapse collapse in" id="MyProducts">
					<ul class="acc-list">
						
						<li><a href="<?php echo base_url('buyer/products/my_active_bids');?>"><i class="icon-docs"></i> My Active Bids </a></li>
						<li><a href="<?php echo base_url('buyer/products/my_favourite_products');?>"><i class="icon-heart"></i>	My Favourite Products  </a></li>
						
					</ul>
				</div>
				<hr />
				<ul class="acc-list">
					
					<!--<li><a class="padding-left-0" href="<?php echo base_url('buyer/products/favourit_products');?>"><i	class="icon-hourglass"></i> Favourit Products </a></li>-->
					<li><a class="padding-left-0" href="<?php echo base_url('buyer/auctions/pending_approval');?>"><i	class="icon-hourglass"></i> Pending approval </a></li>
					<li><a class="padding-left-0" href="<?php echo base_url('buyer/my_associations');?>"><i	class="icon-hourglass"></i> My Associations </a></li>
					<!--<li><a class="padding-left-0" href="<?php echo base_url('buyer/wallet');?>"><i class="icon-docs"></i> Wallet</a></li>
					<li><a class="padding-left-0" href="<?php echo base_url('buyer/wallet/payment_status');?>"><i class="icon-docs"></i> Payment Status</a></li>-->
					<li><a class="padding-left-0" href="<?php echo base_url('buyer/my_messages');?>"><i class="icon-docs"></i> Messages</a></li>
					<li><a class="padding-left-0" href="<?php echo base_url('buyer/notice/personal_notice');?>"><i class="icon-heart"></i>	Notice  </a></li>
					<li><a class="padding-left-0" href="<?php echo base_url('buyer/profile/index/change_password');?>"><i class="icon-heart"></i>	Change Password  </a></li>
					<li><a class="padding-left-0" href="<?php echo base_url('user/logout');?>"><i class="icon-logout"></i>	Log Out </a></li>
					
						

					</ul>
			</div>
			<div class="user-panel-sidebar">
			
			
			
			<!-- /.collapse-box  -->
		</div>
	</div>
	</div>
	<!-- /.inner-box  -->

</aside>