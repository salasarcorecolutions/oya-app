<?php
/* echo "<pre>";
print_r($lots);
echo "</pre>"; */
?>
<body>
	<table border="0" cellpadding="0" cellspacing="0" width="100%" id="table1" class="adminlist">
		<tr>
			<td align="center"><b><font size="3">MY BIDS FOR TENDER NO  <?php echo $auction_data['tender_id']; ?></font></b></td>	
		</tr>
		<tr>
			<td>
				<table border="0" cellspacing="1" width="100%" id="table1" class="adminlist">
					<tr>
						<td align="center" nowrap colspan="4" ><b><font size="2">AUCTION DETAILS</font></b></td>
					</tr>
					<tr>
						<td width="30%" align="right" nowrap ><b>Tender No</b></td>
						<td><?php echo $auction_data['tender_id']; ?></td>
						<td width="30%" align="right"><b>Auction Type</b></td>
						<td><?=($auction_data['bid_type'] == "I") ? "Increment" : "Decrement" ; ?></td>
					</tr>
					<tr>
						<td width="30%" align="right"><b>Company</b></td>
						<td><?php echo $auction_data['vendor_name'];?></td>
						<td width="30%" align="right"><b>Tender Commencement Date</b></td>
						<td><?php echo $auction_data['tcdt']; ?></td>
					</tr>
					<tr>
						<td width="30%" align="right" nowrap><b>Tender Opening Date</b></td>
						<td><?php echo $auction_data['topen_dt']; ?></td>
						<td width="30%" align="right" nowrap><b>Tender Closing Date</b></td>
						<td><?php echo $auction_data['tclose_dt']; ?></td>
					</tr>
					<tr>
						<td width="30%" align="right" nowrap><b>Location </b></td>
						<td><?php echo $auction_data['location']; ?></td>
						<td width="30%" align="right" nowrap><b>Sector </b></td>
						<td><?php echo $auction_data['sector']; ?></td>
					</tr>
					<tr>
						<td width="30%" align="right" nowrap><b>Last CMD/EMD Submition Date </b></td>
						<td><?php echo $auction_data['last_cmd_sub_dt']." ".$auction_data['last_cmd_sub_time']; ?></td>
						<td width="30%" align="right"><b>Particulars</b></td>
						<td colspan="3"><?php echo $auction_data['particulars']; ?></td>
					</tr>		
				</table>
			</td>
		</tr>
		<tr>
			<td bgcolor="#FFF8F0" align="center"><b><font size="2">BID DETAILS</font></b></td>
		</tr>
		<tr>
			<td bgcolor="#FFF8F0" >
				<table border="0" cellspacing="1" width="100%" class="adminlist">
					<thead>
						<tr>
							<th>Lot No</th>
							<?php if($auction_data['is_lot_group'] == 'Yes'){
								echo "<th>Sub Lot No</th>";
							} ?>
							<th>Material Code</th>
							<th width="40%">Product</th>
							<th>Plant</th>
							<th>Unit</th>
							<th>Start Time</th>
							<th>End Time</th>
							<th>Total Qty</th>
							<th>Bid Qty</th>
							<th>My Bid</th>							
						</tr>
					</thead>
					<tbody>
				<?php 
				
				foreach($lots as $singleLot){ 
					/* echo "<pre>";
					print_r($singleLot);
					echo "</pre>"; */
					if(!empty($singleLot['lot_detail']['bid_amount']) || TRUE){ ?>
					<tr>						
						<td align="center"><?=$singleLot['lot_detail']['lotno']?></td>
						<?php if($auction_data['is_lot_group'] == 'Yes'){
							echo "<td></td>";
						} ?>
						<td align="center"><?=$singleLot['lot_detail']['mat_code']?></td>
						<td><?=$singleLot['lot_detail']['description']?></td>
						<td><?=$singleLot['lot_detail']['plant']?></td>
						<td><?=$singleLot['lot_detail']['unit']?></td>
						<td align="center"><?=$singleLot['lot_detail']['lot_open_date']?></td>
						<td align="center"><?=$singleLot['lot_detail']['lot_closing_date']?></td>
						<td align="center"><?=$singleLot['lot_detail']['lot_qty']." ".$singleLot['lot_detail']['unit']?></td>
						<td align="center"><?=$singleLot['lot_detail']['bid_qty']." ".$singleLot['lot_detail']['bid_unit']?></td>
						<td align="center"><?=@$singleLot['lot_detail']['bid_amount']?></td>												
					</tr>
					<?php
						if(!empty($singleLot['sub_lot_bid'])){ 
							foreach($singleLot['sub_lot_bid'] as $sub_lot_bid){
								?>
								<tr>
									<td align="center"></td>
									<td align="center"><?=$sub_lot_bid['lotno']?></td>
									<td align="center"><?=$sub_lot_bid['vendorlotno']?></td>
									<td align="left" colspan="5"><?=$sub_lot_bid['product']?></td>
									<td align="center"><?=@$sub_lot_bid['total_qty']." ".$sub_lot_bid['total_unit']?></td>
									<td align="center"><?=@$sub_lot_bid['bid_qty']." ".$sub_lot_bid['bid_unit']?></td>
									<td align="center"><?=@$sub_lot_bid['bid_amount']?></td>
								</tr>
								<?php 
							}
							?>
							<tr>
								<td colspan="11">&nbsp;</td>
							</tr>
							<?php
						}
					} 
				} ?>
					<tbody>
				</table>
			</td>
		</tr>
		<tr><td colspan="8"><font size="1"><?php echo date('l dS \of F Y h:i:s A'); ?></font></td></tr>
</body>
