<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<!-- Fav and touch icons -->
<link rel="apple-touch-icon-precomposed" sizes="144x144" href="<?php echo base_url(ciHtmlTheme.'/assets/ico/apple-touch-icon-144.png')?>">
<link rel="apple-touch-icon-precomposed" sizes="114x114" href="<?php echo base_url(ciHtmlTheme.'/assets/ico/apple-touch-icon-114.png')?>">
<link rel="apple-touch-icon-precomposed" sizes="72x72" href="<?php echo base_url(ciHtmlTheme.'/assets/ico/apple-touch-icon-72.png')?>">
<link rel="apple-touch-icon-precomposed" sizes="57x57" href="<?php echo base_url(ciHtmlTheme.'/assets/ico/apple-touch-icon-57.png')?>">
<link rel="shortcut icon" href="<?php echo base_url(ciHtmlTheme.'/assets/ico/favicon.png')?>">
<title>Tender Reports | <?php echo $this->session->userdata('conperson');?></title>
<!-- Bootstrap core CSS -->
<link href="<?php echo base_url(ciHtmlTheme.'/assets/bootstrap/css/bootstrap.min.css')?>" rel="stylesheet">
<link rel="stylesheet" href="<?php echo base_url();?>assets/plugins/datatables/dataTables.bootstrap.css">
<link rel="stylesheet" href="<?php echo base_url();?>assets/plugins/datatables/jquery.dataTables_themeroller.css">
<!-- Custom styles for this template -->
<link href="<?php echo base_url(ciHtmlTheme.'/assets/css/style.css')?>" rel="stylesheet">
<link href="<?php echo base_url(ciHtmlTheme.'/assets/css/custom.css')?>" rel="stylesheet">
<!-- styles needed for carousel slider -->
<link href="<?php echo base_url(ciHtmlTheme.'/assets/css/buyer.css')?>" rel="stylesheet">


<script>
    paceOptions = {
      elements: true
    };
</script>
<script src="<?php echo base_url(ciHtmlTheme.'/assets/js/pace.min.js')?>"></script>

 </head>
<body>

<div id="wrapper">
<!-- /.header -->
 <?php $this->load->view(ciHtmlTheme.'/buyer/buyer_header');?>
  <!-- /.header -->
  <div class="main-container">
   <div class="container-fluid">
      <div class="row">
        <div class="col-sm-3 page-sidebar">
         <?php $this->load->view(ciHtmlTheme.'/buyer/left_navigation');?>
        </div>
        <!--/.page-sidebar-->
        
        <div class="col-sm-9 page-content">
         	<div class="inner-box">
         	 <h2 class="title-2"><i class="icon-heart-1"></i> All Reports </h2>
         	 <span class="page-sub-header-sub small">List of Reports</span>
              <section id="content">
                    <section class="vbox">
                        <section class="scrollable padder">
                            
                            <div class="row">
                                <div class="col-sm-3">
                                    <section class="panel panel-default">
                                        <header class="panel-heading"> Report </header>
                                        <div class="list-group bg-white"> 
                                            <button class="list-group-item col-lg-12" onclick="javascript:report_my_bids();">Bids Summary Report</button> 
                                            <button class="list-group-item col-lg-12" onclick="javascript:report_allocated_lots();">Allocated Lots</button>
                                            <button class="list-group-item col-lg-12" onclick="javascript:report_participated_auctions();">Participated Tenders</button>
                                            <button class="list-group-item col-lg-12" onclick="javascript:report_emd_cmd();">My EMD/CMD</button>
                                        </div>
                                    </section>
                                </div>
                                <div class="col-sm-4">
                                    <section class="panel panel-default">
                                        <header class="panel-heading"> Selection </header>
                                        <td valign="top">
                                            <form action="" name="frm" method="post">
                                                <table class="table m-b-none table-striped table-hower">
                                        
                                                        <tbody>
                                                            <tr>
                                                                <td class="col-xs-4" >Tender Id </td>
                                                                <td>
                                                                    <select class="form-control" name="tid" id="tid" onChange="javascript:change_auction_id()">
                                                                        <option vendor_name="" value="">Select Tender</option>
                                                                        <?php
                                                                        for($i=0;$i<count($auctiondet);$i++)
                                                                        { ?>
                                                                            <option value="<?=$auctiondet[$i]['tid']; ?>" vendor_name="<?=$auctiondet[$i]['vendor_name']?>"><?=$auctiondet[$i]['tender_id'];?></option>
                                                                        <?php 
                                                                        }
                                                                        ?>
                                                                    </select>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td class="col-xs-4">Lot No</td>
                                                                <td >
                                                                    <select class="form-control" name="lotno" id="lotno">
                                                                        <option value="">Select Lot No</option>
                                                                    </select>
                                                                </td>
                                                            </tr>
                                                            <tr id="vendor_name_tr" style="display:none;">
                                                                <td class="col-xs-4">Vendor</td>
                                                                <td><label id="vendor_name">Hello<label></td>
                                                            </tr>
                                                        </tbody>
                                                        
                                                </table>
                                            </form>
                                        </td>
                                    </section>
                                </div>
                            </div>
                            <p class="pull-right" style="position: fixed; right: 30px; bottom: 15px;">Copyright &copy;  <?=date('Y'); ?>. <a href="http://salasarauction.com/">SalasarAuction.com</a>. All Rights Reserved</p>
                        </section>
                    </section>
	                <a href="#" class="hide nav-off-screen-block" data-toggle="class:nav-off-screen" data-target="#nav"></a> 
              </section>
				
			
        </div>
        <!--/.page-content--> 
      </div>
      <!--/.row--> 
    </div>
    <!--/.container--> 
  </div>
  <!-- /.main-container -->
  </div>
  
  <!-- /.footer --> 
  <?php $this->load->view(ciHtmlTheme.'/includes/footer');?>
 
</div>
<!-- /.wrapper --> 

<!-- Le javascript
================================================== --> 
<!-- Placed at the end of the document so the pages load faster --> 
<script src="<?php echo base_url(ciHtmlTheme.'/assets/js/jquery.min.js')?>"> </script>
<script src="<?php echo base_url(ciHtmlTheme.'/assets/bootstrap/js/bootstrap.min.js')?>"></script> 
<!-- include equal height plugin  --> 
<script src="<?php echo base_url(ciHtmlTheme.'/assets/js/jquery.matchHeight-min.js')?>"></script> 
<!-- include jquery list shorting plugin plugin  --> 
<script src="<?php echo base_url(ciHtmlTheme.'/assets/js/hideMaxListItem.js')?>"></script> 
<!-- include jquery.fs plugin for custom scroller and selecter  --> 
<script src="<?php echo base_url(ciHtmlTheme.'/assets/plugins/jquery.fs.scroller/jquery.fs.scroller.js')?>"></script>
<script src="<?php echo base_url(ciHtmlTheme.'/assets/plugins/jquery.fs.selecter/jquery.fs.selecter.js')?>"></script>
<script src="<?php echo base_url();?>assets/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="<?php echo base_url();?>assets/plugins/datatables/dataTables.bootstrap.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/manual_datatable.js"></script>
<!-- include custom script for site  --> 
<script src="<?php echo base_url(ciHtmlTheme.'/assets/js/script.js')?>"></script>
<!-- include jquery autocomplete plugin  -->
</body>
<script>

	function report_allocated_lots()
	{
		if($("#tid").val() == "")
		{
		  	alert("Please select Tender");
			$("#tid").focus()
		  	return false;
		}else{
		  	a="<?php echo base_url('buyer/tender_reports/report_allocated_lots/');?>"+$("#tid").val()+"/"+$("#lotno").val();
		  	window.open(a,'CLIENT_WISE_LOT','width=700,height=600,toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=yes,copyhistory=no,resizable=yes');
		}
	}
	function report_emd_cmd()
	{
		a="<?php echo base_url('buyer/tender_reports/report_emd_cmd/');?>"+$("#tid").val();
		window.open(a,'MY EMD CMD','width=700,height=600,toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=yes,copyhistory=no,resizable=yes');
	}
	function report_my_bids()
	{
		if($("#tid").val() == "")
		{
		  	alert("Please select Tender");			
			$("#tid").focus()
		  	return false;
		}else{
			a="<?php echo base_url('buyer/tender_reports/report_my_bids/');?>"+$("#tid").val()+"/"+$("#lotno").val();
			window.open(a,'MY BIDS','width=700,height=600,toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=yes,copyhistory=no,resizable=yes');
		}
	}
	function report_participated_auctions()
	{
		a="<?php echo base_url('buyer/tender_reports/report_participated_auctions/');?>"+$("#tid").val();
		window.open(a,'Participated Tenders','width=700,height=600,toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=yes,copyhistory=no,resizable=yes');
	}
	function change_auction_id(){
		var vendor_name = $('#tid '+'option:selected').attr('vendor_name');
		if(vendor_name != ""){
			$("#vendor_name").html(vendor_name);
			$("#vendor_name_tr").show(400);
		}else{
			$("#vendor_name_tr").hide(400);
		}
		if($("#tid").val() > 0){
			$.ajax({
				url:"<?php echo base_url('buyer/tender_reprots/get_lots');?>",
				data:{'tid':$("#tid").val()},
				type:"post",
				dataType:'json',
				success:function(response){
					if(response.status == "success"){
						$("#lotno").html('<option value="">All Lots</option>'+response.data);
					}else{
						alert(response.msg);
						$("#lotno").html('<option value="">All Lots</option>');
					}
				}
			});
		}else{
			$("#lotno").html('<option value="">All Lots</option>');
		}		
	}
</script>
</html>
