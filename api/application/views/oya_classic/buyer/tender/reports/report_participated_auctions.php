<pre>
<?php 
//print_r($auction_data);exit;
?>
</pre>
<body>
	<table border="0" cellpadding="0" cellspacing="0" width="100%" id="table1" class="adminlist">
		<tr>
			<td align="center"><b><font size="3">TENDER PARTICIPATION</font></b></td>	
		</tr>
		<tr>
			<td>
				<table border="0" cellspacing="1" width="100%" class="adminlist">
					<thead>
						<tr>
							<th>Sale No</th>
							<th>Auction Type</th>
							<th>Vendor</th>
							<th>Commencement Date</th>
							<th>Opening Date</th>
							<th>Closing Date</th>
							<th>Last EMD/CMD Submission Date</th>
							<th>Location</th>
							<th>Sector</th>							
							<th>Particulars</th>
						</tr>
					</thead>
					<tbody>
					<?php
					foreach($auction_data as $singleAution){ ?>
						<tr>
							<td align="center"><?=$singleAution['tender_id'];?></td>
							<td align="center"><?=($singleAution['bid_type'] == "I") ? "Increment" : "Decrement" ; ?></td>
							<td align="center"><?=$singleAution['vendor_name'];?></td>
							<td align="center"><?=$singleAution['tcdt'];?></td>
							<td align="center"><?=$singleAution['topen_dt'];?></td>							
							<td align="center"><?=$singleAution['tclose_dt'];?></td>							
							<td align="center"><?=$singleAution['last_cmd_sub_dt'];?></td>							
							<td align="center"><?=$singleAution['location'];?></td>							
							<td align="center"><?=$singleAution['sector'];?></td>							
							<td align="center"><?=$singleAution['particulars'];?></td>														
						</tr>
					<?php 
					} ?>	
						<tr><td colspan="10">&nbsp;</td></tr>
						<tr><td colspan="10"><font size="1"><?php echo date('l dS \of F Y h:i:s A'); ?></font></td></tr>
					</tbody> 					
				</table>
			</td>
		</tr>
	</table>
</body>
</html>