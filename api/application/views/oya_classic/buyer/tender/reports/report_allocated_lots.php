<pre>
<?php //print_r($lots)?>
</pre>
<body>
	<table border="0" cellpadding="0" cellspacing="0" width="100%" id="table1" class="adminlist">
		<tr>
			<td align="center"><b><font size="3">ALLOCATED LOTS FOR SALE/AUCTION NO  <?php echo $auction_data['tender_id']; ?></font></b></td>	
		</tr>
		<tr>
			<td>
				<table border="0" cellspacing="1" width="100%" id="table1" class="adminlist">
					<tr>
						<td align="center" nowrap colspan="4" ><b><font size="2">AUCTION DETAILS</font></b></td>
					</tr>
					<tr>
						<td width="30%" align="right" nowrap ><b>Tender No</b></td>
						<td><?php echo $auction_data['tender_id']; ?></td>
						<td width="30%" align="right"><b>Auction Type</b></td>
						<td><?=($auction_data['bid_type'] == "I") ? "Increment" : "Decrement" ; ?></td>
					</tr>
					<tr>
						<td width="30%" align="right"><b>Company</b></td>
						<td><?php echo $auction_data['vendor_name'];?></td>
						<td width="30%" align="right"><b>Tender Commencement Date</b></td>
						<td><?php echo $auction_data['tcdt']; ?></td>
					</tr>
					<tr>
						<td width="30%" align="right" nowrap><b>Tender Opening Date</b></td>
						<td><?php echo $auction_data['topen_dt']; ?></td>
						<td width="30%" align="right" nowrap><b>Tender Closing Date</b></td>
						<td><?php echo $auction_data['tclose_dt']; ?></td>
					</tr>
					<tr>
						<td width="30%" align="right" nowrap><b>Location </b></td>
						<td><?php echo $auction_data['location']; ?></td>
						<td width="30%" align="right" nowrap><b>Sector </b></td>
						<td><?php echo $auction_data['sector']; ?></td>
					</tr>
					<tr>
						<td width="30%" align="right" nowrap><b>Last CMD/EMD Submition Date </b></td>
						<td><?php echo $auction_data['last_cmd_sub_dt']." ".$auction_data['last_cmd_sub_time']; ?></td>
						<td width="30%" align="right"><b>Particulars</b></td>
						<td colspan="3"><?php echo $auction_data['particulars']; ?></td>
					</tr>		
				</table>
			</td>
		</tr>
		<tr>
			<td bgcolor="#FFF8F0" align="center"><b><font size="2">BID DETAILS</font></b></td>
		</tr>		
		<tr>
			<td>
				<table border="0" cellspacing="1" width="100%" class="adminlist">
					<thead>
						<tr>
							<th>LOT NO</th>
							<th>MATERIAL CODE</th>
							<th>DESCRIPTION</th>
							<th>PLANT</th>
							<th>TOTAL QTY.</th>
							<th>UNIT</th>
							<th>REMARKS</th>
						</tr>
					</thead>
					<tbody>
						<?php
						foreach($lots as $singleLot){ ?>
						<tr>
							<td align="center"><?=$singleLot['lot_detail']['lotno'];?></td>
							<td align="center"><?=$singleLot['lot_detail']['mat_code'];?></td>
							<td align="center"><?=$singleLot['lot_detail']['description'];?></td>
							<td align="center"><?=$singleLot['lot_detail']['plant'];?></td>
							<td align="center"><?=$singleLot['lot_detail']['lot_qty'];?></td>								
							<td align="center"><?=$singleLot['lot_detail']['unit'];?></td>
							<td align="center"><?=$singleLot['lot_detail']['remarks'];?></td>
						</tr>
						<?php 
						} ?>
						<tr><td colspan="12">&nbsp;</td></tr>
						<tr><td colspan="12"><font size="1"><?php echo date('l dS \of F Y h:i:s A'); ?></font></td></tr>
					</tbody> 					
				</table>
			</td>
		</tr>
	</table>
</body>
</html>