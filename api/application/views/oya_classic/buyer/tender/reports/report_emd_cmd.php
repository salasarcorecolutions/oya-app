<pre>
<?php 
//print_r($auction_data);exit;
?>
</pre>
<body>
	<table border="0" cellpadding="0" cellspacing="0" width="100%" id="table1" class="adminlist">
		<tr>
			<td align="center"><b><font size="3">EMD CMD REPORT</font></b></td>	
		</tr>
		<tr>
			<td>
				<table border="0" cellspacing="1" width="100%" class="adminlist">
					<thead>
						<tr>
							<th rowspan="2">Tender</th>
							<th>Vendor</th>
							<th>Commencement Date</th>
							<th>Opening Date</th>
							<th>Closing Date</th>
							<th>CMD Amount</th>
							<th>Bank Name</th>
							<th>CMD DD NO</th>
							<th>CMD Date</th>
							<th>EMD Amount</th>
							<th>Bank Name</th>
							<th>EMD DD NO</th>
							<th>EMD Date</th>
						</tr>
					</thead>
					<tbody>
					<?php
					foreach($emd_cmd_data as $singleRow){ ?>
						<tr>
							<td align="center"><?=$singleRow['tender_id'];?></td>							
							<td><?=$singleRow['vendor_name'];?></td>
							<td align="center"><?=$singleRow['tcdt'];?></td>
							<td align="center"><?=$singleRow['topen_dt'];?></td>							
							<td align="center"><?=$singleRow['tclose_dt'];?></td>
							<td align="center"><?=$singleRow['cmdamt'];?></td>
							<td align="center"><?=$singleRow['cmdbank'];?></td>
							<td align="center"><?=$singleRow['cmdddno'];?></td>
							<td align="center"><?=$singleRow['cmddate'];?></td>
							<td align="center"><?=$singleRow['emdamt'];?></td>
							<td align="center"><?=$singleRow['emdbank'];?></td>
							<td align="center"><?=$singleRow['emdddno'];?></td>
							<td align="center"><?=$singleRow['emddate'];?></td>
						</tr>
					<?php 
					} ?>	
						<tr><td colspan="13">&nbsp;</td></tr>
						<tr><td colspan="13"><font size="1"><?php echo date('l dS \of F Y h:i:s A'); ?></font></td></tr>
					</tbody> 					
				</table>
			</td>
		</tr>
	</table>
</body>
</html>