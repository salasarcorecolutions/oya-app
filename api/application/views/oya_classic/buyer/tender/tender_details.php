<?php
defined('BASEPATH') or exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<!-- Fav and touch icons -->
<link rel="apple-touch-icon-precomposed" sizes="144x144"
	href="<?php echo base_url(ciHtmlTheme.'/assets/ico/apple-touch-icon-144.png')?>">
<link rel="apple-touch-icon-precomposed" sizes="114x114"
	href="<?php echo base_url(ciHtmlTheme.'/assets/ico/apple-touch-icon-114.png')?>">
<link rel="apple-touch-icon-precomposed" sizes="72x72"
	href="<?php echo base_url(ciHtmlTheme.'/assets/ico/apple-touch-icon-72.png')?>">
<link rel="apple-touch-icon-precomposed" sizes="57x57"
	href="<?php echo base_url(ciHtmlTheme.'/assets/ico/apple-touch-icon-57.png')?>">
<link rel="shortcut icon"
	href="<?php echo base_url(ciHtmlTheme.'/assets/ico/favicon.png')?>">
<title>AUCTION DETAILS - <?php echo @$saleno?></title>
<!-- Bootstrap core CSS -->
<link
	href="<?php echo base_url(ciHtmlTheme.'/assets/bootstrap/css/bootstrap.min.css')?>"
	rel="stylesheet">
<!-- Custom styles for this template -->
<link href="<?php echo base_url(ciHtmlTheme.'/assets/css/style.css')?>"
	rel="stylesheet">
<link href="<?php echo base_url(ciHtmlTheme.'/assets/css/custom.css')?>"
	rel="stylesheet">
<style type="text/css">
.box {
	position: relative;
	border-radius: 0px;
	background: #ffffff;
	border-top: 1px solid #d2d6de;
	margin-bottom: 20px;
	width: 100%;
	box-shadow: 0 1px 1px rgba(0, 0, 0, 0.1);
}
.box-title {
    display: inline-block;
    font-size: 18px;
    margin: 0;
    line-height: 1;
}
.box-header {
	color: #444;
	display: block;
	padding: 10px;
	position: relative;
}

.box-body {
	border-top-left-radius: 0;
	border-top-right-radius: 0;
	border-bottom-right-radius: 3px;
	border-bottom-left-radius: 3px;
	padding: 10px;
}

.box-header>.box-tools {
	position: absolute;
	right: 10px;
	top: 5px;
}
</style>

<script>
    paceOptions = {
      elements: true
    };
</script>
<script
	src="<?php echo base_url(ciHtmlTheme.'/assets/js/pace.min.js')?>"></script>
</head>
<body>

	<div id="wrapper">
		<!-- /.header -->
 <?php $this->load->view(ciHtmlTheme.'/includes/header');?>
  <!-- /.header -->
		<div class="main-container">
			<div class="container-fluid">
				<div class="row">
					<div class="col-sm-3 page-sidebar">
         <?php $this->load->view(ciHtmlTheme.'/buyer/left_navigation');?>
        </div>
					<!--/.page-sidebar-->

					<div class="col-sm-9 page-content">
						<div class="inner-box">
							<h2 class="title-2">
								<i class="icon-heart-1"></i> DETAILS OF SALE NO: <?php echo $saleno?></h2>
							<div class="row">
								<div class="col-md-8">
									<div class="box">
										<div class="box-header">
											<h5 class="box-title">DETAILS</h5>
										</div>
										<!-- /.box-header -->
										<div class="box-body no-padding">
										<table class="table table-responsive">
										<tr><td class="text-right">Sale No :</td><td><?php echo $saleno; ?></td></tr>
										<tr><td class="text-right">Vendor :</td><td><?php echo $auccomp; ?></td></tr>
										<tr><td class="text-right">Start Date :</td><td><?php echo $start_date; ?></td></tr>
										<tr><td class="text-right">End Date :</td><td><?php echo $end_date; ?></td></tr>
										<tr><td class="text-right">Particulars :</td><td><?php echo $aucpartcular; ?></td></tr>
										<tr><td class="text-right">Total Lots :</td><td><?php echo $nooflots; ?></td></tr>
										</table>
										</div>
									</div>
								</div>
								<div class="col-md-4">
									<div class="box">
										<div class="box-header">
											<h5 class="box-title">DOCUMENTS</h5>
											<table class="table">
												<thead>
													<tr>
														<th>Document Name</th>
														<th>View</th>
													</tr>
												</thead>
												<tbody>
												<?php foreach($docs as $doc_items) { ?>
													<tr>

													<td><?php echo $doc_items['doc_name']?></td>
													<td><a href="<?php echo $doc_items['doc_path']?>" target="_new"><i class="fa fa-link"></i> View</a></td>
													</tr>
													<?php }?>
												</tbody>
											</table>

										</div>
										<!-- /.box-header -->
										<div class="box-body no-padding">
											
										</div>
										<!-- /.box-body -->
									</div>
								</div>
							</div>
							<div class="row">
							<div class="col-md-12">
									<div class="box">
										<div class="box-header">
											<h5 class="box-title">LOTS</h5>


										</div>
										<!-- /.box-header -->
										<div class="box-body no-padding">
											<table class="table table-bordered">
												<tbody>
                                                <tr>
                                                    <th>LOT NO</th>
                                                    <th>PLANT</th>
                                                    <th>VENDOR LOT</th>
                                                    <th>PRODUCT</th>
                                                    <th>BID START</th>
                                                    <th>BID END</th>
                                                    <th>QUANTITY</th>
                                                    <th>BID QUANTITY</th>
                                                </tr>
													<tr>
												<?php foreach($lots as $lot_items){?>
													<tr>
														<td><?php echo $lot_items['lotno'];?></td>
														<td><?php echo $lot_items['plant'];?></td>
														<td><?php echo $lot_items['vendor_ltno'];?></td>
														<td><a href="<?php echo base_url('buyer/etenders/lot_details/'.$auction_id.'/'.MD5($lot_items['lotno']))?>"><?php echo $lot_items['description'];?></a></td>
														<td><?php echo servertz_to_usertz($lot_items['lot_open_date'],$this->session->userdata('user_tz'));?></td>
														<td><?php echo servertz_to_usertz($lot_items['lot_closing_date'],$this->session->userdata('user_tz'));?></td>
														<td><?php echo $lot_items['lot_qty'];?> <?php echo $lot_items['unit'];?></td>
														<td><?php echo $lot_items['bid_qty'];?> <?php echo $lot_items['unit'];?></td>
													</tr>
													<?php }?>

												</tbody>
											</table>
										</div>
										<!-- /.box-body -->
									</div>
								</div>
								</div>
						</div>
						<!--/.page-content-->
					</div>
					<!--/.row-->
				</div>
				<!--/.container-->
			</div>
			<!-- /.main-container -->


			<!-- /.footer --> 
  <?php $this->load->view(ciHtmlTheme.'/includes/footer');?>
 
</div>
		<!-- /.wrapper -->
	</div>
	<!-- Le javascript
================================================== -->
	<!-- Placed at the end of the document so the pages load faster -->
	<script
		src="<?php echo base_url(ciHtmlTheme.'/assets/js/jquery.min.js')?>"> </script>
	<script
		src="<?php echo base_url(ciHtmlTheme.'/assets/bootstrap/js/bootstrap.min.js')?>"></script>
	<!-- include equal height plugin  -->
	<script
		src="<?php echo base_url(ciHtmlTheme.'/assets/js/jquery.matchHeight-min.js')?>"></script>
	<!-- include jquery list shorting plugin plugin  -->
	<script
		src="<?php echo base_url(ciHtmlTheme.'/assets/js/hideMaxListItem.js')?>"></script>
	<!-- include jquery.fs plugin for custom scroller and selecter  -->
	<script
		src="<?php echo base_url(ciHtmlTheme.'/assets/plugins/jquery.fs.scroller/jquery.fs.scroller.js')?>"></script>
	<script
		src="<?php echo base_url(ciHtmlTheme.'/assets/plugins/jquery.fs.selecter/jquery.fs.selecter.js')?>"></script>
	<!-- include custom script for site  -->
	<script src="<?php echo base_url(ciHtmlTheme.'/assets/js/script.js')?>"></script>
	<!-- include jquery autocomplete plugin  -->
</body>
</html>
