<?php
defined('BASEPATH') or exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<!-- Fav and touch icons -->
<link rel="apple-touch-icon-precomposed" sizes="144x144"
	href="<?php echo base_url(ciHtmlTheme.'/assets/ico/apple-touch-icon-144.png')?>">
<link rel="apple-touch-icon-precomposed" sizes="114x114"
	href="<?php echo base_url(ciHtmlTheme.'/assets/ico/apple-touch-icon-114.png')?>">
<link rel="apple-touch-icon-precomposed" sizes="72x72"
	href="<?php echo base_url(ciHtmlTheme.'/assets/ico/apple-touch-icon-72.png')?>">
<link rel="apple-touch-icon-precomposed" sizes="57x57"
	href="<?php echo base_url(ciHtmlTheme.'/assets/ico/apple-touch-icon-57.png')?>">
<link rel="shortcut icon"
	href="<?php echo base_url(ciHtmlTheme.'/assets/ico/favicon.png')?>">
<title>TENDER LOT DETAILS OF SALE NO <?php echo @$auction['tender_id']?></title>
<!-- Bootstrap core CSS -->
<link
	href="<?php echo base_url(ciHtmlTheme.'/assets/bootstrap/css/bootstrap.min.css')?>"
	rel="stylesheet">
<!-- Custom styles for this template -->
<link href="<?php echo base_url(ciHtmlTheme.'/assets/css/style.css')?>"
	rel="stylesheet">
<link href="<?php echo base_url(ciHtmlTheme.'/assets/css/custom.css')?>"
	rel="stylesheet">


<script>
    paceOptions = {
      elements: true
    };
</script>
<script
	src="<?php echo base_url(ciHtmlTheme.'/assets/js/pace.min.js')?>"></script>
</head>
<body>

	<div id="wrapper">
		<!-- /.header -->
 <?php $this->load->view(ciHtmlTheme.'/includes/header');?>
  <!-- /.header -->
		<div class="main-container">
			<div class="container-fluid">
				<div class="row">
					<div class="col-sm-3 page-sidebar">
         <?php $this->load->view(ciHtmlTheme.'/buyer/left_navigation');?>
        </div>
					<!--/.page-sidebar-->

					<div class="col-sm-9 page-content">
						<div class="inner-box">
							<h2 class="title-2">
								<i class="icon-heart-1"></i> DETAILS OF SALE NO: <?php echo $auction['tender_id']?></h2>
							<div class="row">
								<div class="col-md-12">
									<div class="panel panel-light-green">
										<div class="panel-heading">
											<h5 class="panel-title">DETAILS</h5>

											
										</div>
										<!-- /.box-header -->
										<div class="panel-body no-padding">
										<table class="table table-responsive">
										<tr><td class="text-right">Sale No :</td><td><?php echo $auction['tender_id']; ?></td></tr>
										<tr><td class="text-right">Vendor :</td><td><?php echo $auction['vendor_name']; ?></td></tr>

										</table>
										
										

										</div>
									</div>
								</div>
								
							</div>
							<div class="row">
							<div class="col-md-8">
									<div class="panel panel-light-green"">
										<div class="panel-heading">
											<h5 class="panel-title">LOTS</h5>


										</div>
										<!-- /.box-header -->
										<div class="panel-body no-padding">
											<table class="table table-bordered">
												<tbody>
													
													
												
													<tr>
														<td>LOT NO</td>
														<td><?php echo $lot_items['lotno'];?></td>
													</tr>
													<tr>
														<td>PLANT</td>
														<td><?php echo $lot_items['plant'];?></td>
													</tr>
													<tr>	
														<td>VENDOR LOT</td>
														<td><?php echo $lot_items['vendor_ltno'];?></td>
													</tr>
													<tr>	
														<td>PRODUCT</td>
														<td><?php echo $lot_items['description'];?></td>
													</tr>
													<tr>
														<td>BID START</td>
														<td><?php echo servertz_to_usertz($lot_items['lot_open_date'],$this->session->userdata('user_tz'));?></td>
													</tr>
													<tr>
														<td>BID END</td>
														<td><?php echo servertz_to_usertz($lot_items['lot_closing_date'],$this->session->userdata('user_tz'));?></td>
													</tr>
													<tr>	
														<td>QUANTITY</td>
														<td><?php echo $lot_items['lot_qty'];?> <?php echo $lot_items['unit'];?></td>
													</tr>
													<tr>	
														<td>BID QUANTITY</td>
														<td><?php echo $lot_items['bid_qty'];?> <?php echo $lot_items['unit'];?></td>
													</tr>
												</tbody>
											</table>
										</div>
										<!-- /.box-body -->
									</div>
								</div>
								<div class="col-md-4">
									<div class="panel panel-light-green">
										<div class="panel-heading">
											<h5 class="panel-title">LOT IMAGES</h5>
										</div>
										<!-- /.box-header -->
										<div class="panel-body no-padding">
										<?php 
										foreach($lot_images as $imgs) {
										      echo '<a href="'.$imgs['url'].'" target="_new" ><img src="'.$imgs['url'].'" height="80px" style="padding:10px"/></a>';
										    
										}?>
										</div>
									</div>
										
								</div>
						</div>
						<!--/.page-content-->
					</div>
					<!--/.row-->
				</div>
				<!--/.container-->
			</div>
			<!-- /.main-container -->


			<!-- /.footer --> 
  <?php $this->load->view(ciHtmlTheme.'/includes/footer');?>
 
</div>
		<!-- /.wrapper -->
	</div>
	<!-- Le javascript
================================================== -->
	<!-- Placed at the end of the document so the pages load faster -->
	<script
		src="<?php echo base_url(ciHtmlTheme.'/assets/js/jquery.min.js')?>"> </script>
	<script
		src="<?php echo base_url(ciHtmlTheme.'/assets/bootstrap/js/bootstrap.min.js')?>"></script>
	<!-- include equal height plugin  -->
	<script
		src="<?php echo base_url(ciHtmlTheme.'/assets/js/jquery.matchHeight-min.js')?>"></script>
	<!-- include jquery list shorting plugin plugin  -->
	<script
		src="<?php echo base_url(ciHtmlTheme.'/assets/js/hideMaxListItem.js')?>"></script>
	<!-- include jquery.fs plugin for custom scroller and selecter  -->
	<script
		src="<?php echo base_url(ciHtmlTheme.'/assets/plugins/jquery.fs.scroller/jquery.fs.scroller.js')?>"></script>
	<script
		src="<?php echo base_url(ciHtmlTheme.'/assets/plugins/jquery.fs.selecter/jquery.fs.selecter.js')?>"></script>
	<!-- include custom script for site  -->
	<script src="<?php echo base_url(ciHtmlTheme.'/assets/js/script.js')?>"></script>
	<!-- include jquery autocomplete plugin  -->
</body>
</html>
