<div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                 <h4 class="modal-title text-info">Tender: <?=$lot_details['tender_id']; ?></h4>
            </div>			<!-- /modal-header -->
            <div class="modal-body">
			<div id="IdMsg"></div>
				<table width="400px" class="table table-striped table-bordered">
					<tr><th>Description :</th><td> <?=$lot_details['description']; ?></td></tr>
					<tr><th>Remarks :</th><td><?=$lot_details['remarks']; ?></td></tr>
					<tr><th>Material Code :</th><td><?=$lot_details['mat_code']; ?></td></tr>
					<tr><th>Closing Time :</th><td><?=$lot_details['lot_closing_date']; ?></td></tr>
					<tr><th>Total Quantity :</th><td><?=(float)$lot_details['lot_qty']; ?> <?=$lot_details['unit']; ?></td></tr>
					<tr><th>Bid Quantity :</th><td><?=(float)$lot_details['min_bid']; ?>  <?=$lot_details['unit']; ?></td></tr>
					<?php
					if($lot_details['part_bid']=='Y')
					{
						?>
						<tr>
							<th>Your Quantity <span class='text-danger'>*</span>:</th>
							<td>
								<input type="text" name="txtQty" class='form-control'   id="txtQty" required onkeyup="validatetenderbid(this);" onblur="validatetenderbid(this);" style="width:50%;float:left;" />
							</td>
						</tr>
						<?php
					}else{
						?>
						<tr class="hide">
							<th>Your Quantity <span class='text-danger'>*</span>:</th>
							<td>
								<input type="text" name="txtQty" style="width:50%;float:left;"  class='form-control' id="txtQty" value="<?=$lot_details['lot_qty']; ?>" readonly="readonly"  />
							</td>
						</tr>
						<?php
					}
					?>
					<!--<tr>
						<th>Your Quantity <span class='text-danger'>*</span>:</th>
						<td>
						<?php
						if($lot_details['part_bid']=='Y')
						{
						?>
						<form>
							<input type="text" name="txtQty" class='form-control'   id="txtQty" required onkeyup="validatetenderbid(this);" onblur="validatetenderbid(this);" style="width:50%;float:left;" />
						</form>
						<?php
						}else{
						?>
						<input type="text" name="txtQty" style="width:50%;float:left;"  class='form-control' id="txtQty" value="<?=$lot_details['lot_qty']; ?>" readonly="readonly"  />
						<?php
						}
						?>
						</td>
					</tr>-->
					<tr style='display:none;'><td class='text-danger' id="your_qty"></td></tr>
					<tr>	
						<th>Your Bid <span class='text-danger'>*</span>:</th>
						<td>
							<input type="text" class='form-control' name="txtMyBid"  id="txtMyBid" onkeyup="validatetenderbid(this);" onblur="validatetenderbid(this);" style="width:50%;float:left;" />
							<span id="lot_min_bid_unit" style="width:50%;float:left;font-size: 25px;padding-left: 20px;" > / <?=(float)$lot_details['min_bid']; ?> <?=$lot_details['unit']; ?></span>
							<span id="currency" style='display:none;'></span></td>
					</tr>
					<?php 
					$show_your_total_amt = "hide";
					if($lot_details['min_bid'] != $lot_details['lot_qty']){
						$show_your_total_amt = "";
					}?>
					<tr class="<?=$show_your_total_amt?>">	
						<th>Your Total Amount <span class='text-danger'>*</span>:</th>
						<td>
							<input type="text" class='form-control' name="txtMyTotalBidAmount"  id="txtMyTotalBidAmount" style="width:50%;float:left;" readonly />
							<span id="lot_min_bid_unit" style="width:50%;float:left;font-size: 25px;padding-left: 20px;" > / <?=(float)$lot_details['lot_qty']; ?> <?=$lot_details['unit']; ?></span>
							<span id="currency" style='display:none;'></span></td>
					</tr>
					<tr>
						<td class='text-danger' id="bid"></td>
						<td class='text-danger' id="mybid_validation"></td>
					</tr>	
				</table>		
            </div>			<!-- /modal-body -->
            <input type="hidden" name="bid_type"  id="bid_type"  value="<?=$lot_details['bid_type'];?>"/>
            <input type="hidden" name="lot"  id="lot"  value="<?=$lot_details['lotno'];?>"/>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" onclick="refresh()" data-dismiss="modal">Close</button>
                <button type="button" id="btnSave" class="btn btn-primary">Save changes</button>
            </div>			<!-- /modal-footer -->
<script>
			$("#btnSave").click(function() {
				var your_qty=$.trim($("#txtQty").val());				
				var bid_type=$("#bid_type").val();
				var lot=$("#lot").val();
				var txtMyBid=$.trim($("#txtMyBid").val());
				if(your_qty!="")
				{
				if(txtMyBid!="")
				{
				 $.ajax({        
				url:"<?=base_url();?>buyer/live_tenders/save_bid/"+"<?php echo $this->uri->segment(4);?>"+"/"+lot+"/"+your_qty+"/"+bid_type+"/"+txtMyBid,
				success:function(data){
					if(data){
						if(data.trim()=="BID Saved Successfully"){
							$("#IdMsg").html("<div class='alert alert-success alert-dismissable'>"+data+"</div>");
						}else{
							$("#IdMsg").html("<div class='alert alert-danger alert-dismissable'>"+data+"</div>");
						}
					}else{
						$("#IdMsg").html("<div class='alert alert-danger alert-dismissable'>Invalid Request</div>");
					}					
				},
				complete:function(data)	{}
				});//AJAX
			}
			else{
				$("#bid").html("Please Enter Your Bid Amount");
				$("#txtMyBid").focus();
			}			
		}
		else{
		$("#your_qty").html("Please Enter Your Quantity");
		$("#txtQty").focus();
		}
	});
	
	function refresh()
	{
        window.location.href="<?=base_url('buyer/live_tenders/live/').$this->uri->segment(4);?>"
	}
function validatetenderbid(obj)
{
	if(!isFinite($(obj).val())){
		$(obj).val("");
	}else{
		var final_amount = 0;
		final_amount = parseFloat($(obj).val())*parseFloat(<?=(float)$lot_details['lot_qty']; ?>)/parseFloat(<?=(float)$lot_details['min_bid']; ?>)
		$("#txtMyTotalBidAmount").val(final_amount);
	}
}
</script>