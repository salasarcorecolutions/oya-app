<?php
defined('BASEPATH') or exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">

	<!-- Fav and touch icons -->
	<link rel="apple-touch-icon-precomposed" sizes="144x144" href="<?php echo base_url(ciHtmlTheme . '/assets/ico/apple-touch-icon-144.png') ?>">
	<link rel="apple-touch-icon-precomposed" sizes="114x114" href="<?php echo base_url(ciHtmlTheme . '/assets/ico/apple-touch-icon-114.png') ?>">
	<link rel="apple-touch-icon-precomposed" sizes="72x72" href="<?php echo base_url(ciHtmlTheme . '/assets/ico/apple-touch-icon-72.png') ?>">
	<link rel="apple-touch-icon-precomposed" sizes="57x57" href="<?php echo base_url(ciHtmlTheme . '/assets/ico/apple-touch-icon-57.png') ?>">
	<link rel="shortcut icon" href="<?php echo base_url(ciHtmlTheme . '/assets/ico/favicon.png') ?>">
	<title>ACCEPT TERMS AND CONDITIONS - <?php echo @$saleno ?></title>
	<!-- Bootstrap core CSS -->
	<link href="<?php echo base_url(ciHtmlTheme . '/assets/bootstrap/css/bootstrap.min.css') ?>" rel="stylesheet">
	<!-- Custom styles for this template -->
	<link href="<?php echo base_url(ciHtmlTheme . '/assets/css/style.css') ?>" rel="stylesheet">
	<link href="<?php echo base_url(ciHtmlTheme . '/assets/css/custom.css') ?>" rel="stylesheet">
	<script>
		paceOptions = {
			elements: true
		};
	</script>
	<link href="<?php echo base_url('assets/css/bootstrap.min.css'); ?>" rel="stylesheet">
	<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/font.css" type="text/css" cache="false" />

	<link href="<?php echo base_url('assets/plugins/font-awesome/css/font-awesome.min.css'); ?>" rel="stylesheet">
	<link href="<?php echo base_url(ciHtmlTheme . '/assets/css/style.css') ?>" rel="stylesheet">
	<link href="<?php echo base_url(ciHtmlTheme . '/assets/css/custom.css') ?>" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>common_plugin/countdown/jquery.countdown.css">
	<!-- Latest compiled and minified JavaScript -->

	<script src="<?php echo base_url(); ?>assets/js/jquery-2.1.1.min.js"></script> <!-- Common functions related to auction-->
	<script src="http://malsup.github.com/jquery.form.js"></script>
	<script src="<?php echo base_url(); ?>assets/plugins/sky-forms/version-2.0.1/js/jquery.validate.min.js"></script>

	<script src="<?php echo base_url(); ?>assets/plugins/sky-forms/version-2.0.1/js/jquery-ui.min.js"></script>
	<script src="<?php echo base_url(); ?>assets/js/bootbox.js"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>assets/plugins/bootstrap/js/bootstrap.min.js"></script>


	<script src="<?php echo base_url(); ?>assets/js/jquery.alphanumeric.pack.js"></script>
	<!--Bootstrap Stylesheet [ REQUIRED ]-->



	<script src="<?php echo base_url(ciHtmlTheme . '/assets/js/pace.min.js') ?>"></script>

	<script src="<?php echo base_url(); ?>assets/js/common.js"></script> <!-- Common functions related to auction-->
	<script src="<?php echo base_url(); ?>assets/js/tender_default.js"></script> <!-- Common functions related to auction-->
	<!-- moment.js for live date time -->
	<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.18.1/moment.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/moment-timezone/0.5.28/moment-timezone-with-data.min.js"></script>

</head>

<body>
	<div id="wrapper">
		<!-- /.header -->
		<?php $this->load->view(ciHtmlTheme . '/buyer/buyer_header'); ?>
		<!-- /.header -->
		<div class="main-container">
			<div class="container-fluid">
				<div class="row">


					<style>
						.panel .table td,
						.panel .table th {
							font-size: 13px;
							padding: 5px !important;
						}

						.table>tbody>tr>td,
						.table>tbody>tr>th,
						.table>tfoot>tr>td,
						.table>tfoot>tr>th,
						.table>thead>tr>td,
						.table>thead>tr>th {
							font-size: 12px;
						}

						p {
							margin-bottom: 0;
						}

						.btn {
							border-radius: 5px;
						}

						.bidnow {
							text-align: center;
						}

						.btn-div {
							text-align: center;
							margin: auto;
						}

						.wrapper {
							border-top: 1px solid #ddd;
							padding: 5px;
						}

						.wrapper ul {
							margin: 0;
							padding: 0;
							/* For IE, the outcast */
							zoom: 1;
							*display: inline;
						}

						#buttons li {
							float: left;
							list-style: none;
							text-align: center;
							width: auto;
							margin-right: 5px;

						}

						#buttons li a {
							text-decoration: none;
							color: #FFFFFF;
							display: block;
							font-size: 14px;
							border-radius: 5px;
						}

						.ClassyCountdown-value h5 {
							margin: 0;
						}

						#buttons li a:hover {
							text-decoration: none;
						}

						.auction-details {
							padding-left: 10px;
							padding-right: 10px;
						}

						.auction-details .details-col {
							border: 1px solid #ddd;
							padding: 5px;
						}

						.auction-details .details-col .details-title {
							border-right: 2px solid #909090;
						}

						.auction-details .details-col .details-title p {
							font-weight: 900;
							font-size: 14px;
						}

						.panel-heading {
							height: auto;
						}

						@media(max-width:990px) {
							#buttons li {
								margin-top: 5px;
								text-align: left;
							}

							#buttons li a {
								width: 150px;
							}

							.btn {
								text-align: left;
							}

							.panel .table td,
							.panel .table th {
								padding: 0;
							}

							.panel .table td {
								padding: 0 !important;
							}
						}

						@keyframes blinker {
							5% {
								opacity: 0;
							}
						}

						.select2-container-multi .select2-choices {
							min-height: 120px;
						}

						#mainnav-container {
							display: none;
						}

						#footer {
							display: none !important;
						}

						#loading {
							display: none !important;
						}

						#ajaxModal .modal-dialog {
							width: 50%;
						}

						.span_aucTimeleft,
						.aucClosed,
						.span_aucTimeleft>div {
							font-size: 15px;
							font-weight: 600;
							color: red;
							text-align: center;
						}

						.select2-container {
							padding: 0 !important;
						}

						.panel {
							margin-bottom: 5px;
							background-color: #fff;
							border: 1px solid transparent;
							border-radius: 4px;
							-webkit-box-shadow: 0 1px 1px rgba(0, 0, 0, .05);
							box-shadow: 0 1px 1px rgba(0, 0, 0, .05);
						}
					</style>
					<?php $tid = $tender_id; ?>
					<script type="text/javascript">
						var lot_close_url = "<?php echo base_url(); ?>buyer/live_tenders/is_closed/" + '<?php echo $this->uri->segment(4); ?>';
						$(document).ready(function() {
							<?php
							foreach ($Runninglots as $Lots) {
							?>
								$("#timeleft_<?= $Lots['lotno']; ?>").countdown({
									padZeroes: true,
									until: +secondConvert("<?= $Lots['TOTTIME']; ?>"),
									onTick: function() {
										highlightLot(this);
									},
									onExpiry: function() {
										CloseLot(this);
									}
								});

							<?php
							}
							?>
							$(".btnBid").each(function() {
								$(this).click(function() {
									var data_id = $(this).attr("data-id");
									$.ajax({
										url: "<?= base_url(); ?>buyer/live_tenders/bid_page/<?= $this->uri->segment(4); ?>/" + data_id,
										success: function(data) {
											$("#modal_content").html(data);
											btnBidClicked(data_id);
										}
									});

								});
							});
						});
					</script>
					<section id="content">
						<section class="vbox">
							<section class="scrollable padder">
								<div class="row">
									<div class="col-md-12 col-xs-12">
										<div class="panel panel-default">
											<marquee>
												<p class="text-primary"><?= $auc_msg; ?></p>
											</marquee>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-lg-12 col-xs-12">
										<div class="wrapper">
											<div class="row">
												<div class="col-lg-8 col-xs-12">
													<ul id="buttons">
														<li><a class="btn btn-info btn-labeled fa fa-list" href="javascript:void(0);" data-target="#TaxDetails" onclick="tax_detail()">Tax Details</a></li>
														<li><a class="btn btn-danger btn-labeled fa fa-times" href="javascript:void(0);" data-target="#ClosedLot" onclick="closed_lot()">Closed Lots</a></li>
													</ul>
												</div>
												<div class="col-md-4 col-xs-12">
													<p class="pull-right">Press the <a class="btn btn-primary btn-labeled fa fa-refresh" id="BtnRefresh">Refresh</a> button for current updates.</p>
												</div>
											</div>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-md-12 col-xs-12">
										<div class="wrapper">
											<div class="row">
												<div class="col-md-3 col-xs-12">
													<span id="online_status" class="btn btn-default btn-xs" style="position: fixed;top: 20%;left: 37%;font-size: 20px;color: white;display: none;z-index: 99999;background-color: red;">Please check your internet connection !!!</span>
													<span id="server_time" class="btn btn-danger btn-small"></span>
												</div>
											</div>
										</div>
									</div>
								</div>
								<script>
									document.onreadystatechange = function() {
										if (navigator.onLine) {
											$("#online_status").hide();
										} else {
											$("#online_status").show();

										}
										window.addEventListener("offline", function(e) {
											$("#online_status").show();

										});
										window.addEventListener("online", function(e) {
											$("#online_status").hide();

										});
									};
									$(document).ready(function() {
										var interval = setInterval(function() {
											var momentNow = moment().tz(<?php echo "'" . $_SESSION['user_tz'] . "'"; ?>).format("DD-MM-YYYY hh:mm:ss A");
											document.getElementById("server_time").innerHTML = momentNow;
										}, 1000);
										checksession();
										setInterval(function() {
											checksession();
										}, 60000);
									});
								</script>
								<div class="row">
									<div class="col-md-12 col-xs-12">
										<div class="wrapper">
											<div class="col-md-4 col-xs-12">
												<p><strong>E-Tender No : </strong><?= $tender_details['tender_id']; ?></p>
											</div>
											<div class="col-md-4 col-xs-12">
												<p><strong>Vendor : </strong><?= $tender_details['vendor_name']; ?></p>
											</div>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-md-12 col-xs-12">
										<div class="wrapper" style="border:none;">
											<section class="panel panel-default">
												<div class="panel-heading">
													<h4 style="color: #ff0000c7;margin: 0">Ongoing Lots:</h4>
												</div>
												<div class="table-responsive">
													<table class="table basictable" id="lot_table">
														<thead>
															<tr>
																<th width="1%"># </th>
																<th width="6%">Lot No. </th>
																<th width="10%">Plant</th>
																<th width="15%">Product</th>
																<th width="10%">Time</th>
																<th width="6%">Tot Qty.</th>
																<th width="7%">Start Bid</th>
																<th width="6%">Min.Incr.</th>
																<th width="6%">Max of You</th>
																<th width="6%">Currency</th>
																<th width="6%">Time Left</th>
																<th width="3%">Bid</th>
															</tr>
														</thead>
														<tbody id="lot_list">
															<?php
															if (count($Runninglots) <= 0) {
															?>
																<tr>
																	<td colspan="14" class='text-center alert-danger'>No Running Lots</td>
																</tr>
															<?php
															}
															foreach ($Runninglots as $Lots) {

																echo "<tr id=lot_" . $Lots['lotno'] . " data_id=" . $Lots['lotno'] . ">
											<td><input type='radio' name='lotno' value=" . $Lots['lotno'] . "></td>
												<td>(" . $Lots['lotno'] . ")" . $Lots['vendor_ltno'] . "</td>
											<td>" . $Lots['plant'] . "</td>
											<td>" . $Lots['description'] . "</td>
											<td>" . $Lots['lot_open_date'] . " <br>TO<br> " . $Lots['lot_closing_date'] . "</td>
											<td>" . $Lots['lot_qty'] . " / " . $Lots['unit'] . "</td>
											<td><span id='Sbid" . $Lots['lotno'] . "'>" . $Lots['start_bid'] . "</span></td>
											<td><span id='Incr" . $Lots['lotno'] . "'>" . $Lots['minimum_incr_decr'] . "</span></td>";
																echo "<td id='MY_" . $Lots['lotno'] . "'><span id='MYAmt" . $Lots['lotno'] . "'>" . $Lots['my_max_nim_amt'] . "</span></td>

											<!--<td><span id='Position" . $Lots['lotno'] . "'>" . $Lots['indication'] . "</span></td>
											<td id='h1_" . $Lots['lotno'] . "'><span id='H1Amt" . $Lots['lotno'] . "'>" . $Lots['max_bid'] . "</span></td>-->
											<td>" . $Lots[$Lots['lotno']]['currency'] . "</td>
											<td id=timeleft_" . $Lots['lotno'] . "></td>
											<td class='bidnow' id=bidnow" . $Lots['lotno'] . ">
											<a  data-id=" . $Lots['lotno'] . " class='btnBid btn btn-sm btn-info' data-toggle='modal' data-target='#modal'>Bid</a>
											</td></tr>";
															}
															?>
														</tbody>
													</table>
												</div>
											</section>
										</div>
									</div>
								</div>

								<!-- Modal -->
								<div class="modal fade" id="modal">
									<div class="modal-dialog">
										<div class="modal-content" id="modal_content">
										</div><!-- /.modal-content -->

									</div>
									<!-- /.modal-dialog -->
								</div>


							</section>
						</section>

						<!-- Modal For Closed Lot-->
						<div class="modal fade" id="ClosedLot" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true">
							<div class="modal-dialog">
								<div class="modal-content">
									<div class="modal-header">
										<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
										<h4 class="modal-title text-info"> Closed Lots for Tender <span id="closed_tender_id"></span></h4>
									</div>
									<div class="modal-body">
										<table class='table'>
											<tr>
												<th>Lot No.</th>
												<th>Product</th>
												<th>Plant</th>
												<th>Tot Qty.</th>
												<th>Time</th>
												<th>Starting Bid</th>
												<th>H1 value</th>
											</tr>
											<tbody id="closed_lot_details">
											</tbody>
										</table>
									</div>
									<div class="modal-footer">
										<button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
									</div>
								</div>
							</div>
						</div>
						<!-- Modal -->

						<!-- Modal For Tax Details -->
						<div class="modal fade" id="TaxDetails" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true">
							<div class="modal-dialog">
								<div class="modal-content">
									<div class="modal-header">
										<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
										<h4 class="m-t-none m-b text-mute text-info"> Tax Details for Auction <?= $this->uri->segment(4); ?> and Lot <span id="tax_lotno_header"></span> </h4>
									</div>
									<table class="table " id="tax_lotno">
									</table>
									<div class="modal-footer">
										<button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
									</div>
								</div>
							</div>
						</div>
						<!-- Modal -->

				</div>
				<!--/.container-->
			</div>
			<!-- /.main-container -->
			<!-- /.footer -->

			<?php $this->load->view(ciHtmlTheme . '/includes/footer'); ?>
		</div>
		<!-- /.wrapper -->
	</div>
	<!-- Le javascript
================================================== -->
	<!-- Placed at the end of the document so the pages load faster -->
	<script src="<?php echo base_url(ciHtmlTheme . '/assets/js/jquery.min.js') ?>"> </script>
	<script src="<?php echo base_url(ciHtmlTheme . '/assets/bootstrap/js/bootstrap.min.js') ?>"></script>
	<!-- include equal height plugin  -->
	<script src="<?php echo base_url(ciHtmlTheme . '/assets/js/jquery.matchHeight-min.js') ?>"></script>
	<!-- include jquery list shorting plugin plugin  -->
	<script src="<?php echo base_url(ciHtmlTheme . '/assets/js/hideMaxListItem.js') ?>"></script>
	<!-- include jquery.fs plugin for custom scroller and selecter  -->
	<script src="<?php echo base_url(ciHtmlTheme . '/assets/plugins/jquery.fs.scroller/jquery.fs.scroller.js') ?>"></script>
	<script src="<?php echo base_url(ciHtmlTheme . '/assets/plugins/jquery.fs.selecter/jquery.fs.selecter.js') ?>"></script>
	<script src="<?php echo base_url(ciHtmlTheme . '/assets/js/jquery.validate.min.js') ?>"></script>
	<!-- include custom script for site  -->
	<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery.plugin.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery.countdown.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/detect_browser.js"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/html2canvas.js"></script>

	<!---------------------DataTables JS-------------------------------------------------->
	<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/datatables/jquery.dataTables.js"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/datatables/manual_datatable.js"></script>
	<script type="text/javascript" src="<?= base_url(); ?>assets/js/jquery.prettyPhoto.js"></script>
	<script src="https://cdn.jsdelivr.net/npm/select2@4.0.13/dist/js/select2.min.js"></script>

	<script>
		function tax_detail() {
			if ($('input[name=lotno]:checked').length <= 0) {
				alert("Please select lot to see tax details");
				return false;
			} else {
				id = $("input[name=lotno]:checked").val();
				$("#tax_lotno_header").html(id);
				$.ajax({
					url: '<?php echo base_url(); ?>buyer/live_tenders/tender_tax_details',
					data: {
						"lotid": id,
						"tid": '<?= $this->uri->segment(4); ?>'
					},
					type: "post",
					dataType: 'json',
					success: function(resObj) {
						var a = "";
						if (resObj.tax != "" && ((resObj.tax.ED != null && resObj.tax.ED != "") || (resObj.tax.VAT != null && resObj.tax.VAT != "") || (resObj.tax.TCS != null && resObj.tax.TCS != "") || (resObj.tax.GST != null && resObj.tax.GST != ""))) {

							a += "<tr><th>Tax Type</th><th>Tax Percentage</th></tr>";
							if (resObj.tax.ED != null && resObj.tax.ED != "") {
								a += "<tr><td>Excise Duty</td><td>" + resObj.tax.ED + "</td></tr>";
							}
							if (resObj.tax.VAT != null && resObj.tax.VAT != "") {
								a += "<tr><td>VAT</td><td>" + resObj.tax.VAT + "</td></tr>";
							}
							if (resObj.tax.CST != null && resObj.tax.CST != "") {
								a += "<tr><td>CST</td><td>" + resObj.tax.CST + "</td></tr>";
							}
							if (resObj.tax.TCS != null && resObj.tax.TCS != "") {
								a += "<tr><td>TCS</td><td>" + resObj.tax.TCS + "</td></tr>";
							}
							if (resObj.tax.GST != null && resObj.tax.GST != "") {
								a += "<tr><td>GST</td><td>" + resObj.tax.GST + "</td></tr>";
							}
						} else {
							a += "<tr><td colspan='16' class='text-center alert-danger'>No Tax Details</td></tr>";
						}
						$("#tax_lotno").html(a);
						$("#TaxDetails").modal('show');
					}
				});
			}
		}

		function closed_lot() {
			$.ajax({
				type: "POST",
				url: "<?php echo base_url(); ?>buyer/live_tenders/closed_lot",
				data: {
					auctionid: '<?= $this->uri->segment(4); ?>'
				},
				dataType: "json",
				success: function(data) {
					var html = "";
					if (data) {
						$("#closed_tender_id").html(data.tender_id);
						if (data.lot.count == 0) {
							html += "<tr><td colspan='7' class='text-center text-danger'>No Lots Closed</td></tr>";
						} else {
							for (var i = 0; i < data.lot.count; i++) {
								html += "<tr><td>" + data.closed_data[i].lotno + "</td><td>" + data.closed_data[i].description + "</td><td>" + data.closed_data[i].plant + "</td><td>" + data.closed_data[i].lot_qty + "</td><td>S:" + data.closed_data[i].lot_open_date + "<br>E:" + data.closed_data[i].lot_closing_date + "</td><td>" + data.closed_data[i].start_bid + "</td>";
								if (data.h1_amount[i].lotid == data.closed_data[i].lotno) {
									html += "<td>" + data.h1_amount[i].amount + "</td>";
								} else {
									html += "<td class='text-danger'>No Bid</td>";
								}
								html += "</tr>";
							}
						}
					} else {
						html += "<tr><td colspan='7' class='text-center text-danger'>You Are Not Authorized to See Closed Lots.</td></tr>";
					}
					$("#closed_lot_details").html(html);
					$('#ClosedLot').modal('show');
				}
			});
		}

		function checksession() {
			jQuery.ajax({
				url: '<?php echo base_url(); ?>buyer/etenders/log_user_session',
				type: 'post',
				dataType: 'json',
				data: {
					'data': user_browser_info
				},
				success: function(res) {}
			});
		}

		// Basic Table Function Init
		// $('#lot_table').basictable();
	</script>