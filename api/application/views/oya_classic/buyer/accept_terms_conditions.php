<?php
defined('BASEPATH') or exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<!-- Fav and touch icons -->
<link rel="apple-touch-icon-precomposed" sizes="144x144"
	href="<?php echo base_url(ciHtmlTheme.'/assets/ico/apple-touch-icon-144.png')?>">
<link rel="apple-touch-icon-precomposed" sizes="114x114"
	href="<?php echo base_url(ciHtmlTheme.'/assets/ico/apple-touch-icon-114.png')?>">
<link rel="apple-touch-icon-precomposed" sizes="72x72"
	href="<?php echo base_url(ciHtmlTheme.'/assets/ico/apple-touch-icon-72.png')?>">
<link rel="apple-touch-icon-precomposed" sizes="57x57"
	href="<?php echo base_url(ciHtmlTheme.'/assets/ico/apple-touch-icon-57.png')?>">
<link rel="shortcut icon"
	href="<?php echo base_url(ciHtmlTheme.'/assets/ico/favicon.png')?>">
<title>ACCEPT TERMS AND CONDITIONS - <?php echo @$saleno?></title>
<!-- Bootstrap core CSS -->
<link
	href="<?php echo base_url(ciHtmlTheme.'/assets/bootstrap/css/bootstrap.min.css')?>"
	rel="stylesheet">
<!-- Custom styles for this template -->
<link href="<?php echo base_url(ciHtmlTheme.'/assets/css/style.css')?>"
	rel="stylesheet">
<link href="<?php echo base_url(ciHtmlTheme.'/assets/css/custom.css')?>"
	rel="stylesheet">


<script>
    paceOptions = {
      elements: true
    };
</script>
<script
	src="<?php echo base_url(ciHtmlTheme.'/assets/js/pace.min.js')?>"></script>
</head>
<body>

	<div id="wrapper">
		<!-- /.header -->
 <?php $this->load->view(ciHtmlTheme.'/includes/header');?>
  <!-- /.header -->
		<div class="main-container">
			<div class="container-fluid">
				<div class="row">
					<div class="col-sm-3 page-sidebar">
         <?php $this->load->view(ciHtmlTheme.'/buyer/left_navigation');?>
        </div>
					<!--/.page-sidebar-->

					<div class="col-sm-9 page-content">
						<div class="inner-box">
							<h2 class="title-2">
								<i class="icon-heart-1"></i> DETAILS OF SALE NO: <?php echo $saleno?></h2>
							<div class="row">
								<div class="col-md-8">
									<div class="box">
										<div class="box-header">
											<h5 class="box-title">DETAILS</h5>

											
										</div>
										<!-- /.box-header -->
										<div class="box-body no-padding">
										<table class="table table-responsive table-bordered">
										<tr><td class="text-right">Sale No :</td><td><?php echo $saleno; ?></td></tr>
										<tr><td class="text-right">Vendor :</td><td><?php echo $auccomp; ?></td></tr>
										<tr><td class="text-right">Auction Type :</td><td><?php echo $auctype; ?></td></tr>
										<tr><td class="text-right">Start Date :</td><td><?php echo $start_date; ?></td></tr>
										<tr><td class="text-right">End Date :</td><td><?php echo $end_date; ?></td></tr>
										<tr><td class="text-right">Particulars :</td><td><?php echo $aucpartcular; ?></td></tr>
										<tr><td class="text-right">Currency :</td><td><?php echo $auction_currency; ?></td></tr>
										<tr><td class="text-right">Total Lots :</td><td><?php echo $nooflots; ?></td></tr>
										
										</table>
										
										
										
										</div>
									</div>
								</div>
								<div class="col-md-4">
								<form name="frmAcceptTerms" id="frmAcceptTerms" method="POST">
									<div class="box">
										<div class="box-header">
											<h5 class="box-title">DOCUMENTS</h5>
											</div>
											<div class="box-body">
											<table class="table table-bordered">
												<thead>
													<tr>

														<th></th>
														<th>Document Name</th>
														<th>View</th>
													</tr>
												</thead>
												<tbody>

													<?php if ( ! empty($common_documets)){
														$this->load->library('Amazon_library/S3upload');
														$s3 = new S3upload();
														foreach ($common_documets as $cd){
															$file = $s3->checkFile($cd['file_name'], 'common_documents');
															echo '<tr><td></td>'
															.'<td>'.$cd['document_name'].'</td>'
															.'<td>';
															if ( ! empty($file[0])){
																echo '<a class="text-success" href="'.$file[0].'" target="_new">Click Here</a>';
															}
															echo '</td>';
														}
													} ?>
												<?php foreach($docs as $doc_items) { ?>
													<tr>

													<td><input type="checkbox" name="terms_accept[]" /></td>
													<td><?php echo $doc_items['doc_name']?></td>
													<td><a href="<?php echo $doc_items['doc_path']?>" target="_new"><i class="fa fa-link"></i> View</a></td>
													</tr>
													<?php }?>
													
													<tr> 
													<tr>
													<td colspan="2" class="text-right" style="middle"> Your Bid Currency</td>
													<td > <select name="bid_currency" class="form-control">
                                                              <option value="<?php echo $primary_currency?>"><?php echo $auction_currency?></option>
                                                              <?php foreach($other_currency as $cItems)?>
                                                              <option value="<?php echo $cItems['currency_id']?>"><?php echo $cItems['currency_name']?>(<?php echo $cItems['currency_symbol']?>)</option>
                                                              </select>
                                                     </td>
												</tbody>
												<tfoot>
													
												
												</tfoot>
											</table>
											
											
										</div>
										
										<div class="box-footer text-center">
											<button name="btnAcceptTerms" id="btnAcceptTerms" class="btn btn-primary  btn-block"> ACCEPT TERMS AND CONDITIONS</button>
											<span id="msg"></span>
										</div>
										<!-- /.box-body -->
									</div>
									</form>
								</div>
							</div>
							
						</div>
						<!--/.page-content-->
					</div>
					<!--/.row-->
				</div>
				<!--/.container-->
			</div>
			<!-- /.main-container -->


			<!-- /.footer --> 
  <?php $this->load->view(ciHtmlTheme.'/includes/footer');?>
 
</div>
		<!-- /.wrapper -->
	</div>
	<!-- Le javascript
================================================== -->
	<!-- Placed at the end of the document so the pages load faster -->
	<script
		src="<?php echo base_url(ciHtmlTheme.'/assets/js/jquery.min.js')?>"> </script>
	<script
		src="<?php echo base_url(ciHtmlTheme.'/assets/bootstrap/js/bootstrap.min.js')?>"></script>


	<!-- include equal height plugin  -->
	<script
		src="<?php echo base_url(ciHtmlTheme.'/assets/js/jquery.matchHeight-min.js')?>"></script>
	<!-- include jquery list shorting plugin plugin  -->
	<script
		src="<?php echo base_url(ciHtmlTheme.'/assets/js/hideMaxListItem.js')?>"></script>
	<!-- include jquery.fs plugin for custom scroller and selecter  -->
	<script
		src="<?php echo base_url(ciHtmlTheme.'/assets/plugins/jquery.fs.scroller/jquery.fs.scroller.js')?>"></script>
	<script
		src="<?php echo base_url(ciHtmlTheme.'/assets/plugins/jquery.fs.selecter/jquery.fs.selecter.js')?>"></script>
		<script src="<?php echo base_url(ciHtmlTheme.'/assets/js/jquery.validate.min.js')?>"></script>
	<!-- include custom script for site  -->
	
	<!-- include jquery autocomplete plugin  -->
	<script>    
$("#btnAcceptTerms").click(function(e){
	 e.preventDefault();
	var form=$("#frmAcceptTerms");
	form.validate({
	    ignore:[],
		rules: {
			'terms_accept[]': {required : true},
			
			
			},
		messages: {	
			
			},
			
		});

	if(form.valid())
	 	$( "#frmAcceptTerms" ).submit();
	

	
});
$('#frmAcceptTerms').on('submit',(function(e) {
	var $btn = $('#btnAcceptTerms').button('loading');
	 e.preventDefault();
		$.ajax({
			url: '<?= base_url();?>buyer/terms_conditions/save_terms/<?php echo $auction_id;?>',
			type:"POST",
			data: new FormData(this),
			contentType: false,
			cache: false,
			processData:false,
			dataType: 'json',
			success: function (res) {
				if (res.status) 
				{
					window.location = "<?php echo base_url('buyer/live_auctions/live/'.$auction_id)?>";
				}
				else{
					$("#msg").html('<div class="alert alert-warning alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button> <strong>Warning! '+res.message+'<i class="fa fa-warning"></i></div>');
					 $btn.button('reset')
				}	
			},
			error: function(){
				$btn.button('reset')
			}
			});
	}));		
 </script>
</body>
</html>
