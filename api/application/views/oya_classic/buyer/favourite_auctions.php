<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<!-- Fav and touch icons -->
<link rel="apple-touch-icon-precomposed" sizes="144x144" href="<?php echo base_url(ciHtmlTheme.'/assets/ico/apple-touch-icon-144.png')?>">
<link rel="apple-touch-icon-precomposed" sizes="114x114" href="<?php echo base_url(ciHtmlTheme.'/assets/ico/apple-touch-icon-114.png')?>">
<link rel="apple-touch-icon-precomposed" sizes="72x72" href="<?php echo base_url(ciHtmlTheme.'/assets/ico/apple-touch-icon-72.png')?>">
<link rel="apple-touch-icon-precomposed" sizes="57x57" href="<?php echo base_url(ciHtmlTheme.'/assets/ico/apple-touch-icon-57.png')?>">
<link rel="shortcut icon" href="<?php echo base_url(ciHtmlTheme.'/assets/ico/favicon.png')?>">
<title>My Favourite Auctions</title>
<!-- Bootstrap core CSS -->
<link href="<?php echo base_url(ciHtmlTheme.'/assets/bootstrap/css/bootstrap.min.css')?>" rel="stylesheet">
<link rel="stylesheet" href="<?php echo base_url();?>assets/plugins/datatables/dataTables.bootstrap.css">
<link rel="stylesheet" href="<?php echo base_url();?>assets/plugins/datatables/jquery.dataTables_themeroller.css">
<!-- Custom styles for this template -->
<link href="<?php echo base_url(ciHtmlTheme.'/assets/css/style.css')?>" rel="stylesheet">
<link href="<?php echo base_url(ciHtmlTheme.'/assets/css/custom.css')?>" rel="stylesheet">
<!-- styles needed for carousel slider -->
<link href="<?php echo base_url(ciHtmlTheme.'/assets/css/buyer.css')?>" rel="stylesheet">


<script>
    paceOptions = {
      elements: true
    };
</script>
<script src="<?php echo base_url(ciHtmlTheme.'/assets/js/pace.min.js')?>"></script>

 </head>
<body>

<div id="wrapper">
<!-- /.header -->
 <?php $this->load->view(ciHtmlTheme.'/buyer/buyer_header');?>
  <!-- /.header -->
  <div class="main-container">
   <div class="container-fluid">
      <div class="row">
        <div class="col-sm-3 page-sidebar">
         <?php $this->load->view(ciHtmlTheme.'/buyer/left_navigation');?>
        </div>
        <!--/.page-sidebar-->
        
        <div class="col-sm-9 page-content">
         	<div class="inner-box">
         	 <h2 class="title-2"><i class="icon-heart-1"></i> All Favourite Auctions </h2>
         	 <span class="page-sub-header-sub small">List of all Auntions</span>
           
				<table class="table table-bordered dataTables " id="fav_table">
					<thead>
						<tr>
							
							<th>SALE NO</th>
							<th>VENDOR NAME</th>
							<th>START DATE</th>
							<th>END DATE</th>
							<th>AUCTION TYPE</th>
					
						</tr>
					</thead>
					<tbody>
                       <?php 
                            if ( ! empty($auction))
                            {
                                foreach($auction as $k=>$v)
                                {
                                    if ( ! empty($v))
                                    {
                                        foreach($v as $row)
                                        {
                                            if ($row['auction_type'] == 4)
                                            {
                                                $url  = base_url('buyer/auctions/details/'.MD5($row['ynk_auctionid']));
                                            }
                                            else
                                            {
                                                $url = base_url('tender/view_details/'.md5($row['saleno']));
                                            }
                        ?>

                                                <tr>
                                                    <td><a href=<?php echo $url?>><i class="fa fa-link"></i><?php echo $row['saleno']?></a></td>
                                                    <td><?php echo $row['vendor_name']?></td>
                                                    <td><?php echo servertz_to_usertz($row['start_date'],$this->session->userdata('user_tz'),'d-m-Y').' '.servertz_to_usertz($row['stime'],$this->session->userdata('user_tz'),'H:i:s')?></td>
                                                    <td><?php echo servertz_to_usertz($row['end_date'],$this->session->userdata('user_tz'),'d-m-Y').' '.servertz_to_usertz($row['etime'],$this->session->userdata('user_tz'),'H:i:s')?></td>
                                                    <td><?php echo $row['auctiontype']?></td>


                                                </tr>
                        <?php
                                        }
                                    }
                                }
                            }

                       ?>
					</tbody>
				</table>
			
        </div>
        <!--/.page-content--> 
      </div>
      <!--/.row--> 
    </div>
    <!--/.container--> 
  </div>
  <!-- /.main-container -->
  </div>
  
  <!-- /.footer --> 
  <?php $this->load->view(ciHtmlTheme.'/includes/footer');?>
 
</div>
<!-- /.wrapper --> 

<!-- Le javascript
================================================== --> 
<!-- Placed at the end of the document so the pages load faster --> 
<script src="<?php echo base_url(ciHtmlTheme.'/assets/js/jquery.min.js')?>"> </script>
<script src="<?php echo base_url(ciHtmlTheme.'/assets/bootstrap/js/bootstrap.min.js')?>"></script> 
<!-- include equal height plugin  --> 
<script src="<?php echo base_url(ciHtmlTheme.'/assets/js/jquery.matchHeight-min.js')?>"></script> 
<!-- include jquery list shorting plugin plugin  --> 
<script src="<?php echo base_url(ciHtmlTheme.'/assets/js/hideMaxListItem.js')?>"></script> 
<!-- include jquery.fs plugin for custom scroller and selecter  --> 
<script src="<?php echo base_url(ciHtmlTheme.'/assets/plugins/jquery.fs.scroller/jquery.fs.scroller.js')?>"></script>
<script src="<?php echo base_url(ciHtmlTheme.'/assets/plugins/jquery.fs.selecter/jquery.fs.selecter.js')?>"></script>
<script src="<?php echo base_url();?>assets/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="<?php echo base_url();?>assets/plugins/datatables/dataTables.bootstrap.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/manual_datatable.js"></script>
<!-- include custom script for site  --> 
<script src="<?php echo base_url(ciHtmlTheme.'/assets/js/script.js')?>"></script>
<!-- include jquery autocomplete plugin  -->
<script>
    $("#fav_table").dataTable();
</script>
</body>
</html>
