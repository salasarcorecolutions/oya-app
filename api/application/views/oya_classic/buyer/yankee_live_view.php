<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<!-- Fav and touch icons -->
<link rel="apple-touch-icon-precomposed" sizes="144x144" href="<?php echo base_url(ciHtmlTheme.'/assets/ico/apple-touch-icon-144.png')?>">
<link rel="apple-touch-icon-precomposed" sizes="114x114" href="<?php echo base_url(ciHtmlTheme.'/assets/ico/apple-touch-icon-114.png')?>">
<link rel="apple-touch-icon-precomposed" sizes="72x72" href="<?php echo base_url(ciHtmlTheme.'/assets/ico/apple-touch-icon-72.png')?>">
<link rel="apple-touch-icon-precomposed" sizes="57x57" href="<?php echo base_url(ciHtmlTheme.'/assets/ico/apple-touch-icon-57.png')?>">
<link rel="shortcut icon" href="<?php echo base_url(ciHtmlTheme.'/assets/ico/favicon.png')?>">
<title>LIVE AUCTION NO: <?php echo $auction_details['ynk_saleno']; ?> | <?php echo $auction_details['vendor_name']; ?> </title>
<!-- Bootstrap core CSS -->
<link href="<?php echo base_url(ciHtmlTheme.'/assets/bootstrap/css/bootstrap.min.css')?>" rel="stylesheet">
<!-- Custom styles for this template -->
<link href="<?php echo base_url(ciHtmlTheme.'/assets/css/style.css')?>" rel="stylesheet">
<link href="<?php echo base_url(ciHtmlTheme.'/assets/css/custom.css')?>" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>common_plugin/countdown/jquery.countdown.css"> 
<style type="text/css">
#newsCont {line-height:20px;margin:5px 0px;border:1px dashed #A81213;background:url("images/highlight.png");box-shadow:0 1px 3px rgba(0, 0, 0, 0.1);text-shadow:0 1px 0 rgba(255, 255, 255, 0.55)}
#news {line-height: 20px;}
.tickerHead{background: #A81213;width:80px;line-height: 20px;overflow: hidden;margin: 0;	padding: 3px;font-weight:bold;color:#fff;float:left}
.ticker {width:500px;height: 20px;overflow: hidden;margin: 0;	padding: 0;	list-style: none;float:left}
.bg_green{background-color: #58D68D}
#lot_list .highlight {    background-color: red !important;color:#fff}
#Lot_Upcoming .highlight {    background-color: green !important;color:#fff}
</style>

<script>
    paceOptions = {
      elements: true
    };
</script>
<script src="<?php echo base_url(ciHtmlTheme.'/assets/js/pace.min.js')?>"></script>

 </head>
<body>

<div id="wrapper">
<!-- /.header -->
 <?php $this->load->view(ciHtmlTheme.'/buyer/buyer_header');?>
  <!-- /.header -->
  <div class="main-container">
   <div class="container-fluid">
      <div class="row">
        <div class="col-sm-12 page-content">
         	<div class="inner-box">
         	 <h2 class="title-2"><i class="icon-hammer"></i> LIVE AUCTION NO: <?php echo $auction_details['ynk_saleno']; ?>  <span id="server_time" class="btn btn-danger btn-small pull-right"><?php echo $this->session->userdata('compname');?></span></h2>
         	  <?php //print_r($lot_settings)?>
         	<!-- MESSAGES -->
			<?php
				$ids = explode(',', liveAucQuantityAmount);
				$update_qty_amt = 0;
				foreach ($ids as $dd){
					if ($dd == $vendor_id)
					{
						$update_qty_amt = 1;
						break;
					}
				}
			?>
					<div class="col-lg-12">
						<strong>Vendor: </strong><?php echo $auction_details['vendor_name']; ?> | <strong>Auction Type:</strong> <?php echo $auction_details['auctiontype']; ?> 
						<span class="pull-right">
						<button data-target="#QuantityDistribution" class="btn btn-warning btn-xs" id="QDistribution"><i class="icon-hammer"></i> Quantity Distribution</button>
						<button data-target="#BidLimit" id="BidLimit" class="btn btn-warning btn-xs"><i class="icon-hammer"></i> Bid Limit</button>
						<button data-target="#ClosedLot" id="ClosedLot" class="btn btn-success btn-xs" ><i class="icon-hammer"></i> Closed Lots</button>
						<a  href="<?php echo base_url();?>buyer/proxy/proxy_details/<?php echo $live_auction_id;?>" class="btn btn-info btn-xs" ><i class="icon-hammer"></i> Add Proxy</a> 								
						<button data-target="#TaxDetails" id="TaxDetails" class="btn btn-default btn-xs" ><i class="icon-hammer"></i> Tax Details</button>
						<button id="BtnRefresh" class="btn btn-danger btn-xs"><i class="fa fa-repeat"></i> Refresh</button>
						</span>
					</div>
					  <div class="row messages">
    					<div class="col-lg-12">
        					<div id="newsCont" class="clearfix">
            				<div class="tickerHead">MESSAGES</div>
                				<div class='col-lg-11'>
                					<marquee scrollamount=3 class='text-info'>
                						<div id="news" class="ticker" style="font-size: 15px"><?php echo $auc_msg; ?></div>
                					</marquee>
                				</div>
            				</div>
    						 
    					</div>
					</div>
					
					<div class="table-responsive">
						<table class="table table-bordered b-t b-light text-sm table-striped" id="lot_table"> 
							<thead>                                                  
								<tr>  
									<th>#</th> 
									<th><?php echo $lot_settings['ltno']==""?'LOT NO':$lot_settings['ltno']; ?> </th> 
									
									<th class="<?= (@$lot_settings['h_product']=="Y" && @$lot_settings['h_product_image']=="Y")?"hide":"show";?>">
										<?php echo @$lot_settings['product']==""?'PRODUCT':$lot_settings['product']; ?>
									</th>
									<th>DETAILS</th>
									<th>BID STATUS</th>
									<?php
									
									if ( $auction_details['allowed_max_extension'] != 0)
									{
										?><th>Extension</th><?php
									}
									?>
									<th style="width:150px">TIME LEFT</th>
									<th>BID NOW</th>
								</tr>
							</thead>
							<tbody id="lot_list">
							<?php
							for ($i=0;$i<count($live_lot);$i++)
							{
							?>
								<tr id="lot_<?php echo $live_lot[$i]['ynk_lotno'];?>" data_id="<?php echo $live_lot[$i]['ynk_lotno'];?>">
									<td><input type="radio" name="lotno" value="<?php echo $live_lot[$i]['ynk_lotno'];?>"></td>
									
									<td>
										<?php
										if ($live_lot[$i]['ynk_lotno'] != $live_lot[$i]['vendorlotno'] && $live_lot[$i]['vendorlotno'] != ''){
											?><?php echo $live_lot[$i]['ynk_lotno'];?><hr style="margin: 5px 0 5px 0;">[<?php echo $live_lot[$i]['vendorlotno'];?>]<?php
										} else {
											?><?php echo $live_lot[$i]['ynk_lotno'];?><?php
										}
										?>
									</td>
									
									<td class="<?= (@$lot_settings['h_product']=="Y" && @$lot_settings['h_product_image']=="Y")?"hide":"show";?>">
									<?php if(@$lot_settings['h_product_image']!="Y"){?>
										<div id="IMG<?php echo $live_lot[$i]['ynk_lotno'];?>">
										<?php 
										foreach($live_lot[$i]['imgs'] as $imgs) {
										      echo '<a href="'.$imgs['url'].'" target="_new" ><img src="'.$imgs['url'].'" class="img-responsive img-thumb" style="max-width:120px"/></a>';
										    
										}
										}
										?>
										
										</div>
										
										<span class="<?php echo @$lot_settings['h_product']=='Y'?'hide':'show';?>" title="<?php echo $live_lot[$i]['product_details'];?>"><?php echo nl2br($live_lot[$i]['product']);?></span>
										<span class="<?php echo @$lot_settings['h_vltno']=='Y'?'hide':'show';?>" title="<?php echo $live_lot[$i]['vendorlotno'];?>">VENDOR LOT:<?php echo $live_lot[$i]['vendorlotno'];?></span>
										
									</td>
									
									
									<td nowrap>
    									<p class="<?= @$lot_settings['h_plant']=="Y"?"hide":"show";?>">
    										<?php echo $lot_settings['plant']==""?'PLANT':$lot_settings['plant']; ?>: <?php echo $live_lot[$i]['plant'];?>
    									</p>
    									<p class="<?= @$lot_settings['h_tq']=="Y"?"hide":"show";?>" >
    										<?php echo $lot_settings['tq']==""?'QTY':$lot_settings['tq']; ?>: <?php echo (float)$live_lot[$i]['totalqty']." ".$live_lot[$i]['totunit'];?>
    									</p>
    									<p class="<?= @$lot_settings['h_bq']=="Y"?"hide":"show";?>" >
    										Min. Bid Qty: <?php echo (float)$live_lot[$i]['min_part_bid']." ".$live_lot[$i]['bidunit'];?>
    									</p>
    									<p class="<?= @$lot_settings['h_lsd']=="Y"?"hide":"show";?>"> Start: <?php echo $live_lot[$i]['lot_open_date'];?></p>
    									<p class="<?= @$lot_settings['h_led']=="Y"?"hide":"show";?>">End : <?php echo $live_lot[$i]['lot_close_date'];?> </p>
    									
    									<p class="<?= @$lot_settings['h_sb']=="Y"?"hide":"show";?>">
    										Starting Bid :<?php	$start_bid = $live_lot[$i]['startbid']/$currency['conversion_rate'];?>
    										<span id="Sbid<?php echo $live_lot[$i]['ynk_lotno'];?>" > <?php echo number_format($start_bid,$auction_details['decimal_point']).' '.$currency['bidder_prefered_currency_name'];?> </span>
    									</p>
    									<p class="<?= @$lot_settings['h_db']=="Y"?"hide":"show";?>">
    										Min.<?php echo $auction_details['bid_logic']=="F"?'Incr.':'Decr.';?>. <span id="Incr<?php echo $live_lot[$i]['ynk_lotno'];?>" > <?php echo number_format($live_lot[$i]['incrby']/$currency['conversion_rate'],$auction_details['decimal_point'], '.', '').$currency['bidder_prefered_currency_name'];?> </span>
    									</p>
    									
    									<input type="hidden" name="lot_close_date_time<?php echo $live_lot[$i]['ynk_lotno'];?>" id="lot_close_date_time<?php echo $live_lot[$i]['ynk_lotno'];?>" value="<?php echo $live_lot[$i]['lot_close_date_time'];?>"/>
									</td>
									
									<td id='h1_<?php echo $live_lot[$i]['ynk_lotno'];?>'><div id="UpdateBids_<?php echo $live_lot[$i]['ynk_lotno'];?>">Loading..</div></td>
									
									
									<?php
									if ($auction_details['allowed_max_extension'] != 0){
									    if ($auction_details['allowed_max_extension'] == $live_lot[$i]['no_of_extenstion']){
											?><td id='lot_extend_<?php echo $live_lot[$i]['ynk_lotno'];?>'>No Further<br>extension</td><?php
										}else{
										    ?><td id='lot_extend_<?php echo $live_lot[$i]['ynk_lotno'];?>'><span id='lot_extend_count_<?php echo $live_lot[$i]['ynk_lotno'];?>'><?php echo $live_lot[$i]['no_of_extenstion']?></span> / <?php echo $auction_details['allowed_max_extension']?></td><?php
										}
									}
									?>									
									<td id="timeleft_<?php echo $live_lot[$i]['ynk_lotno'];?>"></td>
									<?php 
									if ($live_lot[$i]['lot_withdradn']== "Y") 
									 {
									   echo "Lot Withdrawn";
									 } 
									 else 
                                    {
                                       // if($auctiondet['on_page_bid']=="N"){
                                        ?>
                    									
                    					<!--  td id=bidnow<?php echo $live_lot[$i]['ynk_lotno'];?>>
    										<a data-id="<?php echo $live_lot[$i]['ynk_lotno'];?>" 
    										data-part-bid="<?php echo $live_lot[$i]['part_bid'];?>"
    										data-prod="<?php echo $live_lot[$i]['product'];?>" 
    										data-unit="<?php echo $live_lot[$i]['bidunit'];?>" 
    										data-min-incr="<?php echo $live_lot[$i]['incrby'];?>" 
    										data-bid-qty="<?php echo $live_lot[$i]['bidqty'];?>"
    										data-min-part-bid="<?php echo $live_lot[$i]['min_part_bid'];?>"
    										data-currency="<?php echo $live_lot[$i]['currency'];?>" 
    										data-total-qty="<?php echo (float)$live_lot[$i]['totalqty'];?>"
    										data-mul-fact="<?php echo $live_lot[$i]['mul_factor'];?>"
    										data-add-fact="<?php echo $live_lot[$i]['add_factor'];?>" 
    										data-start-bid="<?php echo $live_lot[$i]['startbid'];?>"
    										data-decimal-point="<?php echo $auction_details['decimal_point']; ?>"
    										data-prefered-currency="<?php echo $currency['bidder_prefered_currency_name']; ?>"
    										data-prefered-currency-rate="<?php echo $currency['conversion_rate']; ?>"
    										data-currency-primary-id="<?php echo $currency['bidder_prefered_currency_id']; ?>"
    										data-currency-prefered-id="<?php echo $currency['bidder_prefered_currency_id']; ?>"
    										class="btnBid btn btn-sm btn-success" >Bid Now</a>
										</td--> 
										
                    				
                    					<?php
                                        //}
                                       // else 
                                      //  {
                                        ?>
                                        <td>
                                        <table class="table">
                                       
                                        <tr class="<?php echo $live_lot[$i]['part_bid']=="Y"?'':'';?>">
                                        	<td>QTY(<?php echo empty($update_qty_amt) ? $live_lot[$i]['bidunit'] : 'MT' ?>): </td>
                                        	<td><input type="text" name="txtQty_<?= $live_lot[$i]['ynk_lotno'];?>" id="txtQty_<?= $live_lot[$i]['ynk_lotno'];?>"  <?php if($live_lot[$i]['part_bid']=="N") echo " readonly value='".$live_lot[$i]["totalqty"]."'"; ?> style="width:80px"/></td>
                                        </tr>
                                        <tr>
										<td title="<?php echo '1'. $currency['auction_currency_name'].'='. $currency['conversion_rate'].' '.$currency['bidder_prefered_currency_name']?>">
											<?php if (empty($update_qty_amt)){ ?>
                                        	<?php echo $lot_settings['amt']==""?'RATE':$lot_settings['amt']; ?> (<?php echo  $currency['bidder_prefered_currency_name']?>):
											<?php } else { ?>
												Rate (Rs./Quintal)
											<?php } ?>
											</td>
                                        	<td><input type="text" name="txtOnPageAmt_<?= $live_lot[$i]['ynk_lotno'];?>" id="txtOnPageAmt_<?= $live_lot[$i]['ynk_lotno'];?>" style="width:80px" /></td>
                                        </tr>
                                        <tr>
                                        	<td colspan="2"><div id="OnPageHint_<?= $live_lot[$i]['ynk_lotno'];?>" ></div>
                                        	<button name="btn" class="btnBid btn btn-sm btn-success" data-id="<?= $live_lot[$i]['ynk_lotno'];?>">Save My Bid</button>
                                        	</td>
                                        </tr>
                                        </table>
                                             </td>           
                                    <?php 
                                    //}
                								
                                }
                                echo "</tr>";
											
							
							}
							?>
							</tbody>
						</table>
					</div>
					<section class="panel panel-default">
					<div class="panel-heading">UPCOMING LOTS: Auction No : <?php echo $auction_details['ynk_saleno']; ?> <span class="pull-right"><small>Vendor: <?php echo $auction_details['vendor_name']; ?></small> </span></div>
							<div class="table-responsive">
						   <table class="table table-bordered b-t b-light text-sm table-striped" id="lot_upcoming_table"> 
							<thead>                                                  
							  
							   <tr>                                                 
									<th><?php echo $lot_settings['ltno']==""?'LOT NO':$lot_settings['ltno']; ?></th>  
									<th class="<?= (@$lot_settings['h_product']=="Y" && @$lot_settings['h_product_image']=="Y")?"hide":"";?>"><?php echo @$lot_settings['product']==""?'PRODUCT':$lot_settings['product']; ?></th>									
									<th class="<?= @$lot_settings['h_plant']=="Y"?"hide":"";?>"><?php echo $lot_settings['plant']==""?'PLANT':$lot_settings['plant']; ?></th>
									<th class="<?= @$lot_settings['h_tq']=="Y"?"hide":"";?>"><?php echo $lot_settings['tq']==""?'QTY':$lot_settings['tq']; ?></th>
									<th class="<?= @$lot_settings['h_bq']=="Y"?"hide":"";?>"><?php echo $lot_settings['bq']==""?'BID QTY':$lot_settings['bq']; ?></th>
									<th class="<?= @$lot_settings['h_lsd']=="Y"?"hide":"";?>"><?php echo $lot_settings['lsd']==""?'START':$lot_settings['lsd']; ?></th>
									<th class="<?= @$lot_settings['h_sb']=="Y"?"hide":"";?>"><?php echo $lot_settings['sb']==""?'START':$lot_settings['sb']; ?></th>
									<th class="<?= @$lot_settings['h_dn']=="Y"?"hide":"";?>"><?php echo $lot_settings['db']==""?'INCR/DECR':$lot_settings['db']; ?></th>
								    <th>CURRENCY</th>
									<th style="width:150px">TIME LEFT</th>	
								</tr>
							</thead>
							<tbody id="Lot_Upcoming"></tbody>
							</table>
							</div>

				</section>       
			</div>
           
       
        <!--/.page-content--> 
      </div>
      <input type="hidden" name="sLotID" id="sLotID" value=""/>
     
      <!--/.row--> 
    </div>
    <!--/.container--> 
  </div>
  <!-- /.main-container -->
  
  
  <!-- /.footer --> 
  <?php $this->load->view(ciHtmlTheme.'/includes/footer');?>
 
</div>
<!-- /.wrapper --> 
</div>
<!-- Le javascript
================================================== --> 
<!-- Placed at the end of the document so the pages load faster -->
<script src="<?php echo base_url(ciHtmlTheme.'/assets/js/jquery.min.js')?>"> </script>
<script src="<?php echo base_url(ciHtmlTheme.'/assets/bootstrap/js/bootstrap.min.js')?>"></script> 
<script type="text/javascript" src="<?php echo base_url(); ?>common_plugin/jquery.plugin.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>common_plugin/countdown/jquery.countdown.min.js"></script>

<!-- include equal height plugin  --> 
<script src="<?php echo base_url(ciHtmlTheme.'/assets/js/jquery.matchHeight-min.js')?>"></script> 
<!-- include jquery list shorting plugin plugin  --> 
<script src="<?php echo base_url(ciHtmlTheme.'/assets/js/hideMaxListItem.js')?>"></script> 
<!-- include jquery.fs plugin for custom scroller and selecter  --> 
<script src="<?php echo base_url(ciHtmlTheme.'/assets/plugins/jquery.fs.scroller/jquery.fs.scroller.js')?>"></script>
<script src="<?php echo base_url(ciHtmlTheme.'/assets/plugins/jquery.fs.selecter/jquery.fs.selecter.js')?>"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/detect_browser.js"></script>

<script>


	var trID;
	var BaseUrlCI="<?php echo base_url(); ?>";//base url
	var GlobalAuctionID="<?php echo md5($auction_details['ynk_auctionid']); ?>";
	var auction_currency_id="<?php echo $auction_details['primary_currency']; ?>"; // default currency id
	var auction_type="<?php echo $auction_details['auctiontype']; ?>"; // auction type
	var OpenLots="";
	var show_client = "N";
	var conversion_rate = <?php echo  $currency['conversion_rate']?>;
	var bidder_currency_code = "<?php echo  $currency['bidder_prefered_currency_name']?>";
	/**
	 * initial lot render
	 */
	var decimal_point=<?php echo $auction_details['decimal_point'];?>;
	var client_id=<?php echo $this->session->userdata('bidder_id')?>;
	var vendor_id="";
	var lot_close_url="<?php echo base_url();?>buyer/live_auctions/is_closed/<?php echo $live_auction_id;?>";
	//url for running lots
	var live_lot_url="<?php echo base_url();?>buyer/live_auctions/running_lots/<?php echo $live_auction_id;?>";
	// live update of lot data url
	var LiveUpdateUrl="<?php echo base_url(); ?>buyer/live_auctions/lot_update_bid_data/<?php echo $live_auction_id;?>";
	//add new lots url to check if it is runnng
	var LiveUpdateNewLot="<?php echo base_url(); ?>buyer/live_auctions/live_update_new_lot/<?php echo $live_auction_id;?>";
	//getting the upcoming lots
	var UpcomingLotsUrl="<?php echo base_url(); ?>buyer/live_auctions/upcoming_lots/<?php echo $live_auction_id;?>";
	var one_by_one_lot = "<?php echo $auction_details['one_by_one_lot']?>";
	var allowed_max_extension = "<?php echo $auction_details['allowed_max_extension']?>";
	<?php 
	list($h, $m, $s) = explode(':',$auction_details['increase_lot_time_in']); 
	?>
	var increase_lot_time_in = '<?php echo ($h * 3600) + ($m * 60) + $s;?>';

	$(document).ready(function(){
		checksession();
		setInterval(function() {
			checksession();
		}, 60000);
		<?php
		for($i=0;$i<count($live_lot);$i++)
		{
		  
		?>
		if(auction_type=="Dutch Auction")
			$("#timeleft_<?php echo $live_lot[$i]['ynk_lotno'];?>").countdown({ padZeroes: true,until: +secondConvert("<?php echo $live_lot[$i]['incrtime'];?>"),onTick: function(){highlightLot(this);}, onExpiry: function() { CloseLot(this);}});
		else
			$("#timeleft_<?php echo $live_lot[$i]['ynk_lotno'];?>").countdown({ padZeroes: true,until: +secondConvert("<?php echo $live_lot[$i]['TOTTIME'];?>"),onTick: function(){highlightLot(this);}, onExpiry: function() { CloseLot(this);}});
		OpenLots = "<?php echo $live_lot[$i]['ynk_lotno'].',';?>";
		<?php
		}
		?>
		
		$("#open_lots").val(OpenLots);
		
	
	//live_lot(live_lot_url);
	upcoming_lot(UpcomingLotsUrl);
	waitForMsg(LiveUpdateUrl,BaseUrlCI); /* Start the inital request */
	
	$('.btnBid').click(function(e){	
		e.preventDefault(); 
		var ans=confirm("Save the Bid?");
		if(ans)
		{
    		lot_id=$(this).attr('data-id');
    
    		PartQty=$('#txtQty_'+lot_id+'').val();
    		Amt=$('#txtOnPageAmt_'+lot_id+'').val();
    
    		if(Amt==""){
    			
    			$('#OnPageHint_'+lot_id+'').html('Invalid Amount');
    			$('#OnPageHint_'+lot_id+'').css({"color": "red"}); 
    			setTimeout(function(){  $('#OnPageHint_'+lot_id+'').html('') }, 3000);
    			return false;
    		}
    		else 
    			Amt=parseFloat(Amt);
    		
    		if(Amt<=0){
    			alert();
    			$('#OnPageHint_'+lot_id+'').html('Invalid Amount');
    			return false;
    		}
    		var $btn = $(this).button('loading');
    		$.ajax({
    	        type: "POST",
    	        url:"<?php echo base_url('buyer/live_auctions/save_bid')?>" ,
    			data:{auction_id:GlobalAuctionID,lotid:lot_id,clientBid:Amt,partbidQty:PartQty},
    			dataType:'json',
    	        cache: false,
    	        async: true, /* If set to non-async, browser shows page as "Loading.."*/
    	        cache: false,
    	        timeout:5000, /* Timeout in ms */
    
    	        success: function(data){
    		       
    		        $('#OnPageHint_'+lot_id+'').html(data['msgStr']);
    		       if(data['status'])
    		          	$('#OnPageHint_'+lot_id+'').css({"color": "green"});
    		       else 
    		    	  	$('#OnPageHint_'+lot_id+'').css({"color": "red"});   
    	    		
    			   setTimeout(function(){  $('#OnPageHint_'+lot_id+'').html('') }, 5000);
    				if(data['update_time']){
        				 update_time('timeleft_'+lot_id);
        				 upcoming_lot(UpcomingLotsUrl);
    				}
    		        $btn.button('reset');
    	        },
    	        error: function(data){
    	        	 $btn.button('reset');
    	        }
    	        
    	 		});
		}
		
		});	
	});	
	


	
	$("input[name='lotno']").click(function(e){
		$('#sLotID').val($(this).val());
	});
	$('#QDistribution').click(function(e){
		if($('#sLotID').val()=="")
			alert("Please select a lot");
		else
		{
			testwindow= window.open ("<?php echo base_url()?>buyer/live_auctions/quantity_distribution/<?php echo md5($auction_details['ynk_auctionid']); ?>/"+$('#sLotID').val()+"", "WHERE-AM-I","location=0,status=1,scrollbars=1,width=500,height=400");
			testwindow.moveTo(0,0);
		}
	});
	$('#TaxDetails').click(function(e){
		if($('#sLotID').val()=="")
			alert("Please select a lot");
		else
		{
			testwindow= window.open ("<?php echo base_url()?>buyer/live_auctions/tax_details/<?php echo md5($auction_details['ynk_auctionid']); ?>/"+$('#sLotID').val()+"", "WHERE-AM-I","location=0,status=1,scrollbars=1,width=500,height=400");
			testwindow.moveTo(0,0);
		}
	});
	$('#BidLimit').click(function(e){
		
			testwindow= window.open ("<?php echo base_url()?>buyer/live_auctions/bid_limit_report/<?php echo md5($auction_details['ynk_auctionid']); ?>", "BID-LIMIT","location=0,status=1,scrollbars=1,width=800,height=400");
			testwindow.moveTo(0,0);
		
	});
	$('#ClosedLot').click(function(e){
		testwindow= window.open ("<?php echo base_url()?>buyer/live_auctions/closed_lot/<?php echo md5($auction_details['ynk_auctionid']); ?>/"+$('#sLotID').val()+"", "WHERE-AM-I","location=0,status=1,scrollbars=1,width=500,height=400");
		testwindow.moveTo(0,0);
	});
	function checksession(){
	jQuery.ajax({
		url: '<?php echo base_url();?>buyer/live_auctions/log_user_session',
		type:'post',
		dataType:'json',
		data: {'data':user_browser_info},
		success:function(res){
		}
	});
	}
	function update_time(TIMER_ID){
		
		var parts = TIMER_ID.split('_');
		
		$.ajax({
			url: lot_close_url + "/" +  parts[1],
			dataType:'json',
			success: function(data) {
				if(data.status)
					{
						$('table#lot_table tr#lot_' + parts[1] + '').remove();
					}
				else
					{
						$('#timeleft_' + parts[1]).countdown('option', {until: +secondConvert(data.auction_data.TOTTIME)});
					}
				}
		});
	}
	
</script> 
<script src="<?php echo base_url(); ?>js/common.js"></script> <!-- Common functions related to auction-->
<script src="<?php echo base_url(); ?>js/ynk/ynk_default.js?v=<?php echo rand(1,10000000)?>"></script> <!-- Common functions related to auction-->
<!-- include jquery autocomplete plugin  -->
</body>
</html>
