<div class="header">
    <nav class="navbar navbar-main yamm">
    <div class="container-fluid">
        <div class="navbar-header" style='padding-top:5px;'>
          
          <a style='width: 100%;'href="<?php echo base_url('buyer/dashboard');?>">
            
            <?php if ( ! empty($this->session->userdata('buyer_logo'))){ ?>
							<img style="height: auto;width: 50px;" src="<?php echo $this->session->userdata('buyer_logo'); ?>" alt="Oya Auction" class="brand-icon">
						<?php } else { ?>
							<img style="height: auto;width: 50px;" src="<?php echo base_url('/images/logo.png'); ?>" alt="Oya Auction" class="brand-icon">
						<?php } ?> 
            <span class="comp_name" >OYA AUCTION</span>
          
          </a>
          <button class="navbar-toggle collapsed" type="button" data-toggle="collapse" data-target="#main-nav-collapse" area_expanded="false"><span class="sr-only">Main Menu</span><span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span>
          </button>
        </div>
        <div class="collapse navbar-collapse" id="main-nav-collapse">
          
            <ul class="nav navbar-nav nav_sec">
           
            
                <li><a style='display:flex;align-items:center;font-size:20px;' href="<?php echo base_url();?>"><i class="fa fa-home" aria-hidden="true"></i></a></li>
                <li><a href="<?php echo base_url('auction');?>">SEARCH AUCTIONS</a></li>
                <li><a href="<?php echo base_url('products');?>">SEARCH PRODUCTS</a></li>
               <!-- <li><a href="<?php echo base_url('auction/dutch');?>">DUTCH AUCTION</a></li>
                <li><a href="<?php echo base_url('auction/etender');?>">E-TENDER</a></li>-->
                
               <?php 
			
			
				if($this->session->userdata('bidder_id')!="") {
                   ?>
                       <li style='position: relative;' id="auc_cat_sec" class="dropdown"><a id="auc_cat" href="#"><i class="fa fa-user icon_set"></i>&nbsp;<?php echo $this->session->userdata('compname');?><i class="drop-caret" data-toggle="dropdown"></i></a>
                    <ul class="dropdown-menu dropdown-menu-category">
                        
                    <li ><a href="<?php echo base_url('buyer/dashboard')?>"><i class="icon-home dropdown-menu-category-icon"></i> Dashboard </a></li>
                    <li><a href="<?php echo base_url('buyer/auctions/favourite_auctions')?>"><i class="icon-heart dropdown-menu-category-icon"></i> Favourite Auctions</a></li>
                    <li><a href="<?php echo base_url('buyer/auctions/pending_approval')?>"><i class="icon-heart dropdown-menu-category-icon"></i>  Pending Participations </a></li>
                    <li><a href="<?php echo base_url('buyer/my_associations')?>"><i class="icon-star-circled dropdown-menu-category-icon"></i> My Associates </a></li>
                    <li><a href="<?php echo base_url('buyer/my_messages')?>"><i class="icon-folder-close dropdown-menu-category-icon"></i> My Messages </a></li>
                    <li><a href="<?php echo base_url('buyer/auctions/current_auctions')?>"><i class="icon-hourglass dropdown-menu-category-icon"></i> Live Auctions </a></li>
                    <li><a href="<?php echo base_url('buyer/products/my_favourite_products')?>"><i class=" icon-money dropdown-menu-category-icon"></i> Favourite Products </a></li>
                    <li><a href="<?php echo base_url('buyer/profile');?>"><i class=" icon-money dropdown-menu-category-icon"></i> My Profile </a></li>
                    <li><a href="<?php echo base_url('user/logout');?>"><i class=" icon-money dropdown-menu-category-icon"></i> Log Out </a></li>
                        
                    </ul>
                </li>
                <?php }
                if($this->session->userdata('vendor_id')!="") {
				// buyer menus
			?>
				<li class="dropdown"> <a style='postion:relative;width:150px;' href="#" class="dropdown-toggle" data-toggle="dropdown"> &nbsp;&nbsp;<?php echo $this->session->userdata('user_name')?><i class="icon-user fa icon_1"></i> <i class=" icon-down-open-big fa icon_2"></i></a>
				  <ul class="dropdown-menu dropdown-menu-category">
					<li class="active"><a href="<?php echo base_url('auctioneer/dashboard')?>"><i class="icon-home dropdown-menu-category-icon"></i> Dashboard </a></li>
					<li><a href="<?php echo base_url('auctioneer/buyer/associates')?>"><i class="icon-star-circled dropdown-menu-category-icon"></i> My Associates </a></li>
					<li><a href="<?php echo base_url('auctioneer/messages')?>"><i class="icon-folder-close dropdown-menu-category-icon"></i> My Messages </a></li>
					<li><a href="<?php echo base_url('auctioneer/live_auctions')?>"><i class="icon-hourglass dropdown-menu-category-icon"></i> Live Auctions </a></li>
					<li><a href="<?php echo base_url('auctioneer/wallet')?>"><i class=" icon-money dropdown-menu-category-icon"></i> Wallet </a></li>
					<li><a href="<?php echo base_url('auctioneer/profile');?>"><i class=" icon-money dropdown-menu-category-icon"></i> My Profile </a></li>
					<li><a href="<?php echo base_url('user/logout');?>"><i class=" icon-money dropdown-menu-category-icon"></i> Log Out </a></li>
				  </ul>
				</li>
			<?php } ?>
                  </ul>
            
            
        </div>
    </div>
</nav>
  </div>