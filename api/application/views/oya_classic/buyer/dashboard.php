<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<!-- Fav and touch icons -->
<link rel="apple-touch-icon-precomposed" sizes="144x144" href="<?php echo base_url(ciHtmlTheme.'/assets/ico/apple-touch-icon-144.png')?>">
<link rel="apple-touch-icon-precomposed" sizes="114x114" href="<?php echo base_url(ciHtmlTheme.'/assets/ico/apple-touch-icon-114.png')?>">
<link rel="apple-touch-icon-precomposed" sizes="72x72" href="<?php echo base_url(ciHtmlTheme.'/assets/ico/apple-touch-icon-72.png')?>">
<link rel="apple-touch-icon-precomposed" sizes="57x57" href="<?php echo base_url(ciHtmlTheme.'/assets/ico/apple-touch-icon-57.png')?>">
<link rel="shortcut icon" href="<?php echo base_url(ciHtmlTheme.'/assets/ico/favicon.png')?>">
<title>DASHBOARD - <?php echo $this->session->userdata('conperson');?></title>
<!-- Bootstrap core CSS -->
<link href="<?php echo base_url(ciHtmlTheme.'/assets/bootstrap/css/bootstrap.min.css')?>" rel="stylesheet">
<!-- Custom styles for this template -->
<link href="<?php echo base_url(ciHtmlTheme.'/assets/css/style.css')?>" rel="stylesheet">
<link href="<?php echo base_url(ciHtmlTheme.'/assets/css/custom.css')?>" rel="stylesheet">
<!-- styles needed for carousel slider -->


<script>
    paceOptions = {
      elements: true
    };
</script>
<script src="<?php echo base_url(ciHtmlTheme.'/assets/js/pace.min.js')?>"></script>

 </head>
<body>

<div id="wrapper">
<!-- /.header -->
 <?php $this->load->view(ciHtmlTheme.'/buyer/buyer_header');?>
  <!-- /.header -->
  <div class="main-container">
   <div class="container-fluid">
      <div class="row">
        <div class="col-sm-3 page-sidebar">
         <?php $this->load->view(ciHtmlTheme.'/buyer/left_navigation');?>
        </div>
        <!--/.page-sidebar-->
        
        <div class="col-sm-9 page-content">
          <div class="inner-box">
          <div class="row">
            <div class="col-md-5 col-xs-4 col-xxs-12">
              <h3 class="no-padding text-center-480 useradmin"><a href="<?php echo base_url('buyer/profile')?>">
              <img class="userImg" src="<?php echo base_url('images/user-icon.png')?>" alt="<?php echo $this->session->userdata('conperson');?>" align="left"> <?php echo $this->session->userdata('conperson');?> </a> 
              	<br /><span class="page-sub-header-sub small"><?php echo $this->session->userdata('compname');?></span>
              </h3>
            	
            </div>
            <div class="col-md-7 col-xs-8 col-xxs-12">
              <div class="header-data text-center-xs"> 
                
                <!-- Traffic data -->
                <!--<div class="hdata">
                  <div class="mcol-left"> -->
                    <!-- Icon with red background --> 
                  <!--  <i class="fa fa-eye ln-shadow"></i> </div>
                  <div class="mcol-right"> -->
                    <!-- Number of visitors -->
                   <!-- <p><a href="#">7000</a> <em>visits</em></p>
                  </div>
                  <div class="clearfix"></div>
                </div>-->
                
                <!-- revenue data -->
                <!--<div class="hdata">
                  <div class="mcol-left"> -->
                    <!-- Icon with green background --> 
                   <!-- <i class="icon-th-thumb ln-shadow"></i> </div>
                  <div class="mcol-right"> 
                     Number of visitors -->
                   <!-- <p><a href="#">12</a><em>Ads</em></p>
                  </div>
                  <div class="clearfix"></div>
                </div>-->
                
                <!-- revenue data -->
                <!--<div class="hdata">
                  <div class="mcol-left"> -->
                    <!-- Icon with blue background --> 
                   <!-- <i class="fa fa-user ln-shadow"></i> </div>
                  <div class="mcol-right"> -->
                    <!-- Number of visitors -->
                   <!-- <p><a href="#">18</a> <em>Favorites </em></p>
                  </div>
                  <div class="clearfix"></div>
                </div>-->
              </div>
            </div>
          </div>
          </div>
          
          <div class="inner-box">
            <div class="welcome-msg">
              <h3 class="page-sub-header2 clearfix no-padding">Hello <?php echo $this->session->userdata('conperson');?></h3>
              <span class="page-sub-header-sub small">You last logged in at: <?php echo $this->session->userdata('last_login') ;?>  [<?php echo $this->session->userdata('user_tz')?>]</span> </div>
             	
          </div>
        </div>
        <!--/.page-content--> 
      </div>
      <!--/.row--> 
    </div>
    <!--/.container--> 
  </div>
  <!-- /.main-container -->
  
  
  <!-- /.footer --> 
  <?php $this->load->view(ciHtmlTheme.'/includes/footer');?>
 
</div>
<!-- /.wrapper --> 

<!-- Le javascript
================================================== --> 
<!-- Placed at the end of the document so the pages load faster --> 
<script src="<?php echo base_url(ciHtmlTheme.'/assets/js/jquery.min.js')?>"> </script>
<script src="<?php echo base_url(ciHtmlTheme.'/assets/bootstrap/js/bootstrap.min.js')?>"></script> 
<!-- include carousel slider plugin  --> 

<!-- include equal height plugin  --> 
<script src="<?php echo base_url(ciHtmlTheme.'/assets/js/jquery.matchHeight-min.js')?>"></script> 
<!-- include jquery list shorting plugin plugin  --> 
<script src="<?php echo base_url(ciHtmlTheme.'/assets/js/hideMaxListItem.js')?>"></script> 
<!-- include jquery.fs plugin for custom scroller and selecter  --> 
<script src="<?php echo base_url(ciHtmlTheme.'/assets/plugins/jquery.fs.scroller/jquery.fs.scroller.js')?>"></script>
<script src="<?php echo base_url(ciHtmlTheme.'/assets/plugins/jquery.fs.selecter/jquery.fs.selecter.js')?>"></script>
<!-- include custom script for site  --> 
<script src="<?php echo base_url(ciHtmlTheme.'/assets/js/script.js')?>"></script>
<!-- include jquery autocomplete plugin  -->
</body>
</html>
