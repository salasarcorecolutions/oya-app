<section id="content">
	<section class="vbox">
		<section class="scrollable padder">
			<ul class="breadcrumb no-border no-radius b-b b-light pull-in">
				<li><a href="<?php echo base_url();?>buyer"><i class="fa fa-home"></i> Home</a></li>
				<li><a href="<?php echo base_url();?>buyer">Yankee Auction</a></li>
				<li class="active">Add Yankee Proxy</li>
			</ul>
			<section class="panel panel-default">
				<div class="row">
					<div class="col-lg-12">
						<div class="panel-body">
							<marquee>
								<h4 class="text-primary"><?php echo $auc_msg; ?> </h4>
							</marquee>
						</div>
					</div>
				</div>
			</section>
			<section class="panel panel-default">
				<header class="panel-heading font-bold">
						<center>
							TODAY :<span><?php echo date('l dS \of F Y h:i:s A'); ?></span>
							|| <a class="btn btn-info"  href="<?php echo base_url();?>buyer"> <i class="fa fa-home"></i> Home </a>
						|| <a class="btn btn-info pull-in"  href="<?php echo base_url();?>live_yankee_auction/live/<?php echo $this->uri->segment(3);?>"> Back To Live Auction </a>
						</center>
				</header>
				<?php
					if ( ! empty($auction_available))
					{
					?>
				<header class="panel-heading">Auction No : <?php echo $ynk_saleno; ?> <span class="pull-right"><small>Your ID: <?php echo str_replace('-',$this->session->userdata('bidder_id'),$ynk_saleno); ?></small> </span></header>
				<div class="table-responsive">
				<table class="table table-bordered b-t b-light table-striped" id="lot_table">
					<thead>
						<tr class='text-center'>
							<th>Lot No. </th>
							<th>Product</th>
							<th>Tot Qty.</th>
							<th>Min.Bid Qty.</th>
							<th>Starting Time</th>
							<th>Starting Bid</th>
							<th>Min.Incr.</th>
							<th>Proxy Amount</th><th>Proxy Qty.</th>
							<th nowrap>Bid Now</th>
						</tr>
					</thead>
					<tbody id="lot_list">
						<?php
						if (count($lots)==0)
						{
							echo "<tr><td colspan='9' class='alert-danger text-center'>No Lots</td></tr>";
						}
						for ($i=0;$i<count($lots);$i++)
						{
						?>
						<tr class='text-center'>
							
							<td>
								<?php echo $lots[$i]['ynk_lotno']; ?><input type="hidden" name="currency" id="currency" />
								<input type="hidden" name="my_currency" id="my_currency" value="<?php echo $this->session->userdata("currency"); ?>" />
							</td>
							<td> <a class='text-info' href='<?php echo base_url();?>buyer/yankee_auction_details/<?php echo $this->uri->segment(3);?>/<?php echo $lots[$i]['ynk_lotno'];?>/A' target=_blank  title='Click to view'><i class="fa fa-link"></i> <?php echo $lots[$i]['product']; ?></a></td>
							<td><?php echo $lots[$i]['totalqty']." ".$lots[$i]['totunit']; ?></td>
							<td><?php echo $lots[$i]['bidqty']." ".$lots[$i]['bidunit']; ?></td>
							<td><?php echo "S:".$lots[$i]['stime']."<br>E:".$lots[$i]['etime']; ?></td>
							<td>
								<?php
									if ($this->session->userdata('currency')!=$lots[$i]['currency']){
										$start_bid = round($lot_currency[$i][0]['currency_rate']*$lots[$i]['startbid'],3);
									} else {
										$start_bid = $lots[$i]['startbid'];
									}
									if (empty($currency_data['conversion_details']) || $currency_data['primary_currency']['primary_currency'] == $currency_data['conversion_details']['other_currency'])
									{
										$currency_conversion_rate = 1;
									}
									else
									{
										$currency_conversion_rate = $currency_data['conversion_details']['currency_conversion_rate'];
									}
									echo $start_bid*$currency_conversion_rate;
								?>
							</td>
							<td>
								<?php 
									if ($this->session->userdata('currency')!=$lots[$i]['currency']){
										$min_incrby = round($lot_currency[$i][0]['currency_rate']*$lots[$i]['incrby'],3);
									} else {
										$min_incrby = $lots[$i]['incrby'];
									}
									if (empty($currency_data['conversion_details']) || $currency_data['primary_currency']['primary_currency'] == $currency_data['conversion_details']['other_currency'])
									{
										$currency_conversion_rate = 1;
									}
									else
									{
										$currency_conversion_rate = $currency_data['conversion_details']['currency_conversion_rate'];
									}
									echo $min_incrby*$currency_conversion_rate;
								?>
							</td>
							<?php
							if(count($proxy[$i])>0)
							{
							?>
								<td>
									<?php 
									if ($this->session->userdata('currency')!=$lots[$i]['currency']){
										$proxy_amount = round($lot_currency[$i][0]['currency_rate']*$proxy[$i][0]['proxy_amt'],3);
									} else {
									    $proxy_amount = $proxy[$i][0]['proxy_amt']. " ".$this->session->userdata('currency');
									}
									if (empty($currency_data['conversion_details']) || $currency_data['primary_currency']['primary_currency'] == $currency_data['conversion_details']['other_currency'])
									{
										$currency_conversion_rate = 1;
									}
									else
									{
										$currency_conversion_rate = $currency_data['conversion_details']['currency_conversion_rate'];
									}
									echo round($proxy_amount*$currency_conversion_rate);
									?>
								</td>
								<td><?php echo $proxy[$i][0]['proxy_qty']. " " .$lots[$i]['bidunit']?></td>
								
							<?php
							}else{
								echo "<td>-</td>";
								echo "<td>-</td>";
							}
							if($lots[$i]['proxy']=="Y")
							{
								if(strtotime($lots[$i]['proxystartdate']) <= strtotime(date("Y-m-d")))
								{
								?>
								<td>
								
									<button data-target="#proxy_modal"  onclick="set_lot_no('<?php echo $lots[$i]['ynk_lotno'];?>','<?php echo $lots[$i]['currency']?>','<?php echo $this->session->userdata("currency")?>','<?php echo $lots[$i]['part_bid']?>','<?php echo $lots[$i]['min_part_bid']?>','<?php echo $lots[$i]['totalqty']?>')" class="btn btn-info" />Add Proxy</button>
									
									<?php 
									if(isset($proxy[$i][0]['is_paused'])){
									
									if($proxy[$i][0]['is_paused']=='N'){?>
									<button data-target="#proxy_pause"  onclick="pause_proxy('<?php echo $lots[$i]['ynk_lotno'];?>','<?php echo $ynk_auctionid?>','Y')" class="btn btn-warning" />Pause Proxy</button>
									<?php }else{?>
									<button data-target="#proxy_pause"  onclick="pause_proxy('<?php echo $lots[$i]['ynk_lotno'];?>','<?php echo $ynk_auctionid?>','N')" class="btn btn-warning" />Resume Proxy</button>
									<?php }}?>
								</td>
								<?php
								}else{
									echo "<td>Proxy Not Allowed Yet !!!</td>";
								}									
							}else{
								echo "<td><font color='#FF0000'>Proxy Not Allowed For This Lot</font></td>";
							}
							?>
						</tr>
						<?php
						}
							?>
					</tbody>
				</table>
				</div>
				<?php
					}
					else
					echo "<h4><center><font color='#FF0000'>Auction Ended</font></center></h4>";
					?>
			</section>
			<!-- Modal -->
			<div class="modal fade" id="proxy_modal" tabindex="-1" role="dialog" aria-labelledby="proxy_modal" aria-hidden="true">
				<div class="modal-dialog">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
							<h4 class="modal-title text-info" id="myModalLabel">ADD PROXY FOR <?php echo $this->uri->segment(4)=='R'?'RANK':'DEFAULT';?> YANKEE SALE  <?php echo $this->uri->segment(3);?> & LOT <span id="lotid"></span></h4>
						</div>
						<div class="modal-body" style="margin-bottom:50px">
							<form id="accept_proxy_form" action="">
								<div id="msg"></div>
								<div class="row">
									<div class="col-lg-12">
										<label class="col-lg-3 control-label">Your Proxy Bid Amount</label>
										<div class="col-lg-7"> 
											<input type="text" placeholder="Proxy Amount" name="proxy_amt" id="proxy_amt" class="form-control" onkeyup="change_proxy()"> 
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-lg-12">&nbsp;</div>
									<div class="col-lg-12" id="show_mycurrency" style="display:none;">
										<label class="col-lg-3 control-label"><span id="show_lot_currency"></span> &nbsp;&nbsp;Proxy Bid</label> 
										<div class="col-lg-7"> 
											<input type="text" placeholder="Proxy Amount" name="proxy_amt_convert" id="proxy_amt_convert" class="form-control" /> 
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-lg-12">
										<label class="col-lg-3 control-label">Your Proxy Min Bid Qty</label> 
										<div class="col-lg-7"> 
											<input type="text" placeholder="Proxy Bid Quantity" name="proxy_bid_qty" id="proxy_bid_qty" class="form-control" /> 
										</div>
									</div>
								</div>
									
								
								<input type="hidden" id="proxy_lots" />
								<input type="hidden" id="lot_currency" />
						
    						<div class="modal-footer">
    							<button type="button" class="btn btn-info" data-target="#what_is_proxy_modal"  onclick="whatisproxy()">What is it?</button>
    							<button type="submit" class="btn btn-info" id="accept_term">Accept My Proxy Bid</button>
    							<button type="button" onclick="refresh()" class="btn btn-info" data-dismiss="modal">Close</button>
    						</div>
						</form>
						</div>
					</div>
				</div>
			</div>
			<!-- Modal -->
			<div class="modal fade" id="what_is_proxy_modal" tabindex="-1" role="dialog" aria-labelledby="what_is_proxy_modal" aria-hidden="true">
				<div class="modal-dialog">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
							<h4 class="modal-title" id="myModalLabel">YANKEE PROXY BID <span id="lotid"></span></h4>
						</div>
						<div class="modal-body" style="margin-bottom:50px">
							<div >
								<P>
									<b>PROXY BID:</b><br><br>
									Proxy bid is a facility for automatic bidding. Computer will automatically bid for you.
									<br><br>
									Example: Suppose starting bid for a lot is Rs.1000.00. Minimum increment Rs.10.00. Let say you will bid up to Rs.20,000.00 for the lot. Select the lot, put the maximum amount  that is suitable for you . If you make the proxy bid on then you don't have to worry. Our computer will bid for you.
									<br><br>
									<b>How it works:</b><br><br>
									&nbsp;&nbsp;&nbsp;&nbsp;   In the above case if some one place a bid of Rs. 1010.00, our computer will track the bid and it will automatically bid of Rs 1020.00 for you. So any time you will remain H1.
									<br><br>
									<b>What will happen if somebody bids 20000.00?</b><br><br>
									&nbsp;&nbsp;Our program will not bid for you any more because you have given a maximum amount off Rs. 20000.00.
								</p>
							</div>
						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-info" data-dismiss="modal">Close</button>
						</div>
					</div>
				</div>
			</div>
		</section>
	</section>
</section>
<script>
	var vRules = {
	    'proxy_amt':{required:true,number:true},
	    'proxy_bid_qty':{required:true,number:true}
	};
	var vMessages = {
		'proxy_amt':{required:"<font color='#FF0000'>Please Enter Proxy Amount</font>"},
		'proxy_bid_qty':{required:"<font color='#FF0000'>Please Enter Proxy Bid Quantity</font>"}
	};
	
	$("#accept_proxy_form").validate({
		rules: vRules,
		messages: vMessages,
		submitHandler: function(form){
			jQuery.ajax({
				url: '<?php echo base_url();?>live_yankee_auction/add_yankee_proxy_history',
				data: {"lotid":jQuery("#proxy_lots").val(), "auctionid":<?php echo $this->uri->segment(3); ?>,"txtProxyAmt":jQuery("#proxy_amt").val(),"lot_currency":jQuery("#currency").val(),"txtProxyQty":jQuery("#proxy_bid_qty").val()},
				type:"post",
				dataType: 'json',
				success: function (resObj) {
					$("#msg").html(resObj.msg);
				}
			});	
		}
	});
	// min is not working properly so i used minStrict User Define Method
	$.validator.addMethod('minStrict', function (value, el, param) {
		return parseFloat(value) >= parseFloat(param);
	});
	function set_lot_no(id,lot_currency,my_currency,part_bid,min_part_bid,totalqty)
	{
		$("#proxy_lots").val(id);
		$("#lotid").html(id);
		$("#show_lot_currency").html(lot_currency);
		$("#lot_currency").val(lot_currency);
		$("#currency").val(lot_currency);
		if(lot_currency!=my_currency)
		{
			$("#show_mycurrency").show();
		}
		if(part_bid == "Y"){
			$("#proxy_bid_qty").val(parseFloat(totalqty));
			$("#proxy_bid_qty").removeAttr("readonly");
			$("#proxy_bid_qty").attr("minStrict",parseFloat(min_part_bid));
		}else{
			$("#proxy_bid_qty").val(parseFloat(totalqty));
			$("#proxy_bid_qty").attr("readonly","readonly");
			$("#proxy_bid_qty").removeAttr("min");
		}
		$("#proxy_modal").modal('show');
	}
	
	function pause_proxy(LotID,AuctionID,pStatus)
	{
		msg=(pStatus=='Y')?'Pause Proxy':'Resume Proxy';
		if(confirm('Are you sure to '+msg+' from Now'))
		{

			jQuery.ajax({
				url: '<?php echo base_url();?>live_yankee_auction/pause_proxy',
				data: {"lotid":LotID, "auctionid":AuctionID,'pStatus':pStatus},
				type:"post",
				dataType: 'json',
				success: function (data) {
					if(data.status)
					{
						alert(data.msg);
						location.reload();
					}
					else
					{
						alert(data.msg);
					}
					
				}
			});
			}

		
	}
	
	function whatisproxy()
	{
		$("#what_is_proxy_modal").modal('show');
	}	
	function refresh()
	{
		window.location.href="<?php echo base_url();?>live_yankee_auction/add_proxy/<?php echo $this->uri->segment(3);?>" 
	}
	function change_proxy()
	{
		if($("#proxy_amt").val()!="")
		{
			jQuery.ajax({
				url: '<?php echo base_url();?>add_proxy/get_convert_rate',
				data: {"lotid":jQuery("#proxy_lots").val(), "auctionid":<?php echo $this->uri->segment(3); ?>,"txtProxyAmt":jQuery("#proxy_amt").val(),"lot_currency":jQuery("#currency").val()},
				type:"post",
				dataType: 'json',
				success: function (resObj) {
					$("#proxy_amt_convert").val(resObj.msg);
				}
			});	
		}else{
			$("#proxy_amt_convert").val("");
		}
	}
</script>