<?php
 /**
     * 
     * Same view sending/Returning json or html during live forward auction 
     * depending upon the variable $Parts
     * @param sTRING $parts
     */
/**
 * Sending the running lots in json format 
 */
if($parts=="Running_lots")
{
header('Content-type: application/json; charset=utf-8');

echo json_encode(array('LiveLot'=> $auctiondet));
}
/**
 * Sending the upcoming lots in json format 
 */
if($parts=="UpcomingLot")
{
header('Content-type: application/json; charset=utf-8');

echo json_encode(array('UpcomingLot'=> $auctiondet,'permission'=>$permission,'show_closed_popup'=>$show_closed_popup));
}
/**
 * Sending the updates on lots in string format 
 */
if($parts=="live_update")
{
	header('Content-type: application/json; charset=utf-8');
	echo json_encode($auctiondet);
}

/**
 * Sending the status notification during bid submission 
 */
if($parts=="Save_message")
{
	echo $auctiondet['msgStr'];
}
/**
 * returning 
 */
if($parts=="is_closed")
{
header('Content-type: application/json; charset=utf-8');

echo json_encode($auctiondet);
}
?>