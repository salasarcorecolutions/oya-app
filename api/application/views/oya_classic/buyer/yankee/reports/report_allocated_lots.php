<body>
	<table border="0" cellpadding="0" cellspacing="0" width="100%" id="table1" class="adminlist">
		<tr>
			<td align="center"><b><font size="3">ALLOCATED LOTS FOR SALE/AUCTION NO  <?php echo $auction_data['saleno']; ?></font></b></td>	
		</tr>
		<tr>
			<td>
				<table border="0" cellspacing="1" width="100%" id="table1" class="adminlist">
					<tr>
						<td align="center" nowrap colspan="4" ><b><font size="2">AUCTION DETAILS</font></b></td>
					</tr>
					<tr>
						<td width="30%" align="right" nowrap ><b>E-Auction Sale No</b></td>
						<td><?php echo $auction_data['saleno']; ?></td>
						<td width="30%" align="right"><b>Auction Type</b></td>
						<td><?php echo $auction_data['auctiontype']; ?></td>
					</tr>
					<tr>
						<td width="30%" align="right"><b>Message</b></td>
						<td><?php echo $auction_data['auction_msg']; ?></td>
						<td width="30%" align="right"><b>Auction Start Date</b></td>
						<td><?php echo $auction_data['start_date']; ?></td>
					</tr>
					<tr>
						<td width="30%" align="right" nowrap><b>Auction Open Up to</b></td>
						<td><?php echo $auction_data['end_date']; ?></td>
						<td width="30%" align="right"><b>Company</b></td>
						<td><?php echo $auction_data['vendor_name'];?></td>
					</tr>
					<tr>
						<td width="30%" align="right"><b>Particulars</b></td>
						<td colspan="3"><?php echo $auction_data['particulars']; ?></td>
					</tr>		
				</table>

			</td>
		</tr>
		<tr>
			<td bgcolor="#FFF8F0" align="center"><b><font size="2">BID DETAILS</font></b></td>
		</tr>		
		<tr>
			<td>
				<table border="0" cellspacing="1" width="100%" class="adminlist">
					<thead>
						<tr>
							<th>LOT NO</th>
							<th>VENDOR LOT NO</th>
							<th width="40%">DESCRIPTION</th>
							<th>PLANT</th>
							<th>TOTAL QTY.</th>								
							<th>BID QTY</th>
							<th>STARTING RATE</th>
							<th>CURRENCY</th>
							<th>PROXY</th>
							<th>PROXY DATE</th>
							<th>REMARKS</th>
						</tr>
					</thead>
					<tbody>
					<?php
					foreach($lots as $singleLot){ ?>
						<tr>
							<td align="center"><?php echo $singleLot['lot_detail']['lotno'];?></td>
							<td align="center"><?php echo $singleLot['lot_detail']['vendorlotno'];?></td>
							<td><?php echo $singleLot['lot_detail']['product'];?></td>
							<td><?php echo $singleLot['lot_detail']['plant'];?></td>
							<td align="center"><?php echo $singleLot['lot_detail']['totalqty']." ".$singleLot['lot_detail']['totunit'];?></td>
							<td align="center"><?php echo $singleLot['lot_detail']['bidqty']." ".$singleLot['lot_detail']['bidunit'];?></td>
							<td align="center"><?php echo $singleLot['lot_detail']['startbid'];?></td>
							<td align="center"><?php echo $singleLot['lot_detail']['currency'];?></td>
							<td align="center"><?php echo ($singleLot['lot_detail']['proxy'] == "Y") ? "Yes" : "No";?></td>
							<td align="center"><?php echo ($singleLot['lot_detail']['proxy'] == "Y") ? $singleLot['lot_detail']['proxystartdate'] :"";?></td>
							<td align="center"><?php echo $singleLot['lot_detail']['remarks'];?></td>
						</tr>
					<?php 
					} ?>					
						<tr><td colspan="12">&nbsp;</td></tr>
						<tr><td colspan="12"><font size="1"><?php echo $current_date; ?></font></td></tr>
					</tbody> 					
				</table>
			</td>
		</tr>
	</table>
</body>
</html>