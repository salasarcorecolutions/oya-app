<html>
	<head>
		<meta http-equiv="Content-Language" content="en-us">
		<link href="<?php echo base_url('css/default.css');?>" type="text/css" rel="stylesheet">

		<title><?php echo PRODUCT ?></title>
		<style>
			table.noborder {width: 100%;border:0px;color: #333;}
			table.adminlist {width: 100%;border-spacing: 1px;background-color: #e7e7e7;color: #333;}
            table.header {width: 100%;border-spacing: 1px;color: $efefef;margin-bottom:5px;}
            table.redstar{color:#FF0000}
            table.adminlist td{ padding: 1px; }
            table.adminlist th { padding: 2px; }
            
            table.adminlist thead th {text-align: center;background: #efefef;color: #333;border-bottom: 1px solid #999;border-left: 1px solid #fff;}
            table.adminlist thead a:hover { text-decoration: none; }
            table.adminlist thead th img { vertical-align: middle; }
            table.adminlist tbody th { font-weight: bold; }
            table.adminlist tbody{min-height:400px}
            table.adminlist tbody tr			{ background-color: #fff;  text-align: left; }
            table.adminlist tbody tr.row1 	{ background: #f9f9f9; border-top: 1px solid black; }
            table.adminlist tbody tr.row0:hover td,
            table.adminlist tbody tr.row1:hover td  { background-color: #ffd ; }
            table.adminlist tbody tr td {  background: #fff; border: 1px solid #fff; }
            table.adminlist tbody tr.row1 td { background: #f9f9f9; border-top: 1px solid black; }
            table.adminlist tbody tr.row0 td { background: #f9f9f9; border-top: 1px solid black; }
            table.adminlist tbody tr.platinum  td{background:#363635;color:#fff}
            table.adminlist tbody tr.gold  td{background:#ddc103;}
            table.adminlist tbody tr.silver  td{background:#dfdfdd;}
            table.adminlist tbody tr.red  td{background:#ff0000;}
            table.adminlist tbody tr.green  td{background:#00ff00;}
		</style>
		<script language="javascript">
			document.onmousedown = disableclick;
			status = "Right Click Disabled";
			function disableclick(event)
			{
				if(event.button==2)
				{
					alert(status);
					return false;
				}
			}
			function noMenu() {
				return false;
			}
			function disableCopyPaste(elm) {
				// Disable cut/copy/paste key events
				elm.onkeydown = interceptKeys
				// Disable right click events
				elm.oncontextmenu = function() {
					return false
				}
			}
			function interceptKeys(evt) {
				evt = evt||window.event // IE support
				var c = evt.keyCode
				var ctrlDown = evt.ctrlKey||evt.metaKey // Mac support
				if (ctrlDown && evt.altKey) return true
				else if (ctrlDown && c==67) return false // c
				else if (ctrlDown && c==86) return false // v
				else if (ctrlDown && c==88) return false // x
				// Otherwise allow
				return true
			}
			
			//courtesy of BoogieJack.com
			function killCopy(e){
				return false
			}
			function reEnable(){
				return true
			}
			document.onselectstart = new Function ("return false")
			if (window.sidebar)
			{
				document.onmousedown = killCopy
				document.onclick = reEnable
			}
		</script>
	</head>
<body oncopy="return false" oncut="return false" onpaste="return false" oncontextmenu="return noMenu();" onkeydown="return disableCopyPaste(this);">
	<table border="0" cellpadding="2" cellspacing="0" width="100%" id="table1" class="noborder">
		<tr>
			<td align="right" width="2px"><img src="<?php echo base_url();?>images/logo.png" />
			</td>
			<td align="left">
				<div class="heading"><h2><?php echo PRODUCT ?></h2></div>
			</td>
		</tr>
	</table>