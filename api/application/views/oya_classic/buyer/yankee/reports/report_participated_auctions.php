<body>
	<table border="0" cellpadding="0" cellspacing="0" width="100%" id="table1" class="adminlist">
		<tr>
			<td align="center"><b><font size="3">AUCTION PARTICIPATION</font></b></td>	
		</tr>
		<tr>
			<td>
				<table border="0" cellspacing="1" width="100%" class="adminlist">
					<thead>
						<tr>
							<th>Sale No</th>
							<th>Auction Type</th>
							<th>Vendor</th>
							<th>Start Date</th>								
							<th>End Date</th>
							<th>Bid Logic</th>
							<th>Particulars</th>
						</tr>
					</thead>
					<tbody>
					<?php
					foreach ($auction_data as $singleAution){ ?>
						<tr>
							<td><?php echo $singleAution['saleno'];?></td>
							<td><?php echo $singleAution['auctiontype'];?></td>
							<td><?php echo $singleAution['vendor_name'];?></td>
							<td align="center"><?php echo $singleAution['start_date']." ".$singleAution['stime'];?></td>
							<td align="center"><?php echo $singleAution['end_date']." ".$singleAution['etime'];?></td>							
							<td align="center">
								Yankee Auction
							</td>
							<td align="center"><?php echo $singleAution['particulars'];?></td>
						</tr>
					<?php 
					} ?>
						<tr><td colspan="8">&nbsp;</td></tr>
						<tr><td colspan="8"><font size="1"><?php echo $current_date; ?></font></td></tr>
					</tbody> 					
				</table>
			</td>
		</tr>
	</table>
</body>
</html>