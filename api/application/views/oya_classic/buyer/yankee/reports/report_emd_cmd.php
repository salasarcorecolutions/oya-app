<body>
	<table border="0" cellpadding="0" cellspacing="0" width="100%" id="table1" class="adminlist">
		<tr>
			<td align="center"><b><font size="3">EMD CMD REPORT</font></b></td>	
		</tr>
		<tr>
			<td>
				<table border="0" cellspacing="1" width="100%" class="adminlist">
					<thead>
						<tr>
							<th rowspan="2">Auction Number</th>
							<th rowspan="2">Vendor</th>
							<th rowspan="2">Start Date</th>
							<th rowspan="2">End Date</th>
							<th>EMD / CMD</th>
							<th>Amount</th>
							<th>Bank Name</th>
							<th>Cheque / DD NO</th>
							<th>Date</th>
						</tr>
					</thead>
					<tbody>
					<?php
					foreach($emd_cmd_data as $singleRow){ ?>
						<tr>
							<td align="center"><?php echo $singleRow['saleno'];?></td>							
							<td><?php echo $singleRow['vendor_name'];?></td>
							<td align="center"><?php echo $singleRow['start_date']." ".$singleRow['stime'];?></td>
							<td align="center"><?php echo $singleRow['end_date']." ".$singleRow['etime'];?></td>
							<?php
							if($singleRow['cmdamt'] > $singleRow['emdamt']){?>
								<td align="center">CMD</td>
								<td align="center"><?php echo $singleRow['cmdamt'];?></td>
								<td align="center"><?php echo $singleRow['cmdbank'];?></td>
								<td align="center"><?php echo $singleRow['cmdddno'];?></td>
								<td align="center"><?php echo $singleRow['cmddate'];?></td>
								<?php
							}else{ ?>
								<td align="center">EMD</td>
								<td align="center"><?php echo $singleRow['emdamt'];?></td>
								<td align="center"><?php echo $singleRow['emdbank'];?></td>
								<td align="center"><?php echo $singleRow['emdddno'];?></td>
								<td align="center"><?php echo $singleRow['emddate'];?></td>
								<?php
							}
							?>	
						</tr>
					<?php 
					} ?>	
						<tr><td colspan="12">&nbsp;</td></tr>
						<tr><td colspan="12"><font size="1"><?php echo $current_date; ?></font></td></tr>
					</tbody> 					
				</table>
			</td>
		</tr>
	</table>
</body>
</html>