<body>
	<table border="0" cellpadding="0" cellspacing="0" width="100%" id="table1" class="adminlist">
		<tr>
			<td align="center"><b><font size="3">LOT WISE L1 RATE FOR SALE/AUCTION NO  <?php echo $auction_data['saleno']; ?></font></b></td>	
		</tr>
		<tr>
			<td>
				<table border="0" cellspacing="1" width="100%" id="table1" class="adminlist">
					<tr>
						<td align="center" nowrap colspan="4" ><b><font size="2">AUCTION DETAILS</font></b></td>
					</tr>
					<tr>
						<td width="30%" align="right" nowrap ><b>E-Auction Sale No</b></td>
						<td><?php echo $auction_data['saleno']; ?></td>
						<td width="30%" align="right"><b>Auction Type</b></td>
						<td><?php echo $auction_data['auctiontype']; ?></td>
					</tr>
					<tr>
						<td width="30%" align="right"><b>Message</b></td>
						<td><?php echo $auction_data['auction_msg']; ?></td>
						<td width="30%" align="right"><b>Auction Start Date</b></td>
						<td><?php echo $auction_data['start_date']; ?></td>
					</tr>
					<tr>
						<td width="30%" align="right" nowrap><b>Auction Open Up to</b></td>
						<td><?php echo $auction_data['end_date']; ?></td>
						<td width="30%" align="right"><b>Company</b></td>
						<td><?php echo $auction_data['vendor_name'];?></td>
					</tr>
					<tr>
						<td width="30%" align="right"><b>Particulars</b></td>
						<td colspan="3"><?php echo $auction_data['particulars']; ?></td>
					</tr>		
				</table>

			</td>
		</tr>
		<tr>
			<td bgcolor="#FFF8F0" align="center"><b><font size="2">BID DETAILS</font></b></td>
		</tr>		
		<tr>
			<td>
				<table border="0" cellspacing="1" width="100%" class="adminlist">
					<thead>
						<tr>
							<th>LOT NO</th>
							<th>VENDOR LOT NO</th>
							<th width="40%">DESCRIPTION</th>
							<th>PLANT</th>
							<th>TOTAL QTY.</th>							
							<th>BID QTY</th>
							<th>STARTING RATE</th>
							<th>MY RATE</th>
							<?php if($auction_data['bid_logic'] == "D"){ ?>
								<th>L1 RATE</th><?php
							}?>
							<th>MY POSITION</th>
						</tr>
					</thead>
					<tbody>
					<?php
					foreach($lots as $singleLot){ 
						if(!empty($singleLot['my_bid'])){?>
						<tr>
							<td align="center"><?php echo $singleLot['lot_detail']['lotno'];?></td>
							<td align="center"><?php echo $singleLot['lot_detail']['vendorlotno'];?></td>
							<td><?php echo $singleLot['lot_detail']['product'];?></td>
							<td><?php echo $singleLot['lot_detail']['plant'];?></td>
							<td align="center"><?php echo $singleLot['lot_detail']['totalqty']." ".$singleLot['lot_detail']['totunit'];?></td>
							<td align="center"><?php echo $singleLot['lot_detail']['bidqty']." ".$singleLot['lot_detail']['bidunit'];?></td>
							<td align="center"><?php echo $singleLot['lot_detail']['startbid'];?></td>
							<td align="center"><?php echo $singleLot['my_bid'];?></td>
							<?php 
							if($auction_data['bid_logic'] == "D"){
								if($singleLot['l1_bid'] == $singleLot['my_bid']){
									?><td align="center"><b><font color="green"><?php echo $singleLot['l1_bid'] ;?></font></b></td><?php
								}else{
									?><td align="center"><b><font color="red"><?php echo $singleLot['l1_bid'] ;?></font></b></td><?php
								}
							}
							?>
							<td align="center"><?php echo $singleLot['my_position'];?></td>
						</tr>
					<?php 
						}
					} ?>					
						<?php if ($auction_data['bid_logic'] == "D"){ ?>
							<tr><td colspan="12">&nbsp;</td></tr>
							<tr><td colspan="12"><font size="1"><?php echo $current_date; ?></font></td></tr>
						<?php } else { ?>
							<tr><td colspan="11">&nbsp;</td></tr>
							<tr><td colspan="11"><font size="1"><?php echo $current_date; ?></font></td></tr>
						<?php } ?>
					</tbody> 					
				</table>
			</td>
		</tr>
	</table>
</body>
</html>