
	<table border="0" cellpadding="0" cellspacing="0" width="100%" id="table1" class="">
		<tr>
			<td align="center"><b><font size="3">BID SUMMARY REPORT FOR SALE/AUCTION NO  <?php echo $auction_data['ynk_saleno']; ?></font></b></td>	
		</tr>
		<tr>
			<td>
				<table border="0" cellspacing="1" width="100%" id="table1" class="">
					<tr>
						<td align="center" nowrap colspan="4" ><b><font size="2">AUCTION DETAILS</font></b></td>
					</tr>
					<tr>
						<td width="30%" align="right" nowrap ><b>E-Auction Sale No</b></td>
						<td><?php echo $auction_data['ynk_saleno']; ?></td>
						<td width="30%" align="right"><b>Auction Type</b></td>
						<td><?php echo $auction_data['auctiontype']; ?></td>
					</tr>
					<tr>
						<td width="30%" align="right"><b>Message</b></td>
						<td><?php echo $auction_data['auction_msg']; ?></td>
						<td width="30%" align="right"><b>Auction Start Date</b></td>
						<td><?php echo $auction_data['start_date']; ?></td>
					</tr>
					<tr>
						<td width="30%" align="right" nowrap><b>Auction Open Up to</b></td>
						<td><?php echo $auction_data['end_date']; ?></td>
						<td width="30%" align="right"><b>Company</b></td>
						<td><?php echo $auction_data['vendor_name'];?></td>
					</tr>
					<tr>
						<td width="30%" align="right" nowrap><b>Auction Currency</b></td>
						<td><?php echo $lots[1]['bid_detail'][0]['primary_currency']; ?></td>
						<td width="30%" align="right"><b>Prefered Currency</b></td>
						<td><?php echo $lots[1]['bid_detail'][0]['prefered_bid_currency'];?></td>
					</tr>
					<tr>
						<td width="30%" align="right"><b>Particulars</b></td>
						<td colspan="3"><?php echo $auction_data['particulars']; ?></td>
					</tr>
				</table>

			</td>
		</tr>
		<tr>
			<td bgcolor="#FFF8F0" align="center"><b><font size="2">BID SUMMARY</font></b></td>
		</tr>
		<tr>
			<td bgcolor="#FFF8F0" align="center">
				<table border="0" cellspacing="1" width="100%" class="adminlist">
					<thead>
						<tr>
							<th>Lot No</th>
							<th>Vendor Lot No</th>
							<th width="40%">Product</th>
							<th>Plant</th>
							<th>Start Time</th>
							<th>End Time</th>
							<th>Total Qty</th>
							<th>Bid Qty</th>
							<th>My Bid</th>
							<th>My Bid Prefered Currency</th>
							<th>Alloted Qty</th>
							<?php if ($auction_data['bid_logic'] == 'F')
							{?>
							<th>H1 Bid</th><?php 
							}
							else
							{
							?>
							<th>L1 Bid</th>
							<?php
							}
							?>
							<th>No Of My Bids</th>
						</tr>
					</thead>
					<tbody>
				<?php 
				foreach($lots as $singleLot)
				{
					if ( ! empty($singleLot['bid_detail'][0]['amount']))
					{?>
					<tr>
						<td align="center"><?php echo $singleLot['lot_detail']['ynk_lotno']?></td>
						<td align="center"><?php echo $singleLot['lot_detail']['vendorlotno']?></td>
						<td><?php echo $singleLot['lot_detail']['product']?></td>
						<td><?php echo $singleLot['lot_detail']['plant']?></td>
						<td align="center"><?php echo $singleLot['lot_detail']['lot_open_date']?></td>
						<td align="center"><?php echo $singleLot['lot_detail']['lot_close_date']?></td>
						<td align="center"><?php echo $singleLot['lot_detail']['totalqty']." ".$singleLot['lot_detail']['totunit']?></td>
						<td align="center"><?php echo $singleLot['lot_detail']['bidqty']." ".$singleLot['lot_detail']['bidunit']?></td>
						<td align="center"><?php echo $singleLot['bid_detail'][0]['amount']?></td>
						<td align="center"><?php echo $singleLot['bid_detail'][0]['prefered_bid_amount']?></td>
						<?php
						if ($singleLot['l1_bid'] == $singleLot['bid_detail'][0]['amount'])
						{
							?>
							<td align="center"><b><font color="green"><?php echo $singleLot['bid_qty'] ." ".$singleLot['lot_detail']['bidunit'] ;?></font></b></td>
							<td align="center"><b><font color="green"><?php echo $singleLot['l1_bid'] ;?></font></b></td>
							<?php
						}
						else
						{
							?><td align="center">&nbsp;</td><?php
						}
							
						?>						
						<td align="center"><?php echo sizeOf($singleLot['bid_detail']);?></td>
					</tr>
					<?php 
					}
				} ?>
					<tbody>
				</table>
			</td>
		</tr>

		<tr><td colspan="8">&nbsp;</td></tr>
		<tr><td colspan="8"><font size="1"><?php echo $current_date; ?></font></td></tr>
		</table>
</body>
