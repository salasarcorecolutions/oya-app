<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<!-- Fav and touch icons -->
<link rel="apple-touch-icon-precomposed" sizes="144x144" href="<?php echo base_url(ciHtmlTheme.'/assets/ico/apple-touch-icon-144.png')?>">
<link rel="apple-touch-icon-precomposed" sizes="114x114" href="<?php echo base_url(ciHtmlTheme.'/assets/ico/apple-touch-icon-114.png')?>">
<link rel="apple-touch-icon-precomposed" sizes="72x72" href="<?php echo base_url(ciHtmlTheme.'/assets/ico/apple-touch-icon-72.png')?>">
<link rel="apple-touch-icon-precomposed" sizes="57x57" href="<?php echo base_url(ciHtmlTheme.'/assets/ico/apple-touch-icon-57.png')?>">
<link rel="shortcut icon" href="<?php echo base_url(ciHtmlTheme.'/assets/ico/favicon.png')?>">
<title>PROFILE |</title>
<!-- Bootstrap core CSS -->
<link href="<?php echo base_url(ciHtmlTheme.'/assets/bootstrap/css/bootstrap.min.css')?>" rel="stylesheet">
<!-- Custom styles for this template -->
<link href="<?php echo base_url(ciHtmlTheme.'/assets/css/style.css')?>" rel="stylesheet">
<link href="<?php echo base_url(ciHtmlTheme.'/assets/css/custom.css')?>" rel="stylesheet">
<!-- styles needed for carousel slider -->


<script>
    paceOptions = {
      elements: true
    };
</script>
<script src="<?php echo base_url(ciHtmlTheme.'/assets/js/pace.min.js')?>"></script>

 </head>
<body>

<div id="wrapper">
<!-- /.header -->
 <?php $this->load->view(ciHtmlTheme.'/buyer/buyer_header');?>
  <!-- /.header -->
  <div class="main-container">
   <div class="container-fluid">
      <div class="row">
        <div class="col-sm-3 page-sidebar">
         <?php $this->load->view(ciHtmlTheme.'/buyer/left_navigation');?>
        </div>
        <!--/.page-sidebar-->
        
        <div class="col-sm-9 page-content">
          <div class="inner-box">
          <div class="row">
            <div class="col-md-5 col-xs-4 col-xxs-12">
              <h3 class="no-padding text-center-480 useradmin"><a href="<?php echo base_url('buyer/profile')?>">
              <img class="userImg" src="<?php echo base_url('images/user-icon.png')?>" alt="<?php echo $this->session->userdata('conperson');?>" align="left"> <?php echo $this->session->userdata('conperson');?> </a> 
              	<br /><span class="page-sub-header-sub small"><?php echo $this->session->userdata('compname');?></span>
              </h3>
            	
            </div>
            <div class="col-md-7 col-xs-8 col-xxs-12">
              <div class="header-data text-center-xs"> 
                
                <!-- Traffic data -->
                <!--<div class="hdata">
                  <div class="mcol-left"> 
                    <!-- Icon with red background --> 
                   <!-- <i class="fa fa-eye ln-shadow"></i> </div>
                  <div class="mcol-right"> -->
                    <!-- Number of visitors -->
                    <!--<p><a href="#">7000</a> <em>visits</em></p>
                  </div>
                  <div class="clearfix"></div>
                </div>-->
                
                <!-- revenue data -->
               <!-- <div class="hdata">
                  <div class="mcol-left"> -->
                    <!-- Icon with green background --> 
                   <!-- <i class="icon-th-thumb ln-shadow"></i> </div>
                  <div class="mcol-right"> -->
                    <!-- Number of visitors -->
                   <!-- <p><a href="#">12</a><em>Ads</em></p>
                  </div>
                  <div class="clearfix"></div>
                </div>-->
                
                <!-- revenue data -->
               <!-- <div class="hdata">
                  <div class="mcol-left"> -->
                    <!-- Icon with blue background --> 
                   <!-- <i class="fa fa-user ln-shadow"></i> </div>
                  <div class="mcol-right"> -->
                    <!-- Number of visitors -->
                   <!-- <p><a href="#">18</a> <em>Favorites </em></p>
                  </div>
                  <div class="clearfix"></div>
                </div>
              </div>-->
            </div>
          </div>
          </div>
          
          <div class="inner-box">
            <div class="welcome-msg">
              <h3 class="page-sub-header2 clearfix no-padding">Hello <?php echo $this->session->userdata('conperson');?></h3>
              <span class="page-sub-header-sub small">You last logged in at: 01-01-2014 12:40 AM [UK time (GMT + 6:00hrs)]</span> 
              <button class="btn btn-danger" id="edit_btn">Edit Profile</button>
              <button class="btn btn-info" onclick="location.href='profile/change_email';">Change Email</button>
              <button class="btn btn-default" onclick=coll();>Change Password</button>
            </div>
            <div id="accordion" class="panel-group">
              <div class="panel panel-default">
                <div class="panel-heading">
                  <h4 class="panel-title"> <a href="#collapseB1"  data-toggle="collapse" id="cb1"> My Details </a> </h4>
                </div>
                <div class="panel-collapse collapse in" id="collapseB1">
                  <div class="panel-body">
                    <form class="form-horizontal" id="formEdit" method="post">
                      <?php foreach($profile_details as $pdet) 
                      {
                        $currency = $pdet['currency'];
                        $type_of_comp = $pdet['type_of_comp'];
                        $nature_of_activity = $pdet['nature_of_activity'];
                        $interested_region = $pdet['interested_in_region']; 
                        $pan_no = $pdet['panno'];
                        $id = $pdet['id'];
                      ?>
                        
                        <div class="form-group">
                          <label  class="col-sm-3 control-label">Company name</label>
                          <div class="col-sm-9">
                            <input type="text" class="form-control frm_edit" name="cname" placeholder="Doe" value="<?php echo $pdet['compname'];?>">
                          </div>
                        </div>

                        <div class="form-group">
                          <label  class="col-sm-3 control-label">Company Address</label>
                          <div class="col-sm-9">
                            <input type="text" class="form-control frm_edit" name="addr" placeholder=".." value="<?php echo $pdet['adddress'];?>">
                          </div>
                        </div>

                        <div class="form-group">
                          <label  class="col-sm-3 control-label">Country</label>
                          <div class="col-sm-9">
                            <select class="form-control frm_edit" id="b_country" name="b_country" onchange="fetch_timezone(this.id);fetch_state(this.id)" attr="bidder">
                              <option value="">Select Country</option>
                              <?php
                              if (! empty($countries)) {
                                foreach ($countries as $country) {
                                  ?>
                                  <option value="<?php echo $country['id'];?>"  <?php echo ($pdet['country'] == $country['id'])? 'selected':''?>  ><?php echo $country['name'];?></option>
                                <?php
                                }
                              }
                              ?>
                            </select>
                          </div>
                          <?php //if($pdet['country'] == $country['name']) { echo "selected"}?>
                           
                        </div>

                        <div class="form-group">
												<label class="col-sm-3 control-label">State </label>
												<div class="col-md-9">
													<select class="add_select2 form-control frm_edit" id="b_state" name="b_state" attr= "bidder" onchange="fetch_city(this.id)" >
																	<option value="">Select State</option>
													</select>
												
												</div>
											</div>

                        <div class="form-group">
                          <label  class="col-sm-3 control-label">City</label>
                          <div class="col-sm-9">                               
                            <select class="add_select2 form-control frm_edit" name="b_city" id="b_city" >
                              <option value="">Select City</option>
                              <option></option>
                            </select> 
                          </div>
                        </div>

                        <div class="form-group">
                          <label  class="col-sm-3 control-label">TimeZone</label>
                          <div class="col-sm-9">
                            <select class="user_tz form-control br-5 frm_edit" id="user_tz" name="user_tz" require>
                              <option value=""> --Select Your Timezone--</option>
                             
                            </select> 
                          </div>
                        </div>

                        <div class="form-group">
                          <label  class="col-sm-3 control-label">Pin</label>
                          <div class="col-sm-9">
                            <input type="text" class="form-control frm_edit" name="pin" placeholder=".." value="<?php echo $pdet['pin'];?>">
                          </div>
                        </div>
                        
                        <div class="form-group">
                          <label  class="col-sm-3 control-label">Your Name</label>
                          <div class="col-sm-9">
                            <input type="text" class="form-control frm_edit" name="pname" placeholder="John" value="<?php echo $pdet['conperson'];?>">
                          </div>
                        </div>

                        <div class="form-group">
                          <label for="Phone" class="col-sm-3 control-label">Mobile No.</label>
                          <div class="col-sm-9">
                            <input type="text" class="form-control frm_edit" name="mob" id="Phone" placeholder="880 124 9820" value="<?php echo $pdet['mob'];?>">
                          </div>
                        </div> 
                       
                        <div class="form-group">
                          <label  class="col-sm-3 control-label">Email</label>
                          <div class="col-sm-9">
                            <input type="email" class="form-control frm_not_edit"  placeholder="john.deo@example.com" value="<?php echo $pdet['email'];?>">
                          </div>
                        </div>
                        
                        
                      <?php } ?>
                      <div class="form-group hide"> <!-- remove it if dont need this part -->
                        <label  class="col-sm-3 control-label">Facebook account map</label>
                        <div class="col-sm-9">
                          <div class="form-control"> <a class="link" href="fb.com">Jhone.doe</a> <a class=""> <i class="fa fa-minus-circle"></i></a> </div>
                        </div>
                      </div>
                      <div class="form-group">
                        <div class="col-sm-offset-3 col-sm-9"> </div>
                      </div>
                      <input type="hidden" name='bid' value="<?php echo $pdet['id'];?>">
                      <input type="hidden" name='type' value="prfl">
                      <div class="form-group " id="update_btn">
                        <div class="col-sm-offset-3 col-sm-9">
                          <button id="btnUpdate" class="btn btn-primary">Update</button>
                        </div>
                      </div>
                    </form>
                  </div>
                </div>
              </div>
              <div class="panel panel-default">
                <div class="panel-heading">
                  <h4 class="panel-title"> <a href="#collapseB2"  data-toggle="collapse" id="cb2"> Change Password </a> </h4>
                </div>
                <div class="panel-collapse collapse" id="collapseB2">
                  <div class="panel-body">
                    <form class="form-horizontal" id="frm_pwd" method="post">
                      <div class="form-group">
                        <label  class="col-sm-3 control-label">New Password</label>
                        <div class="col-sm-9">
                          <input type="password" class="form-control" name="cpwd" id="chnge_pwd" placeholder="">
                        </div>
                      </div>
                      <div class="form-group">
                        <label  class="col-sm-3 control-label">Confirm Password</label>
                        <div class="col-sm-9">
                          <input type="password" class="form-control" name="cpwd_conf" placeholder="">
                        </div>
                      </div>
                      <div class="form-group">
                        <div class="col-sm-offset-3 col-sm-9">
                          <button class="btn btn-default" id="uppwd_btn">Update</button>
                        </div>
                      </div>
                    </form>
                  </div>
                </div>
              </div>
              <div class="panel panel-default">
                <div class="panel-heading">
                  <h4 class="panel-title"> <a href="#collapseB3"  data-toggle="collapse" id="cb3"> Preferences </a> </h4>
                </div>
                <div class="panel-collapse collapse" id="collapseB3">
                  <div class="panel-body"> 
                    <form class="form-horizontal" id="frm_pref" method="post">
                      <div class="form-group">
                        <label  class="col-sm-3 control-label">Currency</label>
                        <div class="col-sm-9">
                          <input type="text" class="form-control" name="currency" placeholder="INR" value="<?php echo $currency;?>">
                        </div>
                      </div>
                      <div class="form-group">
                        <label  class="col-sm-3 control-label">Type Of company</label>
                        <div class="col-sm-9">
                          <input type="text" class="form-control" name="typ_of_cmp"   value="<?php echo $type_of_comp;?>">
                        </div>
                      </div> 
                      <div class="form-group">
                        <label  class="col-sm-3 control-label">Nature of Activity</label>
                        <div class="col-sm-9">
                          <input type="text" class="form-control" name="nature_of_activity"  value="<?php echo $nature_of_activity;?>">
                        </div>
                      </div>
                      <div class="form-group">
                        <label  class="col-sm-3 control-label">Intrested in Region</label>
                        <div class="col-sm-9">
                          <input type="text" class="form-control" name="interested_in_region"  value="<?php echo $interested_region;?>">
                        </div>
                      </div>
                      <input type="hidden" name='bid' value="<?php echo $id;?>">
                      <input type="hidden" name='type' value="pref">
                      <div class="form-group">
                        <label  class="col-sm-3 control-label">Pan No</label>
                        <div class="col-sm-9">
                          <input type="text" class="form-control" name="pan_no"  value="<?php echo $pan_no;?>">
                        </div>
                      </div>
                      <div class="form-group " >
                        <div class="col-sm-offset-3 col-sm-9">
                          <button id="btnUpdatepref" class="btn btn-primary">Update</button>
                        </div>
                      </div>

                    </form>  
                  </div>
                </div>
              </div>
              <div class="panel panel-default">
                <div class="panel-heading">
                  <h4 class="panel-title"> <a href="#collapseB4"  data-toggle="collapse" id="cb3"> Upload Logo </a> </h4>
                </div>
                <div class="panel-collapse collapse" id="collapseB4">
                  <div class="panel-body"> 
                  <form id="frm">
                  <div class="col-lg-12">
                    <div class="col-lg-2">
                      <h5 style="margin:0px;">Select image to upload:</h5>
                    </div>
                    <div class="col-lg-2">
                      <input class='form-control' type="file" name="upload_file" id="fileToUpload">
                    </div>
                    <div class="col-lg-2">
                      <button class="btn btn-success" type="submit" value="Upload Image" name="submit">Upload</button>
                    </div>
                  </div>
                </form> 
                  </div>
                </div>
              </div>
            </div>
            <!--/.row-box End--> 
            
          </div>
        </div>
        <!--/.page-content--> 
      </div>
      <!--/.row--> 
    </div>
    <!--/.container--> 
  </div>
  <!-- /.main-container -->
  
  
  <!-- /.footer --> 
  <?php $this->load->view(ciHtmlTheme.'/includes/footer');?>
 
</div>
<!-- /.wrapper --> 

<!-- Le javascript
================================================== --> 
<!-- Placed at the end of the document so the pages load faster --> 
<script src="<?php echo base_url(ciHtmlTheme.'/assets/js/jquery.min.js')?>"> </script>
<script src="<?php echo base_url(ciHtmlTheme.'/assets/bootstrap/js/bootstrap.min.js')?>"></script> 
<!-- include carousel slider plugin  --> 

<!-- include equal height plugin  --> 
<script src="<?php echo base_url(ciHtmlTheme.'/assets/js/jquery.matchHeight-min.js')?>"></script> 
<!-- include jquery list shorting plugin plugin  --> 
<script src="<?php echo base_url(ciHtmlTheme.'/assets/js/hideMaxListItem.js')?>"></script> 
<!-- include jquery.fs plugin for custom scroller and selecter  --> 
<script src="<?php echo base_url(ciHtmlTheme.'/assets/plugins/jquery.fs.scroller/jquery.fs.scroller.js')?>"></script>
<script src="<?php echo base_url(ciHtmlTheme.'/assets/plugins/jquery.fs.selecter/jquery.fs.selecter.js')?>"></script>
<script src="<?php echo base_url(ciHtmlTheme.'/assets/js/jquery.validate.min.js')?>"></script>
<script src="<?php echo base_url(ciHtmlTheme.'/assets/js/jquery.form.min.js')?>"></script>
<!-- include custom script for site  --> 
<!-- include jquery autocomplete plugin  -->

<script	src="<?php echo base_url(ciHtmlTheme.'/assets/js/jquery.validate.min.js')?>"></script>

<script>
$(document).ready(function(){
  fetch_state('b_country','<?php echo $pdet['state']?>');
  fetch_city('b_state','<?php echo $pdet['city']?>');
  fetch_timezone('b_country');


});
//fetch_city('<?php echo $pdet['country']?>','<?php echo $pdet['state']?>');

  $('.frm_edit').attr('disabled', true); 
  $('.frm_not_edit').attr('disabled', true); 
  $('#update_btn').hide();
  <?php 
  if ( ! empty($change_password))
  {
  ?>
coll();
  <?php
  }
  
  ?>
  // disable-enable edit form on click of edit button
  $( "#edit_btn" ).click(function() {
    if($('.frm_edit').attr('disabled')){
      $('.frm_edit').attr('disabled', false);
     
      $('#update_btn').show();
    }else {
      $('.frm_edit').attr('disabled', true);
      $('#update_btn').hide();
    }
    
  });

  // validations for data updation 
  $("#btnUpdate").click(function(e){
    e.preventDefault();
    var form=$("#formEdit");
    form.validate({
        ignore:[],
        rules: { 
          pname: {required : true,minlength:3},
          addr: {required : true,minlength:4},
          cname: {required : true ,minlength:3}, 
          mob: {required : true,maxlength:10,minlength:10,number:true}
        },
      messages: {	
        pname:"Please Enter Name",   
        addr:"Please Enter Address", 
        cname: "Please Enter company Name"
        },
        
      });

    if(form.valid())
      $( "#formEdit" ).submit();  
  });

  // submit updated data after validate fields
  $("#formEdit").on('submit',(function(e) {
	
    e.preventDefault(); 
    var $btn = $("#btnUpdate").button('loading');
    $.ajax({
      url: "<?php echo base_url();?>buyer/profile/update",
      type: "POST",
      data:  new FormData(this),
      contentType: false,
      cache: false,
      processData:false,
      dataType:'json',
      success: function(response)
      {
        if(response.success)
        {  
          setInterval('location.reload()', 500);  
        }
        else
        {
          alert(response.message);
          $btn.button('reset');
        }
          
      },
      error: function() 
      {
        alert(response.message);
        $btn.button('reset');
      } 	
    });
  }));


//-------update password------//
  function coll(){ 
   
    if($( "#collapseB1" ).hasClass( "in" )){
      // collpase the mydetails' panel
      $("#cb1").attr("aria-expanded","false");
      $("#cb1").addClass("collapsed ");
      $("#collapseB1").attr("aria-expanded","false");
      $( "#collapseB1" ).removeClass( "in" );
    }
    
    // class "in" the setting panel 
    $("#cb1").attr("aria-expanded","true");
    $("#collapseB2").attr("aria-expanded","true");
    $( "#collapseB2" ).addClass( "in" );
      
  }

  
  // validations for password data updation 
  $("#uppwd_btn").click(function(e){
    e.preventDefault();
    var form=$("#frm_pwd");
    form.validate({
        ignore:[],
        rules: { 
          cpwd: {required : true,minlength:3}, 
          cpwd_conf: {equalTo: "#chnge_pwd"},
        },
      messages: {	
        cpwd:"Please Enter Password"
        },
        
      });

    if(form.valid())
      $( "#frm_pwd" ).submit();  
  });

  // update password after validate fields
  $("#frm_pwd").on('submit',(function(e) {
	
    e.preventDefault(); 
    var $btn = $("#uppwd_btn").button('loading');
    $.ajax({
      url: "<?php echo base_url();?>buyer/profile/update_pwd",
      type: "POST",
      data:  new FormData(this),
      contentType: false,
      cache: false,
      processData:false,
      dataType:'json',
      success: function(data)
      {
        if(data.success)
        {  
          location.href="<?php echo base_url('user/login')?>";
        }
        else
        {
          alert(data.message);
          $btn.button('reset');
        }
          
      },
      error: function() 
      {
        alert(data.message);
        $btn.button('reset');
      } 	
    });
  }));

  // validations for Prefrences data updation 
  $("#btnUpdatepref").click(function(e){
    e.preventDefault();
    var form=$("#frm_pref");
    form.validate({
        ignore:[],
        rules: { 
          currency: {required : true}, 
          typ_of_cmp: {required : true}, 
          nature_of_activity: {required : true},
          interested_in_region: {required : true},
          pan_no : {required:true ,minlength:10}
        },
      messages: {	
          currency: "Please Enter Currency", 
          pan_no : "Please Enter Pan No"
        },
        
      });

    if(form.valid())
      $( "#frm_pref" ).submit();  
  });


  // update preference data after validate fields
  $("#frm_pref").on('submit',(function(e) {
	
    e.preventDefault(); 
    var $btn = $("#btnUpdatepref").button('loading');
    $.ajax({
      url: "<?php echo base_url();?>buyer/profile/update",
      type: "POST",
      data:  new FormData(this),
      contentType: false,
      cache: false,
      processData:false,
      dataType:'json',
      success: function(data)
      {
        if(data.success)
        {  
          location.reload();
        }
        else
        {
          alert(data.message);
          $btn.button('reset');
        }
          
      },
      error: function() 
      {
        alert(data.message);
        $btn.button('reset');
      } 	
    });
  }));

  function fetch_timezone(id) {
	
    var country_id = $("#"+id).val(); 
    if(country_id){
      jQuery.ajax({
        url: '<?php echo base_url('user/get_time_zones')?>',
        type:"post",
        async: false,
        dataType:"json",
        data: {'country_name':country_id},
        success: function (resObj){
          if(resObj)
          {
            var i = 0;
            var html ='';
            $.each(resObj, function( index, value ) {
                html+='<option value="'+value.timezone+'"> ('+value.gmt_offset+') &nbsp '+value.time_zone_description+' , &nbsp'+ value.country_name+'</option>';
              i++;
            }); 
            $("#user_tz").html(html);
           
            
          }
        },
        error: function(){
          
        }
      });

    } else {
      
    } 
  }

 

$("#b_state").on("change",function(){

	
	if($("#b_state").val()=="0")
	{
		$("#StateOther").addClass("show");
		$("#CityOther").addClass("show");
		$("#b_city").empty();
	}
	else
	{
		$("#StateOther").addClass("hide");
  }
});
	
$("#b_city").on("change",function(){
 
	if($("#b_city").val()=="0")
	{
		$("#CityOther").addClass("show");
	}
	else
	{
		$("#CityOther").addClass("hide");
  }
});


function fetch_state(id,state='')
{
  
  var country_id = $("#"+id).val();
	var attr = $("#"+id).attr('attr');
	if(country_id){
		jQuery.ajax({
			url: '<?php echo base_url('user/get_state_by_country_id')?>',
			type:"post",
			async: false,
			data: {'country_id':country_id,state_id:state},
			dataType:'json',
			success: function (resObj){
				
				var html ='<option value="">Select State</option>';
				$.each(resObj,function(i,v)
				{
          if (state == v.id)
          {
            html+='<option value="'+v.id+'" selected>'+v.state+'</option>';
          }
          else
          {
            html+='<option value="'+v.id+'">'+v.state+'</option>';
          }
				
				});
				
				$("#b_state").html(html);
				
			},
			error: function(){
				$("#b_state").html('<option value="">Select State</option>');
				
			}
		});

	} else {
		$("#b_state").html('<option value="">Select State</option>');
		
	}
}

 function fetch_city(id,city='',state='')
{
	if (state){
		var state_id = state;
	} else {
		var state_id = $("#"+id).val();
	}
	var attr = $("#"+id).attr('attr');
	if(state_id !== ""){
		 jQuery.ajax({
			url: '<?php echo base_url('user/get_cities_by_state_id');?>',
			type:"post",
			async: false,
			data: {'state_id':state_id,'city_id':city},
			dataType:'json',
			success: function (resObj) {

				var html ='<option value="">Select City</option>';
				$.each(resObj,function(i,v)
				{
          if (city == v.id)
          {
            html+='<option value="'+v.id+'" selected>'+v.name+'</option>';
          }
          else
          {
            html+='<option value="'+v.id+'">'+v.name+'</option>';
          }
				
				});
				
				$("#b_city").html(html);
				

			},
			error: function(){
				$("#b_city").html('<option value="">Select City</option>');
				
			}
		});
	} else {
		$("#b_city").html('<option value="">Select City</option>');
		
	}
}
$("#frm").validate({
		rules:{
			upload_file:{required:true},
		},
		messages:{
			upload_file:{required:"<strong class='text-danger'>Upload file to continue</strong>"},
		},
		submitHandler: function(form)
		{
			$(form).ajaxSubmit({
				url: '<?php echo base_url('buyer/profile/upload_buyer_logo');?>',
				type:"post",
				dataType: 'json',
				success: function (response) {
					alert(response.message)
					if (response.status == 'success'){
						setTimeout(function(){ location.reload(1); }, 3000);
					}
				},
				error: function(e){
					alert('Problem in updating data please try again');
				}
			});
		}
	});
</script>
</body>
</html>
