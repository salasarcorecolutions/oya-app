<table class="table table-bordered">
	<tr>
		<td>VENDOR:</td>
		<td><?php echo $auction_details['vendor_name']?></td>
	</tr>
	<tr>
		<td>SALE NO:</td>
		<td><?php echo $auction_details['ynk_saleno']?></td>
	</tr>
	<tr>
		<td>PARTICULARS:</td>
		<td><?php echo $auction_details['particulars']?></td>
	</tr>
	<tr>
		<td>START DATE:</td>
		<td><?php echo $auction_details['start_date'].' '.$auction_details['stime']?></td>
	</tr>
	<tr>
		<td>END DATE:</td>
		<td><?php echo $auction_details['end_date'].' '.$auction_details['etime']?></td>
	</tr>


</table>

<table class="table table-bordered">
<thead>
    <tr>
        <th>SR No</th>
        <th>LOT NO</th>
        <th>PRODUCT</th>
        <th>TOTAL QTY.</th>
        <th>QTY ALLOTED</th>
        <th>RATE</th>
        <th nowrap>AMOUNT</th>
    </tr>
</thead>
<tbody>
    <?php
    
    $total_expense=0;
    if ( ! empty($bid_limit))
        {
            $i = 0;
            foreach($bid_limit as $val)
            {
                $i++;
    ?>
                <tr>
                    <td align="center"><?php echo $i?></td>
                    <td align="center"><?php echo $val['lot_id']?></td>
                    <td align="center"><?php echo nl2br($val['product'])?></td>
                    <td align="center"><?php echo $val['total_qty'].' '.$val['total_unit']?></td>
                    <td align="center"><?php echo $val['sub_alloted_qty'].' '.$val['total_unit']?></td>
                    <td align="center"><?php echo number_format($val['bid_amt'],2)?> <?php echo $val['prefered_currency_name']?>/<?php echo $val['lot_bidqty']==1?' ':$val['lot_bidqty'];?><?php echo $val['total_unit']?></td>
                    <td align="right"><?php echo number_format($val['individual_lot_fund'],2)?> <?php echo $val['prefered_currency_name']?></td>

                </tr>
    <?php
    $total_expense=$total_expense+$val['individual_lot_fund'];
            }
            ?>
             <tr>
                <td colspan="6" align="right">TOTAL FUND USED :</td>
                <td align="right"><?= number_format($total_expense,2)?> <?php echo $val['prefered_currency_name']?></td>
            </tr>
             <tr>
                <td colspan="6" align="right">TOTAL LIMIT :</td>
                <td align="right"><?php echo $bid_limit_status['bidlimited']==1?number_format($bid_limit_status['totallimit'],2). ' '. $val['prefered_currency_name'] : 'UNLIMITED'?></td>
            </tr>
            <?php if($bid_limit_status['bidlimited']==1) {?>
             <tr>
                <td colspan="6" align="right">AVAILABLE BALANCE:</td>
                <td align="right" nowrap><?=number_format($bid_limit_status['totallimit']-$total_expense,2). ' '. $val['prefered_currency_name']; ?></td>
            </tr>
            
            <?php 
            }
        }
        else
        {
    ?>
            <tr>
                <td colspan="7" align="center">No Limit Found</td>
            </tr>

    <?php
        }

    ?>

</tbody>
	
</table>