<html>
<head>
<title><?php echo @$title;?></title>

<style type="text/css">
body{font-size:12px}
h4{margin:0px 0px 0px 5px}
hr{border:1px solid #efefef}
table {
width:100%;
    border-spacing: 0;
    border-collapse: collapse;
    margin-bottom:5px;
}
.table-bordered > tbody > tr > td, .table-bordered > tbody > tr > th, .table-bordered > tfoot > tr > td, .table-bordered > tfoot > tr > th, .table-bordered > thead > tr > td, .table-bordered > thead > tr > th {
    border: 1px solid #ddd;
}
.table > tbody > tr > td, .table > tbody > tr > th, .table > tfoot > tr > td, .table > tfoot > tr > th, .table > thead > tr > td, .table > thead > tr > th {
    padding: 4px;
    vertical-align: top;
    border-top: 1px solid #ddd;
}
</style>

</head>
<body>
<table style="width:auto">

<tr>
<td><img src="<?php echo base_url('images/logo_report.png')?>"></td>
<td style="padding:10px">
<h4><?php echo $vendor['vendor_name']?></h4>
<small><?php echo $vendor['address1'].','.$vendor['address2'].'-'.$vendor['pin'];?></small>


</td>
<td></td>
</tr>
</table>
<hr />