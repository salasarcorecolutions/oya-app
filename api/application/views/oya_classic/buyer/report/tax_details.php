





<table class="table table-bordered">
	<tr>
		<td>VENDOR:</td>
		<td><?php echo $auction_details['vendor_name']?></td>
	</tr>
	<tr>
		<td>SALE NO:</td>
		<td><?php echo $auction_details['ynk_saleno']?></td>
	</tr>
	<tr>
		<td>PARTICULARS:</td>
		<td><?php echo $auction_details['particulars']?></td>
	</tr>
	<tr>
		<td>START DATE:</td>
		<td><?php echo $auction_details['start_date']?></td>
	</tr>
	<tr>
		<td>END DATE:</td>
		<td><?php echo $auction_details['end_date']?></td>
	</tr>


</table>
<table class="table table-bordered">
<thead>
    <th>SR No</th>
    <th>Tax Name</th>
    <th>Tax Rate</th>
    <th>Total Quantity</th>
    <th>Bid Quantity</th>

</thead>
<tbody>
    <?php
        if ( ! empty($tax))
        {
            $i = 0;
            foreach($tax as $key=>$val)
            {
                $i++;
    ?>
                <tr>
                    <td align="center"><?php echo $i?></td>
                    <td align="center"><?php echo $val['tax_name']?></td>
                    <td align="center"><?php echo $val['tax_rate']?></td>
                    <td align="center"><?php echo $val['totalqty'].' '.$val['totunit']?></td>
                    <td align="center"><?php echo $val['bidqty'].' '.$val['bidunit']?></td>

                </tr>
    <?php
            }
        }
        else
        {
    ?>
            <tr>
                <td colspan="5" align="center">No Tax Details Found</td>
            </tr>

    <?php
        }

    ?>

</tbody>
	
</table>