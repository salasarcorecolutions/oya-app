





<table class="table table-bordered">
	<tr>
		<td>VENDOR:</td>
		<td><?php echo $auction_details['vendor_name']?></td>
	</tr>
	<tr>
		<td>SALE NO:</td>
		<td><?php echo $auction_details['ynk_saleno']?></td>
	</tr>
	<tr>
		<td>PARTICULARS:</td>
		<td><?php echo $auction_details['particulars']?></td>
	</tr>
	<tr>
		<td>START DATE:</td>
		<td><?php echo $auction_details['start_date']?></td>
	</tr>
	<tr>
		<td>END DATE:</td>
		<td><?php echo $auction_details['end_date']?></td>
	</tr>


</table>
<table class="table table-bordered">
<thead>
    <th>SR No</th>
    <th>Lot No</th>
    <th>Vendor Lot No</th>
    <th>Material</th>
    <th>Lot Open Date</th>
    <th>Lot Close Date</th>
    <th>Lot Open Time</th>
    <th>Lot Close Time</th>
    <th>Total Quantity</th>
    <th>Bid Quantity</th>
    <th>Start Bid</th>
    <th>Is Bidded</th>

</thead>
<tbody>
    <?php
        if ( ! empty($closed_lot))
        {
            $i = 0;
            foreach($closed_lot as $key=>$val)
            {
                $i++;
    ?>
                <tr>
                    <td align="center"><?php echo $i?></td>
                    <td align="center"><?php echo $val['ynk_lotno']?></td>
                    <td align="center"><?php echo $val['vendorlotno']?></td>
                    <td align="center"><?php echo $val['product']?></td>
                    <td align="center"><?php echo servertz_to_usertz($val['lot_open_date'],$this->session->userdata('user_tz'),'d-m-Y')?></td>
                    <td align="center"><?php echo servertz_to_usertz($val['lot_close_date'],$this->session->userdata('user_tz'),'d-m-Y')?></td>
                    <td align="center"><?php echo servertz_to_usertz($val['stime'],$this->session->userdata('user_tz'),'H:i:s')?></td>
                    <td align="center"><?php echo servertz_to_usertz($val['etime'],$this->session->userdata('user_tz'),'H:i:s')?></td>
                    <td align="center"><?php echo $val['totalqty'].$val['totunit']?></td>
                    <td align="center"><?php echo $val['bidqty'].$val['bidunit']?></td>
                    <td align="center"><?php echo $val['startbid']?></td>
                    <td align="center"><?php echo ! empty($val['bidid'])?'Yes':'No';?></td>










                </tr>
    <?php
            }
        }
        else
        {
    ?>
            <tr>
                <td colspan="12" align="center">No Lot Found</td>
            </tr>

    <?php
        }

    ?>

</tbody>
	
</table>