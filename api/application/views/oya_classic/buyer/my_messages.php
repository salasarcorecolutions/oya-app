<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<!-- Fav and touch icons -->
<link rel="apple-touch-icon-precomposed" sizes="144x144" href="<?php echo base_url(ciHtmlTheme.'/assets/ico/apple-touch-icon-144.png')?>">
<link rel="apple-touch-icon-precomposed" sizes="114x114" href="<?php echo base_url(ciHtmlTheme.'/assets/ico/apple-touch-icon-114.png')?>">
<link rel="apple-touch-icon-precomposed" sizes="72x72" href="<?php echo base_url(ciHtmlTheme.'/assets/ico/apple-touch-icon-72.png')?>">
<link rel="apple-touch-icon-precomposed" sizes="57x57" href="<?php echo base_url(ciHtmlTheme.'/assets/ico/apple-touch-icon-57.png')?>">
<link rel="shortcut icon" href="<?php echo base_url(ciHtmlTheme.'/assets/ico/favicon.png')?>">
<title>My Messages</title>
<!-- Bootstrap core CSS -->
<link href="<?php echo base_url(ciHtmlTheme.'/assets/bootstrap/css/bootstrap.min.css')?>" rel="stylesheet">
<link rel="stylesheet" href="<?php echo base_url();?>assets/plugins/datatables/dataTables.bootstrap.css">
<link rel="stylesheet" href="<?php echo base_url();?>assets/plugins/datatables/jquery.dataTables_themeroller.css">
<link rel="stylesheet" href="<?php echo base_url();?>assets/plugins/rich-text-editor/richtext.min.css">
<!-- Custom styles for this template -->
<link href="<?php echo base_url(ciHtmlTheme.'/assets/css/style.css')?>" rel="stylesheet">
<link href="<?php echo base_url(ciHtmlTheme.'/assets/css/custom.css')?>" rel="stylesheet">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
<!-- styles needed for carousel slider -->
<!-- <link href="<?php echo base_url(ciHtmlTheme.'/assets/css/buyer.css')?>" rel="stylesheet"> -->


<script>
    paceOptions = {
      elements: true
    };
</script>
<script src="<?php echo base_url(ciHtmlTheme.'/assets/js/pace.min.js')?>"></script>

 </head>
<body>

<div id="wrapper">
<!-- /.header -->
 <?php $this->load->view(ciHtmlTheme.'/buyer/buyer_header');?>
  <!-- /.header -->
  <div class="main-container">
   <div class="container-fluid">
      <div class="row">
        <div class="col-sm-3 page-sidebar">
         <?php $this->load->view(ciHtmlTheme.'/buyer/left_navigation');?>
        </div>
        <!--/.page-sidebar-->
        
        <div class="col-sm-9 page-content">
         	<div class="inner-box">
         		
				<!-- <section id="content">
					<section class="hbox stretch">
						<aside class="aside aside-md bg-white">
						<section class="vbox">
							<header class="dk header">
							<a href="<?=base_url();?>buyer/my_messages/1" class="btn btn-icon btn-default btn-sm pull-right"><i class="fa fa-refresh"></i></a>
								<button class="btn btn-icon btn-default btn-sm pull-right visible-xs m-r-xs" data-toggle="class:show" data-target="#mail-nav"><i class="fa fa-reorder"></i></button> 
								<p class="h4">Messages</p>
							</header>
							<section>
								<section>
									<section id="mail-nav" class="hidden-xs">
									<ul class="nav nav-pills nav-stacked no-radius">
										<li  class="active" id="profil_li" onclick="hideMessage('i');"><a href="#profile" data-toggle="tab"><span class="badge pull-right"><?=$all_message['maxrows']['numrows'];?></span>Inbox</a></li>
										<li  id="passwordTab_li" onclick="hideMessage('s');"><a href="#passwordTab" data-toggle="tab"><span class="badge badge-hollow pull-right"><?=$all_message['maxrows1']['numrows'];?></span>Sent</a></li>
										<li id="payment_li" style="display:none;"><a href="#payment" data-toggle="tab">Messages</a></li>
									</ul>
									</section>
								</section>
							</section>
						</section>
						</aside>
						<aside class="bg-light lter b-l" id="email-list">
						<section class="vbox">
							<section class="scrollable hover" id="profile">
							<ul class="list-group no-radius m-b-none m-t-n-xxs list-group-alt list-group-lg" >
									<?php
										for($i=0;$i<count($all_message['messages']);$i++)
										{
										?>
										
											
											<li class="list-group-item"> 
												<a href="#" class="thumb-xs pull-left m-r-sm"> <img src="<?php echo base_url();?>images/avatar_default.jpg" class="img-circle"> 
											
												</a> 
											<a class="clear" href="javascript:void(0);" onclick="getMessage('<?php echo $all_message['messages'][$i]["msgid"]; ?>');" > 
												<small class="pull-right text-muted"><?php if($all_message['messages'][$i]['msgread']=="N") echo"<b class='text-primary '>".$all_message['messages'][$i]['mdate']."</b>"; else echo $all_message['messages'][$i]['mdate']; ?></small> 
												<strong><?php if($all_message['messages'][$i]['msgread']=="N") echo"<b class='text-primary '>".$all_message['messages'][$i]['conperson']."</b>"; else echo $all_message['messages'][$i]['conperson']; ?></strong> 
												<span><?php if($all_message['messages'][$i]['msgread']=="N") echo"<b class='text-primary '>".$all_message['messages'][$i]['subject']."</b>"; else echo $all_message['messages'][$i]['subject']; ?></span> 
												
											</a> 
											</li>
									<?php
										}
										if(count($all_message['messages'])<=0)
										{
										?>
										<li class="list-group-item">
												
												<strong class="text-danger">No Record Found for : <?=@$this->input->post('filter');?></strong> 
												
											
											</li>
										<?php
										}
										?>
											</ul>
								</section>
								
								<section class="scrollable hover" id="passwordTab">
								<ul class="list-group no-radius m-b-none m-t-n-xxs list-group-alt list-group-lg" >
									<?php	
										for($i=0;$i<count($all_message['sent_messages']);$i++)
										{
										?>
										
											
											<li class="list-group-item"> 
												<a href="#" class="thumb-xs pull-left m-r-sm"> <img src="<?php echo base_url();?>images/avatar_default.jpg" class="img-circle"> 
											
												</a> 
											<a href="javascript:void(0);" onclick="getMessage('<?php echo $all_message['sent_messages'][$i]["msgid"]; ?>');"> 
											<small class="pull-right text-muted"><?php  echo $all_message['sent_messages'][$i]['mdate']; ?></small> 
												<strong><?php  echo $all_message['sent_messages'][$i]['conperson'];  ?></strong> 
												<span><?php  echo $all_message['sent_messages'][$i]['subject'];  ?></span> 
												
											</a> 
											</li>

										
									
									<?php
										}
										if(count($all_message['sent_messages'])<=0)
										{
										?>
										<li class="list-group-item">
												
												<strong class="text-danger">No Record Found for : <?=@$this->input->post('filter');?></strong> 
												
											
											</li>
										<?php
										}
										?>
										</ul>
										</section>
										<section class="scrollable" id="payment">
										<ul class="list-group no-radius m-b-none m-t-n-xxs list-group-alt list-group-lg" id="read_message">
										</ul>
										</section>
										<section class="scrollable" id="user_message">
										</section>
										<footer class="footer b-t bg-white-only"> 
										<form class="m-t-sm" action="<?=base_url();?>buyer/my_messages/1" method="post"> 
										<div class="input-group"> 
										<input type="text" placeholder="Search Messages on Subject Based" id="filter" name="filter" value="<?=@$this->input->post('filter');?>" class="input-sm form-control input-s-sm"> 
										<div class="input-group-btn"> 
										<button class="btn btn-sm btn-default" type="submit"><i class="fa fa-search"></i></button> 
										</div> 
										</div> 
										</form> 
										</footer>
										</section>
									</aside>
										
						</section>
						
					
						
					</section>
					<a href="#" class="hide nav-off-screen-block" data-toggle="class:nav-off-screen" data-target="#nav"></a> 
				</section> -->
				<div id="window">
					<section class="col-lg-2 menu">
						<div class="mail_header">
							<div class="logo-wrap">
								MY CONTACT
							</div>
							
						</div>
						
						<div class="content" id="contact">
								
								
							
								
							</div>
					</section>
					
					<section class="emails col-lg-4">
						<div class="mail_header">
							<div class="input-group"> 
								<input type="text" placeholder="Search Messages on Subject Based" id="filter" name="filter" value="" class="input-sm form-control input-s-sm"> 
								<div class="input-group-btn"> 
									<button class="btn btn-link" type="button" onclick="search()"><i class="fa fa-search"></i></button> 
								</div> 
							</div> 
						</div>
						
						<div class="content">
							<ul style='position:relative;' id="emails-list">
							<button style='position:absolute;right:0;' type="button" class="label label-primary btn-circle btn-xl pull-right" data-toggle="modal" data-target="#myModal"><i class="fa fa-plus"></i></button>
								<span id="message_list"></span>
							
								<!-- <li class="selected">
									
									<div class="sub_title">
										<p class="sender">Shubham Sawant</p>
										<h4 class="title">Do Androids Dream of Electric Sheep?</h4>
										<p class="bread-text-preview">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam venenatis mauris id mi vestibulum faucibus. Etiam non tristique eros. Aenean et ullamcorper velit, quis varius est.</p>
										<p class="recieved">3 min ago</p>
									</div>
								</li>
								<li class="marked">
									
									<div class="sub_title">
										<p class="sender">Shubham Sawant</p>
										<h4 class="title">I Was Told There'd Be Cake</h4>
										<p class="bread-text-preview">Etiam non tristique eros. Aenean et ullamcorper velit, quis varius est.</p>
										<p class="recieved">1 day ago</p>
									</div>
								</li>
								<li>
									
									<div class="sub_title">
										<p class="sender">Shubham Sawant</p>
										<h4 class="title">Extremely Loud and Incredibly Close</h4>
										<p class="bread-text-preview">Nullam a lacus cursus, tincidunt orci in, mattis ipsum. Quisque volutpat ultricies pretium.</p>
										<p class="recieved">2 days ago</p>
									</div>
								</li>
								<li>
									
									<div class="sub_title">
										<p class="sender">Search Results</p>
										<h4 class="alizarin">No Records Found</h4>
										
									</div>
								</li> -->
							</ul>
						</div>
					</section>
					
					<section class="preview col-lg-6">
						<div class="mail_header">
						<div class="logo-wrap">
								THREAD VIEW
							</div>
							
						</div>
						
						<div class="content-wrap">
							<div class="chat-area">
	
								<div class="chat-area-main">
									<!--<div class="chat-msg">
										<div class="chat-msg-content">
											<div class="chat-msg-text">Luctus et ultrices posuere cubilia curae.</div>
											<div class="chat-msg-text">Neque gravida in fermentum et sollicitudin ac orci phasellus egestas. Pretium lectus quam id leo.</div>
										</div>
									</div>

									<div class="chat-msg owner">
										<div class="chat-msg-content">
											<div class="chat-msg-text">Sit amet risus nullam eget felis eget. Dolor sed viverra ipsum😂😂😂</div>
											<div class="chat-msg-text">Cras mollis nec arcu malesuada tincidunt.</div>
										</div>
									</div>
									<div class="chat-msg">
									
									<div class="chat-msg-content">
									<div class="chat-msg-text">Aenean tristique maximus tortor non tincidunt. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia curae😊</div>
									<div class="chat-msg-text">Ut faucibus pulvinar elementum integer enim neque volutpat.</div>
									</div>
									</div>
									<div class="chat-msg owner">
									
									<div class="chat-msg-content">
									<div class="chat-msg-text">posuere eget augue sodales, aliquet posuere eros.</div>
									<div class="chat-msg-text">Cras mollis nec arcu malesuada tincidunt.</div>
									</div>
									</div>
									<div class="chat-msg">
									
									<div class="chat-msg-content">
									<div class="chat-msg-text">Egestas tellus rutrum tellus pellentesque</div>
									</div>
									</div>
									<div class="chat-msg">
									
									<div class="chat-msg-content">
									<div class="chat-msg-text">Consectetur adipiscing elit pellentesque habitant morbi tristique senectus et.</div>
									</div>
									</div>
									<div class="chat-msg owner">
									
									<div class="chat-msg-content">
									<div class="chat-msg-text">Tincidunt arcu non sodales😂</div>
									</div>
									</div>
									<div class="chat-msg">
									
									<div class="chat-msg-content">
									<div class="chat-msg-text">Consectetur adipiscing elit pellentesque habitant morbi tristique senectus et🥰</div>
									</div>
									</div>
								</div>-->
								</div>
								
						</div>
						<div class="col-lg-12">
						<form name="compose_thread" id="compose_thread">
						<div class="col-lg-10 chatbox">
							<textarea rows="4" class="message_content ckeditor form-control" name="msg1" id="msg1"></textarea>
						</div>
						<div class="col-lg-2">
							
							<br>
							<div class="image-upload">
								<label for="file-input">
									<i class="fa fa-upload" aria-hidden="true"></i>
								</label>
								<input id="file-input" type="file" name="upload_file[]" multiple/>
							</div>
						</div>
						</div>
						<input type="hidden" name="vendor_id" id="ven_id" value=""/>
						<input type="hidden" name="parent_id" id="parent_id" value=""/>
						<input type="hidden" name="sub" value=""/>


						</form>
						
						<button type="button" class="label label-success btn-circle btn-xl pull-right" onclick="submit_msg();"><i class="fa fa-paper-plane"></i></button>

					</section>
				</div>
        	</div>
        
		  </div>
		  
		  
      <!--/.row--> 
    </div>
    <!--/.container--> 
  </div>
  <!-- /.main-container -->
  </div>
  <!-- Button trigger modal -->


<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
	<div class="modal-content">
			
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="myModalLabel">Compose Messages</h4>
      </div>
      <div class="modal-body compose_msg">
		<form class="form-group"  id="compose_msg">
	 		<div class='col-lg-12'>
				 <div class='col-lg-3'>
					<label class='control-label'>To</label>
				</div>
				<div class='col-lg-9'>
					<select type='text' class='form-control' name="vendor_id">
						<option value="">--Select One--</option>
						<?php
							if( ! empty($vendor))
							{
								foreach($vendor as $v)
								{
							?>
									<option value="<?php echo $v['vendor_id']?>"><?php echo $v['vendor_name']?></option>
							<?php
								}
							}
						?>
					</select>
				</div>
				<div class='col-lg-3'>
					<label class='control-label'>Subject</label>
				</div>
				<div class='col-lg-9'>
					<input type='text' class='form-control' name="sub">
				</div>
				<div class='col-lg-3'>
					<label class='control-label'>Attachments</label>
				</div>
				<div class='col-lg-9'>
					<input id="input-upload-img1" name="upload_file[]" type="file" class="file" data-preview-file-type="text" multiple>
				</div>
			</div>
			<br>
			<div class='col-lg-12'>
				<div class="page-wrapper box-content">
					<textarea class="message_content ckeditor form-control" name="msg" id="msg" ></textarea>
				</div>
			</div>
		</form>
      </div>
      <div class="modal-footer">
		  <button type="button" class="btn btn-primary" onclick="submit_form();" >Save changes</button>
        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
  <!-- /.footer --> 
  <?php $this->load->view(ciHtmlTheme.'/includes/footer');?>
 
</div>
<!-- /.wrapper --> 

<!-- Le javascript
================================================== --> 
<!-- Placed at the end of the document so the pages load faster --> 
<script src="<?php echo base_url(ciHtmlTheme.'/assets/js/jquery.min.js')?>"> </script>
<script src="<?php echo base_url(ciHtmlTheme.'/assets/bootstrap/js/bootstrap.min.js')?>"></script> 
<!-- include equal height plugin  --> 
<script src="<?php echo base_url(ciHtmlTheme.'/assets/js/jquery.matchHeight-min.js')?>"></script> 
<!-- include jquery list shorting plugin plugin  --> 
<script src="<?php echo base_url(ciHtmlTheme.'/assets/js/hideMaxListItem.js')?>"></script> 
<!-- include jquery.fs plugin for custom scroller and selecter  --> 
<script src="<?php echo base_url(ciHtmlTheme.'/assets/plugins/jquery.fs.scroller/jquery.fs.scroller.js')?>"></script>
<script src="<?php echo base_url(ciHtmlTheme.'/assets/plugins/jquery.fs.selecter/jquery.fs.selecter.js')?>"></script>
<script src="<?php echo base_url();?>assets/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="<?php echo base_url();?>assets/plugins/datatables/dataTables.bootstrap.min.js"></script>
<script src="<?php echo base_url();?>assets/plugins/rich-text-editor/jquery.richtext.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/manual_datatable.js"></script>
<script src="<?php echo base_url(ciHtmlTheme.'/assets/js/jquery.validate.min.js')?>"></script>
<script src="<?php echo base_url(ciHtmlTheme.'/assets/js/jquery.form.min.js')?>"></script>
<script src="https://cdn.ckeditor.com/4.14.0/standard/ckeditor.js"></script>

<!-- include custom script for site  --> 
<!-- <script src="<?php echo base_url(ciHtmlTheme.'/assets/js/script.js')?>"></script> -->
<!-- include jquery autocomplete plugin  -->
<script>
$(document).ready(function ()
{
	
	//get_thread_msg(0);
	get_inbox_msg();
	get_contacts();
    /* $('.message_content').richText({
		bold:true,
		italic:true,
		underline:true,
		// text alignment
		leftAlign:true,
		centerAlign:true,
		rightAlign:true,
		 // lists
		 ol: false,
  		ul: false,
		// title
		heading: false,
		fontColor: false,
  		fontSize: false,
		// uploads
		imageUpload: false,
		fileUpload: false,

		// link
		urls: false,

		// tables
		table: false,

		// code
		removeStyles: false,
		code: false,
	}); */
		
});	


 function submit_form()
{
	
		$("#msg").val(CKEDITOR.instances.msg.getData());

		var form = $("#compose_msg");
		form.validate({
			ignore:[],
			rules: {
				sub: "required",
				vendor_id:"required",
				msg:{ 
					required: function() 
                        {
                         CKEDITOR.instances.msg.updateElement();
                        },

                     minlength:10
				},
			},
			messages: {
				sub: "<b class='text-danger'>Please Enter Subject Line</b>",
				vendor_id:"<b class='text-danger'>Please Select Vendor</b>",
				msg:"<b class='text-danger'>Please Enter Message</b>",
				
			}
		});

		if (form.valid()){
			$('#compose_msg').ajaxSubmit({
				url:"<?php echo base_url('buyer/my_messages/compose_msg');?>",
				type: 'post',
				dataType:'json',
				cache: false,
				clearForm: false,
				enctype: 'multipart/form-data',
				success: function (response)
				{
					if (response.status=="success")
					{
						
						$("#myModal").modal("hide");
						get_inbox_msg();
					}
					else
					{
						alert('Please Try Again After Some Time');
						$("#myModal").modal("hide");
						get_inbox_msg();
					}
				},
				error:function()
				{
					alert('Please Try Again After Some Time');
				}
			});
	
		}
	

	
} 
function submit_msg()
{
	var desc = CKEDITOR.instances.msg1.getData();
	$("#msg1").val(desc);
	console.log(desc);
	var form = $("#compose_thread");
	form.validate({
		ignore:[],
		rules: {
			sub: "required",
			
		},
		messages: {
			sub: "<b class='text-danger'>Please Enter Subject Line</b>",
			
		}
	});

	if (form.valid()){
		$('#compose_thread').ajaxSubmit({
			url:"<?php echo base_url('buyer/my_messages/compose_thread');?>",
			type: 'post',
			dataType:'json',
			cache: false,
			clearForm: false,
			enctype: 'multipart/form-data',
			success: function (response)
			{
				if (response.status=="success")
				{
					
					$("#myModal").modal("hide");
					CKEDITOR.instances['msg1'].setData('');
					get_thread_msg(response.msg_id);
					get_contacts();
					get_inbox_msg();

				}
				else
				{
					alert(response.msg);
					$("#myModal").modal("hide");
				}
			}
		});
	
	}
}

function get_thread_msg(msg_id='')
{
	$.ajax({
		url:"<?php echo base_url('buyer/my_messages/get_thread_msg');?>",
		type: 'post',
		data:{'msg_id':msg_id},
		cache:false,
		clearForm:false,
		dataType: 'json',
		success: function (response)
		{
			var html ='';
			if (response)
			{
				var user_id = '<?php echo $this->session->userdata('bidder_id');?>';
				var html ='';

				$.each(response, function(i,v)
				{
					if(v['sender_id']!=user_id)
					{
						html+= '<div class="chat-msg">';
					}
					else
					{
						html+='<div class="chat-msg owner">';
						
					}
					
					html+='<div class="chat-msg-content">'+
							'<div class="chat-msg-text">'+ v['text_message'] +'</div>';
							
					html+='<div style="color:grey;">'+v['date_time']+'</div></div>';
					if (v['attachment'].length >0)
					{
						
						var i =1;
						$.each(v['attachment'],function(k,value){
							
							var file = '<?php echo AMAZON_BUCKET.'/'.s3BucketTestDir?>message_attachment/'+value;
							html+='<div class="speech-right"><h4><span class="attach">'+'<i class="fa fa-paperclip fa-fw"></i><a href="'+file+'" target="_blank">File'+i+'</a></span></h4></div>';
							i++;
						});
					}
					html+='</div>';
					html+='</div>';
					
				});
				
			}
			else
			{
				html+='No Record Found';
			}
			$('.chat-area-main').html(html);	

		}
	});
}
function get_inbox_msg()
{
	$.ajax({
		url:"<?php echo base_url('buyer/my_messages/get_inbox_msg');?>",
		type: 'post',
		cache:false,
		clearForm:false,
		dataType: 'json',
		success: function (response)
		{
			var html ='';
			if (response.length > 0)
			{
				
				var i =0;
				var id =0;
				$.each(response,function(i,value)
				{
					if (i == 0)
					{
						var cl="selected";
					   id = value['msg_id'];
					}
					else
					{
						var cl="marked";
					}
					html+='<a onclick="thread('+value['msg_id']+','+value['vendor_id']+');read_msg('+value['msg_id']+')"><li class="'+cl+'">'+			
							'<div class="sub_title">'+
							'<p class="sender">'+value['vendor_name']+'</p>';
							if (value['unread'] > 0)
							{
								html+='<p style="color:orange"><b>Unread ('+value['unread']+')</></p>';
							}
							
							html+='<h4 class="title">'+value['msg_sub']+'</h4>'+
							'<p class="bread-text-preview">'+value['message_text']+'</p>'+
							'<div class="recieved">'+value['date_time']+'</div>'+
							'</div>'+
							'</li>'+
							'</a>';
					i++;
				});
				
				
			}
			else
			{
				html+='<li>'+			
					'<div class="sub_title">'+
						'<p class="sender">Search Results</p>'+
						'<h4 class="alizarin">No Records Found</h4>'+
					'</div>'+
				'</li>';
			}
			$("#message_list").html(html);
			//get_thread_msg(0);

		}
	});
}
function thread(msg_id,vendor_id)
{
$("#parent_id").val(msg_id);
$("#ven_id").val(vendor_id);
get_thread_msg(msg_id);

}	
function read_msg(msg_id)
{
	$.ajax({
		url:"<?php echo base_url('buyer/my_messages/read_msg');?>",
		type: 'post',
		data:{'msg_id':msg_id},
		cache:false,
		clearForm:false,
		dataType: 'json',
		success: function (response)
		{
			if (response.status == 'success')
			{
				get_thread_msg(response.msg_id);
				get_contacts();
			}
		}
	});
}	
function get_contacts()
{
	$.ajax({
		url:"<?php echo base_url('buyer/my_messages/get_contacts');?>",
		type: 'post',
		cache:false,
		clearForm:false,
		dataType: 'json',
		success: function (response)
		{
			var html ='';
			html+='<ul id="categories">';
			if (response)
			{
				$.each(response, function(i,v)
				{
					html+='<li class="selected">'+
							'<i class="fa fa-user" aria-hidden="true"></i> '+
							i+
							'<span class="badge">'+v+'</span>'+
						'</li>';


				});
			}
			else
			{
				html+='No Record Found';
			}					
										
			html+='</ul>';
			$('#contact').html(html);
			
		}
	});
}	
function search()
{
	var msg = $("#filter").val();
	$.ajax({
		url:"<?php echo base_url('buyer/my_messages/search_msg');?>",
		type: 'post',
		data:{'filter':msg},
		cache:false,
		clearForm:false,
		dataType: 'json',
		success: function (response)
		{
			
			var html ='';
			console.log(response);
			if (response)
			{
				
				var i =0;
				var id =0;
				$.each(response,function(i,value)
				{
					if (i == 0)
					{
						var cl="selected";
					   id = value['msg_id'];
					}
					else
					{
						var cl="marked";
					}
					html+='<a onclick="thread('+value['msg_id']+','+value['vendor_id']+');read_msg('+value['msg_id']+')"><li class="'+cl+'">'+			
							'<div class="sub_title">'+
							'<p class="sender">'+value['vendor_name']+'</p>'+
							'<h4 class="title">'+value['msg_sub']+'</h4>'+
							'<p class="bread-text-preview">'+value['message_text']+'</p><br/>'+
							'<p class="recieved">'+value['date_time']+'</p>'+
							'</div>'+
							'</li>'+
							'</a>';
					i++;
				});
				
				
			}
			else
			{
				html+='<li>'+			
					'<div class="sub_title">'+
						'<p class="sender">Search Results</p>'+
						'<h4 class="alizarin">No Records Found</h4>'+
					'</div>'+
				'</li>';
			}
			$("#message_list").html(html);
			

		}
	});
}								   
</script>	
</body>
</html>
