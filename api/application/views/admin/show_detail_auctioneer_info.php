<style>
fieldset { 
  display: block;
  margin-left: 2px;
  margin-right: 2px;
  padding-top: 0.35em;
  padding-bottom: 0.625em;
  padding-left: 0.75em;
  padding-right: 0.75em;
  border: 2px groove (internal value);
}

</style>
<!--CONTENT CONTAINER-->
<!--===================================================-->
<div id="content-container">
	<!--Page Title-->
	<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
	<div id="page-title">
		<h1 class="page-header text-overflow"><?php echo $pagetitle?></h1>
	</div>
	<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
	<!--End page title-->


	<!--Breadcrumb-->
	<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
	<ol class="breadcrumb">
		<li><a href="<?php echo base_url('admin/dashboard');?>">Home</a></li>
		<li class="active"><?php echo $pagetitle?></li>
	</ol>
	<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
	<!--End breadcrumb-->
	<input type="hidden" id="t11" name="auctionId">
	<div id="page-content">	
		<div class="panel">
			<div class="panel-heading">
				<h3 class="panel-title"><?php echo $pagetitle?></h3>
			</div>
			<div class="panel-body">
			
				<form name="frm" method="post" id="auctioneer_form">
					<input type="hidden" name="vendor_id" id="vendor_id" value="<?php echo $auctioneer_details['vendor_id'];?>" />
					<input type="hidden" name="plant_id" id="plant_id" value="<?php echo $auctioneer_details['plant_id'];?>" />
					<input type="hidden" name="user_contact_id" id="user_contact_id" value="<?php echo $auctioneer_details['user_contact_id'];?>" />
					<fieldset>
						<legend>Company Details:</legend>
							<div class="col-sm-12 form-group">
								<label class="col-sm-2 control-label">Vendor Name :</label>
								<div class="col-md-6">							
									<input type="text" class="form-control" name="vendor_name" id="vendor_name" value="<?php echo $auctioneer_details['vendor_name'];?>" />
								</div>
							</div>
							<div class="col-sm-12 form-group">
								<label class="col-sm-2 control-label">Address1 :</label>
								<div class="col-md-6">							
									<input type="text" class="form-control" name="add1" id="add1" value="<?php echo $auctioneer_details['address1'];?>"/>
								</div>
							</div>
							<div class="col-sm-12 form-group">
								<label class="col-sm-2 control-label">Address2 :</label>
								<div class="col-md-6">							
									<input type="text" class="form-control" name="add2" id="add2" value="<?php echo $auctioneer_details['address2'];?>" />
								</div>
							</div>
							<div class="col-sm-12 form-group">
								<label class="col-sm-2 control-label">Country :</label>
								<div class="col-md-6">							
									<select class="form-control selectpicker" name="cmp_country" id="cmp_country" onchange="fetch_state(this.id)" attr = "cmp">
									<option value="">--Select One--</option>
									<?php 
										if ( ! empty($countries))
										{
											foreach ($countries as $key=>$value)
											{
											?>
												<option value="<?php echo $value['name']?>"><?php echo $value['name']?></option>
											<?php
											}
										}
										?>
									</select>
								</div>
							</div>
							<div class="col-sm-12 form-group">
								<label class="col-sm-2 control-label">State :</label>
								<div class="col-md-6">							
									<select class="form-control" name="cmp_state" id="cmp_state" attr = 'cmp' onchange="fetch_city(this.id);" >
									</select>
								</div>
							</div>
							<div class="col-sm-12 form-group">
								<label class="col-sm-2 control-label">City :</label>
								<div class="col-md-6">							
									<select class="form-control" name="cmp_city" id="cmp_city">
									</select>
								</div>
							</div>
							
				   </fieldset>
				   <fieldset>
						<legend>Plant Details:</legend>
							<div class="col-sm-12 form-group">
								<label class="col-sm-2 control-label">Plant Name :</label>
								<div class="col-md-6">							
									<input type="text" class="form-control" name="plant_name" id="plant_name" value="<?php echo $auctioneer_details['plant_name'];?>" />
								</div>
							</div>
							<div class="col-sm-12 form-group">
								<label class="col-sm-2 control-label">Plant Address1 :</label>
								<div class="col-md-6">							
									<input type="text" class="form-control" name="plant_add1" id="plant_add1" value="<?php echo $auctioneer_details['plant_add1'];?>" />
								</div>
							</div>
							<div class="col-sm-12 form-group">
								<label class="col-sm-2 control-label">Plant Address2 :</label>
								<div class="col-md-6">							
									<input type="text" class="form-control" name="plant_add2" id="plant_add2" value="<?php echo $auctioneer_details['plant_add2'];?>" />
								</div>
							</div>
							<div class="col-sm-12 form-group">
								<label class="col-sm-2 control-label">Country :</label>
								<div class="col-md-6">
								
									<select class="form-control selectpicker" name="plant_country" id="plant_country" onchange="fetch_state(this.id)" attr = "plant">
										<option value="">--Select One--</option>
										<?php 
									
										
										if( ! empty($countries))
										{
											foreach($countries as $key=>$value)
											{
										?>
												<option value="<?php echo $value['name']?>"><?php echo $value['name']?></option>
										
										<?php
											}
										}
										?>
									</select>
								</div>
							</div>
							<div class="col-sm-12 form-group">
								<label class="col-sm-2 control-label">State :</label>
								<div class="col-md-6">							
									<select class="form-control" name="plant_state" id="plant_state" onchange="fetch_city(this.id)" attr = "plant" >
									</select>
								</div>
							</div>
							<div class="col-sm-12 form-group">
								<label class="col-sm-2 control-label">City :</label>
								<div class="col-md-6">							
									<select class="form-control" name="plant_city" id="plant_city">
									</select>
								</div>
							</div>
							
				   </fieldset>
				   <fieldset>
						<legend>Contact Details:</legend>
							<div class="col-sm-12 form-group">
								<label class="col-sm-2 control-label">Person Name :</label>
								<div class="col-md-6">							
									<input type="text" class="form-control" name="c_name" id="c_name"  value="<?php echo $auctioneer_details['c_name'];?>"/>
								</div>
							</div>
							<div class="col-sm-12 form-group">
								<label class="col-sm-2 control-label">Primary Email :</label>
								<div class="col-md-6">							
									<input type="text" class="form-control" name="primary_email" id="primary_email" value="<?php echo $auctioneer_details['c_email'];?>" />
								</div>
							</div>
							<div class="col-sm-12 form-group">
								<label class="col-sm-2 control-label">Primary Contact :</label>
								<div class="col-md-6">							
									<input type="text" class="form-control" name="primary_contact" id="primary_contact" value="<?php echo $auctioneer_details['c_contacts'];?>"/>
								</div>
							</div>
							<div class="col-sm-12 form-group">
								<label class="col-sm-2 control-label">Secondary Contact :</label>
								<div class="col-md-6">							
									<input type="text" class="form-control" name="secondary_contact" id="secondary_contact" value="<?php echo $auctioneer_details['secondary_contacts'];?>" />
								</div>
							</div>
							<div class="col-sm-12 form-group">
								<label class="col-sm-2 control-label">Secondary Email :</label>
								<div class="col-md-6">							
									<input type="text" class="form-control" name="secondary_email" id="secondary_email" value="<?php echo $auctioneer_details['secondary_email'];?>" />
								</div>
							</div>
							<div class="col-sm-12 form-group">
								<label class="col-sm-2 control-label">User Name :</label>
								<div class="col-md-6">							
									<input type="text" class="form-control" name="user_name" id="user_name" value="<?php echo $auctioneer_details['uname'];?>"/>
								</div>
							</div>
							<div class="col-sm-12 form-group">
								<label class="col-sm-2 control-label">Password :</label>
								<div class="col-md-6">							
									<input type="text" class="form-control" name="pass" id="pass" value="<?php echo $auctioneer_details['password'];?>"/>
								</div>
							</div>
							<div class="col-sm-12 form-group">
								<label class="col-sm-2 control-label">Is Active :</label>
								<div class="col-md-6">							
									<select class="form-control" name="is_active" id="is_active" >
										<option value='Y'
										<?php 
											if ($auctioneer_details['activated']=='Y')
											{
												echo 'selected';
											}
										?>
										>Yes</option>
										<option value='N'
										<?php 
											if ($auctioneer_details['activated']=='N')
											{
												echo 'selected';
											}
										?>
										
										>No</option>
									</select>
								</div>
							</div>
							
							<!--Vendor Interest-->
							<?php 
								$vendor_int = json_decode($auctioneer_details['vendor_interest'],true);
								
							?>
							<div class="col-sm-12 form-group">
								<label class="col-sm-2 control-label">Vendor Interest :</label>
								<div class="col-md-6">	
									<div class="col-sm-3">
										<input id="SM" type="checkbox" name="interest[]" value='scrap_management' class="checkBoxClass" 
										<?php 
										if (! empty($vendor_int))
										{
											if (array_key_exists('scrap_management',$vendor_int))
											{
												echo 'checked';
											}
										}
										?>

										/> Scrap Management
									</div>
									<div class="col-sm-3">
										<input id="LG" type="checkbox" class="checkBoxClass" name="interest[]" value="logistics"
										<?php 
										if ( ! empty($vendor_int))
										{
											if (array_key_exists('logistics',$vendor_int))
											{
												echo 'checked';
											}
										}
										?>
										
										/> Logistics
									</div>
									<div class="col-sm-3">
										<input id="ET" type="checkbox" class="checkBoxClass" name="interest[]" value="e_tender"
										<?php 
										if ( ! empty($vendor_int))
										{
											if (array_key_exists('e_tender',$vendor_int))
											{
												echo 'checked';
											}
										}
										?>
										
										/> E-tender 
									</div>
									<div class="col-sm-3">
										<input id="PC" type="checkbox" class="checkBoxClass" name="interest[]" value="Procurement"
										<?php 
										if ( ! empty($vendor_int))
										{
											if (array_key_exists('Procurement',$vendor_int))
											{
												echo 'checked';
											}
										}
										?>
										
										/> Procurement
									</div>
									<div class="col-sm-3">
										<input id="FA" type="checkbox" class="checkBoxClass" name="interest[]" value="forward_auction"
										<?php 
										if ( ! empty($vendor_int))
										{
											if (array_key_exists('forward_auction',$vendor_int))
											{
												echo 'checked';
											}
										}
											
										?>
										/> Forward Auction
									</div>
									<div class="col-sm-3">
										<input id="RA" type="checkbox" class="checkBoxClass" name="interest[]" value="reverse_auction"
										<?php 
										if ( ! empty($vendor_int))
										{
											if (array_key_exists('reverse_auction',$vendor_int))
											{
												echo 'checked';
											}
										}
										?>
										
										/> Reverse Auction<br/><br/>
									</div>
									<div class="col-sm-3">
										<input id="DA" type="checkbox" class="checkBoxClass" name="interest[]" value="dutch_auction"
										<?php 
										if ( ! empty($vendor_int))
										{
											if (array_key_exists('dutch_auction',$vendor_int))
											{
												echo 'checked';
											}
										}
										?>
										
										/> Dutch Auction
									</div>
								</div>
							</div>
							<div class="col-md-6">
								<button class="btn btn-success pull-right" type="submit">Save</button>
							</div>
							<div class="col-md-6">
								<button class="btn btn-danger" type="button">Back</button>
							</div>
				   </fieldset>
				</form>
			</div>	
		</div>
	</div>	
</div>

<!--===================================================-->
<!--END CONTENT CONTAINER-->
<script>
$(document).ready(function(){
	//for company
	var cmp_id = 'cmp_country';
	var state_id = 'cmp_state';
	var city_id = 'cmp_city';
	$("#"+cmp_id).val('<?php echo $auctioneer_details['country']?>');
	$("#"+cmp_id).trigger("change");
	
	// for plant
	var plant_cmp_id = 'plant_country';
	var plant_state_id = 'plant_state';
	var plant_city_id = 'plant_city';
	$("#"+plant_cmp_id).val('<?php echo $auctioneer_details['plant_country']?>');
	$("#"+plant_cmp_id).trigger("change");
	
});
var vRules 	= 	'';
var vMessages = '';
$("#auctioneer_form").validate({
	rules: vRules,
	messages: vMessages,
	submitHandler: function(form){
		$(form).ajaxSubmit({
			url: '<?php echo base_url('admin/auctioneer_details/update_details');?>',
			type:"post",
			dataType: 'json',
			success: function (resObj) {
				if (resObj.success == true) 
				{
					Display_msg(resObj.block_msg,resObj.success);
				}else{
					Display_msg(resObj.block_msg,resObj.success);
					return false;
				}
			},
			error: function(){
				
			}
		});
	}
});
function fetch_state(id)
{
	var country_name = $("#"+id).val();
	var attr = $("#"+id).attr('attr');
	if(country_name != ""){
		jQuery.ajax({
			url: '<?php echo base_url();?>clients/fetch_state',
			type:"post",
			data: {'country_name':country_name},
			success: function (resObj) {
				$("#"+attr+"_state").html('<option value="">Select State</option>'+resObj);	
				if (attr == 'cmp'){
					$("#"+attr+"_state").val('<?php echo $auctioneer_details['state']?>');
				}else{
					$("#"+attr+"_state").val('<?php echo $auctioneer_details['plant_state']?>');
				}
				$("#"+attr+"_state").selectpicker('render');
				$("#"+attr+"_state").trigger("change");
				$("#"+attr+"_state").selectpicker('refresh');
				
			},
			error: function(){
				$("#"+attr+"_state").html('<option value="">Select State</option>');
				$("#"+attr+"_state").selectpicker('refresh');
				$("#"+attr+"_state").selectpicker('render');
			}
		});	
		
	}else{
		$("#"+attr+"_state").html('<option value="">Select State</option>');
		$("#"+attr+"_state").selectpicker('refresh');
		$("#"+attr+"_state").selectpicker('render');
	}
}
function fetch_city(id){
	var state_name = $("#"+id).val();
	var attr = $("#"+id).attr('attr');
	if(state_name != ""){
		jQuery.ajax({
			url: '<?php echo base_url();?>clients/fetch_city',
			type:"post",
			data: {'state_name':state_name},
			success: function (resObj) {
				$("#"+attr+"_city").html('<option value="">Select City</option>'+resObj);	
				if (attr == 'cmp'){
					$("#"+attr+"_city").val('<?php echo $auctioneer_details['city']?>');
				}else{
					$("#"+attr+"_city").val('<?php echo $auctioneer_details['plant_city']?>');
				}						
				$("#"+attr+"_city").selectpicker('refresh');
				$("#"+attr+"_city").selectpicker('render');
			},
			error: function(){
				$("#"+attr+"_city").html('<option value="">Select City</option>');
				$("#"+attr+"_city").selectpicker('refresh');
				$("#"+attr+"_city").selectpicker('render');
			}
		});	
		
	}else{
		$("#"+attr+"_city").html('<option value="">Select City</option>');
		$("#"+attr+"_city").selectpicker('refresh');
		$("#"+attr+"_city").selectpicker('render');
	}
}	

</script>