
<!--CONTENT CONTAINER-->
<!--===================================================-->
<div id="content-container">
	<!--Page Title-->
	<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
	<div id="page-title">
		<h1 class="page-header text-overflow">Salvage Auction Images</h1>
	</div>
	<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
	<!--End page title-->


	<!--Breadcrumb-->
	<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
	<ol class="breadcrumb">
		<li><a href="<?php echo base_url('admin/main/dashboard');?>">Home</a></li>
		<li class="active"><a href="<?php echo base_url('admin/salvage/index');?>">Dashboard</a></li>
		<li class="active">Salvage Auction Images</li>
	</ol>
	<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
	<!--End breadcrumb-->
	<div id="page-content">	
		<div class="panel">
			<div class="panel-heading">
				<h3 class="panel-title">Auction Images</h3>
			</div>
			
			<div class="panel-body">
				<form class="form-horizontal"  method="POST" id="upload_frm">
					<input type='hidden' id="product_id" name="product_id" value='<?php echo $product_id; ?>' />
					<div class="form-group">
                        <div class="col-sm-2">
                            <label class="label-control">Select File : </label>
                        </div>
                        <div class="col-sm-3">
                            <input id="imageFile" type="file" multiple="multiple" name="file[]" class="form-control">
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <input class="btn btn-primary" type="submit" value='Submit' name="btn_upload_file_submit" id='btn_upload_file_submit'  />
                     </div>
                </form>
               
                
			</div>	
		</div>
        <!-- Auction images panel-->
        <div class="panel">
			<div class="panel-heading">
				<h3 class="panel-title">Uploaded Images</h3>
			</div>
			
			<div class="panel-body">
				<?php  if ( ! empty($uploaded_img[$product_id])):
            ?>
                 <div class="col-sm-12">
                <?php
                    $i=1;
					foreach ($uploaded_img[$product_id] as $id=>$singleimg):

                ?>
                    <div class="col-sm-3">
                        <div class="col-sm-2">
                            <input type="checkbox" name="img" value="<?php echo $id?>">
                        </div>
                        <div class="col-sm-6">
                            <a href="<?php echo AMAZON_BUCKET."/marketplace/images/auctionpic/salvage/".$singleimg['image_name']?>">Image <?php echo $i;?></a>
                        </div>
                    </div>
                <?php
                    $i++;
                    endforeach;
                ?>
            
                </div>
                <br/>
                <br/>
                <br/>
                <br/>
                <div class="col-sm-12">
                    <button class="btn btn-sm btn-danger" onclick="remove_img();">Remove Selected Image</button>
                </div>
            <?php 
             endif;
            ?>
            </div>
        </div>
	</div>
</div>

<!--===================================================-->
<!--END CONTENT CONTAINER-->
<script type="text/javascript">
var imgwidth = 0;
var imgheight = 0;
function ResizeImage() {
    $("#resizeimg").hide();
    $("#btn_upload_file_submit").show();
    imgwidth = document.getElementById('imgwidth').value;
    imgheight = document.getElementById('imgheight').value;
    var filesToUpload = document.getElementById('imageFile').files
	
	for (var j=0 ;j<filesToUpload.length;j++)	
	{	
		var file = filesToUpload[j];
	
		
		var reader = new FileReader();
		
		reader.onload = function(e) {
            
            var img = document.createElement("img");
            var element = document.createElement("input");
            element.type = 'hidden';
            element.name = 'img[]';
           
            var img = new Image();
 
			img.src = this.result;

            setTimeout(function(){
				var canvas = document.createElement("canvas");
				var MAX_WIDTH = imgwidth;
				var MAX_HEIGHT = imgheight;
				var width = img.width;
				var height = img.height;

				if (width > height) {
					if (width > MAX_WIDTH) {
						height *= MAX_WIDTH / width;
						width = MAX_WIDTH;
					}
				} else {
					if (height > MAX_HEIGHT) {
						width *= MAX_HEIGHT / height;
						height = MAX_HEIGHT;
					}
				}
				canvas.width = width;
				canvas.height = height;
				var ctx = canvas.getContext("2d");
				ctx.drawImage(img, 0, 0, width, height);
				
				var dataurl = canvas.toDataURL("image/jpeg");
				
				img.src = dataurl;
				element.value = dataurl;
				document.getElementById("demo").appendChild(img);
				document.getElementById("demo").appendChild(element);

			},1000);
        }
        // Load files into file reader
		
		reader.readAsDataURL(file);
	}
	
}
function remove_img()
{
    var img = '';
	if( $('input[name="img"]:checked').length > 0)
	{
		$("input:checkbox[name='img']:checked").each(function(){
			img+=$(this).val()+',';
		});
       
        if (img)
        {
            $.ajax({
				url:"<?php echo base_url('admin/salvage/delete_images/');?>",
				type: 'post',
				data:{'img':img},
				dataType:'json',
				success: function (response)
				{ 
					if (response.status=="success")
					{
						Display_msg(response.message,response.status);
						window.location.reload();                        
					}
					else
					{	
						Display_msg(response.message,response.status);
						return false;
					}

				}
            });
    	}
   }
   else
   {
       alert('kindly select image');
   }
   
}
$(function(){
	var vRules = {
		'file[]':{required:true}
	};
	var vMessages = {
		'file[]':{required:"<p class='text-danger'>Please select file.</p>"}
	};
	$("#upload_frm").validate({
		rules: vRules,
		messages: vMessages,
		submitHandler: function(form){
			$(form).ajaxSubmit({
				url:"<?php echo base_url('admin/salvage/upload_auction_images/');?>",
				type: 'post',
				dataType:'json',
				cache: false,
				clearForm: true,
				success: function (response) {               
					if(response.status=="success")
					{
						Display_msg(response.message,response.status);
						
						setTimeout(function()
						{
							window.location.reload();
						}, 3000); 						
					}
					else
					{	
						Display_msg(response.message,response.status);
						return false;
					}
				}
			});
		}
	});
})

</script>