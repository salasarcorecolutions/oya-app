<!--CONTENT CONTAINER-->
<!--===================================================-->
<div id="content-container">
	<!--Page Title-->
	<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
	<div id="page-title">
		<h1 class="page-header text-overflow">Salvage Auction Reports</h1>
	</div>
	<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
	<!--End page title-->


	<!--Breadcrumb-->
	<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
	<ol class="breadcrumb">
		<li><a href="admin.php">Home</a></li>
		<li><a class="active">Salvage Auction Reports</a></li>
	</ol>
	<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
	<!--End breadcrumb-->

	<!--Page content-->
	<!--===================================================-->
	<div id="page-content">
		<div class="row">
			<div class="col-sm-12">
		
			
				<!--Primary Panel-->
				<!--===================================================-->
				<div class="panel panel-primary">
					<div class="panel-heading">
						<h3 class="panel-title">Salvage Auction Reports</h3>
					</div>
					<!--Horizontal Form-->
					<!--===================================================-->
					
					<div class="panel-body" >
						<form method="post" name="frm" id="frm"   target="_blank" >
							<div class="col-lg-12">
								
									<div class="col-sm-3">
										<div class="form-group">
											<label class="control-label">Auction Id :</label>
											<select class="form-control" size="1" name="cboauctionid" id="cboauctionid" >
											<option value="" >--select--</option>
											<?php 
												if ( ! empty($get_all_salvage_auction))
												{
													foreach($get_all_salvage_auction as $single_auc)
													{
											?>
													<option value="<?php echo $single_auc['product_id']?>"><?php echo $single_auc['product_name'] .'  [' .date('Y-m-d' ,strtotime($single_auc['start_date'])).' ]  [ '.$single_auc['vendor_name'].' ]'.'['.$single_auc['sale_no'].']'?></option>
											
											<?php
													}
												}
											
											?>
											</select>
											
										</div>
									</div>
								

								<input type='hidden' name="auctionid" id="auctionid" value="">
								<input type='hidden' name="clientid" id="clientid" value="">
							</div>
							<div id="checkboxlist">
								<div class="col-lg-4 normal_auction">
									<div class="row">
										<div class="form-group">
											
											<?php  echo "<input type='checkbox' class='checked_lot'  name='client_wise_lot' value='client_wise_lot'>&nbsp;&nbsp;&nbsp;<input class=\"btn btn-primary\" type=\"button\" value=\"PARTICIPATED BIDDER\" id=\"B6\"  ><br /><br />"; ?>

											<?php  echo "<input type='checkbox' class='checked_lot'  name='lotwise_bid_log' value='lotwise_bid_log'>&nbsp;&nbsp;&nbsp;<input class=\"btn btn-primary\" type=\"button\" value=\"BID LOG\" id=\"B7\"  ><br /><br />"; ?>

								
											<?php  echo "<input type='checkbox' class='checked_lot' name='h1_n_total_bids' value='h1_n_total_bids'>&nbsp;&nbsp;&nbsp;<input type=\"button\" class=\"btn btn-primary\" value=\"H1 AND TOTAL BIDS\" id=\"B12\" ><br /><br />";?>


											<?php 
											echo "<input type='checkbox' class='checked_lot'  name='bid_summary' value='bid_summary'>&nbsp;&nbsp;&nbsp;<input type=\"button\" class=\"btn btn-primary\" value=\"BID SUMMARY\" id=\"B991\"  style=\"width:285px\"><br /><br />";?>

											<?php 
											echo "<input type='checkbox' class='checked_lot'  name='product_details' value='product_details'>&nbsp;&nbsp;&nbsp;<input type=\"button\" class=\"btn btn-primary\" value=\"PRODUCT DETAILS\" id=\"B991\"  style=\"width:285px\"><br /><br />";?>

											<?php 
											echo "<input type='checkbox' class='checked_lot'  name='product_on_auction' value='product_on_auction'>&nbsp;&nbsp;&nbsp;<input type=\"button\" class=\"btn btn-primary\" value=\"PRODUCT ON AUCTION\" id=\"B991\"  style=\"width:285px\"><br /><br />";?>




											
										</div>
									</div>
								</div>
								<div class="col-lg-4 normal_auction">
									<div class="row">
										<div class="form-group">
											<input type="button" class="btn btn-success" value="Export In Excel" onclick="export_in_excel()">
		
										</div>
									</div>
									<div class="row">
										<div class="form-group">
											<input type="button" class="btn btn-info" value="Export In PDF" onclick="export_in_pdf()">
										</div>
									</div>
								</div>
								
							</div>
							<input type="hidden" name="save_file" id="save_file" value="0"/>
						</form>	
					</div>
						
					<!--===================================================-->
					<!--End Horizontal Form-->
				</div>
				<!--===================================================-->
				<!--End Primary Panel-->
		
			
		
			</div>
		
		</div>	
	</div>
	<!--===================================================-->
	<!--End page content-->


</div>
<!--===================================================-->
<!--END CONTENT CONTAINER-->	

<script type="text/javascript">

function export_in_pdf(){
	var theForm = $("#frm");
	if (document.getElementById("cboauctionid").selectedIndex==0){
		alert("Please select Auction");
		document.getElementById("cboauctionid").focus();
		return false;
	}
	var chkArray = [];
	var selected;
	/* look for all checkboes that have a parent id called 'checkboxlist' attached to it and check if it was checked */
	$(".checked_lot:checkbox:checked").each(function() {
		chkArray.push($(this).val());
		/* we join the array separated by the comma */
	});
		
	/* check if there is selected checkboxes, by default the length is 1 as it contains one single comma */
	if(chkArray.length > 0)
	{
		if (chkArray.length == 1){
			$('#save_file').val('0');
		} else {
			$('#save_file').val('1');
		}
		$(theForm).attr('action', '<?php echo base_url('admin/salvage_auction_reports/get_salvage_reports_in_pdf')?>');
		$(theForm).submit();
	} else {
		alert("Please at least check one of the checkbox");
	}

}

function export_in_excel()
{
	var theForm = $("#frm");
	if(document.getElementById("cboauctionid").selectedIndex==0)
	{
		alert("Please select Auction");
		document.getElementById("cboauctionid").focus();
		return false;
	}
	var chkArray = [];

	/* look for all checkboes that have a parent id called 'checkboxlist' attached to it and check if it was checked */
	$("#checkboxlist input:checked").each(function() {
		chkArray.push($(this).val());
	});
	
	/* we join the array separated by the comma */
	var selected;
	selected = chkArray.join(',');
	
	/* check if there is selected checkboxes, by default the length is 1 as it contains one single comma */
	if(selected.length > 0){
		$(theForm).attr('action', '<?php echo base_url('admin/salvage_auction_reports/get_salvage_reports_in_excel')?>');
		$(theForm).submit();
	}
	else
	{
		alert("Please at least check one of the checkbox");	
	}
	
}
</script>


<style type="text/css">
#livesearch
  {
  margin:0px;
  width:200px;
  text-align: left;
  padding:5px;
  z-index:10;
  position:absolute;
  background:#fff;
  
  }
  #livesearch a{padding:2px;margin:1px;width:100%}
  #livesearch a:hover{background:#00FF00}
#txt1
  {
  margin:0px;
  }
</style>
<style>
.btn-primary{
	min-width:288px !important;
}
</style>