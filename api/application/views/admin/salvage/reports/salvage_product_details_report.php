<style>
tr {
	page-break-inside: avoid;
}
thead {
	display: table-header-group;
}
tfoot {
	display: table-row-group;
}
tr {
	page-break-inside: avoid;
}
td.wrappable,table.data_table td.wrappable {
    white-space: normal;	
}            
table td,table th {
	font-size:10px;
	
	
}
</style>


<page backbottom='110px' backtop="0px" backleft="0px" backright="0px" style="font-size:10px;">
   <page_footer>
        <table style='width: 100%;'>
			<tr>
				<td align='left' style='float:left;width:200px;'>
				Page :  [[page_cu]]/[[page_nb]] <br/>
				<?php echo (new DateTime(null, new DateTimeZone($_SESSION['user_tz'])))->format('D dS \of M Y h:i:s A'); ?>
				
				</td>
				<td align='right' style='float:right;width:545px;'>
					
				</td>
			</tr>
		</table>
    </page_footer>
	<br>
	<table border='0.5' cellspacing='0' cellpadding='0' width='100%'>
		<tbody>
			<tr>
				<td style="width:550px;" >
								
				</td>
				<td align='center' style="width:180px;">
					<span style='font-size:20px;color:#2a5395;'><strong><?php echo PRODUCT ?></strong></span>
				</td>
			</tr>
			<tr>
				<td colspan='2' align='center' >
					<strong>PRODUCT DETAILS REPORT FOR AUCTION :  <?php echo $common_auction_details[0]['sale_no']; ?> </strong>
				</td>
			</tr>
			<tr>
				<td colspan='2' align='center' >
					<strong><?=strtoupper($common_auction_details[0]['vendor_name'])?></strong>
				</td>
			</tr>
			<tr>
				<td colspan='2' align='center' >
					<strong>SALVAGE AUCTION DETAILS</strong>
				</td>
			</tr>
			<tr>
				<td colspan='2' align='left'>
					<strong>Product No :</strong><?php echo $common_auction_details[0]['product_id']?>
				</td>
			</tr>
			<tr>
				<td colspan='2' align='left'>
					<strong>Auction Type :  </strong>Salvage Auction
				</td>
			</tr>
			<tr>
				<td colspan='2' align='left'>
					<strong>Start Date : </strong><?php echo $common_auction_details[0]['start_date']?>
				</td>
			</tr>
			<tr>
				<td colspan='2' align='left'>
					<strong>Particulars : </strong><?php echo $common_auction_details[0]['short_description']?>

				</td>
			</tr>
		</tbody>		
	</table>
	<br>

	<?php 
	if ( ! empty($product_details['product_details']))
		{
		
	?>
	<table border='0.5' cellspacing='0' cellpadding='0'>
			<thead>
				<tr>
					<td colspan="9" align="center">
						Product <?php echo $product_details['product_details']['product_name']?>
					</td>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td align='center' class='wrappable' style="font-weight:bold;" bgcolor="#e0e0e0" >Model No</td>
					<td align='center' class='wrappable' style="font-weight:bold;" bgcolor="#e0e0e0" >Manufacturer Year</td>
					<td align='center' class='wrappable' style="font-weight:bold;" bgcolor="#e0e0e0" >Dimension</td>
					<td align='center' class='wrappable' style="font-weight:bold;" bgcolor="#e0e0e0" >Serial No</td>
					<td align='center' class='wrappable' style="font-weight:bold;" bgcolor="#e0e0e0" >Price</td>
					<td align='center' class='wrappable' style="font-weight:bold;" bgcolor="#e0e0e0" >URL</td>
					<td align='center' class='wrappable' style="font-weight:bold;" bgcolor="#e0e0e0" >START DATETIME</td>
					<td align='center' class='wrappable' style="font-weight:bold;" bgcolor="#e0e0e0" >END DATETIME</td>

				</tr>
				<tr>

					<td style="width:50px;"><?php echo $product_details['product_details']['model_no']?></td>
					<td style="width:50px;"><?php echo $product_details['product_details']['manufacturer_year']?></td>
					<td style="width:50px;"><?php echo $product_details['product_details']['dimension']?></td>
					<td style="width:50px;"><?php echo $product_details['product_details']['serial_no']?></td>
					<td style="width:50px;"><?php echo $product_details['product_details']['price'].$product_details['product_details']['price_currency']?></td>
					<td style="width:92px;"><?php echo $product_details['product_details']['url']?></td>
					<td style="width:90px;padding-top:10px;"><?php echo $product_details['product_details']['start_date']." ".$product_details['product_details']['start_time']?></td>
					<td style="width:92px;padding-top:10px;"><?php echo $product_details['product_details']['end_date']." ".$product_details['product_details']['end_time']?></td>

				</tr>

			</tbody>
	</table>
	<?php

		}
	?>
	<br/>
	<?php 
	if ( ! empty($product_details['section_details']))
		{
			foreach($product_details['section_details'] as $key=>$value)
			{
	?>
	<table border='0.5' cellspacing='0' cellpadding='0'>
		<thead>
			<tr>
				<td colspan="2" align="center">
					Product <?php echo $value['section_name']?>
				</td>
			</tr>
		</thead>
		<tbody>
			<tr>
				<td style="width:737px;" align="center"><?php echo strip_tags($value['section_description']); ?></td>
			</tr>

		</tbody>
	</table><br/>
	<?php
				}
		}
	?>
	<?php 
	
/* 	if ( ! empty($product_details['product_img']))
		{
			 */
	?>
	<!-- <table border='0.5' cellspacing='0' cellpadding='0'>
			<thead>
				<tr>
					<td style="width:740px;">
						Product Image
					</td>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td>
				<?php
				foreach($product_details['product_img'] as $key=>$value)
				{
				?>
				
					<img src="<?php echo AMAZON_BUCKET."/marketplace/images/auctionpic/salvage/".$value['image_name']?>"  width="200px;"/>
					
					

				<?php
				}
				?>
					</td>
				</tr>
			</tbody>
	</table> -->
	<?php
				
	//	}
	?>
	
</page>
