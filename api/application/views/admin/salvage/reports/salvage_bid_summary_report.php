<style>
tr {
	page-break-inside: avoid;
}
thead {
	display: table-header-group;
}
tfoot {
	display: table-row-group;
}
tr {
	page-break-inside: avoid;
}
td.wrappable,table.data_table td.wrappable {
    white-space: normal;	
}            
table td,table th {
	font-size:10px;
	
	
}
</style>


<page backbottom='110px' backtop="0px" backleft="0px" backright="0px" style="font-size:10px;">
   <page_footer>
        <table style='width: 100%;'>
			<tr>
				<td align='left' style='float:left;width:200px;'>
				Page :  [[page_cu]]/[[page_nb]] <br/>
				<?php echo (new DateTime(null, new DateTimeZone($_SESSION['user_tz'])))->format('D dS \of M Y h:i:s A'); ?>
				
				</td>
				<td align='right' style='float:right;width:545px;'>
					
				</td>
			</tr>
		</table>
    </page_footer>
	<br>
	<table border='0.5' cellspacing='0' cellpadding='0' width='100%'>
		<tbody>
			<tr>
				<td style="width:550px;" >
								
				</td>
				<td align='center' style="width:180px;">
					<span style='font-size:20px;color:#2a5395;'><strong><?php echo PRODUCT ?></strong></span>
				</td>
			</tr>
			<tr>
				<td colspan='2' align='center' >
					<strong>BID SUMMARY REPORT FOR AUCTION :  <?php echo $common_auction_details[0]['sale_no']; ?> </strong>
				</td>
			</tr>
			<tr>
				<td colspan='2' align='center' >
					<strong><?=strtoupper($common_auction_details[0]['vendor_name'])?></strong>
				</td>
			</tr>
			<tr>
				<td colspan='2' align='center' >
					<strong>SALVAGE AUCTION DETAILS</strong>
				</td>
			</tr>
			<tr>
				<td colspan='2' align='left'>
					<strong>Product No :</strong><?php echo $common_auction_details[0]['product_id']?>
				</td>
			</tr>
			<tr>
				<td colspan='2' align='left'>
					<strong>Auction Type :  </strong>Salvage Auction
				</td>
			</tr>
			<tr>
				<td colspan='2' align='left'>
					<strong>Start Date : </strong><?php echo $common_auction_details[0]['start_date']?>
				</td>
			</tr>
			<tr>
				<td colspan='2' align='left'>
					<strong>Particulars : </strong><?php echo $common_auction_details[0]['short_description']?>

				</td>
			</tr>
		</tbody>		
	</table>
	<br>
	
	<?php 
	
		if (!empty($bid_summary))
		{
			foreach ($bid_summary as $value)
			{
	?>
			<table border='0.5' cellspacing='0' cellpadding='0'>
					
					<tbody>
						<tr>
							<td align='center' class='wrappable'  style="width:105px;"><strong>Start Time : </strong> <br> <?php echo $value['st'];?> </td>
							<td align='center' class='wrappable'  style="width:105px;"  > <strong>End Time : </strong>  <br> <?php echo $value['et'];?></td>
							<td align='center' class='wrappable'  style="width:105px;"  > <strong>Model No : </strong>  <br> <?php echo $value['model_no'];?></td>
							<td align='center' class='wrappable' colspan="3" style="width:105px;"  > <strong>Product Name : </strong>  <br> <?php echo $value['product_name'];?></td>


						</tr>
						<tr>
							
							
						</tr>
						<tr>
							<td align='center' class='wrappable'  bgcolor="#e0e0e0" style="font-weight:bold;width:100px;">STATUS</td>
							<td align='center' class='wrappable' bgcolor="#e0e0e0"  style="font-weight:bold;width:190px;">BIDDER NAME</td>
							<td align='center' class='wrappable' bgcolor="#e0e0e0"  style="font-weight:bold;width:100px;">DATE</td>
							<td align='center' class='wrappable' bgcolor="#e0e0e0"  style="font-weight:bold;width:100px;" >TIME</td>
							<td align='center' class='wrappable' bgcolor="#e0e0e0"  style="font-weight:bold;width:100px;">AMOUNT</td>
							<td align='center' class='wrappable' bgcolor="#e0e0e0"  style="font-weight:bold;width:100px;"> NO. OF BIDS </td>
						</tr    >

				 		<?php
				 			 if (isset($value['lot_details']))
			 				{
								foreach ($value['lot_details'] as $key=>$lot_bid)
								{
						?>
								<tr>
									<td align="center" class='wrappable' style="width:100px;color:red;" >
										<?php echo $lot_bid['h']; ?>
									</td>
									<td align='center' class='wrappable' style="width:190px;"  >
										<?php echo $lot_bid['cname']; ?> 
									</td>
									<td align="center" class='wrappable' style="width:100px;" >
										<?php echo $lot_bid['dt']; ?>
									</td>
									<td align="center" class='wrappable' style="width:100px;" >
										<?php echo $lot_bid['tm']; ?> Hrs.
									</td>
									<td align="center"class='wrappable' style="width:100px;">
										<?php echo $lot_bid['amt']; ?>
									</td>
									<td align="center" class='wrappable' style="width:100px;"  >
										<?php echo $lot_bid['nob']?> 
									</td>
								</tr>
									
						<?php
								}

							}
							else
							{
						?>
								<tr>
									<td align="center" class='wrappable' colspan="6"  style="width:700px;" >No Bid</td>
								</tr>
						
						<?php
							}
						
						?>
					
					</tbody>
			</table>
			<br>
				
	
	<?php
				
			}
			
		}
	
	
	?>
	
</page>
