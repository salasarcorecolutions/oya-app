<!--CONTENT CONTAINER-->
<!--===================================================-->
<div id="content-container">
	<!--Page Title-->
	<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
	<div id="page-title">
		<h1 class="page-header text-overflow"><?php echo $pagetitle; ?></h1>
	</div>
	<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
	<!--End page title-->

	<!--Breadcrumb-->
	<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
	<ol class="breadcrumb">
		<li><a href="<?php echo base_url('admin/main/dashboard');?>">Home</a></li>
		<li class="active"><?php echo $pagetitle; ?></li>
	</ol>
	<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
	<!--End breadcrumb-->
	<div id="page-content">
		<div class="panel">
			<div class="panel-heading">
				<h3 class="panel-title"><?php echo $pagetitle; ?></h3>
			</div>

			<div class="panel-body">
                <button style="margin-bottom: 1%;" id="add_section" type="button" class="btn btn-primary pull-right">Add Section</button>
                <br>
                <table id="section_table" class="table table-striped table-bordered table-responsive table-hover">
                    <thead>
                        <tr>
                            <th>Sr. No.</th>
                            <th>Product Name</th>
                            <th>Section Name</th>
                            <th>Section Description</th>
                            <th>Updated By</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php $i = 1; foreach ($custom_sections as $value){ ?>
                        <tr>
                            <td><?php echo $i++; ?></td>
                            <td><?php echo $value['product_name']; ?></td>
                            <td><?php echo $value['section_name']; ?></td>
                            <td><?php echo $value['section_description']; ?></td>
                            <td><?php echo $value['updated_by']; ?></td>
                            <td><a id="edit_<?php echo $value['section_id']; ?>" onclick="editSection('<?php echo $value['section_id']; ?>')" product_name="<?php echo $value['product_name']; ?>" class="btn btn-primary" section_name="<?php echo $value['section_name']; ?>" section_description="<?php echo $value['section_description']; ?>" product_id="<?php echo $value['product_id']; ?>">Edit</a><br>
                            <a onclick="deleteSection('<?php echo $value['section_id']; ?>')" class="btn btn-danger">Delete</a></td>
                        </tr>
                        <?php } ?>
                    <tbody>
                </table>
               
			</div>
		</div>
	</div>
</div>
 <!-- Modal -->
 <div id="section_modal" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Add/Edit Product Custom Section</h4>
            </div>
            <div class="modal-body">
                <form id="section_modal_form" class="form-group">
                    <div>
                        <input id="section_id" name="section_id" type="hidden" />
                        <input id="product_id" name="product_id" type="hidden" value="<?php echo $section_id; ?>" />
                    <div>
                        <label for="section_name">Section Name</label>
                        <input id="section_name" class="form-control" name="section_name" type="text" />
                    </div>
                    <div>
                        <label for="section_description">Section Description</label>
                        <textarea id="section_description" class="form-control ckeditor" name="section_description"></textarea>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <a id="add_section_data" class="btn btn-primary" onclick="submit_form();">Save</a>
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
</div>

<!--===================================================-->
<!--END CONTENT CONTAINER-->
<script src="https://cdn.ckeditor.com/4.14.0/standard/ckeditor.js"></script>

<script>
    $(document).ready(function(){

        $("#section_table").dataTable();
    });

    $("#add_section").click(function(){
        $("#section_modal").modal('show');
    });

    $("#add_section_data").click(function(){
        $("#section_modal_form").submit();
    });
function submit_form()
{
   
    var vRules = {
        product_id:{required:true},
        section_name:{required:true},
        section_description:{ 
					required: function() 
                        {
                         CKEDITOR.instances.section_description.updateElement();
                        },

                     minlength:10
                    }
    };

    var vMessages = {
        product_id:{required:"Please select product name"},
        section_name:{required:"Please enter section name"},
        section_description:{required:"Please enter section description"},
    };

    $("#section_modal_form").validate({
        ignore:[],
        rules: vRules,
        messages: vMessages,
        submitHandler: function(form){
            $("#loading").show();
            for (instance in CKEDITOR.instances) {
                CKEDITOR.instances[instance].updateElement();
            }
            $(form).ajaxSubmit({
                url: '<?php echo base_url();?>admin/salvage/add_edit_section',
                type:"post",
                dataType: 'json',
                success: function (result) {
                    $("#loading").hide();
                    if (result.status == 'success'){
                       
						Display_msg(result.message, result.status);
                        setTimeout(function(){ window.location.reload(1); }, 3000);
					} else {
						Display_msg(result.message, result.status);
					}
                },
                error: function(e){
                    alert('Invalid request please try again.');
                }
            });
        }
    });
}
    

    function editSection(id)
    {
        $("#section_id").val(id);
        $("#section_name").val($("#edit_"+id).attr("section_name"));
        CKEDITOR.instances["section_description"].setData($("#edit_"+id).attr("section_description"));
        $("#section_modal").modal('show');
    }

    $("#change_image").click(function(){
        $(".img").show();
    });

    $("#section_modal").on('hide.bs.modal', function(){
        clearAll();
    });

    function clearAll()
    {
        $("#section_id").val("");
        $("#section_name").val("");
        $("#section_description").val("");
    }

    function deleteSection(id)
    {
        $("#loading").show();
        $.ajax({
            url: '<?php echo base_url();?>admin/salvage/delete_section',
            type:"post",
            data:{ id: id },
            dataType: 'json',
            success: function (result) {
                $("#loading").hide();
                if (result.status == 'success'){
                    Display_msg(result.message, result.status);
                    setTimeout(function(){ window.location.reload(1); }, 3000);
                } else {
                    Display_msg(result.message, result.status);
                }
            },
            error: function(e){
                alert('Invalid request please try again.');
            }
        });
    }

</script>