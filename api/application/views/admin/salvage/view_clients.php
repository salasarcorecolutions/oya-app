<!--CONTENT CONTAINER-->
<!--===================================================-->
<div id="content-container">
	<!--Page Title-->
	<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
	<div id="page-title">
		<h1 class="page-header text-overflow">Salvage Auction Clients</h1>
	</div>
	<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
	<!--End page title-->

	<!--Breadcrumb-->
	<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
	<ol class="breadcrumb">
		<li><a href="<?php echo base_url('dashboard');?>">Home</a></li>
		<li class="active"><a href="<?php echo base_url('admin/salvage/index');?>">Salvage Auction</a></li>
		<li class="active">Salvage Auction Clients</li>
	</ol>
	<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
	<!--End breadcrumb-->
	<input type="hidden" id="t11" name="auctionId">
	<div id="page-content">
		<div class="panel">
			<div class="panel-heading">
				<h3 class="panel-title">Salvage Auction Clients</h3>
			</div>

			<div class="panel-body">
					<div class="pad-btm form-inline">
					<div class="row">
						<div class="col-sm-12 table-toolbar-right">
							<a class="btn btn-primary btn-labeled fa fa-plus" href="<?php echo base_url('admin/salvage/add_edit_clients/'.$auctionid); ?>" title="Add Client" name="btn_add_client" id="btn_add_client">Add Client</a>
							
							<a class="btn btn-warning btn-labeled fa fa-plus" href="<?php echo base_url('admin/salvage/view_participation_request/'.$auctionid); ?>" title="Add Client" name="btn_view_participation_request" id="btn_view_participation_request">View Participation Request From Client</a>
						</div>
					</div>
				</div>
				<div class="table-responsive">
					<table class="table table-striped table-bordered table-hover no-footer dtr-inline ">
						<thead>
							<tr>
								<th style='width:3%;'>SR</th>
								<th style='width:20%;'>BIDDER</th>
								<th>ACTION</th>
							</tr>
						</thead>
						<tbody>
						<?php

						if ( ! empty($client_list))
						{
							$sr = 1;
							
							$delete_permission = true;
							
							foreach($client_list as $single_client)
							{
								$action = '';

								
								if($delete_permission)
								{
									$action .= '<a title="Delete Client" href="javascript:void(0)" onclick="delete_client(\''.$single_client['bidder_id'].'\',\''.$auctionid.'\')">Delete</a><br>';
								}
								?>
								<tr>
									<td><?php echo $sr++; ?></td>
									<td><?php echo $single_client['compname'].' ['.$single_client['conperson'].']'; ?></td>
									<td><?php echo $action;?></td>
								</tr>
							<?php
							}
						}
						else
						{
							?>
							<tr>
								<td colspan='10' align='center'>No Records Found</td>
							</tr>
							<?php
						}
						?>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>

<!--===================================================-->
<!--END CONTENT CONTAINER-->
<script>

function delete_client(id,auctionid)
{
	if(id > 0)
	{
		var ans = confirm('Are you sure you want to edit this client?');
		if(ans)
		{
			$.ajax(
			{
				url: "<?php echo base_url('admin/salvage/delete_auction_client'); ?>",
				type: "POST",
				dataType:"json",
				cache: false,
				clearForm: false,
				data:{"client_id":id,'auctionid':auctionid},
				success:function(response)
				{
					if(response.status == "success")
					{
						Display_msg(response.message,response.status);
						setTimeout(function()
						{
							window.location.reload();
						}, 3000);
					}
					else
					{
						Display_msg(response.message,response.status);
						return false;
					}
				}
			});
		}
	}
	else
	{
		Display_msg('Please Select Auction.','error');
		return false;
	}
}
</script>
