<script language="javascript" type="text/javascript" src="<?php echo base_url('assets/js/ajax.js'); ?>"></script> 
<style>
  a.btn{
	 
	  text-align:left;
	  margin-top:5px;
  }
  .btn-group{
		  margin-left:19px;
	  }
	.fa-upload{
	margin-left:5px !important;
	}
	.auction-msg{
		float:right !important;
	}
	.auction-live{
		float:right !important;
	}
	.btn-group:first-child{
		margin-left:0px;
	}
  @media (max-width:990px){
	  a.btn{
		  margin-top:10px;
		  width:250px;
	  }
	  .btn-group{
		  margin-left:0;
	  }
	  .fa-upload{
		margin-left:0 !important;
	  }
	  .auction-msg{
		float:left !important;
	}
	.auction-live{
		float:left !important;
	}
  }
</style>
<!--CONTENT CONTAINER-->
<!--===================================================-->
<div id="content-container">
	<!--Page Title-->
	<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
	<div id="page-title">
		<h1 class="page-header text-overflow"><?php echo $pagetitle?></h1>
	</div>
	<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
	<!--End page title-->


	<!--Breadcrumb-->
	<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
	<ol class="breadcrumb">
		<li><a href="<?php echo base_url('dashboard'); ?>">Home</a></li>
		<li class="active">Salvage Auction</li>
	</ol>
	<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
	<!--End breadcrumb-->

	<div id="page-content">	
		<div class="panel">
			<div class="panel-heading">
				<h3 class="panel-title"><?php echo $pagetitle?></h3>
			</div>
			
			<div class="panel-body">
				
				<form name="salvage_auction_form" method="post" >
					<div class="table-responsive">
						<table callfunction="<?php echo base_url('admin/salvage/fetch_requested_bidder/').$auctionid;?>" class="table table-striped table-bordered table-hover no-footer dtr-inline dynamicTable">
							<thead>
								<tr>
									<th data-bSortable="false">#</th>
									<th>BIDDER NAME</th>
									<th>DATE REQUESTED</th>
									<th>NOTE</th>
									<th>ACCEPT CLIENT</th>
								</tr>
							</thead>
							<tbody>
							</tbody>
						</table>
					</div>

				</form>
			</div>	
		</div>
	</div>	
</div>


<!--===================================================-->
<!--END CONTENT CONTAINER-->
<script>
function add_client(product_id,bidder_id)
{
	$.ajax({
		url:"<?php echo base_url('admin/salvage/add_client/');?>",
		type: 'post',
		data:{auctionid:product_id,client_id:bidder_id,note:'Added via vendor'},
		dataType:'json',
		success:function(response)
		{
			Display_msg(response.message,response.status);
			setTimeout(function()
			{
				window.location.href="<?php echo base_url('admin/salvage/view_participation_request/'.$auctionid);?>";
			}, 3000);
		},
		error:function()
		{
			alert('Invalid Request');
		}
	});

}
</script>