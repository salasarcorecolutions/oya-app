<style>
 a.btn{
    text-align:left;
    margin-top:5px;
 }
 .btn-group{
		 margin-left:2px;
	 }
   .fa-upload{
   margin-left:5px !important;
   }
   .auction-msg{
	   float:right !important;
   }
   .auction-live{
	   float:right !important;
   }
   .btn-group:first-child{
		margin-left:0px;
	}
 @media (max-width:990px){
	 a.btn{
		 margin-top:10px;
		 width:250px;
	 }
	 .btn-group{
		 margin-left:0;
	 }
	 .fa-upload{
	   margin-left:0 !important;
	 }
	 .auction-msg{
	   float:left !important;
   }
   .auction-live{
	   float:left !important;
   }
 }
 .ellipsis {
        width: 200px;
    }

	.ellipsis span {
		display: -webkit-box;
		-webkit-line-clamp: 4;
		-webkit-box-orient: vertical;
		overflow: hidden;
	}
</style>
<!-- In order to let it work in CI, need to fix it to only get the viwes -->

<!--CONTENT CONTAINER-->
<!--===================================================-->
<div id="content-container">
	<!--Page Title-->
	<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
	<div id="page-title">
		<h1 class="page-header text-overflow"><?php echo $pagetitle; ?></h1>
	</div>
	<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
	<!--End page title-->


	<!--Breadcrumb-->
	<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
	<ol class="breadcrumb">
		<li><a href="<?php echo base_url('admin/main/dashboard');?>">Home</a></li>
		<li class="active"><?php echo $pagetitle; ?></li>
	</ol>
	<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
	<!--End breadcrumb-->
	<input type="hidden" id="t11" name="auctionId">
	<div id="page-content">	
		<div class="panel">
			<div class="panel-heading">
				<h3 class="panel-title"><?php echo $pagetitle; ?></h3>
			</div>
			
			<div class="panel-body">
				<div class="pad-btm form-inline">
  					<div class="btn-group pull-center">
					  	<a class="btn btn-primary btn-labeled fa fa-plus" href="javascript:void(0);" title="Add Auction" name="btn_add" id="btn_add" onclick="javascript:auction_actions('ADD')">Add</a>
						<a class="btn btn-info btn-labeled fa fa-edit" href="javascript:void(0);" title="Edit Auction" name="btn_edit" id="btn_edit" onclick="javascript:auction_actions('EDIT')">Edit</a>
						<a class="btn btn-danger btn-labeled fa fa-warning" href="javascript:void(0);" title="Delete Auction" name="btn_delete" onclick="javascript:delete_auction()">Delete</a>
					</div>
					<div class="btn-group pull-center">

							<a class="btn btn-primary btn-labeled fa fa-envelope" href="javascript:void(0);" title="Auction Images" name="btn_auction_images" onclick="javascript:auction_actions('AUCTION_IMAGES')">Auction Images</a>

					</div>
					<div class="btn-group pull-center">

							<a class="btn btn-primary btn-labeled fa fa-envelope" href="javascript:void(0);" title="CUSTOM SECTION" name="btn_auction_images" onclick="javascript:auction_actions('CUSTOM_SECTION')">Custom Additional Information</a>

					</div>
					<div class="btn-group pull-center">
						<a class="btn btn-info btn-labeled fa fa-upload" href="javascript:void(0);" title="Add Permission" name="btn_add_permission" onclick="javascript:auction_actions('ADD_PERMISSION')">Permission</a>
					</div>
					<div class="btn-group pull-center">
						<a class="btn btn-info btn-labeled fa fa-upload" href="javascript:void(0);" title="Add Document" name="add_document" onclick="javascript:auction_actions('ADD_DOCUMENT')">UPLOAD DOCUMENT</a>
					</div>
					<div class="btn-group pull-center">
						<a class="btn btn-info btn-labeled fa fa-upload" href="javascript:void(0);" title="Add Document" name="add_document" onclick="javascript:auction_actions('ADD_CLIENT')">Add Client</a>
					</div>
				</div>
				<form name="rev_auction_form" method="post" >
					<div class="row filter">
						<div class="col-sm-2">
							<input class="form-control searchInput" placeholder="Sale No" type="text" name="search_sale_no" id="search_sale_no" value="" />
						</div>
						<div class="col-sm-2">
							<input class="form-control addDatePicker searchInput" readonly placeholder="Start Date" type="text" name="search_start_date" id="search_start_date" value="" />
						</div>
						<div class="col-sm-2">
							<input class="form-control addDatePicker searchInput" readonly placeholder="End Date" type="text" name="search_end_date" id="search_end_date" value="" />
						</div>
						<div class="col-sm-2">
							<select class="form-control searchInput" name="buying_format">
								<option value="">--Select Buying Format--</option>
								<option value="Live Auction">Live Auction</option>
								<option value="Make Offer">Make Offer</option>
								<option value="Online Auction">Online Auction</option>
								<option value="Buy Now">Buy Now</option>
								<option value="Sailed Bid">Sailed Bid</option>
							</select>
						</div>
						<div class="col-sm-2">
							<input class="form-control searchInput" placeholder="Product Name" type="text" name="search_product_name" id="search_auction_type" value="" />
						</div>
						<div class="col-sm-2">
							<input class="form-control searchInput" placeholder="Manufacturer" type="text" name="search_manufacture" id="search_particulars" value="" />
						</div>

						
						<div class="col-sm-2 pull-right">
							<a style=" float: left;" class="btn btn-default pull-right" onclick="refresh_datatable();">Refresh </a>
							<a style=" float: left;" class="btn btn-info pull-right" onclick="clearSearchFilters();">Clear Search </a>
						</div>
					</div>
					<div class="table-responsive">
						<table callfunction="<?php echo base_url('admin/salvage/fetch');?>" class="table table-striped table-bordered table-hover no-footer dtr-inline dynamicTable">
							<thead>
								<tr>
									<th data-bSortable="false">#</th>
									<th>SALE NO</th>
									<th>START DATE</th>
									<th>END DATE</th>
                                    <th>PRODUCT NAME</th>
									<th>DESCRIPTION</th>
									<th>PARENT CATEGORY</th>
									<th>MANUFACTURER</th>
									<th>BUYING FORMAT</th>
									<th>PUBLISHED</th>
								</tr>
							</thead>
							<tbody>
							</tbody>
							<input type="hidden" id="vendorid" name="vendorid">
						</table>
					</div>	
				</form>
			</div>
		</div>
		<div id="upload_file_modal" class="modal fade" role="dialog">
			<div class="modal-dialog">
				<div class="modal-content">
					<form name="upload_file_form" id="upload_file_form" method="post" >
						<input type='hidden' id='upload_file_type' name='upload_file_type' value=''/>
						<input type='hidden' id='upload_auction_id' name='upload_auction_id' value=''/>
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal">&times;</button>
							<h4 class="modal-title" id="modal_header_text">Modal Header</h4>
						</div>
						<div class="modal-body">
							
							<div class="form-group">
								<label class="col-sm-4 control-label">Select File <span class="text-danger">*</span>:</label>
								<div class="col-md-8">
									<input class="form-control" type="file" name="upload_file" id="upload_file" />
								</div>
							</div>
						</div>
						<div class="modal-footer">
							<input class="btn btn-primary" type="submit" value='Submit' name="btn_upload_file_submit" id='btn_upload_file_submit' />
							<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>	
</div>
<div id="add_permission_modal" class="modal fade" role="dialog">
			<div class="modal-dialog">
				<div class="modal-content">
					<form name="add_permission_form" id="add_permission_form" method="post" >
						<input type='hidden' id='auction_id' name='auction_id' value=''/>
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal">&times;</button>
							<h4 class="modal-title" >Add Permission</h4>
						</div>
						<div class="modal-body">
							
							<div class="row">
								<div class="col-sm-2">
									<label class="col-sm-2 control-label">Is Public :</label>
								</div>
								<div class="col-sm-2">
									<input  type="checkbox" name="is_public" id="is_public" value="y"  />
								</div>
								<div class="col-sm-2">
									<label class="control-label">Bid Below RP :</label>
								</div>
								<div class="col-md-2">
									<input  type="checkbox" name="bid_below_rp" id="bid_below_rp" value="y" />
								</div>
								<div class="col-sm-2">
									<label class="control-label">Show Other Bidders :</label>
								</div>
								<div class="col-md-2">
									<input  type="checkbox" name="show_other_bidder" id="show_other_bidder" value="y"  />
								</div>
							</div>
							<div class="row">
								<div class="col-sm-2">
									<label class="control-label">Show Bidder Name :</label>
								</div>
								<div class="col-sm-2">
									<input  type="checkbox" name="show_bidder_name" id="show_bidder_name" value="y" />
								</div>
								<div class="col-sm-2">
									<label class="control-label">Show Bidder Rate :</label>
								</div>
								<div class="col-sm-2">
									<input  type="checkbox" name="show_bidder_rate" id="show_bidder_rate" value="y"  />
								</div>
								<div class="col-sm-2">
									<label class="control-label">Allow Multiple Bid :</label>
								</div>
								<div class="col-md-2">
									<input  type="checkbox" name="allow_multiple_bid" id="allow_multiple_bid" value="y"  />
								</div>
							</div>
							<div class="row">
								<div class="col-sm-2">
									<label class="control-label">Allow Less Bid Than Previous :</label>
								</div>
								<div class="col-sm-2">
									<input  type="checkbox" name="allow_less_bid_than_previous" id="allow_less_bid_than_previous" value="y" />
								</div>
								<div class="col-sm-2">
									<label class=" control-label">Show Rank :</label>
								</div>
								<div class="col-sm-2">
									<input  type="checkbox" name="show_rank" id="show_rank" value="y" />
								</div>
								<!-- <div class="col-sm-2">
									<label class=" control-label">Notify Bidder Email :</label>
								</div>
								<div class="col-sm-2">
									<input  type="checkbox" name="notify_bidder_email" id="notify_bidder_email"  value="y"/>
								</div> -->
							</div>
						</div>
						<div class="modal-footer">
							<button class="btn btn-primary" type="submit" value="submit" name="btn_permission_submit" id='btn_permission_submit'>Save</button>
							<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
						</div>
					</form>
				</div>
			</div>
		</div>


<!--===================================================-->
<!--END CONTENT CONTAINER-->
<script>
function auction_actions(act){
	if (act == 'ADD')
	{
		window.location.href = '<?php echo base_url("admin/salvage/addedit") ?>';
	}
	else if(act == 'EDIT')
	{
		var id = $("#list_product_id:checked").val()
		if(id == '' || id == undefined)
		{
			Display_msg('Please Select Auction.','error');
			return false;
		}
		else
		{
			window.location.href = '<?php echo base_url("admin/salvage/addedit/") ?>'+id;
		}
	}
	else if(act == 'AUCTION_IMAGES')
	{
		var id = $("#list_product_id:checked").val()
		if(id == '' || id == undefined)
		{
			Display_msg('Please Select Auction.','error');
			return false;
		}
		else
		{
			window.location.href = '<?php echo base_url("admin/salvage/auction_images/") ?>'+id;
		}
	}
	else if(act == 'CUSTOM_SECTION')
	{
		var id = $("#list_product_id:checked").val()
		if(id == '' || id == undefined)
		{
			Display_msg('Please Select Auction.','error');
			return false;
		}
		else
		{
			window.location.href = '<?php echo base_url("admin/salvage/product_custom_section/") ?>'+id;
		}
	}
	else if(act == 'UPLOAD_TERMS')
	{
		var id = $("#list_product_id:checked").val()
		if(id == '' || id == undefined)
		{
			Display_msg('Please Select Auction.','error');
			return false;
		}
		else
		{
			$('#modal_header_text').html('Upload Terms and Condition File');
			$('#upload_auction_id').val(id);
			$('#upload_file_type').val('terms');
			$('#upload_file_modal').modal('show');
		}
	}
	else if(act == 'UPLOAD_PRODUCT')
	{
		var id = $("#list_product_id:checked").val()
		if(id == '' || id == undefined)
		{
			Display_msg('Please Select Auction.','error');
			return false;
		}
		else
		{
			$('#modal_header_text').html('Upload Product Description');
			$('#upload_auction_id').val(id);
			$('#upload_file_type').val('product');
			$('#upload_file_modal').modal('show');
		}
	}
	else if(act == 'ADD_PERMISSION')
	{
		var id = $("#list_product_id:checked").val()
		if(id == '' || id == undefined)
		{
			Display_msg('Please Select .','error');
			return false;
		}
		else
		{
			$.ajax({
				url: "<?php echo base_url('admin/salvage/get_permission'); ?>",
				type: "POST",
				cache: false,
				clearForm: false,
				data: { "id" :id },
				success:function(response)
				{
					var data = jQuery.parseJSON(response);
					 $.each(data, function(index, value) {
					 	if (value == 'y')
					 	{
					 		$("#"+index).prop('checked',true);
					 	}
					 	else
					 	{
					 		$("#"+index).prop('checked',false);
					 	}
					 });
					$("#auction_id").val(id);
					$("#add_permission_modal").modal('show');
				},
				error :function (response)
				{
					alert('Invalid Request');
				}
			});
		
		}
	}
	else if (act == 'ADD_DOCUMENT')
	{
		var id = $("#list_product_id:checked").val()
		if(id == '' || id == undefined)
		{
			Display_msg('Please Select .','error');
			return false;
		}
		else
		{
			window.location.href = '<?php echo base_url("admin/salvage/document_upload/") ?>'+id;
		}
	}
	else if (act == 'ADD_CLIENT')
	{
		var id = $("#list_product_id:checked").val()
		if( ! id)
		{
			Display_msg('Please Select.','error');
			return false;
		}
		else
		{
			window.location.href = '<?php echo base_url("admin/salvage/view_clients/") ?>'+id;
		}
	}
	else
	{
		Display_msg('Please Select Action.','error');
		return false;
	}
}

function delete_auction()
{
	var id = $("#list_product_id:checked").val()
	if (id == '' || id == undefined)
	{
		Display_msg('Please Select Auction.','error');
		return false;
	}
	else
	{
		if(id > 0)
		{
			var ans = confirm('Are you sure you want to delete this auction?');
			if(ans)
			{
				$.ajax({
					url: "<?php echo base_url('admin/salvage/delete_auction'); ?>",
					type: "POST",
					dataType:"json",
					cache: false,
					clearForm: false,
					data: { "id" :id },
					success:function(response)
					{
						if (response.status == "success"){
							Display_msg(response.message,response.status);
							refresh_datatable();
						} else {
							Display_msg(response.message,response.status);
							return false;
						}
					}
				});
			}
		}
		else
		{
			Display_msg('Please Select Auction.','error');
			return false;
		}
	}
}

function publish_status(id,status){
	var msg = '';
	if (status == 'N'){
		msg = 'Are you sure to unpublish this auction?';
	} else {
		msg = 'Are you sure to publish this auction?';
	}
	var ans = confirm(msg);
	if(ans)
	{
		$.ajax({
			url: "<?php echo base_url('admin/salvage/update_publish_status'); ?>",
			type: "POST",
			dataType:"json",
			cache: false,
			clearForm: false,
			data:{"id":id,'status':status},
			success:function(response){
				if (response.status == "success") {
					Display_msg(response.message,response.status);
					refresh_datatable();
				} else {
					Display_msg(response.message,response.status);
					return false;
				}
			}
		});
	}
}

var vRules = {
	upload_file:{
		required:true
	}
};
var vMessages = {
	upload_file:{required:"<p class='text-danger'>Please select file.</p>"}
};

$("#upload_file_form").validate({
	rules: vRules,
	messages: vMessages,
	submitHandler: function(form){
		$(form).ajaxSubmit({
			url:"<?php echo base_url('admin/salvage/upload_file/');?>",
			type: 'post',
			dataType:'json',
			cache: false,
			clearForm: true,
			success: function (response) {
				if(response.status=="success")
				{
					Display_msg(response.message,response.status);
					refresh_datatable();
					$('#upload_file_modal').modal('hide');
				}
				else
				{	
					Display_msg(response.message,response.status);
					return false;
				}
			}
		});
	}
});
$("#add_permission_form").validate({
	rules: vRules,
	messages: vMessages,
	submitHandler: function(form){
		$(form).ajaxSubmit({
			url:"<?php echo base_url('admin/salvage/add_permission/');?>",
			type: 'post',
			dataType:'json',
			cache: false,
			clearForm: true,
			success: function (response) {
				if(response.status=="success")
				{
					Display_msg(response.message,response.status);
					refresh_datatable();
					$('#add_permission_modal').modal('hide');
				}
				else
				{	
					Display_msg(response.message,response.status);
					return false;
				}
			}
		});
	}
})


</script>