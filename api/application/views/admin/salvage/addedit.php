<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/css/bootstrap-datepicker.min.css" integrity="sha256-siyOpF/pBWUPgIcQi17TLBkjvNgNQArcmwJB8YvkAgg=" crossorigin="anonymous" />
<style>
	.mdtp__wrapper {
		bottom:175px!important;
	}
.select2-container{
	padding: 0 !important;
}
.form-horizontal .control-label{
	text-align:left !important;
}
/* If the bid logic is Rank */
.highlight {
	background-color: #f3f3f3;
	padding: 10px;
	margin-bottom: 10px;
    border-radius: 8px;
}
.form_section{
	color: #555;
	padding: 13px 30px;
    border: solid 1px #29B6F6;
    border-radius: 5px;
    
}
h4 {
	font-weight: 500;
}
.error_section{
	margin: 0px;
    height: 16px;
}
.error_section label{
	margin: 0px;
    height: 16px;
}
.form-group {
    margin-bottom: 5px;
}

</style>
<link href="<?php echo base_url('auctioneer_assets/css/mdtimepicker.css'); ?>" rel="stylesheet" />


<!--CONTENT CONTAINER-->
<!--===================================================-->
<div id="content-container">
	<!--Page Title-->
	<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
	<div id="page-title">
		<h1 class="page-header text-overflow"><?php echo $pagetitle; ?></h1>
	</div>
	<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
	<!--End page title-->
	<!--Breadcrumb-->
	<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
	<ol class="breadcrumb">
		<li><a href="<?php echo base_url('dashboard');?>">Home</a></li>
		<li><a href="<?php echo base_url('/auction/salvage/index');?>">All Salvage Auction</a></li>
		<li><a class="active"><?php echo $pagetitle; ?></a></li>
	</ol>
	<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
	<!--End breadcrumb-->
	
	<!--Page content-->
	<!--===================================================-->
	<div id="page-content">
		<div class="row">
			<div class="col-sm-12">
				<!--Primary Panel-->
				<!--===================================================-->
				<div class="panel panel-primary">
					<div class="panel-heading">
						<h3 class="panel-title"><?php echo $pagetitle; ?></h3>
					</div>
					<!--Horizontal Form-->
					<!--===================================================-->
					<form class="form-horizontal" method="post" name="salvage_addedit" id="salvage_addedit">
						<input type='hidden' id='product_id' name='product_id' value='<?php echo ! empty($product_id) ? $product_id : ''; ?>' />
						<div class="panel-body">
							<div class="form-group">
								<label class="col-sm-2 control-label">E-Auction Sale No <span class="text-danger">*</span>:</label>
								<div class="col-sm-4">
									<input type='text' class='form-control' name='sale_no' value='<?php echo $sale_no?>' readonly>
								</div>
							</div>
							<h4>Product Details</h4>
							<div class="form_section">
								<div class="form-group">
									<label class="col-sm-2 control-label">Product Name <span class="text-danger">*</span>:</label>
									<div class="col-md-2">
										<input class="form-control" type="text" name="product_name" id="product_name" size="52" maxlength="500" value='<?php echo ! empty($product_name) ? $product_name : ""; ?>'>
										<div class="error_section">
											<label style="display: none;" for="product_name" generated="true" class="error"><p class="text-danger">Please Enter Product Name</p></label>
										</div>
									</div>
									<label class="col-sm-2 control-label">Product Category<span class="text-danger">*</span>:</label>
									<div class="col-md-2">
										<select id="parent_category_id" name="parent_category_id">
											<option></option>
											<?php foreach ($product_category as $value){ ?>
												<option value="<?php echo $value['category_id']; ?>"><?php echo $value['category_name']; ?></option>
											<?php } ?>
										</select>
										<div class="error_section">
											<label style="display: none;" for="parent_category_id" generated="true" class="error"><p class="text-danger">Please Select Product Category</p></label>
										</div>
									</div>
									<label class="col-sm-2 control-label">Sub Product Category<span class="text-danger">*</span>:</label>
									<div class="col-md-2">
										<select id="level_2_category_id" name="level_2_category_id">
											<option></option>
										</select>
										<div class="error_section">
										
										</div>
									</div>

									
								</div>
								<div class="form-group">
									<label class="col-sm-2 control-label">Description <span class="text-danger">*</span>:</label>
									<div class="col-md-8">
										<textarea class="form-control ckeditor" name="short_description" id="short_description"><?php echo ! empty($short_description) ? $short_description : "" ?></textarea>
										<div class="error_section">
											<label style="display: none;" for="short_description" generated="true" class="error"><p class="text-danger">Please Enter Description</p></label>
										</div>
									</div>
								</div>

								
								<div class="form-group">
									

									
								</div>
								
							</div>


							<h4>Auctioneer/Vendor Details</h4>
							<div class="form_section">
								<div class="form-group">
									<label class="col-sm-2 control-label">Auctioneer Name <span class="text-danger">*</span>:</label>
									<div class="col-sm-4">
										<select name="auctioneer_name" id="auctioneer_name" class="form-control select2 required" title="Select auctioneer/vendor name">
											<option value=""></option>
											<?php
											if ( ! empty($vendor_id && ! empty($product_id))){
												echo '<option value="'.$vendor_id.'" selected>'.$c_name.'</option>';
											}
											?>
										</select>
										<div class="error_section">
											<label style="display: none;" for="auctioneer_name" generated="true" class="error"><p class="text-danger">Please select auctioneer name</p></label>
										</div>
									</div>

									<div class="col-sm-6 other_company form-group">
										<label class="col-sm-4 control-label">Company <span class="text-danger">*</span>: </label>
										<div class="col-sm-8">
											<select class="form-control" size="1" name="company_id" id="company_id" >
											</select>
											<div class="error_section">
												<label style="display: none;" for="company_id" generated="true" class="error"><p class="text-danger">Please select company name</p></label>
											</div>
										</div>
									</div>
								</div>

							
								<div class="other_company vendor_plant form-group">
									<label class="col-sm-2 control-label">Plant<span class="text-danger">*</span>: </label>
									<div class="col-sm-4">
										<select class="form-control" size="1" name="plant" id="plant"></select>
										<div class="error_section">
											<label style="display: none;" for="plant" generated="true" class="error"><p class="text-danger">Please select plant name</p></label>
										</div>
									</div>
								</div>
							</div>
							
							<h4>Bidding Timing</h4>
							<div class="form_section">
								<div class="form-group">
									<label class="col-sm-2 control-label">Auction Start Date <span class="text-danger">*</span>:</label>
									<div class="col-md-4">
										<input class="form-control" name="start_date" id="start_date" readonly type="text" value='<?php echo ! empty($start_date) ? servertz_to_usertz($start_date,$this->session->userdata('user_tz'),'Y-m-d') : ""; ?>'/>
										<div class="error_section">
											<label style="display: none;" for="start_date" generated="true" class="error"><p class="text-danger">Please Enter The Auction Start Date</p></label>
										</div>
									</div>
								
									<label class="col-sm-2 control-label">Auction Open Up to <span class="text-danger">*</span>:</label>
									<div class="col-md-4">
										<input class="form-control" name="end_date" id="end_date" readonly type="text" value='<?php echo ! empty($end_date) ? servertz_to_usertz($end_date,$this->session->userdata('user_tz'),'Y-m-d') : ""; ?>' />
										<div class="error_section">
											<label style="display: none;" for="end_date" generated="true" class="error"><p class="text-danger">Please Enter Auction Open Up to</p></label>
										</div>
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-2 control-label">Auction Start Time <span class="text-danger">*</span>:</label>
									<div class="col-md-4">
										<input class="form-control" name="start_time" type="text" id="start_time" type="time" value='<?php echo ! empty($start_time) ? servertz_to_usertz($start_time,$this->session->userdata('user_tz'),'H:i:s')  : ""; ?>' />
										<div class="error_section">
											<label style="display: none;" for="start_time" generated="true" class="error"><p class="text-danger">Please Enter Start Time </p></label>
										</div>
									</div>
								
									<label class="col-sm-2 control-label">Auction End Time <span class="text-danger">*</span>:</label>
									<div class="col-md-4">
										<input class="form-control" name="end_time" type="text" id="end_time" type="time" value='<?php echo ! empty($end_time) ? servertz_to_usertz($end_time,$this->session->userdata('user_tz'),'H:i:s') : ""; ?>' />
										<div class="error_section">
											<label style="display: none;" for="end_time" generated="true" class="error"><p class="text-danger">Please Enter End Time</p></label>
										</div>
									</div>
								</div>
							</div>
							<h4>Product Location</h4>
							<div class="form_section">
								<div class="form-group">
									<label class="col-sm-2 control-label">Country <span class="text-danger">*</span>: </label>
									<div class="col-sm-4">
										<select id="country_id" name="country_id">
											<option></option>
											<?php foreach ($country as $value){ ?>
												<option value="<?php echo $value['id']; ?>"><?php echo $value['name']; ?></option>
											<?php } ?>
										</select>
										<div class="error_section">
											<label style="display: none;" for="country_id" generated="true" class="error"><p class="text-danger">Please Select Country </p></label>
										</div>
									</div>
								
									<label class="col-sm-2 control-label" for="state_id">State <span class="text-danger">*</span>: </label>
									<div class="col-sm-4">
										<select id="state_id" name="state_id">
											<option></option>
										</select>
										<div class="error_section">
											<label style="display: none;" for="state_id" generated="true" class="error"><p class="text-danger">Please Select State</p></label>
										</div>
									</div>
								</div>
								<div id="s_other" class="form-group col-sm-6">
									<label class="col-sm-4 control-label" for="state_other">Other State<span class="text-danger">*</span>: </label>
									<div class="col-sm-8">
										<input class="form-control" name="state_other" type="text" id="state_other" type="text"  />
										<div class="error_section">
											<label style="display: none;" for="city_id" generated="true" class="error"><p class="text-danger">Please Select City</p></label>
										</div>
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-2 control-label" for="city_id"> City <span class="text-danger">*</span>: </label>
									<div class="col-sm-4">
										<select id="city_id" name="city_id">
											<option></option>
										</select>
										<div class="error_section">
										
										</div>
									</div>
								</div>
								<div id="c_other" class="form-group">
									<label class="col-sm-2 control-label" for="city_other">Other City<span class="text-danger">*</span>: </label>
									<div class="col-sm-4">
										<input class="form-control" name="city_other" type="text" id="city_other" type="text"   value='<?php echo ! empty($other_city) ? $other_city : ""; ?>' />
										<div class="error_section">
										
										</div>
									</div>
								</div>
							</div>
							<h4>Manufacturing Details</h4>
							<div class="form_section">
								<div class="form-group">
									<label class="col-sm-2 control-label" for="manufacturer_id">Manufacturer <span class="text-danger">*</span>: </label>
									<div class="col-sm-4">
										<select id="manufacturer_id" name="manufacturer_id">
											<option></option>
											<?php foreach ($manufacturer as $value){ ?>
												<option value="<?php echo $value['manufacture_id']; ?>"><?php echo $value['manufacturer_name']; ?></option>
											<?php } ?>
											<option value="other">Other</option>
										</select>
										<div class="error_section">
											<label style="display: none;" for="manufacturer_id" generated="true" class="error"><p class="text-danger">Please Select Manufacturer</p></label>
										</div>
									</div>

									<label class="col-sm-2 control-label" for="manufacturer_year">Manufacturer Year<span class="text-danger">*</span>: </label>
									<div class="col-sm-4">
										<input class="form-control" name="manufacturer_year" type="text" id="manufacturer_year" type="text" value="<?php echo ! empty($manufacturer_year) ? $manufacturer_year : ""; ?>" />
										<div class="error_section">
											<label style="display: none;" for="manufacturer_year" generated="true" class="error"><p class="text-danger">Please Enter Manufacturer Year</p></label>
										</div>
									</div>
								</div>
								<div id="manu_other" class="form-group">
									<label class="col-sm-2 control-label" for="manufacturer_other">Other Manufacturer<span class="text-danger">*</span>: </label>
									<div class="col-sm-4">
										<input class="form-control" name="manufacturer_other" type="text" id="manufacturer_other" type="text"  />
										<div class="error_section">
										
										</div>
									</div>
								</div>
							
								<div class="form-group">
									<label class="col-sm-2 control-label" for="dimension">Model No.: </label>
									<div class="col-sm-4">
										<input class="form-control" name="model_no" type="text" id="model_no" type="text" value="<?php echo ! empty($model_no) ? $model_no : ""; ?>"  />
										<div class="error_section">
										
										</div>
									</div>

									<label class="col-sm-2 control-label" for="dimension">Dimension: </label>
									<div class="col-sm-4">
										<input class="form-control" name="dimension" type="text" id="dimension" type="text" value="<?php echo ! empty($dimension) ? $dimension : ""; ?>" />
										<div class="error_section">
										
										</div>
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-2 control-label" for="serial_no">Serial No.: </label>
									<div class="col-sm-4">
										<input class="form-control" name="serial_no" type="text" id="serial_no" type="text" value="<?php echo ! empty($serial_no) ? $serial_no : ""; ?>" />
										<div class="error_section">
										
										</div>
									</div>
								</div>
							</div>
							<h4>Bidding details</h4>
							<div class="form_section">
								<div class="form-group">
									<label class="col-sm-2 control-label">Price Currency <span class="text-danger">*</span>:</label>
									<div class="col-sm-4">
										<select name="price_currency" id="price_currency" class="form-control select2 required" title="Choose Auction Currency">
											<option ></option>
											<?php
											if ( ! empty($all_currency))
											{
												foreach ($all_currency as $single_currency)
												{
													$selected = '';
													if ($single_currency['currency_name']===$price_currency)
													{
														$selected = "selected";
													}
													?><option value='<?php echo $single_currency['currency_name'];?>' <?php echo $selected; ?>><?php echo $single_currency['currency_name'];?></option><?php
												}
											}
											?>
										</select>
										<div class="error_section">
											<label style="display: none;" for="price_currency" generated="true" class="error"><p class="text-danger">Please Select Price Currency</p></label>
										</div>
									</div>
							
									<label class="col-sm-2 control-label" for="price">Price<span class="text-danger">*</span>: </label>
									<div class="col-sm-4">
										<input class="form-control" name="price" type="text" id="price" type="text" value="<?php echo ! empty($price) ? $price : ""; ?>" />
										<div class="error_section">
											<label style="display: none;" for="price" generated="true" class="error"><p class="text-danger">Please enter price</p></label>
										</div>
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-2 control-label">Buying Logic <span class="text-danger">*</span>:</label>
									<div class="col-sm-4">
										<select class="form-control" name="buying_format" id="buying_format">
											<option value="">--Select Buying logic--</option>
											<option <?php if ( ! empty($buying_format) && $buying_format == 'Live Auction'){ echo 'selected'; } ?> value="Live Auction">Live Auction</option>
											<option <?php if ( ! empty($buying_format) && $buying_format == 'Make Offer'){ echo 'selected'; } ?> value="Make Offer">Make Offer</option>
											<option <?php if ( ! empty($buying_format) && $buying_format == 'Online Auction'){ echo 'selected'; } ?> value="Online Auction">Online Auction</option>
											<option <?php if ( ! empty($buying_format) && $buying_format == 'Buy Now'){ echo 'selected'; } ?> value="Buy Now">Buy Now</option>
											<option <?php if ( ! empty($buying_format) && $buying_format == 'Sailed Bid'){ echo 'selected'; } ?> value="Sailed Bid">Sailed Bid</option>
										</select>
										<div class="error_section">
											<label style="display: none;" for="buying_format" generated="true" class="error"><p class="text-danger">Please select buying format</p></label>
										</div>
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-2 control-label" >Reserved Price<span class="text-danger">*</span></label>
									<div class="col-sm-4">
										<input class="form-control" name="reserve_price" type="text" id="reserve_price" type="text" value="<?php echo ! empty($reserve_price) ? $reserve_price : ""; ?>" />
									</div>
									<label class="col-sm-2 control-label" >Result Date<span class="text-danger">*</span></label>
									<div class="col-sm-4">
										<input class="form-control datepickerclass" name="result_date" type="text" id="result_date" type="text" value="<?php echo ! empty($result_date) ? servertz_to_usertz($result_date,$this->session->userdata('user_tz'),'Y-m-d') : ""; ?>" readonly/>
									</div>
								</div>
							</div>

							<div class="form-group">
								<div class="col-xs-12" style="text-align: center">
									<input class="btn btn-primary" type="submit" value='Submit' name="btn_submit" id='btn_submit' />
									<a class="btn btn-warning" href='<?php echo base_url('admin/salvage/index'); ?>'>Back</a>
								</div>
							</div>
						</div>

					</form>
					<!--===================================================-->
					<!--End Horizontal Form-->
				</div>
				<!--===================================================-->
				<!--End Primary Panel-->
			</div>

		</div>	
	</div>
	<!--===================================================-->
	<!--End page content-->
</div>
<!--===================================================-->
<!--END CONTENT CONTAINER-->




<script src="<?php echo base_url('auctioneer_assets/js/mdtimepicker.js'); ?>"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/js/bootstrap-datepicker.min.js" integrity="sha256-bqVeqGdJ7h/lYPq6xrPv/YGzMEb6dNxlfiTUHSgRCp8=" crossorigin="anonymous"></script>
<script src="https://cdn.ckeditor.com/4.14.0/standard/ckeditor.js"></script>

<script>

$(document).ready(function() {

	$("#manu_other").hide();
	$("#c_other").hide();
	$("#s_other").hide();

	$("#auction_conduct_type").select2({
		placeholder: 'Auction Conduct'
	});

	$('#company_id').select2({
		placeholder: "Select Company"
	});
	$('#plant').select2({
		placeholder: "Select Plant"
	});
	$('#mat_id').select2({
		placeholder: "Select Material"
	});
	
	$('#country_id').select2({
		placeholder: "Select Country Name",
		width: "100%"
	});
	$('#state_id').select2({
		placeholder: "Select State Name",
		width: "100%"
	});
	$('#city_id').select2({
		placeholder: "Select City Name",
		width: "100%"
	});

	$('#manufacturer_id').select2({
		placeholder: "Select Manufacturer Name",
		width: "100%"
	});

	$('#level_2_category_id').select2({
		placeholder: "Select Sub Category",
		width: "100%"
	});

	$('#parent_category_id').select2({
		placeholder: "Select Category",
		width: "100%"
	});

	$('#price_currency').select2({
		placeholder: "Select price currency",
		width: "100%"
	});

	$('#start_time').mdtimepicker({
		format: 'hh:mm:ss',
		theme: 'blue'
	}); //Initializes the time picker and uses the specified format (i.e. 23:30)
	$('#end_time').mdtimepicker({
		format: 'hh:mm:ss',
		theme: 'blue'
	}); //Initializes the time picker and uses the specified format (i.e. 23:30)

	$("#auctioneer_name").select2({
		placeholder:"Select Auctioneer/Vendor Name",
		minimumInputLength: 3,
		ajax: {
			url: '<?php echo base_url("admin/salvage/get_auctioneer_vendor_list"); ?>',
			dataType: 'json',
			type: "post",
			data: function (term) {
				return {
					term: term
				};
			},
			processResults: function (data, params) {
				console.log(data);
				return {
					results: data.results,
				};
			},
			cache: true
		}
	})

	<?php if ( ! empty($start_date) && ! empty($end_date)){ ?>
		$("#start_date").datepicker({
			format: 'yyyy-mm-dd',
			startDate: new Date("<?php echo $start_date; ?>")
		});
		$("#end_date").datepicker({
            format: 'yyyy-mm-dd',
            startDate: new Date("<?php echo $start_date; ?>")
        });
	<?php } else { ?>
	$("#start_date").datepicker({
        format: 'yyyy-mm-dd',
        startDate: new Date("<?php echo $today_date; ?>")
    });
	<?php } ?>
    $("#start_date").change(function(){
        startDatee = '"'+$("#start_date").val()+'"';
		$("#end_date").val('');
		$("#end_date").datepicker('destroy');
        $("#end_date").datepicker({
            format: 'yyyy-mm-dd',
            startDate: new Date(startDatee)
        });
    });
	<?php if ( ! empty($product_id)){ ?>
		changeProductValues()
	<?php } ?>
});

$("#auctioneer_name").change(function(){
	var auctioneer_id = $("#auctioneer_name").val();
	$.ajax({
		url: "<?php echo base_url('admin/salvage/get_company_details') ?>",
		type: "POST",
		data:{
			'auctioneer_id': auctioneer_id
		},
		dataType: "json",
		async: false,
		success: function(data)
		{
			var html = '<option value=""></option>';
			var i;
			for (i=0; i<data.length; i++)
			{
				html += '<option value="'+data[i].company_id+'">'+data[i].company_name+'</option>';
			}
			$('#company_id').html(html);
		}
	});
})

$("#company_id").change(function(){
	$("#plant").select2('val', '');
	var company_id = $("#company_id").val();
	$.ajax({
		url: "<?php echo base_url('admin/salvage/get_plant_details') ?>",
		type: "POST",
		data:{
			'company_id': company_id
		},
		dataType: "json",
		async: false,
		success: function(data)
		{
			var html = '<option value=""></option>';
			var i;
			for (i=0; i<data.length; i++)
			{
				html += '<option value="'+data[i].company_plant_id+'">'+data[i].plant_name+'</option>';
			}
			$('#plant').html(html);
		}
	});
});

$(function(){
	$("#get_auction_select").select2({
		placeholder: "Select Backdated Auction",
		allowClear: true
	});	
	$("#auction_allowed_currency").select2({
		placeholder: "Select Allowed Currency",
		allowClear: true
	});

	$.validator.addMethod('compare_time', function (value, element, param) {
		return this.optional(element) || check_start_end_time();
	}, "<p class='text-danger'>Enter Correct start time and end time</p>");

	function check_start_end_time(){
		var startTime = '"'+document.getElementById("start_time").value+'"';
		var endTime = '"'+document.getElementById("end_time").value+'"';
		var startDate = document.getElementById("start_date").value;
		var endDate = document.getElementById("end_date").value;
		if (startDate == endDate) {
			if (startTime >= endTime){
				return false;
			} else {
				return true;
			}
		} else {
			return true;
		}
	}

	

	
	var vRules = {
		sale_no: { required:true},
		product_name: { required:true},
		short_description: { 
		required: function() 
			{
				var msg = CKEDITOR.instances['short_description'].getData().replace(/<[^>]*>/gi, '').length;
				if (msg ==0) {
				
					return true;
				}else
				{
					return false;
				}
			},

			minlength:10
		},
		
		parent_category_id: { required:true },
		start_date: { required:true },
		end_date: { required:true},
		start_time: { required:true },
		end_time: {
			required: true,
			compare_time: "#start_time"
		},
		country_id: { required:true},
		state_id: { required:true},
		city_id: { required:true},
		manufacturer_id: { required:true},
		price_currency: { required:true},
		manufacturer_year: { required:true },
		price: { required:true },
		buying_format:{required:true},
		auctioneer_name:{required:true},
		company_id:{required:true},
		plant:{required:true},
		reserve_price:{required:true},
		result_date:{required:true},
	};

	var vMessages = {
		sale_no: { required:"<p class='text-danger'>Please Enter The E-Auction Sale No</p>" },
		product_name: { required:"<p class='text-danger'>Please Enter Product Name</p>" },
		short_description: { required:"<p class='text-danger'>Please Enter Description</p>" },
		parent_category_id:{ required:"<p class='text-danger'>Please Select Product Category</p>" },
		start_date: { required:"<p class='text-danger'>Please Enter The Auction Start Date</p>" },
		end_date: { required:"<p class='text-danger'>Please Enter Auction Open Up to</p>" },
		start_time: { required:"<p class='text-danger'>Please Enter Start Time </p>" },
		end_time: { required:"<p class='text-danger'>Please Enter End Time</p>" },
		country_id: { required:"<p class='text-danger'>Please Select Country </p>" },
		state_id:{ required:"<p class='text-danger'>Please Select State</p>" },
		city_id: { required:"<p class='text-danger'>Please Select City</p>" },
		manufacturer_id: { required:"<p class='text-danger'>Please Select Manufacturer</p>" },
		price_currency: { required:"<p class='text-danger'>Please Select Price Currency</p>" },
		manufacturer_year: { required:"<p class='text-danger'>Please Enter Manufacturer Year</p>" },
		price: { required:"<p class='text-danger'>Please enter price</p>" },
		buying_format:{required:"<p class='text-danger'>Please select buying format</p>"},
		auctioneer_name:{required:"<p class='text-danger'>Please select auctioneer name</p>"},
		company_id:{required:"<p class='text-danger'>Please select company name</p>"},
		plant:{required:"<p class='text-danger'>Please select plant name</p>"},
		reserve_price:{required:"<p class='text-danger'>Please Enter Reserve Price</p>"},
		result_date:{required:"<p class='text-danger'>Please select Result Date</p>"},

	};

	$("#salvage_addedit").validate({
		ignore:[],
		rules: vRules,
		messages: vMessages,
		submitHandler: function(form){
			for (instance in CKEDITOR.instances) {
                CKEDITOR.instances[instance].updateElement();
            }
			$(form).ajaxSubmit({
				url:"<?php echo base_url('admin/salvage/update_data/'); ?>",
				type: 'post',
				dataType:'json',
				cache: false,
				clearForm: false,
				success: function (response) {
					if (response.status=="success"){
						Display_msg(response.message,response.status);
						setTimeout(function(){
							window.location.href="<?php echo base_url('admin/salvage/index');?>";
						}, 3000);							
					} else {	
						Display_msg(response.message,response.status);
					}
				}
			});
		}
	});

});


$("#parent_category_id").change(function(){
	$.ajax({
		url: "<?php echo base_url('admin/salvage/get_sub_parent_category') ?>",
		type: "POST",
		data:{'id':$("#parent_category_id").val()},
		dataType:"json",
		async: false,
		success:function(response){
			var html = "<option></option>";
			if(response.length > 0){
				for (var i=0; i<response.length; i++){
					html = html + '<option value="'+response[i]['category_id']+'">'+response[i]['category_name']+'</option>';
				}
			}
			$("#level_2_category_id").html(html);
		}
	});
});

$("#country_id").change(function(){
	$.ajax({
		url: "<?php echo base_url('admin/salvage/get_state') ?>",
		type: "POST",
		data:{'id':$("#country_id").val()},
		dataType:"json",
		async: false,
		success:function(response){
			$("#state_id").empty();
			var html = "<option></option>";
			if(response.length > 0){
				for (var i=0; i<response.length; i++){
					html = html + '<option value="'+response[i]['id']+'">'+response[i]['state']+'</option>';
				}
			}
			html = html + "<option value='other'>Other</option>";
			$("#state_id").html(html);
		}
	});
});

$("#state_id").change(function(){
	if ($("#state_id").val() !== "other")
	{
		$("#s_other").hide();
		$.ajax({
			url: "<?php echo base_url('admin/salvage/get_city') ?>",
			type: "POST",
			data:{'id':$("#state_id").val()},
			dataType:"json",
			async: false,
			success:function(response){
				$("#city_id").empty();
				var html = "<option></option>";
				if(response.length > 0){
					for (var i=0; i<response.length; i++){
						html = html + '<option value="'+response[i]['id']+'">'+response[i]['name']+'</option>';
					}
				}
				html = html + "<option value='other'>Other</option>";
				$("#city_id").html(html);
			}
		});
	}
	else
	{
		$("#s_other").show();
		$("#city_id").empty();
		var html = "<option></option>";
		html = html + "<option value='other'>Other</option>";
		$("#city_id").html(html);
	}
})

$("#city_id").change(function(){
	if ($("#city_id").val() !== "other"){
		$("#c_other").hide();
	} else {
		$("#c_other").show();
	}
})

$("#manufacturer_id").change(function(){
	if ($("#manufacturer_id").val() !== "other"){
		$("#manu_other").hide();
	} else {
		$("#manu_other").show();
	}
});

<?php if ( ! empty($product_id)){ ?>
function changeProductValues()
{
	var ProductCategory = "<?php echo ! empty($parent_category_id) ? $parent_category_id : ""; ?>";
	var subProductCategory = "<?php echo ! empty($level_2_category_id) ? $level_2_category_id : ""; ?>";
	var countryId = "<?php echo ! empty($country_id) ? $country_id : ""; ?>";
	var stateId = "<?php echo ! empty($state_id) ? $state_id : "other"; ?>";
	var stateOther = "<?php echo ! empty($state_other) ? $state_other : ""; ?>";
	var cityId = "<?php echo ! empty($city_id) ? $city_id : "other"; ?>";
	var cityOther = "<?php echo ! empty($city_other) ? $city_other : ""; ?>";
	var manufacturerId = "<?php echo ! empty($manufacturer_id) ? $manufacturer_id : "other"; ?>";
	var manufacturerOther = "<?php echo ! empty($manufacturer_other) ? $manufacturer_other : ""; ?>";
	var vendorId = "<?php echo ! empty($vendor_id) ? $vendor_id : ""; ?>";
	var vendorName = "<?php echo ! empty($c_name) ? $c_name : ""; ?>";
	var companyId = "<?php echo ! empty($company_id) ? $company_id : ""; ?>";
	var plantId = "<?php echo ! empty($plant_id) ? $plant_id : ""; ?>";
	$("#auctioneer_name").trigger('change');
	if (ProductCategory !== ""){
		$("#parent_category_id").val(ProductCategory).trigger('change');
	}
	if (subProductCategory !== ""){
		$("#level_2_category_id").val(subProductCategory).trigger('change');
	}
	if (countryId !== ""){
		$("#country_id").val(countryId).trigger('change');
	}
	$("#state_id").val(stateId).trigger('change');
	if (stateId == 'other'){
		$("#state_other").val(stateOther);
	}
	$("#city_id").val(cityId).trigger('change');
	if (cityId == 'other'){
		$("#city_other").val(cityOther);
	}
	$("#manufacturer_id").val(manufacturerId).trigger('change');
	if (manufacturerId == 'other'){
		$("#manufacturer_other").val(manufacturerOther);
	}
	$("#company_id").val(companyId).trigger('change');
	$("#plant").val(plantId).trigger('change');
}

<?php } ?>

</script>