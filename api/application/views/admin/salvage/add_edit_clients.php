<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/css/bootstrap-datepicker.min.css" integrity="sha256-siyOpF/pBWUPgIcQi17TLBkjvNgNQArcmwJB8YvkAgg=" crossorigin="anonymous" />

<style>
.add_select2{
	width:100%;
}
</style>
<!--CONTENT CONTAINER-->
<!--===================================================-->
<div id="content-container">

	<!--Page Title-->
	<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
	<div id="page-title">
		<h1 class="page-header text-overflow">Salvage Auction Client Add Lot</h1>
	</div>
	<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
	<!--End page title-->


	<!--Breadcrumb-->
	<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
	<ol class="breadcrumb">
		<li><a href="<?php echo base_url('dashboard');?>">Home</a></li>
		<li class="active"><a href="<?php echo base_url('salvage/index');?>">Salvage Auction</a></li>
		<li class="active"><a href="<?php echo base_url('admin/salvage/view_clients/'.$auctionid);?>">Salvage Auction Clients</a></li>
		<li class="active">Salvage Auction Client Add Lot</li>
	</ol>
	<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
	<!--End breadcrumb-->
	<div id="page-content">	
		<div class="panel">
			<div class="panel-heading">
				<h3 class="panel-title">Salvage Auction Client Add Lot</h3>
			</div>
			
			<div class="panel-body">
				<fieldset>
					<legend>Auction Details</legend>
					<div class="table-responsive">
						<table class="table table-striped table-bordered table-hover no-footer dtr-inline">
							<thead>
								<tr>
									<th>Sale No</th>
									<th>Start Time</th>
									<th>End Time</th>
									<th>Particulars</th>
								</tr>
								<tr>
									<td><?php echo $auction_details['sale_no'] ?></td>
									<td><?php echo servertz_to_usertz($auction_details['start_date']." ".$auction_details['start_time'],$this->session->userdata('user_tz'),'d-m-Y H:i:s') ?></td>
									<td><?php echo servertz_to_usertz($auction_details['end_date']." ".$auction_details['end_time'],$this->session->userdata('user_tz'),'d-m-Y H:i:s') ?></td>
									<td><?php echo $auction_details['short_description'] ?></td>
								</tr>
							</thead>
						</table>
					</div>				
				</fieldset>
				
				<form name="form_auc_client" id="form_auc_client" method="post" class="form-horizontal">
					<fieldset>
						<legend>Client Details</legend>					
						<input type='hidden' id="auctionid" name="auctionid" value='<?php echo $auctionid; ?>' />
						<div class="form-group">
							<label class="col-sm-2 control-label">Client Name <span class="text-danger">*</span>:</label>
							<div class="col-sm-3">
								<select class=" add_select2" name="client_id" id="client_id" >
									<option></option>
									<?php 
									if ( ! empty($all_clients))
									{
										foreach ($all_clients as $single_client)
										{
											$selected = '';
											if ($single_client['id'] == $client_details['client_id'])
											{
												$selected = 'selected="selected"';
											}
											?><option value='<?php echo $single_client['id'];?>' <?php echo $selected; ?>><?php echo $single_client['compname']." [".$single_client['conperson']."] [".$single_client['mob']."] [".$single_client['email']."]"?></option><?php
										}
									}
									?>
								</select>
							</div>			
						</div>
						<div class="form-group">
							<label class="col-sm-2 control-label">Note :  </label>
							<div class="col-sm-3">
								<textarea class="form-control" name="note" id="note"></textarea>
							</div>
						</div>
						<input type="hidden" name="end_date" id="end_date" value="<?php echo date('d-m-Y H:i:s',strtotime($auction_details['end_date']." ".$auction_details['end_time'])) ?>"/>
					</fieldset>
					
					
					<div class="row ">
						<div class="col-sm-12" style="text-align: center;">
							<input type='submit' class="btn btn-info" placeholder="Submit" name="btn_submit" id="btn_submit" value='Submit'  />
							<a class="btn btn-warning" href='<?php echo base_url('admin/salvage/index');?>' name="btn_back" id="btn_back">Back</a>
						</div>
					</div>
				</form>
			</div>	
		</div>
	</div>	
</div>

<!--===================================================-->
<!--END CONTENT CONTAINER-->

<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/js/bootstrap-datepicker.min.js" integrity="sha256-bqVeqGdJ7h/lYPq6xrPv/YGzMEb6dNxlfiTUHSgRCp8=" crossorigin="anonymous"></script>

<script>

$("#transaction_date").datepicker({
	format: 'yyyy-mm-dd'
});

	
	/* Select Multiple Checkbox With Shift Key - End */

	var vRules = {
		client_id:{required:true},
	};
	var vMessages = {
		client_id:{required:"<p class='text-danger'>Please Select Client</p>"},
	};

	$("#form_auc_client").validate({

		submitHandler: function(form){
			$(form).ajaxSubmit({
				url:"<?php echo base_url('admin/salvage/add_client/');?>",
				type: 'post',
				dataType:'json',
				cache: false,
				success: function (response) {               
					if(response.status=="success")
					{
						Display_msg(response.message,response.status);
						setTimeout(function()
						{
							window.location.href="<?php echo base_url('admin/salvage/view_clients/'.$auctionid);?>";
						}, 3000);
					}
					else
					{
						Display_msg(response.message,response.status);
						return false;
					}
				}
			});
		}
	});
</script>