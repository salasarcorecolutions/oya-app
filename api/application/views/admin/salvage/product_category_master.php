<style>
.img-thumbnail {
    height:20%
}
</style>

<!--CONTENT CONTAINER-->
<!--===================================================-->
<div id="content-container">
	<!--Page Title-->
	<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
	<div id="page-title">
		<h1 class="page-header text-overflow"><?php echo $pagetitle; ?></h1>
	</div>
	<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
	<!--End page title-->

	<!--Breadcrumb-->
	<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
	<ol class="breadcrumb">
		<li><a href="<?php echo base_url('admin/main/dashboard');?>">Home</a></li>
		<li class="active"><?php echo $pagetitle; ?></li>
	</ol>
	<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
	<!--End breadcrumb-->
	<div id="page-content">
		<div class="panel">
			<div class="panel-heading">
				<h3 class="panel-title"><?php echo $pagetitle; ?></h3>
			</div>

			<div class="panel-body">
                <button style="margin-bottom: 1%;" id="add_product" type="button" class="btn btn-primary pull-right">Add Category</button>
                <br>
                <table id="prod_cat_table" class="table table-striped table-bordered table-responsive table-hover">
                    <thead>
                        <tr>
                            <th>Sr. No.</th>
                            <th>Category Name</th>
                            <th>Parent Material Name</th>
                            <th>Note</th>
                            <th>SEO URL</th>
                            <th>Category Order</th>
                            <th>Updated By</th>
                            <th>Category Image</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php $i = 1; foreach ($cat_data as $cat){ ?>
                        <tr>
                            <td><?php echo $i++; ?></td>
                            <td><?php echo $cat['category_name']; ?></td>
                            <td><?php if ( ! empty($cat['parent_id'])){
                                foreach ($cat_data as $value){
                                    if ($value['category_id'] == $cat['parent_id']){
                                        echo $cat['category_name'];
                                        break;
                                    }
                                }
                            } ?></td>
                            <td><?php echo $cat['note']; ?></td>
                            <td><?php echo $cat['seo_url']; ?></td>
                            <td><?php if ($cat['category_order'] == 1){
                                echo "Order 1";
                            } else if ($cat['category_order'] == 2){
                                echo "Order 2";
                            } else if ($cat['category_order'] == 3){
                                echo "Order 3";
                            } else if ($cat['category_order'] == 4){
                                echo "Order 4";
                            } else if ($cat['category_order'] == 5){
                                echo "Order 5";
                            } ?></td>
                            <td><?php echo $cat['updated_by']; ?></td>
                            <td>
                            <a ref="<?php echo s3BucketName.'/'.s3BucketTestDir?>images/prod_cat_image/<?php echo $cat['category_image']; ?>" target="_blank"><img class="img-thumbnail" src="<?php echo s3BucketName.'/'.s3BucketTestDir?>images/prod_cat_image/<?php echo $cat['category_image']; ?>">
                            </td>
                            <td>
                                <?php
                                    if (empty($level) && empty($category_id))
                                    {
                                        
                                ?>
                                   <a href="<?php $level_no = $level+1; echo base_url('/admin/salvage/product_category_master/').$level_no.'/'.$cat['category_id']; ?>">Sub Categories</a>
                                   <a id="edit_<?php echo $cat['category_id']; ?>" onclick="editProduct('<?php echo $cat['category_id']; ?>')" category_name="<?php echo $cat['category_name']; ?>" class="btn btn-primary" note="<?php echo $cat['note']; ?>" category_order="<?php echo $cat['category_order']; ?>" >Edit</a><br>
                                    <a onclick="deleteProduct('<?php echo $cat['category_id']; ?>')" class="btn btn-danger">Delete</a></td>
                                <?php
                                    }
                                    else
                                    {
                                ?>

                                   <a id="edit_<?php echo $cat['category_id']; ?>" onclick="editProduct('<?php echo $cat['category_id']; ?>')" category_name="<?php echo $cat['category_name']; ?>" class="btn btn-primary" note="<?php echo $cat['note']; ?>" category_order="<?php echo $cat['category_order']; ?>" >Edit</a><br>
                                    <a onclick="deleteProduct('<?php echo $cat['category_id']; ?>','<?php echo $category_id?>')" class="btn btn-danger">Delete</a></td>
                                <?php
                                    }
                                
                                ?>
                           
                        </tr>
                        <?php } ?>
                    <tbody>
                </table>
                <!-- Modal -->
                <div id="product_modal" class="modal fade" role="dialog">
                    <div class="modal-dialog">
                        <!-- Modal content-->
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                <h4 class="modal-title">Add Product Category</h4>
                            </div>
                            <div class="modal-body">
                                <form id="add_prod_cat_form" class="form-group">
                                    <div>
                                        <input id="parent_id" name="parent_id" type="hidden" value="<?php echo ! empty($category_id) ? $category_id : ""; ?>" />
                                        <input id="level" name="level" type="hidden" value="<?php echo ! empty($level) ? $level : ""; ?>" />
                                        <input id="category_id" name="category_id" type="hidden" />
                                        <label for="category_name">Category Name</label>
                                        <input id="category_name" class="form-control" name="category_name" type="text" />
                                    </div>
                
                                    <div><a id="change_image">Change Image</a></div>
                                    <div class="img">
                                        <label for="category_image">Category Image</label>
                                        <input class="form-control" name="category_image" type="file" id="img" accept="image/*">
                                    </div>
                                    <div>
                                        <label for="note">Note</label>
                                        <textarea id="note" class="form-control" name="note"></textarea>
                                    </div>
                                    <div>
                                        <label for="category_order">Category Order</label>
                                        <input type="number" id="category_order" class="form-control" name="category_order"/>
                                            
                                    </div>
                                </form>
                            </div>
                            <div class="modal-footer">
                                <a id="add_product_cat" class="btn btn-primary">Save</a>
                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            </div>
                        </div>
                    </div>
                </div>
			</div>
		</div>
	</div>
</div>
<!--===================================================-->
<!--END CONTENT CONTAINER-->

<script>

    $(document).ready(function(){
        $("#change_image").hide();
        $("#prod_cat_table").dataTable();
    });

    $("#add_product").click(function(){
        $("#product_modal").modal('show');
    });

    $("#add_product_cat").click(function(){
        $("#add_prod_cat_form").submit();
    });

    var vRules = {
        category_name:{required:true},
        note:{required:true},
        category_order:{required:true},
        category_image:{required:true}
    };

    var vMessages = {
        category_name:{required:"Please enter category name"},
        note:{required:"Please enter note"},
        category_order:{required:"Please enter category order"},
        category_image:{required:"Please Enter Category Image"}
    };

    $("#add_prod_cat_form").validate({
        rules: vRules,
        messages: vMessages,
        submitHandler: function(form){
            $(form).ajaxSubmit({
                url: '<?php echo base_url();?>admin/salvage/add_edit_prod_cat',
                type:"post",
                dataType: 'json',
                success: function (result) {
                    if (result.status == 'success'){
						Display_msg(result.message, result.status);
                        setTimeout(function(){ window.location.reload(1); }, 3000);
					} else {
						Display_msg(result.message, result.status);
					}
                },
                error: function(e){
                    alert('Invalid request please try again.');
                }
            });
        }
    });

    function editProduct(id)
    {
        $(".img").hide();
        $("#category_id").val(id);
        $("#category_name").val($("#edit_"+id).attr("category_name"));
        $("#note").val($("#edit_"+id).attr("note"));
        $("#category_order").val($("#edit_"+id).attr("category_order"));
        $("#change_image").show();
        $("#product_modal").modal('show');
    }

    $("#change_image").click(function(){
        $(".img").show();
    });

    $("#product_modal").on('hide.bs.modal', function(){
        clearAll();
    });

    function clearAll()
    {
        $(".img").show();
        $("#category_id").val("");
        $("#category_name").val("");
        $("#note").val("");
        $("#category_order").val("");
        $("#change_image").hide();
    }

    function deleteProduct(id,parent_id='')
    {
        $.ajax({
            url: '<?php echo base_url();?>admin/salvage/delete_category',
            type:"post",
            data:{ id: id ,parent_id :parent_id},
            dataType: 'json',
            success: function (result) {
                if (result.status == 'success'){
                    Display_msg(result.message, result.status);
                    setTimeout(function(){ window.location.reload(1); }, 3000);
                } else {
                    Display_msg(result.message, result.status);
                }
            },
            error: function(e){
                alert('Invalid request please try again.');
            }
        });
    }

</script>