<!--CONTENT CONTAINER-->
<!--===================================================-->
<div id="content-container">
	<!--Page Title-->
	<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
	<div id="page-title">
		<h1 class="page-header text-overflow"><?php echo $pagetitle; ?></h1>
	</div>
	<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
	<!--End page title-->

	<!--Breadcrumb-->
	<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
	<ol class="breadcrumb">
		<li><a href="<?php echo base_url('admin/dashboard');?>">Home</a></li>
		<li class="active"><?php echo $pagetitle; ?></li>
	</ol>
	<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
	<!--End breadcrumb-->
	<div id="page-content">
		<div class="panel">
			<div class="panel-heading">
				<h3 class="panel-title"><?php echo $pagetitle; ?></h3>
			</div>

			<div class="panel-body">
                <button style="margin-bottom: 1%;" id="add_manufacturer" type="button" class="btn btn-primary pull-right">Add Manufacturer</button>
                <br>
                <table id="manufacturer_table" class="table table-striped table-bordered table-responsive table-hover">
                    <thead>
                        <tr>
                            <th>Sr. No.</th>
                            <th>Manufacturer Name</th>
                            <th>Description</th>
                           <!--  <th>Manufacturer Logo</th> -->
                            <th>Updated By</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php $i = 1; foreach ($manufacturers as $value){ ?>
                        <tr>
                            <td><?php echo $i++; ?></td>
                            <td><?php echo $value['manufacturer_name']; ?></td>
                            <td><?php echo $value['short_descriptions']; ?></td>
<!--                             <td><a href="http://staticweb.salasarauction.com/marketplace/images/manufacturer_logo/<?php echo $value['manufacturer_logo']; ?>" target="_blank">Click Here</a></td>
 -->                            <td><?php echo $value['updated_by']; ?></td>
                            <td><a id="edit_<?php echo $value['manufacture_id']; ?>" onclick="editManufacturer('<?php echo $value['manufacture_id']; ?>')" manufacturer_logo="<?php echo $value['manufacturer_logo']; ?>" class="btn btn-primary" manufacturer_name="<?php echo $value['manufacturer_name']; ?>" description="<?php echo $value['short_descriptions']; ?>">Edit</a><br>
                            <a onclick="deleteManufacturer('<?php echo $value['manufacture_id']; ?>')" class="btn btn-danger">Delete</a></td>
                        </tr>
                        <?php } ?>
                    <tbody>
                </table>
                <!-- Modal -->
                <div id="manufacturer_modal" class="modal fade" role="dialog">
                    <div class="modal-dialog">
                        <!-- Modal content-->
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                <h4 class="modal-title">Add/Edit Manufacturer</h4>
                            </div>
                            <div class="modal-body">
                                <form id="manufacturer_modal_form" class="form-group">
                                    <div>
                                        <input id="manufacture_id" name="manufacture_id" type="hidden" />
                                        <label for="manufacturer_name">Manufacturer Name</label>
                                        <input id="manufacturer_name" class="form-control" name="manufacturer_name" type="text" />
                                    </div>
                                    <div>
                                        <label for="short_descriptions">Description</label>
                                        <textarea id="short_descriptions" class="form-control" name="short_descriptions"></textarea>
                                    </div>
                                   <!--  <div><a id="change_image">Change Image</a></div>
                                    <div class="img">
                                        <label for="manufacturer_logo">Category Image</label>
                                        <input class="form-control" name="manufacturer_logo" type="file" id="manufacturer_logo" accept="image/*">
                                    </div> -->
                                </form>
                            </div>
                            <div class="modal-footer">
                                <a id="add_manufacturer_data" class="btn btn-primary" onclick="submit_form();">Save</a>
                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            </div>
                        </div>
                    </div>
                </div>
			</div>
		</div>
	</div>
</div>
<!--===================================================-->
<!--END CONTENT CONTAINER-->

<script>
    $(document).ready(function(){
        $("#change_image").hide();
        $("#manufacturer_table").dataTable();
    });

    $("#add_manufacturer").click(function(){
        $("#manufacturer_modal").modal('show');
    });

    $("#add_manufacturer_data").click(function(){
        $("#manufacturer_modal_form").submit();
    });
    function submit_form()
    {
        $("#add_manufacturer_data").attr('disabled',true);
        var vRules = {
            manufacturer_name:{required:true},
            short_descriptions:{required:true},
        };

        var vMessages = {
            manufacturer_name:{required:"Please enter manufacturer name"},
            short_descriptions:{required:"Please enter description"},
        };

        $("#manufacturer_modal_form").validate({
            rules: vRules,
            messages: vMessages,
            submitHandler: function(form){
                $(form).ajaxSubmit({
                    url: '<?php echo base_url();?>admin/salvage/add_edit_manufacturer',
                    type:"post",
                    dataType: 'json',
                    success: function (result) {
                        if (result.status == 'success'){
                            Display_msg(result.message, result.status);
                            setTimeout(function(){ window.location.reload(1); }, 3000);
                        } else {
                            Display_msg(result.message, result.status);
                        }
                        $("#add_manufacturer_data").removeAttr('disabled');
                    },
                    error: function(e){
                        alert('Invalid request please try again.');
                    }
                });
            }
        });

    }
   
    function editManufacturer(id)
    {
        $(".img").hide();
        $("#manufacture_id").val(id);
        $("#manufacturer_name").val($("#edit_"+id).attr("manufacturer_name"));
        $("#short_descriptions").val($("#edit_"+id).attr("description"));
        $("#change_image").show();
        $("#manufacturer_modal").modal('show');
    }

    $("#change_image").click(function(){
        $(".img").show();
    });

    $("#manufacturer_modal").on('hide.bs.modal', function(){
        clearAll();
    });

    function clearAll()
    {
        $(".img").show();
        $("#manufacture_id").val("");
        $("#manufacturer_name").val("");
        $("#short_descriptions").val("");
        $("#change_image").hide();
    }

    function deleteManufacturer(id)
    {
        $.ajax({
            url: '<?php echo base_url();?>admin/salvage/delete_manufacturer',
            type:"post",
            data:{ id: id },
            dataType: 'json',
            success: function (result) {
                if (result.status == 'success'){
                    Display_msg(result.message, result.status);
                    setTimeout(function(){ window.location.reload(1); }, 3000);
                } else {
                    Display_msg(result.message, result.status);
                }
            },
            error: function(e){
                alert('Invalid request please try again.');
            }
        });
    }

</script>