
<!--CONTENT CONTAINER-->
<!--===================================================-->
<div id="content-container">
	<!--Page Title-->
	<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
	<div id="page-title">
		<h1 class="page-header text-overflow"><?php echo $pagetitle;?></h1>
	</div>
	<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
	<!--End page title-->


	<!--Breadcrumb-->
	<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
	<ol class="breadcrumb">
		<li><a href="#">Settings</a></li>
		<li><a href="<?php echo base_url('admin/dashboard');?>">Master Data</a></li>
		<li class="active"><?php echo $pagetitle;?></li>
	</ol>
	<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
	<!--End breadcrumb-->
	<input type="hidden" id="t11" name="auctionId">
	<div id="page-content">	
		<div class="panel">
			<div class="panel-heading">
				<h3 class="panel-title"><?php echo $pagetitle;?></h3>
				<div class="col-sm-3 pull-right">
					<button class="btn btn-info pull-right" style="margin:5%;" id="add_currency" onclick='add_currency();'>ADD</button>
				</div>
			</div>
			<div class="panel-body">
                <table id="currency_table" class="table-responsive table-striped table table-bordered table-hover">
                    <thead>
                        <tr>
                            <th>SR. NO.</th>
                            <th>CURRENCY NAME</th>
                            <th>CURRENCY SYMBOL</th>
                            <th>COUNTRY NAME</th>
                            <th>ACTION</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php $i = 1; foreach ($currencies as $value){ ?>
                        <tr>
                            <td><?php echo $i++; ?></td>
                            <td><?php echo $value['currency_name']; ?></td>
                            <td><?php echo $value['currency_symbol']; ?></td>
                            <td><?php echo $value['country_name']; ?></td>
                            <td><a id="edit_<?php echo $value['id']; ?>" onclick="editCurrency('<?php echo $value['id']; ?>')" currency_name="<?php echo $value['currency_name']; ?>" currency_symbol="<?php echo $value['currency_symbol']; ?>" country_id="<?php echo $value['country_id']; ?>" is_active="<?php echo $value['is_active']; ?>" class="btn btn-primary">Edit</a><a onclick="deleteCurrency('<?php echo $value['id']; ?>')" class="btn btn-danger">Delete</a></td>
                        </tr>
                        <?php } ?>
                    </tbody>
                </table>
			</div>
		</div>
	</div>	
</div>
<!--===================================================-->
<!--END CONTENT CONTAINER-->
<!--modal-->
    <div class="modal fade" id="add_edit_currency" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Add/Edit Currency</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form id="currency_form" class="form-group">
                        <div class="row">
                            <div class="col-4">
                                <label>Currency Name : </label>
                            </div>
                            <div class="col-8">
                                <input type="hidden" class="form-control" id="currency_id" name="currency_id" />
                                <input type="text" class="form-control" id="currency_name" name="currency_name" />
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-4">
                                <label>Currency Symbol : </label>
                            </div>
                            <div class="col-8">
                                <input type="text" class="form-control" id="currency_symbol" name="currency_symbol" />
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-4">
                                <label>Country Name : </label>
                            </div>
                            <div class="col-8">
                                <select class="form-control" id="country_id" name="country_id" >
                                    <option value=""></option>
                                    <?php foreach ($country_list as $value){ ?>
                                        <option value="<?php echo $value['id'] ?>"><?php echo $value['name'] ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-4">
                                <label>Is Active : </label>
                            </div>
                            <div class="col-8">
                                <select class="form-control" id="is_active" name="is_active" >
                                    <option value="">--->Select Status<---</option>
                                    <option value="1">Active</option>
                                    <option value="0">Inactive</option>
                                </select>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" onclick="submit_form()">Save changes</button>
                </div>
            </div>
        </div>
    </div>
<!--modal end-->
<script>

$(document).ready(function(){
    $("#country_id").select2({
        dropdownParent: $('#add_edit_currency'),
        placeholder: "Select Country Name",
        width: "100%"
    });
    $("#currency_table").dataTable();
})

function editCurrency(id)
{
    clearCurrencyForm();
	$('#currency_id').val(id);
	$('#currency_name').val($("#edit_"+id).attr("currency_name"));
	$('#currency_symbol').val($("#edit_"+id).attr("currency_symbol"));
	$('#country_id').val($("#edit_"+id).attr("country_id")).trigger('change');
	$('#is_active').val($("#edit_"+id).attr("is_active"));
	$("#add_edit_currency").modal('show');
}

function deleteCurrency(id)
{
    $.ajax({
		url :'<?php echo base_url('admin/master/delete_currency')?>',
		data:{'currency_id':id},
		type:'POST',
		dataType:'json',
		success:function(response)
		{
            if (response.status == "success") {
                Display_msg(response.message,response.status);
                setTimeout(function(){
                    window.location.reload(1);
                }, 3000);
            } else {
                Display_msg(response.message,response.status);
            }
		},
        error: function()
        {
            Display_msg('Problem in deleting currency data please try again','failed');
        }
	 });
}

function clearCurrencyForm()
{
    $('#currency_id').val('');
	$('#currency_name').val('');
	$('#currency_symbol').val('');
	$('#country_id').val('');
	$('#is_active').val('');
}

function add_currency()
{
    clearCurrencyForm();
	$("#add_edit_currency").modal('show');
}

function submit_form()
{
    $("#currency_form").submit();
}

var vRules = {
    country_id:{required:true},
    currency_name:{required:true},
    currency_symbol:{required:true},
};
var vMessages = {
    country_id:{required:"<p class='text-danger'>Please Select Country Name</p>"},
    currency_name:{required:"<p class='text-danger'>Please Enter Currency Name</p>"},
    currency_symbol:{required:"<p class='text-danger'>Please Enter Currency Symbol</p>"},
};

$("#currency_form").validate({
    rules: vRules,
    messages: vMessages,
    submitHandler: function(form){
        $(form).ajaxSubmit({
            url:"<?php echo base_url('admin/master/add_edit_currency/');?>",
            type: 'post',
            dataType:'json',
            cache: false,
            success: function (response) {
                if(response.status=="success"){
                    Display_msg(response.message,response.status);
                    setTimeout(function(){
                        window.location.reload(1);
                    }, 3000);
                } else {
                    Display_msg(response.message,response.status);
                    return false;
                }
            }
        });
    }
});


</script>
