<!--CONTENT CONTAINER-->
<!--===================================================-->
<div id="content-container">
	<!--Page Title-->
	<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
	<div id="page-title">
		<h1 class="page-header text-overflow">Vendor List</h1>
	</div>
	<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
	<!--End page title-->

	<!--Breadcrumb-->
	<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
	<ol class="breadcrumb">
		<li><a href="<?php echo base_url('admin/dashboard');?>">Home</a></li>
		<li class="active">Vendor List</li>
	</ol>
	<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
	<!--End breadcrumb-->
	<input type="hidden" id="t11" name="auctionId">
	<div id="page-content">
		<div class="panel">
			<div class="panel-heading">
				<h3 class="panel-title">Vendor List</h3>
			</div>

			<div class="panel-body">
				<form name="frm" method="post" onsubmit="SubmitButton()">
					<div class="row filter">

						<div class="col-sm-2">
							<input class="form-control searchInput" placeholder="Vendor Name" type="text" name="search_vendor_name" id="search_vendor_name" value="" />
						</div>
						<div class="col-sm-2">
							<input class="form-control searchInput" placeholder="Country" type="text" name="search_vendor_country" id="search_vendor_country" value="" />
						</div>
						<div class="col-sm-2">
							<input class="form-control searchInput" placeholder="State" type="text" name="search_vendor_state" id="search_vendor_state" value="" />
						</div>
						<div class="col-sm-2">
							<input class="form-control searchInput" placeholder="City" type="text" name="search_vendor_city" id="search_vendor_city" value="" />
						</div>
						<div class="col-sm-2 pull-right">
							<a style=" float: left;" class="btn btn-default pull-right" onclick="refresh_datatable();">Refresh </a>
							<a style=" float: left;" class="btn btn-info pull-right" onclick="clearSearchFilters();">Clear Search </a>
						</div>
					</div>
					<div class="table-responsive">
						<table callfunction="<?php echo base_url('admin/vendor/fetch');?>" class="table table-striped table-bordered table-hover no-footer dtr-inline dynamicTable">
							<thead>
								<tr>
									<th data-bSortable="false">SR</th>
									<th>VENDOR NAME</th>
									<th data-bSortable="false">ADDRESS</th>
									<th>COUNTRY</th>
									<th>STATE</th>
									<th>CITY</th>
									<th>REGISTERED DATE</th>
									<th>ADMIN APPROVAL</th>
									<th>ACTIVE TILL</th>
									<th data-bSortable="false">ACTION</th>
								</tr>
							</thead>
							<tbody>
							</tbody>
							<input type="hidden" id="vendorid" name="vendorid">
						</table>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>
<!--===================================================-->
<!--END CONTENT CONTAINER-->

<script>
	$(document).on('focus',".active_date_to", function(){
		$(this).datepicker({
			format: 'yyyy-mm-dd'
		}).on('changeDate', function(e) {
			$.ajax({
				url: '<?php echo base_url("admin/vendor/change_to_date")  ;?>',
				data:{
					'vendor_id': $(this).attr('activeDate'),
					'to_date': $("#active_date_to_"+$(this).attr('activeDate')).val()
				},
				type: 'post',
				dataType: 'json',
				success: function (result){
					if (result.status == 'success'){
						Display_msg(result.message,result.status);
					} else {
						Display_msg(result.message,result.status);
					}
				}
			});
		});
	});

	function approvalStatus(vendorId, approval)
	{
		$.ajax({
            url: '<?php echo base_url("admin/vendor/admin_vendor_approval");?>',
            data:{
                'vendor_id': vendorId,
                'approval': approval
            },
            type: 'post',
            dataType: 'json',
            success: function (result) {
                Display_msg(result.message,result.status);
                refresh_datatable();
            },
            error: function (e){
                Display_msg('Please refresh the page and try again','failed');
            }
        });
	}

</script>