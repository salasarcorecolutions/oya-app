<!--CONTENT CONTAINER-->
<!--===================================================-->
<div id="content-container">
    <!--Page Title-->
    <!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
    <div id="page-title">
        <h1 class="page-header text-overflow"><?php echo $pagetitle ?></h1>
    </div>
    <style>
    .table>tbody>tr>td, .table>tbody>tr>th, .table>tfoot>tr>td, .table>tfoot>tr>th, .table>thead>tr>td, .table>thead>tr>th{
        padding: 4px !important;
    }
    </style>
    <link href="//cdn.datatables.net/1.10.20/css/jquery.dataTables.min.css" rel="stylesheet">


<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
<!--End page title-->

<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
<!--End breadcrumb-->
	<div id="page-content">
		<div class="panel">
			<div class="panel-heading">
				<h3 class="panel-title col-sm-5"><?php echo $pagetitle ?></h3>
            </div>
        	<div class="panel-body">
				<div class="table-responsive">
                    <table id="permission_table" class="table table-bordered">
                        <thead>
                            <tr>
                                <th>Sr. No.</th>
                                <th>Company Name</th>
                                <th>User Name</th>
                                <th>Vendor Name</th>
                                <th>Plant Name</th>
                                <th>Admin Approval</th>
                            </tr>
                        </thead>
                        <tbody>
                        <?php $i = 1; foreach($company_details as $value){ ?>
                        <tr>
                            <td><?php echo $i++; ?></td>
                            <td><?php echo $value['c_name'] ?></td>
                            <td><?php echo $value['uname'] ?></td>
                            <td><?php echo $value['vendor_name'] ?></td>
                            <td><?php echo $value['plant_name'] ?></td>
                            <td><a onclick="updateApproval('<?php echo $value['user_company_relation_id'] ?>','<?php echo $value['admin_approval'] ?>')"><?php echo $value['admin_approval'] ?></a></td>
                        </tr>
                        <?php } ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<!--===================================================-->
<!--END CONTENT CONTAINER-->
<script src="//cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>

<script>
    function updateApproval(id, adminApproval){
        $.ajax({
            url: '<?php echo base_url("admin/view_company_details/update_admin_approval");?>',
            data:{
                'user_company_relation_id': id,
                'admin_approval': adminApproval
            },
            type: 'post',
            dataType: 'json',
            success: function (result) {
                Display_msg(result.message,result.status);
                setTimeout(function(){
                    window.location.reload(1);
                }, 3000);
            },
            error: function (e){
                Display_msg('Please refresh the page and try again','failed');
            }
        });
    }
</script>