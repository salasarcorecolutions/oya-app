<style>
fieldset {
  display: block;
  margin-left: 2px;
  margin-right: 2px;
  padding-top: 0.35em;
  padding-bottom: 0.625em;
  padding-left: 0.75em;
  padding-right: 0.75em;
  border: 2px groove (internal value);
}

</style>
<!--CONTENT CONTAINER-->
<!--===================================================-->
<div id="content-container">
	<!--Page Title-->
	<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
	<div id="page-title">
		<h1 class="page-header text-overflow"><?php echo $pagetitle?></h1>
	</div>
	<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
	<!--End page title-->
	<!--Breadcrumb-->
	<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
	<ol class="breadcrumb">
		<li><a href="<?php echo base_url('admin/dashboard');?>">Home</a></li>
		<li class="active"><?php echo $pagetitle?></li>
	</ol>
	<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
	<!--End breadcrumb-->
	<input type="hidden" id="t11" name="auctionId">
	<div id="page-content">
		<div class="panel">
			<div class="panel-heading">
				<h3 class="panel-title"><?php echo $pagetitle?></h3>
			</div>
			<div class="panel-body">
				<form name="frm" method="post" id="bidder_form">
					<input type="hidden" name="bidder_id" id="bidder_id" value="<?php echo $bidder_details['id'];?>" />
					<fieldset>
						<legend>Company Details:</legend>
							<div class="col-sm-12 form-group">
								<label class="col-sm-2 control-label">Company Name :</label>
								<div class="col-md-6">
									<input type="text" class="form-control" name="comp_name" id="comp_name" value="<?php echo $bidder_details['compname'];?>" />
								</div>
							</div>
							<div class="col-sm-12 form-group">
								<label class="col-sm-2 control-label">Contact Person :</label>
								<div class="col-md-6">
									<input type="text" class="form-control" name="conperson" id="conperson" value="<?php echo $bidder_details['conperson'];?>"/>
								</div>
							</div>
							<div class="col-sm-12 form-group">
								<label class="col-sm-2 control-label">Address :</label>
								<div class="col-md-6">
									<input type="text" class="form-control" name="add" id="add" value="<?php echo $bidder_details['adddress'];?>" />
								</div>
							</div>
							<div class="col-sm-12 form-group">
								<label class="col-sm-2 control-label">Country :</label>
								<div class="col-md-6">
									<select class="form-control selectpicker" name="bidder_country" id="bidder_country" onchange="fetch_state(this.id)" attr = "bidder">
									<option value="">--Select One--</option>
									<?php
									if ( ! empty($countries)):
										foreach ($countries as $key=>$value):
									?>
										<option value="<?php echo $value['name']?>"><?php echo $value['name']?></option>

									<?php
										endforeach;
									endif;
									?>
									</select>
								</div>
							</div>
							<div class="col-sm-12 form-group">
								<label class="col-sm-2 control-label">State :</label>
								<div class="col-md-6">
									<select class="form-control" name="bidder_state" id="bidder_state" attr = 'bidder' onchange="fetch_city(this.id);" >
									</select>
								</div>
							</div>
							<div class="col-sm-12 form-group">
								<label class="col-sm-2 control-label">City :</label>
								<div class="col-md-6">
									<select class="form-control" name="bidder_city" id="bidder_city">
									</select>
								</div>
							</div>
							<div class="col-sm-12 form-group">
								<label class="col-sm-2 control-label">Pin :</label>
								<div class="col-md-6">
									<input type="text" class="form-control" name="pin" id="pin" value="<?php echo $bidder_details['pin'];?>"/>
								</div>
							</div>
				   </fieldset>
				   <fieldset>
						<legend>Personal Details:</legend>
							<div class="col-sm-12 form-group">
								<label class="col-sm-2 control-label">Mob :</label>
								<div class="col-md-6">
									<input type="text" class="form-control" name="mob" id="mob" value="<?php echo $bidder_details['mob'];?>" />
								</div>
							</div>
							<div class="col-sm-12 form-group">
								<label class="col-sm-2 control-label">Other Mob:</label>
								<div class="col-md-6">
									<input type="text" class="form-control" name="other_mob" id="other_mob" value="<?php echo $bidder_details['other_mob'];?>" />
								</div>
							</div>
							<div class="col-sm-12 form-group">
								<label class="col-sm-2 control-label">Tel :</label>
								<div class="col-md-6">
									<input type="text" class="form-control" name="tel" id="tel" value="<?php echo $bidder_details['tel'];?>" />
								</div>
							</div>
							<div class="col-sm-12 form-group">
								<label class="col-sm-2 control-label">Email:</label>
								<div class="col-md-6">
									<input type="text" class="form-control" name="email" id="email" value="<?php echo $bidder_details['email'];?>" />
								</div>
							</div>
							<div class="col-sm-12 form-group">
								<label class="col-sm-2 control-label">Alternate Email:</label>
								<div class="col-md-6">
									<input type="text" class="form-control" name="alt_email" id="alt_email" value="<?php echo $bidder_details['other_email'];?>" />
								</div>
							</div>
							<div class="col-sm-12 form-group">
								<label class="col-sm-2 control-label">User Name:</label>
								<div class="col-md-6">
									<input type="text" class="form-control" name="userid" id="userid" value="<?php echo $bidder_details['userid'];?>" />
								</div>
							</div><div class="col-sm-12 form-group">
								<label class="col-sm-2 control-label">Password:</label>
								<div class="col-md-6">
									<input type="text" class="form-control" name="pass" id="pass" value="<?php echo $bidder_details['password'];?>" />
								</div>
							</div>
							<div class="col-sm-12 form-group">
								<label class="col-sm-2 control-label">Fax :</label>
								<div class="col-md-6">
									<input type="text" class="form-control" name="fax" id="fax" value="<?php echo $bidder_details['fax'];?>" />
								</div>
							</div>
				   </fieldset>
				   <fieldset>
						<legend>Other Details:</legend>
							<div class="col-sm-12 form-group">
								<label class="col-sm-2 control-label">Currency:</label>
								<div class="col-md-6">
									<input type="text" class="form-control" name="currency" id="currency"  value="<?php echo $bidder_details['currency'];?>"/>
								</div>
							</div>
							<div class="col-sm-12 form-group">
								<label class="col-sm-2 control-label">Type of Company :</label>
								<div class="col-md-6">
									<select class="form-control" name="type_of_comp" id="type_of_comp">
										<option value="">Select Type Of Company</option>
										<option value="Individual" >Individual</option>
										<option value="Sole Proprietorship">Sole Proprietorship</option>
										<option value="Partnership">Partnership</option>
										<option value="Private Ltd.">Private Ltd.</option>
										<option value="Public Ltd">Public Ltd.</option>
										<option value="Trust">Trust</option>
										<option value="Other">Other</option>

									</select>
								</div>
							</div>
							<div class="col-sm-12 form-group">
								<label class="col-sm-2 control-label">Nature of Acitivity :</label>
								<div class="col-md-6">
									<select class="form-control" name="nature_of_act" id="nature_of_act">
										<option value="">Select Nature of Activity</option>
											<option value="Trading">Trading</option>
											<option value="Manufacturing">Manufacturing</option>
											<option value="Both">Trading & Manufacturing</option>
											<option value="Other">Other</option>
									</select>
								</div>
							</div>
							<div class="col-sm-12 form-group">
								<label class="col-sm-2 control-label">Material Interested :</label>
								<div class="col-md-6">
									<select class="selectpicker" multiple title="Material Interested In" name="category[]" id="category">

											<?php

											for($i=0;$i<count($material['category']);$i++):
											?>
												<option value='<?php echo $material['category'][$i]['CategoryName']; ?>'><?php echo $material['category'][$i]['CategoryName']?></option>
											<?php
											endfor;
											?>
										</select>
								</div>
							</div>

							<div class="col-sm-12 form-group">
								<label class="col-sm-2 control-label">CPCB:</label>
								<div class="col-md-6">
									<select class="form-control" name="cpcb" id="cpcb">
									<option value= "Available">Available</option>
									<option value= "Not Available">Not Available</option>

									</select>
								</div>
							</div>
							<div class="col-sm-12 form-group">
								<label class="col-sm-2 control-label">Pan Available :</label>
								<div class="col-md-6">
									<select class="form-control" name="pan_avail" id="pan_avail">
									<option value= "Y">Yes</option>
									<option value= "N">No</option>

									</select>
								</div>
							</div>
							<div class="col-sm-12 form-group">
								<label class="col-sm-2 control-label">Pan No :</label>
								<div class="col-md-6">
									<input type="text" class="form-control" name="panno" id="panno" value="<?php echo $bidder_details['panno']?>">
								</div>
							</div>
							<div class="col-sm-12 form-group">
								<label class="col-sm-2 control-label">Tin :</label>
								<div class="col-md-6">
									<input type="text" class="form-control" name="tin" id="tin" value="<?php echo $bidder_details['tin'];?>" />
								</div>
							</div>
							<div class="col-sm-12 form-group">
								<label class="col-sm-2 control-label">IES :</label>
								<div class="col-md-6">
									<input type="text" class="form-control" name="iec" id="iec" value="<?php echo $bidder_details['iec'];?>" />
								</div>
							</div>
							<div class="col-sm-12 form-group">
								<label class="col-sm-2 control-label">Interested Region :</label>
								<div class="col-md-6">
									<select name="int_region[]" id="int_region" class="selectpicker form-control" multiple>
										<option value= "east">East</option>
										<option value= "north">North</option>
										<option value= "south">South</option>
										<option value= "west">West</option>
									</select>
								</div>
							</div>
							<div class="col-sm-12 form-group">
								<label class="col-sm-2 control-label">Interested In :</label>
								<div class="col-md-6">
									<div class="col-sm-3">
										<input type="checkbox" name="intrF" id="intrF" value="F"
										<?php
											if($bidder_details['intrF']=='F'):
											echo 'checked';
											endif;

										?>

										/> Forward Auction
									</div>
									<div class="col-sm-3">
										<input type="checkbox" name="intrR" id="intrR" value="R"
										<?php
											if($bidder_details['intrR']=='R'):
												echo 'checked';
											endif;

										?>
										/> Reverse Auction
									</div>
									<div class="col-sm-3">
										<input type="checkbox" name="intrT" id="intrT" value="T"
										<?php
											if($bidder_details['intrT']=='T'):
											echo 'checked';
											endif;


										?>

										/> Tender
									</div>
								</div>
							</div>
							<div class="col-sm-12 form-group">
								<label class="col-sm-2 control-label">KYC :</label>
								<div class="col-md-6">
									<select class="form-control selectpicker" name="kyc" id="kyc">
											<option value="Yes"
											<?php
												if($bidder_details['kyc']=='Yes'):
												echo 'selected';
												endif;


											?>

											>Yes</option>
											<option value="No"
											<?php
												if($bidder_details['kyc']=='No'):
												echo 'selected';
												endif;

											?>

											>No</option>
									</select>
								</div>
							</div>
							<div class="col-md-6">
								<button class="btn btn-success pull-right" type="submit">Save</button>
							</div>
							<div class="col-md-6">
								<a href="<?php echo base_url('admin/bidder_details/bidder_details')?>">
								<button class="btn btn-danger" type="button" >Back</button>
								</a>
							</div>
				   </fieldset>
				</form>
			</div>
		</div>
	</div>
</div>

<!--===================================================-->
<!--END CONTENT CONTAINER-->
<script>
$(document).ready(function(){
	//for company
	var cmp_id = 'bidder_country';
	var state_id = 'bidder_state';
	var city_id = 'bidder_city';
	$("#"+cmp_id).val('<?php echo $bidder_details['country']?>');
	$("#"+cmp_id).trigger("change");
	var int_mat  = '<?php echo $bidder_details['material_interested']?>';
	var mat = int_mat.split(',');
	if (mat!=''){
		$("#category").val(mat);
	}
	var int_region = '<?php echo $bidder_details['interested_in_region']?>';
	var region = int_region.split(',');
	$("#int_region").val(region);
	$("#type_of_comp").val('<?php echo $bidder_details['type_of_comp']?>');
	$("#nature_of_act").val('<?php echo $bidder_details['nature_of_activity']?>');

});
var vRules 	= 	'';
var vMessages = '';
$("#bidder_form").validate({
	rules: vRules,
	messages: vMessages,
	submitHandler: function(form){
		$(form).ajaxSubmit({
			url: '<?php echo base_url('admin/bidder_details/update_details');?>',
			type:"post",
			dataType: 'json',
			success: function (resObj) {
				Display_msg(resObj.message,resObj.status);
			},
			error: function(e){
				Display_msg('Problem in updating details please try again.','failed');
			}
		});
	}
});
function fetch_state(id)
{
	var country_name = $("#"+id).val();
	var attr = $("#"+id).attr('attr');
	if(country_name){
		jQuery.ajax({
			url: '<?php echo base_url();?>clients/fetch_state',
			type:"post",
			data: {'country_name':country_name},
			success: function (resObj) {
				$("#"+attr+"_state").html('<option value="">Select State</option>'+resObj);

				$("#"+attr+"_state").val('<?php echo $bidder_details['state']?>');
				
				$("#"+attr+"_state").selectpicker('render');
				$("#"+attr+"_state").trigger("change");
				$("#"+attr+"_state").selectpicker('refresh');

			},
			error: function(){
				$("#"+attr+"_state").html('<option value="">Select State</option>');
				$("#"+attr+"_state").selectpicker('refresh');
				$("#"+attr+"_state").selectpicker('render');
			}
		});

	} else {
		$("#"+attr+"_state").html('<option value="">Select State</option>');
		$("#"+attr+"_state").selectpicker('refresh');
		$("#"+attr+"_state").selectpicker('render');
	}
}
function fetch_city(id){
	var state_name = $("#"+id).val();
	var attr = $("#"+id).attr('attr');
	if(state_name) {
		jQuery.ajax({
			url: '<?php echo base_url();?>clients/fetch_city',
			type:"post",
			data: {'state_name':state_name},
			success: function (resObj) {
				$("#"+attr+"_city").html('<option value="">Select City</option>'+resObj);

				$("#"+attr+"_city").val('<?php echo $bidder_details['city']?>');

				$("#"+attr+"_city").selectpicker('refresh');
				$("#"+attr+"_city").selectpicker('render');
			},
			error: function(){
				$("#"+attr+"_city").html('<option value="">Select City</option>');
				$("#"+attr+"_city").selectpicker('refresh');
				$("#"+attr+"_city").selectpicker('render');
			}
		});

	} else {
		$("#"+attr+"_city").html('<option value="">Select City</option>');
		$("#"+attr+"_city").selectpicker('refresh');
		$("#"+attr+"_city").selectpicker('render');
	}
}

</script>