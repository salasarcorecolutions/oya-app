
<!--CONTENT CONTAINER-->
<!--===================================================-->
<div id="content-container">
	<!--Page Title-->
	<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
	<div id="page-title">
		<h1 class="page-header text-overflow"><?php echo $pagetitle;?></h1>
	</div>
	<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
	<!--End page title-->


	<!--Breadcrumb-->
	<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
	<ol class="breadcrumb">
		<li><a href="#">Home</a></li>
		<li><a href="<?php echo base_url('admin/dashboard');?>">FAQ</a></li>
		<li class="active"><?php echo $pagetitle;?></li>
	</ol>
	<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
	<!--End breadcrumb-->
	<input type="hidden" id="t11" name="auctionId">
	<div id="page-content">	
		<div class="panel">
			<div class="panel-heading">
				<h3 class="panel-title"><?php echo $pagetitle;?></h3>
				<div class="col-sm-3 pull-right">
					<button class="btn btn-info pull-right" style="margin:5%;" id="add_mat" onclick='add_mat();'>ADD</button>
				</div>

			</div>
			
			<div class="panel-body">
				<form name="frm" method="post" id="auctioneer_form">
					<div class="row filter">
						
						
						
						<div class="col-sm-2">
							<select class="form-control searchInput" name="is_active" id="active" >
								<option value="1">Yes</option>
								<option value="0">No</option>
							</select>

						</div>
						
						<div class="col-sm-2 pull-right">
							<a style=" float: left;" class="btn btn-default pull-right" onclick="refresh_datatable();">Refresh </a>
							<a style=" float: left;" class="btn btn-info pull-right" onclick="clearSearchFilters();">Clear Search </a>
						</div>
					</div>
					<div class="table-responsive">
						<table callfunction="<?php echo base_url('admin/master/fetch_faq')?>" class="table table-striped table-bordered table-hover no-footer dtr-inline dynamicTable">
							<thead>
								<tr>
									<th data-bSortable="false">SR</th>
									<th>FAQ Question</th>
									<th>FAQ Answer</th>
									<th>IS ACTIVE</th>
									<th>UPDATED BY</th>
									<th data-bSortable="false">UPDATED ON</th>
									<th data-bSortable="false">ACTION</th>
								</tr>
							</thead>
							<tbody>
							</tbody>
						</table>
					</div>	
				</form>
			</div>	
		</div>
	</div>	
</div>
<!--===================================================-->
<!--END CONTENT CONTAINER-->
<!--modal-->
		<div class="modal fade" id="add_faq_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
			<div class="modal-dialog" role="document">
				<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title" id="exampleModalLabel">Add FAQ</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
				
						<form id="add_group_form" class="form-horizontal" name="add_group_form">
                            <input type="hidden" name="id" id="id"/>
                            <div class="form-group">
								<div class="col-sm-3">
									<label class="label-control">FAQ Group:</label>
								</div>
								<div class="col-sm-8">
									<select class="form-control" name="faq_group" id="faq_group">
                                        <option value=''>--select one--</option>
                                        <?php if ( ! empty($faq_group))
                                        {
                                            foreach($faq_group as $k=>$v)
                                            {
                                        ?>
                                                <option value="<?php echo $v['id']?>"><?php echo $v['group_name']?></option>
                                        <?php
                                            }
                                        } ?>
                                    </select>   
								</div>
                            </div>
							<div class="form-group">
								<div class="col-sm-3">
									<label class="label-control">FAQ Question:</label>
								</div>
								<div class="col-sm-8">
									<textarea class="form-control" name="quest" id="quest"></textarea>
								</div>
                            </div>
                            <div class="form-group">
								<div class="col-sm-3">
									<label class="label-control">FAQ Answer:</label>
								</div>
								<div class="col-sm-8">
									<textarea  class="form-control ckeditor" name="ans" id="ans"></textarea>
								</div>
							</div>
							<div class="form-group">
								<div class="col-sm-3">
									<label class="label-control">FAQ Question Order:</label>
								</div>
								<div class="col-sm-6">
									<input name='order' class="form-control" id="order" type="number" />
								</div>
							</div>
							<div class="form-group">
								<div class="col-sm-3">
									<label class="label-control">Is Active:</label>
								</div>
								<div class="col-sm-6">
									<select class="form-control" name="published" id="published">
										<option value="1">Yes</option>
										<option value="0">No</option>
									</select>
								</div>
							</div>
						</form>		
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-primary" onclick="submit_form()">Save changes</button>
				</div>
				</div>
			</div>
		</div>
<!--modal end-->
<script src="https://cdn.ckeditor.com/4.14.0/standard/ckeditor.js"></script> 
<script>
function update(id)
{
    CKEDITOR.instances.ans.setData(id.getAttribute("faq_ans"));
	$("#quest").html(id.getAttribute("faq_quest"));
	$('#id').val(id.getAttribute("id"));
	$('#order').val(id.getAttribute("order"));
    $('#is_active').val(id.getAttribute("is_active"));
	$("#faq_group").val(id.getAttribute("group_id"));
	

	$("#add_faq_modal").modal('show');
}
function add_mat()
{
	CKEDITOR.instances.ans.setData('');
	document.getElementById("add_group_form").reset();
	$("#add_faq_modal").modal('show');
}

function submit_form()
{
    var form = $("#add_group_form");
	form.validate({
		ignore:[],
		rules: {
            published: "required",
            faq_order: "required",
			faq_group :"required",
			quest :"required",
            ans:{ 
                required: function() 
                    {
                        CKEDITOR.instances.ans.updateElement();
                    }, 
                minlength:10

            },
		},
		messages: {
			quest: "<b class='text-danger'>Please Enter FAQ Question</b>",
            ans: "<b class='text-danger'>Please Enter FAQ Answer</b>",
            faq_group:"<b class='text-danger'>Please Enter FAQ Group</b>",
			faq_order: "<b class='text-danger'>Please Enter FAQ  Order</b>",
			published:"<b class='text-danger'>Please select one</b>"
		}
	});

	if (form.valid()){
		$('#add_group_form').ajaxSubmit({
			url:"<?php echo base_url();?>admin/master/add_edit_faq",
			type: 'post',
			dataType:'json',
			cache: false,
			clearForm: false,
			success: function (response)
			{
				if (response.status=="success")
				{
					Display_msg(response.message,response.status);
					refresh_datatable();
					$("#add_faq_modal").modal("hide");
				}
				else
				{
					Display_msg(response.message,response.status);
					$("#add_faq_modal").modal("hide");
				}
			}
		});
	}
}
</script>
