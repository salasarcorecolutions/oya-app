<!--CONTENT CONTAINER-->
<!--===================================================-->
<div id="content-container">
	<!--Page Title-->
	<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
	<div id="page-title">
		<h1 class="page-header text-overflow">Country List</h1>
	</div>
	<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
	<!--End page title-->


	<!--Breadcrumb-->
	<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
	<ol class="breadcrumb">
		<li><a href="<?php echo base_url('admin/dashboard');?>">Home</a></li>
		<li class="active">Country List</li>
	</ol>
	<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
	<!--End breadcrumb-->
	<div id="page-content">	
		<div class="panel">
			<div class="panel-heading">
				<h3 class="panel-title">Country List</h3>
			</div>
			<div class="panel-nav"> 
                <a style=" float: left;" class="btn btn-info pull-left white" onclick="addCountry()">ADD</a>
			</div>
			<div class="panel-body">
				<form name="frm" method="post" id="location_form">
					<div class="row filter">
						
						<div class="col-sm-2">
							<input class="form-control searchInput" placeholder="Country Name" type="text" name="search_country_name" id="search_country_name" value="" />
						</div>
						 
						
						<div class="col-sm-2 pull-right">
							<a style=" float: left;" class="btn btn-default pull-right" onclick="refresh_datatable();">Refresh </a>
							<a style=" float: left;" class="btn btn-info pull-right" onclick="clearSearchFilters();">Clear Search </a>
						</div>
					</div>
					<div class="table-responsive">
						<table callfunction="<?php echo base_url('admin/master/ajax_table_data_country')?>" class="table table-striped table-bordered table-hover no-footer dtr-inline dynamicTable">
							<thead>
								<tr>
									<th data-bSortable="false">SR NO</th>
									<th>COUNTRY CODE</th>
									<th>COUNTRY NAME</th> 
									<th data-bSortable="false">STATUS</th> 
									<th data-bSortable="false">ACTION</th>
								</tr>
							</thead>
							<tbody>
							</tbody>
						</table>
					</div>	
				</form>
			</div>
		</div>
	</div>
</div>

<!-- Country Modal -->
<div id="add_edit_country" class="modal fade" role="dialog">
	<div class="modal-dialog">
    <!-- Modal content-->
    	<div class="modal-content">
      		<div class="modal-header">
        		<button onclick="clearForm()" type="button" class="close" data-dismiss="modal">&times;</button>
        		<h4 class="modal-title">ADD/EDIT COUNTRY</h4>
      		</div>
      		<div class="modal-body">
        		<form id="country_form">
					<div>
						<input id="country_id" name="id" type="hidden" />
						<label for="country_code">Country Code : </label>
						<input placeholder="Enter Country Code" id="country_code" type="text" name="country_code" class="form-control" />
					</div>
					<div>
						<label for="country_name">Country Name : </label>
						<input placeholder="Enter Country Name" id="country_name" type="text" name="country_name" class="form-control" />
					</div>
					<div>
						<label for="country_isd">ISD Code : </label>
						<input placeholder="Enter ISD Code" id="country_isd" type="text" name="country_isd" class="form-control" />
					</div>
					<div>
						<label>Active : </label>
						<select class="form-control" name="active" id="active">
							<option value="1">Yes</option>
							<option value="0">No</option>
						</select>
					</div>
				</form>
      		</div>
      		<div class="modal-footer">
			  	<button type="button" onclick="addEditCountry()" class="btn btn-default">Add</button>	
				<button type="button" class="btn btn-default" onclick="clearForm()" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>	

<!--===================================================-->
<!--END CONTENT CONTAINER-->

<script>

	function editCountry(countryid)
	{
		$("#country_code").val($("#edit_country_"+countryid).attr('country_code'));
		$("#country_name").val($("#edit_country_"+countryid).attr('county_name'));
		$("#country_isd").val($("#edit_country_"+countryid).attr('country_isd')); 
		$("#country_id").val(countryid);
		$("#active").val($("#edit_country_"+countryid).attr('status'));
		$("#add_edit_country").modal('show');
	}

	function change_action_status(id,status){
		$.ajax({
			url :'<?php echo base_url('admin/master/change_action_status')?>',
			data:{
				'id':id,
				'status':status
			},
			type:'POST',
			dataType:'json',
			success:function(response){
				Display_msg(response.msg,response.status);
			}
		});
	}
	
	function addCountry()
	{
		$("#add_edit_country").modal('show');
	}

	$("#country_form").validate({
		rules: {
			country_code: {required: true},
			country_name: {required: true},
		},
		messages: {
			country_code: {required:"Please Enter Company Code"},
			country_name: {required:"Please Enter Company Name"},
		},
		submitHandler: function(form){
			$(form).ajaxSubmit({
				url:"<?php echo base_url('admin/master/add_edit_country_data/');?>",
				type: 'post',
				dataType:'json',
				success: function (response){
					Display_msg(response.message,response.status);
					setTimeout(function(){
						window.location.reload(1);
					}, 3000);
				},
				error: function(){
					Display_msg('Problem in adding/updating country please try again','failed');
				}
			});
		}
	});

	function addEditCountry()
	{
		$("#country_form").submit();
	}

	function clearForm()
	{
		$("#country_code").val('');
		$("#country_name").val('');
		$("#country_isd").val(''); 
		$("#country_id").val('');
		$("#active").val('1');
	}

</script>
