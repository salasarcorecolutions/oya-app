<!--CONTENT CONTAINER-->
<!--===================================================-->
<div id="content-container">
	<!--Page Title-->
	<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
	<div id="page-title">
		<h1 class="page-header text-overflow">State List</h1>
	</div>
	<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
	<!--End page title-->


	<!--Breadcrumb-->
	<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
	<ol class="breadcrumb">
		<li><a href="<?php echo base_url('admin/dashboard');?>">Home</a></li>
		<li class="active">State List</li>
	</ol>
	<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
	<!--End breadcrumb-->
	<div id="page-content">	
		<div class="panel">
			<div class="panel-heading">
				<h3 class="panel-title">State List</h3>
			</div>
			<div class="panel-nav"> 
                <a style=" float: left;" class="btn btn-info pull-left white" onclick="addState()">ADD</a>  
			</div>
			<div class="panel-body">
				<form name="frm" method="post" id="location_form">
					<div class="row filter">
						
						<div class="col-sm-2">
							<input class="form-control searchInput" placeholder="State Name" type="text" name="search_state_name" id="search_state_name" value="" />
                        </div>
                        <div class="col-sm-2">
                            <input class="form-control searchInput" placeholder="Country Name" type="text" name="search_country_name" id="search_country_name" value="" />
						</div>
						
						<div class="col-sm-2 pull-right">
							<a style=" float: left;" class="btn btn-default pull-right" onclick="refresh_datatable();">Refresh </a>
							<a style=" float: left;" class="btn btn-info pull-right" onclick="clearSearchFilters();">Clear Search </a>
						</div>
					</div>
					<div class="table-responsive">
						<table callfunction="<?php echo base_url('admin/master/ajax_table_data_state')?>" class="table table-striped table-bordered table-hover no-footer dtr-inline dynamicTable">
							<thead>
								<tr>
									<th data-bSortable="false">SR NO</th>
									<th>COUNTRY NAME</th>
									<th>STATE NAME</th>
									<th data-bSortable="false">ACTIVE</th>
									<th data-bSortable="false">ACTION</th>
								</tr>
							</thead>
							<tbody>
							</tbody>
						</table>
					</div>	
				</form>
            </div>	
		</div>
	</div>	
</div>

<!-- State Modal -->
<div id="add_edit_state" class="modal fade" role="dialog">
	<div class="modal-dialog">
    <!-- Modal content-->
    	<div class="modal-content">
      		<div class="modal-header">
        		<button onclick="clearForm()" type="button" class="close" data-dismiss="modal">&times;</button>
        		<h4 class="modal-title">ADD/EDIT STATE</h4>
      		</div>
      		<div class="modal-body">
        		<form id="state_form">
					<div>
						<input id="state_id" name="state_id" type="hidden" />
						<label for="country_name">Country Name : </label>
						<select class="form-control" id="country_namee" name="country_name">
							<option></option>
							<?php if ( ! empty($countries)){ 
								foreach ($countries as $country) { ?>
									<option value="<?php echo $country['id'];?>"><?php echo $country['name'];?></option>
								<?php }
							} ?>
						</select>
					</div>
					<div>
						<label for="state_name">State Name : </label>
						<input placeholder="Enter State Name" id="state_name" type="text" name="state_name" class="form-control" />
					</div>
					<div>
						<label>Active : </label>
						<select class="form-control" name="active" id="active">
							<option value="1">Yes</option>
							<option value="0">No</option>
						</select>
					</div>
				</form>
      		</div>
      		<div class="modal-footer">
			  	<button type="button" onclick="addEditState()" class="btn btn-default">Add</button>	
				<button type="button" class="btn btn-default" onclick="clearForm()" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>	


<!--===================================================-->
<!--END CONTENT CONTAINER-->

<script>

	$(document).ready(function(){
		$("#country_name").select2({
			placeholder: "Please select country",
			dropdownParent: $("#add_edit_state")
		});
	})

     

    function change_state_status(id,status){
		$.ajax({
			url :'<?php echo base_url('admin/master/change_state_status')?>',
			data:{'id':id,'status':status},
			type:'POST',
			dataType:'json',
			success:function(response){
				Display_msg(response.msg,response.status);
			}
		});
	}

	function editState(stateId)
	{
		$("#country_namee").val($("#edit_state_"+stateId).attr('country_id'));   
		$("#state_name").val($("#edit_state_"+stateId).attr('state_name')); 
		$("#state_id").val(stateId);
		$("#active").val($("#edit_state_"+stateId).attr('status'));
		$("#add_edit_state").modal('show');
	}
	
	function addState()
	{
		$("#add_edit_state").modal('show');
	}

	$("#state_form").validate({
		rules: {
			country_name: {required: true},
			state_name: {required: true},
		},
		messages: {
			country_name: {required:"Please Select Country"},
			state_name: {required:"Please Enter State Name"},
		},
		submitHandler: function(form){
			$(form).ajaxSubmit({
				url:"<?php echo base_url('admin/master/add_edit_state_data/');?>",
				type: 'post',
				dataType:'json',
				success: function (response){
					Display_msg(response.message,response.status);
					setTimeout(function(){
						window.location.reload(1);
					}, 3000);
				},
				error: function(){
					Display_msg('Problem in adding/updating state please try again','failed');
				}
			});
		}
	});

	function addEditState()
	{
		$("#state_form").submit();
	}

	function clearForm()
	{
		$("#state_id").val('');
		$("#country_name").select2('val','');
		$("#state_name").val('');
		$("#active").val('1');
	}


	function deleteState(id){
		$.ajax({
			url :'<?php echo base_url('admin/master/delete_location')?>',
			data:{
				'id':id,
				'table':'state_master',
				'loc_type':'state'
			},
			type:'POST',
			dataType:'json',
			success:function(response){
				Display_msg(response.msg,response.status);
				setTimeout(function(){
					refresh_datatable();
				}, 3000);
			},
			error: function(){
				Display_msg('Problem in deleting City please try again','failed');
			}
		});
	}

 </script>
 