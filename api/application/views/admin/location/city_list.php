<!--CONTENT CONTAINER-->
<!--===================================================-->
<div id="content-container">
	<!--Page Title-->
	<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
	<div id="page-title">
		<h1 class="page-header text-overflow">City List</h1>
	</div>
	<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
	<!--End page title-->


	<!--Breadcrumb-->
	<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
	<ol class="breadcrumb">
		<li><a href="<?php echo base_url('admin/dashboard');?>">Home</a></li>
		<li class="active">City List</li>
	</ol>
	<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
	<!--End breadcrumb-->
	<div id="page-content">	
		<div class="panel">
			<div class="panel-heading">
				<h3 class="panel-title">City List</h3>
			</div>
			<div class="panel-nav"> 
				<a style=" float: left;" class="btn btn-info pull-left white" onclick="addCity()">ADD</a> 
            </div>
			<div class="panel-body">
				<form name="frm" method="post" id="location_form">
					<div class="row filter">
						
						<div class="col-sm-2">
							<input class="form-control searchInput" placeholder="State Name" type="text" name="search_state_name" id="search_state_name" value="" />
                        </div>
                        <div class="col-sm-2">
                            <input class="form-control searchInput" placeholder="City Name" type="text" name="search_city_name" id="search_city_name" value="" />
						</div>
						
						<div class="col-sm-2 pull-right">
							<a style=" float: left;" class="btn btn-default pull-right" onclick="refresh_datatable();">Refresh </a>
							<a style=" float: left;" class="btn btn-info pull-right" onclick="clearSearchFilters();">Clear Search </a>
						</div>
					</div>
					<div class="table-responsive">
						<table callfunction="<?php echo base_url('admin/master/ajax_table_data_city')?>" class="table table-striped table-bordered table-hover no-footer dtr-inline dynamicTable">
							<thead>
								<tr>
									<th data-bSortable="false">SR NO</th>
									<th>STATE NAME</th>
									<th>CITY NAME</th>
									<th data-bSortable="false">ACTIVE</th>
									<th data-bSortable="false">ACTION</th>
								</tr>
							</thead>
							<tbody>
							</tbody>
						</table>
					</div>	
				</form>
                <input type="hidden" id="rad_val" name="editId" >
			</div>	
		</div>
	</div>	
</div>


<!-- City Modal -->
<div id="add_edit_city" class="modal fade" role="dialog">
	<div class="modal-dialog">
    <!-- Modal content-->
    	<div class="modal-content">
      		<div class="modal-header">
        		<button onclick="clearForm()" type="button" class="close" data-dismiss="modal">&times;</button>
        		<h4 class="modal-title">ADD/EDIT CITY</h4>
      		</div>
      		<div class="modal-body">
        		<form id="city_form">
					<div>
						<input id="city_id" name="city_id" type="hidden" />
						<label for="state_name">State Name : </label>
						<select class="form-control" id="state_namee" name="state_name">
							<option></option>
							<?php if ( ! empty($states)){ 
								foreach ($states as $state) { ?>
									<option value="<?php echo $state['id'];?>"><?php echo $state['state'];?></option>
								<?php }
							} ?>
						</select>
					</div>  
					 
					<div>
						<label for="city_name">City Name : </label>
						<input type="text" name="city_name" placeholder="Enter City Name" id="city_name" class="form-control" >
					</div>
					<div>
						<label>Active : </label>
						<select class="form-control" name="active" id="active">
							<option value="1">Yes</option>
							<option value="0">No</option>
						</select>
					</div> 
				</form>
      		</div>
      		<div class="modal-footer">
			  	<button type="button" onclick="addEditCity()" class="btn btn-default">Add</button>	
				<button type="button" class="btn btn-default" onclick="clearForm()" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>	



<!--===================================================-->
<!--END CONTENT CONTAINER-->
 <script>

	function editCity(cityid)
	{
		$("#add_edit_city").modal('show');
		$("#state_namee").val($("#edit_city_"+cityid).attr('state_id')); 
		$("#city_name").val($("#edit_city_"+cityid).attr('city_name'));
		$("#city_id").val(cityid);
		$("#active").val($("#edit_city_"+cityid).attr('status')); 
	}
  
	function addCity()
	{
		$("#add_edit_city").modal('show');
	}

	$("#city_form").validate({
		rules: {
			state_name: {required: true},
			city_name: {required: true},
		},
		messages: {
			state_name: {required:"Please Enter State Name"},
			city_name: {required:"Please Enter City Name"},
		},
		submitHandler: function(form){
			$(form).ajaxSubmit({
				url:"<?php echo base_url('admin/master/add_edit_city_data/');?>",
				type: 'post',
				dataType:'json',
				success: function (response){
					Display_msg(response.message,response.status);
					setTimeout(function(){
						window.location.reload(1);
					}, 3000);
				},
				error: function(){
					Display_msg('Problem in adding/updating country please try again','failed');
				}
			});
		}
	}); 

	function clearForm()
	{
		$("#state_namee").val('');
		$("#city_name").val('');
		$("#city_id").val('');
		$("#active").val('1'); 
	}
 
	function change_city_status(id,status){
		$.ajax({
			url :'<?php echo base_url('admin/master/change_city_status')?>',
			data:{'id':id,'status':status},
			type:'POST',
			dataType:'json',
			success:function(response)
			{
				if(response.status == 'success')
				{
					Display_msg(response.msg,response.status);
					refresh_datatable();
				}
				else
				{
					Display_msg(response.msg,response.status);
					return false;
				} 		
			}		 
		});
	}
 

	function addEditCity(){
		$("#city_form").submit();
	}

	function deleteCity(stateid) {
		$.ajax({
			url :'<?php echo base_url('admin/master/delete_location')?>',
			data:{
				'id':stateid,
				'table':'city_master',
				'loc_type':'city'
			},
			type:'POST',
			dataType:'json',
			success:function(response){
				Display_msg(response.msg,response.status);
				setTimeout(function(){
					refresh_datatable();
				}, 3000);
			},
			error: function(){
				Display_msg('Problem in deleting City please try again','failed');
			}
		});
	} 
	 
 </script>