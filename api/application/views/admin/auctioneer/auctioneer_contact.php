
<!--CONTENT CONTAINER-->
<!--===================================================-->
<div id="content-container">
	<!--Page Title-->
	<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
	<div id="page-title">
		<h1 class="page-header text-overflow">Auctioneer Contact List</h1>
	</div>
	<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
	<!--End page title-->


	<!--Breadcrumb-->
	<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
	<ol class="breadcrumb">
		<li><a href="<?php echo base_url('admin/dashboard');?>">Home</a></li>
		<li><a href="<?php echo base_url('admin/vendor/index');?>">Auctioneer List</a></li>
		<li class="active">Auctioneer Contact List</li>
	</ol>
	<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
	<!--End breadcrumb-->
	<input type="hidden" id="t11" name="auctionId">
	<div id="page-content">	
		<div class="panel">
			<div class="panel-heading">
				<h3 class="panel-title">Auctioneer Contact List</h3>
			</div>
			
			<div class="panel-body">					
				<div class="table-responsive">
					<table class="table table-striped table-bordered table-hover no-footer dtr-inline staticTable">
						<thead>
							<tr>
								<th>SR</th>
								<th>Contact Person Name</th>
								<th>Contact Number</th>
								<th>Email Id</th>
								<th>User Name</th>
								<th>User Type</th>
								<th>Date Added</th>
								<th>Activation Date</th>
								<th>ACTION</th>
							</tr>
						</thead>
						<tbody>
							<?php 
							if( ! empty($vendor_contact))
							{
								$cnt = 1;
								foreach($vendor_contact as $single_contact)
								{
									?>
									<tr>
										<td><?php echo $cnt++; ?></td>
										<td><?php echo $single_contact['c_name']; ?></td>
										<td><?php echo $single_contact['c_contacts']; ?></td>
										<td><?php echo $single_contact['c_email']; ?></td>
										<td><?php echo $single_contact['uname']; ?></td>
										<td><?php echo ($single_contact['user_type'] == 'A') ? 'Admin' : 'User' ; ?></td>
										<td><?php echo $single_contact['date_added']; ?></td>
										<td>
											<?php 
											if(strtotime($single_contact['active_date_to']) > strtotime(date('Y-m-d')))
											{
												echo '<span style="color:green;">'.date('d-m-Y',strtotime($single_contact['active_date_from']))." <strong>TO</strong> ".date('d-m-Y',strtotime($single_contact['active_date_to'])).'</span>';
											}
											else
											{
												echo '<span style="color:red;">'.date('d-m-Y',strtotime($single_contact['active_date_from']))." <strong>TO</strong> ".date('d-m-Y',strtotime($single_contact['active_date_to'])).'</span>';
											}
											?>
										</td>
										<td>
											<a target='_blank' href='<?php echo base_url('admin/auctioneer/set_permission/'.$vendor_id.'/'.$single_contact['id']) ?>'>Set Permission</a>
										</td>
									</tr>
									<?php
								}
							}
							?>
						</tbody>
						
					</table>
				</div>					
			</div>	
		</div>
	</div>	
</div>
<!--===================================================-->
<!--END CONTENT CONTAINER-->