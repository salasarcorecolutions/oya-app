
<!--CONTENT CONTAINER-->
<!--===================================================-->
<div id="content-container">
	<!--Page Title-->
	<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
	<div id="page-title">
		<h1 class="page-header text-overflow">Auctioneer List</h1>
	</div>
	<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
	<!--End page title-->
	<!--Breadcrumb-->
	<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
	<ol class="breadcrumb">
		<li><a href="<?php echo base_url('admin/dashboard');?>">Home</a></li>
		<li class="active">Auctioneer List</li>
	</ol>
	<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
	<!--End breadcrumb-->
	<input type="hidden" id="t11" name="auctionId">
	<div id="page-content">	
		<div class="panel">
			<div class="panel-heading">
				<h3 class="panel-title">Auctioneer List</h3>
			</div>
			
			<div class="panel-body">
				<form name="frm">
					<div class="row filter">
						
						<div class="col-sm-2">
							<input class="form-control searchInput" placeholder="Auctioneer Name" type="text" name="search_Auctioneer_name" id="search_auctioneer_name" value="" />
						</div>
						<div class="col-sm-2">
							<input class="form-control searchInput" placeholder="Country" type="text" name="search_auctioneer_country" id="search_auctioneer_country" value="" />
						</div>
						<div class="col-sm-2">
							<input class="form-control searchInput" placeholder="State" type="text" name="search_auctioneer_state" id="search_auctioneer_state" value="" />
						</div>
						<div class="col-sm-2">
							<input class="form-control searchInput" placeholder="City" type="text" name="search_auctioneer_city" id="search_auctioneer_city" value="" />
						</div>
						<div class="col-sm-2 pull-right">
							<a style=" float: left;" class="btn btn-default pull-right" onclick="refresh_datatable();">Refresh </a>
							<a style=" float: left;" class="btn btn-info pull-right" onclick="clearSearchFilters();">Clear Search </a>
						</div>
					</div>
					<div class="table-responsive">
						<table callfunction="<?php echo base_url('admin/auctioneer/fetch');?>" class="table table-striped table-bordered table-hover no-footer dtr-inline dynamicTable">
							<thead>
								<tr>
									<th data-bSortable="false">SR</th>
									<th>AUCTIONER NAME</th>
									<th data-bSortable="false">ADDRESS</th>
									<th>COUNTRY</th>
									<th>STATE</th>
									<th>CITY</th>
									<th>REGISTRATION DATE</th>
									<th data-bSortable="false">ACTION</th>
								</tr>
							</thead>
							<tbody>
							</tbody>
						</table>
					</div>
				</form>
			</div>	
		</div>
	</div>	
</div>
<!--===================================================-->
<!--END CONTENT CONTAINER-->