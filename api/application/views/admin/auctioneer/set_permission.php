<style>
.table>tbody>tr>td, .table>tbody>tr>th, .table>tfoot>tr>td, .table>tfoot>tr>th, .table>thead>tr>td, .table>thead>tr>th
{
	padding: 3px !important;
}
</style>
<!--CONTENT CONTAINER-->
<!--===================================================-->
<div id="content-container">
	<div id="page-title">
		<h1 class="page-header text-overflow">Auctioneer Page Permission</h1>
	</div>
	<ol class="breadcrumb">
		<li><a href="<?php echo base_url('admin/dashboard');?>">Home</a></li>
		<li><a href="<?php echo base_url('admin/auctioneer/index');?>">Auctioneer List</a></li>
		<li class="active">Auctioneer Page Permission</li>
	</ol>
	<div id="page-content">
		<div class="panel">
			<div class="panel-heading">
				<h3 class="panel-title">Page Permission</h3>
			</div>
			
			<form id="form_permission">
				<input type="hidden" value="<?php echo $vendor_id; ?>" name="vendor_id" id="vendor_id" />
				<input type="hidden" value="<?php echo $vendor_contact_id; ?>" name="vendor_contact_id" id="vendor_contact_id" />
				<div class="panel-body">
					<div class="row">
						<div class="col-lg-12">
							<table class="table table-striped table-bordered table-hover">
								<thead>
									<tr>
										<th>Auctioneer</th>
										<th>Person Name</th>
										<th>Contact No</th>
										<th>Email Id</th>
										<th>Username</th>
										<th>User Type</th>
										<th>Activation Date</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td><?php echo $vendor_contact_details['vendor_name']; ?></td>
										<td><?php echo $vendor_contact_details['c_name']; ?></td>
										<td><?php echo $vendor_contact_details['c_contacts']; ?></td>
										<td><?php echo $vendor_contact_details['c_email']; ?></td>
										<td><?php echo $vendor_contact_details['uname']; ?></td>
										<td><?php echo ($vendor_contact_details['user_type'] == 'A') ? 'Admin' : 'User' ; ?></td>
										<td>
											<input name="start_date" id="start_date" type="text" value="<?php echo $vendor_contact_details['active_date_from']; ?>">
											<input name="end_date" id="end_date" type="text" value="<?php echo $vendor_contact_details['active_date_to']; ?>">
										</td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
					<div class="row">
						<div class="col-lg-12">
							<div class="col-lg-1">
								<input onclick="check_all_group('all');" type="checkbox" name="all" id="all" value="Y" >
							</div>
							<div class="col-lg-10">
								<big><b>Check All</b></big>
							</div>
							<div class="col-lg-1">
								<input type="button" value="Back" name="back" class="btn btn-warning" onclick="javascript:location.href='<?php echo base_url('admin/auctioneer/index');?>'" />
							</div>
						</div>
					</div>
				<br />
				<br />
			<?php
				foreach($permission as $group_name => $single_group)
				{ ?>
					<div class="col-lg-3">
						<div class="table-responsive">
							<table class="table table-bordered table-striped">
								<thead>
									<tr>
										<th>
											<input onclick="check_all_group('<?php echo $group_name?>');" type="checkbox" name="<?php echo $group_name?>" id="<?php echo $group_name?>" value="Y" >
										</th>
										<th><?php echo ucwords(str_replace("_"," ",$group_name)); ?></th>
									</tr>
								</thead>
								<tbody>
								<?php 									
								foreach($single_group as $permission_id => $permission_name)
								{ ?>
									<tr>
										<td>
											<input class="<?php echo $group_name?> all" type="checkbox" name="page_id[]" id="<?php echo $permission_id?>" value="<?php echo $permission_id?>" <?php if(in_array($permission_id,$page_ids)) {echo "checked"; }?>>
										</td>
										<td><?php echo ucwords(str_replace("_"," ",$permission_name)); ?></td>
									</tr> <?php
								}
								?>
								</tbody>
							</table>
						</div>
					</div>
				<?php 
				}
				?>
				</div>
				<div class="row">
					<div class="form-group" align="center">
						<input class="btn btn-primary" type="submit" value='UPDATE' name="B1" >
					</div>
				</div>
			</form>
		</div>
	</div>
</div>
<script>
	function check_all_group(class_name)
	{
		if ($("#"+class_name).is(':checked')){
			$("."+class_name).prop('checked', true);
		} else {
			$("."+class_name).prop('checked', false);
		}
	}
	
	$(document).ready(function() {
		var vRules = {};
		var vMessages = {};

		$("#start_date").datepicker({
			format: 'yyyy-mm-dd'
		}).on('changeDate', function(e) {
			$.ajax({
				url: '<?php echo base_url("admin/vendor/change_from_date");?>',
				data:{
					'vendor_contact_id': "<?php echo $this->uri->segment(5) ?>",
					'from_date': $("#start_date").val()
				},
				type: 'post',
				dataType: 'json',
				success: function (result) {
					if (result.status == 'success'){
						Display_msg(result.message,result.status);
					} else {
						Display_msg(result.message,result.status);
					}
				}
       		});
		});

		$("#end_date").datepicker({
			format: 'yyyy-mm-dd'
		}).on('changeDate', function(e) {
			$.ajax({
				url: '<?php echo base_url("admin/vendor/change_to_date")  ;?>',
				data:{
					'vendor_contact_id': "<?php echo $this->uri->segment(5) ?>",
					'to_date': $("#end_date").val()
				},
				type: 'post',
				dataType: 'json',
				success: function (result){
					if (result.status == 'success'){
						Display_msg(result.message,result.status);
					} else {
						Display_msg(result.message,result.status);
					}
            	}
        	});
		});

		$("#form_permission").validate({
			rules: vRules,
			messages: vMessages,
			submitHandler: function(form){
				$(form).ajaxSubmit({
					url:"<?php echo base_url('admin/auctioneer/set_permission/'.$vendor_id.'/'.$vendor_contact_id);?>",
					type: 'post',
					dataType:'json',
					cache: false,
					clearForm: false,
					success: function (response){
						if (response.status == "success"){
							Display_msg(response.message,response.status);
							setTimeout(function(){
								window.location.href="<?php echo base_url('admin/auctioneer/index');?>";
							}, 3000);
						} else {
							Display_msg(response.message,response.status);
							return false;
						}
					}
				});
			}
		});
	});
</script>