<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Module Head</title>
	<style>
	.alert-error{
		background-color:#F2DEDF !important;
	}
	</style>
</head>

<body>
	<div id="container" class="effect mainnav-lg">
		<div class="boxed">
			<!--CONTENT CONTAINER-->
			<!--===================================================-->
			<div id="content-container">
				<!--Page Title-->
				<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
				<div id="page-title"><h1 class="page-header text-overflow">Module Head</h1></div>
				<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
				<!--End page title-->
				<!--Breadcrumb-->
				<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
				<ol class="breadcrumb">
					<li><a href="<?php echo base_url()?>dashboard">Home</a></li>
					<li class="active">Permission</li>
					<li class="active">Module Head</li>
				</ol>
				<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
				<!--End breadcrumb-->
				<!--Page content-->
				<!--===================================================-->
				<div id="page-content">
					<div class="row">
						<div class="col-lg-12">
							<!--Invoice table-->
							<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
							<div class="panel">
								<div class="panel-heading">
									<div class="panel-control">
										<?php
										if($this->common_model->check_permission('ModuleHeadAdd') || TRUE)
										{
											?>
												<button class="btn btn-purple btn-labeled fa fa-plus" id="buttonAdd" data-toggle="modal" data-target="#module_head_add_edit_model">Add</button>
											<?php
										}
										?>
										
									</div>

									<h3 class="panel-title">Module Head</h3>
								</div>
								<div class="panel-body">
									<!-- for filter start -->
									<div class="row text-sm wrapper">
										<div class="col-sm-2 filter">
											<label class="control-label">Module Head: </label>
											<div>
												<input type="text" class="form-control searchInput" placeholder="Module Head">
											</div>
										</div>
										<div class="col-sm-2">
											<label style="width:100%;" class="control-label">&nbsp;</label>
											<a style=" float: right;margin-right: 5%;" class="btn btn-primary" onclick="clearSearchFilters();">Clear Search </a>
										</div>
									</div>
									<br><br>
									<!-- for filter end -->
									<div class="table-responsive">
                                    <table class="table table-bordered table-hover table-striped">
											<thead>
                                                <tr>
                                                    <th>Sr. No.</th>
                                                    <th>Module Head</th>
                                                    <th>Actions</th>
                                                </tr>
                                            </thead>
										<tbody>
                                        <?php $i = 1; foreach ($module_head_listing as $mhl){
                                        $actions = "";
                                        if ($this->common_model->check_permission('ModuleHeadEdit') || TRUE)
                                        {
                                            $actions = "<a data-container='body'  data-id='".$mhl["head_id"]."' head_name='".$mhl['head_name']."' data-original-title='Edit' href='javascript:void(0);' data-toggle='tooltip' class='btnEdit btn btn-xs btn-default add-tooltip'><i class='fa fa-pencil'></i></a>";
                                        }
                                        if ($this->common_model->check_permission('ModuleHeadDelete') || TRUE)
                                        {
                                            $actions .= "<a data-container='body'  data-id='".$mhl["head_id"]."' head_name='".$mhl['head_name']."' data-original-title='Delete' href='javascript:void(0);' data-toggle='tooltip' class='btnDelete btn btn-xs btn-danger add-tooltip'><i class='fa fa-times'></i></a>";
                                        }
                                        ?>
                                            <tr>
                                                <th><?php echo $i++; ?></th>
                                                <th><?php echo $mhl['head_name']; ?></th>
                                                <th><?php echo $actions; ?></th>
                                            </tr>
                                        <?php } ?>
										</tbody>
									</table>
								</div>
							</div>
						</div>
						<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
						<!--End Invoice table-->
					</div>
				</div>
			</div>
			<!--===================================================-->
			<!--End page content-->
		</div>
		<!--===================================================-->
		<!--END CONTENT CONTAINER-->
		<!--MAIN NAVIGATION-->
		<!--===================================================-->
	</div>
	<!--END BOXED DIV-->
</div>

<?php   /* -----------------------------Start Module Head Add Edit Model ------------------- */   ?>

<div class="modal" id="module_head_add_edit_model" role="dialog" tabindex="-1" aria-labelledby="demo-default-modal" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<!--Modal header-->
			<div class="modal-header">
				<button data-dismiss="modal" class="close" type="button">
					<span aria-hidden="true">&times;</span>
				</button>
				<h4 class="modal-title">Module Head</h4>
			</div>
			<!--Modal body-->
			<div class="modal-body">
				<div class="panel">
					<div id="opMessage"></div>
					<!--Horizontal Form-->
					<!--===================================================-->
					<form class = "form-horizontal" id = "frm_module_head" name = "frm_module_head">
						<div class="panel-body">
							<div class="form-group">
								<label for="sub_head_name" class="col-sm-5 control-label">Module Head Name</label>
								<div class="col-sm-7">
									<input type="text" class="form-control" id="head_name" name="head_name" placeholder="Module Head Name">
								</div>
							</div>
							<input type="hidden" name="data_op" id="data_op" />
							<input type="hidden" id="head_id" name="head_id" />
						</div>
					</form>
				<!--===================================================-->
				<!--End Horizontal Form-->
				</div>
			</div>
			<!--Modal footer-->
			<div class="modal-footer">
				<button data-dismiss="modal" class="btn btn-default" type="button">Close</button>
				<button class="btn btn-primary" id="btnSave">Save changes</button>
			</div>
		</div>
	</div>
</div>
<?php   /* -----------------------------End Module Head Add Edit Model ------------------- */   ?>


<!--JAVASCRIPT-->
<!--=================================================-->
<script>
    var data_op="add";
    var object_id='';
	$(".btnEdit").click(function(e){
		e.preventDefault();
		document.getElementById("frm_module_head").reset();
		$('#data_op').val("edit");
		$('#head_id').val($(this).attr("data-id"));
		$('#head_name').val($(this).attr("head_name"));
		object_id = $(this).attr("data-id");
		data_op = 'edit';
		$('#module_head_add_edit_model').modal('show');
	});

	$(".btnDelete").click(function(e){
		e.preventDefault();
		data_op = "del";
		var ans = confirm("Are you sure to delete "+$(this).attr("head_name")+"?");
		if(ans)
		{
			$.ajax({
				url: "<?php echo base_url();?>admin/permission/head_op",
				type: "POST",
				dataType:'json',
				cache: false,
				data:{
					"head_id":$(this).attr("data-id"),
					"data_op":data_op
				},
				success: function(response)
				{
					if(response.status=="success")
					{
						$.niftyNoty({
							type: 'success',
							container : 'floating',
							html : '<h4 class="alert-title">Success</h4><p class="alert-message">'+response.msg+'</p>',
							closeBtn : false,
							timer: 4000
						});
						location.reload();
					}
					else
					{
						$.niftyNoty({
							type: 'error',
							container : 'floating',
							html : '<h4 class="alert-title">Fail</h4><p class="alert-message">'+response.msg+'</p>',
							closeBtn : false,
							timer: 4000
						});
					}
				}
			});
		}
	});


    $("#buttonAdd").click(function(){
        document.getElementById("frm_module_head").reset();
        $('#data_op').val("add");
        $('#head_id').val("");
        data_op='add';
    });

    $("#btnSave").click(function(e){
        e.preventDefault();
        var form = $("#frm_module_head");
        form.validate({
            ignore:[],
            rules: {
                head_name: "required"
            },
            messages: {
                head_name: "<b class='text-danger'>Please Enter Module Head Name</b>"
            }
        });
        if (form.valid())
        {
            $('#frm_module_head').ajaxSubmit({
                url:"<?php echo base_url();?>admin/permission/head_op",
                type: 'post',
                dataType:'json',
                cache: false,
                clearForm: false,
                success: function (response)
                {
                    if (response.status=="success"){
                        $('#module_head_add_edit_model').modal('hide');
                        $.niftyNoty({
                            type: 'success',
                            container : 'floating',
                            html : '<h4 class="alert-title">Success</h4><p class="alert-message">'+response.msg+'</p>',
                            closeBtn : false,
                            timer: 4000
                        });
						location.reload();
                    } else {
                        $('#opMessage').show();
                        $('#opMessage').html('<div class="alert alert-danger fade in"><button data-dismiss="alert" class="close"><span>×</span></button><strong>Oh!</strong> '+response.msg+'</div>');
                        $('#opMessage').delay(4000).fadeOut();
                    }
                }
            });
        }
    });

    $(window).on('hidden.bs.modal', function() {
        $('#data_op').val("add");
        $('#head_id').val("");
        data_op='add';
    });
</script>
</body>
</html>
