<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Permission Sets</title>
    <style>
	.alert-error{
		background-color:#F2DEDF !important;
	}
	</style>
</head>

<body>
	<div id="container" class="effect mainnav-lg">
		<div class="boxed">
			<!--CONTENT CONTAINER-->
			<!--===================================================-->
			<div id="content-container" class="ignoreCopyPaste">
				<!--Page Title-->
				<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
				<div id="page-title"><h1 class="page-header text-overflow">Permission Sets</h1></div>
				<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
				<!--End page title-->
				<!--Breadcrumb-->
				<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
				<ol class="breadcrumb">
					<li><a href="<?php echo base_url()?>dashboard">Home</a></li>
					<li class="active">Permission Sets</li>
				</ol>
				<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
				<!--End breadcrumb-->
				<!--Page content-->
				<!--===================================================-->
				<div id="page-content">
					<div class="row">
						<div class="col-lg-12">
							<!--Invoice table-->
							<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
							<div class="panel">
								<div class="panel-heading">
									<div class="panel-control">
										<button class="btn btn-purple btn-labeled fa fa-plus btnAdd" >Add</button>
									</div>
									<h3 class="panel-title">Permission Sets</h3>
								</div>
								<div class="panel-body">
									<div class="pad-btm form-inline">
										<div class="row">
											<div class="col-sm-6 table-toolbar-left">
											</div>
											<div class="col-sm-6 table-toolbar-right">
												<div class="form-group">
												<select class="form-control Select-default" id="S_permission_id" name="S_permission_id">
												<option value="">-- All --</option>
												<?php foreach($sub_heads as $items):?>
													<option <?php if($items["module_sub_head_id"]==$sub_head_id) echo "selected='selected';"?>  value="<?php echo $items["module_sub_head_id"];?>" ><?php echo $items["sub_head_name"];?></option>
												 <?php endforeach ?>
												</select>
												</div>
											</div>
										</div>
									</div>
									<div class="table-responsive">
										<table class="table table-bordered table-hover table-striped staticTable" id="cTable">
											<thead>
												<tr>
													<th>Permission Name</th>
													<th>Permission CODE</th>
													<th class="text-right" nowrap>Actions</th>
												</tr>
											</thead>
											<tbody>
											<?php foreach($permission_sets as $items):?>
												<tr id="T<?php echo $items["permission_id"];?>">
													<td><?php echo $items["permission_name"];?></td>
													<td><?php echo $items["permission_code"];?></td>
													<td class="text-right">
														<a data-container="body" 
														permission_id="<?php echo $items["permission_id"];?>" 
														module_sub_head_id="<?php echo $items["module_sub_head_id"];?>" 
														permission_name="<?php echo $items["permission_name"];?>" 
														permission_code="<?php echo $items["permission_code"];?>"   data-original-title="Edit" href="#" data-toggle="tooltip" class="btnEdit btn btn-xs btn-default add-tooltip"><i class="fa fa-pencil"></i></a>
														<a data-container="body"  data-id="<?php echo $items["permission_id"];?>" permission_name="<?php echo $items["permission_name"];?>" data-original-title="Delete" href="#" data-toggle="tooltip" class="btnDelete btn btn-xs btn-danger add-tooltip"><i class="fa fa-times"></i></a>
													</td>
												</tr>
												 <?php endforeach ?>
											</tbody>
										</table>
									</div>
								</div>
							</div>
							<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
							<!--End Invoice table-->
						</div>
					</div>
				</div>
				<!--===================================================-->
				<!--End page content-->
			</div>
			<!--===================================================-->
			<!--END CONTENT CONTAINER-->
			<!--MAIN NAVIGATION-->
			<!--===================================================-->	
		</div>
		<!--END BOXED DIV-->
	</div>
<div class="modal" id="Modal_Project" role="dialog" tabindex="-1" aria-labelledby="demo-default-modal" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <!--Modal header-->
            <div class="modal-header">
                <button data-dismiss="modal" class="close" type="button">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title">Permission Sets</h4>
            </div>
            <!--Modal body-->
            <div class="modal-body">
                <div class="panel">
                    <div id="opMessage"></div>
                        <!--Horizontal Form-->
                        <!--===================================================-->
                        <form class="form-horizontal" id="frm_prj">
                            <div class="panel-body">
                                <div class="form-group">
                                    <label for="sub_head_code" class="col-sm-5 control-label">Sub Head</label>
                                    <div class="col-sm-7">
                                        <select class="form-control Select-default" id="module_sub_head_id" name="module_sub_head_id">
                                            <option value="">-- All --</option>
                                            <?php foreach ($sub_heads as $items):?>
                                                <option value="<?php echo $items["module_sub_head_id"];?>" ><?php echo $items["sub_head_name"];?></option>
                                            <?php endforeach ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="sub_head_code" class="col-sm-5 control-label">Permission Code</label>
                                    <div class="col-sm-7">
                                        <input type="text" class="form-control" id="permission_code" name="permission_code" placeholder="Unique Code">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="sub_head_name" class="col-sm-5 control-label">Permission Name</label>
                                    <div class="col-sm-7">
                                        <textarea class="form-control" id="permission_name" name="permission_name" placeholder="permission Name"></textarea>
                                    </div>
                                </div>
                                <input type="hidden" id="data_op" name="data_op" />
                                <input type="hidden" id="permission_id" name="permission_id" />
                            </div>
                        </form>
                        <!--===================================================-->
                        <!--End Horizontal Form-->
                    </div>
				</div>
				<!--Modal footer-->
				<div class="modal-footer">
					<button data-dismiss="modal" class="btn btn-default" type="button">Close</button>
					<button class="btn btn-primary" id="btnSave">Save changes</button>
				</div>
			</div>
		</div>
	</div>
	<!--JAVASCRIPT-->
	<!--=================================================-->
	<script>
	var data_op="add";
	var object_id='';

	$(".btnEdit").click(function(e){
		e.preventDefault();
		$('#data_op').val("edit");
		$('#permission_id').val($(this).attr("permission_id"));
		$('#module_sub_head_id').val($(this).attr("module_sub_head_id"));
		$('#permission_name').val($(this).attr("permission_name"));
		$('#permission_code').val($(this).attr("permission_code"));
		data_op='edit';
		$('#Modal_Project').modal('show');
	});

	$(".btnAdd").click(function(e){
		e.preventDefault();
		$('#module_sub_head_id').val($('#S_permission_id').val());
		$('#data_op').val("add");
		data_op='add';
		$('#Modal_Project').modal('show');
	});

	$(".btnDelete").click(function(e){
		e.preventDefault();
		data_op = "del";
		var ans = confirm("Are you sure to delete "+$(this).attr("permission_name")+"?");
		if(ans)
		{
			$.ajax({
				url: "<?php echo base_url();?>admin/permission/permission_op",
				type: "POST",
				dataType:'json',
				cache: false,
				data:{
					"permission_id":$(this).attr("data-id"),
					"data_op":data_op
				},
				success: function(response)
				{
					if (response.status=="success"){
						$.niftyNoty({
							type: 'success',
							container : 'floating',
							html : '<h4 class="alert-title">Success</h4><p class="alert-message">'+response.msg+'</p>',
							closeBtn : false,
							timer: 4000
						});
						location.reload();
					} else {
						$.niftyNoty({
							type: 'error',
							container : 'floating',
							html : '<h4 class="alert-title">Fail</h4><p class="alert-message">'+response.msg+'</p>',
							closeBtn : false,
							timer: 4000
						});
					}
				}
			});
		}
	});


	$("#btnSave").click(function(e){
	    e.preventDefault();
        var form = $("#frm_prj");
        form.validate({
			ignore:[],
            rules: {
                module_sub_head_id: "required",
                permission_code: "required"
            },
			messages: {
                module_sub_head_id: "<b class='text-danger'>Please Select Module Sub Head</b>",
                permission_code: "<b class='text-danger'>Permission Code Mandatory And Unique</b>"
            }
		});
		if (form.valid()){
			$('#frm_prj').ajaxSubmit({
				url:"<?php echo base_url();?>admin/permission/permission_op",
				type: 'post',
				dataType:'json',
				cache: false,
				clearForm: false,
				success: function (response)
				{
					if (response.status=="success"){
						$('#Modal_Project').modal('hide');
						$.niftyNoty({
							type: 'success',
							container : 'floating',
							html : '<h4 class="alert-title">Success</h4><p class="alert-message">'+response.msg+'</p>',
							closeBtn : false,
							timer: 4000
						});
						location.reload();
					} else {
						$('#opMessage').show();
						$('#opMessage').html('<div class="alert alert-danger fade in"><button data-dismiss="alert" class="close"><span>�</span></button><strong>Oh!</strong> '+response.msg+'</div>');
						$('#opMessage').delay(4000).fadeOut();
					}
				}
			});
		}
	});

	$(window).on('hidden.bs.modal', function() {
		$('#module_sub_head_id').val($('#S_permission_id').val());
		$('#data_op').val("add");
		data_op='add';
		location.reload();
	});

	$("#S_permission_id").on('change', function() {
		location.href="<?php echo base_url();?>/admin/permission/per_list/"+$('#S_permission_id').val();
	});

	</script>
	</body>
</html>
