<!--CONTENT CONTAINER-->
<!--===================================================-->
<div id="content-container">
	<!--Page Title-->
	<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
	<div id="page-title">
		<h1 class="page-header text-overflow">Bidder Details</h1>
	</div>
	<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
	<!--End page title-->
	<!--Breadcrumb-->
	<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
	<ol class="breadcrumb">
		<li><a href="<?php echo base_url('admin/dashboard');?>">Home</a></li>
		<li class="active">Bidder Details</li>
	</ol>
	<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
	<!--End breadcrumb-->
	
	<div id="page-content">	
		<div class="panel">
			<div class="panel-heading">
				<h3 class="panel-title">Bidder Details</h3>
			</div>
			
			<div class="panel-body">
				<form name="frm">
					<div class="row filter">
						
						<div class="col-sm-2">
							<input class="form-control searchInput" placeholder="Company Name" type="text" name="search_user_name" id="search_user_name" value="" />
						</div>
						<div class="col-sm-2">
							<input class="form-control searchInput" placeholder="Contact Person" type="text" name="search_vendor_name" id="search_vendor_name" value="" />
						</div>
						<div class="col-sm-2">
							<input class="form-control searchInput" placeholder="Contact No" type="text" name="search_contact_no" id="search_contact_no" value="" />
						</div>
						<div class="col-sm-2">
							<input class="form-control searchInput" placeholder="Email Id" type="text" name="search_email" id="search_email" value="" />
						</div>
						<div class="col-sm-2">
							<input class="form-control searchInput addDatePicker" placeholder="Reg Date From" type="text" name="reg_date_frm" id="reg_date_frm" value="" readonly />
						</div>
						<div class="col-sm-2">
							<input class="form-control searchInput addDatePicker" placeholder="Reg Date To" type="text" name="reg_date_to" id="reg_date_to" value="" readonly />
						</div>
						
						
						<div class="col-sm-2 pull-right" style="margin-top:2px;">
							<a style=" float: left;" class="btn btn-default pull-right" onclick="refresh_datatable();">Refresh </a>
							<a style=" float: left;" class="btn btn-info pull-right" onclick="clearSearchFilters();">Clear Search </a>
						</div>
					</div>
					<div class="table-responsive">
						<table callfunction="<?php echo base_url('admin/bidder_details/fetch_bidder_detatils')?>" class="table table-striped table-bordered table-hover no-footer dtr-inline dynamicTable">
							<thead>
								<tr>
									<th data-bSortable="false">SR</th>
									<th>Company Name</th>
									<th>Contact Person</th>
									<th>Registration Date</th>
									<th>Contact No</th>
									<th>Email</th>
									<th>Block</th>
									<th data-bSortable="false">View Details</th>
									
								</tr>
							</thead>
							<tbody>
							</tbody>
							<input type="hidden" id="vendorid" name="vendorid">
						</table>
					</div>	
				</form>
			</div>	
		</div>
	</div>	
</div>
<!--===================================================-->
<!--END CONTENT CONTAINER-->
<script>

function block_bidder(id,status){
	 $.ajax({
		url :'<?php echo base_url('admin/bidder_details/block_bidder');?>',
		data:{'bidder_id':id,'status':status},
		type:'POST',
		dataType:'json',
		success:function(response)
		{
			if(response.status == 'success')
			{
				if(response.status == "success")
				{
					Display_msg(response.msg,response.status);
					refresh_datatable();
				}
				else
				{
					Display_msg(response.msg,response.status);
					return false;
				}
			}		
		}		 
	 });
}
function view_details(user_contact_id){
	window.open('<?php echo base_url()?>admin/bidder_details/show_bidder_details/'+user_contact_id,'width=800,height=400');
}

</script>