<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Administration</title>
	<!--STYLESHEET-->
	<!--=================================================-->
	<!--Open Sans Font [ OPTIONAL ] -->
 	<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700&amp;subset=latin" rel="stylesheet">

	<!--Bootstrap Stylesheet [ REQUIRED ]-->
	<link href="<?php echo base_url('auctioneer_assets/css/bootstrap.min.css'); ?>" rel="stylesheet">

	<!--Nifty Stylesheet [ REQUIRED ]-->
	<link href="<?php echo base_url('auctioneer_assets/css/nifty.min.css'); ?>" rel="stylesheet">
	<!-- <link href="<?php echo base_url('auctioneer_assets/css/nifty-demo.min.css'); ?>" rel="stylesheet"> -->

	<!--Font Awesome [ OPTIONAL ]-->
	<link href="<?php echo base_url('auctioneer_assets/plugins/font-awesome/css/font-awesome.min.css'); ?>" rel="stylesheet">

	<!--Demo [ DEMONSTRATION ]-->
	<!-- <link href="css/demo/nifty-demo.min.css" rel="stylesheet"> -->

	<!--SCRIPT-->
	<!--=================================================-->
	<!--Page Load Progress Bar [ OPTIONAL ]-->
	<link href="<?php echo base_url('auctioneer_assets/plugins/pace/pace.min.css'); ?>" rel="stylesheet">
	<script src="<?php echo base_url('auctioneer_assets/plugins/pace/pace.min.js'); ?>"></script>
	
	<!--Nifty Stylesheet [ REQUIRED ]-->
	<link href="<?php echo base_url('auctioneer_assets/css/style.css'); ?>" rel="stylesheet">

	<style>
	#container.cls-container .cls-brand brand-icon, #container.cls-container .cls-brand .brand-title{
			display: inline-block;
			font-size: 35px;

		}

	</style>

	
</head>

<body>
	<div id="container" class="cls-container">

		<!-- BACKGROUND IMAGE -->
		<!--===================================================-->
		<div id="bg-overlay" class="bg-img img-balloon"></div>
		
		
		<!-- HEADER -->
		<!--===================================================-->
		<div class="cls-header cls-header-lg">
			<div class="cls-brand">
				<a class="box-inline" href="<?php echo base_url(); ?>">
					<!--<img alt="Oya Auction" src="<?php echo base_url('assets/images/logo.jpg'); ?>" class="brand-icon"> <br>-->
					<span class="brand-title"><?php echo appName;?> Admin</span>
				</a>
			</div>
		</div>
		<!--===================================================-->
		
		<!-- LOGIN FORM -->
		<!--===================================================-->
		<div class="cls-content">
			<div class="cls-content-sm panel">
				<div class="panel-body">
					<p class="pad-btm">Sign In to your account</p>
					<?php echo form_open('admin/login',['id' => 'frm_login']); ?>
						<div class="form-group">
							<div class="input-group">
								<div class="input-group-addon"><i class="fa fa-user"></i></div>
								<input class="form-control" type="text" name="user_name" id="user_name" value="" />
							</div>
						</div>
						<div class="form-group">
							<div class="input-group">
								<div class="input-group-addon"><i class="fa fa-asterisk"></i></div>
								<input class="form-control" type="password" name="user_password" id="user_password" value="" />
							</div>
						</div>
						<div class="form-group">
							<div class="input-group">
								<label class="control-label text-danger" id='error_msg'></label>
							</div>
						</div>
						<div class="row">
							<div class="col-xs-8 text-left checkbox">
								&nbsp;
							</div>
							<div class="col-xs-4">
								<div class="form-group text-right">
								<button class="btn btn-success text-uppercase" value="Login" name="B1" type="submit">Sign In</button>
								</div>
							</div>
						</div>
						
					</form>
				</div>
			</div>
			
		</div>
		<!--===================================================-->
						
	</div>
	<!--===================================================-->
	<!-- END OF CONTAINER -->

		
	<!--JAVASCRIPT-->
	<!--=================================================-->
	<!--jQuery [ REQUIRED ]-->
	<script src="<?php echo base_url('auctioneer_assets/js/jquery-2.1.1.min.js'); ?>"></script>

	<!--BootstrapJS [ RECOMMENDED ]-->
	<script src="<?php echo base_url('auctioneer_assets/js/bootstrap.min.js'); ?>"></script>

	<!--Fast Click [ OPTIONAL ]-->
	<script src="<?php echo base_url('auctioneer_assets/js/fastclick.min.js'); ?>"></script>
	
	<!--Nifty Admin [ RECOMMENDED ]-->
	<script src="<?php echo base_url('auctioneer_assets/js/nifty.min.js'); ?>"></script>
	
	<!--Form Js [ RECOMMENDED ]-->
	<script src="<?php echo base_url('auctioneer_assets/js/jquery.form.js'); ?>"></script>
	<script src="<?php echo base_url('auctioneer_assets/js/jquery.validate.js'); ?>"></script>

	<script>
		$(document).ready(function() {
			var vRules = {
				user_name:{required:true},
				user_password:{required:true}
			};
			var vMessages = {
				user_name:{required:"Please enter Username"},
				user_password:{required:"Please enter password"}
			};

			$("#frm_login").validate({
				rules: vRules,
				messages: vMessages,
				submitHandler: function(form){
					$(form).ajaxSubmit({
						url:"<?php echo base_url('admin/main/login');?>",
						type: 'post',
						dataType:'json',
						cache: false,
						clearForm: false,
						success: function (response) {               
							if(response.status=="success")
							{
								window.location.href="<?php echo base_url('admin/main/dashboard');?>";
							}
							else
							{	
								$("#error_msg").html(response.message);
								$("#error_msg").show();
								return false;
							}
						}
					});
				}
			});
		});
	</script>
		
</body>
</html>