<style>
fieldset { 
  display: block;
  margin-left: 2px;
  margin-right: 2px;
  padding-top: 0.35em;
  padding-bottom: 0.625em;
  padding-left: 0.75em;
  padding-right: 0.75em;
  border: 2px groove (internal value);
}

.input-sm{
    margin-top: 5%;
}

</style>
<!--CONTENT CONTAINER-->
<!--===================================================-->
<div id="content-container">
	<!--Page Title-->
	<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
	<div id="page-title">
		<h1 class="page-header text-overflow"><?php echo $pagetitle?></h1>
	</div>
	<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
	<!--End page title-->
	<!--Breadcrumb-->
	<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
	<ol class="breadcrumb">
		<li><a href="<?php echo base_url('admin/dashboard');?>">Home</a></li>
		<li class="active"><?php echo $pagetitle?></li>
	</ol>
	<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
	<!--End breadcrumb-->
	<input type="hidden" id="t11" name="auctionId">
	<div id="page-content">	
		<div class="panel">
			<div class="panel-heading">
				<h3 class="panel-title"><?php echo $pagetitle?></h3>
			</div>
			<div class="panel-body">
                <button onclick="showModal()" class="pull-right btn btn-primary">Add</button>
                
                <table id="users" class="table table-striped table-bordered table-hover table-responsive">
                    <thead>
                        <th>USER NAME</th>
                        <th>USER ID</th>
                        <th>USER TYPE</th>
                        <th>EMAIL</th>
                        <th>RESTRICTED</th>
                        <th>ACTIONS</th>
                    </thead>
                    <tbody>
                        <?php foreach ($user_list as $value){ ?>
                            <tr>
                                <td><?php echo $value['user_name']; ?></td>
                                <td><?php echo $value['userid']; ?></td>
                                <td><?php echo $value['usertype']; ?></td>
                                <td><?php echo $value['email']; ?></td>
                                <td><?php echo $value['restricted']; ?></td>
                                <td><button uid="<?php echo $value['id']; ?>"
                                            user_name="<?php echo $value['user_name']; ?>"
                                            userid="<?php echo $value['userid']; ?>"
                                            usertype="<?php echo $value['usertype']; ?>"
                                            email="<?php echo $value['email']; ?>"
                                            restricted="<?php echo $value['restricted']; ?>"
                                            password="<?php echo $value['password']; ?>"
                                            id="edit_<?php echo $value['id']; ?>"
                                            onclick="editData('<?php echo $value['id']; ?>')">Edit</button>
                                    <button onclick="deleteUser('<?php echo $value['id']; ?>')">Delete</button></td>
                            </tr>
                        <?php } ?>
                    </tbody>
                </table>

                <!-- Modal -->
                <div class="modal fade" id="add_edit_user" role="dialog">
                    <div class="modal-dialog">
                        <!-- Modal content-->
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                <h4 class="modal-title">Add/Edit User</h4>
                            </div>
                            <form id="add_update_user">
                                <div class="modal-body">
                                    <input type="hidden" id='uid' name="id">
                                    <div class='col-sm-12 form-group'>
                                        <div class="col-sm-3">
                                            <label>Person Name : </label>
                                        </div>
                                        <div class="col-sm-9 ">
                                            <input class="form-control" id="user_name" type="text" name="user_name">
                                        </div>
                                    </div>
                                    <div class='col-sm-12 form-group'>
                                        <div class="col-sm-3">
                                            <label>User Id : </label>
                                        </div>
                                        <div class="col-sm-9">
                                            <input class="form-control" id="userid" type="text" name="userid">
                                        </div>
                                    </div>
                                    <div class='col-sm-12 form-group'>
                                        <div class="col-sm-3">
                                            <label>Password : </label>
                                        </div>
                                        <div class="col-sm-9">
                                            <input class="form-control" id="password" type="text" name="password">
                                        </div>
                                    </div>
                                    <div class='col-sm-12 form-group'>
                                        <div class="col-sm-3">
                                            <label>User Type : </label>
                                        </div>
                                        <div class="col-sm-9">
                                            <select class="form-control" id="usertype" name="usertype">
                                                <option></option>    
                                                <option value="U">User</option>    
                                                <option value="A">Admin</option>
                                                <option value="S">Super Admin</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class='col-sm-12 form-group'>
                                        <div class="col-sm-3">
                                            <label>Email : </label>
                                        </div>
                                        <div class="col-sm-9">
                                            <input class="form-control" id="email" type="text" name="email">
                                        </div>
                                    </div>
                                    <div class='col-sm-12 form-group'>
                                        <div class="col-sm-3">
                                            <label>Restricted : </label>
                                        </div>
                                        <div class="col-sm-9">
                                            <select class="form-control" id="restricted" name="restricted">
                                                <option></option>
                                                <option value="Y">Yes</option>
                                                <option value="N">No</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <button class="btn btn-primary">Add</button>
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
			</div>	
		</div>
	</div>	
</div>

<!--===================================================-->
<!--END CONTENT CONTAINER-->

<script>

    $(document).ready(function(){
        $("#usertype").select2({
            dropdownParent: $('#add_edit_user'),
            placeholder: "Select user type"
        });
        $("#restricted").select2({
            dropdownParent: $('#add_edit_user'),
            placeholder: "Select Restricted"
        });
        $("#users").dataTable();
    })

    var vrules = {
        user_name:"required",
		user_id:"required",
		email: "required",
		password: "required",
		usertype:"required",
		resticted: "required"
	};
	var vmessages = {
		user_name: {
            required: "<b class='text-danger'>Please Enter User Name</b>"
        },
		user_id:{
			required: "<b class='text-danger'>Please Enter User Id</b>",
		},
		email: {
            required: "<b class='text-danger'>Please Enter Email Id</b>"
        },
		password: {
            required: "<b class='text-danger'>Please Enter Confirm Password</b>"
        },
		usertype:{
			required: "<b class='text-danger'>Please Select user type</b>",
		},
		restricted: {
            required: "<b class='text-danger'>Please Select restricted</b>"
        }
	};

    function showModal()
    {
        $("#uid").val('');
        $("#user_name").val('');
        $("#userid").val('');
        $("#password").val('');
        $('#usertype').val('').trigger('change');
        $('#restricted').val('').trigger('change');
        $("#email").val('');
        $("#add_edit_user").modal('show');
    }

    function editData(id)
    {
        $("#uid").val($('#edit_'+id).attr('uid'));
        $("#user_name").val($('#edit_'+id).attr('user_name'));
        $("#userid").val($('#edit_'+id).attr('userid'));
        $("#password").val($('#edit_'+id).attr('password'));
        $('#usertype').val($('#edit_'+id).attr('usertype')).trigger('change');
        $('#restricted').val($('#edit_'+id).attr('restricted')).trigger('change');
        $("#email").val($('#edit_'+id).attr('email'));
        $("#add_edit_user").modal('show');
    }

    $('#add_update_user').validate({
		rules: vrules,
		messages: vmessages,
		submitHandler: function(form){
			$(form).ajaxSubmit({
				url:"<?php echo base_url('admin/master/add_update_user');?>",
				type:"post",
				dataType: "json",
				success: function (response){
					if (response.status == "success"){
						Display_msg(response.message,response.status);
						setTimeout(function(){
							window.location.href="<?php echo base_url('admin/master/admin_users');?>";
						}, 3000);
					} else {
						Display_msg(response.message,response.status);
					}
				},
				error: function(e){
					Display_msg(e);
				}
			});
		}
	});

    function deleteUser(id)
    {
        $.ajax({
            url: '<?php echo base_url();?>admin/master/delete_user',
            type:"post",
            data: {'id':id},
            dataType:'json',
            success: function (response) {
                Display_msg(response.message,response.status);
                setTimeout(function(){
                    window.location.reload(1);
                }, 3000);
            },
            error: function(){
                Display_msg('Please refresh the page and try again','failed');
            }
        });
    }
</script>