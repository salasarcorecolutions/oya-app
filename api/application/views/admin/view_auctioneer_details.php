<!--CONTENT CONTAINER-->
<!--===================================================-->
<div id="content-container">
	<!--Page Title-->
	<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
	<div id="page-title">
		<h1 class="page-header text-overflow">Auctioneer List</h1>
	</div>
	<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
	<!--End page title-->


	<!--Breadcrumb-->
	<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
	<ol class="breadcrumb">
		<li><a href="<?php echo base_url('admin/dashboard');?>">Home</a></li>
		<li class="active">Auctioneer List</li>
	</ol>
	<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
	<!--End breadcrumb-->
	<div id="page-content">	
		<div class="panel">
			<div class="panel-heading">
				<h3 class="panel-title">Auctioneer List</h3>
			</div>
			
			<div class="panel-body">
				<form name="frm" method="post" id="auctioneer_form">
					<div class="row filter">
						
						<div class="col-sm-2">
							<input class="form-control searchInput" placeholder="Auctioneer Name" type="text" name="search_user_name" id="search_user_name" value="" />
						</div>
						<div class="col-sm-2">
							<input class="form-control searchInput" placeholder="Vendor Name" type="text" name="search_vendor_name" id="search_vendor_name" value="" />
						</div>
						<div class="col-sm-2">
							<input class="form-control searchInput" placeholder="Plant Name" type="text" name="search_plant_unit" id="search_plant_unit" value="" />
						</div>
						<div class="col-sm-2" style="display:none;">
							<input class="form-control searchInput" placeholder="" type="text" name="search_reg_type" id="search_reg_type" value="1" />
						</div>
						
						<div class="col-sm-2 pull-right">
							<a style=" float: left;" class="btn btn-default pull-right" onclick="refresh_datatable();">Refresh </a>
							<a style=" float: left;" class="btn btn-info pull-right" onclick="clearSearchFilters();">Clear Search </a>
						</div>
					</div>
					<div class="table-responsive">
						<table callfunction="<?php echo base_url('admin/auctioneer_details/fetch_auctioneer_data')?>" class="table table-striped table-bordered table-hover no-footer dtr-inline dynamicTable">
							<thead>
								<tr>
									<th data-bSortable="false">SR</th>
									<th>AUCTIONEER NAME</th>
									<th>COMPANY NAME</th>
									<th>PLANT NAME</th>
									<th>VIEW DETAILS</th>
									<th data-bSortable="false">ACTION</th>
								</tr>
							</thead>
							<tbody>
							</tbody>
						</table>
					</div>	
				</form>
			</div>	
		</div>
	</div>	
</div>
<!--===================================================-->
<!--END CONTENT CONTAINER-->

<script>

function change_registration_status(user_contact_id,status){
	 $.ajax({
		url :'<?php echo base_url('admin/auctioneer_details/change_registration_status')?>',
		data:{'contact_id':user_contact_id,'status':status},
		type:'POST',
		dataType:'json',
		success:function(response)
		{
			if(response.status == 'success')
			{
				if(response.status == "success")
				{
					Display_msg(response.msg,response.status);
					refresh_datatable();
				}
				else
				{
					Display_msg(response.msg,response.status);
					return false;
				}
			}		
		}		 
	 });
}

function view_details(user_contact_id)
{
	window.open('<?php echo base_url()?>admin/auctioneer_details/show_auctioneer_details/'+user_contact_id+'/auctioneer','width=800,height=400');
}


</script>
