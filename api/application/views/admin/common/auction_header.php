<?php

/* $link is used here to connect to mysql and require constant file */
$imgpath = "productpic";
$pagepath = "admin.php?src=../";

?>

<!DOCTYPE HTML>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title><?php if(isset($pagetitle)){echo $pagetitle;}else{echo "Salasar Marketplace";} ?></title>

	<!-----------------------------------DataTables css---------------------------------------------------------->
	<link rel="stylesheet" href="<?php echo base_url('auctioneer_assets/css/dataTables/dataTables.bootstrap.css'); ?>" />
	<link rel="stylesheet" href="<?php echo base_url('auctioneer_assets/css/dataTables/dataTables.responsive.css'); ?>" />
	<!--Bootstrap Stylesheet [ REQUIRED ]-->
	<link href="<?php echo base_url('auctioneer_assets/css/bootstrap.min.css'); ?>" rel="stylesheet">
	<!--Nifty Stylesheet [ REQUIRED ]-->
	<link href="<?php echo base_url('auctioneer_assets/css/nifty.min.css'); ?>" rel="stylesheet">
	<!--Font Awesome [ OPTIONAL ]-->
	<link href="<?php echo base_url('auctioneer_assets/plugins/font-awesome/css/font-awesome.min.css'); ?>" rel="stylesheet">
	<!--Animate.css [ OPTIONAL ]-->
	<link href="<?php echo base_url('auctioneer_assets/plugins/animate-css/animate.min.css'); ?>" rel="stylesheet">
	<!--Morris.js [ OPTIONAL ]-->
	<link href="<?php echo base_url('auctioneer_assets/plugins/morris-js/morris.min.css'); ?>" rel="stylesheet">
	<!--Summernote [ OPTIONAL ]-->
	<link href="<?php echo base_url('auctioneer_assets/plugins/summernote/summernote.min.css'); ?>" rel="stylesheet">
	<!--Switchery [ OPTIONAL ]-->
	<link href="<?php echo base_url('auctioneer_assets/plugins/switchery/switchery.min.css'); ?>" rel="stylesheet">
	<!--Bootstrap Select [ OPTIONAL ]-->
	<link href="<?php echo base_url('auctioneer_assets/plugins/bootstrap-select/bootstrap-select.min.css'); ?>" rel="stylesheet">
	
	<!--Bootstrap Tags Input [ OPTIONAL ]-->

	<!--Chosen [ OPTIONAL ]-->
	<link href="<?php echo base_url('auctioneer_assets/plugins/chosen/chosen.min.css'); ?>" rel="stylesheet">
	<!--Demo script [ DEMONSTRATION ]-->
	<!-- <link href="<?php echo base_url('auctioneer_assets/css/nifty-demo.min.css'); ?>" rel="stylesheet"> -->

	<!--SCRIPT-->
	<!--=================================================-->

	<!--Page Load Progress Bar [ OPTIONAL ]-->
	<link href="<?php echo base_url('auctioneer_assets/plugins/pace/pace.min.css'); ?> " rel="stylesheet">
	<script src="<?php echo base_url('auctioneer_assets/plugins/pace/pace.min.js'); ?>"></script>
	
	<!--Bootstrap Timepicker [ OPTIONAL ]-->
	<link href="<?php echo base_url('auctioneer_assets/plugins/bootstrap-timepicker/bootstrap-timepicker.min.css'); ?>" rel="stylesheet">

	<!--Bootstrap Datepicker [ OPTIONAL ]-->
	<link href="<?php echo base_url('auctioneer_assets/plugins/bootstrap-datepicker/bootstrap-datepicker.css'); ?>" rel="stylesheet">
	
	<link href="<?php echo base_url('auctioneer_assets/css/style.css'); ?>" rel="stylesheet">

	<link href="https://cdn.jsdelivr.net/npm/select2@4.0.13/dist/css/select2.min.css" rel="stylesheet">
	<!-- Favicons
	================================================== -->
	<link rel="shortcut icon" href="<?php echo base_url('auctioneer_assets/favicon.ico'); ?>">
	<script src="<?php echo base_url('auctioneer_assets/js/jquery-2.1.1.min.js'); ?>"></script>
	<!--BootstrapJS [ RECOMMENDED ]-->
	<script src="<?php echo base_url('auctioneer_assets/js/bootstrap.min.js'); ?>"></script>
	<!-----------------------------------DataTables JS---------------------------------------------------------->
	<script src="<?php echo base_url('auctioneer_assets/js/datatables/jquery.dataTables.min.js'); ?>"></script>
	<script src="<?php echo base_url('auctioneer_assets/js/datatables/dataTables.bootstrap.min.js'); ?>"></script>
	<script src="<?php echo base_url('auctioneer_assets/js/datatables/manual_datatable.js'); ?>"></script>
	<!--Fast Click [ OPTIONAL ]-->
	<script src="<?php echo base_url('auctioneer_assets/js/fastclick.min.js'); ?>"></script>
	<!--Modals [ SAMPLE ]-->
	<script src="<?php echo base_url('auctioneer_assets/js/ui-modals.js'); ?>"></script>

	<!--Bootbox Modals [ OPTIONAL ]-->
	<script src="<?php echo base_url('auctioneer_assets/js/bootbox.min.js'); ?>"></script>
	<script src="<?php echo base_url('auctioneer_assets/plugins/morris-js/raphael-js/raphael.min.js'); ?>"></script>

	<script src="<?php echo base_url('auctioneer_assets/plugins/summernote/summernote.min.js'); ?>"></script>

	<!--Skycons [ OPTIONAL ]-->
	<script src="<?php echo base_url('auctioneer_assets/plugins/skycons/skycons.min.js'); ?>"></script>

	<!-- Validatation [ RECOMMENDED ]-->
	<script src="<?php echo base_url('auctioneer_assets/js/jquery.validate.js'); ?>"></script>
	<script src="<?php echo base_url('auctioneer_assets/js/jquery.form.js'); ?>"></script>

	<!--Switchery [ OPTIONAL ]-->
	<script src="<?php echo base_url('auctioneer_assets/plugins/switchery/switchery.min.js'); ?>"></script>

	<!--Bootstrap Select [ OPTIONAL ]-->
	<script src="<?php echo base_url('auctioneer_assets/plugins/bootstrap-select/bootstrap-select.min.js'); ?>"></script>

	<link href="<?php echo base_url('auctioneer_assets/css/datatables/dataTables.bootstrap.css'); ?>" rel="stylesheet">
	<link href="<?php echo base_url('auctioneer_assets/css/datatables/dataTables.responsive.css'); ?>" rel="stylesheet">
	<!--Chosen [ OPTIONAL ]-->
	<script src="<?php echo base_url('auctioneer_assets/plugins/chosen/chosen.jquery.min.js'); ?>"></script>

	<!--Bootstrap Tags Input [ OPTIONAL ]-->
	<script src="<?php echo base_url('auctioneer_assets/plugins/bootstrap-tagsinput/bootstrap-tagsinput.js'); ?>"></script>

	<!--Bootstrap Timepicker [ OPTIONAL ]-->
	<script src="<?php echo base_url('auctioneer_assets/plugins/bootstrap-timepicker/bootstrap-timepicker.min.js'); ?>"></script>

		<!--Bootstrap Datepicker [ OPTIONAL ]-->
	<script src="<?php echo base_url('auctioneer_assets/plugins/bootstrap-datepicker/bootstrap-datepicker.js'); ?>"></script>

<script language="javascript">
function refreshParent(page) {
  location.href = "index.php?src="+page;
}
</script>

<style>
a.btn {
    margin: 1px;
}
.loading{
	position: fixed;
	top: 0%;
	opacity: 0.7;
	filter: alpha(opacity=70);
	width: 100%;
	height:100%;
	right: 0px;
	background-color: white;
	z-index: 50000;
}
.loading img{
	margin-top:10%;
}
</style>
<!-- added by aatish gupta start  22 sep 2016-->
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.js"></script>
<!-- added by aatish gupta end-->
</head>
<body>

<!-- Top Menu -->

<div id="container" class="effect mainnav-lg">
				<!--NAVBAR-->
		<!--===================================================-->
		<header id="navbar">
			<div id="navbar-container" class="boxed">

				<!--Brand logo & name-->
				<!--================================-->
				<div class="navbar-header">

					<!-- _dev_ Please look at this that how to adjust href in case of called from the controller. -->
					<a href="vendor/" class="navbar-brand">

						<!--<img src="<?php echo base_url('auctioneer_assets/images/logo.png'); ?>" alt="Nifty Logo" class="brand-icon">-->
						<div class="brand-title">
							<span class="brand-text"><?php echo appName; ?> Admin</span>
						</div>
					</a>
				</div>
				<!--================================-->
				<!--End brand logo & name-->


				<!--Navbar Dropdown-->
				<!--================================-->
				
				<div class="navbar-content clearfix">
					<ul class="nav navbar-top-links pull-left">

						<!--Navigation toogle button-->
						<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
						<li class="tgl-menu-btn">
							<a class="mainnav-toggle" href="#">
								<i class="fa fa-navicon fa-lg"></i>
							</a>
						</li>
						<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
						<!--End Navigation toogle button-->
					</ul>
					<ul class="nav navbar-top-links pull-right">

						<!--User dropdown-->
						<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
						<li id="dropdown-user" class="dropdown">
							<a href="#" data-toggle="dropdown" class="dropdown-toggle text-right">
								<span class="pull-right">
								<img class="img-circle img-user media-object" alt="Profile Picture" src="<?php echo base_url('auctioneer_assets/images/av1.png'); ?>">

								</span>
								<div class="username hidden-xs"><?php echo $this->session->userdata('ha_uname'); ?></div>
							</a>
							<div class="dropdown-menu dropdown-menu-md dropdown-menu-right with-arrow panel-default">
								<!-- Dropdown footer -->
								<div class="pad-all text-right">
									<a class="btn btn-primary" onclick="showTimeZoneModal()">
										Time Zone
									</a>
									<a href="<?php echo base_url('admin/main/logout') ?>" class="btn btn-primary">
										<i class="fa fa-sign-out fa-fw"></i> Sign Out
									</a>
								</div>
							</div>
						</li>
						<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
						<!--End user dropdown-->

					</ul>
					<ul class="nav navbar-top-links pull-right">
						<div class="brand-title">
							<div class="username hidden-xs"><div id='date-part'></div></div>
						</div>
					</ul>	
					</div>
				<!--================================-->
				<!--End Navbar Dropdown-->
				
			</div>
		</header>

		<!-- Modal -->
		<div class="modal fade" id="time_zone_modal" role="dialog">
			<div class="modal-dialog">
				<!-- Modal content-->
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal">&times;</button>
						<h4 class="modal-title">Update time zone</h4>
					</div>
					<div class="modal-body">
						<div>
							<select class="form-control" id="time_zone_list">
							</select>
						</div>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-primary" onclick="updateUserTimeZone()">Update</button>
						<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
					</div>
				</div>
			</div>
		</div>
		<!--===================================================-->
		<!--END NAVBAR-->
	<div class="boxed">	
 