<div id="container" class="effect mainnav-lg">
				<!--NAVBAR-->
		<!--===================================================-->
		<header id="navbar">
			<div id="navbar-container" class="boxed">

				<!--Brand logo & name-->
				<!--================================-->
				<div class="navbar-header">

					<!-- _dev_ Please look at this that how to adjust href in case of called from the controller. -->
					<a href="admin.php?src=init.php" class="navbar-brand">

						<img src="<?php echo base_url('assets/images/logo.png'); ?>" alt="Nifty Logo" class="brand-icon">
						<div class="brand-title">
							<span class="brand-text">Oya Auction</span>
						</div>
					</a>
				</div>
				<!--================================-->
				<!--End brand logo & name-->


				<!--Navbar Dropdown-->
				<!--================================-->
				
				<div class="navbar-content clearfix">
					<ul class="nav navbar-top-links pull-left">

						<!--Navigation toogle button-->
						<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
						<li class="tgl-menu-btn">
							<a class="mainnav-toggle" href="#">
								<i class="fa fa-navicon fa-lg"></i>
							</a>
						</li>
						<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
						<!--End Navigation toogle button-->
					</ul>
					<ul class="nav navbar-top-links pull-right">

						<!--User dropdown-->
						<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
						<li id="dropdown-user" class="dropdown">
							<a href="#" data-toggle="dropdown" class="dropdown-toggle text-right">
								<span class="pull-right">
								<img class="img-circle img-user media-object" alt="Profile Picture" src="<?php echo base_url('assets/images/av1.png'); ?>">

								<!-- <?php
								/* if($_SESSION['user_type']=="A")		$sql="Select mime from complex_admins where user_id='".$_SESSION['user_id']."'";
								if($_SESSION['user_type']=="O")		$sql="Select mime from block_flats_occupent_details  where occupent_id='".$_SESSION['user_id']."'";
								if($_SESSION['user_type']=="M")		$sql="Select mime from block_flats_occupent_member_details where member_id='".$_SESSION['user_id']."'";
								 $result  = mysql_query($sql) or die('Error, query failed'.mysql_error());
								if(mysql_affected_rows($link)>0)
								{
									$row     = mysql_fetch_array($result, MYSQL_ASSOC);
									$mime=$row['mime'];
									$path="site_img/user_imgs/".$_SESSION['user_type']."_".$_SESSION['user_id'].".".$mime;
								}
								if(is_file($path))
									echo "<img src='".$path."' class='img-circle img-user media-object' alt='".$_SESSION['user_name']."'/>";
									else
									{
									$path="share_images/new/av1.png";
									echo "<img src='".$path."' class='img-circle img-user media-object' alt='".$_SESSION['user_name']."'/>";
									} */
								?> -->
									
								</span>
								<div class="username hidden-xs"><?php echo $_SESSION['ha_uname']; ?></div>
							</a>


							<div class="dropdown-menu dropdown-menu-md dropdown-menu-right with-arrow panel-default">

								
								<!-- User dropdown menu 
								<ul class="head-list">
									<li>
										<a href="page.php">
											<i class="fa fa-dashboard fa-fw fa-lg"></i>  Dashboard
										</a>
									</li>
									<li>
										<a href="admin.php?src=<?php echo $profile_page;?>">
											<i class="fa fa-user fa-fw fa-lg"></i> My Profile
										</a>
									</li>
									<li>
										<a href="admin.php?src=message_list.php">
											<i class="fa fa-envelope fa-fw fa-lg"></i> Messages
										</a>
									</li>
									<li>
										<a href="admin.php?src=change_password.php">
											<i class="fa fa-gear fa-fw fa-lg"></i> Change Password
										</a>
									</li>
									
								</ul>-->

								<!-- Dropdown footer -->
								<div class="pad-all text-right">
									<a href="logout.php" class="btn btn-primary">
										<i class="fa fa-sign-out fa-fw"></i> Sign Out
									</a>
								</div>
							</div>
						</li>
						<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
						<!--End user dropdown-->

					</ul>
					<ul class="nav navbar-top-links pull-right">
						<div class="brand-title">
							<div class="username hidden-xs"><?php echo (date("l dS \of F Y h:i:s A")); ?></div>
						</div>
					</ul>	
					</div>
				<!--================================-->
				<!--End Navbar Dropdown-->
				
			</div>
		</header>
		
		<!--===================================================-->
		<!--END NAVBAR-->
	<div class="boxed">	

