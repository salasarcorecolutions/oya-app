<?php
/* _dev_ Please take care of the admin.php?src files that are called here. */
?>

<!--MAIN NAVIGATION-->
<!--===================================================-->
<nav id="mainnav-container">
	<div id="mainnav">

		<!--Shortcut buttons-->
		<!--================================-->
		<div id="mainnav-shortcut">
			<ul class="list-unstyled">
				<li class="col-xs-4" >
				
				</li>
				<li class="col-xs-4">

				</li>
				<li class="col-xs-4">

				</li>
			</ul>
		</div>
		<!--================================-->
		<!--End shortcut buttons-->
	
		<!--Menu-->
		<!--================================-->
		<div id="mainnav-menu-wrap">
			<div class="nano">
				<div class="nano-content">
					<ul id="mainnav-menu" class="list-group">
						<!--Category name-->
						<li>
							<a href="<?php echo base_url()?>">
								<i class="fa fa-dashboard"></i>
								<span class="menu-title">
									<strong>Oya Dashboard</strong>
								</span>
							</a>
						</li>

						<!--Menu list item-->
						<li>
							<a href="<?php echo base_url('admin/main/dashboard')?>">
								<i class="fa fa-dashboard"></i>
								<span class="menu-title">
									<strong>Dashboard</strong>
								</span>
							</a>
						</li>
						<li>
							<a href="#">
								<i class="fa fa-user"></i>
								<span class="menu-title">Master Data</span>
								<i class="arrow"></i>
							</a>
							<ul class="collapse">
								<li><a href="<?php echo base_url('admin/master/material_cat');?>">Auction Categories</a></li>
								<li><a href="<?php echo base_url('admin/master/material');?>">Auction Sub-Categories(Materials)</a></li>
								<li><a href="<?php echo base_url('admin/master/currency_master');?>">Currency Master</a></li>
							</ul>
						</li>
						<li>
							<a href="#">
								<i class="fa fa-user"></i>
								<span class="menu-title">Location Master Data</span>
								<i class="arrow"></i>
							</a>
							<ul class="collapse">
								
								<li><a href="<?php echo base_url('admin/master/country');?>">Country Master</a></li>
								<li><a href="<?php echo base_url('admin/master/state');?>">State Master</a></li>
								<li><a href="<?php echo base_url('admin/master/city');?>">City Master</a></li>
									
							</ul>
						</li>
						<!--Category name-->
			
						<!--Menu list item-->
						<li>
							<a href="#">
								<i class="fa fa-user"></i>
								<span class="menu-title">User</span>
								<i class="arrow"></i>
							</a>
							<!--Submenu-->
							<ul class="collapse">
									<li>
										<a href="<?php echo base_url('admin/master/admin_users');?>">
											<i class="fa fa-user-plus" aria-hidden="true"></i>
											<span class="menu-title">Add User</span>
										</a>
									</li>
									<li>
										<a href="#">
											<i class="fa fa-university"></i>
											<span class="menu-title">Company</span>
											<i class="arrow"></i>
										</a>
										<!--Submenu-->
										<ul class="collapse">
											<li><a href="<?php echo base_url('admin/view_company_details/registered_companies');?>">Registered Companies Details</a></li>
											<li><a href="<?php echo base_url('admin/vendor/index');?>">Users List</a></li>
										</ul>
									</li>

									<li>
										<a href="#">
											<i class="fa fa-briefcase"></i>
											<span class="menu-title">Auctioneer</span>
											<i class="arrow"></i>
										</a>
										<!--Submenu-->
										<ul class="collapse">

											<li><a href="<?php echo base_url('admin/view_company_details/view_auctioneer_details');?>">Registered Auctioneer Details</a></li>
											<li><a href="<?php echo base_url('admin/view_company_details/link_company');?>">Linked Companies</a></li>
											<li><a href="<?php echo base_url('admin/auctioneer/index');?>">Auctioneer List</a></li>

										</ul>
									</li>
									<li>
										<a href="#">
											<i class="fa fa-gavel"></i>
											<span class="menu-title">Bidders</span>
											<i class="arrow"></i>
										</a>
										<!--Submenu-->
										<ul class="collapse">
											<li><a href="<?php echo base_url('admin/bidder_details/bidder');?>">Registered Bidders</a></li>
										</ul>
									</li>
									
							</ul>
						</li>
						<li>
							<a href="<?php echo base_url('admin/salvage');?>">
								<i class="fa fa-dashboard" aria-hidden="true"></i>
								<span class="menu-title">Products Section</span>
							</a>
							<ul class="collapse">
								<li><a href="<?php echo base_url('admin/salvage/manufacturer_master');?>">Manufacturer Master</a></li>
								<li><a href="<?php echo base_url('admin/salvage/product_category_master');?>">Master Category</a></li>
								<li><a href="<?php echo base_url('admin/salvage/index');?>">Product</a></li>
								<li><a href="<?php echo base_url('admin/salvage_auction_reports/view_all_salvage_reports');?>">All Product Auction Reports</a></li>
							</ul>
						</li>
						<li>
							<a href="#">
								<i class="fa fa-user"></i>
								<span class="menu-title">Permission</span>
								<i class="arrow"></i>
							</a>
							<!--Submenu-->
							<ul class="collapse">
								<li>
									<a href="<?php echo base_url('admin/permission/roles');?>">
										<i class="fa fa-user-plus" aria-hidden="true"></i>
										<span class="menu-title">Roles</span>
									</a>
								</li>
								<li>
									<a href="<?php echo base_url('admin/permission/module_head');?>">
										<i class="fa fa-user-plus" aria-hidden="true"></i>
										<span class="menu-title">Module Head</span>
									</a>
								</li>

								<li>
									<a href="<?php echo base_url('admin/permission/module_sub_head');?>">
										<i class="fa fa-user-plus" aria-hidden="true"></i>
										<span class="menu-title">Module Sub Head</span>
									</a>
								</li>
							</ul>
						</li>
						<li>
							<a href="#">
								<i class="fa fa-user"></i>
								<span class="menu-title">FAQ</span>
								<i class="arrow"></i>
							</a>
							<ul class="collapse">
								<li><a href="<?php echo base_url('admin/master/faq_group');?>">FAQ Group</a></li>
								<li><a href="<?php echo base_url('admin/master/add_faq');?>">FAQ</a></li>
							</ul>
						</li>
					</ul>
					<!--================================-->
					<!--End widget-->
				</div>
			</div>
		</div>
		<!--================================-->
		<!--End menu-->

	</div>
</nav>
<!--===================================================-->
<!--END MAIN NAVIGATION-->			

