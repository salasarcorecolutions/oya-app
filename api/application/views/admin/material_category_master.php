
<!--CONTENT CONTAINER-->
<!--===================================================-->
<div id="content-container">
	<!--Page Title-->
	<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
	<div id="page-title">
		<h1 class="page-header text-overflow"><?php echo $pagetitle;?></h1>
	</div>
	<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
	<!--End page title-->


	<!--Breadcrumb-->
	<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
	<ol class="breadcrumb">
		<li><a href="#">Settings</a></li>
		<li><a href="<?php echo base_url('admin/dashboard');?>">Master Data</a></li>
		<li class="active"><?php echo $pagetitle;?></li>

	</ol>
	<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
	<!--End breadcrumb-->
	
	<div id="page-content">	
		<div class="panel">
			<div class="panel-heading">
				<h3 class="panel-title"><?php echo $pagetitle;?></h3>
				<div class="col-sm-3 pull-right">
					<button class="btn btn-info pull-right" style="margin:5%;" id="add_mat_cat" onclick='add_mat_cat();'>ADD</button>
					
				</div>
			</div>
			
			<div class="panel-body">
				<form name="frm" method="post">
					<div class="row filter">
						
						<div class="col-sm-2">
							<input class="form-control searchInput" placeholder="Material Category" type="text" name="search_user_name" id="search_user_name" value="" />
						</div>
						<div class="col-sm-2">
							<select class="form-control searchInput" name="is_active" id="active" >
								<option value="1">Yes</option>
								<option value="0">No</option>
							</select>
						</div>
						<div class="col-sm-2 pull-right">
							<a style=" float: left;" class="btn btn-default pull-right" onclick="refresh_datatable();">Refresh </a>
							<a style=" float: left;" class="btn btn-info pull-right" onclick="clearSearchFilters();">Clear Search </a>
						</div>
					</div>
					<div class="table-responsive">
						<table callfunction="<?php echo base_url('admin/master/fetch_material_cat')?>" class="table table-striped table-bordered table-hover no-footer dtr-inline dynamicTable">
							<thead>
								<tr>
									<th data-bSortable="false">SR</th>
									<th>MATERIAL CATEGORY</th>
									<th>CATEGORY IMAGE</th>
									<th>IS ACTIVE</th>
									<th>UPDATED BY</th>
									<th data-bSortable="false">UPDATED ON</th>
									<th data-bSortable="false">ACTION</th>
								</tr>
							</thead>
							<tbody>
							</tbody>
							
						</table>
					</div>	
				</form>
			</div>	
		</div>
	</div>	
</div>
<!--===================================================-->
<!--END CONTENT CONTAINER-->
<!--modal-->
		<div class="modal fade" id="add_mat_cat_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
			<div class="modal-dialog" role="document">
				<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title" id="exampleModalLabel">Add Category</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body" style="height:200px">
					<div class="col-sm-12">
						<form id="add_group_form" class="form-horizontal" name="add_group_form">
							<input type="hidden" name="category_id" id="category_id"/>
							<div class="form-group">
								<div class="col-sm-3">
									<label class="label-control">Material Category:</label>
								</div>
								<div class="col-sm-6">
									<input name='mat_cat' class="form-control" id="mat_cat"  />
								</div>
							</div>
							<div class="form-group">
								
								<div><a id="change_image">Change Image</a></div>
								
							</div>
							<div class="form-group img">
								<div class="col-sm-3">
									<label class="label-control">Category Image</label>
								</div>
								<div class="col-sm-6">
									<input class="form-control" name="category_image" type="file" id="img" accept="image/*">
								</div>
							</div>
							<div class="form-group">
								<div class="col-sm-3">
									<label class="label-control">Is Active:</label>
								</div>
								<div class="col-sm-6">
									<select class="form-control" name="is_active" id="is_active">
										<option value="1">Yes</option>
										<option value="0">No</option>
									</select>
								</div>
							</div>
						</form>
					</div>
					</br></br></br></br></br>
					
				</div>
				<div class="modal-footer">
					
					<button type="button" class="btn btn-primary" onclick="submit_form()">Save changes</button>
				</div>
				</div>
			</div>
		</div>
<!--modal end-->
<script>
$(document).ready(function(){
	$("#change_image").hide();
      
});
function update(id)
{
	$("#add_group_form")[0].reset();
	$('#mat_cat').val(id.getAttribute("cat_name"));
	$('#category_id').val(id.getAttribute("mat_cat_id"));
	$('#is_active').val(id.getAttribute("is_active"));
	$("#change_image").show();
	$(".img").hide();
	$("#add_mat_cat_modal").modal('show');
}
$("#change_image").click(function(){
        $(".img").show();
 });

function delete_data(id)
{
	if(confirm("Do You want to delete!"))
	{
				$.ajax({
					url:"<?php echo base_url('admin/master/delete_material_category');?>",
					type: 'post',
					data:{id:id.getAttribute('mat_cat_id')},
					dataType:'json',
					cache: false,
					clearForm: false,
					success: function (response) 
					{               
						if(response.status=="success")
						{
							$.niftyNoty({
								type: 'success',
								container : 'floating',
								html : '<h4 class="alert-title">Success</h4><p class="alert-message">'+response.msg+'</p>',
								closeBtn : false,
								timer: 4000
							}); 
							$("#Modal_add_edit").modal("hide");	
						}
						else
						{ 
							$.niftyNoty({
								type: 'error',
								container : 'floating',
								html : '<h4 class="alert-title">Fail</h4><p class="alert-message">'+response.msg+'</p>',
								closeBtn : false,
								timer: 4000
							}); 					
						}

					}
				});
				
	}
}
function add_mat_cat()
{
	$("#add_group_form")[0].reset();
	$("#change_image").hide();
	$('.img').show();
	$("#add_mat_cat_modal").modal('show');
}

function submit_form()
{
	var form=$("#add_group_form");
		
		form.validate({
			ignore:[],
			rules: {
				mat_cat: "required",
				is_active: "required"
				
			},
			messages: {
				mat_cat: "<b class='text-danger'>Please Enter Material Category</b>",
				is_active:"<b class='text-danger'>Please select one</b>"
				
			}
		});
		
		if(form.valid())
		{	
				$('#add_group_form').ajaxSubmit({
				url:"<?php echo base_url('admin/master/add_material_category');?>",
				type: 'post',
				dataType:'json',
				cache: false,
				clearForm: false,
				success: function (response) 
				{               
					if(response.status=="success")
					{
						Display_msg(response.msg,response.status);		
						refresh_datatable();
						setTimeout(function(){ window.location.reload(1); }, 3000);
						
						$("#add_mat_cat_modal").modal("hide");		
					}
					else
					{ 
						Display_msg(response.msg,response.status);
						$("#add_mat_cat_modal").modal("hide");						
					}
				}
				});		
			
			
		}

}

</script>
