<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Auction extends CI_Controller {
 
    function __construct()
    {
        parent::__construct(); 
        $this->load->model("auction_model");
    } 
	 
	public function auc_category(){  
		$get_data = $this->auction_model->auction_category();
		echo json_encode($get_data);
		exit();
	}
	
	public function auc_type(){ 
		$get_data = $this->auction_model->auction_type();
		echo json_encode($get_data);
		exit();
	}
	
	public function auc_location(){ 
		$get_data = $this->auction_model->auction_location();
		echo json_encode($get_data);
		exit();
	}
	
	public function getmatlist(){ 
		$get_data = $this->auction_model->getmatdata();
		echo json_encode($get_data);
		exit();
	}
	  
	 
	public function auction_detail(){ 
		$auction_id = $this->uri->segment('3');
		$data['auction_detail'] =  $this->auction_model->getauction_detail($auction_id);
		$data['auction_images'] = $this->auction_model->view_auction_images($data['auction_detail']['ynk_auctionid']);
		$data['auction_lots'] = $this->auction_model->auction_lot_details($data['auction_detail']['ynk_auctionid']);
		$data['auction_documents'] = $this->auction_model->auction_documents($data['auction_details']['ynk_auctionid']);
		echo json_encode($data);
		exit();
	}
	
	public function auction_lot_detail(){ 
		$auction_id = makeSafe($this->input->post('auc_id'));
		$auction_lotid = makeSafe($this->input->post('lot_id')); 
		$data['auction_lots'] = $this->auction_model->auction_lot_details($auction_id,$auction_lotid); 
		echo json_encode($data);
		exit();
	}
	 
	
	public function auction_filter(){ 
		$auction_type = makeSafe($this->input->post('s_atype'));
		$auction_category = makeSafe($this->input->post('s_cat')); 
		$search_location = makeSafe($this->input->post('s_loc'));
		
		if(empty($auction_type)){ $auction_type = ''; } 
		if(empty($auction_category)){ $auction_category = ''; }  
		if(empty($search_location)){ $search_location = ''; }
		
		$get_data = $this->auction_model->getfilter_data($auction_type,$auction_category,$search_location,$mat_id,$vendor_id);
		echo json_encode($get_data);
		exit();
		
	}
	
	 
	public function current_auctions(){
		echo json_encode($this->auction_model->current_auctions());
		exit(); 
	}
	
	public function upcoming_auctions(){
		echo json_encode($this->auction_model->upcoming_auctions());
		exit(); 
	}
	
}
