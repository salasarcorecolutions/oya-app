<?php
use Restserver\Libraries\REST_Controller;
header('Access-Control-Allow-Origin: *');

defined('BASEPATH') OR exit('No direct script access allowed');
if($_SERVER['REQUEST_METHOD'] === 'OPTIONS') {
	header('Access-Control-Allow-Methods: GET, PUT, POST, DELETE, OPTIONS');
	header('Access-Control-Allow-Headers: Content-Type');
	exit;
}


class User extends CI_Controller {
 
	
	
	function __construct() {
        parent::__construct();
	
		$this->load->model('user_model');
    }
	
	/**
	 * VALIDATING USETID AND PASSWORD FOR BIDDER
	*/
	public function check_login()
	{	  

		echo json_encode($this->user_model->auctioneer_login());
		exit(); 
	}
	
	// bidder registration functions
	
	public function country_list()
	{   
		echo json_encode($this->common_model->country_master());
		exit(); 
	} 
	
	public function time_zones($country_id){ 
		echo json_encode($this->common_model->get_time_zones());
		exit(); 
	}
	
	public function state_list($country_id){  
		echo json_encode($this->common_model->get_state_by_country_id($country_id));
		exit(); 
	}
	
	public function city_list($state_id){  
		echo json_encode($this->common_model->get_cities_by_state_id($state_id));
		exit(); 
	} 
	
	public function check_temp_user(){
		echo json_encode($this->user_model->check_temp_buyer());
		exit(); 
	}
	
	public function save_temp_user(){
		$email= makeSafe($this->input->post('email'));
		if(!empty($email)){ 
			$set_otp = rand(100000, 999999);
			$message = "Dear User <br/><br/>Your OTP for registration is ".$set_otp.".Use this code to validate your Email Id.<br/><br/>Regards,<br/>".PRODUCT." Team"; 
			$subject = 'Registration OTP : '; 
		}
		echo json_encode($this->user_model->save_temp_buyer($email, $subject, $message,$set_otp));
		exit(); 
	} 
	
	public function save_reg_data(){
		echo json_encode($this->user_model->save_buyer());
		exit(); 
	} 
	
	public function send_otp_forget_password(){
		
	}
}
