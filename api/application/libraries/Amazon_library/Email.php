<?php
require 'autoload.php';

use Aws\Sqs\SqsClient;
use Aws\Common\Aws;
use Aws\Common\Credentials\Credentials;

class Email {
	Private $queueUrl;
	Private $client;
	Private $uid,$pin,$sender,$domain,$route,$return_val;
	
	
	public function __construct() {
		$this->queueUrl = "https://sqs.us-east-1.amazonaws.com/143131574933/SalasarEmail";
		$credentials = new Credentials('AKIAIDHACCAFC5XFLMNA', '7AcMbw5kWHxxys7Ru5gAxM0nCeXP7Gqka73rj/ob');
		$this->client = SqsClient::factory(array(
			'credentials' => $credentials,
			'region'  => 'us-east-1'
		));
    }

	public function sendMsg($to,$subject,$msgbody,$from="admin@salasarauction.com",$reply_to="scs@salasarauction.com",$vendor="amazon",$from_app="SalasarAuction"){
		$transaction_id = md5(array_shift((explode(".",$_SERVER['HTTP_HOST'])))."_".uniqid()."_".date("dmYHis"));
		$data = array(
			"to"=>$to,
			"subject"=>$subject,
			"msgbody"=>$msgbody,
			"from"=>$from,
			"reply_to"=>$reply_to,
			"vendor"=>$vendor,
			"from_app"=>$from_app,
			"transction_id"=>$transaction_id			
		);
		$this->client->sendMessage(array(
			'QueueUrl'    => $this->queueUrl,
			'MessageBody' => json_encode($data),
		));
		return $transaction_id;
	}
	
	public function getMsg(){
		$msg = array("Body"=>"","MD5"=>"","ReceiptHandle"=>"","MessageId"=>"");
		$result = $this->client->receiveMessage(array(
			'QueueUrl' => $this->queueUrl
		));
		if(sizeOf($result) > 0){		
			$msgArray = $result->getPath('Messages');
			if(isset($msgArray)){
				foreach ($msgArray as $messageBody) {				
					$msg = $messageBody;
				}
			}
		}
		return $msg;
	}
	public function deleteMsg($ReceiptHandle){
		$result = false;
		if($ReceiptHandle){
			$this->client->deleteMessage(array(
				'QueueUrl' => $this->queueUrl,
				'ReceiptHandle'=>$ReceiptHandle,
			));
			$result = true;
		}
		return $result;
	}
	public function getNoofMsg(){
		$noOfMsg = "";
		$result = $this->client->getQueueAttributes(array(
			'QueueUrl' => $this->queueUrl,
			'AttributeNames' => array('ApproximateNumberOfMessages'),
		));
		if(sizeOf($result) > 0){		
			$resArray = $result->getPath('Attributes');
			$noOfMsg = $resArray['ApproximateNumberOfMessages'];
		}
		return $noOfMsg;
	}	
}
