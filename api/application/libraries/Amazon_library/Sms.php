<?php
require_once 'autoload.php';

use Aws\Sqs\SqsClient;
use Aws\Common\Aws;
use Aws\Common\Credentials\Credentials;
class SMS {
	Private $queueUrl;
	Private $client;
	Private $uid,$pin,$sender,$domain,$route,$return_val;
	
	
	public function __construct() 
	{
        // initialise the reference to the codeigniter instance
		
		$this->queueUrl = smsQueueUrl;
		$credentials = new Credentials(smsUsername, smsPassword);
		$this->client = SqsClient::factory(array(
			'credentials' => $credentials,
			'region'  => 'us-east-1'
		));
    }
	
	public function sendMsg($to, $msg, $from="AOSAUC", $from_app="Marketplace", $template_id='', $scheduledTime='', $vendor="hsp")
	{
		$explode_host = explode(".",$_SERVER['HTTP_HOST']);
		$transaction_id = md5(array_shift($explode_host)."_".uniqid()."_".date("dmYHis"));
		$data = array(
			"to"=>$to,
			"message"=>$msg,
			"from"=>$from,
			"from_app"=>$from_app,
			"template_id"=>$template_id,
			"vendor"=>$vendor,
			"transction_id"=>$transaction_id ,
			"time"=>$scheduledTime,
		);
		$this->client->sendMessage(array(
			'QueueUrl'    => $this->queueUrl,
			'MessageBody' => json_encode($data),
		));
		return $transaction_id;
	}
	
	public function getMsg()
	{
		$msg = array("Body"=>"","MD5"=>"","ReceiptHandle"=>"","MessageId"=>"");
		$result = $this->client->receiveMessage(array(
			'QueueUrl' => $this->queueUrl
		));
		if (sizeOf($result) > 0)
		{		
			$msgArray = $result->getPath('Messages');
			if (isset($msgArray))
			{
				foreach ($msgArray as $messageBody)
				{				
					$msg = $messageBody;
				}
			}
		}
		return $msg;
	}
	
	public function deleteMsg($ReceiptHandle)
	{
		$result = false;
		if ($ReceiptHandle)
		{
			$this->client->deleteMessage(array(
				'QueueUrl' => $this->queueUrl,
				'ReceiptHandle'=>$ReceiptHandle,
			));
			$result = true;
		}
		return $result;
	}
	public function getNoofMsg()
	{
		$noOfMsg = "";
		$result = $this->client->getQueueAttributes(array(
			'QueueUrl' => $this->queueUrl,
			'AttributeNames' => array('ApproximateNumberOfMessages'),
		));
		if(sizeOf($result) > 0)
		{		
			$resArray = $result->getPath('Attributes');
			$noOfMsg = $resArray['ApproximateNumberOfMessages'];
		}
		return $noOfMsg;
	}
	
	function getDeliveryReport($transactionId)
	{
		$url="http://salasarsms.com/smslogapi/getStatus";
		
		$transactionId=urlencode($transactionId);
		
		$parameters="id=$transactionId";
		
		$get_url=$url."?".$parameters;
		$ch=curl_init();
		curl_setopt($ch, CURLOPT_POST,0);
		curl_setopt($ch, CURLOPT_URL, $get_url);
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION,1);
		curl_setopt($ch, CURLOPT_HEADER,0);  // DO NOT RETURN HTTP HEADERS
		curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);  // RETURN THE CONTENTS OF THE CALL
		$return_val = curl_exec($ch);		
		return $return_val;
	}
	
	function sms_transaction_log($sms_type,$name,$message,$mobile_no,$flag,$transaction_id,$br_id)
	{
		$ret=false;
		$data = array(
			'sms_type' => $sms_type,
			'name' => $name,
			'message' => $message,
			'mobile_no' => $mobile_no,
			'status' => 'Y',
			'flag' => $flag,
			'transaction_id' => $transaction_id,
			'delivery_status' => 'Sent',
			'date_sent' => date("Y-m-d H:i:s"),
			'br_id' => $br_id,
			'message_count' => ceil(strlen($message)/160)
		);
		$ret=$this->db->insert('sms_transaction_log',$data);
		if($sms_type=="T")
		{
			$ret=$this->db->query("UPDATE sms_counter SET transactional_msg_used = transactional_msg_used+".ceil(strlen($message)/160));
		}else{
			$ret=$this->db->query("UPDATE sms_counter SET promotional_msg_used = promotional_msg_used+".ceil(strlen($message)/160));	
		}
		return $ret;		
	}
	
	 function updateDeliveryStatus($transactionId,$data)
    {  
        $where=array('transaction_id'=>$transactionId);
        if($this->db->update('sms_transaction_log',$data,$where))
        {
            
            if($data['delivery_status']=='Failed')
            { 
                $sql=$this->common->Fetch("sms_counter",'*');
                $smsCounterTable=$sql->result_array();
               
                if($data['sms_type']=='T')
                {
                    $transactional_msg_used=$smsCounterTable[0]['transactional_msg_used']-$data['message_count'];                   
                    $data1=array('transactional_msg_used'=>$transactional_msg_used);
                    $where1=array('sms_counter_id'=>$smsCounterTable[0]['sms_counter_id']);
                    $resultcouner=$this->db->update('sms_counter',$data1,$where1);                    
                    if($resultcouner)
                    {
                        //echo "Success";
                    }else
                    {
                        // echo "Error1";
                    }
                    
                }else
                {
                    $promotional_msg_used=$smsCounterTable[0]['promotional_msg_used']-$data['message_count'];                    
                    $data1=array('promotional_msg_used'=>$promotional_msg_used);
                    $where1=array('sms_counter_id'=>$smsCounterTable[0]['sms_counter_id']);
                    $resultcouner=$this->db->update('sms_counter',$data1,$where1);
                    if($resultcouner)
                    {
                       // echo "Success";
                    }else
                    {
                        // echo "Error2";
                    }
                }
                               
            }
			else
			{
				// echo 'No Update';
			}
        
        return;
            
        }
		else
		{
            return json_encode(array('status'=>'fail','msg'=>"Problem in data updating"));
        }
}
}
