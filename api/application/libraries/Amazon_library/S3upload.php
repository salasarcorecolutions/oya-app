<?php

/**
 * @author: Tejas Patel
 * Created Date: 28th March 2015
 * @desc:  This Class for upload files on amazon server.

	Example Code Upload
	$upload = $s3->uploadfile($_FILES['userfile']['name'],'userfile');
	
	Delete File
	$delete = $s3->deletefile($_FILES['userfile']['name']);
	
	Check file
	if($s3->checkFile("rank5.png","client_docs/abc cmp")){
		echo "Exist";
	}else{
		echo "Not Exist";
	}
*/

require_once 'autoload.php';

use Aws\Common\Aws;
use Aws\Common\Credentials\Credentials;
use Aws\S3\S3Client;

class S3upload{

	Private $s3;
	Private $bucket;
	Private $bucket_files;
	Private $local_dir;

	public function __construct() 
		{
			$credentials = new Credentials(s3BucketUsername, s3BucketPassword);

			$this->s3 = S3Client::factory(array(
				'credentials' => $credentials,
				'region'  => 'eu-central-1'
			));
			$this->bucket = s3BucketName;
			$this->local_dir = s3BucketTestDir;
			$bucket_files = array();
		}

	public function uploadfile($fileName,$controlName="",$dirName="",$filepath=""){
		$dirName = $this->local_dir.$dirName;
		//$fileName = strtolower($fileName);
		if($filepath == ""){		
			$upload = $this->s3->upload($this->bucket, $dirName."/".$fileName, fopen($_FILES[$controlName]['tmp_name'], 'rb'));
		}else{
			$upload = $this->s3->upload($this->bucket, $dirName."/".$fileName, fopen($filepath, 'rb'));
		}
		$status = $upload->get('ObjectURL');
		if(!empty($status)){
			return true;
		}else{
			return false;
		}
	}

	public function deletefile($fileName,$dirName=""){
		$dirName = $this->local_dir.$dirName;
		//$fileName = strtolower($fileName);
		$delete = $this->s3->deleteObject(array(
			'Bucket' => $this->bucket,
			'Key'    => $dirName."/".$fileName
		));
		$status = $delete->get('RequestId');
		if(!empty($status)){			
			return true;
		}else{
			return false;
		}
	}

	public function checkFile($fileName,$dirName=""){
		$dirName = $dirName;
		$returnArray = array();
		try{
			if(!empty($fileName) && !is_numeric($fileName)){
				$returnArray[0] = $this->getUrl($fileName,$dirName);
				$returnArray[1] = "Click To Open";
			}else{
				return false;
			}
			return $returnArray;
		}catch(Exception $e){
			return false;
		}
	}
	
	public function getUrl($fileName,$dirName=""){
		$dirName = $this->local_dir.$dirName;
		return "http://".s3BucketName."/".$dirName."/".$fileName;
	}
	
	public function getUrl2($fileName,$dirName=""){
		//$fileName = strtolower($fileName);
		if(!empty($dirName)){
			$dirName = $this->local_dir.$dirName;
			return "download.php?q=".$dirName."/".$fileName;
		}else{
			return "download.php?q=".$fileName;
		}
	}
	
	public function getAllFiles(){
		//$return_array = array();
		try{
			$response = $this->s3->getListObjectsIterator(array(
				'Bucket' => $this->bucket
			));
			foreach ($response as $object) {
				$this->bucket_files[$object['Key']] = $object['Size'];			
			}			
		}catch(Exception $e){
		}		
	}
	
	public function getAllFilesFromFolder($dirName){
		$dirName = $this->local_dir.$dirName;
		$return_array = array();
		try{
			$response = $this->s3->getListObjectsIterator(array(
				'Bucket' => $this->bucket,
				'Prefix' => $dirName
			));
			foreach ($response as $object) {
				$return_array[] = $object['Key'];//$object['Size'];			
			}	
			return $return_array;
		}catch(Exception $e){
		}		
	}
	
	public function getAllFilesFromFolderWithKey($dirName){
		$dirName = $this->local_dir.$dirName;
		$return_array = array();
		try{
			$response = $this->s3->getListObjectsIterator(array(
				'Bucket' => $this->bucket,
				'Prefix' => $dirName
			));
			foreach ($response as $object) {
				$file_path = str_replace($dirName,"",$object['Key']);
				$parts = explode('/', $file_path);
				$last = array_pop($parts);
				$parts = array(implode('/', $parts), $last);
				$return_array[$parts[0]][] = $parts[1];//$object['Size'];			
			}	
			return $return_array;
		}catch(Exception $e){
		}		
	}
	
	public function donwloadFile($fileName){
		$result = $this->s3->getObject(array(
			'Bucket' => $this->bucket,
			'Key'    => $fileName
		));
		return $result;
	}
}
