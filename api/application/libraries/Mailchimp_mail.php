<?php
require(AMAZONLIB.'/mailchimp/autoload.php');

class Mailchimp_mail {
	Private $apikey;
	Private $listid;
	Private $camp_id;
	Private $MailChimp_obj;	
	Private $template_id;
	
	public function __construct() {
        // initialise the reference to the codeigniter instance
		$this->apikey = "6d157e908dfddf3ec3b808ea44a253a8-us12";
		$this->listid = "db75d9d1ef";
		$this->camp_id = "ae93461681";
		$this->template_id= "32553";
		$this->MailChimp_obj = new Mailchimp($this->apikey);
    }
	
	public function addUser($user_name,$email_id){
		$merge_vars = array('FNAME'=>$user_name);
		$result = $this->MailChimp_obj->lists->subscribe($this->listid,array('email'=>$email_id),$merge_vars,'html',false,true,false,false);		
		return true;
	}
	
	public function listUser(){
		$result = $this->MailChimp_obj->lists->members($this->listid);
		return $result;
	}
	
	public function sendMail($html){
		if(!empty($html)){
			// Set value for updating template
			$value = array();
			$value['html'] = $html;
			
			// Update Template with provided html
			$result = $this->MailChimp_obj->templates->update($this->template_id,$value);
			
			// Create Replicate Campaigns
			$result = $this->MailChimp_obj->campaigns->replicate($this->camp_id);
			
			if(!empty($result['id'])){
				$result = $this->MailChimp_obj->campaigns->send($result['id']);
				return true;
			}else{
				return false;
			}
		}else{
			return false;
		}		
	}
}
