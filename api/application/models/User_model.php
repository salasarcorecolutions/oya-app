<?php

class User_model extends CI_Model
{
   
   public function __construct()
    {        
	  parent::__construct();
	    
    }
   
	 
    public function auctioneer_login()
    {
        $rawData = file_get_contents("php://input");
        $postedValue = json_decode($rawData,true);
        $user = $postedValue['username'];
        $pass = $postedValue['password'];
        $current_date = (new DateTime())->format('Y-m-d H:i:s');
        //To fetch the details of the user if Username and Password exists.
        $sql = 'SELECT vc.c_name,vc.role,vc.department_id, user_tz, password,vc.id, active_date_to,
                    vc.uname, vc.vendor_id, v.vendor_name, vc.registration_type, admin_approval,
                    primary_email_verify,primary_mobile_verify,vc.user_type, v.logo
				FROM vendor_contacts vc
				LEFT JOIN vendor v ON v.vendor_id = vc.vendor_id
				WHERE vc.uname = ?';
        $result = $this->db->query($sql, array($user));
        $permission_array = array();

        if ($result->num_rows() >= 1) {
            $row = $result->row_array();
            if (password_verify($pass, $row['password'])) {
                //To fetch out the page IDs for which the vendor has permissions.
                $sql_get_page_id = 'SELECT distinct permission_id
                                    FROM roles_permissions rp
                                    LEFT JOIN role_user ru on rp.role_id = ru.role_id
                                    WHERE ru.user_id =' . $row['id'];
                $result = $this->db->query($sql_get_page_id)->result_array();
                //Grouping page_ids and storing data in session to check page permission.

                if (!empty($result)) {
                    $permission_id  = 0;
                    foreach ($result as $single_row) {
                        $permission_id .= ',' . $single_row['permission_id'];
                    }
                }

                if (!empty($permission_id)) {
                    $sql_page_permissions = '   SELECT mshp.permission_code
                                                FROM roles_permissions rp
                                                LEFT JOIN module_sub_head_permission_sets mshp ON mshp.permission_id = rp.permission_id
                                                WHERE rp.permission_id IN (' . $permission_id . ')';
                    $result2 = $this->db->query($sql_page_permissions)->result_array();

                    if (!empty($result2)) {
                        foreach ($result2 as $single_permission) {
                            $permission_array[] = $single_permission['permission_code'];
                        }
                    }
                }
                if ($row['admin_approval'] == 1) {
                    if ($row['active_date_to'] < $current_date) {
                        $response = array("status" => false, "msg" => "Your account is expired");
                        return $response;
                    }
                    $company_plant = $this->get_company_plant_id($row['id']);
                    if ($row['user_type'] == 1) {
                        $this->session->set_userdata('log_type', 'A');
                    } else {
                        $this->session->set_userdata('log_type', 'V');
                    }
                    if ($row['role'] == 0) {
                        $insert_arr = array(
                            'vendor_id' => $row['vendor_id'],
                            'officials_role' => 'admin'
                        );
                        $this->db->insert('vendor_officials_role', $insert_arr);
                        $roleid = $this->db->insert_id();
                        if (!empty($roleid)) {
                            $this->db->where('vendor_id', $row['vendor_id']);
                            $update_arr = array(
                                'role' => $roleid
                            );
                            $this->db->update('vendor_contacts', $update_arr);
                        }
                        $_SESSION['officials_role'] = 'admin';
                    } else {
                        $this->db->select('officials_role');
                        $roleid = $this->db->get('vendor_officials_role')->row_array()['officials_role'];
                        $_SESSION['officials_role'] = $roleid;
                    }
                    if (empty($row['user_tz'])) {
                        $row['user_tz'] = 'UTC';
                    }
                    $vendor_logo = array();
                    if (!empty($row['logo'])) {
                        $this->load->library('Amazon_library/S3upload');
                        $s3 = new S3upload();
                        $vendor_logo = $s3->checkFile($row['vendor_id'] . "." . $row['logo'], "images/vendor_logo");
                    }
                    $sqlRoles = "select role_id from role_user where user_id = " . $row['id'];
                    $session_data = array(
                        'username' => $row['uname'],
                        'user_id' => $row['id'],
                        'user_name' => $row['c_name'],
                        'user_type' => 'vendor',
                        'uid' => $row['id'],
                        'vendor_id' => $row['vendor_id'],
                        'user_roles' => $this->db->query($sqlRoles)->result_array(),
                        'registration_type' => $row['registration_type'],
                        'vendor_name' => $row['vendor_name'],
                        'company_plant' => $company_plant,
                        'active_till' => $row['active_date_to'],
                        'user_tz' => $row['user_tz'],
                        'vendor_logo' => !empty($vendor_logo[0]) ? $vendor_logo[0] : "",
                        'page_permission' => $permission_array,
                        'primary_email_verify' => $row['primary_email_verify'],
                        'primary_mobile_verify' => $row['primary_mobile_verify'],
                        'vid' => $row['vendor_id'],
                        'vname' => $row['c_name'],
                    );
                  
                    /* Check Mobile & Email Verification */
                    if ($row['primary_email_verify'] == 0) {
                        $response = array('status' => true, 'redirect_url' => base_url() . 'user/verify_auctioneer_email');
                    } else {
                        $response = array("status" => true, 'data' =>$session_data);
                    }
                    return $response;
                } else {

                    $response = array("status" => false, "msg" => "Admin approval is pending");
                    return $response;
                }
            } else {
                $response = array("status" => false, "msg" => "Invalid Credentials");
                return $response;
            }
        } else {
            $sql = "SELECT vc.c_name,vc.role,vc.department_id,vc.c_email,vc.c_contacts, user_tz, password,vc.id, active_date_to,vc.uname, vc.vendor_id, v.vendor_name, vc.registration_type, admin_approval,primary_email_verify,primary_mobile_verify,vc.user_type
					FROM temp_vendor_contacts vc
					LEFT JOIN temp_vendor v ON v.vendor_id = vc.vendor_id
					WHERE vc.uname = ?";
            $results = $this->db->query($sql, array($user));
            if ($results->num_rows() == 1) {
                $temp_reg = $results->row_array();
                $_SESSION['temp_id'] = $temp_reg['vendor_id'];
                $_SESSION['primary_mail'] = $temp_reg['c_email'];
                $_SESSION['primary_email_verify'] = 0;
                $_SESSION['primary_mobile_verify'] = $temp_reg['c_contacts'];
                $_SESSION['type'] = 'vendor';
                return array('status' => true, 'message' => 'Registration Pending', 'redirect_url' => base_url('user/verify_vemail'));
            } else {
                $response = array("status" => false, "msg" => "Invalid Credentials");
                return $response;
            }
        }
    }  
    /**
     * it is used in auctioneer login
     */
    public function get_company_plant_id($user_id)
    {
        $this->db->select('company_id, group_concat(plant_id) as plant_id');
        $this->db->from('vendor_company_relation vcr');
        $this->db->where('vendor_contact_id', $user_id);
        $this->db->where('admin_approval', 1);
        $this->db->group_by('company_id');
        $company_plant_ids = $this->db->get()->result_array();
        $id = array();
        foreach ($company_plant_ids as $value) {
            $id[$value['company_id']] = $value['plant_id'];
        }
        return $id;
    }
	public function check_temp_buyer(){
        $primary_email  = makeSafe($this->input->post('email'));
        $compname  = makeSafe($this->input->post('company_name'));
        
        //check company or email 
        $query = "SELECT count(*) as cnt FROM temp_bidders WHERE REPLACE(compname, ' ', '') = '".$compname."' OR email ='".makeSafe($primary_email)."'";
        $res = $this->db->query($query)->row_array($query);

        if ($res['cnt'] > 1)
        {
            $data['msg'] = "Duplicate Registration";
            $data['success'] = false;
            return $data;
            exit;
        }

        //check user exists or not
        $data=$this->check_buyer_user(makeSafe($this->input->post('username')));
        if($data['status']=="success")
        {
            $data['msg'] = "Duplicate User Id";
            $data['success'] = false;
            return $data;
            exit;
        } else { 
            $data['msg'] = "No duplicate data";
            $data['success'] = true;
            return $data;
            exit;
        }
    }
	
	public function check_buyer_user($user_id)
    {
        if ( ! empty($user_id))
        {
           
            $chk_user = "SELECT COUNT(*) AS cnt FROM bidders WHERE userid= '".makeSafe($user_id)."'";
            $con_user_check = $this->db->query($chk_user)->row_array();
            if ($con_user_check['cnt'] ==0)
            {
                $chk_user = "SELECT COUNT(*) AS cnt FROM temp_bidders WHERE userid= '".makeSafe($user_id)."'";
                $con_user_check = $this->db->query($chk_user)->row_array();
            }
            if ($con_user_check['cnt']>=1)
            {
                //status: used in reg temp check
                return array('status'=>'success','message'=>'User ID Alread Exist');
            }
            else
            {
                return array('status'=>'fail');
            }
        }
        else
        {
            return array('status'=>'fail');
        } 
    }
	
	
	//save the temp buyer
	public function save_temp_buyer($email, $subject, $message ,$otp)
    { 

        //--- saving the temp data after Accepting terms and conditions  ---// 
        
		$compname= makeSafe($this->input->post('company_name'));
		$pass=makeSafe($this->input->post('pass')); 
		
		$insert_bidder_data = array(
            'compname'=>ucwords($compname),
            'conperson'=> makeSafe($this->input->post('first_name')),
            'adddress'=> makeSafe($this->input->post('company_address')),
            'city'=> makeSafe($this->input->post('city_id')),
            'pin'=> makeSafe($this->input->post('zip')),
            'state'=> makeSafe($this->input->post('state_id')), 
            'country'=> makeSafe($this->input->post('country_id')),
            'state_other'=> makeSafe($this->input->post('b_state_other')),
            'city_other'=> makeSafe($this->input->post('b_city_other')),
            'mob'=> makeSafe($this->input->post('phone')),
            'email'=> makeSafe($this->input->post('email')),
            'userid'=> makeSafe($this->input->post('username')),
            'password'=>password_hash($pass, PASSWORD_DEFAULT),
            'regdate'=>date("Y-m-d H:i:s"),	
            'user_tz'=>trim($this->input->post('zone_id')!="" ? $this->input->post('zone_id') : 'UTC'),	
        );

        if ($this->db->insert('temp_bidders',$insert_bidder_data))
        {
			$send_email= $this->common_model->send_common_email($email, $subject, $message); 
			if($send_email){
				//echo "mail send";
				$data['msg'] = "mail send";
				$data['success'] = true; 
				$data['temp_id'] = $this->db->insert_id(); 
				$data['otp']= $otp;
				return $data;				
			}
			else
			{
				$data['msg'] = "mail not send";
				$data['success'] = false;  
				return $data;	
			} 
            
        }
        else
        {
            $data['msg'] = "Please Try Again After Some Time";
            $data['success'] = false;
            return $data; 
        }  
    }
	
	 
	public function save_buyer(){
		$temp_id=makeSafe($this->input->post('temp_id')); 
		$update_otp = array('primary_email_verify'=>'1');
		$this->db->where('id',$temp_id);
		$this->db->update('temp_bidders',$update_otp);
		$sql = "SELECT compname,conperson,tb.adddress,city,tb.state,country,mob,email,userid,tb.password,initcode,user_tz,pin,regdate FROM temp_bidders tb WHERE id =".$temp_id;
		$res = $this->db->query($sql)->row_array();
		if ( ! empty($res))
		{
			$query = "SELECT count(*) as cnt FROM bidders WHERE compname = '".$res['compname']."' and email ='".$res['email']."'";
			$cnt = $this->db->query($query)->row_array();
			if ( $cnt['cnt'] == 0)
			{
				unset($res['id']);
				$res['primary_email_verify'] = 1;
				$res['activated'] = 1;
				if ($this->db->insert('bidders',$res))
				{
                    $this->db->where('id',$temp_id);
                    $this->db->delete('temp_bidders');
                    if($this->db->affected_rows() == true)
                    {
                        $data['msg'] = "Registred successfully";
						$data['success'] = true; 
                    }
                    else {
                        $data['msg'] = "Error while deleting temp_bidder data";
						$data['success'] = false;
						
                    }
				}
				else
				{
					$data['msg'] = "Error while Register";
					$data['success'] = false; 
				}
				return $data; 
			}
			else
			{
				$data['msg'] = "Please Try Again After Some Time";
				$data['success'] = false;
				return $data; 
			}
			
		} 
	}
   
}
