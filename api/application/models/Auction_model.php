<?php

class Auction_model extends CI_Model
{
   
   public function __construct()
    {        
	  parent::__construct();
	    
    } 
	
	//----Fetch auction category---//
    public function auction_category()
    {
        $query = "SELECT mmc.id,mmc.cat_name FROM material_master_category mmc";
        $result = $this->db->query($query)->result_array();
        $mat_cat = array();
       
        if ( ! empty($result))
        {
            $total = 0;
            foreach($result as $row)
            {

               $sql = "SELECT id,mat_name FROM material_master WHERE cat_id IN (".$row['id'].")";
               $resrows = $this->db->query($sql)->result_array();
               $mat = array();
               if ( ! empty($resrows))
			   {
                   
                   foreach($resrows as $rows)
                   {
                        
                        $sql= "SELECT COUNT(ynk_auctionid) as cnt FROM ynk_auction WHERE  find_in_set (".$rows['id'].",mat_id) > 0 and published = 'Y' ";
                        $resrows = $this->db->query($sql)->row_array();
                        $ynk_cnt = $resrows['cnt'];

                        $sql= "SELECT COUNT(tid) as cnt FROM tender WHERE find_in_set (".$rows['id'].",mat_id) > 0 and published = 'Y' ";
                        $resrows = $this->db->query($sql)->row_array();
                        $tender_cnt = $resrows['cnt'];

                        $total =$ynk_cnt + $tender_cnt;
                        
                        if ($total > 0)
                        {
                            $mat[$rows['mat_name']]['mat_total'] = $total;
                            $mat[$rows['mat_name']]['cat_id'] = $row['id'];

                        } 
                   }
               }
            
               if ( ! empty($mat))
               {
                 $mat_cat[$row['cat_name']] = $mat;
               }
               
            }
        }
        
        return $mat_cat;
    }
	
	 //---Fetch auction type---//
    public function auction_type()
    { 
        $sql = "SELECT COUNT(ya.auctiontype) as cnt ,ya.auctiontype AS type FROM ynk_auction ya where ya.published = 'Y' GROUP BY ya.auctiontype ";
        $result[] = $this->db->query($sql)->result_array();

        $sql2 = "SELECT COUNT(t.auctiontype) as cnt ,t.auctiontype AS type FROM tender t where t.published = 'Y' GROUP BY t.auctiontype ";
        $result[] = $this->db->query($sql2)->result_array();
        $auc = array();
        if ( ! empty($result))
        {
            foreach($result as $row)
            {
                
                    foreach($row as $resrows)
                    {
                        if( ! empty($resrows['type']))
                        {
                            $auc[$resrows['type']] = $resrows['cnt'];
                        }
                        
                    }
                
            }
        }
      
        return $auc;

    }
	
    //----Fetch auction location----//
    public function auction_location()
    {
        $query="SELECT COUNT(ya.location) AS cnt,com.name,com.countries_iso_code_2 FROM ynk_auction ya 
                LEFT JOIN city_master cm ON cm.id = ya.location 
                LEFT JOIN state_master sm ON sm.id = cm.state_id 
                LEFT JOIN country_master com ON com.id = sm.country_id 
                GROUP BY com.id";
        $result[] = $this->db->query($query)->result_array();


        $sql = "SELECT COUNT(t.location) AS cnt,com.name,com.id,com.countries_iso_code_2 FROM tender t 
        LEFT JOIN city_master cm ON cm.id = t.location 
        LEFT JOIN state_master sm ON sm.id = cm.state_id 
        LEFT JOIN country_master com ON com.id = sm.country_id 
        GROUP BY com.id";
        $result[] = $this->db->query($sql)->result_array();
        
        $loc = array();
        $location = array();
        if ( ! empty($result))
        {
            foreach($result as $row)
            {
                
                    foreach($row as $resrows)
                    {
                        if( ! empty($resrows['name']))
                        {
                            $loc[$resrows['name']]['name'][] = $resrows['cnt'];
                            $loc[$resrows['name']]['country_id'] = $resrows['countries_iso_code_2'];

                        }
                        
                    }
                
            }
        }
      
        if ( ! empty($loc))
        {
            $total = 0;
            $city ='';
            foreach($loc as $key=>$val)
            {
                if ($val)
                {
                    foreach($val['name'] as $k=>$v)
                    {
                        if ($city != $key)
                        {
                            $total = 0;
                        }
                        $total = $total+$v;
                        $location[$key]['count'] = $total;
                        $location[$key]['country'] = $loc[$key]['country_id'];

                        $city = $key;
                    }
                }
               
            }
        }
       
        return $location;

    }
    //---Fetch auction material---//
    public function auction_material()
    {
        $query = "SELECT id,mat_name FROM material_master";
        $result = $this->db->query($query)->result_array();
        $mat = array();
       
        if ( ! empty($result))
        { 
            foreach($result as $rows)
            { 
				$sql= "SELECT COUNT(ynk_auctionid) as cnt FROM ynk_auction WHERE mat_id IN (".$rows['id'].") and published ='Y'";
				$resrows = $this->db->query($sql)->row_array();
				$ynk_cnt = $resrows['cnt'];

				$sql= "SELECT COUNT(tid) as cnt FROM tender WHERE mat_id IN (".$rows['id'].") and published = 'Y'";
				$resrows = $this->db->query($sql)->row_array();
				$tender_cnt = $resrows['cnt'];

				$total =$ynk_cnt + $tender_cnt;
				
				if ($total > 0)
				{
					$mat[$rows['mat_name']]['count'] = $total;
					$mat[$rows['mat_name']]['mat_id'] = $rows['id']; 
				}  
            }
        }
       
        return $mat;
    }

    //---Fetch auction vendor---//
    public function auction_vendor()
    {
        $sql = "SELECT if(var.vendor_type=0, (select vendor_name from vendor where vendor_id =var.vendor_id), (select vendor_name 
                from vendor where vendor_id = var.vendor_company_id)) as vendor_name,if(var.vendor_type=0, (select vendor_id from vendor where vendor_id =var.vendor_id), (select vendor_id 
                from vendor where vendor_id = var.vendor_company_id)) as vendor_id,
                COUNT(*) as cnt 
                FROM vendor_auction_relation var WHERE (var.auction_type = 4 OR var.auction_type = 5) AND is_active = 1 GROUP BY vendor_id";
        $result = $this->db->query($sql)->result_array();
        return $result;
    }
   
	//------filter data end------//
	
    
	public function getauction_detail($saleno)
	{ 
		$sql = "SELECT y.ynk_auctionid,y.ynk_saleno,y.location,y.cmd,y.start_date,y.end_date,y.stime,y.etime,y.mat_id,y.particulars,com.name,var.auction_type,
        y.auctiontype,if(var.vendor_type=0, (select vendor_name from vendor where vendor_id =var.vendor_id), (select vendor_name from vendor where vendor_id = var.vendor_company_id)) as vendor_name,
        if(var.vendor_type=0, (select vendor_id from vendor where vendor_id =var.vendor_id), (select vendor_id from vendor where vendor_id = var.vendor_company_id)) as vendor_id
        FROM ynk_auction y 
        LEFT JOIN city_master cm ON cm.id = y.location
        LEFT JOIN state_master sm ON sm.id = cm.state_id
        LEFT JOIN country_master com ON com.id = sm.country_id
        LEFT JOIN vendor_auction_relation var ON var.auction_id = y.ynk_auctionid WHERE ynk_saleno = '".$saleno."'";
		
		$result = $this->db->query($sql)->row_array();
		/*if(!empty($result)){ 
			$data['auctionDetail'] = $result;
			$data['success'] = true;
		}else {
			$data['message'] = "no data found";
			$data['success'] = false;
		}	*/		
		
		return $result; 
	}
	
	public function view_auction_images($ynk_auctionid){
		$sql = "SELECT DISTINCT ai.img FROM ynk_auction y 
                LEFT JOIN auction_images ai ON ai.auc_id = y.ynk_auctionid and auc_type = 'y' 
                WHERE y.ynk_auctionid =".$ynk_auctionid;
        $result  = $this->db->query($sql)->result_array();
        return $result; 
	}
	
	public function auction_lot_details($auctionid,$lot_id='')
    {
        $query = "SELECT ynk_auctionid,ynk_lotno,vendorlotno,product,product_details,lot_open_date,lot_close_date,
        stime,etime,totalqty,totunit,bidqty,bidunit,currency,cmd,plant,part_bid,min_part_bid
         FROM ynk_auction_lots  WHERE ynk_auctionid =".$auctionid;
         $data = array();
        if ( ! empty($lot_id))
        {
            $query.=" && ynk_lotno =".$lot_id;
            $result = $this->db->query($query)->row_array();
            if ( ! empty($result) && ! empty($this->session->userdata('user_tz')))
            {
                
                $result['lot_open_date'] = usertz_to_servertz($result['lot_open_date'],$this->session->userdata('user_tz'),'d-m-Y H:i:s');
                $result['lot_close_date'] = usertz_to_servertz($result['lot_close_date'],$this->session->userdata('user_tz'),'d-m-Y H:i:s');

            }
            $sql= "SELECT * FROM auction_lot_images WHERE auc_id ='".$auctionid."' AND auc_type = 'y' AND lot_id ='".$lot_id."' ";
            $result['auc_img'] = $this->db->query($sql)->result_array();
            return $result;
        }
        else
        {
            $result = $this->db->query($query)->result_array();
            if ( ! empty($result))
            {
                foreach($result as $row)
                {
                    if ( ! empty($this->session->userdata('user_tz')))
                    {
                        $row['lot_open_date'] = usertz_to_servertz($row['lot_open_date'],$this->session->userdata('user_tz'),'d-m-Y H:i:s');
                        $row['lot_close_date'] = usertz_to_servertz($row['lot_close_date'],$this->session->userdata('user_tz'),'d-m-Y H:i:s');
                    }
                    $data[] = $row;
                }
            }
            return $data;
        } 
    }
    
	public function auction_documents($auction_id)
    {
        $doc = array();
        $this->db->where('ynk_auction_id',$auction_id);
        $result = $this->db->get('ynk_auction_docs')->result_array(); 
        $i=0;
        $doc_array=array();
        $data = array();
        if( ! empty($result))
        {
            $s3 = new S3upload();
            foreach($result as $single_doc)
            { 
                $terms = $s3->getUrl('y_'.$single_doc['ynk_auction_id']."_".$single_doc['ynk_doc_id'].".".$single_doc['image_mime'], "ynk_auc_docs");
                if( ! empty($terms))
                {
                    $doc_array[$i]['doc_path'] = $terms;
                    
                }
                $doc_array[$i]['doc_name'] = $single_doc['doc_name'];
                
                $i=$i+1;
            }
        }
        $data['docs']=$doc_array;
        return $data;
    }
	
    public function getfilter_data($auc_type='',$category_id='',$country='',$mat_id='',$vendor_id='')
	{
		if ( ! empty($category_id))
        {
            $sql = "SELECT id FROM material_master WHERE cat_id =".$category_id;
            $result = $this->db->query($sql)->result_array();
        }
        if ( ! empty($category_id))
        {
            $sql="SELECT mm.id FROM material_master mm LEFT JOIN material_master_category mmc ON mm.cat_id = mmc.id WHERE mmc.id ='".$category_id."' ";
            $res = $this->db->query($sql)->result_array();
            $mat_id = implode(', ', array_map(function ($entry) {
                return $entry['id'];
              }, $res));
        }
        
       
        //Query for Ynk Auction
        $query = "
        SELECT DISTINCT bvr.bidder_id as registered,bvr.admin_approval,bvr.vendor_approval,y.ynk_saleno as saleno,
                group_concat(distinct img) as auc_img,y.auction_visibility,GROUP_CONCAT(distinct mm.mat_name ORDER BY mm.id) as mat_name,y.mat_id,
					DATE_FORMAT(y.start_date,'%d-%m-%Y') as start_date,y.start_date as sdate,
					DATE_FORMAT(y.stime ,'%H:%i:%s') as stime,
					DATE_FORMAT(y.end_date,'%d-%m-%Y') as end_date,
					DATE_FORMAT(y.etime ,'%H:%i:%s') as etime,
					y.particulars,
					v.vendor_name,
					var.auction_type,
					y.auctiontype,
                    y.location,
                    y.cmd,
                    MD5(y.ynk_saleno) as encrypt_saleno,
				(
						IF(DATE_FORMAT(y.start_date,'%Y-%m-%d') < DATE_FORMAT(CURDATE(),'%Y-%m-%d'), 
							(
								IF(DATE_FORMAT(y.end_date,'%Y-%m-%d') > DATE_FORMAT(CURDATE(),'%Y-%m-%d'), 
									'1', 
								(
									IF(DATE_FORMAT(y.end_date,'%Y-%m-%d') = DATE_FORMAT(CURDATE(),'%Y-%m-%d'), 
										(
											IF(DATE_FORMAT(y.etime,'%H:%i:%s') > DATE_FORMAT(now(),'%H:%i:%s'), 
												'1', 
											'3'
											)
										),
									'3'
									)
								)
								)
							),
							(
								IF(DATE_FORMAT(y.end_date,'%Y-%m-%d') = DATE_FORMAT(CURDATE(),'%Y-%m-%d'),
									(
										IF(DATE_FORMAT(y.stime,'%H:%i:%s') <= DATE_FORMAT(now(),'%H:%i:%s'),
											(
												IF(DATE_FORMAT(y.end_date,'%Y-%m-%d') > DATE_FORMAT(CURDATE(),'%Y-%m-%d'),
													('1'),
												(
													IF(DATE_FORMAT(y.end_date,'%Y-%m-%d') = DATE_FORMAT(CURDATE(),'%Y-%m-%d'),
														(
															IF(DATE_FORMAT(y.etime,'%H:%i:%s') > DATE_FORMAT(now(),'%H:%i:%s'),
																('1'),
															'3'
															)
														),
													'3'
													)
												)
												)
											),
											IF(DATE_FORMAT(y.stime,'%H:%i:%s') > DATE_FORMAT(now(),'%H:%i:%s'),
												'2',
											1
											)
										)
									),
								IF(DATE_FORMAT(y.start_date,'%Y-%m-%d') > DATE_FORMAT(CURDATE(),'%Y-%m-%d'),
									'2',
								'3'
								)
								)
							)
						)
					)as status,
				v.vendor_id,
                y.ynk_auctionid ,if(var.vendor_type=0, (select vendor_name from vendor where vendor_id =var.vendor_id), (select vendor_name from vendor where vendor_id = var.vendor_company_id)) as vendor_name,
                if(var.vendor_type=0, (select vendor_id from vendor where vendor_id =var.vendor_id), (select vendor_id from vendor where vendor_id = var.vendor_company_id)) as ven_id
                FROM vendor_auction_relation var 
				LEFT JOIN ynk_auction y ON y.ynk_auctionid = var.auction_id and auction_type = 4
				LEFT JOIN auction_images ai ON ai.auc_id = y.ynk_auctionid and auc_type = 'y'
				LEFT JOIN material_master mm ON FIND_IN_SET(mm.id, y.mat_id) > 0
				LEFT JOIN bidder_vendor_relation bvr ON bvr.vendor_id = var.company_id
                LEFT JOIN vendor v ON v.vendor_id = var.vendor_company_id 
                LEFT JOIN city_master cm ON cm.id = y.location
                LEFT JOIN state_master sm ON sm.id = cm.state_id
                LEFT JOIN country_master com ON com.id = sm.country_id
                WHERE 1=1  AND y.published = 'Y'";
                if ( ! empty($country))
                {
                    $query.=" &&  com.countries_iso_code_2 ='".$country."'";
                }
                if ( ! empty($mat_id))
                {
                    $query.=" && y.mat_id IN (".$mat_id.")";
                }
                if ( ! empty($auc_type))
                {
                    $query.=" && y.auctiontype = '".$auc_type."'";
                }
                if ( ! empty($vendor_id))
                {
                    $query.=" && (var.vendor_id ='".$vendor_id."' OR var.vendor_company_id='".$vendor_id."')";
                }
                 
                $query.=" GROUP BY y.ynk_auctionid"; 	   
                $query.=" UNION ";

                // query for tender_cnt
                $query.="SELECT DISTINCT bvr.bidder_id as registered,bvr.admin_approval,bvr.vendor_approval,t.tender_id as saleno,group_concat(distinct img) as img,t.auction_visibility,GROUP_CONCAT(distinct mm.mat_name ORDER BY mm.id) as mat_name,t.mat_id,
				DATE_FORMAT(t.topen_dt,'%d-%m-%Y') as start_date,t.topen_dt as sdate,
				DATE_FORMAT(t.topen_time ,'%H:%i:%s') as stime,
				DATE_FORMAT(t.tclose_dt,'%d-%m-%Y') as end_date,
				DATE_FORMAT(t.tclose_time ,'%H:%i:%s') as etime, 
				t.particulars, 
				v.vendor_name,
				var.auction_type,
				t.tender_type as auctiontype,
				t.location,
                t.cmd,
                MD5(t.tender_id) as encrypt_saleno,
				(
						IF(DATE_FORMAT(t.topen_dt,'%Y-%m-%d') < DATE_FORMAT(CURDATE(),'%Y-%m-%d'), 
							(
								IF(DATE_FORMAT(t.tclose_dt,'%Y-%m-%d') > DATE_FORMAT(CURDATE(),'%Y-%m-%d'), 
									'1', 
								(
									IF(DATE_FORMAT(t.tclose_dt,'%Y-%m-%d') = DATE_FORMAT(CURDATE(),'%Y-%m-%d'), 
										(
											IF(DATE_FORMAT(t.tclose_time,'%H:%i:%s') > DATE_FORMAT(now(),'%H:%i:%s'), 
												'1', 
											'3'
											)
										),
									'3'
									)
								)
								)
							),
							(
								IF(DATE_FORMAT(t.tclose_dt,'%Y-%m-%d') = DATE_FORMAT(CURDATE(),'%Y-%m-%d'),
									(
										IF(DATE_FORMAT(t.topen_time,'%H:%i:%s') <= DATE_FORMAT(now(),'%H:%i:%s'),
											(
												IF(DATE_FORMAT(t.tclose_dt,'%Y-%m-%d') > DATE_FORMAT(CURDATE(),'%Y-%m-%d'),
													('1'),
												(
													IF(DATE_FORMAT(t.tclose_dt,'%Y-%m-%d') = DATE_FORMAT(CURDATE(),'%Y-%m-%d'),
														(
															IF(DATE_FORMAT(t.tclose_time,'%H:%i:%s') > DATE_FORMAT(now(),'%H:%i:%s'),
																('1'),
															'3'
															)
														),
													'3'
													)
												)
												)
											),
											IF(DATE_FORMAT(t.topen_time,'%H:%i:%s') > DATE_FORMAT(now(),'%H:%i:%s'),
												'2',
											1
											)
										)
									),
								IF(DATE_FORMAT(t.topen_dt,'%Y-%m-%d') > DATE_FORMAT(CURDATE(),'%Y-%m-%d'),
									'2',
								'3'
								)
								)
							)
						)
					)as status,
				v.vendor_id,
				t.tid as auctionid ,if(var.vendor_type=0, (select vendor_name from vendor where vendor_id =var.vendor_id), (select vendor_name from vendor where vendor_id = var.vendor_company_id)) as vendor_name,
                if(var.vendor_type=0, (select vendor_id from vendor where vendor_id =var.vendor_id), (select vendor_id from vendor where vendor_id = var.vendor_company_id)) as ven_id
                FROM vendor_auction_relation var 
				LEFT JOIN tender t ON t.tid = var.auction_id and auction_type = 5
				LEFT JOIN auction_images ai ON ai.auc_id = t.tid and auc_type = 't' 
				LEFT JOIN material_master mm ON FIND_IN_SET(mm.id, t.mat_id) > 0
				LEFT JOIN bidder_vendor_relation bvr ON bvr.vendor_id = t.vendor_id
                LEFT JOIN vendor v ON v.vendor_id = var.vendor_company_id 
                LEFT JOIN city_master cm ON cm.id = t.location
                LEFT JOIN state_master sm ON sm.id = cm.state_id
                LEFT JOIN country_master com ON com.id = sm.country_id
                WHERE 1=1  AND t.published = 'Y'";
                
                if ( ! empty($country))
                {
                    $query.=" &&  com.countries_iso_code_2 ='".$country."'";
                }
                if ( ! empty($mat_id))
                {
                    $query.=" && t.mat_id IN (".$mat_id.")";
                }
                if ( ! empty($auc_type))
                {
                    $query.=" && t.auctiontype = '".$auc_type."'";
                }
                if ( ! empty($vendor_id))
                {
                    $query.=" && (var.vendor_id ='".$vendor_id."' OR var.vendor_company_id='".$vendor_id."')";
                }
                $query.="GROUP BY t.tid ORDER BY sdate desc";
             
            $result =  $this->db->query($query)->result_array();
            
            $data_arr = array();
            if($result)
            {
               
                foreach($result  as $row)
                {		
                    	
                    //Converting results in user timezone
                   
                  /* if ( ! empty($this->session->userdata('user_tz')))
                   {
                        $row['start_date'] = usertz_to_servertz($row['start_date'],$this->session->userdata('user_tz'),'d-m-Y');
                        $row['end_date'] = usertz_to_servertz($row['end_date'],$this->session->userdata('user_tz'),'d-m-Y');
                        $row['stime'] = usertz_to_servertz($row['stime'],$this->session->userdata('user_tz'),'H:i:s');
                        $row['etime'] = usertz_to_servertz($row['etime'],$this->session->userdata('user_tz'),'H:i:s');

                   }*/
                    
                     if ($row['auction_visibility'] == 1)
                    { 
                        $data_arr[] = $row;
                    }
                    else if (($row['auction_visibility']==2) && (! empty($row['registered']) && $row['admin_approval']==1 && $row['vendor_approval']==1) && ($this->session->userdata('bidder_id') == $row['registered']))
                    {
                        $data_arr[] = $row;
                    }
                    else if ($row['auction_visibility'] == 3)
                    {
                        $this->db->select('client_id,auctionid');
                        $this->db->from('auction_client');
                        $this->db->where('auctionid',$row['auctionid']);
                        $res = $this->db->get()->result_array();
                        
                        if ($res)
                        {
                            foreach($res as $singlerow)
                            {
                                if ( ! empty($singlerow['auctionid']) && ($row['vendor_id']==$this->session->userdata('vendor_id') || $this->session->userdata('bidder_id') == $singlerow['client_id']))
                                {
                                    
                                    $data_arr[] = $row;
                                }
                            }
                        }
                        else
                        {
                            if ($row['vendor_id']==$this->session->userdata('vendor_id'))
                            {
                                $data_arr[] = $row;
                            }
                        } 
                         
                        
                        
                    }
                  
                }
            } 
	    	return array ('auctionlisting'=>$data_arr,'count'=>count($data_arr));
		
          
	}
	
	public function getmatdata(){
		 $query = "SELECT id,mat_name FROM material_master";
        $result = $this->db->query($query)->result_array();
        $mat = array();
       
        if ( ! empty($result))
        {
            
            foreach($result as $rows)
            {
               
                        $sql= "SELECT COUNT(ynk_auctionid) as cnt FROM ynk_auction WHERE mat_id IN (".$rows['id'].") and published ='Y'";
                        $resrows = $this->db->query($sql)->row_array();
                        $ynk_cnt = $resrows['cnt'];

                        $sql= "SELECT COUNT(tid) as cnt FROM tender WHERE mat_id IN (".$rows['id'].") and published = 'Y'";
                        $resrows = $this->db->query($sql)->row_array();
                        $tender_cnt = $resrows['cnt'];

                        $total =$ynk_cnt + $tender_cnt;
                        
                        if ($total > 0)
                        {
                            $mat[$rows['mat_name']]['count'] = $total;
                            $mat[$rows['mat_name']]['mat_id'] = $rows['id'];

                        }  
            }
        }
       
        return $mat;
	}
    

	//get current auctions
	 public function current_auctions()
    {
        $sql="SELECT distinct A.ynk_auctionid,A.ynk_saleno,A.start_date,A.stime,A.end_date,A.etime,A.auctiontype,A.particulars,V.vendor_name
		from ynk_auction A INNER JOIN vendor V ON V.vendor_id=A.vendor_id
        INNER JOIN ynk_auction_client C  ON C.ynk_auctionid = A.ynk_auctionid
		WHERE 1=1
		AND NOW() BETWEEN DATE_FORMAT(CONCAT(A.start_date,' ',A.stime),'%Y-%m-%d %H:%i:%s') and DATE_FORMAT(CONCAT(A.end_date,' ',A.etime),'%Y-%m-%d %H:%i:%s')
        AND published='Y' and blocked='N' AND C.client_id = '".$bidder_id."'";
       
        $query=$this->db->query($sql);
        
        return $query->result_array();
    }
	
	public function upcoming_auctions()
    {
        $sql="SELECT distinct A.ynk_auctionid,A.ynk_saleno,A.start_date,A.stime,A.end_date,A.etime,A.auctiontype,A.particulars,V.vendor_name
		from ynk_auction A, vendor V,ynk_auction_client C
		WHERE V.vendor_id=A.vendor_id
        AND client_id IN(select distinct client_id from ynk_auction_client where C.ynk_auctionid=A.ynk_auctionid AND C.client_id=".$this->session->userdata('bidder_id').")
		AND DATE_FORMAT(SYSDATE(),'%Y-%m-%d') < A.start_date
        AND published='Y' and blocked='N'";
        
        $query=$this->db->query($sql);
        
        return $query->result_array();
    }
	
}
