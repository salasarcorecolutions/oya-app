  /**
 * Salasarcore Solution
 *
 * @package		salasarauction
 * @author		SCS Dev Team
 * @copyright	Copyright (c) 2008 - 2014, EllisLab, Inc.
 * @license
 * @link		http://salasarauction.com
 * @since		Version 1.0
 */

/*
 * Check the user inputs
 * 
 */
function chkME(){
	

	return true;
}
/**
 * Highlight the lot on closing 5 Minute alert
 * @param obj
 */
function highlightLot(obj) {

	var lotno = obj.id;
	var parts = lotno.split('_');
	var periods = $('#timeleft_' + parts[1]).countdown('getTimes');
	
	if ($.countdown.periodsToSeconds(periods) < increase_lot_time_in) {
		$('#timeleft_' + parts[1]).toggleClass('highlight');
	}else{
		$('#timeleft_' + parts[1]).removeClass('highlight'); 
	}
 	
}
/**
 * Add lot dynamically using object id
 * @param $obj
 * @returns
 */
function AddLot(obj)
{
	
location.reload();

}

/**
 * upcoming lots
 * @param UpcomingLotsUrl
 * @returns
 */
function upcoming_lot(UpcomingLotsUrl){
	$.ajax({
		url:UpcomingLotsUrl,
		dataType:'json',
		success:function(data)
		{
			$("#Lot_Upcoming").empty();
			for (i=0;i<data.UpcomingLot.length;i++)
			{
				var LotNO=data.UpcomingLot[i].ynk_lotno;
				var currency=(data.UpcomingLot[i].currency==null)?"-":data.UpcomingLot[i].currency;
				var html="";
				html += 	"<tr id=Ulot_"+data.UpcomingLot[i].ynk_lotno+" data_id="+data.UpcomingLot[i].ynk_lotno+">"+
								"<td>" + data.UpcomingLot[i].ynk_lotno + "<hr style='margin: 5px 0 5px 0;'>" +data.UpcomingLot[i].vendorlotno+ "</td>"+
								"<td width='350px'><span title='"+data.UpcomingLot[i].product_details+"'>"+data.UpcomingLot[i].product+"</span></td>"+
								"<td>"+data.UpcomingLot[i].plant+"</td>"+
								"<td>"+parseFloat(data.UpcomingLot[i].totalqty)+" "+data.UpcomingLot[i].totunit+"</td>"+
								"<td>"+parseFloat(data.UpcomingLot[i].bidqty)+" "+data.UpcomingLot[i].bidunit+"</td>"+
								"<td>S :"+data.UpcomingLot[i].stime+"<br>E :"+data.UpcomingLot[i].etime+"</td>";
								if(vendor_id){
									if(data.permission.show_starting_bid=="Y"){
										html+="<td><span id='Sbid"+data.UpcomingLot[i].ynk_lotno+"'>"+parseFloat(data.UpcomingLot[i].startbid).toFixed(decimal_point)+"</span></td>";
									}
									if(data.permission.show_min_incr=="Y"){
										html+="<td><span id='Incr"+data.UpcomingLot[i].ynk_lotno+"'>"+parseFloat(data.UpcomingLot[i].incrby).toFixed(decimal_point)+"</span></td>";
									}
									if(data.permission.show_l1_bid=="Y"){
										html+="<td id='h1_"+data.UpcomingLot[i].ynk_lotno+"'><span id='H1Amt"+data.UpcomingLot[i].ynk_lotno+"'>0.00</span></td>";
									}
								}
								else{
								html+="<td><span id='Sbid"+data.UpcomingLot[i].ynk_lotno+"'>"+parseFloat(data.UpcomingLot[i].startbid).toFixed(decimal_point)+"</span></td>";
								html+="<td><span id='Incr"+data.UpcomingLot[i].ynk_lotno+"'>"+parseFloat(data.UpcomingLot[i].incrby).toFixed(decimal_point)+"</span></td>";
								html+="<td id='h1_"+data.UpcomingLot[i].ynk_lotno+"'><span id='H1Amt"+data.UpcomingLot[i].ynk_lotno+"'>0.00</span></td>";
								}
								html+="<td>" + currency + "</td>" +
								"<td id=timeleft_"+data.UpcomingLot[i].ynk_lotno+"></td>"+
							"</tr>";
				$("#Lot_Upcoming").append(html);
				$('#timeleft_' + data.UpcomingLot[i].ynk_lotno).countdown({padZeroes: true, until: +secondConvert(data.UpcomingLot[i].TOTTIME), onExpiry: function() { AddLot(this);},onTick: function(){highlightLot(this);}});
				OpenLots = OpenLots+data.UpcomingLot[i].ynk_lotno+",";
			}
			$("#open_lots").val(OpenLots);
			/* Only for Vendor To Show Report */
			if(data.show_closed_popup){
				if(data.show_closed_popup != 'no'){
					window.location.href=data.show_closed_popup;
				}
			}
		},
    });//AJAX
}

/**
 * Getting updates
 * @param LiveUpdateUrl
 */

 var user_status = "";
function waitForMsg(LiveUpdateUrl,BaseUrlCI){
	
	$.ajax({
        type: "POST",
        url:LiveUpdateUrl ,
		data:{"user_status":user_status},
		dataType:'json',
        cache: false,
        async: true, /* If set to non-async, browser shows page as "Loading.."*/
        cache: false,
        timeout:50000, /* Timeout in ms */
        success: function(data){ /* called when request to  completes */
		if(data){
			for (var key in data){
				console.lod
				rHtml="<table class='table table-bordered'><tr><th>Bidder</th><th>Qty. Alloted</th><th>Per / "+data[key]['bidqty']+" "+data[key]['total_unit']+"</th><th>Status</th><tr>";
				for (i=0;i<data[key]['bid_details'].length;i++){
					
					CLS = (client_id == data[key]['bid_details'][i]['bidder_id']) ? 'bg_green' : '';
					rHtml = rHtml +"<tr class='"+CLS+"'><td>"+data[key]['bid_details'][i]['compname']+"</td><td align='right'>"+number_formatter(data[key]['bid_details'][i]['sub_alloted_qty'])+" "+data[key]['total_unit']+"</td>";
					if(data[key]['show_l1_h1_bid']=="Y")
						rHtml = rHtml +"<td align='right'><span id='H1Amt"+data[key]['lotno']+"'>"+parseFloat(parseFloat(data[key]['bid_details'][i]['max_bid_amt']/parseFloat(conversion_rate)).toFixed(2))+ " "+ bidder_currency_code +"</span></td>";
					else 
						rHtml = rHtml +"<td align='right'><span id='H1Amt"+data[key]['lotno']+"'>****</td>";
					
					rHtml = rHtml +"<td> "+data[key]['bid_details'][i]['status_in_lot']+" <input type='hidden' id='my_own_quantity_"+data[key]['lotno']+"_"+data[key]['bid_details'][i]['bidder_id']+"' value='"+data[key]['bid_details'][i]['buyer_own_quantity']+"' /></td></tr>";
					//$('#txtQty_'+key).val(data[key]['bid_details'][i]['buyer_own_quantity']);
					
				}
				rHtml= rHtml+"<tr><td>FREE QTY:</td><td class='text-right'>"+data[key]['available_qty']+" "+data[key]['total_unit']+"</td><td colspan='2'></td>";
				rHtml= rHtml+"<table>";
				$('#UpdateBids_'+key).html(rHtml);
				$('#lot_extend_count_'+key).html(data[key]['no_of_extenstion']);
				
				if(data[key]['lot_close_date_time'] != $("#lot_close_date_time"+data[key]['lotno']).val() )
				{
				
				$("#timeleft_"+data[key]['lotno']+"").countdown('destroy');
				$("#timeleft_"+data[key]['lotno']+"").countdown({ padZeroes: true,until: +secondConvert(""+data[key]['TOTTIME']+""),onTick: function(){highlightLot(this);}, onExpiry: function() { CloseLot(this);}});
				$("#lot_close_date_time"+data[key]['lotno']).val(data[key]['lot_close_date_time']);
				
				}
				
			}
			
			
	
			user_status = data;
			OpenLots="";
			}
			$("#open_lots").val(OpenLots);
			
			setTimeout(function() {
				waitForMsg(LiveUpdateUrl,BaseUrlCI); // Do something after 5 seconds
			}, 5000);

        },
        error: function(XMLHttpRequest, textStatus, errorThrown){
            
		setTimeout(
			waitForMsg(LiveUpdateUrl,BaseUrlCI), /* Try again after.. */
		10000); /* milliseconds (1.5seconds) */
        }
    });
}

	var mul_fact;
	var add_fact;
	var total_qty;
	var min_bid_amt;
	var prefered_id;
	var primary_id;
	var conversionRate;
	$(document).on('click', ".btnBidWindow", function(e) {
		$("#IdMsg").html('');

		if ($(this).attr("data-part-bid") == "Y"){
			$("#bid_qty_tr").show();
			$("#bid_qty_unit").html($(this).attr("data-unit"));
			$("#MyBidQty").removeAttr("readonly");
		} else {
			$("#bid_qty_tr").hide();
			$("#MyBidQty").attr("readonly","readonly");
		}


		var MyOwnQtyID="my_own_quantity_"+$(this).attr("data-id")+"_"+client_id;
		if ($('#'+MyOwnQtyID+'').length){

			$("#MyBidQty").val(parseFloat($('#'+MyOwnQtyID+'').val()));
		} else {
			$("#MyBidQty").val(parseFloat($(this).attr("data-total-qty")));
		}
		mul_fact = $(this).attr("data-mul-fact");
		add_fact = $(this).attr("data-add-fact");
		total_qty = $(this).attr("data-total-qty");

		$("#lot_id").val($(this).attr("data-id"));

		$("#LotNo").html($(this).attr("data-id"));
		$("#totqty").html($(this).attr("data-total-qty"));
		//current bid value
		ltDivId='H1Amt'+$(this).attr("data-id");
		prefered_id = $(this).attr("data-currency-prefered-id");
		primary_id = $(this).attr("data-currency-primary-id");
		conversionRate = $(this).attr("data-prefered-currency-rate");
		if (prefered_id == primary_id){
			conversionRate = 1;
		} else {
			conversionRate = $(this).attr("data-prefered-currency-rate");
		}
		var preferedCurrencyBid = $('#'+ltDivId+'').html() * conversionRate;
		$("#pref_bid").html(noNaN(preferedCurrencyBid.toFixed(4))+' '+$(this).attr("data-prefered-currency"));
		$("#CB").html(noNaN($('#'+ltDivId+'').html())+' '+$(this).attr("data-currency"));
		$("#currency").html($(this).attr("data-prefered-currency"));
		$("#prod").html($(this).attr("data-prod"));
		$("#BidQty").html($(this).attr("data-min-part-bid")+' ' +$(this).attr("data-unit"));
		if (parseFloat($("#H1Amt" + $(this).attr("data-id")).html()) == "0"  || parseFloat($("#H1Amt" + $(this).attr("data-id")).html())==undefined || isNaN(parseFloat($("#H1Amt" + $(this).attr("data-id")).html())))
		{
			sum = parseFloat(parseFloat($(this).attr("data-start-bid")) + parseFloat($(this).attr("data-min-incr"))).toFixed(decimal_point);
		}
		else
		{
			sum = parseFloat(parseFloat($("#H1Amt" + $(this).attr("data-id")+"").html()) + parseFloat($(this).attr("data-min-incr"))).toFixed(decimal_point);
		}
		if (sum <= 0){
			sum = 0;
		}
		sum = parseFloat(sum);
		sum = noNaN(sum);
		// Set Parameter for Multiplication Factor and Additional Factor
		if (prefered_id == primary_id){
			sum = sum * 1;
		} else {
			sum = sum * conversionRate;
			sum = sum.toFixed(4);
		}
		$("#MyBid").val(sum);
		$("#mul_fact_amt").val(mul_fact);
		$("#add_fact_amt").val(add_fact);
		min_bid_amt = sum;
		calculate_mul_add_fact(sum,true);
		$("#btnSave").removeClass("hide");
		$("#modalBidWindow").modal("show");
	});

	function calculate_mul_add_fact(sum,flag){
		var cnt_new_test = 1;
		mul_fact_amt = parseFloat(sum)*parseFloat(mul_fact)/parseFloat(100);
		add_fact_amt = parseFloat(add_fact)/parseFloat(total_qty);		
		final_bid_amt = parseFloat(sum)+parseFloat(mul_fact_amt)+parseFloat(add_fact_amt);
		if (isNaN(mul_fact_amt)){
			mul_fact_amt=0.00;
		}
		if (isNaN(add_fact_amt)){
			add_fact_amt=0.00;
		}
		if (isNaN(final_bid_amt)){
			final_bid_amt=0.00;
		}

		$("#mul_fact_amt").val(mul_fact_amt.toFixed(decimal_point));
		$("#add_fact_amt").val(add_fact_amt.toFixed(decimal_point));
		$("#total_amt").val(final_bid_amt.toFixed(decimal_point));

		if (mul_fact_amt > 0 || add_fact_amt > 0)
		{
			sum = parseFloat(sum).toFixed(decimal_point);
			var bid_html = "";
			if (parseFloat(mul_fact_amt) > 0){
				bid_html = "<b>Multiplication Factor : </b> "+mul_fact+" => "+mul_fact_amt.toFixed(decimal_point)+" <br/>";
			}
			if (parseFloat(add_fact_amt) > 0){
				bid_html += "<b>Additional Factor : </b>"+parseFloat(add_fact)+" / "+parseFloat(total_qty)+" =>"+add_fact_amt.toFixed(decimal_point)+"<br/>";
			}
			bid_html += "<b>Final Bid Amount : <font color='red' size='5'>"+final_bid_amt.toFixed(decimal_point)+"</font></b> &nbsp;&nbsp;"+$("#currency").html();

			$("#final_bid_amt").html(bid_html);
			if (flag === true){
				$("#MyBid").val(noNaN(sum));
				final_bid_amt = final_bid_amt.toFixed(decimal_point);
				if (parseFloat(final_bid_amt) > parseFloat(min_bid_amt)){
					sum = parseFloat(sum)+parseFloat(0.01);
				} else {
					$('#modal').modal('show');

				}
			} else {

			}
		} else {
			$("#final_bid_amt").html("");
			$("#MyBid").val(noNaN(sum));
		}
	}

	/**
	 * iNCREASING 
	 * @param obj timerobject
	 */

	function CloseLot(obj)
	{
		
		if(obj != null){
			setTimeout(function() {
				var lotno = obj.id;
				var parts = lotno.split('_');
				$.ajax({
					url: lot_close_url + "/" +  parts[1],
					dataType:'json',
					success: function(data) {
						if(data.status)
							{
								$('table#lot_table tr#lot_' + parts[1] + '').remove();
							}
						else
							{
								$('#timeleft_' + parts[1]).countdown('option', {until: +secondConvert(data.auction_data.TOTTIME)});
							}
						}
				});
			}, 1000);
	    }else{
			//alert("Please refresh your page");
			window.location.reload();
		}
	}
	function noNaN(a){return (isNaN( a ) || a=="") ? 0 : a;}

	
	function number_formatter(num){
		num = num.replace(",","");
		num = parseFloat(num).toFixed(2);
		num = noNaN(num);
		return num;
	}