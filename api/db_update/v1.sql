
--Soumen 09-06-2020
ALTER TABLE `temp_bidders` ADD `state_other` VARCHAR( 100 ) NOT NULL AFTER `country` , ADD `city_other` VARCHAR( 100 ) NOT NULL AFTER `state_other`;
ALTER TABLE `bidders` ADD `state_other` VARCHAR( 100 ) NOT NULL AFTER `country` , ADD `city_other` VARCHAR( 100 ) NOT NULL AFTER `state_other`;
--Soumen 12-06-2020
ALTER TABLE `ynk_auction` ADD `lot_settings` TINYTEXT NOT NULL ;
ALTER TABLE `ynk_auction` CHANGE `auctiontype` `auctiontype` VARCHAR( 30 ) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL ;
ALTER TABLE `ynk_auction_docs` ADD `document_type` VARCHAR( 1 ) NOT NULL AFTER `doc_name` ,ADD `amazon_file_name` VARCHAR( 200 ) NOT NULL AFTER `document_type` ;
--soumen issue: 396
ALTER TABLE `ynk_auction_currency` ADD `primary_currency_id` INT NOT NULL AFTER `ynk_auction_id` ,
ADD `other_currency_id` INT NOT NULL AFTER `primary_currency_id` ,
ADD `currency_conversion_rate` FLOAT NOT NULL AFTER `other_currency_id` ,
ADD `date_updated` TIMESTAMP ON UPDATE CURRENT_TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP AFTER `currency_conversion_rate` ;

ALTER TABLE `ynk_auction_currency` CHANGE `currency_conversion_rate` `currency_conversion_rate` FLOAT NULL DEFAULT NULL;
--=========
ALTER TABLE `ynk_auction_currency` ADD `updated_by` INT( 11 ) NOT NULL ;
ALTER TABLE `ynk_auction_currency` DROP `auction_currency`;
--DROP TABLE ynk_currency_rate;
ALTER TABLE `ynk_auction` ADD `on_page_bid` VARCHAR( 1 ) NOT NULL DEFAULT 'N';
ALTER TABLE `ynk_lot_bid_details` ADD `conversion_rate` FLOAT NOT NULL AFTER `prefered_bid_currency` ;

--Shivani 17-07-2020
ALTER TABLE `temp_bidders` ADD `primary_email_verify` INT NOT NULL COMMENT '1=verified' AFTER `date_registered`;

ALTER TABLE `ynk_auction` ADD `primary_currency` VARCHAR(10) NULL AFTER `on_page_bid`;

---Pooja 27-07-2020

CREATE TABLE IF NOT EXISTS `buy_sell_bid_log` (
  `log_id` int(11) NOT NULL AUTO_INCREMENT,
  `product` int(11) NOT NULL,
  `bidder_id` int(11) NOT NULL,
  `bid_date_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `amount` decimal(12,2) NOT NULL,
  `log_note` varchar(500) NOT NULL,
  `bidder_ip` varchar(20) NOT NULL,
  PRIMARY KEY (`log_id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=latin1;

CREATE TABLE IF NOT EXISTS `favourite_product` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `bidder_id` int(11) NOT NULL,
  `product_id` text NOT NULL,
  `type` varchar(1) NOT NULL COMMENT 'P-Product,A-Auction,T-Tender',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=latin1;

CREATE TABLE `auctioneer_vendor_permission` (
  `auctioneer_vendor_permission_id` int(11) NOT NULL AUTO_INCREMENT,
  `vendor_id` int(11) DEFAULT NULL,
  `auctioneer_id` int(11) DEFAULT NULL,
  `permission_id` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `updated_datetime` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`auctioneer_vendor_permission_id`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=latin1;

CREATE TABLE `auctioneer_vendor_relation` (
  `auctioneer_vendor_relation_id` int(11) NOT NULL AUTO_INCREMENT,
  `auctioneer_id` int(11) DEFAULT NULL,
  `vendor_id` int(11) DEFAULT NULL,
  `is_active` int(1) DEFAULT NULL COMMENT '1 - Active, 0 - Inactive',
  `updated_by` int(11) DEFAULT NULL,
  `updated_datetime` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`auctioneer_vendor_relation_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

CREATE TABLE `roles_permissions` (
  `roles_permissions_id_new` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `role_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL,
  PRIMARY KEY (`roles_permissions_id_new`)
) ENGINE=InnoDB AUTO_INCREMENT=101 DEFAULT CHARSET=latin1;


--pooja 01-08-2020
CREATE TABLE `messages` (
  `msg_id` int(11) NOT NULL,
  `vendor_id` int(11) NOT NULL,
  `bidder_id` int(11) NOT NULL,
  `msg_sub` varchar(255) NOT NULL,
  `text_message` varchar(255) NOT NULL,
  `parent_id` int(11) NOT NULL COMMENT '0-if it is parent else auto msg_id of parent',
  `attachment` varchar(255) NOT NULL,
  `sender_id` int(11) NOT NULL,
  `sender_type` varchar(1) NOT NULL COMMENT 'b-buyer,v-vendor',
  `read` int(11) NOT NULL DEFAULT '0' COMMENT '0-unread,1-read',
  `msg_date_time` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for table `messages`
--
ALTER TABLE `messages`
  ADD PRIMARY KEY (`msg_id`);

CREATE TABLE `auction_lot_images` (
  `id` int(11) NOT NULL,
  `auc_type` varchar(1) DEFAULT NULL,
  `auc_id` varchar(50) DEFAULT NULL,
  `lot_id` int(11) DEFAULT NULL,
  `img_name` varchar(200) DEFAULT NULL,
  `date_added` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `auction_lot_images`
--
ALTER TABLE `auction_lot_images`
  ADD PRIMARY KEY (`id`);


  ALTER TABLE `bidders` ADD `logo` VARCHAR(200) NOT NULL AFTER `primary_mobile_verify`;

/*05-08-2000*/
CREATE TABLE `ynk_lot_tax_details` (
  `tax_id` int(11) NOT NULL,
  `ynk_auction_id` int(11) NOT NULL,
  `ynk_lot_id` int(11) NOT NULL,
  `tax_name` varchar(200) NOT NULL,
  `tax_rate` decimal(10,2) NOT NULL,
  `updated_on` datetime NOT NULL,
  `updated_by` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

ALTER TABLE `ynk_lot_tax_details` ADD PRIMARY KEY(`tax_id`);
ALTER TABLE `ynk_lot_tax_details` CHANGE `tax_id` `tax_id` INT(11) NOT NULL AUTO_INCREMENT;


/*faq*/
CREATE TABLE IF NOT EXISTS `faq` (
  `faq_id` int(10) NOT NULL AUTO_INCREMENT,
  `faq_group_id` int(11) NOT NULL,
  `quest` varchar(300) NOT NULL,
  `ans` mediumtext NOT NULL,
  `date_updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_by` varchar(30) NOT NULL,
  `order` int(10) NOT NULL,
  `published` varchar(1) NOT NULL DEFAULT 'N',
  PRIMARY KEY (`faq_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;


CREATE TABLE IF NOT EXISTS `faq_group` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `group_name` varchar(100) NOT NULL,
  `group_order` int(11) NOT NULL,
  `updated_by` varchar(50) NOT NULL,
  `updated_on` datetime NOT NULL,
  `is_active` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;


ALTER TABLE `password_reset_link` CHANGE `password_reset_link_id` `password_reset_link_id` INT(11) NOT NULL AUTO_INCREMENT;
ALTER TABLE `auction_lot_images` CHANGE `id` `id` INT( 11 ) NOT NULL AUTO_INCREMENT;
ALTER TABLE `vendor` ADD `other_state` VARCHAR( 100 ) NOT NULL AFTER `city` ,ADD `other_city` VARCHAR( 100 ) NOT NULL AFTER `other_state`;

ALTER TABLE `salasara_system_log` ADD `vendor_id` INT NOT NULL AFTER `type`;

//auction_y_proxy
//auction_y_proxy_history 

/*08-09-2020*/
CREATE TABLE `call_me_back` (
  `call_me_back_id` int(10) UNSIGNED NOT NULL,
  `call_me_user_name` varchar(50) DEFAULT NULL,
  `call_me_comp_name` varchar(100) DEFAULT NULL,
  `call_me_email_id` varchar(100) DEFAULT NULL,
  `call_me_remarks` mediumtext,
  `call_me_mobile` varchar(20) DEFAULT NULL,
  `updated_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `call_back_on` varchar(20) DEFAULT NULL,
  `prefered_time_from` time DEFAULT NULL,
  `prefered_time_to` time DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
ALTER TABLE `call_me_back` ADD `auc_type` VARCHAR(50) NOT NULL AFTER `call_me_mobile`;


ALTER TABLE `salasara_system_log` ADD `browser` VARCHAR(255) NOT NULL AFTER `vendor_id`, ADD `browserVersion` VARCHAR(255) NOT NULL AFTER `browser`, ADD `platform` VARCHAR(255) NOT NULL AFTER `browserVersion`, ADD `full_user_agent_string` VARCHAR(2048) NOT NULL AFTER `platform`, ADD `mac_address` VARCHAR(255) NOT NULL AFTER `full_user_agent_string`;

ALTER TABLE `company_plant` ADD `other_state` VARCHAR(100) NULL AFTER `city`, ADD `other_city` VARCHAR(100) NULL AFTER `other_state`;
ALTER TABLE `temp_company_plant` ADD `other_state` VARCHAR(100) NULL AFTER `city`, ADD `other_city` VARCHAR(100) NULL AFTER `other_state`;
ALTER TABLE `temp_vendor` ADD `other_state` VARCHAR(100) NULL AFTER `city`, ADD `other_city` VARCHAR(100) NULL AFTER `ohter_state`;




